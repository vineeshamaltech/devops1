
# Laravel Dunzo Clone


# How to use

 1. Clone the repo.
 2. Copy the .env.example as .env file and change the database and email configuration.
 3. Run the migration using `php artisan migrate`
 4. Run the following commands `php artisan cache:clear & php artisan config:clear`
 5. Link the storage using `php artisan storage:link`
