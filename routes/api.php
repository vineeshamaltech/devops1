<?php

use App\Http\Controllers\API\AdminAPIController;
use App\Http\Controllers\ServiceBooking\HomeServiceTimingAPIController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */

Route::group(['prefix' => 'user'], function () {
    Route::post('/login', [App\Http\Controllers\API\CustomerAuthAPIController::class, 'login']);
    Route::post('/phonelogin', [App\Http\Controllers\API\CustomerAuthAPIController::class, 'phoneLogin']);
    Route::post('/verify', [App\Http\Controllers\API\CustomerAuthAPIController::class, 'verifyOtp']);
    Route::post('/register', [App\Http\Controllers\API\CustomerAuthAPIController::class, 'register']);
    Route::post('/userRegister', [App\Http\Controllers\API\CustomerAuthAPIController::class, 'userRegister']);
    Route::group(['middleware' => 'auth:sanctum'], function () {
        Route::get('/profile', [App\Http\Controllers\API\CustomerAuthAPIController::class, 'userprofile']);
        Route::post('/profile', [App\Http\Controllers\API\CustomerAuthAPIController::class, 'updateProfile']);
        Route::post('/address/setdefault', [App\Http\Controllers\API\AddressAPIController::class, 'setDefault']);

        Route::post('/drivers/find', [App\Http\Controllers\API\DriverAPIController::class, 'sendCabOrderNotification']);
        //Cart APIs
        Route::get('cart', [App\Http\Controllers\API\CartAPIController::class, 'index']);
        Route::get('cart/refresh', [App\Http\Controllers\API\CartAPIController::class, 'refresh_cart']);
        Route::post('cart/add', [App\Http\Controllers\API\CartAPIController::class, 'addtocart']);
        Route::post('cart/change_address', [App\Http\Controllers\API\CartAPIController::class, 'changeDeliveryAddress']);
        Route::post('cart/apply_coupon', [App\Http\Controllers\API\CartAPIController::class, 'applyCoupon']);
        Route::get('cart/remove_coupon', [App\Http\Controllers\API\CartAPIController::class, 'removeCoupon']);
        Route::get('cart/clear', [App\Http\Controllers\API\CartAPIController::class, 'clearCart']);

        Route::post('cart/add_tip', [App\Http\Controllers\API\CartAPIController::class, 'setTipAmount']);
        Route::post('cart/pickup_from_store', [App\Http\Controllers\API\CartAPIController::class, 'setPickupFromStore']);
        Route::post('cart/pickup_address', [App\Http\Controllers\API\CartAPIController::class, 'setPickupAddress']);
        Route::post('cart/set_vehicle', [App\Http\Controllers\API\CartAPIController::class, 'setVehicle']);

        Route::post('cart/json_data', [App\Http\Controllers\API\CartAPIController::class, 'setJsonData']);
        Route::post('cart/add_service_cat_id', [App\Http\Controllers\API\CartAPIController::class, 'setService']);

        //Payment
        Route::post('razorpay/create', [App\Http\Controllers\API\RazorpayAPIController::class, 'createOrder']);
        Route::post('razorpay/verify', [App\Http\Controllers\API\RazorpayAPIController::class, 'getPayment']);
        Route::post('/razorpay/webhook', [App\Http\Controllers\API\RazorpayAPIController::class,'webhook']);
        //Recharge wallet
        Route::post('wallet/recharge', [App\Http\Controllers\API\UserAPIController::class, 'rechargeWallet']);
    });
});

Route::group(['prefix' => 'vendor'], function () {
    Route::post('/login', [App\Http\Controllers\API\VendorAPIController::class, 'login']);
    Route::post('/register', [App\Http\Controllers\API\VendorAPIController::class, 'register']);
    Route::group(['middleware' => 'auth:sanctum'], function () {
        Route::resource('kyc', 'VendorKycAPIController');
        Route::post('/update', [App\Http\Controllers\API\VendorAPIController::class, 'update']);
        Route::get('/profile', [App\Http\Controllers\API\VendorAPIController::class, 'profile']);
        Route::post('/reports', [App\Http\Controllers\API\ReportsAPIController::class, 'index']);
    });
    Route::post('/password/reset', [App\Http\Controllers\API\VendorAuthAPIController::class, 'resetPassword']);
    Route::post('/password/change', [App\Http\Controllers\API\VendorAuthAPIController::class, 'changePassword']);
});

Route::group(['prefix' => 'driver'], function () {
    Route::post('/phonelogin', [App\Http\Controllers\API\DriverAuthAPIController::class, 'phoneLogin']);
    Route::post('/verify', [App\Http\Controllers\API\DriverAuthAPIController::class, 'verifyOtp']);
Route::post('/driverRegister', [App\Http\Controllers\API\DriverAuthAPIController::class, 'driverRegister']);
    Route::group(['middleware' => 'auth:sanctum'], function () {
        Route::post('/earnings', [App\Http\Controllers\API\DriverAPIController::class, 'get_earnings']);
        Route::post('/wallet_recharge', [App\Http\Controllers\API\DriverAPIController::class, 'wallet_recharge']);
        Route::post('/order/driver-cancel', [App\Http\Controllers\API\OrderAPIController::class, 'driver_cancel_order']);
        Route::get('/pending_orders', [App\Http\Controllers\API\DriverAPIController::class, 'get_pending_orders']);
        Route::get('/order_stats', [App\Http\Controllers\API\DriverAPIController::class, 'get_order_stats']);
        Route::post('/sync', [App\Http\Controllers\API\DriverAuthAPIController::class, 'sync']);
        Route::post('/duty', [App\Http\Controllers\API\DriverAuthAPIController::class, 'toggle_duty']);
        Route::post('/update', [App\Http\Controllers\API\DriverAuthAPIController::class, 'update']);
        Route::get('/profile', [App\Http\Controllers\API\DriverAuthAPIController::class, 'profile']);
    });
});

Route::group(['middleware' => 'auth:sanctum'], function () {
    Route::resource('addresses', 'AddressAPIController');
    Route::resource('coupons', 'CouponAPIController');
    Route::resource('orders', 'OrderAPIController');
    
    Route::get('/orders_delete',[App\Http\Controllers\API\OrderAPIController::class, 'delete_orders']);
     
    Route::post('/update_order_payment',[App\Http\Controllers\API\OrderAPIController::class, 'update_order_payment']);
    Route::post('/change_payment',[App\Http\Controllers\API\OrderAPIController::class, 'change_payment']);
  
    Route::post('orders/{order}/changestatus', [App\Http\Controllers\API\OrderAPIController::class, 'update_order_status']);
    Route::resource('user_wallet_transactions', 'UserWalletTransactionAPIController');
    Route::get('/stores/{id}/stats', [App\Http\Controllers\API\StoreAPIController::class, 'stats']);

    Route::get('/nearby-drivers', [App\Http\Controllers\API\DriverAPIController::class, 'nearby_drivers']);
    Route::get('/vehicles', [App\Http\Controllers\API\VehicleTypeAPIController::class, 'get_vehicles']);

    Route::group(['prefix' => 'carpool'], function () {
        Route::resource('rides', 'CarPool\RidesAPIController');
        Route::get('ride/search', [\App\Http\Controllers\API\CarPool\RidesAPIController::class, 'search']);
        Route::post('/calculate-price', [\App\Http\Controllers\API\CarPool\RidesAPIController::class, 'calculatePrice']);
        Route::post('/create', [\App\Http\Controllers\API\CarPool\RidesAPIController::class, 'create']);
        Route::resource('stop_overs', 'CarPool\StopOverAPIController');
        Route::resource('ride_bookings', 'CarPool\RideBookingAPIController');
    });
    Route::resource('stores', 'StoreAPIController');
    Route::resource('banners', 'BannerAPIController');
});

Route::get('/notification-test', [App\Http\Controllers\Admin\NotificationController::class, 'test']);
Route::post('/autoassign', [App\Http\Controllers\API\OrderAPIController::class, 'autoAssign']);
Route::resource('categories', 'CategoryAPIController');
Route::get('/get_categories', [App\Http\Controllers\API\CategoryAPIController::class, 'get_categories']);

Route::get('/settings', [App\Http\Controllers\API\SettingsAPIController::class, 'index']);
Route::get('/search', [App\Http\Controllers\API\SearchAPIController::class, 'index']);
Route::get('/get_admin', [AdminAPIController::class, 'get_admin']);
Route::get('/get_vendor', [AdminAPIController::class, 'get_vendor']);
Route::get('/get_customer', [AdminAPIController::class, 'get_customer']);

Route::resource('admins', 'AdminAPIController');
Route::resource('users', 'UserAPIController');

Route::resource('store_categories', 'StoreCategoryAPIController');
Route::resource('service_categories', 'ServiceCategoryAPIController');
Route::resource('vehicle_types', 'VehicleTypeAPIController');
Route::resource('service_types', 'ServiceTypeAPIController');
Route::resource('payment_types', 'PaymentTypeAPIController');
Route::resource('store_reviews', 'StoreReviewsAPIController');
Route::resource('store_timings', 'StoreTimingAPIController');
Route::resource('products', 'ProductAPIController');
Route::resource('product_variants', 'ProductVariantAPIController');
Route::resource('drivers', 'DriverAPIController');
Route::resource('payments', 'PaymentAPIController');
Route::resource('delivery_fees', 'DeliveryFeeAPIController');
Route::resource('earnings', 'EarningAPIController');
Route::resource('driver_wallet_transactions', 'DriverWalletTransactionAPIController');
Route::resource('reviews', 'ReviewAPIController');
Route::resource('faqs', 'FAQAPIController');
Route::resource('home_service_categories', 'ServiceBooking\HomeServiceCategoryAPIController');
Route::resource('home_services', 'ServiceBooking\HomeServiceAPIController');
Route::resource('home_services_timing', 'ServiceBooking\HomeServiceTimingAPIController');

Route::get('zones', [App\Http\Controllers\API\ZoneAPIController::class, 'index']);
Route::get('get_all_driver', [App\Http\Controllers\API\DriverAPIController::class, 'get_all_driver']);

Route::group(['prefix' => 'service'], function () {
    Route::get('/drivers', [App\Http\Controllers\API\DriverAPIController::class, 'getHomeServiceDrivers']);
    Route::group(['middleware' => 'auth:sanctum'], function () {
        Route::get('cart/get', [App\Http\Controllers\API\ServiceBooking\ServiceCartAPIController::class, 'getCart']);
        Route::post('cart/add', [App\Http\Controllers\API\ServiceBooking\ServiceCartAPIController::class, 'addtocart']);
        Route::get('cart/clear', [App\Http\Controllers\API\ServiceBooking\ServiceCartAPIController::class, 'clearCart']);
        Route::get('cart/refresh', [App\Http\Controllers\API\ServiceBooking\ServiceCartAPIController::class, 'refresh_cart']);
        Route::post('cart/set_address', [App\Http\Controllers\API\ServiceBooking\ServiceCartAPIController::class, 'setAddress']);

        Route::resource('get_timeslots', 'ServiceBooking\TimeSlotAPIController');
        Route::resource('service_order', 'ServiceBooking\ServiceOrderAPIController');
         Route::get('service_orders/check_autoassign', [App\Http\Controllers\API\ServiceBooking\ServiceOrderAPIController::class, 'check_autoassign']);
        Route::post('service_orders/changestatus', [App\Http\Controllers\API\ServiceBooking\ServiceOrderAPIController::class, 'update_order_status']);
    });

});
