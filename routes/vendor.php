<?php
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/login',[App\Http\Controllers\Auth\VendorLoginController::class, 'loginform'])->name('loginform');
Route::post('/login',[App\Http\Controllers\Auth\VendorLoginController::class, 'login'])->name('login');

Route::group(['middleware'=>['auth:vendor']],function (){
    Route::get('/dashboard',[App\Http\Controllers\Auth\VendorLoginController::class, 'dashboard'])->name('dashboard');
    Route::resource('categories', 'CategoryController');
    Route::resource('coupons', 'CouponController');
    
    Route::resource('banners', 'BannerController');
    Route::resource('stores', 'StoreController');
    Route::resource('products', 'ProductController');
    Route::resource('storeTimings', 'StoreTimingController');
});
