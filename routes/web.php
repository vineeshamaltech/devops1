<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', [App\Http\Controllers\HomeController::class,'index'])->name('root');
Route::get('/', function () {
    // return view('welcome');
    return redirect('/home');
});

Auth::routes();
Route::middleware(['auth'])->group(function (){
    Route::get('/home', [App\Http\Controllers\Admin\HomeController::class,'settings']);
    Route::get('/getorders',[App\Http\Controllers\Admin\HomeController::class, 'get_orders'])->name('getorders');
 
});
Route::middleware(['auth,vendor'])->group(function () {
    Route::get('/profile', [App\Http\Controllers\Admin\HomeController::class,'profile']);
  
});

Route::get('/page/{slug}', [\App\Http\Controllers\Admin\PageController::class,'show'])->name('page.show');
