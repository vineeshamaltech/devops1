<?php

use App\Http\Controllers\Admin\HomeServiceBooking\HomeServiceController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
|
| Here is where you can register admin routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "admin" middleware group. Now create something great!
|
*/

 Route::group(['prefix' => 'api'], function(){
        Route::get('/banners',[App\Http\Controllers\API\BannerAPIController::class, 'index']);
        Route::get('home_service_categories', [App\Http\Controllers\API\ServiceBooking\HomeServiceCategoryAPIController::class, 'index']);
        
    });


Route::group(['middleware'=>['auth']],function (){
    Route::impersonate();
    Route::get('/',[App\Http\Controllers\Admin\HomeController::class, 'settings'])->name('dashboard');
    Route::resource('categories', 'CategoryController');
    Route::get('/get_categories',[App\Http\Controllers\Admin\CategoryController::class, 'get_categories'])->name('get_categories');
    Route::post('categories/destroy_multiple',[App\Http\Controllers\Admin\CategoryController::class,'destroy_multiple'])->name('destroy_multiple');
    Route::resource('coupons', 'CouponController');
    Route::post('coupons/destroy_multiple',[App\Http\Controllers\Admin\CouponController::class,'destroy_multiple'])->name('destroy_multiple_coupon');
    Route::resource('roles', 'RoleController');
    Route::resource('users', 'UserController');
    Route::resource('admins', 'AdminController');
    // Route::resource('vehicles', 'VehicleController');

    Route::resource('vendorKycs', 'VendorKycController');
    Route::resource('driverKycs', 'DriverKycController');
    Route::resource('reviews', 'ReviewController');
    Route::post('reviews/destroy_multiple',[App\Http\Controllers\Admin\ReviewController::class,'destroy_multiple'])->name('destroy_multiple_review');
    Route::get('/vendorKyc/{id}/approve', [\App\Http\Controllers\Admin\VendorKycController::class,'approve'])->name('vendorKycs.approve');
    Route::get('/vendorKyc/{id}/reject', [\App\Http\Controllers\Admin\VendorKycController::class,'reject'])->name('vendorKycs.reject');

    Route::get('/vendorKyc/{id}/delete', [\App\Http\Controllers\Admin\VendorKycController::class,'delete'])->name('vendorKycs.delete');
    Route::post('/vendorKyc/{id}/storeImage', [\App\Http\Controllers\Admin\VendorKycController::class,'storeImage'])->name('vendorKycs.storeImage');
    Route::get('/vendorKyc/{id}/download', [\App\Http\Controllers\Admin\VendorKycController::class,'download'])->name('vendorKycs.download');

    Route::get('/vendorKyc/approve/all/{id}', [\App\Http\Controllers\Admin\VendorKycController::class,'approveall'])->name('vendorKycs.approveall');
    Route::get('/vendorKyc/reject/all/{id}', [\App\Http\Controllers\Admin\VendorKycController::class,'rejectall'])->name('vendorKycs.rejectall');

    Route::get('/driverKycs/{id}/approve', [\App\Http\Controllers\Admin\DriverKycController::class,'approve'])->name('driverKycs.approve');
    Route::get('/driverKycs/{id}/reject', [\App\Http\Controllers\Admin\DriverKycController::class,'reject'])->name('driverKycs.reject');
    Route::get('/driverKycs/approve/all/{id}', [\App\Http\Controllers\Admin\DriverKycController::class,'approveall'])->name('driverKycs.approveall');
    Route::get('/driverKycs/reject/all/{id}', [\App\Http\Controllers\Admin\DriverKycController::class,'rejectall'])->name('driverKycs.rejectall');
    Route::resource('vendors', 'VendorController');
    // Route::resource('services', 'ServiceController');
    Route::resource('paymentTypes', 'PaymentTypeController');
    Route::resource('serviceTypes', 'ServiceTypeController');
    Route::resource('vehicleTypes', 'VehicleTypeController');
    Route::post('vehicleTypes/destroy_multiple',[App\Http\Controllers\Admin\VehicleTypeController::class,'destroy_multiple'])->name('destroy_multiple_vehicle_type');
    Route::resource('serviceCategories', 'ServiceCategoryController');
    Route::resource('storeCategories', 'StoreCategoryController');
    Route::post('storeCategories/destroy_multiple',[App\Http\Controllers\Admin\StoreCategoryController::class,'destroy_multiple'])->name('destroy_multiple_storeCategories');
    Route::resource('banners', 'BannerController');    
    Route::post('banners/destroy_multiple',[App\Http\Controllers\Admin\BannerController::class,'destroy_multiple'])->name('destroy_multiple_banner');
    Route::resource('send_push', 'NotificationController');
    Route::resource('stores', 'StoreController');
    Route::get('/get_store',[App\Http\Controllers\Admin\StoreController::class, 'get_store'])->name('get_store');
    Route::resource('storeTimings', 'StoreTimingController');
    Route::resource('products', 'ProductController');
    Route::post('products/ProductController',[App\Http\Controllers\Admin\ProductController::class,'destroy_multiple'])->name('destroy_multiple_products');
    Route::resource('productVariants', 'ProductVariantController');
    Route::resource('drivers', 'DriverController');
    Route::resource('addresses', 'AddressController');
    Route::resource('payments', 'PaymentController');
    Route::resource('deliveryFees', 'DeliveryFeeController');
    Route::post('deliveryFees/destroy_multiple',[App\Http\Controllers\Admin\DeliveryFeeController::class,'destroy_multiple'])->name('destroy_multiple_delivery_fee');
    Route::get('logintime/{id}', [\App\Http\Controllers\Admin\DriverController::class,'logintime'])->name('logintime');

    Route::resource('homeOrders', 'HomeOrderController');
    Route::resource('orders', 'OrderController');
    Route::post('orders/changeStatus',[App\Http\Controllers\Admin\OrderController::class,'changeStatus'])->name('changeStatus');
    Route::post('orders/assign',[App\Http\Controllers\Admin\OrderController::class,'assignDriver'])->name('assignDriver');
    Route::post('orders/ping',[App\Http\Controllers\Admin\OrderController::class,'pingDriver'])->name('pingDriver');

    Route::post('service/orders/assign',[App\Http\Controllers\Admin\HomeOrderController::class,'assignServiceDriver'])->name('assignServiceDriver');
    Route::resource('pages', 'PageController');

    Route::group(['prefix' => 'carpool',], function () {
        Route::resource('rides', 'RidesController');
        Route::resource('rideBookings', 'RideBookingController');
    });
    
    
   

    Route::resource('notification', 'NotificationController');
    Route::resource('earnings', 'EarningController');
    Route::post('earnings/destroy_multiple',[App\Http\Controllers\Admin\EarningController::class,'destroy_multiple'])->name('destroy_multiple_earning');
    Route::resource('driverWalletTransactions', 'DriverWalletTransactionController');
    Route::post('driverWalletTransactions/destroy_multiple',[App\Http\Controllers\Admin\DriverWalletTransactionController::class,'destroy_multiple'])->name('destroy_multiple_driverWalletTransactions');
    Route::resource('userWalletTransactions', 'UserWalletTransactionController');
    Route::post('userWalletTransactions/destroy_multiple',[App\Http\Controllers\Admin\UserWalletTransactionController::class,'destroy_multiple'])->name('destroy_multiple_userWalletTransactions');
    Route::get('/earningReport', [\App\Http\Controllers\Admin\EarningReportController::class,'index'])->name('earnings.report.index');
    Route::post('/earningReport', [\App\Http\Controllers\Admin\EarningReportController::class,'generate'])->name('earnings.report.generate');

    Route::resource('zones', 'ZoneController');
    Route::resource('timeSlots','TimeSlotsController');
    //Home Services
    Route::resource('homeServices', 'HomeServiceBooking\HomeServiceController');
    Route::resource('homeServiceCategories', 'HomeServiceBooking\HomeServiceCategoryController');
    Route::get('homeServiceTiming/{id}', [HomeServiceController::class,'homeServiceTiming'])->name('homeService.timing');
    Route::post('homeService/setTiming', [HomeServiceController::class,'setTiming'])->name('homeServices.setTiming');

    Route::resource('faqs', 'FAQController');
    Route::resource('sendmails', 'SendOutMailController');
    Route::post('sendmail', [\App\Http\Controllers\Admin\SendOutMailController::class,'send'])->name('sendmails.send');
    //Import/Export
    Route::get('import-export', [\App\Http\Controllers\Admin\ImportExportController::class,'index'])->name('importExport.index');
    Route::post('import', [\App\Http\Controllers\Admin\ImportExportController::class,'import'])->name('importExport.import');
    Route::post('export', [\App\Http\Controllers\Admin\ImportExportController::class,'export'])->name('importExport.export');
    //Reports
    Route::get('reports', [\App\Http\Controllers\Admin\ReportController::class,'index'])->name('reports.index');
    Route::post('reports', [\App\Http\Controllers\Admin\ReportController::class,'generate'])->name('reports.generate');

    Route::get('settings',[\App\Http\Controllers\Admin\SettingsController::class,'index'])->name('settings.index');
    Route::post('settings',[\App\Http\Controllers\Admin\SettingsController::class,'update'])->name('settings.update');
    Route::post('grant-permission',[App\Http\Controllers\API\RoleAPIController::class,'grant_permission'])->name('grand_permission');
    Route::post('revoke-permission',[App\Http\Controllers\API\RoleAPIController::class,'revoke_permission'])->name('revoke_permission');
    Route::post('filterByZone',[\App\Http\Controllers\Admin\OrderController::class,'filterByZone'])->name('orders.filterByZone');
    
       Route::get('/driversmaps',[\App\Http\Controllers\Admin\TrackingMapsController::class,'getTrackingMaps'])->name('trackingmaps.index');
    Route::get('/merchantsmaps',[\App\Http\Controllers\Admin\TrackingMapsController::class,'getTrackingMerchantes'])->name('trackingmaps.merchants');


});
