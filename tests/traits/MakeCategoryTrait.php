<?php

use Faker\Factory as Faker;
use App\Models\StoreCategory;
use App\Repositories\CategoryRepository;

trait MakeCategoryTrait
{
    /**
     * Create fake instance of StoreCategory and save it in database
     *
     * @param array $categoryFields
     * @return StoreCategory
     */
    public function makeCategory($categoryFields = [])
    {
        /** @var CategoryRepository $categoryRepo */
        $categoryRepo = App::make(CategoryRepository::class);
        $theme = $this->fakeCategoryData($categoryFields);
        return $categoryRepo->create($theme);
    }

    /**
     * Get fake instance of StoreCategory
     *
     * @param array $categoryFields
     * @return StoreCategory
     */
    public function fakeCategory($categoryFields = [])
    {
        return new StoreCategory($this->fakeCategoryData($categoryFields));
    }

    /**
     * Get fake data of StoreCategory
     *
     * @param array $postFields
     * @return array
     */
    public function fakeCategoryData($categoryFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'name' => $fake->word,
            'image' => $fake->word,
            'parent' => $fake->randomDigitNotNull,
            'status' => $fake->word,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $categoryFields);
    }
}
