<?php namespace Tests\Repositories;

use App\Models\StoreCategory;
use App\Repositories\StoreCategoryRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class StoreCategoryRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var StoreCategoryRepository
     */
    protected $storeCategoryRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->storeCategoryRepo = \App::make(StoreCategoryRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_store_category()
    {
        $storeCategory = factory(StoreCategory::class)->make()->toArray();

        $createdStoreCategory = $this->storeCategoryRepo->create($storeCategory);

        $createdStoreCategory = $createdStoreCategory->toArray();
        $this->assertArrayHasKey('id', $createdStoreCategory);
        $this->assertNotNull($createdStoreCategory['id'], 'Created StoreCategory must have id specified');
        $this->assertNotNull(StoreCategory::find($createdStoreCategory['id']), 'StoreCategory with given id must be in DB');
        $this->assertModelData($storeCategory, $createdStoreCategory);
    }

    /**
     * @test read
     */
    public function test_read_store_category()
    {
        $storeCategory = factory(StoreCategory::class)->create();

        $dbStoreCategory = $this->storeCategoryRepo->find($storeCategory->id);

        $dbStoreCategory = $dbStoreCategory->toArray();
        $this->assertModelData($storeCategory->toArray(), $dbStoreCategory);
    }

    /**
     * @test update
     */
    public function test_update_store_category()
    {
        $storeCategory = factory(StoreCategory::class)->create();
        $fakeStoreCategory = factory(StoreCategory::class)->make()->toArray();

        $updatedStoreCategory = $this->storeCategoryRepo->update($fakeStoreCategory, $storeCategory->id);

        $this->assertModelData($fakeStoreCategory, $updatedStoreCategory->toArray());
        $dbStoreCategory = $this->storeCategoryRepo->find($storeCategory->id);
        $this->assertModelData($fakeStoreCategory, $dbStoreCategory->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_store_category()
    {
        $storeCategory = factory(StoreCategory::class)->create();

        $resp = $this->storeCategoryRepo->delete($storeCategory->id);

        $this->assertTrue($resp);
        $this->assertNull(StoreCategory::find($storeCategory->id), 'StoreCategory should not exist in DB');
    }
}
