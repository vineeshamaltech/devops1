<?php namespace Tests\Repositories;

use App\Models\StopOver;
use App\Repositories\StopOverRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class StopOverRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var StopOverRepository
     */
    protected $stopOverRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->stopOverRepo = \App::make(StopOverRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_stop_over()
    {
        $stopOver = factory(StopOver::class)->make()->toArray();

        $createdStopOver = $this->stopOverRepo->create($stopOver);

        $createdStopOver = $createdStopOver->toArray();
        $this->assertArrayHasKey('id', $createdStopOver);
        $this->assertNotNull($createdStopOver['id'], 'Created StopOver must have id specified');
        $this->assertNotNull(StopOver::find($createdStopOver['id']), 'StopOver with given id must be in DB');
        $this->assertModelData($stopOver, $createdStopOver);
    }

    /**
     * @test read
     */
    public function test_read_stop_over()
    {
        $stopOver = factory(StopOver::class)->create();

        $dbStopOver = $this->stopOverRepo->find($stopOver->id);

        $dbStopOver = $dbStopOver->toArray();
        $this->assertModelData($stopOver->toArray(), $dbStopOver);
    }

    /**
     * @test update
     */
    public function test_update_stop_over()
    {
        $stopOver = factory(StopOver::class)->create();
        $fakeStopOver = factory(StopOver::class)->make()->toArray();

        $updatedStopOver = $this->stopOverRepo->update($fakeStopOver, $stopOver->id);

        $this->assertModelData($fakeStopOver, $updatedStopOver->toArray());
        $dbStopOver = $this->stopOverRepo->find($stopOver->id);
        $this->assertModelData($fakeStopOver, $dbStopOver->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_stop_over()
    {
        $stopOver = factory(StopOver::class)->create();

        $resp = $this->stopOverRepo->delete($stopOver->id);

        $this->assertTrue($resp);
        $this->assertNull(StopOver::find($stopOver->id), 'StopOver should not exist in DB');
    }
}
