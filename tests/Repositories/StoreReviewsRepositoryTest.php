<?php namespace Tests\Repositories;

use App\Models\StoreReviews;
use App\Repositories\StoreReviewsRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class StoreReviewsRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var StoreReviewsRepository
     */
    protected $storeReviewsRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->storeReviewsRepo = \App::make(StoreReviewsRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_store_reviews()
    {
        $storeReviews = factory(StoreReviews::class)->make()->toArray();

        $createdStoreReviews = $this->storeReviewsRepo->create($storeReviews);

        $createdStoreReviews = $createdStoreReviews->toArray();
        $this->assertArrayHasKey('id', $createdStoreReviews);
        $this->assertNotNull($createdStoreReviews['id'], 'Created StoreReviews must have id specified');
        $this->assertNotNull(StoreReviews::find($createdStoreReviews['id']), 'StoreReviews with given id must be in DB');
        $this->assertModelData($storeReviews, $createdStoreReviews);
    }

    /**
     * @test read
     */
    public function test_read_store_reviews()
    {
        $storeReviews = factory(StoreReviews::class)->create();

        $dbStoreReviews = $this->storeReviewsRepo->find($storeReviews->id);

        $dbStoreReviews = $dbStoreReviews->toArray();
        $this->assertModelData($storeReviews->toArray(), $dbStoreReviews);
    }

    /**
     * @test update
     */
    public function test_update_store_reviews()
    {
        $storeReviews = factory(StoreReviews::class)->create();
        $fakeStoreReviews = factory(StoreReviews::class)->make()->toArray();

        $updatedStoreReviews = $this->storeReviewsRepo->update($fakeStoreReviews, $storeReviews->id);

        $this->assertModelData($fakeStoreReviews, $updatedStoreReviews->toArray());
        $dbStoreReviews = $this->storeReviewsRepo->find($storeReviews->id);
        $this->assertModelData($fakeStoreReviews, $dbStoreReviews->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_store_reviews()
    {
        $storeReviews = factory(StoreReviews::class)->create();

        $resp = $this->storeReviewsRepo->delete($storeReviews->id);

        $this->assertTrue($resp);
        $this->assertNull(StoreReviews::find($storeReviews->id), 'StoreReviews should not exist in DB');
    }
}
