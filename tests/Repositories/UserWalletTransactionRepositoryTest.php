<?php namespace Tests\Repositories;

use App\Models\UserWalletTransaction;
use App\Repositories\UserWalletTransactionRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class UserWalletTransactionRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var UserWalletTransactionRepository
     */
    protected $userWalletTransactionRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->userWalletTransactionRepo = \App::make(UserWalletTransactionRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_user_wallet_transaction()
    {
        $userWalletTransaction = factory(UserWalletTransaction::class)->make()->toArray();

        $createdUserWalletTransaction = $this->userWalletTransactionRepo->create($userWalletTransaction);

        $createdUserWalletTransaction = $createdUserWalletTransaction->toArray();
        $this->assertArrayHasKey('id', $createdUserWalletTransaction);
        $this->assertNotNull($createdUserWalletTransaction['id'], 'Created UserWalletTransaction must have id specified');
        $this->assertNotNull(UserWalletTransaction::find($createdUserWalletTransaction['id']), 'UserWalletTransaction with given id must be in DB');
        $this->assertModelData($userWalletTransaction, $createdUserWalletTransaction);
    }

    /**
     * @test read
     */
    public function test_read_user_wallet_transaction()
    {
        $userWalletTransaction = factory(UserWalletTransaction::class)->create();

        $dbUserWalletTransaction = $this->userWalletTransactionRepo->find($userWalletTransaction->id);

        $dbUserWalletTransaction = $dbUserWalletTransaction->toArray();
        $this->assertModelData($userWalletTransaction->toArray(), $dbUserWalletTransaction);
    }

    /**
     * @test update
     */
    public function test_update_user_wallet_transaction()
    {
        $userWalletTransaction = factory(UserWalletTransaction::class)->create();
        $fakeUserWalletTransaction = factory(UserWalletTransaction::class)->make()->toArray();

        $updatedUserWalletTransaction = $this->userWalletTransactionRepo->update($fakeUserWalletTransaction, $userWalletTransaction->id);

        $this->assertModelData($fakeUserWalletTransaction, $updatedUserWalletTransaction->toArray());
        $dbUserWalletTransaction = $this->userWalletTransactionRepo->find($userWalletTransaction->id);
        $this->assertModelData($fakeUserWalletTransaction, $dbUserWalletTransaction->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_user_wallet_transaction()
    {
        $userWalletTransaction = factory(UserWalletTransaction::class)->create();

        $resp = $this->userWalletTransactionRepo->delete($userWalletTransaction->id);

        $this->assertTrue($resp);
        $this->assertNull(UserWalletTransaction::find($userWalletTransaction->id), 'UserWalletTransaction should not exist in DB');
    }
}
