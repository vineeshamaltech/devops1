<?php namespace Tests\Repositories;

use App\Models\DriverWalletTransaction;
use App\Repositories\DriverWalletTransactionRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class DriverWalletTransactionRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var DriverWalletTransactionRepository
     */
    protected $driverWalletTransactionRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->driverWalletTransactionRepo = \App::make(DriverWalletTransactionRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_driver_wallet_transaction()
    {
        $driverWalletTransaction = factory(DriverWalletTransaction::class)->make()->toArray();

        $createdDriverWalletTransaction = $this->driverWalletTransactionRepo->create($driverWalletTransaction);

        $createdDriverWalletTransaction = $createdDriverWalletTransaction->toArray();
        $this->assertArrayHasKey('id', $createdDriverWalletTransaction);
        $this->assertNotNull($createdDriverWalletTransaction['id'], 'Created DriverWalletTransaction must have id specified');
        $this->assertNotNull(DriverWalletTransaction::find($createdDriverWalletTransaction['id']), 'DriverWalletTransaction with given id must be in DB');
        $this->assertModelData($driverWalletTransaction, $createdDriverWalletTransaction);
    }

    /**
     * @test read
     */
    public function test_read_driver_wallet_transaction()
    {
        $driverWalletTransaction = factory(DriverWalletTransaction::class)->create();

        $dbDriverWalletTransaction = $this->driverWalletTransactionRepo->find($driverWalletTransaction->id);

        $dbDriverWalletTransaction = $dbDriverWalletTransaction->toArray();
        $this->assertModelData($driverWalletTransaction->toArray(), $dbDriverWalletTransaction);
    }

    /**
     * @test update
     */
    public function test_update_driver_wallet_transaction()
    {
        $driverWalletTransaction = factory(DriverWalletTransaction::class)->create();
        $fakeDriverWalletTransaction = factory(DriverWalletTransaction::class)->make()->toArray();

        $updatedDriverWalletTransaction = $this->driverWalletTransactionRepo->update($fakeDriverWalletTransaction, $driverWalletTransaction->id);

        $this->assertModelData($fakeDriverWalletTransaction, $updatedDriverWalletTransaction->toArray());
        $dbDriverWalletTransaction = $this->driverWalletTransactionRepo->find($driverWalletTransaction->id);
        $this->assertModelData($fakeDriverWalletTransaction, $dbDriverWalletTransaction->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_driver_wallet_transaction()
    {
        $driverWalletTransaction = factory(DriverWalletTransaction::class)->create();

        $resp = $this->driverWalletTransactionRepo->delete($driverWalletTransaction->id);

        $this->assertTrue($resp);
        $this->assertNull(DriverWalletTransaction::find($driverWalletTransaction->id), 'DriverWalletTransaction should not exist in DB');
    }
}
