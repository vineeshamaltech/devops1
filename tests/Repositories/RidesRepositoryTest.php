<?php namespace Tests\Repositories;

use App\Models\Ride;
use App\Repositories\RidesRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class RidesRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var RidesRepository
     */
    protected $ridesRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->ridesRepo = \App::make(RidesRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_rides()
    {
        $rides = factory(Ride::class)->make()->toArray();

        $createdRides = $this->ridesRepo->create($rides);

        $createdRides = $createdRides->toArray();
        $this->assertArrayHasKey('id', $createdRides);
        $this->assertNotNull($createdRides['id'], 'Created Ride must have id specified');
        $this->assertNotNull(Ride::find($createdRides['id']), 'Ride with given id must be in DB');
        $this->assertModelData($rides, $createdRides);
    }

    /**
     * @test read
     */
    public function test_read_rides()
    {
        $rides = factory(Ride::class)->create();

        $dbRides = $this->ridesRepo->find($rides->id);

        $dbRides = $dbRides->toArray();
        $this->assertModelData($rides->toArray(), $dbRides);
    }

    /**
     * @test update
     */
    public function test_update_rides()
    {
        $rides = factory(Ride::class)->create();
        $fakeRides = factory(Ride::class)->make()->toArray();

        $updatedRides = $this->ridesRepo->update($fakeRides, $rides->id);

        $this->assertModelData($fakeRides, $updatedRides->toArray());
        $dbRides = $this->ridesRepo->find($rides->id);
        $this->assertModelData($fakeRides, $dbRides->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_rides()
    {
        $rides = factory(Ride::class)->create();

        $resp = $this->ridesRepo->delete($rides->id);

        $this->assertTrue($resp);
        $this->assertNull(Ride::find($rides->id), 'Ride should not exist in DB');
    }
}
