<?php namespace Tests\Repositories;

use App\Models\VendorKyc;
use App\Repositories\VendorKycRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class VendorKycRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var VendorKycRepository
     */
    protected $vendorKycRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->vendorKycRepo = \App::make(VendorKycRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_vendor_kyc()
    {
        $vendorKyc = factory(VendorKyc::class)->make()->toArray();

        $createdVendorKyc = $this->vendorKycRepo->create($vendorKyc);

        $createdVendorKyc = $createdVendorKyc->toArray();
        $this->assertArrayHasKey('id', $createdVendorKyc);
        $this->assertNotNull($createdVendorKyc['id'], 'Created VendorKyc must have id specified');
        $this->assertNotNull(VendorKyc::find($createdVendorKyc['id']), 'VendorKyc with given id must be in DB');
        $this->assertModelData($vendorKyc, $createdVendorKyc);
    }

    /**
     * @test read
     */
    public function test_read_vendor_kyc()
    {
        $vendorKyc = factory(VendorKyc::class)->create();

        $dbVendorKyc = $this->vendorKycRepo->find($vendorKyc->id);

        $dbVendorKyc = $dbVendorKyc->toArray();
        $this->assertModelData($vendorKyc->toArray(), $dbVendorKyc);
    }

    /**
     * @test update
     */
    public function test_update_vendor_kyc()
    {
        $vendorKyc = factory(VendorKyc::class)->create();
        $fakeVendorKyc = factory(VendorKyc::class)->make()->toArray();

        $updatedVendorKyc = $this->vendorKycRepo->update($fakeVendorKyc, $vendorKyc->id);

        $this->assertModelData($fakeVendorKyc, $updatedVendorKyc->toArray());
        $dbVendorKyc = $this->vendorKycRepo->find($vendorKyc->id);
        $this->assertModelData($fakeVendorKyc, $dbVendorKyc->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_vendor_kyc()
    {
        $vendorKyc = factory(VendorKyc::class)->create();

        $resp = $this->vendorKycRepo->delete($vendorKyc->id);

        $this->assertTrue($resp);
        $this->assertNull(VendorKyc::find($vendorKyc->id), 'VendorKyc should not exist in DB');
    }
}
