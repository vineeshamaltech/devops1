<?php namespace Tests\Repositories;

use App\Models\StoreGallery;
use App\Repositories\StoreGalleryRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class StoreGalleryRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var StoreGalleryRepository
     */
    protected $storeGalleryRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->storeGalleryRepo = \App::make(StoreGalleryRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_store_gallery()
    {
        $storeGallery = factory(StoreGallery::class)->make()->toArray();

        $createdStoreGallery = $this->storeGalleryRepo->create($storeGallery);

        $createdStoreGallery = $createdStoreGallery->toArray();
        $this->assertArrayHasKey('id', $createdStoreGallery);
        $this->assertNotNull($createdStoreGallery['id'], 'Created StoreGallery must have id specified');
        $this->assertNotNull(StoreGallery::find($createdStoreGallery['id']), 'StoreGallery with given id must be in DB');
        $this->assertModelData($storeGallery, $createdStoreGallery);
    }

    /**
     * @test read
     */
    public function test_read_store_gallery()
    {
        $storeGallery = factory(StoreGallery::class)->create();

        $dbStoreGallery = $this->storeGalleryRepo->find($storeGallery->id);

        $dbStoreGallery = $dbStoreGallery->toArray();
        $this->assertModelData($storeGallery->toArray(), $dbStoreGallery);
    }

    /**
     * @test update
     */
    public function test_update_store_gallery()
    {
        $storeGallery = factory(StoreGallery::class)->create();
        $fakeStoreGallery = factory(StoreGallery::class)->make()->toArray();

        $updatedStoreGallery = $this->storeGalleryRepo->update($fakeStoreGallery, $storeGallery->id);

        $this->assertModelData($fakeStoreGallery, $updatedStoreGallery->toArray());
        $dbStoreGallery = $this->storeGalleryRepo->find($storeGallery->id);
        $this->assertModelData($fakeStoreGallery, $dbStoreGallery->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_store_gallery()
    {
        $storeGallery = factory(StoreGallery::class)->create();

        $resp = $this->storeGalleryRepo->delete($storeGallery->id);

        $this->assertTrue($resp);
        $this->assertNull(StoreGallery::find($storeGallery->id), 'StoreGallery should not exist in DB');
    }
}
