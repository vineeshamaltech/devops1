<?php namespace Tests\Repositories;

use App\Models\RideBooking;
use App\Repositories\RideBookingRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class RideBookingRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var RideBookingRepository
     */
    protected $rideBookingRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->rideBookingRepo = \App::make(RideBookingRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_ride_booking()
    {
        $rideBooking = factory(RideBooking::class)->make()->toArray();

        $createdRideBooking = $this->rideBookingRepo->create($rideBooking);

        $createdRideBooking = $createdRideBooking->toArray();
        $this->assertArrayHasKey('id', $createdRideBooking);
        $this->assertNotNull($createdRideBooking['id'], 'Created RideBooking must have id specified');
        $this->assertNotNull(RideBooking::find($createdRideBooking['id']), 'RideBooking with given id must be in DB');
        $this->assertModelData($rideBooking, $createdRideBooking);
    }

    /**
     * @test read
     */
    public function test_read_ride_booking()
    {
        $rideBooking = factory(RideBooking::class)->create();

        $dbRideBooking = $this->rideBookingRepo->find($rideBooking->id);

        $dbRideBooking = $dbRideBooking->toArray();
        $this->assertModelData($rideBooking->toArray(), $dbRideBooking);
    }

    /**
     * @test update
     */
    public function test_update_ride_booking()
    {
        $rideBooking = factory(RideBooking::class)->create();
        $fakeRideBooking = factory(RideBooking::class)->make()->toArray();

        $updatedRideBooking = $this->rideBookingRepo->update($fakeRideBooking, $rideBooking->id);

        $this->assertModelData($fakeRideBooking, $updatedRideBooking->toArray());
        $dbRideBooking = $this->rideBookingRepo->find($rideBooking->id);
        $this->assertModelData($fakeRideBooking, $dbRideBooking->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_ride_booking()
    {
        $rideBooking = factory(RideBooking::class)->create();

        $resp = $this->rideBookingRepo->delete($rideBooking->id);

        $this->assertTrue($resp);
        $this->assertNull(RideBooking::find($rideBooking->id), 'RideBooking should not exist in DB');
    }
}
