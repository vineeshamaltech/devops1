<?php namespace Tests\Repositories;

use App\Models\HomeServiceCategory;
use App\Repositories\HomeServiceCategoryRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class HomeServiceCategoryRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var HomeServiceCategoryRepository
     */
    protected $homeServiceCategoryRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->homeServiceCategoryRepo = \App::make(HomeServiceCategoryRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_home_service_category()
    {
        $homeServiceCategory = factory(HomeServiceCategory::class)->make()->toArray();

        $createdHomeServiceCategory = $this->homeServiceCategoryRepo->create($homeServiceCategory);

        $createdHomeServiceCategory = $createdHomeServiceCategory->toArray();
        $this->assertArrayHasKey('id', $createdHomeServiceCategory);
        $this->assertNotNull($createdHomeServiceCategory['id'], 'Created HomeServiceCategory must have id specified');
        $this->assertNotNull(HomeServiceCategory::find($createdHomeServiceCategory['id']), 'HomeServiceCategory with given id must be in DB');
        $this->assertModelData($homeServiceCategory, $createdHomeServiceCategory);
    }

    /**
     * @test read
     */
    public function test_read_home_service_category()
    {
        $homeServiceCategory = factory(HomeServiceCategory::class)->create();

        $dbHomeServiceCategory = $this->homeServiceCategoryRepo->find($homeServiceCategory->id);

        $dbHomeServiceCategory = $dbHomeServiceCategory->toArray();
        $this->assertModelData($homeServiceCategory->toArray(), $dbHomeServiceCategory);
    }

    /**
     * @test update
     */
    public function test_update_home_service_category()
    {
        $homeServiceCategory = factory(HomeServiceCategory::class)->create();
        $fakeHomeServiceCategory = factory(HomeServiceCategory::class)->make()->toArray();

        $updatedHomeServiceCategory = $this->homeServiceCategoryRepo->update($fakeHomeServiceCategory, $homeServiceCategory->id);

        $this->assertModelData($fakeHomeServiceCategory, $updatedHomeServiceCategory->toArray());
        $dbHomeServiceCategory = $this->homeServiceCategoryRepo->find($homeServiceCategory->id);
        $this->assertModelData($fakeHomeServiceCategory, $dbHomeServiceCategory->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_home_service_category()
    {
        $homeServiceCategory = factory(HomeServiceCategory::class)->create();

        $resp = $this->homeServiceCategoryRepo->delete($homeServiceCategory->id);

        $this->assertTrue($resp);
        $this->assertNull(HomeServiceCategory::find($homeServiceCategory->id), 'HomeServiceCategory should not exist in DB');
    }
}
