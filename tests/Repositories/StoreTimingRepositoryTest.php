<?php namespace Tests\Repositories;

use App\Models\StoreTiming;
use App\Repositories\StoreTimingRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class StoreTimingRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var StoreTimingRepository
     */
    protected $storeTimingRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->storeTimingRepo = \App::make(StoreTimingRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_store_timing()
    {
        $storeTiming = factory(StoreTiming::class)->make()->toArray();

        $createdStoreTiming = $this->storeTimingRepo->create($storeTiming);

        $createdStoreTiming = $createdStoreTiming->toArray();
        $this->assertArrayHasKey('id', $createdStoreTiming);
        $this->assertNotNull($createdStoreTiming['id'], 'Created StoreTiming must have id specified');
        $this->assertNotNull(StoreTiming::find($createdStoreTiming['id']), 'StoreTiming with given id must be in DB');
        $this->assertModelData($storeTiming, $createdStoreTiming);
    }

    /**
     * @test read
     */
    public function test_read_store_timing()
    {
        $storeTiming = factory(StoreTiming::class)->create();

        $dbStoreTiming = $this->storeTimingRepo->find($storeTiming->id);

        $dbStoreTiming = $dbStoreTiming->toArray();
        $this->assertModelData($storeTiming->toArray(), $dbStoreTiming);
    }

    /**
     * @test update
     */
    public function test_update_store_timing()
    {
        $storeTiming = factory(StoreTiming::class)->create();
        $fakeStoreTiming = factory(StoreTiming::class)->make()->toArray();

        $updatedStoreTiming = $this->storeTimingRepo->update($fakeStoreTiming, $storeTiming->id);

        $this->assertModelData($fakeStoreTiming, $updatedStoreTiming->toArray());
        $dbStoreTiming = $this->storeTimingRepo->find($storeTiming->id);
        $this->assertModelData($fakeStoreTiming, $dbStoreTiming->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_store_timing()
    {
        $storeTiming = factory(StoreTiming::class)->create();

        $resp = $this->storeTimingRepo->delete($storeTiming->id);

        $this->assertTrue($resp);
        $this->assertNull(StoreTiming::find($storeTiming->id), 'StoreTiming should not exist in DB');
    }
}
