<?php namespace Tests\Repositories;

use App\Models\ProductVariant;
use App\Repositories\ProductVariantRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class ProductVariantRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var ProductVariantRepository
     */
    protected $productVariantRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->productVariantRepo = \App::make(ProductVariantRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_product_variant()
    {
        $productVariant = factory(ProductVariant::class)->make()->toArray();

        $createdProductVariant = $this->productVariantRepo->create($productVariant);

        $createdProductVariant = $createdProductVariant->toArray();
        $this->assertArrayHasKey('id', $createdProductVariant);
        $this->assertNotNull($createdProductVariant['id'], 'Created ProductVariant must have id specified');
        $this->assertNotNull(ProductVariant::find($createdProductVariant['id']), 'ProductVariant with given id must be in DB');
        $this->assertModelData($productVariant, $createdProductVariant);
    }

    /**
     * @test read
     */
    public function test_read_product_variant()
    {
        $productVariant = factory(ProductVariant::class)->create();

        $dbProductVariant = $this->productVariantRepo->find($productVariant->id);

        $dbProductVariant = $dbProductVariant->toArray();
        $this->assertModelData($productVariant->toArray(), $dbProductVariant);
    }

    /**
     * @test update
     */
    public function test_update_product_variant()
    {
        $productVariant = factory(ProductVariant::class)->create();
        $fakeProductVariant = factory(ProductVariant::class)->make()->toArray();

        $updatedProductVariant = $this->productVariantRepo->update($fakeProductVariant, $productVariant->id);

        $this->assertModelData($fakeProductVariant, $updatedProductVariant->toArray());
        $dbProductVariant = $this->productVariantRepo->find($productVariant->id);
        $this->assertModelData($fakeProductVariant, $dbProductVariant->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_product_variant()
    {
        $productVariant = factory(ProductVariant::class)->create();

        $resp = $this->productVariantRepo->delete($productVariant->id);

        $this->assertTrue($resp);
        $this->assertNull(ProductVariant::find($productVariant->id), 'ProductVariant should not exist in DB');
    }
}
