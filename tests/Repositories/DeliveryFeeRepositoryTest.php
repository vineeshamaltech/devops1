<?php namespace Tests\Repositories;

use App\Models\DeliveryFee;
use App\Repositories\DeliveryFeeRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class DeliveryFeeRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var DeliveryFeeRepository
     */
    protected $deliveryFeeRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->deliveryFeeRepo = \App::make(DeliveryFeeRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_delivery_fee()
    {
        $deliveryFee = factory(DeliveryFee::class)->make()->toArray();

        $createdDeliveryFee = $this->deliveryFeeRepo->create($deliveryFee);

        $createdDeliveryFee = $createdDeliveryFee->toArray();
        $this->assertArrayHasKey('id', $createdDeliveryFee);
        $this->assertNotNull($createdDeliveryFee['id'], 'Created DeliveryFee must have id specified');
        $this->assertNotNull(DeliveryFee::find($createdDeliveryFee['id']), 'DeliveryFee with given id must be in DB');
        $this->assertModelData($deliveryFee, $createdDeliveryFee);
    }

    /**
     * @test read
     */
    public function test_read_delivery_fee()
    {
        $deliveryFee = factory(DeliveryFee::class)->create();

        $dbDeliveryFee = $this->deliveryFeeRepo->find($deliveryFee->id);

        $dbDeliveryFee = $dbDeliveryFee->toArray();
        $this->assertModelData($deliveryFee->toArray(), $dbDeliveryFee);
    }

    /**
     * @test update
     */
    public function test_update_delivery_fee()
    {
        $deliveryFee = factory(DeliveryFee::class)->create();
        $fakeDeliveryFee = factory(DeliveryFee::class)->make()->toArray();

        $updatedDeliveryFee = $this->deliveryFeeRepo->update($fakeDeliveryFee, $deliveryFee->id);

        $this->assertModelData($fakeDeliveryFee, $updatedDeliveryFee->toArray());
        $dbDeliveryFee = $this->deliveryFeeRepo->find($deliveryFee->id);
        $this->assertModelData($fakeDeliveryFee, $dbDeliveryFee->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_delivery_fee()
    {
        $deliveryFee = factory(DeliveryFee::class)->create();

        $resp = $this->deliveryFeeRepo->delete($deliveryFee->id);

        $this->assertTrue($resp);
        $this->assertNull(DeliveryFee::find($deliveryFee->id), 'DeliveryFee should not exist in DB');
    }
}
