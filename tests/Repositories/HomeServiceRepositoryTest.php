<?php namespace Tests\Repositories;

use App\Models\HomeService;
use App\Repositories\HomeServiceRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class HomeServiceRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var HomeServiceRepository
     */
    protected $homeServiceRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->homeServiceRepo = \App::make(HomeServiceRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_home_service()
    {
        $homeService = factory(HomeService::class)->make()->toArray();

        $createdHomeService = $this->homeServiceRepo->create($homeService);

        $createdHomeService = $createdHomeService->toArray();
        $this->assertArrayHasKey('id', $createdHomeService);
        $this->assertNotNull($createdHomeService['id'], 'Created HomeService must have id specified');
        $this->assertNotNull(HomeService::find($createdHomeService['id']), 'HomeService with given id must be in DB');
        $this->assertModelData($homeService, $createdHomeService);
    }

    /**
     * @test read
     */
    public function test_read_home_service()
    {
        $homeService = factory(HomeService::class)->create();

        $dbHomeService = $this->homeServiceRepo->find($homeService->id);

        $dbHomeService = $dbHomeService->toArray();
        $this->assertModelData($homeService->toArray(), $dbHomeService);
    }

    /**
     * @test update
     */
    public function test_update_home_service()
    {
        $homeService = factory(HomeService::class)->create();
        $fakeHomeService = factory(HomeService::class)->make()->toArray();

        $updatedHomeService = $this->homeServiceRepo->update($fakeHomeService, $homeService->id);

        $this->assertModelData($fakeHomeService, $updatedHomeService->toArray());
        $dbHomeService = $this->homeServiceRepo->find($homeService->id);
        $this->assertModelData($fakeHomeService, $dbHomeService->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_home_service()
    {
        $homeService = factory(HomeService::class)->create();

        $resp = $this->homeServiceRepo->delete($homeService->id);

        $this->assertTrue($resp);
        $this->assertNull(HomeService::find($homeService->id), 'HomeService should not exist in DB');
    }
}
