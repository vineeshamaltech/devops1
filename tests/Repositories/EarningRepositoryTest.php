<?php namespace Tests\Repositories;

use App\Models\Earning;
use App\Repositories\EarningRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class EarningRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var EarningRepository
     */
    protected $earningRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->earningRepo = \App::make(EarningRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_earning()
    {
        $earning = factory(Earning::class)->make()->toArray();

        $createdEarning = $this->earningRepo->create($earning);

        $createdEarning = $createdEarning->toArray();
        $this->assertArrayHasKey('id', $createdEarning);
        $this->assertNotNull($createdEarning['id'], 'Created Earning must have id specified');
        $this->assertNotNull(Earning::find($createdEarning['id']), 'Earning with given id must be in DB');
        $this->assertModelData($earning, $createdEarning);
    }

    /**
     * @test read
     */
    public function test_read_earning()
    {
        $earning = factory(Earning::class)->create();

        $dbEarning = $this->earningRepo->find($earning->id);

        $dbEarning = $dbEarning->toArray();
        $this->assertModelData($earning->toArray(), $dbEarning);
    }

    /**
     * @test update
     */
    public function test_update_earning()
    {
        $earning = factory(Earning::class)->create();
        $fakeEarning = factory(Earning::class)->make()->toArray();

        $updatedEarning = $this->earningRepo->update($fakeEarning, $earning->id);

        $this->assertModelData($fakeEarning, $updatedEarning->toArray());
        $dbEarning = $this->earningRepo->find($earning->id);
        $this->assertModelData($fakeEarning, $dbEarning->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_earning()
    {
        $earning = factory(Earning::class)->create();

        $resp = $this->earningRepo->delete($earning->id);

        $this->assertTrue($resp);
        $this->assertNull(Earning::find($earning->id), 'Earning should not exist in DB');
    }
}
