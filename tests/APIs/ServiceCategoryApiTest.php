<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\ServiceCategory;

class ServiceCategoryApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_service_category()
    {
        $serviceCategory = factory(ServiceCategory::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/admin/service_categories', $serviceCategory
        );

        $this->assertApiResponse($serviceCategory);
    }

    /**
     * @test
     */
    public function test_read_service_category()
    {
        $serviceCategory = factory(ServiceCategory::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/admin/service_categories/'.$serviceCategory->id
        );

        $this->assertApiResponse($serviceCategory->toArray());
    }

    /**
     * @test
     */
    public function test_update_service_category()
    {
        $serviceCategory = factory(ServiceCategory::class)->create();
        $editedServiceCategory = factory(ServiceCategory::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/admin/service_categories/'.$serviceCategory->id,
            $editedServiceCategory
        );

        $this->assertApiResponse($editedServiceCategory);
    }

    /**
     * @test
     */
    public function test_delete_service_category()
    {
        $serviceCategory = factory(ServiceCategory::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/admin/service_categories/'.$serviceCategory->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/admin/service_categories/'.$serviceCategory->id
        );

        $this->response->assertStatus(404);
    }
}
