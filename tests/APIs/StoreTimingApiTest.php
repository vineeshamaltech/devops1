<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\StoreTiming;

class StoreTimingApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_store_timing()
    {
        $storeTiming = factory(StoreTiming::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/admin/store_timings', $storeTiming
        );

        $this->assertApiResponse($storeTiming);
    }

    /**
     * @test
     */
    public function test_read_store_timing()
    {
        $storeTiming = factory(StoreTiming::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/admin/store_timings/'.$storeTiming->id
        );

        $this->assertApiResponse($storeTiming->toArray());
    }

    /**
     * @test
     */
    public function test_update_store_timing()
    {
        $storeTiming = factory(StoreTiming::class)->create();
        $editedStoreTiming = factory(StoreTiming::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/admin/store_timings/'.$storeTiming->id,
            $editedStoreTiming
        );

        $this->assertApiResponse($editedStoreTiming);
    }

    /**
     * @test
     */
    public function test_delete_store_timing()
    {
        $storeTiming = factory(StoreTiming::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/admin/store_timings/'.$storeTiming->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/admin/store_timings/'.$storeTiming->id
        );

        $this->response->assertStatus(404);
    }
}
