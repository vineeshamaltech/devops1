<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\RideBooking;

class RideBookingApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_ride_booking()
    {
        $rideBooking = factory(RideBooking::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/admin/ride_bookings', $rideBooking
        );

        $this->assertApiResponse($rideBooking);
    }

    /**
     * @test
     */
    public function test_read_ride_booking()
    {
        $rideBooking = factory(RideBooking::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/admin/ride_bookings/'.$rideBooking->id
        );

        $this->assertApiResponse($rideBooking->toArray());
    }

    /**
     * @test
     */
    public function test_update_ride_booking()
    {
        $rideBooking = factory(RideBooking::class)->create();
        $editedRideBooking = factory(RideBooking::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/admin/ride_bookings/'.$rideBooking->id,
            $editedRideBooking
        );

        $this->assertApiResponse($editedRideBooking);
    }

    /**
     * @test
     */
    public function test_delete_ride_booking()
    {
        $rideBooking = factory(RideBooking::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/admin/ride_bookings/'.$rideBooking->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/admin/ride_bookings/'.$rideBooking->id
        );

        $this->response->assertStatus(404);
    }
}
