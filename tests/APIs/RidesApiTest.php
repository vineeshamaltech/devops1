<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Ride;

class RidesApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_rides()
    {
        $rides = factory(Ride::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/admin/rides', $rides
        );

        $this->assertApiResponse($rides);
    }

    /**
     * @test
     */
    public function test_read_rides()
    {
        $rides = factory(Ride::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/admin/rides/'.$rides->id
        );

        $this->assertApiResponse($rides->toArray());
    }

    /**
     * @test
     */
    public function test_update_rides()
    {
        $rides = factory(Ride::class)->create();
        $editedRides = factory(Ride::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/admin/rides/'.$rides->id,
            $editedRides
        );

        $this->assertApiResponse($editedRides);
    }

    /**
     * @test
     */
    public function test_delete_rides()
    {
        $rides = factory(Ride::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/admin/rides/'.$rides->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/admin/rides/'.$rides->id
        );

        $this->response->assertStatus(404);
    }
}
