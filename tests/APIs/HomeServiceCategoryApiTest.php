<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\HomeServiceCategory;

class HomeServiceCategoryApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_home_service_category()
    {
        $homeServiceCategory = factory(HomeServiceCategory::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/admin/home_service_categories', $homeServiceCategory
        );

        $this->assertApiResponse($homeServiceCategory);
    }

    /**
     * @test
     */
    public function test_read_home_service_category()
    {
        $homeServiceCategory = factory(HomeServiceCategory::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/admin/home_service_categories/'.$homeServiceCategory->id
        );

        $this->assertApiResponse($homeServiceCategory->toArray());
    }

    /**
     * @test
     */
    public function test_update_home_service_category()
    {
        $homeServiceCategory = factory(HomeServiceCategory::class)->create();
        $editedHomeServiceCategory = factory(HomeServiceCategory::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/admin/home_service_categories/'.$homeServiceCategory->id,
            $editedHomeServiceCategory
        );

        $this->assertApiResponse($editedHomeServiceCategory);
    }

    /**
     * @test
     */
    public function test_delete_home_service_category()
    {
        $homeServiceCategory = factory(HomeServiceCategory::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/admin/home_service_categories/'.$homeServiceCategory->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/admin/home_service_categories/'.$homeServiceCategory->id
        );

        $this->response->assertStatus(404);
    }
}
