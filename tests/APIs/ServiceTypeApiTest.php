<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\ServiceType;

class ServiceTypeApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_service_type()
    {
        $serviceType = factory(ServiceType::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/admin/service_types', $serviceType
        );

        $this->assertApiResponse($serviceType);
    }

    /**
     * @test
     */
    public function test_read_service_type()
    {
        $serviceType = factory(ServiceType::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/admin/service_types/'.$serviceType->id
        );

        $this->assertApiResponse($serviceType->toArray());
    }

    /**
     * @test
     */
    public function test_update_service_type()
    {
        $serviceType = factory(ServiceType::class)->create();
        $editedServiceType = factory(ServiceType::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/admin/service_types/'.$serviceType->id,
            $editedServiceType
        );

        $this->assertApiResponse($editedServiceType);
    }

    /**
     * @test
     */
    public function test_delete_service_type()
    {
        $serviceType = factory(ServiceType::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/admin/service_types/'.$serviceType->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/admin/service_types/'.$serviceType->id
        );

        $this->response->assertStatus(404);
    }
}
