<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\StopOver;

class StopOverApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_stop_over()
    {
        $stopOver = factory(StopOver::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/admin/stop_overs', $stopOver
        );

        $this->assertApiResponse($stopOver);
    }

    /**
     * @test
     */
    public function test_read_stop_over()
    {
        $stopOver = factory(StopOver::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/admin/stop_overs/'.$stopOver->id
        );

        $this->assertApiResponse($stopOver->toArray());
    }

    /**
     * @test
     */
    public function test_update_stop_over()
    {
        $stopOver = factory(StopOver::class)->create();
        $editedStopOver = factory(StopOver::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/admin/stop_overs/'.$stopOver->id,
            $editedStopOver
        );

        $this->assertApiResponse($editedStopOver);
    }

    /**
     * @test
     */
    public function test_delete_stop_over()
    {
        $stopOver = factory(StopOver::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/admin/stop_overs/'.$stopOver->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/admin/stop_overs/'.$stopOver->id
        );

        $this->response->assertStatus(404);
    }
}
