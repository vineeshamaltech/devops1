<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\StoreReviews;

class StoreReviewsApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_store_reviews()
    {
        $storeReviews = factory(StoreReviews::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/admin/store_reviews', $storeReviews
        );

        $this->assertApiResponse($storeReviews);
    }

    /**
     * @test
     */
    public function test_read_store_reviews()
    {
        $storeReviews = factory(StoreReviews::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/admin/store_reviews/'.$storeReviews->id
        );

        $this->assertApiResponse($storeReviews->toArray());
    }

    /**
     * @test
     */
    public function test_update_store_reviews()
    {
        $storeReviews = factory(StoreReviews::class)->create();
        $editedStoreReviews = factory(StoreReviews::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/admin/store_reviews/'.$storeReviews->id,
            $editedStoreReviews
        );

        $this->assertApiResponse($editedStoreReviews);
    }

    /**
     * @test
     */
    public function test_delete_store_reviews()
    {
        $storeReviews = factory(StoreReviews::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/admin/store_reviews/'.$storeReviews->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/admin/store_reviews/'.$storeReviews->id
        );

        $this->response->assertStatus(404);
    }
}
