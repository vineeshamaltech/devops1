<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\StoreGallery;

class StoreGalleryApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_store_gallery()
    {
        $storeGallery = factory(StoreGallery::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/admin/store_galleries', $storeGallery
        );

        $this->assertApiResponse($storeGallery);
    }

    /**
     * @test
     */
    public function test_read_store_gallery()
    {
        $storeGallery = factory(StoreGallery::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/admin/store_galleries/'.$storeGallery->id
        );

        $this->assertApiResponse($storeGallery->toArray());
    }

    /**
     * @test
     */
    public function test_update_store_gallery()
    {
        $storeGallery = factory(StoreGallery::class)->create();
        $editedStoreGallery = factory(StoreGallery::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/admin/store_galleries/'.$storeGallery->id,
            $editedStoreGallery
        );

        $this->assertApiResponse($editedStoreGallery);
    }

    /**
     * @test
     */
    public function test_delete_store_gallery()
    {
        $storeGallery = factory(StoreGallery::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/admin/store_galleries/'.$storeGallery->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/admin/store_galleries/'.$storeGallery->id
        );

        $this->response->assertStatus(404);
    }
}
