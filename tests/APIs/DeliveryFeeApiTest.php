<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\DeliveryFee;

class DeliveryFeeApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_delivery_fee()
    {
        $deliveryFee = factory(DeliveryFee::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/admin/delivery_fees', $deliveryFee
        );

        $this->assertApiResponse($deliveryFee);
    }

    /**
     * @test
     */
    public function test_read_delivery_fee()
    {
        $deliveryFee = factory(DeliveryFee::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/admin/delivery_fees/'.$deliveryFee->id
        );

        $this->assertApiResponse($deliveryFee->toArray());
    }

    /**
     * @test
     */
    public function test_update_delivery_fee()
    {
        $deliveryFee = factory(DeliveryFee::class)->create();
        $editedDeliveryFee = factory(DeliveryFee::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/admin/delivery_fees/'.$deliveryFee->id,
            $editedDeliveryFee
        );

        $this->assertApiResponse($editedDeliveryFee);
    }

    /**
     * @test
     */
    public function test_delete_delivery_fee()
    {
        $deliveryFee = factory(DeliveryFee::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/admin/delivery_fees/'.$deliveryFee->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/admin/delivery_fees/'.$deliveryFee->id
        );

        $this->response->assertStatus(404);
    }
}
