<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\VendorKyc;

class VendorKycApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_vendor_kyc()
    {
        $vendorKyc = factory(VendorKyc::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/admin/vendor_kycs', $vendorKyc
        );

        $this->assertApiResponse($vendorKyc);
    }

    /**
     * @test
     */
    public function test_read_vendor_kyc()
    {
        $vendorKyc = factory(VendorKyc::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/admin/vendor_kycs/'.$vendorKyc->id
        );

        $this->assertApiResponse($vendorKyc->toArray());
    }

    /**
     * @test
     */
    public function test_update_vendor_kyc()
    {
        $vendorKyc = factory(VendorKyc::class)->create();
        $editedVendorKyc = factory(VendorKyc::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/admin/vendor_kycs/'.$vendorKyc->id,
            $editedVendorKyc
        );

        $this->assertApiResponse($editedVendorKyc);
    }

    /**
     * @test
     */
    public function test_delete_vendor_kyc()
    {
        $vendorKyc = factory(VendorKyc::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/admin/vendor_kycs/'.$vendorKyc->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/admin/vendor_kycs/'.$vendorKyc->id
        );

        $this->response->assertStatus(404);
    }
}
