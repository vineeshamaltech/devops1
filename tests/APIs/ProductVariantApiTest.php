<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\ProductVariant;

class ProductVariantApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_product_variant()
    {
        $productVariant = factory(ProductVariant::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/admin/product_variants', $productVariant
        );

        $this->assertApiResponse($productVariant);
    }

    /**
     * @test
     */
    public function test_read_product_variant()
    {
        $productVariant = factory(ProductVariant::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/admin/product_variants/'.$productVariant->id
        );

        $this->assertApiResponse($productVariant->toArray());
    }

    /**
     * @test
     */
    public function test_update_product_variant()
    {
        $productVariant = factory(ProductVariant::class)->create();
        $editedProductVariant = factory(ProductVariant::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/admin/product_variants/'.$productVariant->id,
            $editedProductVariant
        );

        $this->assertApiResponse($editedProductVariant);
    }

    /**
     * @test
     */
    public function test_delete_product_variant()
    {
        $productVariant = factory(ProductVariant::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/admin/product_variants/'.$productVariant->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/admin/product_variants/'.$productVariant->id
        );

        $this->response->assertStatus(404);
    }
}
