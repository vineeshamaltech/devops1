<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\HomeService;

class HomeServiceApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_home_service()
    {
        $homeService = factory(HomeService::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/admin/home_services', $homeService
        );

        $this->assertApiResponse($homeService);
    }

    /**
     * @test
     */
    public function test_read_home_service()
    {
        $homeService = factory(HomeService::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/admin/home_services/'.$homeService->id
        );

        $this->assertApiResponse($homeService->toArray());
    }

    /**
     * @test
     */
    public function test_update_home_service()
    {
        $homeService = factory(HomeService::class)->create();
        $editedHomeService = factory(HomeService::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/admin/home_services/'.$homeService->id,
            $editedHomeService
        );

        $this->assertApiResponse($editedHomeService);
    }

    /**
     * @test
     */
    public function test_delete_home_service()
    {
        $homeService = factory(HomeService::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/admin/home_services/'.$homeService->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/admin/home_services/'.$homeService->id
        );

        $this->response->assertStatus(404);
    }
}
