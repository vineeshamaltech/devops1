<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Earning;

class EarningApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_earning()
    {
        $earning = factory(Earning::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/admin/earnings', $earning
        );

        $this->assertApiResponse($earning);
    }

    /**
     * @test
     */
    public function test_read_earning()
    {
        $earning = factory(Earning::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/admin/earnings/'.$earning->id
        );

        $this->assertApiResponse($earning->toArray());
    }

    /**
     * @test
     */
    public function test_update_earning()
    {
        $earning = factory(Earning::class)->create();
        $editedEarning = factory(Earning::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/admin/earnings/'.$earning->id,
            $editedEarning
        );

        $this->assertApiResponse($editedEarning);
    }

    /**
     * @test
     */
    public function test_delete_earning()
    {
        $earning = factory(Earning::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/admin/earnings/'.$earning->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/admin/earnings/'.$earning->id
        );

        $this->response->assertStatus(404);
    }
}
