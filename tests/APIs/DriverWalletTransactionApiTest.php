<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\DriverWalletTransaction;

class DriverWalletTransactionApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_driver_wallet_transaction()
    {
        $driverWalletTransaction = factory(DriverWalletTransaction::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/admin/driver_wallet_transactions', $driverWalletTransaction
        );

        $this->assertApiResponse($driverWalletTransaction);
    }

    /**
     * @test
     */
    public function test_read_driver_wallet_transaction()
    {
        $driverWalletTransaction = factory(DriverWalletTransaction::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/admin/driver_wallet_transactions/'.$driverWalletTransaction->id
        );

        $this->assertApiResponse($driverWalletTransaction->toArray());
    }

    /**
     * @test
     */
    public function test_update_driver_wallet_transaction()
    {
        $driverWalletTransaction = factory(DriverWalletTransaction::class)->create();
        $editedDriverWalletTransaction = factory(DriverWalletTransaction::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/admin/driver_wallet_transactions/'.$driverWalletTransaction->id,
            $editedDriverWalletTransaction
        );

        $this->assertApiResponse($editedDriverWalletTransaction);
    }

    /**
     * @test
     */
    public function test_delete_driver_wallet_transaction()
    {
        $driverWalletTransaction = factory(DriverWalletTransaction::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/admin/driver_wallet_transactions/'.$driverWalletTransaction->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/admin/driver_wallet_transactions/'.$driverWalletTransaction->id
        );

        $this->response->assertStatus(404);
    }
}
