<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\StoreCategory;

class StoreCategoryApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_store_category()
    {
        $storeCategory = factory(StoreCategory::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/admin/store_categories', $storeCategory
        );

        $this->assertApiResponse($storeCategory);
    }

    /**
     * @test
     */
    public function test_read_store_category()
    {
        $storeCategory = factory(StoreCategory::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/admin/store_categories/'.$storeCategory->id
        );

        $this->assertApiResponse($storeCategory->toArray());
    }

    /**
     * @test
     */
    public function test_update_store_category()
    {
        $storeCategory = factory(StoreCategory::class)->create();
        $editedStoreCategory = factory(StoreCategory::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/admin/store_categories/'.$storeCategory->id,
            $editedStoreCategory
        );

        $this->assertApiResponse($editedStoreCategory);
    }

    /**
     * @test
     */
    public function test_delete_store_category()
    {
        $storeCategory = factory(StoreCategory::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/admin/store_categories/'.$storeCategory->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/admin/store_categories/'.$storeCategory->id
        );

        $this->response->assertStatus(404);
    }
}
