<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\UserWalletTransaction;

class UserWalletTransactionApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_user_wallet_transaction()
    {
        $userWalletTransaction = factory(UserWalletTransaction::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/admin/user_wallet_transactions', $userWalletTransaction
        );

        $this->assertApiResponse($userWalletTransaction);
    }

    /**
     * @test
     */
    public function test_read_user_wallet_transaction()
    {
        $userWalletTransaction = factory(UserWalletTransaction::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/admin/user_wallet_transactions/'.$userWalletTransaction->id
        );

        $this->assertApiResponse($userWalletTransaction->toArray());
    }

    /**
     * @test
     */
    public function test_update_user_wallet_transaction()
    {
        $userWalletTransaction = factory(UserWalletTransaction::class)->create();
        $editedUserWalletTransaction = factory(UserWalletTransaction::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/admin/user_wallet_transactions/'.$userWalletTransaction->id,
            $editedUserWalletTransaction
        );

        $this->assertApiResponse($editedUserWalletTransaction);
    }

    /**
     * @test
     */
    public function test_delete_user_wallet_transaction()
    {
        $userWalletTransaction = factory(UserWalletTransaction::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/admin/user_wallet_transactions/'.$userWalletTransaction->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/admin/user_wallet_transactions/'.$userWalletTransaction->id
        );

        $this->response->assertStatus(404);
    }
}
