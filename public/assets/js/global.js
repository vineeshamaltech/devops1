$(document).ready(function() {
    var select2 = $(".select2").select2({
        dropdownAutoWidth : true,
        theme: "classic"
    });
    $("img").on("error", function () {
        $(this).attr("src", "https://fakeimg.pl/350x200/?text=No%20Image");
    });
    $('[data-toggle="tooltip"]').tooltip();
});
