<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->constrained();
            $table->foreignId('store_id')->constrained();
            $table->text('coupon_data')->nullable();
            $table->string('order_type');
            $table->double('total_price');
            $table->double('discount_value');
            $table->double('delivery_fee');
            $table->double('tax');
            $table->double('grand_total');
            $table->string('pickup_address');
            $table->string('delivery_address');
            $table->double('distance_travelled')->nullable();
            $table->double('preparation_time')->default(0)->nullable();
            $table->double('travel_time')->default(0)->nullable();
            $table->integer('driver_id')->nullable();
            $table->string('order_status')->default('PLACED');
            $table->string('payment_status')->default('PENDING');
            $table->string('payment_method')->default('COD');
            $table->foreignId('payment_id')->constrained();
            $table->json('json_data')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('orders');
    }
}
