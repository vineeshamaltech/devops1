<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateServiceCategoriesTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('service_categories', function (Blueprint $table) {
            $table->id();
            $table->string('icon');
            $table->string('bg_image');
            $table->string('home_view');
            $table->string('name');
            $table->string('description')->nullable();
            $table->foreignId('service_type_id')->constrained();
            $table->double('discount')->default(0)->nullable();
            $table->double('commission')->default(0)->nullable();
            $table->string('min_price')->default(0)->nullable();
            $table->string('driver_radius')->default(10)->nullable();
            $table->double('base_distance')->nullable();
            $table->double('base_price')->nullable();
            $table->double('base_km')->nullable();
            $table->double('price_km')->nullable();
            $table->double('max_order_distance');
            $table->boolean('active')->default(true)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('service_categories');
    }
}
