<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVendorsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
//        Schema::create('vendors', function (Blueprint $table) {
//            $table->id();
//            $table->string('name');
//            $table->string('mobile');
//            $table->string('email');
//            $table->string('password');
//            $table->string('fcm_token')->nullable();
//            $table->string('bank_name')->nullable();
//            $table->string('account_number')->nullable();
//            $table->string('ifsc_code')->nullable();
//            $table->string('branch_name')->nullable();
//            $table->string('profile_photo')->nullable();
//            $table->boolean('active')->default(true)->nullable();
//            $table->boolean('verified')->default(false)->nullable();
//            $table->timestamps();
//        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('vendors');
    }
}
