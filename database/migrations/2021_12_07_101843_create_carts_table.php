<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCartsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('carts', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->constrained();
            $table->foreignId('coupon_id')->nullable()->constrained();
            $table->foreignId('service_category_id')->nullable()->constrained();
            $table->foreignId('store_id')->constrained();
            $table->double('total_price')->default(0);
            $table->double('discount_value')->default(0);
            $table->double('delivery_fee')->default(0);
            $table->double('tax')->default(0);
            $table->double('tip')->default(0);
            $table->double('grand_total')->default(0);
            $table->string('pickup_address');
            $table->string('delivery_address')->nullable();
            $table->double('distance',10,2)->default(null);
            $table->double('travel_time',10,2)->default(null);
            $table->foreignId('address_id')->nullable()->constrained();
            $table->foreignId('payment_id')->nullable()->constrained();
            $table->json('json_data')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('carts');
    }
}
