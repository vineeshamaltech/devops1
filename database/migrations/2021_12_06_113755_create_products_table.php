<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->foreignId('store_id')->constrained()->onDelete('cascade');
            $table->enum('product_type', ['SIMPLE', 'VARIABLE'])->default('SIMPLE');
            $table->string('name');
            $table->string('description')->nullable();
            $table->string('image');
            $table->string('unit');
            $table->double('price',10,2);
            $table->double('discount_price',10,2);
            $table->foreignId('category_id')->nullable()->constrained();
            $table->integer('sku')->default(0);
            $table->string('product_tags');
            $table->time('opening_time')->nullable();
            $table->time('closing_time')->nullable();
            $table->boolean('active')->default(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('products');
    }
}
