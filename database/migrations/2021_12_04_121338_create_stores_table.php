<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStoresTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stores', function (Blueprint $table) {
            $table->id();
            $table->foreignId('admin_id')->constrained();
            $table->foreignId('service_category_id')->constrained();
            $table->string('store_categories');
            $table->string('name');
            $table->string('description');
            $table->string('image');
            $table->string('lat');
            $table->string('long');
            $table->string('address');
            $table->double('delivery_range');
            $table->double('base_distance')->default(null)->nullable();
            $table->double('base_price')->default(null)->nullable();
            $table->double('price_per_km')->default(null)->nullable();
            $table->string('mobile');
            $table->string('email');
            $table->integer('order_count')->default(0)->nullable();
            $table->string('gst_doc')->nullable();
            $table->string('pan_doc')->nullable();
            $table->string('food_license')->nullable();
            $table->string('shop_photo')->nullable();
            $table->double('commission',5,2)->default(0.00);
            $table->double('tax',5,2)->default(0.00);
            $table->boolean('is_open')->default(false);
            $table->boolean('active')->default(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('stores');
    }
}
