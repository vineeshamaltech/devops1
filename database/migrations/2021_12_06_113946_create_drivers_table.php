<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDriversTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('drivers', function (Blueprint $table) {
            $table->id();
            $table->string('name')->nullable();
            $table->string('country_code')->default("+91")->nullable();
            $table->string('mobile');
            $table->string('email')->nullable();
            $table->string('password')->nullable();
            $table->double('cash_in_hand',12,2)->default(0);
            $table->enum('vehicle_type',['BIKE','CAR'])->default('BIKE');
            $table->string('vehicle_reg_no')->nullable();
            $table->string('vehicle_color')->nullable();
            $table->string('vehicle_img')->nullable();
            $table->string('dl_photo')->nullable();
            $table->string('aadhar_photo')->nullable();
            $table->foreignId('service_type_id')->nullable()->constrained();
            $table->string('bank_name')->nullable();
            $table->string('account_no')->nullable();
            $table->string('ifsc_code')->nullable();
            $table->string('branch_name')->nullable();
            $table->boolean('active')->default(false);
            $table->boolean('is_verified')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('drivers');
    }
}
