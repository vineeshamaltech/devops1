<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePaymentRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment_requests', function (Blueprint $table) {
            $table->id();
            $table->string('invoice_id');
            $table->bigInteger('user_id');
            $table->string('user_type');
            $table->bigInteger('order_id')->unsigned()->nullable();
            $table->string('remarks')->nullable();
            $table->integer('amount');
            $table->string('gateway')->default('RazorPay');
            $table->string('gateway_order_id');
            $table->json('gateway_response')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payment_requests');
    }
}
