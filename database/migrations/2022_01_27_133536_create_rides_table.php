<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRidesTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rides', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->constrained();
            $table->string('from_address');
            $table->string('to_address');
            $table->double('from_lat');
            $table->double('from_long');
            $table->double('to_lat');
            $table->double('to_long');
            $table->timestamp('travel_datetime');
            $table->string('route');
            $table->integer('max_copassengers')->default(1);
            $table->boolean('can_book_instantly')->default(false);
            $table->double('price');
            $table->enum('status', ['DRAFT','PUBLISHED','STARTED','FINISHED','CANCELLED'])->default('DRAFT');
            $table->boolean('active')->default(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('rides');
    }
}
