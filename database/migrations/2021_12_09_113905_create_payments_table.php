<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePaymentsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->id();
            $table->foreignId('payment_request_id')->constrained();
            $table->string('ref_no');
            $table->bigInteger('user_id')->unsigned();
            $table->string('user_type');
            $table->bigInteger('order_id')->unsigned()->nullable();
            $table->string('remarks')->nullable();
            $table->string('payment_method');
            $table->integer('amount');
            $table->json('gateway_response');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('payments');
    }
}
