<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\User::create([
            'name' => 'Admin',
            'mobile' => '0123456789',
            'email' => 'admin@admin.com',
            'password' => '12345678',
            'user_type' => 'ADMIN',
            'active' => '1'
        ]);
        $this->call([
            DriverSeeder::class,
        ]);
    }
}
