<?php

namespace Database\Factories;

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\Driver;

class DriverFactory extends Factory
{

    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Driver::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->name(),
            'mobile' => $this->faker->e164PhoneNumber(),
            'email' => $this->faker->email(),
            'password' => \Illuminate\Support\Facades\Hash::make('123456'),
            'cash_in_hand' => $this->faker->randomFloat(2, 0, 500),
            'vehicle_type' => $this->faker->numberBetween(2, 4),
            'vehicle_reg_no' => $this->faker->regexify('[A-Z]{2}[0-9]{2}[A-Z]{2}[0-9]{4}'),
            'vehicle_color' => $this->faker->randomElement(['red', 'blue', 'green', 'black', 'white']),
            'vehicle_img' => $this->faker->randomElement([
                'https://wedun.ssdemo.xyz/storage/vehicle_types/Asset%201.png',
                'https://wedun.ssdemo.xyz/storage/vehicle_types/Asset%202.png',
                'https://wedun.ssdemo.xyz/storage/vehicle_types/Asset%203.png',
                'https://wedun.ssdemo.xyz/storage/vehicle_types/Asset%204.png',
            ]),
            'latitude' => $this->faker->latitude(28.369218,28.550902),
            'longitude' => $this->faker->longitude(76.940959,77.130444),
            'dl_photo' => $this->faker->imageUrl(400, 400, 'animals', true),
            'aadhar_photo' => $this->faker->imageUrl(400, 400, 'birds', true),
            'service_type_id' => 4,
            'bank_name' => $this->faker->randomElement(['HDFC', 'ICICI', 'SBI', 'AXIS', 'KOTAK']),
            'account_no' => $this->faker->creditCardNumber(),
            'ifsc_code' => $this->faker->randomNumber(6),
            'branch_name' => $this->faker->word,
            'active' => true,
            'is_verified' => true,
            'created_at' => $this->faker->date('Y-m-d H:i:s'),
            'updated_at' => $this->faker->date('Y-m-d H:i:s')
        ];
    }
}
