<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\StopOver;
use Faker\Generator as Faker;

$factory->define(StopOver::class, function (Faker $faker) {

    return [
        'id' => $faker->word,
        'user_id' => $faker->word,
        'ride_id' => $faker->word,
        'name' => $faker->word,
        'lat' => $faker->word,
        'long' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
