<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Ride;
use Faker\Generator as Faker;

$factory->define(Ride::class, function (Faker $faker) {

    return [
        'id' => $faker->word,
        'user_id' => $faker->word,
        'from_address' => $faker->word,
        'to_address' => $faker->word,
        'from_lat' => $faker->word,
        'from_long' => $faker->word,
        'to_lat' => $faker->word,
        'to_long' => $faker->word,
        'travel_datetime' => $faker->date('Y-m-d H:i:s'),
        'route' => $faker->word,
        'max_copassengers' => $faker->word,
        'can_book_instantly' => $faker->word,
        'price' => $faker->word,
        'status' => $faker->randomElement(]),
        'active' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
