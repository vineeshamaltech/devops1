<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\StoreReviews;
use Faker\Generator as Faker;

$factory->define(StoreReviews::class, function (Faker $faker) {

    return [
        'store_id' => $faker->randomDigitNotNull,
        'title' => $faker->word,
        'description' => $faker->word,
        'rating' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
