<?php

namespace Database\Factories;

use App\Models\Store;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;
class StoreFactory extends \Illuminate\Database\Eloquent\Factories\Factory{

    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Store::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition(){
        return [
            'admin_id' => $this->faker->numberBetween(1,20),
            'service_category_id' => $this->faker->randomDigitNotNull,
            'store_categories' => $this->faker->word,
            'name' => $this->faker->word,
            'description' => $this->faker->word,
            'image' => $this->faker->imageUrl,
            'latitude' => $this->faker->latitude,
            'longitude' => $this->faker->longitude,
            'address' => $this->faker->word,
            'delivery_range' => $this->faker->numberBetween(1,100000),
            'base_distance' => null,
            'base_price' => null,
            'price_per_km' => null,
            'mobile' => $this->faker->phoneNumber,
            'email' => $this->faker->email,
            'order_count' => $this->faker->randomDigitNotNull,
            'is_open' =>   $this->faker->boolean,
            'active' => $this->faker->boolean,
            'created_at' => $this->faker->date('Y-m-d H:i:s'),
            'updated_at' => $this->faker->date('Y-m-d H:i:s')
        ];
    }
}

