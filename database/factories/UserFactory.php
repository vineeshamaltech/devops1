<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\User;
use Faker\Generator as Faker;

$factory->define(User::class, function (Faker $faker) {

    return [
        'name' => $faker->word,
        'mobile' => $faker->word,
        'email' => $faker->word,
        'password' => $faker->word,
        'profile_img' => $faker->word,
        'wallet_amount' => $faker->word,
        'fcm_token' => $faker->word,
        'lat' => $faker->word,
        'long' => $faker->word,
        'user_type' => $faker->word,
        'active' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
