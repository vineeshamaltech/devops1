<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Vendor;
use Faker\Generator as Faker;

$factory->define(Vendor::class, function (Faker $faker) {

    return [
        'name' => $faker->word,
        'mobile' => $faker->word,
        'email' => $faker->word,
        'password' => $faker->word,
        'fcm_token' => $faker->word,
        'active' => $faker->word,
        'verified' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
