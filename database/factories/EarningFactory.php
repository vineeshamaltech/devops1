<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Earning;
use Faker\Generator as Faker;

$factory->define(Earning::class, function (Faker $faker) {

    return [
        'payment_id' => $faker->randomDigitNotNull,
        'credited_to' => $faker->randomElement(]),
        'amount' => $faker->word,
        'remarks' => $faker->word,
        'is_settled' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
