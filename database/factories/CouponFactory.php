<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Coupon;
use Faker\Generator as Faker;

$factory->define(Coupon::class, function (Faker $faker) {

    return [
        'store_id' => $faker->word,
        'image' => $faker->word,
        'code' => $faker->word,
        'description' => $faker->word,
        'max_use_per_user' => $faker->randomDigitNotNull,
        'min_cart_value' => $faker->word,
        'start_date' => $faker->date('Y-m-d H:i:s'),
        'end_date' => $faker->date('Y-m-d H:i:s'),
        'discount_type' => $faker->word,
        'discount_value' => $faker->word,
        'active' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
