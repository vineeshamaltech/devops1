<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\ProductVariant;
use Faker\Generator as Faker;

$factory->define(ProductVariant::class, function (Faker $faker) {

    return [
        'product_id' => $faker->word,
        'name' => $faker->word,
        'price' => $faker->word,
        'sku' => $faker->word,
        'image' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
