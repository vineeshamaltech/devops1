<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\StoreGallery;
use Faker\Generator as Faker;

$factory->define(StoreGallery::class, function (Faker $faker) {

    return [
        'store_id' => $faker->randomDigitNotNull,
        'title' => $faker->word,
        'description' => $faker->word,
        'image' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
