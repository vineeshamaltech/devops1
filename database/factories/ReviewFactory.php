<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Review;
use Faker\Generator as Faker;

$factory->define(Review::class, function (Faker $faker) {

    return [
        'id' => $faker->word,
        'order_id' => $faker->randomDigitNotNull,
        'store_rating' => $faker->word,
        'driver_rating' => $faker->word,
        'store_review' => $faker->word,
        'driver_review' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
