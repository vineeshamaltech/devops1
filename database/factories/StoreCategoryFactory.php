<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\StoreCategory;
use Faker\Generator as Faker;

$factory->define(StoreCategory::class, function (Faker $faker) {

    return [
        'name' => $faker->word,
        'image' => $faker->word,
        'service_category_id' => $faker->randomDigitNotNull,
        'status' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
