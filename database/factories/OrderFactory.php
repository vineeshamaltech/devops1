<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Order;
use Faker\Generator as Faker;

$factory->define(Order::class, function (Faker $faker) {

    return [
        'user_id' => $faker->randomDigitNotNull,
        'store_id' => $faker->randomDigitNotNull,
        'coupon_data' => $faker->word,
        'order_type' => $faker->word,
        'total_price' => $faker->word,
        'discount_type' => $faker->word,
        'delivery_fee' => $faker->word,
        'tax' => $faker->word,
        'grand_total' => $faker->word,
        'pickup_address' => $faker->word,
        'delivery_address' => $faker->word,
        'distance_travelled' => $faker->word,
        'travel_time' => $faker->word,
        'payment_id' => $faker->randomDigitNotNull,
        'driver_id' => $faker->randomDigitNotNull,
        'order_status' => $faker->word,
        'json_data' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
