<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\StoreTiming;
use Faker\Generator as Faker;

$factory->define(StoreTiming::class, function (Faker $faker) {

    return [
        'store_id' => $faker->randomDigitNotNull,
        'day' => $faker->word,
        'open' => $faker->word,
        'close' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
