<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\ServiceCategory;
use Faker\Generator as Faker;

$factory->define(ServiceCategory::class, function (Faker $faker) {

    return [
        'icon' => $faker->word,
        'bg_image' => $faker->word,
        'home_view' => $faker->randomDigitNotNull,
        'name' => $faker->word,
        'description' => $faker->word,
        'type' => $faker->randomDigitNotNull,
        'discount' => $faker->word,
        'commission' => $faker->word,
        'vehicles' => $faker->word,
        'min_price' => $faker->word,
        'driver_radius' => $faker->word,
        'base_distance' => $faker->word,
        'base_price' => $faker->word,
        'base_km' => $faker->word,
        'price_km' => $faker->word,
        'max_order_distance' => $faker->word,
        'payment_methods' => $faker->word,
        'active' => $faker->word
    ];
});
