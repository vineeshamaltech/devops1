<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\HomeServiceCategory;
use Faker\Generator as Faker;

$factory->define(HomeServiceCategory::class, function (Faker $faker) {

    return [
        'id' => $faker->word,
        'name' => $faker->word,
        'icon' => $faker->word,
        'background_image' => $faker->word,
        'view' => $faker->randomElement(]),
        'parent' => $faker->randomDigitNotNull,
        'tax' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
