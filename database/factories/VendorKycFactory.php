<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\VendorKyc;
use Faker\Generator as Faker;

$factory->define(VendorKyc::class, function (Faker $faker) {

    return [
        'vedor_id' => $faker->word,
        'doc_type' => $faker->word,
        'photo' => $faker->word,
        'status' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
