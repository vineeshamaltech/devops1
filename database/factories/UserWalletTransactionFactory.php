<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\UserWalletTransaction;
use Faker\Generator as Faker;

$factory->define(UserWalletTransaction::class, function (Faker $faker) {

    return [
        'user_id' => $faker->randomDigitNotNull,
        'type' => $faker->randomElement(]),
        'amount' => $faker->word,
        'remarks' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
