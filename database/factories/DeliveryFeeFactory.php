<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\DeliveryFee;
use Faker\Generator as Faker;

$factory->define(DeliveryFee::class, function (Faker $faker) {

    return [
        'from_km' => $faker->randomDigitNotNull,
        'to_km' => $faker->randomDigitNotNull,
        'delivery_fee' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
