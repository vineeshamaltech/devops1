<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Product;
use Faker\Generator as Faker;

$factory->define(Product::class, function (Faker $faker) {

    return [
        'store_id' => $faker->randomDigitNotNull,
        'product_type' => $faker->word,
        'name' => $faker->word,
        'description' => $faker->word,
        'image' => $faker->word,
        'unit' => $faker->word,
        'price' => $faker->word,
        'discount_price' => $faker->word,
        'store_category_id' => $faker->word,
        'sku' => $faker->randomDigitNotNull,
        'product_tags' => $faker->word,
        'active' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
