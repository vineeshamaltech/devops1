<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\RideBooking;
use Faker\Generator as Faker;

$factory->define(RideBooking::class, function (Faker $faker) {

    return [
        'id' => $faker->word,
        'user_id' => $faker->word,
        'ride_id' => $faker->word,
        'from_name' => $faker->word,
        'to_name' => $faker->word,
        'from_lat' => $faker->word,
        'from_long' => $faker->word,
        'to_lat' => $faker->word,
        'to_long' => $faker->word,
        'price' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
