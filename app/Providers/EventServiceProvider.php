<?php

namespace App\Providers;

use App\Events\OrderChanged;
use App\Events\OrderStatusChangedEvent;
use App\Events\ServiceOrderStatusChanged;
use App\Helpers\WalletHelper;
use App\Listeners\AssignDriver;
use App\Listeners\CalculateEarnings;
use App\Listeners\DistributeServiceOrderEarnings;
use App\Listeners\LogNotification;
use App\Listeners\OrderStatusChanged;
use App\Listeners\ReferralCredit;
use App\Listeners\WalletCashback;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        OrderChanged::class => [
            CalculateEarnings::class,
            ReferralCredit::class,
            WalletCashback::class,
            AssignDriver::class
        ],
        ServiceOrderStatusChanged::class => [
            DistributeServiceOrderEarnings::class
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
