<?php

namespace App\Providers;

use Illuminate\Cache\RateLimiting\Limit;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Http\Request;
use Illuminate\Routing\Router;
use Illuminate\Support\Facades\RateLimiter;
use Illuminate\Support\Facades\Route;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * The path to the "home" route for your application.
     *
     * This is used by Laravel authentication to redirect users after login.
     *
     * @var string
     */
    public const HOME = '/home';

    public const VENDORHOME = '/vendor/index';

    /**
     * The controller namespace for the application.
     *
     * When present, controller route declarations will automatically be prefixed with this namespace.
     *
     * @var string|null
     */
    // protected $namespace = 'App\\Http\\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        $this->configureRateLimiting();
        $this->routes(function () {
            Route::prefix('api')
                ->middleware('api')
                ->as('api.')
                ->namespace($this->app->getNamespace().'Http\Controllers\API')
                ->group(base_path('routes/api.php'));

            Route::middleware(['web'])
                ->namespace($this->app->getNamespace().'Http\Controllers')
                ->group(base_path('routes/web.php'));

            Route::prefix('vendor')
                ->middleware(['web'])
                ->as('vendor.')
                ->namespace($this->app->getNamespace().'Http\Controllers\Admin')
                ->group(base_path('routes/vendor.php'));

            Route::prefix('admin')
                ->middleware(['web'])
                ->as('admin.')
                ->namespace($this->app->getNamespace().'Http\Controllers\Admin')
                ->group(base_path('routes/admin.php'));

        });
    }

    /**
     * Configure the rate limiters for the application.
     *
     * @return void
     */
    protected function configureRateLimiting()
    {
        RateLimiter::for('api', function (Request $request) {
            return Limit::perMinute(600)->by(optional($request->user())->id ?: $request->ip());
        });
    }
}
