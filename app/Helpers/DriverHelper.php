<?php

namespace App\Helpers;

use App\Models\Driver;
use App\Models\DriverDutySession;
use App\Models\Earning;
use App\Notifications\WalletNotification;
use Carbon\Carbon;
use Notification;

class DriverHelper{

    public static function dutyToggle($driver_id){
        $driver = Driver::where('id', $driver_id)->first();
        $duty = DriverDutySession::where('driver_id', $driver->id)->orderBy('id', 'desc')->first();
        if(!empty($duty)){
            if ($duty->end_time == null) {
                //Update same record
                $duty->end_time = Carbon::now();
                $duty->save();
                //Duty ended. Calculate incentive
                $start = Carbon::parse($duty->start_time);
                $end = Carbon::parse($duty->end_time);
                $interval = $start->diffInSeconds($end);
                $min_sec_for_incentive = setting('min_hours_for_incentive',10) * 60 * 60;

                if ($interval >= $min_sec_for_incentive){
                    $amount = setting('driver_incentive_per_hour',0);
                    $amountSecond = round($amount/3600,2);
                    $incentive_commission = round($interval*$amountSecond,2);
                    $driver->incentive = $driver->incentive + $incentive_commission;
                    $driver->save();
                    Earning::create([
                        'payment_id' => NULL,
                        'order_id' =>NULL,
                        'entity_type' => Driver::class,
                        'entity_id' => $driver->id,
                        'amount' => $incentive_commission,
                        'type' => 'INCENTIVE',
                        'remarks' => 'Incentive added for login time',
                        'is_settled' => false
                    ]);
                    Notification::send($driver, new WalletNotification($incentive_commission.' credited to your incentive'));
                }
                return true;
            } else {
                //No record found. Create new one
                DriverDutySession::create([
                    'driver_id' => $driver->id,
                    'start_time' => Carbon::now()
                ]);
                return false;
            }
        }else{
            //No record found. Create new one
            DriverDutySession::create([
                'driver_id' => $driver->id,
                'start_time' => Carbon::now()
            ]);
            return false;
        }
    }

}
