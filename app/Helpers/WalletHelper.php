<?php

namespace App\Helpers;

use App\Models\Driver;
use App\Models\DriverWalletTransaction;
use App\Models\User;
use App\Models\UserWalletTransaction;
use App\Notifications\WalletNotification;
use DB;
use Notification;

class WalletHelper{

    public static function debitUserWallet($user_id, $amount, $description){
        try{
            DB::beginTransaction();
            $user = User::where('id', $user_id)->first();
            UserWalletTransaction::create([
                'user_id' => $user_id,
                'amount' => $amount,
                'type' => 'DEBIT',
                'opening_balance' => $user->wallet_amount,
                'closing_balance' => $user->wallet_amount - $amount,
                'remarks' => $description
            ]);
            $user->wallet_amount = $user->wallet_amount - $amount;
            $user->save();
            Notification::send($user, new WalletNotification($amount.' debited from your wallet'));
            DB::commit();
        }catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    public static function creditUserWallet($user_id, $amount, $description){
        DB::beginTransaction();
        try{
            $user = User::where('id', $user_id)->first();
            UserWalletTransaction::create([
                'user_id' => $user_id,
                'amount' => $amount,
                'type' => 'CREDIT',
                'opening_balance' => $user->wallet_amount,
                'closing_balance' => $user->wallet_amount + $amount,
                'remarks' => $description
            ]);
            $user->wallet_amount = $user->wallet_amount + $amount;
            $user->save();
            Notification::send($user, new WalletNotification($amount.' credited to your wallet'));
            DB::commit();
        }catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    public static function addDriverCashInHand($driver_id, $amount, $description){
        DB::beginTransaction();
        try{
            $driver = User::where('id', $driver_id)->first();
            DriverWalletTransaction::create([
                'driver_id' => $driver_id,
                'amount' => $amount,
                'type' => 'CREDIT',
                'opening_balance' => $driver->cash_in_hand,
                'closing_balance' => $driver->cash_in_hand + $amount,
                'remarks' => $description
            ]);
            $driver->wallet_amount = $driver->cash_in_hand + $amount;
            if (($driver->cash_in_hand + $amount) >= setting('driver_bootcash_limit',2000)){
                $driver->cash_in_hand_overflow = false;
            }
            $driver->save();
            Notification::send($driver, new WalletNotification($amount.' added to your bootcash'));
            DB::commit();
        }catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    public static function clearDriverCashInHand($driver_id, $amount, $description){
        DB::beginTransaction();
        try{
            $driver = User::where('id', $driver_id)->first();
            DriverWalletTransaction::create([
                'driver_id' => $driver_id,
                'amount' => $amount,
                'type' => 'DEBIT',
                'opening_balance' => $driver->cash_in_hand,
                'closing_balance' => $driver->cash_in_hand - $amount,
                'remarks' => $description
            ]);
            $driver->wallet_amount = $driver->cash_in_hand - $amount;
            if (($driver->cash_in_hand + $amount) < setting('driver_bootcash_limit',2000)){
                $driver->cash_in_hand_overflow = false;
            }
            $driver->save();
            Notification::send($driver, new WalletNotification($amount.' added to your bootcash'));
            DB::commit();
        }catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    public static function debitDriverWallet($driver_id, $amount, $description){
        try{
            DB::beginTransaction();
            $driver = Driver::where('id', $driver_id)->first();
            DriverWalletTransaction::create([
                'driver_id' => $driver_id,
                'amount' => $amount,
                'type' => 'DEBIT',
                'opening_balance' => $driver->cash_in_hand,
                'closing_balance' => $driver->cash_in_hand - $amount,
                'remarks' => $description
            ]);
            if (($driver->cash_in_hand - $amount) <= setting('driver_bootcash_limit',2000)){
                $driver->cash_in_hand_overflow = true;
                $driver->on_duty = false;
                $driver->save();
                DriverHelper::dutyToggle($driver_id);
            }else{
                $driver->cash_in_hand_overflow = false;
            }
            $driver->cash_in_hand = $driver->cash_in_hand - $amount;
            $driver->save();
            if($amount != 0){
                Notification::send($driver, new WalletNotification($amount.' debited from your wallet'));
            }
            // Notification::send($driver, new WalletNotification($amount.' debited from your wallet'));
            DB::commit();
        }catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    public static function creditDriverWallet($driver_id, $amount, $description){
        try{

            DB::beginTransaction();
            $driver = Driver::where('id', $driver_id)->first();
            DriverWalletTransaction::create([
                'driver_id' => $driver_id,
                'amount' => $amount,
                'type' => 'CREDIT',
                'opening_balance' => $driver->cash_in_hand,
                'closing_balance' => $driver->cash_in_hand + $amount,
                'remarks' => $description
            ]);



            if (($driver->cash_in_hand + $amount) <= setting('driver_bootcash_limit',2000)){
                $driver->cash_in_hand_overflow = true;
                $driver->on_duty = false;
                DriverHelper::dutyToggle($driver_id);
            }else{
                $driver->cash_in_hand_overflow = false;
            }
            $driver->cash_in_hand = $driver->cash_in_hand + $amount;
            $driver->save();
            Notification::send($driver, new WalletNotification($amount.' credited to your wallet'));
            DB::commit();
        }catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

}
