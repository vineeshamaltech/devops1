<?php

namespace App\Helpers;

use Carbon\Carbon;

class DatatableHelpers
{
    public static function roundImageSection($img_url){
        return '<img onerror="this.onerror=null;this.src=\''.asset("assets/img/no-image.png").'\';" src="'.$img_url.'" width="50" height="50" class="rounded-circle" />';
    }

    public static function imageSection($img_url){
        //add corner radius to image via css
        return '<img onerror="this.onerror=null;this.src=\''.asset("assets/img/no-image.png").'\';" src="'.$img_url.'" width="50" height="50" class="rounded" />';
    }

    public static function booleanColumn($value){
        if($value){
            return '<span class="badge badge-success">Yes</span>';
        }else{
            return '<span class="badge badge-danger">No</span>';
        }
    }

    public static function urlColumn($value){
        if ($value == null){
            return DatatableHelpers::dangerBadge("No URL");
        }else{
            return '<a href="'.$value.'" target="_blank">'.$value.'</a>';
        }

    }

    public static function statusColumn($value){
        if($value){
            return '<span class="badge badge-success">Active</span>';
        }else{
            return '<span  class="badge badge-danger">Inactive</span>';
        }
    }

    // format time with carbon
    public static function timeColumn($value){
        return Carbon::parse($value)->format('g:i A');
    }

    public static function dangerBadge($value){
        return '<span class="badge badge-danger">'.$value.'</span>';
    }

    public static function successBadge($value){
        return '<span class="badge badge-success">'.$value.'</span>';
    }

    public static function dateDiffColumn($value){
        $parsed = Carbon::parse($value);
        $diff = $parsed->diffInDays(Carbon::now());
        if($diff > 7){
            return '<span data-toggle="tooltip" title="'.$value.'" class="badge badge-success">'.$diff.' days</span>';
        }else if($diff >= 1){
            return '<span data-toggle="tooltip" title="'.$value.'" class="badge badge-warning">'.Carbon::parse($value)->diffForHumans().'</span>';
        }else{
            return '<span data-toggle="tooltip" title="'.$value.'" class="badge badge-danger">Expired '.Carbon::parse($value)->diffForHumans().'</span>';
        }
    }

    public static function dateColumn($value, $format = 'd M Y g:i A'){
        $parsed = Carbon::parse($value);
        $diff = $parsed->format($format);
        return '<span data-toggle="tooltip" title="'.$value.'" class="badge badge-success">'.$diff.'</span>';
    }

    public static function currencyColumn($value){
        return '<span class="badge badge-primary">'.config('constants.currency_symbol').$value.'</span>';
    }
}

