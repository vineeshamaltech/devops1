<?php

use App\Models\Admin;
use App\Models\Store;
use App\Models\User;

class Helpers{

    public static function img_url($img_name='no-image.png',$path = 'storage'){
        return $path."/".$img_name;
    }

    //generate random string
    public static function randomString($length = 10) {
        $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    //generate random string
    public static function randomOTP($length = 6) {
        $characters = '0123456789';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    //generate random invoice_id
    public static function invoice_id() {
        return "ORD-".time();
    }

    //upload image to storage directory
    public static function uploadImage($file,$folder){
        $rand=rand(1,999);
        $name = time() .$rand.'.' . $file->getClientOriginalExtension();
        $destinationPath = public_path('/storage/'.$folder);
        $file->move($destinationPath, $name);
        return env('APP_URL','').'/storage/'.$folder.'/'.$name;
    }

    //upload image to storage directory
    public static function uploadCsv($file,$folder){
        $name = time() . '.' . $file->getClientOriginalExtension();
        $destinationPath = public_path('/storage/'.$folder);
        $file->move($destinationPath, $name);
        return '/storage/'.$folder.'/'.$name;
    }

    //delete image from storage directory
    public static function deleteImage($image_path){
        try{
            $image_path = str_replace(env('APP_URL',''),'',$image_path);
            $image_path = public_path($image_path);
            if(file_exists($image_path)){
                unlink($image_path);
            }
        }catch (\Exception $e){
            return false;
        }
    }


    /**
     * Get user role id
     *
     * @param $user
     * @return string
     */
    static function get_user_stores()
    {
        if(auth()->user()->hasRole('Vendor') || auth()->user()->hasRole('Franchisee') || auth()->user()->hasRole('Admin')){
            $stores = Store::where('admin_id', auth()->user()->id)->where('active',1)->get()->pluck('id');
        }else{
            $stores = Store::get()->pluck('id');
        }
        return $stores;
    }

    static function get_stores_by_user($id)
    {
        $admin = Admin::findOrFail($id);
        if($admin->hasRole('Vendor')){
            $stores = Store::where('admin_id', $id)->where('active',1)->get();
        }else{
            $stores = Store::get();
        }
        return $stores;
    }

    //is user admin
    public static function is_admin(){
        return auth()->user()->hasRole('Admin');
    }

    //is user super admin
    public static function is_super_admin(){
        return auth()->user()->hasRole('Super Admin');
    }

    //is user vendor
    public static function is_vendor(){
        return auth()->user()->hasRole('Vendor');
    }

    //is user Franchisee
    public static function is_franchisee(){
        return auth()->user()->hasRole('Franchisee');
    }

    /**
     * Get zones for user
     *
     * @param $id
     * @return zones array
     */
    public static function get_user_zones($id = null)
    {
        $admin = Admin::find(auth()->user()->id);
        $zones = explode(",",$admin->zones);
        return $zones;
    }

    /**
     * Get zones for auth user
     *
     * @param 
     * @return zones array
     */
    public static function get_assigned_zones()
    {
        $admin = auth()->user()->zones;
        $zones = explode(",",$admin->zones);
        return $zones;
    }
    public static function format_settings_title($str){
        if(str_contains($str,'_')){
            $str = str_replace('_',' ',$str);
            $str = ucwords($str);
        }else{
            $str = ucwords($str);
        }
        return $str;
    }

    public static function upload_csv($file, $type='products', $delimiter = ',')
    {
        $filepath = Helpers::uploadImage($file,'imports');
        if (!file_exists($filepath) || !is_readable($filepath))
            return false;
        $header = null;
        $data = array();
        if (($handle = fopen($filepath, 'r')) !== false)
        {
            while (($row = fgetcsv($handle)) !== false)
            {
                if (!$header)
                    $header = $row;
                else
                    $data[] = array_combine($header, $row);
            }
            fclose($handle);
        }
        return $data;
    }

    public static function getTestPhoneNo(): array {
        return ['9999999999','8888888888','7777777777','6666666666','5555555555','4444444444','3333333333','2222222222','1111111111'];
    }

    public static function findInZone($latitude,$longitude) : array{
        $zones = \App\Models\Zone::where('active',1)->get();
        $allzone = [];
        foreach ($zones as $zone){
            $response =  \GeometryLibrary\PolyUtil::containsLocation(
                ['lat' => $latitude, 'lng' => $longitude], // point array [lat, lng]
                $zone->polygon // polygon array of points
            );
            if($response){
                $allzone[] = $zone;
            }
        }
        return $allzone;
    }

    public static function getDriversByHomeServiceCategoryId($hscId,$zoneId){
    
        $driverIds = \App\Models\DriverHomeServices::where('home_service_category_id',$hscId)->get()->pluck('driver_id');
       
        if (count($driverIds) > 0){
            if($zoneId == null)
            {
                return []; 
            }
            $driver = \App\Models\Driver::whereIn('id',$driverIds)
                ->where('home_service_enabled',1)
                ->where('active',1)
                ->where('is_verified',1)
                ->where('cash_in_hand_overflow',0)
                ->where('on_duty',1)
                //  ->whereRaw("FIND_IN_SET(".(int)$zoneId.",zones)")
                // ->whereRaw('find_in_set('.$zoneId.',zones)')
                ->get();
                $newdriver = [];
                $i = 0;
                 
                foreach($driver as $key => $value)
                {
                   
                    $dr_zones = explode(',',$value->zones);
                    
                    foreach($dr_zones as $value2)
                    {
                      
                        if((int)$value2 == $zoneId)
                        {
                            
                              $newdriver[$i++] = $value;
                             
                        }
                      
                        
                    }
                    
                }
             
                return $newdriver;
        }else{
            return [];
        }
    }

    public function check_in_time($from,$to){
        $current_time = now()->format('H:i:s');
        if($to == "00:00:00"){
            $to = "23:59:59";
        }
        if (strtotime($current_time) > strtotime($from) && strtotime($current_time) < strtotime($to))
        {
            return true;
        }else{
            return false;
        }
    }
}
