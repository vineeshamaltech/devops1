<?php

namespace App\Helpers;

use App\Models\DeliveryFee;
use App\Models\Driver;
use App\Models\DriverHomeServices;
use App\Models\Order;
use App\Models\Payment;
use App\Models\PaymentType;
use App\Models\ServiceCategory;
use App\Models\ServiceOrder;
use App\Models\Store;
use App\Models\User;
use Illuminate\Support\Facades\DB;

class OrderHelper
{

    public static function assignDriver($order_id, $excluded_drivers = []): String {
        try {
           
            $order = Order::with('store')->find($order_id);
            $latitude = $order->pickup_address['latitude'] ?? 0;
            $longitude = $order->pickup_address['longitude'] ?? 0;
            //Skip is lat long is not available
            if ($latitude == null || $longitude == null) {
                return false;
            }

            // $vehicle_type = DB::table('service_categories as sc')

            //     ->where('sc.service_type_id', '=', $type)
            //     ->join('service_category_vehicle_type as scv', 'scv.service_category_id', '=', 'sc.id')
            //     ->select('scv.vehicle_type_id as id')->first();

            $drivers = Driver::select(DB::raw("SQRT(
                POW(69.1 * (latitude - $latitude), 2) +
                POW(69.1 * ($longitude - longitude) * COS(latitude / 57.3), 2)) AS distance"), 'id', 'on_duty','fcm_token', 'active', 'is_verified', 'cash_in_hand_overflow', 'cash_in_hand')
                ->whereNotIn('id', $excluded_drivers)
                ->where('active', '=', 1)
                ->where('is_verified', '=', 1)
                ->where('on_duty', '=', 1)
                ->where('is_busy', '=', 0)
                // ->where('cash_in_hand_overflow', '=', 0)
                ->having("distance", "<", setting('find_driver_in_radius', 5));
 
            if ($order->vehicle_id!=null) {
                $drivers->where('vehicle_type_id', $order->vehicle_id);
            }

            $drivers = $drivers->orderBy("distance", 'asc')
                ->offset(0)
                ->limit(20)
                ->get();

            if (count($drivers) > 0) {
                //driver found
                 $driver = $drivers->first();
              
                $order->driver_id = $driver->id;
                $order->order_status = "ASSIGNED";
                $order->save();

                DB::table('autoassign_logs')->insert([
                    'order_id' => $order_id,
                    'assigned' => $driver->id,
                    'in_range' => json_encode($drivers),
                    'excluded' => json_encode($excluded_drivers),
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                ]);

                Driver::where('id', $driver->id)->update(['is_busy' => 1]);
                
        //   send notification to driver to accept orders
   
          $fcmkey =  DB::table('settings')->where('key_name','fcm_key')->first();
            
        // $result = array();
        // $chunks = array_chunk($tokens,999);
       
        // foreach($chunks as $chunk){
               $chunk = $driver->fcm_token;
          
            $url = 'https://fcm.googleapis.com/fcm/send'; 
            // if($order != null)
            // {
                $dataArr = array('click_action' => 'FLUTTER_NOTIFICATION_CLICK','type' => 'order_assigned',
            'is_auto_assigned' => true,'id'=> $order->id); 
            // }
            // else
            // $dataArr = array('click_action' => 'FLUTTER_NOTIFICATION_CLICK'); 
           
    
            $notification = array('title' =>'Order Assigned', 'body' => 'Please response on new order assigned',  'sound' =>'enabled' , 'default', 'badge' => '1');
    
            $arrayToSend = array('to' =>$chunk , 'notification' => $notification, 'data' => $dataArr, 'priority'=>'high' );
    
            $tempArr = $arrayToSend;
    
            $fields = json_encode($arrayToSend);
           
            
            $headers = array (
                'Authorization: key=' . $fcmkey->key_value,
                'Content-Type: application/json'
    
            );
    
            //Register notification in database
            $ch = curl_init ();
    
            curl_setopt ( $ch, CURLOPT_URL, $url );
    
            curl_setopt ( $ch, CURLOPT_POST, true );
    
            curl_setopt ( $ch, CURLOPT_HTTPHEADER, $headers );
    
            curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );
    
            curl_setopt ( $ch, CURLOPT_POSTFIELDS, $fields );
            $result[] = curl_exec ( $ch );
   
            //   dd($result);
            curl_close ( $ch );
           return 1;
                
                //Send notification to driver
                // $driver->notify(new \App\Notifications\OrderAssigned("Order #" . $order->id . " has been assigned to you.", $order));
                // send_push($driver,'Order Assigned',"Order #".$order->id." has been assigned to you.",$order);
                //   SELF::send_push_driver($driver->fcm_token,'Order Assigned','new order assigned to you ',$order);
                  
                // return $driver->fcm_token;
            } else {
                DB::table('autoassign_logs')->insert([
                    'order_id' => $order_id,
                    'assigned' => null,
                    'in_range' => json_encode($drivers),
                    'excluded' => json_encode($excluded_drivers),
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                ]);
            }
            return "Not assigned";
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public static function assignHomeServiceDriver($order_id, $excluded_drivers = []): bool{
        try {
            $order = ServiceOrder::with('items')->find($order_id);

            $latitude = $order->service_address['latitude'] ?? 0;
            $longitude = $order->service_address['longitude'] ?? 0;
            // dd($latitude, $longitude);
            //Skip is lat long is not available
            if ($latitude == null || $longitude == null) {
                return false;
            }
             
            $drivers_list = DriverHomeServices::select('driver_id')->where('home_service_category_id', $order->service_category_id)->get();

            $drivers = Driver::
                select(DB::raw("SQRT(
                POW(69.1 * (latitude - $latitude), 2) +
                POW(69.1 * ($longitude - longitude) * COS(latitude / 57.3), 2)) AS distance"), 'id', 'on_duty', 'active', 'is_verified', 'cash_in_hand_overflow', 'cash_in_hand','fcm_token')
                ->whereNotIn('id', $excluded_drivers)
                ->whereIn('id', $drivers_list)
                ->where('active', '=', 1)
                ->where('is_verified', '=', 1)
                ->where('on_duty', '=', 1)
                ->where('is_busy', '=', 0)
                // ->where('cash_in_hand_overflow', '=', 0)
                ->where('home_service_enabled', 1)
                ->having("distance", "<", setting('find_driver_in_radius', 10))
                // ->whereRaw("find_in_set(".$order->zones.",zones)")
                  ->whereRaw("FIND_IN_SET(".$order->zone_id.",zones)")
                ->orderBy("distance", 'asc')
                ->offset(0)
                ->limit(20)
                ->get();
        
            if (count($drivers) > 0) {
                //driver found
                $driver = $drivers->first();
             
                $order->driver_id = $driver->id;
                $order->order_status = "ASSIGNED";
                $order->save();

                // DB::table('autoassign_logs')->insert([
                //     'order_id' => $order_id,
                //     'assigned' => $driver->id,
                //     'in_range' => json_encode($drivers),
                //     'excluded' => json_encode($excluded_drivers),
                //     'created_at' => date('Y-m-d H:i:s'),
                //     'updated_at' => date('Y-m-d H:i:s'),
                // ]);

                Driver::where('id', $driver->id)->update(['is_busy' => 1]);
                $driver->notify(new \App\Notifications\NewOrder("Order #" . $order->id . " has been assigned to you.", $order));
                return $driver;
            } else {
                DB::table('autoassign_logs')->insert([
                    'order_id' => $order_id,
                    'assigned' => null,
                    'in_range' => json_encode($drivers),
                    'excluded' => json_encode($excluded_drivers),
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                ]);
            }
            return false;
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public static function calculateEarnings($order_id)
    {
        try {
            
          
            $order = Order::with('service_category')->find($order_id);
            $coupon = $order->coupon_data;
            $driver = Driver::find($order->driver_id);
            $store = Store::find($order->store_id);
            if (!empty($store)) {
                $store_commission = $store->commission;
            }
            $tip_commission = setting('tip_commission', 0);

            $admin_discount = 0;
            $vendor_discount = 0;

            if ($coupon != null) {
                if ($coupon['store_id'] == null) {
                    $admin_discount = $order->discount_value;
                } else {
                    $vendor_discount = $order->discount_value;
                }
            }
            if (in_array($order->order_type,config('constants.dynamic_delivery_ids')) || $order->order_type == 4) {
                $serviceCategory = ServiceCategory::find($order->service_category_id);
                $driver_commission = $order->delivery_fee - $order->delivery_fee * $serviceCategory->commission / 100;
            }else{
                $deliveryfee = DeliveryFee::where('delivery_fee', $order->delivery_fee)->first();
                $driver_commission = $deliveryfee->driver_commission ?? 0;
            }

            if (!empty($store)) {
                //Calculate Vendor Earnings
                $vendor_commission = $order->total_price - ($order->total_price * ($store_commission / 100));
            }else{
                $vendor_commission = 0;
            }

            //Calculate Driver Earnings
            $driver_tip_commission = $order->tip - ($tip_commission / 100 * $order->tip);

            //Calculate admin Earnings
            $admin_commission = $order->tax + ($order->total_price - $vendor_commission) + ($order->delivery_fee - $driver_commission) + ($order->tip - $driver_tip_commission);

            //Calculate Driver Earnings
            $order->admin_commission = $admin_commission - $admin_discount;
            $order->store_commission = $vendor_commission - $vendor_discount;
            $order->driver_commission = $driver_commission + $driver_tip_commission;
            $order->save();
            return "Admin: " . $order->admin_commission . " Vendor: " . $order->store_commission . " Driver: " . $order->driver_commission;
        } catch (\Exception $e) {
            $order = Order::find($order_id);
            $order->admin_commission = 0;
            $order->store_commission = 0;
            $order->driver_commission = 0;
            $order->save();
            return $e->getMessage();
        }
    }

    public static function testPayment()
    {
        $payment_id = "pay_IdzQZBvVSchwjv";
        $paymentMethod = PaymentType::find(1);
        $key = $paymentMethod->payment_key;
        $secret = $paymentMethod->payment_secret;
        $api = new \Razorpay\Api\Api($key, $secret);
        $response = $api->payment->fetch($payment_id);
        $payment = Payment::create([
            'ref_no' => $response->id,
            'user_id' => 1,
            'user_type' => get_class(User::find(1)),
            'payment_request_id' => 1,
            'remarks' => "test pay",
            'payment_method' => "ONLINE",
            'amount' => $response->amount,
            'gateway_response' => $response->toArray(),
        ]);
        return $payment;
    }

    public static function get_status_by_user_type()
    {
        if (auth()->user()->hasRole('Vendor')) {
            return [
                'ACCPETED' => 'ACCPETED',
                'PREPARED' => 'PREPARED',
                'CANCELLED' => 'CANCELLED',
            ];
        } else {
            return [
                'PLACED' => 'PLACED',
                'ACCEPTED' => 'ACCEPTED',
                'ASSIGNED' => 'ASSIGNED',
                'PREPARED' => 'PREPARED',
                'REACHED' => 'REACHED',
                'PICKEDUP' => 'PICKEDUP',
                'DELIVERED' => 'DELIVERED',
                'USERCANCELLED' => 'USERCANCELLED',
                'CANCELLED' => 'CANCELLED',
            ];
        }
    }

    public static function assignVendor($order_id, $excluded_vendors = []): bool
    {
        try {
            $order = Order::find($order_id);
            $latitude = $order->service_address['latitude'] ?? 0;
            $longitude = $order->service_address['longitude'] ?? 0;
            //Skip is lat long is not available
            if ($latitude == null || $longitude == null) {
                return false;
            }
            $drivers = Driver::select(DB::raw("SQRT(
                POW(69.1 * (latitude - $latitude), 2) +
                POW(69.1 * ($longitude - longitude) * COS(latitude / 57.3), 2)) AS distance"), 'id', 'on_duty', 'active', 'is_verified', 'cash_in_hand_overflow', 'cash_in_hand')
                ->whereNotIn('id', $excluded_vendors)
                ->where('active', '=', 1)
                ->where('is_verified', '=', 1)
                ->where('on_duty', '=', 1)
                ->where('is_busy', '=', 0)
                ->where('cash_in_hand_overflow', '=', 0)
                ->having("distance", "<", setting('find_driver_in_radius', 5))
                ->orderBy("distance", 'asc')
                ->offset(0)
                ->limit(20)
                ->get();

            if (count($drivers) > 0) {
                //driver found
                $driver = $drivers->first();
                $order->driver_id = $driver->id;
                $order->order_status = "ASSIGNED";
                $order->save();

                DB::table('autoassign_logs')->insert([
                    'order_id' => $order_id,
                    'assigned' => $driver->id,
                    'in_range' => json_encode($drivers),
                    'excluded' => json_encode($excluded_vendors),
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                ]);

                Driver::where('id', $driver->id)->update(['is_busy' => 1]);
                //Send notification to driver
                $driver->notify(new \App\Notifications\OrderAssigned("Order #" . $order->id . " has been assigned to you.", $order));
                return true;
            } else {
                DB::table('autoassign_logs')->insert([
                    'order_id' => $order_id,
                    'assigned' => null,
                    'in_range' => json_encode($drivers),
                    'excluded' => json_encode($excluded_vendors),
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                ]);
            }
            return false;
        } catch (\Exception$e) {
            return false;
        }
    }

    public static function testOrder($id){
        $order = Order::find($id);

    }
    
      public static function send_push_driver($user,$title,$desc,$order=null) {
        
       
        // }
        
    }
   

}
