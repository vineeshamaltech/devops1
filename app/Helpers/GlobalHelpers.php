<?php

use Illuminate\Support\Facades\Auth;

use App\Models\User;
use App\Models\Order;
use App\Models\Earning;
use App\Models\Admin;
use App\Models\Driver;
use App\Models\Store;

if (!function_exists('get_user_role')) {
    /**
     * Get user role
     *
     * @param $user
     * @return string
     */
    function get_user_role($user)
    {
        return $user->role->name;
    }
}

//is vendor
if (!function_exists('is_vendor')) {
    /**
     * Check if user is vendor
     *
     * @param $user
     * @return bool
     */
    function is_vendor()
    {
        $user = auth()->user();
        return $user->role->name == 'Vendor';
    }
}

//is admin
if (!function_exists('is_admin')) {
    /**
     * Check if user is admin
     *
     * @param $user
     * @return bool
     */
    function is_admin()
    {
        $user = Auth::user();
        return $user->role->name == 'Admin';
    }
}


 function get_dashboards()
{
    $dashboard=[];
    $startDate=date('y-m-d 00:00:01');
    $endDate=date('y-m-d 23:59:59');


    if(auth()->user()->type =='Vendor')
    {
        $zone = \Helpers::get_user_zones();
        $store= \Helpers::get_user_stores()->toArray();
        $orders=Order::select('id')->whereIn('store_id',$store)->count();
        $today_orders=Order::whereBetween('created_at', [$startDate, $endDate])->whereIn('store_id',$store)->count();
        $unassigned_orders=Order::whereBetween('created_at', [$startDate, $endDate])->where('driver_id',NULL)->whereIn('store_id',$store)->count();
        $earning=Earning::where('entity_type','App\Models\Store')->whereIn('entity_id',$store)->sum('amount');
        $today_earning=Earning::where('entity_type','App\Models\Store')->whereIn('entity_id',$store)->whereBetween('created_at', [$startDate, $endDate])->sum('amount');
        $admin_commision="";
        $today_admin_commision = "";
        $tax = "";
        $today_tax = "";

        $delivery_fee=Order::where('order_status','Delivered')->whereIn('store_id',$store)->sum('delivery_fee');
        $today_delivery_fee =Order::whereBetween('created_at', [$startDate, $endDate])->where('order_status','Delivered')->whereIn('store_id',$store)->sum('delivery_fee');
    }
    else if(auth()->user()->type =='Franchisee')
    {
        $zones = \Helpers::get_user_zones();
        $store= Store::whereIn('zone',$zones)->get()->pluck('id')->toArray();
        $orders=Order::select('id')->whereIn('store_id',$store)->count();
        $today_orders=Order::whereBetween('created_at', [$startDate, $endDate])->whereIn('store_id',$store)->count();
        $unassigned_orders=Order::whereBetween('created_at', [$startDate, $endDate])->where('driver_id',NULL)->whereIn('store_id',$store)->count();
        $earning=Earning::where('entity_type','App\Models\Store')->whereIn('entity_id',$store)->sum('amount');
        $today_earning=Earning::where('entity_type','App\Models\Store')->whereIn('entity_id',$store)->whereBetween('created_at', [$startDate, $endDate])->sum('amount');
        $admin_commision="";
        $today_admin_commision = "";
        $tax = "";
        $today_tax = "";

        $delivery_fee=Order::where('order_status','Delivered')->whereIn('store_id',$store)->sum('delivery_fee');
        $today_delivery_fee =Order::whereBetween('created_at', [$startDate, $endDate])->where('order_status','Delivered')->whereIn('store_id',$store)->sum('delivery_fee');
    }
    else{
        $earning=Earning::sum('amount');
        $today_earning=Earning::whereBetween('created_at', [$startDate, $endDate])->sum('amount');
        $orders=Order::count();
        $today_orders=Order::whereBetween('created_at', [$startDate, $endDate])->count();
        $unassigned_orders=Order::whereBetween('created_at', [$startDate, $endDate])->where('driver_id',NULL)->count();
        $admin_commision=Order::where('order_status','Delivered')->sum('admin_commission');
        $today_admin_commision = Order::whereBetween('created_at', [$startDate, $endDate])->where('order_status','Delivered')->sum('admin_commission');
        $tax=Order::where('order_status','Delivered')->sum('tax');
        $today_tax = Order::whereBetween('created_at', [$startDate, $endDate])->where('order_status','Delivered')->sum('tax');

        $delivery_fee=Order::where('order_status','Delivered')->sum('delivery_fee');
        $today_delivery_fee = Order::whereBetween('created_at', [$startDate, $endDate])->where('order_status','Delivered')->sum('delivery_fee');

    }
    $users=User::count();
    $today_users=User::whereBetween('created_at', [$startDate, $endDate])->count();
    $vendors=Admin::where('type','Vendor')->count();
    $pending_vendor=Admin::where('type','Vendor')->where('verified',0)->count();
    $drivers=Driver::count();
    $pending_driver=Driver::where('is_verified',0)->count();

    $dashboard=array(
       'users'=>$users,
       'today_users'=>$today_users,
       'orders'=>$orders,
       'today_orders'=>$today_orders,
       'unassigned_orders'=>$unassigned_orders,
       'earning'=>$earning,
       'today_earning'=>$today_earning,
       'vendor'=>$vendors,
       'pending_vendor'=>$pending_vendor,
       'driver'=>$drivers,
       'pending_driver'=>$pending_driver,
       'admin_commision' => number_format((float)$admin_commision, 2, '.', ''),
       'today_admin_commision' => number_format((float)$today_admin_commision, 2, '.', ''),
       'tax' => number_format((float)$tax, 2, '.', ''),
       'today_tax' => number_format((float)$today_tax, 2, '.', ''),
       'delivery_fee' => number_format((float)$delivery_fee, 2, '.', ''),
       'today_delivery_fee' => number_format((float)$today_delivery_fee, 2, '.', ''),
    );
    return $dashboard;
}


//is super admin
if (!function_exists('is_super_admin')) {
    /**
     * Check if user is super admin
     *
     * @param $user
     * @return bool
     */
    function is_super_admin()
    {
        $user = auth()->user();
        return $user->role->name == 'Super Admin';
    }
}

if (!function_exists('setting')) {
    /**
     * Get setting by key
     *
     * @param  string $key key of setting
     * @return mixed value of setting
     */
    function setting($key,$default = null)
    {
        try {
            $setting = \App\Models\Setting::where('key_name', $key)->first();
            if($setting != null){
                if(is_numeric($setting->key_value)){
                    return floatval($setting->key_value);
                }elseif (is_bool($setting->key_value)){
                    return (bool)$setting->key_value;
                }else{
                    return $setting->key_value;
                }
            }else{
                return $default;
            }

        }catch (\Exception $e) {
            return $default;
        }
    }
}

if (!function_exists('format_number')) {
    function format_number($number){
        return floatval(number_format($number, 2, '.', ''));
    }
}

if (!function_exists('seconds_to_hms')){
    function seconds_to_hms($seconds_time = 0): string {
        if ($seconds_time == null || $seconds_time == 0) {
            return '00:00:00';
        }
        if ($seconds_time < 24 * 60 * 60) {
            return gmdate('H:i:s', $seconds_time);
        } else {
            $hours = floor($seconds_time / 3600);
            $minutes = str_pad(floor(($seconds_time - $hours * 3600) / 60), 2, "0", STR_PAD_LEFT);
            $seconds = str_pad(floor($seconds_time - ($hours * 3600) - ($minutes * 60)), 2, "0", STR_PAD_LEFT);
            return "$hours:$minutes:$seconds";
        }
    }
}

if (!function_exists('write_constant')){
    function write_constant($key,$value){
        config([$key => $value]);
        $text = '<?php return ' . var_export(config('constants'), true) . ';';
        file_put_contents(config_path('constants.php'), $text);
    }
}

