<?php

namespace App\Services;

use App\Models\Driver;
use App\Models\Order;
use App\Models\OrderStatusTimestamp;
use App\Notifications\OrderAssigned;
use App\Notifications\PushNotification;
use Notification;
use DB;
class OrderService{

    public function assignDriverToOrder($order, $driverId){
        $driver = \App\Models\Driver::find($driverId);
        //Free old driver to take new orders
        if ($order->driver_id != null) {
            $oldDriver = Driver::find($order->driver_id);
            $this->freeDriver($oldDriver);
            Notification::send($oldDriver, new PushNotification(['title'=>'Order Unassigned','description'=>'You have been unassigned from order #'.$order->id]));
        }else{
            //No driver assigned to order
            //Proceed ahead
        }
        //Add assign timestamp
        OrderStatusTimestamp::create([
            'order_id' => $order->id,
            'status' => "ASSIGNED",
            'comment' => ''
        ]);

        //assign driver to order
        $order->order_status = "ASSIGNED";
        $order->driver_id = $driverId;
        $order->save();
        Driver::where('id',$driverId)->update(['is_busy'=>1]);
        //   $driver->notify(new \App\Notifications\OrderAssigned("Order #" . $order->id . " has been assigned to you.", $order,true));
          $this->send_push($order->driver,'Order Assigned',"Order #".$order->id." has been assigned to you.",$order);
        // Notification::send($driver,new OrderAssigned("Order #".$order->id." has been assigned to you.",$order,false));
        // Notification::send($order->user,new PushNotification("Order #".$order->id." has been assigned to you.",$order,false));
    }

    public function pingAssignDriverToOrder($order, $driverId){
        $driver = \App\Models\Driver::find($driverId);
        //Add assign timestamp
        OrderStatusTimestamp::create([
            'order_id' => $order->id,
            'status' => "PING",
            'comment' => ''
        ]);
        //   $res =  $driver->notify(new \App\Notifications\OrderAssigned("Order #" . $order->id . " has been assigned to you.", $order,true));
         
          $this->send_push($order->driver,'Order Assigned',"Order #".$order->id." has been assigned to you.");
        // Notification::send($driver,new OrderAssigned("Order #".$order->id." has been assigned to you.",$order,false));
        // Notification::send($order->user,new PushNotification("Order #".$order->id." has been assigned to you.",$order,false));
    }

    public function freeDriver($driver){
        try{
            $count = Order::where('driver_id',$driver->id)->whereIn('order_status',['ASSIGNED','PREPARED','REACHED','PICKEDUP'])->count();
            if ($count > 0){
                Driver::where('id',$driver->id)->update(['is_busy'=>0]);
            }else{
                Notification::send($driver, new PushNotification(['title'=>$count. ' Order Pending','description'=>'Please deliver all the order to receive new order']));
            }
        }catch (\Exception $e){
            return false;
        }

    }

    public function changeOrderStatus($order, $newStatus){
        switch ($newStatus){
            case 'PLACED':
                $order->order_status = 'PLACED';
                $order->driver_id = null;
                $order->save();
                Notification::send($order->user, new PushNotification(['title'=>'#'.$order->id.' Order Placed','description'=>'Your order '.$order->id.' has been placed']));
                break;
            case 'ACCEPTED':
                $order->order_status = 'ACCEPTED';
                $order->save();
                Notification::send($order->user, new PushNotification(['title'=>'#'.$order->id.' Order Accepted','description'=>'Your order '.$order->id.' has been accepted by merchant. We will assign driver to it shortly.']));
                break;
            case 'ASSIGNED':
                $order->order_status = 'ASSIGNED';
                $order->save();
                if ($order->driver_id != null){
                    $driver = Driver::find($order->driver_id);
                    Notification::send($order->user, new PushNotification(['title'=>'#'.$order->id.' Order Assigned','description'=>$driver->name.' has been assigned to your order '.$order->id]));
                }
                break;
            case 'PREPARED':
                $order->order_status = 'PREPARED';
                $order->save();
                Notification::send($order->user, new PushNotification(['title'=>'#'.$order->id.' Order Prepared','description'=>'Your order '.$order->id.' has been prepared. It will be picked up very soon.']));
                Notification::send($order->driver, new PushNotification(['title'=>'#'.$order->id.' Order Prepared','description'=>'#'.$order->id.' has been prepared. Please pick and deliver it.']));
                break;
            case 'REACHED':
                $order->order_status = 'REACHED';
                $order->save();
                 Notification::send($order->driver, new PushNotification(['title'=>'#'.$order->id.' Order Reached','description'=>'#'.$order->id.' has been reached']));
              
                //Notification::send($order->user, new PushNotification(['title'=>'#'.$order->id.' Order Prepared','description'=>'Your order '.$order->id.' has been prepared. It will be picked up very soon.']));
                break;
            case 'PICKEDUP':
                $order->order_status = 'PICKEDUP';
                $order->save();
                $driver = Driver::find($order->driver_id);
                if ($driver!=null){
                     Notification::send($order->driver, new PushNotification(['title'=>'#'.$order->id.' Order Picked Up','description'=>'#'.$order->id.' has picked up ']));
              
                    Notification::send($order->user, new PushNotification(['title'=>'#'.$order->id.' Order Picked Up','description'=> $driver->name.' has picked up your order']));
                }
                break;
            case 'DELIVERED':
                //Free the driver
                $this->freeDriver($order->driver);
                //No need to change order status as it is being performed in the controller
                //Send a notification to the customer
                Notification::send($order->user, new PushNotification(['title'=>'Order Delivered','description'=>'Your order #'.$order->id.' has been delivered']));
                //Send a notification to the driver
                Notification::send($order->driver, new PushNotification(['title'=>'Order Delivered','description'=>'Order #'.$order->id.' is delivered successfully']));
                //No need to calculate the earnings as there is a listener that will do the calculation and amount distribution
                break;
            case 'CANCELLED':
            case 'USERCANCELLED':
                //Free the driver
                $this->freeDriver($order->driver);
                //No need to change order status as it is being performed in the controller
                //Send a notification to the customer
                Notification::send($order->user, new PushNotification(['title'=>'Order Cancelled','description'=>'Your order #'.$order->id.' has been cancelled']));
                //Send a notification to the driver
                Notification::send($order->driver, new PushNotification(['title'=>'Order Cancelled','description'=>'Order #'.$order->id.' has been cancelled']));
                //No need to calculate the earnings as there is a listener that will do the calculation and amount distribution
                break;
            default:
                return false;
        }
        OrderStatusTimestamp::create([
            'order_id' => $order->id,
            'status' => $newStatus,
            'comment' => ''
        ]);
    }
    
     public function send_push($user,$title,$desc,$order=null) {
        
        
         $title = $title;
         $body = $desc;
        //  $imageurl = '';
        // $tokens = array();
        // foreach($alltoken as $token){
        //     array_push($tokens,$token['fcm_token']);
        // }
   
          $fcmkey =  DB::table('settings')->where('key_name','fcm_key')->first();
            
        // $result = array();
        // $chunks = array_chunk($tokens,999);
       
        // foreach($chunks as $chunk){
               $chunk = $user->fcm_token;
          
            $url = 'https://fcm.googleapis.com/fcm/send'; 
            if($order != null)
            {
                $dataArr = array('click_action' => 'FLUTTER_NOTIFICATION_CLICK','type' => 'order_assigned',
            'is_auto_assigned' => true,'id'=> $order->id); 
            }
            else
            $dataArr = array('click_action' => 'FLUTTER_NOTIFICATION_CLICK'); 
           
    
            $notification = array('title' =>$title, 'body' => $body,  'sound' =>'disabled' , 'default', 'badge' => '1');
    
            $arrayToSend = array('to' =>$chunk , 'notification' => $notification, 'data' => $dataArr, 'priority'=>'high' );
    
            $tempArr = $arrayToSend;
    
            $fields = json_encode($arrayToSend);
           
            
            $headers = array (
                'Authorization: key=' . $fcmkey->key_value,
                'Content-Type: application/json'
    
            );
    
            //Register notification in database
            $ch = curl_init ();
    
            curl_setopt ( $ch, CURLOPT_URL, $url );
    
            curl_setopt ( $ch, CURLOPT_POST, true );
    
            curl_setopt ( $ch, CURLOPT_HTTPHEADER, $headers );
    
            curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );
    
            curl_setopt ( $ch, CURLOPT_POSTFIELDS, $fields );
            $result[] = curl_exec ( $ch );
   
            //   dd($result);
            curl_close ( $ch );
           return 1;
        // }
        
    }



}
