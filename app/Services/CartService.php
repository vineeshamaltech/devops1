<?php

namespace App\Services;

use App\Models\Cart;
use App\Models\VehicleType;

class CartService{

    public function calculateDistance($from_lat,$from_lang,$to_lat,$to_long){
        $url = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=".$from_lat.",".$from_lang."&destinations=".$to_lat.",".$to_long."&mode=driving&language=en-US&key=".setting('google_map_api_key');
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $response = curl_exec($ch);
        curl_close($ch);
        try {
            $response_a = json_decode($response, true);
            $dist = $response_a['rows'][0]['elements'][0]['distance']['value'];
            $time = $response_a['rows'][0]['elements'][0]['duration']['value'];
            $distKm = $dist / 1000;
            $arr = array('distance' => $distKm, 'time' => $time);
        }catch (\Exception $e){
            $arr = array('distance' => 0, 'time' => 0);
        }
        return $arr;
    }

    public function calculateCabFare($cart_id){
        $cart = Cart::find($cart_id);
        if (empty($cart) || $cart->service_category->service_type_id != config('constants.cab_service_type_id')) {
            return 0;
        }
        $response = $this->calculateDistance($cart->pickup_address['latitude'], $cart->pickup_address['longitude'], $cart->delivery_address['latitude'], $cart->delivery_address['longitude']);
        $distance = $response['distance'];
        $time = $response['time'];
        $vehicle = VehicleType::find($cart->vehicle_id) ?? VehicleType::first();
        if (empty($vehicle)) {
            return 0;
        }
        return $this->calculateFare($vehicle,$distance);
    }

    public function calculateFare($vehicle,$km_to_travel){
        $baseDistance = $vehicle->base_km;
        $basePrice = $vehicle->base_price;
        $pricePerKm = $vehicle->price_km;
        return $basePrice + max(($km_to_travel - $baseDistance), 0) * $pricePerKm;
    }

    public function getUserCart(){
        return Cart::with('service_category')->where('user_id', auth()->user()->id)->first();
    }
}
