<?php

namespace App\Services;

use App\Models\Admin;
use App\Models\DeliveryFee;
use App\Models\Driver;
use App\Models\HomeServiceOrderStatusTimestamp;
use App\Models\Order;
use App\Models\ServiceOrder;
use App\Models\Store;
use App\Models\Vendor;
use App\Notifications\OrderAssigned;
use App\Notifications\PushNotification;
use Notification;

class HomeOrderService{

    public function assignDriverToHomeServiceOrder($order, $driverId){
        $driver = \App\Models\Driver::find($driverId);
        //Free old driver to take new orders
        if ($order->driver_id != null) {
            $oldDriver = Driver::find($order->driver_id);
            Notification::send($oldDriver, new PushNotification(['title'=>'Order Unassigned','description'=>'You have been unassigned from order #'.$order->id]));
        }else{
            //No driver assigned to order
            //Proceed ahead
        }
        //Add assign timestamp
        HomeServiceOrderStatusTimestamp::create([
            'order_id' => $order->id,
            'status' => "ASSIGNED",
            'comment' => ''
        ]);

        //assign driver to order
        $order->order_status = "ASSIGNED";
        $order->driver_id = $driverId;
        $order->save();
        Driver::where('id',$driverId)->update(['is_busy'=>1]);
        Notification::send($driver,new OrderAssigned("Order #".$order->id." has been assigned to you.",$order,false));
        Notification::send($order->user,new PushNotification("Order #".$order->id." has been assigned to you.",$order,false));
    }

    public function changeOrderStatus($order, $newStatus){
        switch ($newStatus){
            case 'RECEIVED':
                $order->order_status = 'RECEIVED';
                $order->driver_id = null;
                $order->save();
                Notification::send($order->user, new PushNotification(['title'=>'#'.$order->id.' Order Placed','description'=>'Your order '.$order->id.' has been placed']));
                break;
            case 'ASSIGNED':
                $order->order_status = 'ASSIGNED';
                $order->save();
                if ($order->driver_id != null){
                    $driver = Driver::find($order->driver_id);
                    Notification::send($order->user, new PushNotification(['title'=>'#'.$order->id.' Service Assigned','description'=>$driver->name.' has been assigned to your order '.$order->id]));
                }
                break;
            case 'ACCEPTED':
                $order->order_status = 'ACCEPTED';
                $order->save();
                Notification::send($order->user, new PushNotification(['title'=>'#'.$order->id.' Order Accepted','description'=>'Your order '.$order->id.' has been accepted by our service provider.']));
                break;
            case 'REACHED':
                $order->order_status = 'REACHED';
                $order->save();
                if ($order->driver_id != null){
                    $driver = Driver::find($order->driver_id);
                    Notification::send($order->user, new PushNotification(['title'=> 'Driver Reached at your doorstep','description'=>$driver->name.' has reached your doorstep.']));
                }
                break;
            case 'STARTED':
                $order->order_status = 'STARTED';
                $order->save();
                Notification::send($order->user, new PushNotification(['title'=>'Service Started','description'=>'Our service provider has started your order.']));
                Notification::send($order->driver, new PushNotification(['title'=>'#'.$order->id.' Service Started','description'=>'Service started. Please complete it.']));
                break;
            case 'FINISHED':
                $order->order_status = 'FINISHED';
                $order->save();
                $driver = Driver::find($order->driver_id);
                if ($driver!=null){
                    Notification::send($order->user, new PushNotification(['title'=>'#'.$order->id.' Service Picked Up','description'=> $driver->name.' has picked up your order']));
                }
                Notification::send($order->user, new PushNotification(['title'=>'#'.$order->id.' Service Finished','description'=> $driver->name.' has finished your order. Please rate your experience.']));
                break;
            case 'COMPLETED':
                //Free the driver
                //$this->freeDriver($order->driver);
                $order->order_status = 'COMPLETED';
                $order->save();
                //No need to change order status as it is being performed in the controller
                //Send a notification to the customer
                Notification::send($order->user, new PushNotification(['title'=>'Service Completed','description'=>'Your order #'.$order->id.' has been delivered']));
                //Send a notification to the driver
                Notification::send($order->driver, new PushNotification(['title'=>'Service Completed','description'=>'Order #'.$order->id.' is delivered successfully']));
                //No need to calculate the earnings as there is a listener that will do the calculation and amount distribution
                break;
            case 'CANCELLED':
                $order->driver_id = null;
                $order->save();
                //Free the driver
                //$this->freeDriver($order->driver);
                //No need to change order status as it is being performed in the controller
                //Send a notification to the customer
                Notification::send($order->user, new PushNotification(['title'=>'Order Cancelled','description'=>'Your order #'.$order->id.' has been cancelled']));
                //Send a notification to the driver
                Notification::send($order->driver, new PushNotification(['title'=>'Order Cancelled','description'=>'Order #'.$order->id.' has been cancelled']));
                //No need to calculate the earnings as there is a listener that will do the calculation and amount distribution
                break;
            default:
                return false;
        }
        HomeServiceOrderStatusTimestamp::create([
            'order_id' => $order->id,
            'status' => $newStatus,
            'comment' => ''
        ]);
    }

    public static function calculateEarnings($order_id)
    {
        try {
            $order = ServiceOrder::find($order_id);
            $coupon = $order->coupon_data;
            $provider = Driver::find($order->driver_id);
            $vendor = Admin::find($order->vendor_id);
            $vendor_commission = setting('home_service_vendor_commission',0);
            $admin_commission = setting('home_service_admin_commission',0);

            $admin_discount = 0;

            if ($coupon != null) {
                $admin_discount = $order->discount_value;
            }

            //$deliveryfee = DeliveryFee::where('delivery_fee', $order->delivery_fee)->first();
            $admin_commission =(  $order->total_price * ($admin_commission / 100) ) +$order->tax  - $order->tip ;

            //Calculate Vendor Earnings
            $vendor_commission = ($order->total_price * ($vendor_commission / 100))- $order->tip ;

            //Calculate Driver Earnings
            $provider_commission = $order->grand_total - $admin_commission - $vendor_commission;

            //Calculate Driver Earnings
            $order->admin_commission = $admin_commission - $admin_discount;
            $order->vendor_commission = $vendor_commission;
            $order->driver_commission = $provider_commission;
            $order->save();
        } catch (\Exception $e) {
            $order = ServiceOrder::find($order_id);
            $order->admin_commission = 0;
            $order->vendor_comission = 0;
            $order->driver_commission = 0;
            $order->save();
        }
    }

}
