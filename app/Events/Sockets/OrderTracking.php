<?php

namespace App\Events\Sockets;

use App\Models\Store;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class OrderTracking implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $order;
    public $orderId;
    public $adminId = 0;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($orderId,$order)
    {
        $this->order = $order;
        $this->orderId = $orderId;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        try{
            $this->adminId = Store::find($this->order->store_id)->admin_id;
        }catch (\Exception $e){
            return new PrivateChannel('channel-name');
        }
        return [
            new Channel('order.admin.'.$this->adminId),
            new Channel('order.'.$this->orderId)
        ];
    }

    public function broadcastAs(){
        return 'data';
    }
}
