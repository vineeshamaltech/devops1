<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Queue\SerializesModels;

class ExportGenerated extends Mailable
{
    use Queueable, SerializesModels;
    private $file;
    private $file2;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($file,$file2)
    {
        $this->file = $file;
        $this->file2 = $file2;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Export file generated successfully')
            ->attach($this->file)
            ->attach($this->file2)
            ->from(env('MAIL_FROM_ADDRESS'), env('MAIL_FROM_NAME'))
            ->html((new MailMessage)
                ->line('The export report you request has been generated successfully and is attached to this email.')
                ->line('Thank you for using our application!')
                ->render()
            );
    }
}
