<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Queue\SerializesModels;

class FileImported extends Mailable
{
    use Queueable, SerializesModels;
    private $success_file;
    private $error_file;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($success_file,$error_file)
    {
        $this->success_file = $success_file;
        $this->error_file = $error_file;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Your File has been imported')
            ->attach($this->error_file)
            ->attach($this->success_file)
            ->from(env('MAIL_FROM_ADDRESS'), env('MAIL_FROM_NAME'))
            ->html((new MailMessage)
                ->line('The file you submitted for import has been processed. The results are attached to this email.')
                ->line('First attachment contains the records that were successfully imported. Second attachment contains the records that were not imported due to errors.')
                ->line('Thank you for using our application!')
                ->render()
            );
    }
}
