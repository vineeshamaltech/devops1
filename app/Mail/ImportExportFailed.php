<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Queue\SerializesModels;

class ImportExportFailed extends Mailable
{
    use Queueable, SerializesModels;
    private $mail_subject;
    private $message;
    private $file;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($mail_subject,$message,$file = null)
    {
        $this->mail_subject = $mail_subject;
        $this->message = $message;
        $this->file = $file;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject($this->mail_subject)
            ->attach($this->file)
            ->from(env('MAIL_FROM_ADDRESS'), env('MAIL_FROM_NAME'))
            ->html((new MailMessage)
                ->line($this->message)
                ->line('Thank you for using our application!')
                ->render()
            );
    }
}
