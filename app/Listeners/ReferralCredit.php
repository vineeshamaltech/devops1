<?php

namespace App\Listeners;

use App\Events\OrderChanged;
use App\Helpers\WalletHelper;
use App\Models\Order;
use App\Models\User;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class ReferralCredit
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  \App\Events\OrderChanged  $event
     * @return void
     */
    public function handle(OrderChanged $event)
    {
        $order = $event->order;
       try {
            if ($order->order_status == 'DELIVERED') {
                //dd($order);
                //Check for referral credits
                $user = $order->user;
               //;
                if ($user->ref_by != null && $user->ref_by != 0 && $user->ref_used == 0){
                    $ref_by = User::find($user->ref_by);
                    //dd($ref_by);
                    //User is refereed by someone and has not been credited yet
                    $referral_amount = setting('referal_amount',20);
                    $referral_order_count = setting('referal_order_count',999999);
                    $totalOrderCount = Order::where('user_id', $user->id)->where('order_status', 'DELIVERED')->count();
                    // echo $totalOrderCount;
                    // echo "<br>";
                    // echo $referral_order_count;
                    // die();
                    if ($totalOrderCount >= $referral_order_count){
                        //User has completed the required order count
                        //Add credits to user's wallet
                        WalletHelper::creditUserWallet($user->id,$referral_amount,'Referral amount credited. You were referred by '.$ref_by->name);
                        WalletHelper::creditUserWallet($ref_by->id,$referral_amount,'Referral amount for referring '.$user->name);
                        User::where('id', $user->id)->update(['ref_used' => 1]);
                    }
                }
                //dd($user);
            }
        }catch (\Exception $e) {

        }
    }
}
