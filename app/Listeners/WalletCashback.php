<?php

namespace App\Listeners;

use App\Events\OrderChanged;
use App\Helpers\WalletHelper;
use App\Models\CouponUsage;

class WalletCashback
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  \App\Events\OrderChanged  $event
     * @return void
     */
    public function handle(OrderChanged $event)
    {
        $order = $event->order;
        try{
            if ($order->status == 'DELIVERED') {
                $coupon_data = $order->coupon_data;
                $user_usage_count = CouponUsage::where('coupon_id', $order->id)->where('user_id', $order->user_id)->count();
                if($coupon_data->discount_type == "WALLET CASHBACK"){
                    if ($user_usage_count < $coupon_data->max_use_per_user){
                        $total_order_value = $order->total_price;
                        $cashback_amount = $total_order_value * ($coupon_data->discount_value/100);
                        if ($cashback_amount > $coupon_data->max_discount_value) {
                            $cashback_amount = $coupon_data->max_discount_value;
                            CouponUsage::create([
                                'coupon_id' => $order->id,
                                'user_id' => $order->user_id,
                                'coupon_code' => $coupon_data->code,
                                'discount_value' => $cashback_amount
                            ]);
                            WalletHelper::creditUserWallet($order->user_id, $cashback_amount,'Cashback for order #'.$order->id);
                        }
                    }
                }
            }
        }catch (\Exception $e){
            //Error occurred while processing the cashback
        }
    }
}
