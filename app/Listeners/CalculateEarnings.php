<?php

namespace App\Listeners;

use App\Events\OrderChanged;
use App\Helpers\OrderHelper;
use App\Models\Admin;
use App\Models\Driver;
use App\Models\Earning;
use App\Models\ServiceCategory;
use App\Helpers\WalletHelper;
use App\Models\Order;
use App\Models\Store;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class CalculateEarnings
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(){
    }

    /**
     * Handle the event.
     *
     * @param  \App\Events\OrderChanged  $event
     * @return void
     */
    public function handle(OrderChanged $event)
    {
        if ($event->order->order_status == 'DELIVERED'){
            if (Earning::where('order_id', $event->order->id)->count() == 0) {
                try {
                    //Get store
                    $store = $event->order->store;
                    //$store_commission = $store->commission;
                    //Calculate admin Earnings
                    $admin_commission = $event->order->admin_commission;
                    $type = ServiceCategory::where('id',$event->order->service_category_id)->first();

                    Earning::create([
                        'payment_id' => $event->order->payment_id,
                        'order_id' => $event->order->id,
                        'entity_type' => Admin::class,
                        'entity_id' => 1,
                        'service_type' => $event->order->service_category_id ,
                        'amount' => $admin_commission,
                        'type' => "order",
                        'service' => $type->name,
                        'remarks' => 'Admin Commission for Order #' . $event->order->id,
                        'is_settled' => false
                    ]);
                    //Calculate Vendor Earnings
                    if ($store !=null){
                        $vendor_commission = $event->order->store_commission;
                        Earning::create([
                            'payment_id' => $event->order->payment_id,
                            'order_id' => $event->order->id,
                            'entity_type' => Store::class,
                            'entity_id' => $store->id,
                            'amount' => $vendor_commission,
                            'type' => "order",
                        'service' => $type->name,
                            'remarks' => 'Store earning for Order #' . $event->order->id,
                            'is_settled' => false
                        ]);
                    }
                    //Calculate Driver Earnings
                    $driver_commission = $event->order->driver_commission;
                    if($event->order->payment_method == 'COD') {
                        $dedAmount = $event->order->grand_total - $driver_commission;
                        WalletHelper::debitDriverWallet($event->order->driver_id,$dedAmount,"Debit for COD Order #" . $event->order->id);
                    }
                    Earning::create([
                        'payment_id' => $event->order->payment_id,
                        'order_id' => $event->order->id,
                        'entity_type' => Driver::class,
                        'entity_id' => $event->order->driver_id,
                        'amount' => $driver_commission,
                        'type' => "order",
                        'service' => $type->name,
                        'remarks' => 'Driver earning for Order #' . $event->order->id,
                        'is_settled' => false
                    ]);
                } catch (\Exception $e) {
                    throw $e;
                }
            }
            Order::where('id', $event->order->id)->update(['payment_status' => 'SUCCESS']);
        }
    }
}
