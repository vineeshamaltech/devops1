<?php

namespace App\Listeners;

use App\Helpers\OrderHelper;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class AssignDriver
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        if ($event->order->order_status == 'ACCEPTED'){
            OrderHelper::assignDriver($event->order->id);
        }
    }
}
