<?php

namespace App\Listeners;

use App\Helpers\WalletHelper;
use App\Models\Admin;
use App\Models\Driver;
use App\Models\Earning;
use App\Models\ServiceOrder;

class DistributeServiceOrderEarnings
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        if ($event->order->order_status == 'COMPLETED'){
            if (Earning::where('order_id', $event->order->id)->count() == 0) {
                try {
                    $vendor = Admin::find($event->order->vendor_id);
                    $admin_commission = $event->order->admin_commission;
                    Earning::create([
                        'payment_id' => $event->order->payment_id,
                        'service_order_id' => $event->order->id,
                        'entity_type' => Admin::class,
                        'entity_id' => 1,
                        'service' =>'SERVICE ORDER',
                        'amount' => $admin_commission,
                        'type' => 'SERVICE ORDER',
                        'remarks' => 'Admin Commission for Service Order #' . $event->order->id,
                        'is_settled' => false
                    ]);
                    //Calculate Vendor Earnings
                    if ($vendor !=null){
                        $vendor_commission = $event->order->vendor_commission;
                        Earning::create([
                            'payment_id' => $event->order->payment_id,
                            'service_order_id' => $event->order->id,
                            'entity_type' => Admin::class,
                            'entity_id' => $vendor->id,
                            'amount' => $vendor_commission,
                            'service' =>'SERVICE ORDER',
                            'type' => 'SERVICE ORDER',
                            'remarks' => 'Vendor earning for Service Order #' . $event->order->id,
                            'is_settled' => false
                        ]);
                    }
                    //Calculate Driver Earnings
                    $driver_commission = $event->order->driver_commission;
                    if($event->order->payment_method == 'COD') {
                        $dedAmount = $event->order->grand_total - $driver_commission;
                        WalletHelper::debitDriverWallet($event->order->driver_id,$dedAmount,"Debit for COD Service ID #" . $event->order->id);
                    }
                    Earning::create([
                        'payment_id' => $event->order->payment_id,
                        'service_order_id' => $event->order->id,
                        'entity_type' => Driver::class,
                        'entity_id' => $event->order->driver_id,
                        'amount' => $driver_commission,
                        'service' =>'SERVICE ORDER',
                        'type' => 'SERVICE ORDER',
                        'remarks' => 'Driver earning for Service Order #' . $event->order->id,
                        'is_settled' => false
                    ]);
                } catch (\Exception $e) {
                    //throw $e;
                }
            }
            ServiceOrder::where('id', $event->order->id)->update(['payment_status' => 'SUCCESS']);
        }
    }
}
