<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Spatie\Permission\Models\Permission;

class CreatePermissions extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'permission:make {name} {--guard=} {--rollback=1}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Make crud permissions';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $name = $this->argument('name');
        $guard = $this->option('guard');
        $rollback = $this->option('rollback');
        if ($guard == null) {
            $guard = 'web';
        }
        if($rollback != null){
            Permission::create(['name' => $name.'.index', 'guard_name' => $guard]);
            Permission::create(['name' => $name.'.create', 'guard_name' => $guard]);
            Permission::create(['name' => $name.'.view', 'guard_name' => $guard]);
            Permission::create(['name' => $name.'.edit', 'guard_name' => $guard]);
            Permission::create(['name' => $name.'.delete', 'guard_name' => $guard]);
            $this->info('Permission created successfully');
        }else{
            Permission::findByName($name.'.index')->delete();
            Permission::findByName($name.'.create')->delete();
            Permission::findByName($name.'.view')->delete();
            Permission::findByName($name.'.edit')->delete();
            Permission::findByName($name.'.delete')->delete();
            $this->info('Permission deleted successfully');
        }
        return Command::SUCCESS;
    }
}
