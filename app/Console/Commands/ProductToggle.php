<?php

namespace App\Console\Commands;

use App\Models\Product;
use Illuminate\Console\Command;

class ProductToggle extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'product:toggle';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Open/Close the product based on the current time';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
      //  Product::where('active',true)->update(['active'=>false]);
      //  $count = Product::whereRaw('CURRENT_TIME < closing_time && CURRENT_TIME > opening_time')->update(['active'=>true]);
      //  $this->info($count.' Product activated');
      //  return 0;
    }
}
