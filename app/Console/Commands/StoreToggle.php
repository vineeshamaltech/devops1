<?php

namespace App\Console\Commands;

use App\Models\Store;
use App\Models\StoreTiming;
use Illuminate\Console\Command;

class StoreToggle extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'store:toggle';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Open/Close the store based on the current time';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        // $day = date('l');
        // Store::where('active',true)->update(['is_open'=>false]);
        // $count = Store::whereHas('store_timings', function ($query) use ($day) {
        //     $query->where('day', 'like', $day);
        //     $query->whereRaw('CURRENT_TIME < close && CURRENT_TIME > open');
        // })->update(['is_open'=>true]);
        // $this->info($count.' Store activated');

       // $day = date('l');
        //$in_time = false;
        //Store::where('active',true)->update(['is_open'=>false]);
      //  $stores = Store::all();
       // foreach($stores as $s){
         //   $storeTiming = StoreTiming::where('store_id', $s->id)
          //  ->where('day',$day)->get();
          //  if(count($storeTiming)>0){
              //  foreach($storeTiming as $store){
                //    $from = $store->open;
                  //  $to = $store->close;
                    // $chk = \Helpers::check_in_time($from,$to); 
                  //   $current_time = now()->format('H:i:s');
                   //     if ($current_time > $from && $current_time < $to)
                      //  {
                      //      $chk = true;
                       //       Store::where('id',$s->id)->update(['is_open'=>true]);
                      //  }else{
                         //   $chk = false;
                            // Store::where('id',$s->id)->update(['is_open'=>false]);
                       // }
                  //  // if($chk){
                  //  //     $in_time = true;
                //    // }
                //}
             //   // if($in_time){
              //  //     Store::where('id',$s->id)->update(['is_open'=>true]);
              //  // }
               // // else
              //  //  Store::where('id',$s->id)->update(['is_open'=>false]);
          //  }
      //  }
        
      //  return 0;
    }
}
