<?php

namespace App\Console\Commands;

use App\Models\Driver;
use Illuminate\Console\Command;

class DriverBootcash extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'driver:bootcash';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Disable drivers who have bootcash limit more than the set limit';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $drivers = Driver::where('is_verified')->get();
        $bootcash_limit = setting('driver_bootcash_limit',0);
        foreach ($drivers as $driver) {
            if ($driver->cash_in_hand >= $bootcash_limit) {
                $driver->cash_in_hand_overflow = 1;

            }else{
                $driver->cash_in_hand_overflow = 0;
            }
            $driver->save();
        }
        return 0;
    }
}
