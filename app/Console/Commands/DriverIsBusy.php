<?php

namespace App\Console\Commands;

use App\Models\Driver;
use App\Models\Order;
use Illuminate\Console\Command;

class DriverIsBusy extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'driver:isbusy';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $drivers = Driver::where('is_busy',1)->get();
        $busycount = 0;
        $freecount = 0;
        foreach ($drivers as $driver) {
            $orders = Order::where('driver_id',$driver->id)->whereNotIn('order_status',['PLACED', 'ACCEPTED','DRIVERREJECTED','DELIVERED','USERCANCELLED','CANCELLED'])->count();
            if($orders>0){
                $driver->is_busy = 1;
                $busycount++;
            }else{
                $driver->is_busy = 0;
                $freecount++;
            }
            $driver->save();
        }
        $this->info('Total Drivers: '.count($drivers));
        $this->info('Busy Drivers: '.$busycount);
        $this->info('Free Drivers: '.$freecount);
        return 0;
    }
}
