<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class TempDumper extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'tempdata:dump';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        //Delete all temp data
        //Empty websockets table
        DB::table('websockets_statistics_entries')->truncate();
        $this->info('Websocket Logs Cleared');
        //Empty notification logs table
        DB::table('notification_logs')->truncate();
        $this->info('Notification Logs Cleared');
        return 0;
    }
}
