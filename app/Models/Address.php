<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class Address
 * @package App\Models
 * @version December 6, 2021, 11:36 am UTC
 *
 * @property integer $user_id
 * @property string $name
 * @property string $phone
 * @property string $address
 * @property number $latitude
 * @property number $longitude
 * @property string $description
 * @property boolean $is_default
 */
class Address extends Model
{

    public $table = 'addresses';

    public $fillable = [
        'user_id',
        'name',
        'phone',
        'house_no',
        'building',
        'address',
        'latitude',
        'longitude',
        'type',
        'description',
        'is_default'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'user_id' => 'integer',
        'name' => 'string',
        'phone' => 'string',
        'address' => 'string',
        'house_no' => 'string',
        'building' => 'string',
        'type' => 'string',
        'latitude' => 'double',
        'longitude' => 'double',
        'description' => 'string',
        'is_default' => 'boolean'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
 public static $rules = [
        'user_id' => 'nullable',
        'name' => 'required|regex:/^[\pL\s\-]+$/u',
       'phone' => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|min:10',
        'address' => 'required',
        'latitude' => 'required|numeric',
        'longitude' => 'required|numeric',
        'house_no' => 'required',
        'building' => 'required',
        'type' => 'required',
        'description' => 'required',
        'is_default' => 'required'
    ];

    public static $updaterules = [
        'name' => 'nullable',
        'phone' => 'nullable',
        'address' => 'nullable',
        'latitude' => 'nullable|numeric',
        'longitude' => 'nullable|numeric',
        'house_no' => 'nullable',
        'building' => 'nullable',
        'type' => 'nullable',
        'description' => 'nullable',
        'is_default' => 'nullable'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function user()
    {
        return $this->belongsTo(User::class);
    }

}
