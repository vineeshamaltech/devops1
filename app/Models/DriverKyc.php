<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class VendorKyc
 * @package App\Models
 * @version December 2, 2021, 6:33 am UTC
 *
 * @property string $vedor_id
 * @property string $doc_type
 * @property string $photo
 * @property string $status
 */
class DriverKyc extends Model
{

    public $table = 'driver_kycs';

    public $fillable = [
        'driver_id',
        'doc_type',
        'photo',
        'status'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'driver_id' => 'string',
        'doc_type' => 'string',
        'photo' => 'string',
        'status' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'driver_id' => 'nullable',
        'doc_type' => 'nullable',
        'photo' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        'status' => 'nullable'
    ];


    public function driver()
    {
        return $this->belongsTo(Driver::class);
    }


}
