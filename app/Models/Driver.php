<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Spatie\Permission\Traits\HasRoles;

/**
 * Class Driver
 * @package App\Models
 * @version December 6, 2021, 11:39 am UTC
 *
 * @property string $name
 * @property string $mobile
 * @property string $email
 * @property string $password
 * @property number $cash_in_hand
 * @property string $vehicle_type
 * @property string $vehicle_reg_no
 * @property string $vehicle_color
 * @property string $vehicle_img
 * @property string $dl_photo
 * @property string $aadhar_photo
 * @property integer $service_type_id
 * @property string $bank_name
 * @property string $account_no
 * @property string $ifsc_code
 * @property string $branch_name
 * @property boolean $active
 * @property boolean $is_verified
 */
class Driver extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable, HasRoles;

    public $table = 'drivers';

    public $fillable = [
        'name',
        'country_code',
        'mobile',
        'email',
        'password',
        'cash_in_hand',
        'vehicle_type_id',
        'vehicle_type',
        'vehicle_reg_no',
        'vehicle_color',
        'vehicle_img',
        'dl_photo',
        'aadhar_photo',
        'service_type_id',
        'latitude',
        'longitude',
        'bearing',
        'bank_name',
        'account_no',
        'ifsc_code',
        'branch_name',
        'incentive',
        'emergency_contact_name',
        'emergency_contact_no',
        'emergency_contact_relation',
        'aadhar_photo_back',
        'profile_img',
        'secondary_mobile',
        'rating',
        'dob',
        'languages',
        'active',
        'is_verified',
        'is_busy',
        'home_service_enabled',
        'zones'
    ];

    protected $hidden = [
        'password',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'country_code' => 'string',
        'mobile' => 'string',
        'email' => 'string',
        'password' => 'string',
        'vehicle_type_id' => 'integer',
        'cash_in_hand' => 'double',
        'vehicle_type' => 'string',
        'vehicle_reg_no' => 'string',
        'vehicle_color' => 'string',
        'vehicle_img' => 'string',
        'dl_photo' => 'string',
        'aadhar_photo' => 'string',
        'service_type_id' => 'integer',
        'bank_name' => 'string',
        'account_no' => 'string',
        'ifsc_code' => 'string',
        'branch_name' => 'string',
        'active' => 'boolean',
        'is_verified' => 'boolean',
        'on_duty' => 'integer',
        'bearing' => 'double',
        'emergency_contact_name' => 'string',
        'emergency_contact_no' => 'string',
        'emergency_contact_relation' => 'string',
        'aadhar_photo_back' => 'string',
        'profile_img' => 'string',
        'secondary_mobile' => 'string',
        'dob' => 'string',
        'languages' => 'string',
        'is_busy' => 'boolean',
        'home_service_enabled' => 'boolean',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
         'name' => 'required|regex:/^[\pL\s\-]+$/u',
        'country_code' => 'nullable',
        'mobile' => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|min:10',
        'email' => 'required|regex:/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/ix',
        'password' => 'nullable',
        'cash_in_hand' => 'required',
        'vehicle_type_id' => 'nullable',
        'vehicle_type' => 'required',
        'vehicle_reg_no' => 'required|',
        'vehicle_color' => 'required|',
        'vehicle_img' => 'required',
        'dl_photo' => 'required',
        'aadhar_photo' => 'required',
        'service_type_id' => 'required',
        'bank_name' => 'required|regex:/^[\pL\s\-]+$/u',
        'account_no' => 'required',
        'ifsc_code' => 'required',
        'branch_name' => 'required|regex:/^[\pL\s\-]+$/u',
        'active' => 'nullable',
        'on_duty' => 'integer',
        'is_verified' => 'nullable',
        'emergency_contact_name' => 'nullable',
        'emergency_contact_no' => 'nullable',
        'emergency_contact_relation' => 'nullable',
        'aadhar_photo_back' => 'nullable',
        'profile_img' => 'nullable',
        'secondary_mobile' => 'nullable',
        'dob' => 'nullable',
        'languages' => 'nullable'
    ];

    public static $updateRules = [
        'name' => 'nullable',
        'country_code' => 'nullable',
        'mobile' => 'nullable',
        'email' => 'nullable',
        'password' => 'nullable',
        'vehicle_type_id' => 'nullable',
        'cash_in_hand' => 'nullable',
        'vehicle_type' => 'nullable',
        'vehicle_reg_no' => 'nullable',
        'vehicle_color' => 'nullable',
        'vehicle_img' => 'nullable',
        'dl_photo' => 'nullable',
        'aadhar_photo' => 'nullable',
        'service_type_id' => 'nullable',
        'bank_name' => 'nullable',
        'account_no' => 'nullable',
        'ifsc_code' => 'nullable',
        'branch_name' => 'nullable',
        'active' => 'nullable',
        'is_verified' => 'nullable',
        'on_duty' => 'integer',
        'is_busy' => 'nullable',
        'emergency_contact_name' => 'nullable',
        'emergency_contact_no' => 'nullable',
        'emergency_contact_relation' => 'nullable',
        'aadhar_photo_back' => 'nullable',
        'profile_img' => 'nullable',
        'secondary_mobile' => 'nullable',
        'dob' => 'nullable',
        'languages' => 'nullable'
    ];

    /**
     * Route notifications for the FCM channel.
     *
     * @param \Illuminate\Notifications\Notification $notification
     * @return string
     */
    public function routeNotificationForFcm($notification)
    {
        return $this->fcm_token;
    }

    public function kyc()
    {
        return $this->hasMany(DriverKyc::class, 'driver_id', 'id');
    }

    /**
     * Route notifications for the Msg91 channel.
     *
     * @param \Illuminate\Notifications\Notification $notification
     * @return string
     */
    public function routeNotificationForMsg91($notification)
    {
        return "91" . $this->mobile;
    }

    public function review(){
        return $this->hasMany(Review::class);
    }

    public function getRatingAttribute()
    {
        return $this->review()->average('driver_rating');
    }

}
