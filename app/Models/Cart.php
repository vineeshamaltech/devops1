<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'coupon_id',
        'service_category_id',
        'store_id',
        'pickup_from_store',
        'total_price',
        'discount_value',
        'delivery_fee',
        'tax',
        'tip',
        'grand_total',
        'pickup_address',
        'delivery_address',
        'vehicle_id',
        'json_data',
        'distance',
        'travel_time',
        'address_id',
        'quantity',
        'json_data'
    ];

    protected $casts = [
        'pickup_address' => 'array',
        'delivery_address' => 'array',
        'json_data' => 'array',
    ];

    public function service_category()
    {
        return $this->belongsTo(ServiceCategory::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function coupon()
    {
        return $this->belongsTo(Coupon::class);
    }

    public function address()
    {
        return $this->belongsTo(Address::class);
    }

    public function items()
    {
        return $this->hasMany(CartItem::class);
    }

    public function store()
    {
        return $this->belongsTo(Store::class);
    }
}
