<?php

namespace App\Models;
use App\Models\ServiceCategory;
use Eloquent as Model;

/**
 * Class Earning
 * @package App\Models
 * @version December 17, 2021, 8:43 am UTC
 *
 * @property integer $payment_id
 * @property string $credited_to
 * @property string $remarks
 * @property boolean $is_settled
 */
class Earning extends Model
{

    public $table = 'earnings';

    public $fillable = [
        'payment_id',
        'order_id',
        'entity_type',
        'entity_id',
        'amount',
        'type',
        'service',
        'service_type',
        'remarks',
        'is_settled'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'payment_id' => 'integer',
        'credited_to' => 'string',
        'amount' => 'double',
        'type' => 'string',
        'service' => 'string',
        'service_type' => 'integer',
        'remarks' => 'string',
        'is_settled' => 'boolean'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'payment_id' => 'required',
        'order_id'   => 'required',
        'credited_to' => 'required',
        'amount' => 'required',
        'type' => 'nullable',
        'service' => 'nullable',
        'service_type' => 'nullable',
        'remarks' => 'nullable',
        'is_settled' => 'nullable'
    ];

    public function earnings(){
        return $this->belongsTo(ServiceCategory::class,'service_type');
    }
}
