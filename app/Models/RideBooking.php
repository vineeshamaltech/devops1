<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class RideBooking
 * @package App\Models
 * @version January 27, 2022, 1:38 pm IST
 *
 * @property string $id
 * @property integer $user_id
 * @property integer $ride_id
 * @property string $from_name
 * @property number $to_name
 * @property number $from_lat
 * @property string $from_long
 * @property string $to_lat
 * @property string $to_long
 * @property number $price
 */
class RideBooking extends Model
{

    public $table = 'ride_bookings';

    public $fillable = [
        'id',
        'user_id',
        'ride_id',
        'from_city',
        'to_city',
        'from_address',
        'to_address',
        'from_lat',
        'from_long',
        'to_lat',
        'to_long',
        'price',
        'seats',
        'polyline',
        'status'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'user_id' => 'integer',
        'ride_id' => 'integer',
        'from_city' => 'string',
        'to_city' => 'string',
        'from_address' => 'string',
        'to_address' => 'string',
        'from_lat' => 'double',
        'from_long' => 'double',
        'to_lat' => 'double',
        'to_long' => 'double',
        'price' => 'double',
        'polyline' => 'array',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'ride_id' => 'required',
        'from_address' => 'required',
        'to_address' => 'required',
        'from_lat' => 'required',
        'from_long' => 'required',
        'to_lat' => 'required',
        'to_long' => 'required',
        'price' => 'nullable',
        'seats' => 'nullable'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $updaterules = [
        'ride_id' => 'nullable',
        'from_address' => 'nullable',
        'to_address' => 'nullable',
        'from_lat' => 'nullable',
        'from_long' => 'nullable',
        'to_lat' => 'nullable',
        'to_long' => 'nullable',
        'price' => 'nullable',
        'seats' => 'nullable'
    ];

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function ride(){
        return $this->belongsTo(Ride::class);
    }

}
