<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OrderStatusTimestamp extends Model
{
    use HasFactory;

    protected $table = 'order_status_timestamps';

    protected $fillable = [
        'order_id',
        'status',
        'comment'
    ];

    protected $hidden = [
        'updated_at'
    ];

    protected $rules = [
        'order_id' => 'required|integer',
        'status' => 'required|string',
        'comment' => 'nullable|string'
    ];

    public function order(){
        return $this->belongsTo(Order::class);
    }
}
