<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Pivot;

class StoreCategoriesAssoc extends Pivot
{
    use HasFactory;

    protected $table = 'store_categories_assocs';

    protected $fillable = [
        'store_id',
        'store_category_id',
    ];

    public function store()
    {
        return $this->belongsToMany(Store::class,'store_categories_assocs','store_id','store_category_id');
    }
}
