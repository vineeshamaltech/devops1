<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Lab404\Impersonate\Services\ImpersonateManager;
use Laravel\Sanctum\HasApiTokens;
use Spatie\Permission\Traits\HasRoles;

/**
 * Class Vendor
 * @package App\Models
 * @version December 2, 2021, 6:33 am UTC
 *
 * @property string $name
 * @property string $mobile
 * @property string $email
 * @property string $password
 * @property string $fcm_token
 * @property boolean $active
 * @property boolean $verified
 */
class Vendor extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable, HasRoles;

    public $table = 'vendors';

    protected $guard_name = 'vendor';

    public $fillable = [
        'name',
        'mobile',
        'email',
        'country_code',
        'password',
        'fcm_token',
        'active',
        'verified'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'mobile' => 'string',
        'email' => 'string',
        'password' => 'string',
        'fcm_token' => 'string',
        'active' => 'boolean',
        'verified' => 'boolean'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required',
        'mobile' => 'required',
        'email' => 'required',
        'password' => 'required',
        'fcm_token' => 'nullable',
        'active' => 'nullable',
        'verified' => 'nullable'
    ];

    public function kyc()
    {
        return $this->hasMany(VendorKyc::class);
    }

    /**
     * Specifies the user's FCM token
     *
     * @return string|array
     */
    public function routeNotificationForFcm()
    {
        return $this->fcm_token;
    }

    /**
     * Route notifications for the Msg91 channel.
     *
     * @param  \Illuminate\Notifications\Notification  $notification
     * @return string
     */
    public function routeNotificationForMsg91 ($notification) {
        return str_replace('+', '', $this->country_code) . $this->mobile;
    }

    /**
     * Check if the current user is impersonated.
     *
     * @param   void
     * @return  bool
     */
    public function isImpersonated()
    {
        return app(ImpersonateManager::class)->isImpersonating();
    }

}
