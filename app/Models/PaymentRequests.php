<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PaymentRequests extends Model
{
    use HasFactory;

    protected $fillable = [
        'invoice_id',
        'user_id',
        'user_type',
        'order_id',
        'remarks',
        'amount',
        'gateway',
        'gateway_order_id',
        'gateway_response'
    ];
}
