<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class HomeService
 * @package App\Models
 * @version February 4, 2022, 2:23 pm IST
 *
 * @property string $name
 * @property string $description
 * @property string $image
 * @property number $original_price
 * @property number $discount_price
 */
class HomeService extends Model
{

    public $table = 'home_services';

    public $fillable = [
        'name',
        'description',
        'image',
        'home_service_category_id',
        'short_description',
        'original_price',
        'discount_price',
      
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'description' => 'string',
        'short_description'=>'string',
        'image' => 'string',
        'home_service_category_id' =>'integer',
        'original_price' => 'double',
        'discount_price' => 'double'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required|regex:/^[\pL\s\-]+$/u',
        'description' => 'required',
        'home_service_category_id' =>'required',
        'short_description'=>'required',
        'image' => 'required',
        'original_price' => 'required|regex:/^\d+(\.\d{1,2})?$/',
        'discount_price' => 'nullable'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $updateRules = [
        'name' => 'required',
        'description' => 'nullable',
        'home_service_category_id' =>'required',
        'image' => 'nullable',
        'original_price' => 'required',
        'discount_price' => 'nullable'
    ];

}
