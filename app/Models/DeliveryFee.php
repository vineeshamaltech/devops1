<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class DeliveryFee
 * @package App\Models
 * @version December 10, 2021, 8:57 am UTC
 *
 * @property integer $from_km
 * @property integer $to_km
 * @property number $delivery_fee
 */
class DeliveryFee extends Model
{

    public $table = 'delivery_fees';




    public $fillable = [
        'from_km',
        'to_km',
        'delivery_fee',
        'driver_commission'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'from_km' => 'double',
        'to_km' => 'double',
        'delivery_fee' => 'double'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'from_km' => 'required',
        'to_km' => 'required',
        'delivery_fee' => 'required'
    ];


}
