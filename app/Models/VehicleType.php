<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class VehicleType
 * @package App\Models
 * @version December 2, 2021, 8:42 am UTC
 *
 * @property string $name
 * @property string $icon
 * @property boolean $active
 */
class VehicleType extends Model
{
    public $table = 'vehicle_types';

    public $fillable = [
        'name',
        'icon',
        'base_price',
        'base_km',
        'price_km',
        'marker_icon',
        'max_passengers',
        'active'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'max_passengers' => 'integer',
        'base_price' => 'integer',
        'base_km' => 'integer',
        'price_km' => 'integer',
        'icon' => 'string',
        'marker_icon' => 'string',
        'active' => 'boolean'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required',
        'icon' => 'nullable',
        'max_passengers' => 'nullable',
        'marker_icon' => 'nullable',
        'base_price' => 'nullable',
        'base_km' => 'nullable',
        'price_km' => 'nullable',
        'active' => 'nullable'
    ];


}
