<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class StoreReviews
 * @package App\Models
 * @version December 4, 2021, 12:36 pm UTC
 *
 * @property integer $store_id
 * @property string $title
 * @property string $description
 * @property number $rating
 */
class StoreReviews extends Model
{

    public $table = 'store_reviews';
    



    public $fillable = [
        'store_id',
        'title',
        'description',
        'rating'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'store_id' => 'integer',
        'title' => 'string',
        'description' => 'string',
        'rating' => 'double'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'store_id' => 'required',
        'title' => 'nullable',
        'description' => 'nullable',
        'rating' => 'required'
    ];

    
}
