<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HomeServiceOrderStatusTimestamp extends Model
{
    use HasFactory;

    public $table = 'home_service_order_status_timestamps';

    protected $fillable = [
        'order_id',
        'status',
        'comment',
    ];

    public function order(){
        return $this->belongsTo(ServiceOrder::class, 'order_id');
    }
}
