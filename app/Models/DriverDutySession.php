<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DriverDutySession extends Model
{
    use HasFactory;

    protected $table = "driver_duty_sessions";

    protected $fillable = [
        'driver_id',
        'start_time',
        'end_time'
    ];

    public function driver(){
        return $this->belongsTo(Driver::class);
    }
}
