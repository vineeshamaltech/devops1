<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class ServiceType
 * @package App\Models
 * @version December 2, 2021, 8:27 am UTC
 *
 * @property string $name
 */
class ServiceType extends Model
{

    public $table = 'service_types';

    public $fillable = [
        'name'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required'
    ];


}
