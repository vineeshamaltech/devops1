<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class Payment
 * @package App\Models
 * @version December 9, 2021, 11:39 am UTC
 *
 * @property string $ref_no
 * @property string $payment_method
 * @property number $amount
 * @property string $gateway_response
 */
class Payment extends Model
{

    public $table = 'payments';

    public $fillable = [
        'ref_no',
        'user_id',
        'user_type',
        'payment_request_id',
        'order_id',
        'remarks',
        'payment_method',
        'amount',
        'type',
        'gateway_response'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'ref_no' => 'string',
        'payment_request_id' => 'integer',
        'payment_method' => 'string',
        'order_id' => 'integer',
        'remarks' => 'string',
        'amount' => 'double',
        'type' => 'string',
        'gateway_response' => 'array'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'ref_no' => 'required',
        'user_id' => 'required',
        'payment_request_id' => 'nullable',
        'user_type' => 'required',
        'order_id' => 'nullable',
        'remarks' => 'nullable',
        'payment_method' => 'nullable',
        'amount' => 'required',
        'type' => 'required',
        'gateway_response' => 'required'
    ];


}
