<?php

namespace App\Models;

use Carbon\Carbon;
use DateTimeInterface;
use Eloquent as Model;
use Illuminate\Database\Eloquent\Casts\Attribute;

/**
 * Class Order
 * @package App\Models
 * @version December 13, 2021, 7:19 am UTC
 *
 * @property integer $user_id
 * @property integer $store_id
 * @property string $coupon_data
 * @property string $order_type
 * @property number $total_price
 * @property number $discount_type
 * @property number $delivery_fee
 * @property number $tax
 * @property number $grand_total
 * @property string $pickup_address
 * @property string $delivery_address
 * @property number $distance_travelled
 * @property number $travel_time
 * @property number $preparation_time
 * @property integer $payment_id
 * @property integer $status
 * @property integer $driver_id
 * @property string $order_status
 * @property string $json_data
 */
class Order extends Model
{

    public $table = 'orders';

    public $fillable = [
        'user_id',
        'store_id',
        'coupon_data',
        'service_category_id',
        'order_type',
        'total_price',
        'discount_value',
        'delivery_fee',
        'pickup_from_store',
        'tax',
        'tip',
        'grand_total',
        'pickup_address',
        'delivery_address',
        'distance_travelled',
        'travel_time',
        'preparation_time',
        'payment_id',
        'driver_id',
        'vehicle_id',
        'order_status',
        'payment_status',
        'payment_method',
        'json_data',
        'pickup_otp',
        'delivery_otp'
    ];

    protected $dates = ['created_at','deleted_at'];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */

    protected $casts = [
        'id' => 'integer',
        'user_id' => 'integer',
        'store_id' => 'integer',
        'service_category_id' => 'integer',
        'coupon_data' => 'array',
        'order_type' => 'integer',
        'pickup_from_store' => 'boolean',
        'total_price' => 'double',
        'discount_value' => 'double',
        'delivery_fee' => 'double',
        'tax' => 'double',
        'tip' => 'double',
        'grand_total' => 'double',
        'pickup_address' => 'array',
        'delivery_address' => 'array',
        'distance_travelled' => 'double',
        'travel_time' => 'double',
        'preparation_time' => 'double',
        'payment_id' => 'integer',
        'driver_id' => 'integer',
        'vehicle_id' => 'integer',
        'order_status' => 'string',
        'payment_status' => 'string',
        'payment_method' => 'string',
        'json_data' => 'array',
        'pickup_otp' => 'string',
        'delivery_otp' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'user_id' => 'nullable',
        'store_id' => 'nullable',
        'service_category_id' => 'nullable',
        'coupon_data' => 'nullable',
        'order_type' => 'nullable',
        'total_price' => 'nullable',
        'discount_value' => 'nullable',
        'delivery_fee' => 'nullable',
        'tax' => 'nullable',
        'tip' => 'nullable',
        'grand_total' => 'nullable',
        'pickup_address' => 'nullable',
        'delivery_address' => 'nullable',
        'distance_travelled' => 'nullable',
        'travel_time' => 'nullable',
        'payment_id' => 'nullable',
        'driver_id' => 'nullable',
        'order_status' => 'nullable',
        'json_data' => 'nullable',
        'pickup_otp' => 'nullable',
        'delivery_otp' => 'nullable'
    ];

    public function getEstimatedDeliveryTime(){
        return $this->preparation_time + $this->travel_time;
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function store()
    {
        return $this->belongsTo(Store::class);
    }

    public function payment()
    {
        return $this->belongsTo(Payment::class);
    }

    public function driver()
    {
        return $this->belongsTo(Driver::class);
    }

    public function orderType(){
        return $this->belongsTo(ServiceType::class,'order_type','id');
    }

    public function service_category()
    {
        return $this->belongsTo(ServiceCategory::class);
    }

    public function items()
    {
        return $this->hasMany(OrderProducts::class);
    }

    public function status_timestamps(){
        return $this->hasMany(OrderStatusTimestamp::class);
    }

    public function earnings(){
        return $this->hasMany(Earning::class);
    }

    /**
     * Prepare a date for array / JSON serialization.
     *
     * @param  \DateTimeInterface  $date
     * @return string
     */
    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }

    public function review(){
        return $this->hasOne(Review::class);
    }

    public function vehicle(){
        return $this->belongsTo(VehicleType::class,'vehicle_id');
    }

//    protected function createdAt(): Attribute {
//        return new Attribute(
//            get: fn ($value) => Carbon::parse($value)->format('d-m-Y H:i:s'),
//        );
//    }
}
