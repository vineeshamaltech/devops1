<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class Store
 * @package App\Models
 * @version December 4, 2021, 12:13 pm UTC
 *
 * @property string $vendor_id
 * @property integer $service_cat_id
 * @property string $store_categories
 * @property string $name
 * @property string $description
 * @property string $image
 * @property string $lat
 * @property string $long
 * @property string $address
 * @property number $delivery_range
 * @property number $base_distance
 * @property number $base_price
 * @property number $price_per_km
 * @property string $mobile
 * @property string $email
 * @property integer $order_count
 * @property boolean $is_open
 * @property boolean $active
 */
class Store extends Model
{
    use HasFactory;

    public $table = 'stores';

    public $fillable = [
        'admin_id',
        'service_category_id',
        'store_categories',
        'name',
        'description',
        'image',
        'latitude',
        'longitude',
        'address',
        'delivery_range',
        'base_distance',
        'base_price',
        'price_per_km',
        'commission',
        'tax',
        'rating',
        'mobile',
        'email',
        'order_count',
        'is_open',
        'active',
        'payment_methods',
        'zone'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'admin_id' => 'string',
        'service_category_id' => 'integer',
        'store_categories' => 'string',
        'name' => 'string',
        'description' => 'string',
        'image' => 'string',
        'latitude' => 'string',
        'longitude' => 'string',
        'address' => 'string',
        'delivery_range' => 'double',
        'base_distance' => 'double',
        'base_price' => 'double',
        'price_per_km' => 'double',
        'commission' => 'double',
        'tax' => 'double',
        'rating' => 'double',
        'mobile' => 'string',
        'email' => 'string',
        'order_count' => 'integer',
        'is_open' => 'boolean',
        'active' => 'boolean',
        'payment_methods' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'admin_id' => 'nullable',
        'service_category_id' => 'nullable',
        'store_categories' => 'nullable',
        'name' => 'nullable|regex:/^[\pL\s\-]+$/u',
        'description' => 'nullable',
        'image' => 'required',
        'latitude' => 'nullable',
        'longitude' => 'nullable',
        'address' => 'nullable',
        'delivery_range' => 'nullable',
        'base_distance' => 'nullable',
        'base_price' => 'nullable',
        'price_per_km' => 'nullable',
        'commission' => 'nullable',
        'tax' => 'nullable',
        'mobile' => 'nullable',
        'email' => 'nullable',
        'order_count' => 'nullable',
        'is_open' => 'nullable',
        'active' => 'nullable',
        'payment_methods' => 'nullable',
        'zone'=> 'nullable' 
    ];

    public function vendor()
    {
        return $this->belongsTo(Admin::class,'admin_id');
    }

    public function zone_data()
    {
        return $this->belongsTo(Zone::class,'id','zone');
    }
    
    public function serviceCategory()
    {
        return $this->belongsTo(ServiceCategory::class);
    }

    public function coupons(){
        return $this->hasMany(Coupon::class);
    }

    public function gallery()
    {
        return $this->hasMany(StoreGallery::class);
    }

    public function reviews()
    {
        return $this->hasMany(StoreReviews::class);
    }

    public function store_timings(){
        return $this->hasMany(StoreTiming::class);
    }

    public function store_categories(){
        return $this->belongsToMany(StoreCategory::class,'store_categories_assocs','store_id','store_category_id');
    }

    public function products(){
        return $this->hasMany(Product::class);
    }

    public function orders(){
        return $this->hasMany(Order::class);
    }

    public function categories(){
        return $this->hasMany(Category::class);
    }

    public function review(){
        return $this->hasMany(Review::class);
    }

    public function getRatingAttribute()
    {
        return $this->review()->average('store_rating');
    }
}
