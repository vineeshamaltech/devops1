<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ServiceCategoryVehicleType extends Model
{
    use HasFactory;

    protected $table = 'service_category_vehicle_type';

    protected $fillable = [
        'service_category_id',
        'vehicle_type_id',
    ];

}
