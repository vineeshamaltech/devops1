<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class Coupon
 * @package App\Models
 * @version December 7, 2021, 11:27 am UTC
 *
 * @property string $store_id
 * @property string $image
 * @property string $code
 * @property string $description
 * @property integer $max_use_per_user
 * @property number $min_cart_value
 * @property string $start_date
 * @property string $end_date
 * @property string $discount_type
 * @property string $discount_value
 * @property boolean $active
 */
class Coupon extends Model
{
    public $table = 'coupons';

    public $fillable = [
        'store_id',
        'image',
        'code',
        'description',
        'max_use_per_user',
        'min_cart_value',
        'start_date',
        'end_date',
        'discount_type',
        'discount_value',
        'max_discount_value',
        'active'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'store_id' => 'string',
        'image' => 'string',
        'code' => 'string',
        'description' => 'string',
        'max_use_per_user' => 'integer',
        'min_cart_value' => 'double',
        'discount_type' => 'string',
        'discount_value' => 'string',
        'max_discount_value' => 'string',
        'active' => 'boolean'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'store_id' => 'nullable',
        'image' => 'nullable',
        'code' => 'required',
        'description' => 'nullable',
        'max_use_per_user' => 'nullable',
        'min_cart_value' => 'nullable',
        'start_date' => 'nullable',
        'end_date' => 'nullable',
        'discount_type' => 'required',
        'discount_value' => 'nullable',
        'max_discount_value' => 'nullable',
        'active' => 'nullable'
    ];

    public static $updateRules = [
        'store_id' => 'nullable',
        'image' => 'nullable',
        'code' => 'nullable',
        'description' => 'nullable',
        'max_use_per_user' => 'nullable',
        'min_cart_value' => 'nullable',
        'start_date' => 'nullable',
        'end_date' => 'nullable',
        'discount_type' => 'nullable',
        'discount_value' => 'nullable',
        'max_discount_value' => 'nullable',
        'active' => 'nullable'
    ];

    public function store()
    {
        return $this->belongsTo(Store::class);
    }


}
