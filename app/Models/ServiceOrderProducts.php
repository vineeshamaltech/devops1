<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ServiceOrderProducts extends Model
{
    use HasFactory;

    protected $table = "service_order_products";

    protected $fillable = [
        'service_order_id',
        'service_name',
        'service_price',
        'discount_price',
        'quantity',
      
    ];

    public function order(){
        return $this->belongsTo(ServiceOrder::class);
    }
}
