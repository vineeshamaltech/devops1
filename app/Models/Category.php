<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class Category
 * @package App\Models
 * @version December 7, 2021, 8:09 am UTC
 *
 * @property string $name
 * @property string $image
 * @property integer $parent
 */
class Category extends Model
{

    public $table = 'categories';

    public $fillable = [
        'name',
        'store_id',
        'image',
        'parent'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'store_id' => 'integer',
        'image' => 'string',
        'parent' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'nullable',
        'image' => 'nullable',
        'store_id' => 'nullable',
        'parent' => 'nullable'
    ];

    public function parent_cat(){
        return $this->belongsTo(Category::class,'parent','id');
    }

    public function children(){
        return $this->hasMany(Category::class, 'parent');
    }

    public function products(){
        return $this->hasMany(Product::class,'category_id');
    }

    public function product(){
        return $this->belongsTo(Product::class,'id','parent_category_id');
    }

    public function store(){
        return $this->belongsTo(Store::class);
    }

}
