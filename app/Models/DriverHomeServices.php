<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DriverHomeServices extends Model
{
    use HasFactory;

    protected $table = 'home_service_category_driver_id';

    protected $fillable = [
        'home_service_category_id',
        'driver_id',
    ];
}
