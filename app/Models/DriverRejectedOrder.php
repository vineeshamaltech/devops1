<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DriverRejectedOrder extends Model
{
    use HasFactory;

    protected $table = 'driver_rejected_orders';

    protected $fillable = [
        'driver_id',
        'order_id',
        'reason'
    ];
}
