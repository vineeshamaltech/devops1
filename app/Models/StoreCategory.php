<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class StoreCategory
 * @package App\Models
 * @version December 2, 2021, 10:59 am UTC
 *
 * @property string $name
 * @property string $image
 * @property integer $service_category_id
 * @property boolean $active
 */
class StoreCategory extends Model
{

    public $table = 'store_categories';

    public $fillable = [
        'name',
        'image',
        'service_category_id',
        'active'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'image' => 'string',
        'service_category_id' => 'integer',
        'active' => 'boolean'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required|regex:/^[\pL\s\-]+$/u',
        'image' => 'required|image|max:2048',
        'service_category_id' => 'required|integer',
        'active' => 'nullable'
    ];

    public function serviceCategory()
    {
        return $this->belongsTo(ServiceCategory::class);
    }

    public function store()
    {
        return $this->hasMany(Store::class);
    }

}
