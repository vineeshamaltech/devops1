<?php

namespace App\Models;

use Carbon\Carbon;
use Eloquent as Model;
use Illuminate\Database\Eloquent\Casts\Attribute;

/**
 * Class Ride
 * @package App\Models
 * @version January 27, 2022, 1:35 pm IST
 *
 * @property string $id
 * @property integer $user_id
 * @property string $from_address
 * @property string $to_address
 * @property number $from_lat
 * @property number $from_long
 * @property number $to_lat
 * @property number $to_long
 * @property string $travel_datetime
 * @property string $route
 * @property int $max_seats
 * @property boolean $can_book_instantly
 * @property number $price
 * @property string $status
 * @property boolean $active
 */
class Ride extends Model
{
    public $table = 'rides';

    public $fillable = [
        'id',
        'user_id',
        'from_city',
        'to_city',
        'from_address',
        'to_address',
        'from_lat',
        'from_long',
        'to_lat',
        'to_long',
        'travel_datetime',
        'route',
        'max_seats',
        'passenger_count',
        'can_book_instantly',
        'price',
        'status',
        'active',
        'polyline'
    ];


    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'user_id' => 'integer',
        'from_city' => 'string',
        'to_city' => 'string',
        'from_address' => 'string',
        'to_address' => 'string',
        'from_lat' => 'double',
        'from_long' => 'double',
        'to_lat' => 'double',
        'to_long' => 'double',
        'route' => 'string',
        'can_book_instantly' => 'boolean',
        'price' => 'double',
        'status' => 'string',
        'active' => 'boolean',
        'polyline' => 'array',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'user_id' => 'nullable',
        'from_city' => 'nullable',
        'to_city' => 'nullable',
        'from_address' => 'required',
        'to_address' => 'required',
        'from_lat' => 'required',
        'from_long' => 'required',
        'to_lat' => 'required',
        'to_long' => 'required',
        'travel_datetime' => 'required',
        'route' => 'required',
        'max_seats' => 'nullable',
        'can_book_instantly' => 'nullable',
        'price' => 'nullable',
        'status' => 'nullable',
        'active' => 'nullable'
    ];

    public function travelDatetime() : Attribute {
        return new Attribute(
            get: function ($value) {
                return Carbon::parse($value)->format('d-m-Y H:i:s');
            },
            set: function ($value) {
                return Carbon::parse($value);
            }
        );
    }

    public function stopovers(){
        return $this->hasMany(StopOver::class);
    }

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function bookings(){
        return $this->hasMany(RideBooking::class);
    }

}
