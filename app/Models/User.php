<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Spatie\Permission\Traits\HasRoles;
use Lab404\Impersonate\Services\ImpersonateManager;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable, HasRoles;

    public $table = 'users';

    public $fillable = [
        'name',
        'country_code',
        'mobile',
        'email',
        'password',
        'profile_img',
        'wallet_amount',
        'fcm_token',
        'lat',
        'long',
        'status',
        'rating',
        'ref_code',
        'ref_by',
    ];

    public $hidden = [
        'password',
        'remember_token',
        'email_verified_at',
    ];


    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'mobile' => 'string',
        'email' => 'string',
        'password' => 'string',
        'profile_img' => 'string',
        'wallet_amount' => 'double',
        'fcm_token' => 'string',
        'lat' => 'string',
        'long' => 'string',
        'status' => 'boolean',
        'ref_code' => 'string',
        'ref_by' => 'integer',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
      'name' => 'required|regex:/^[\pL\s\-]+$/u',
        'mobile' => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|min:10',
        'email' => 'required|regex:/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/ix',
        'password' => 'required|string',
        'profile_img' => 'required|image',
        'wallet_amount' => 'nullable|numeric',
        'fcm_token' => 'nullable|string',
        'lat' => 'nullable|string',
        'long' => 'nullable|string',
        'status' => 'nullable'
    ];

    public static $updaterules = [
        'name' => 'nullable',
        'mobile' => 'nullable',
        'email' => 'nullable',
        'password' => 'nullable|string',
        'profile_img' => 'nullable|image',
        'wallet_amount' => 'nullable|numeric',
        'fcm_token' => 'nullable|string',
        'lat' => 'nullable|string',
        'long' => 'nullable|string',
        'status' => 'nullable'
    ];

    /**
     * Route notifications for the FCM channel.
     *
     * @param  \Illuminate\Notifications\Notification  $notification
     * @return string
     */
    public function routeNotificationForFcm($notification)
    {
        return $this->fcm_token;
    }

    /**
     * Route notifications for the Msg91 channel.
     *
     * @param  \Illuminate\Notifications\Notification  $notification
     * @return string
     */
    public function routeNotificationForMsg91 ($notification) {
        return "91".$this->mobile;
    }

    /**
     * Check if the current user is impersonated.
     *
     * @param   void
     * @return  bool
     */
    public function isImpersonated()
    {
        return app(ImpersonateManager::class)->isImpersonating();
    }
}
