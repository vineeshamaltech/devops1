<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class PaymentType
 * @package App\Models
 * @version December 2, 2021, 8:10 am UTC
 *
 * @property string $name
 * @property string $description
 * @property string $icon
 * @property string $payment_key
 * @property string $payment_secret
 */
class PaymentType extends Model
{

    public $table = 'payment_methods';

    public $fillable = [
        'name',
        'description',
        'icon',
        'type',
        'min_order_amount',
        'payment_key',
        'payment_secret',
        'home_service',
        'service'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'description' => 'string',
        'icon' => 'string',
        'min_order_amount' => 'double',
        'type' => 'string',
        'payment_key' => 'string',
        'payment_secret' => 'string',
        'home_service' => 'integer',
        'service' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required',
        'description' => 'nullable',
        'icon' => 'nullable',
        'min_order_amount' => 'nullable',
        'type' => 'required',
        'payment_key' => 'nullable',
        'payment_secret' => 'nullable',
        'home_service' => 'nullable',
        'service' => 'nullable'
    ];

    public function service_category(){
        return $this->belongsToMany(ServiceCategory::class)->using(ServiceCategoryPaymentMethod::class);
    }


}
