<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class Zone
 * @package App\Models
 * @version February 2, 2022, 6:04 pm IST
 *
 * @property string $id
 * @property string $title
 * @property string $polygon
 * @property boolean $active
 */
class Zone extends Model
{
    public $table = 'zones';

    public $fillable = [
        'id',
        'title',
        'polygon',
        'active'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'string',
        'title' => 'string',
        'polygon' => 'array',
        'active' => 'boolean'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'title' => 'required',
        'polygon' => 'required',
        'active' => 'nullable'
    ];


}
