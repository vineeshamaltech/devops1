<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class Review
 * @package App\Models
 * @version January 15, 2022, 2:07 pm IST
 *
 * @property string $id
 * @property integer $order_id
 * @property number $store_rating
 * @property number $driver_rating
 * @property string $store_review
 * @property string $driver_review
 */
class Review extends Model
{
    public $table = 'reviews';

    public $fillable = [
        'order_id',
        'driver_id',
        'store_id',
        'user_id',
        'store_rating',
        'driver_rating',
        'user_rating',
        'store_review',
        'driver_review',
        'user_review'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'order_id' => 'integer',
        'driver_id' => 'integer',
        'store_id' => 'integer',
        'store_rating' => 'double',
        'driver_rating' => 'double',
        'store_review' => 'string',
        'driver_review' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'order_id' => 'required',
        'driver_id' => 'nullable',
        'store_id' => 'nullable',
        'store_rating' => 'nullable',
        'driver_rating' => 'nullable',
        'store_review' => 'nullable',
        'driver_review' => 'nullable'
    ];

    public function order()
    {
        return $this->belongsTo(Order::class);
    }

    public function driver()
    {
        return $this->belongsTo(Driver::class);
    }

    public function store()
    {
        return $this->belongsTo(Store::class);
    }

}
