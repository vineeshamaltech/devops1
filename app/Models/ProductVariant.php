<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class ProductVariant
 * @package App\Models
 * @version December 6, 2021, 11:39 am UTC
 *
 * @property integer $store_id
 * @property string $product_id
 * @property string $name
 * @property string $price
 * @property string $sku
 * @property string $image
 * @property boolean $is_open
 */
class ProductVariant extends Model
{

    public $table = 'product_variants';

    public $fillable = [
        'product_id',
        'name',
        'price',
        'discount_price',
        'sku',
        'image',
        'active',
        'store_id',
          'is_open'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'product_id' => 'string',
        'name' => 'string',
        'price' => 'double',
        'discount_price' => 'double',
        'sku' => 'integer',
        'image' => 'string',
        'active' => 'boolean',
        'store_id' => 'integer',
        'is_open' => 'boolean'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'product_id' => 'nullable',
        'name' => 'required',
        'price' => 'required',
        'discount_price' => 'required',
        'sku' => 'nullable',
        'image' => 'nullable',
        'active' => 'nullable',
        'store_id' => 'nullable',
         'is_open' => 'nullable'
    ];

    public static $updaterules = [
        'product_id' => 'nullable',
        'name' => 'nullable',
        'price' => 'nullable',
        'discount_price' => 'nullable',
        'sku' => 'nullable',
        'image' => 'nullable',
        'active' => 'nullable',
        'store_id' => 'nullable',
        'is_open' => 'nullable'
    ];
 public function store()
    {
        return $this->belongsTo(Store::class, 'store_id');
    }
    public function product()
    {
        return $this->belongsTo(Product::class);
    }

}
