<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ZoneHomeServices extends Model
{
    use HasFactory;

    protected $table = 'home_service_category_zone_id';

    protected $fillable = [
        'home_service_category_id',
        'zone_id',
    ];
}
