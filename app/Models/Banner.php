<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class Banner
 * @package App\Models
 * @version December 4, 2021, 9:10 am UTC
 *
 * @property string $store_id
 * @property string $image
 * @property string $title
 * @property integer $service_category_id
 * @property boolean $active
 */
class Banner extends Model
{
    public $table = 'banners';

    public $fillable = [
        'store_id',
        'image',
        'title',
        'service_category_id',
        'home_service_category_id',
        'url',
        'type',
        'zones',
        'active',
        'top_picks_enabled'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'store_id' => 'string',
        'image' => 'string',
        'title' => 'string',
        'service_category_id' => 'integer',
        'home_service_category_id' => 'integer',
        'url' => 'string',
        'type' => 'string',
        'active' => 'boolean',
        'top_picks_enabled' => 'boolean'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'store_id' => 'nullable',
        'image' => 'nullable|image:jpeg,png,jpg,gif,svg,webp|max:2048',
        'title' => 'nullable',
        'service_category_id' => 'nullable',
        'active' => 'nullable',
        'top_picks_enabled' => 'nullable'
    ];


    public function serviceCategory()
    {
        return $this->belongsTo(ServiceCategory::class);
    }

    public function store()
    {
        return $this->belongsTo(Store::class);
    }

    // public function zone()
    // {
    //     return $this->hasMany(Zone::class,'id','zones');
    // }

    public function homeServiceCategory()
    {
        return $this->belongsTo(HomeServiceCategory::class, 'home_service_category_id','id');
    }
}
