<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\Pivot;

class ServiceCategoryPaymentMethod extends Pivot
{
    use HasFactory;

    protected $table = 'payment_type_service_category';

    protected $fillable = [
        'service_category_id',
        'payment_type_id',
    ];
}
