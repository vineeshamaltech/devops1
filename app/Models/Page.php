<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class Page
 * @package App\Models
 * @version January 28, 2022, 3:08 pm IST
 *
 * @property string $id
 * @property string $title
 * @property string $slug
 * @property string $content
 */
class Page extends Model
{

    public $table = 'pages';




    public $fillable = [
        'id',
        'title',
        'slug',
        'content'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'title' => 'string',
        'slug' => 'string',
        'content' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'title' => 'required',
        'slug' => 'nullable',
        'content' => 'nullable'
    ];


}
