<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class TimeSlots
 * @package App\Models
 * @version June 2, 2021, 10:51 am UTC
 *
 * @property string $day
 * @property string $from
 * @property string $to
 * @property integer $max_bookings
 * @property boolean $enabled
 */
class TimeSlots extends Model
{

    use HasFactory;

    public $table = 'timeslots';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    protected $dates = ['deleted_at'];

    public $fillable = [
        'day',
        'from',
        'to',
        'max_bookings',
        'enabled',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'day' => 'string',
        'from' => 'string',
        'to' => 'string',
        'max_bookings' => 'integer',
        'enabled' => 'boolean',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'day' => 'required|string|max:255',
        'from' => 'required|string|max:255',
        'to' => 'required|string|max:255',
        'max_bookings' => 'required|integer',
        'enabled' => 'nullable',
        'deleted_at' => 'nullable',
        'created_at' => 'nullable',
        'updated_at' => 'nullable',
    ];

}
