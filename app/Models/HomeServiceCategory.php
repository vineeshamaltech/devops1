<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\Casts\Attribute;

/**
 * Class HomeServiceCategory
 * @package App\Models
 * @version February 4, 2022, 2:14 pm IST
 *
 * @property string $id
 * @property string $name
 * @property string $icon
 * @property string $background_image
 * @property string $view
 * @property integer $parent
 * @property number $tax
 */
class HomeServiceCategory extends Model
{

    public $categories = [];

    public $table = 'home_service_categories';

    public $fillable = [
        'id',
        'name',
        'icon',
        'background_image',
        'view',
        'parent',
        'tax',
        'admin_commission',
        'zones',
        'payment_method',

    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'string',
        'name' => 'string',
        'icon' => 'string',
        'background_image' => 'string',
        'view' => 'string',
        'parent' => 'integer',
        'tax' => 'double',
        'admin_commission' => 'double',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required',
        'icon' => 'required',
        'background_image' => 'required',
        'parent' => 'required',
        'tax' => 'required'
    ];

    public static $updateRules = [
        'name' => 'required',
        'icon' => 'nullable',
        'background_image' => 'nullable',
        'parent' => 'required',
        'tax' => 'required'
    ];

    // public function zones() : Attribute{
    //     return new Attribute(
    //         set: function($value) {
    //             return implode(',', $value);
    //         }
    //     );
    // }

    public static function getHierarchy(): array{
        return (new self())->getCategories();
    }

    private function getCategories(): array{
        $mainCategories = self::where('parent',0)->get();
        foreach ($mainCategories as $category) {
            $this->categories[] = $category;
            $this->getParentCategories($category, 0);
        }
        return $this->categories;
    }

    private function getParentCategories($category, $level){
        if($subCategories = $category->hasSubCategories) {
            $level++;
            foreach($subCategories as $subCategory) {
                $subCategory->name = str_repeat(' - ', $level) . $subCategory->name;
                $this->categories[] = $subCategory;
                $this->getParentCategories($subCategory, $level);
            }
        }
    }

    public function hasSubCategories(){
        return $this->hasMany($this, 'parent');
    }

    public function children(){
        return $this->hasMany($this, 'parent');
    }

    public function parent_cat(){
        return $this->belongsTo($this, 'parent');
    }
}
