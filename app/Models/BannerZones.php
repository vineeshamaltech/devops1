<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BannerZones extends Model
{
    public $table = 'banner_zones';

    public $fillable = [
        'banner_id',
        'zone_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'banner_id' => 'integer',
        'zone_id' => 'integer'
    ];

    
}
