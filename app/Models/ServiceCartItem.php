<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ServiceCartItem extends Model
{
    use HasFactory;

    protected $fillable = [
        'cart_id',
        'service_id',
        'quantity',
        'service_price',
        'cart_price',
        'service_name',
      
    ];

   

    public function service(){
        return $this->belongsTo(HomeService::class);
    }
    

    public function getVariantsAttribute(){
        $collection = collect(explode(',', $this->variant_ids));
        return $collection->map(function($id){
            return ProductVariant::find($id);
        });
    }
}
