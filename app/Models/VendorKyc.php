<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class VendorKyc
 * @package App\Models
 * @version December 2, 2021, 6:33 am UTC
 *
 * @property string $vedor_id
 * @property string $doc_type
 * @property string $photo
 * @property string $status
 */
class VendorKyc extends Model
{

    public $table = 'vendor_kycs';

    public $fillable = [
        'admin_id',
        'doc_type',
        'photo',
        'status'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'admin_id' => 'string',
        'doc_type' => 'string',
        'photo' => 'string',
        'status' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules  = [        
            'admin_id' => 'required',
            'status' => 'required',
            'aadhar' => 'required|file|mimes:jpeg,png,jpg|max:2048',
            'pan' => 'required|file|mimes:jpeg,png,jpg|max:2048',
            'fssai' => 'required|file|mimes:jpeg,png,jpg|max:2048',
            'gst' => 'required|file|mimes:jpeg,png,jpg|max:2048',
             'other' => 'required|file|mimes:jpeg,png,jpg|max:2048' ,
            'photo' => 'required|file|mimes:jpeg,png,jpg|max:2048' 
          ];


    public function vendor()
    {
        return $this->belongsTo(Admin::class);
    }


}
