<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class StoreTiming
 * @package App\Models
 * @version December 6, 2021, 10:26 am UTC
 *
 * @property integer $store_id
 * @property string $day
 * @property time $open
 * @property time $close
 */
class StoreTiming extends Model
{

    public $table = 'store_timings';

    public $fillable = [
        'store_id',
        'day',
        'open',
        'close'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'store_id' => 'integer',
        'day' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'store_id' => 'required',
    ];

    public function store()
    {
        return $this->belongsTo(Store::class);
    }


}
