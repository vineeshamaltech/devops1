<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ServiceCart extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'coupon_id',
        'service_category_id',
        'total_price',
        'discount_value',
        'tax',
        'grand_total',
        'service_address',
        'json_data',
        'distance',
        'address_id',
        'quantity',
        'json_data'
    ];

    protected $casts = [
     
        'service_address' => 'array',
        'json_data' => 'array',
    ];

    public function service()
    {
        return $this->belongsTo(HomeService::class,'service_category_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function coupon()
    {
        return $this->belongsTo(Coupon::class);
    }

    public function address()
    {
        return $this->belongsTo(Address::class);
    }

    public function items()
    {
        return $this->hasMany(ServiceCartItem::class,'cart_id');
    }

  
}
