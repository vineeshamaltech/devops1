<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class StopOver
 * @package App\Models
 * @version January 27, 2022, 1:37 pm IST
 *
 * @property string $id
 * @property integer $user_id
 * @property integer $ride_id
 * @property string $name
 * @property number $lat
 * @property number $long
 */
class StopOver extends Model
{

    public $table = 'stop_overs';

    public $fillable = [
        'id',
        'user_id',
        'ride_id',
        'name',
        'lat',
        'long'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'string',
        'user_id' => 'integer',
        'ride_id' => 'integer',
        'name' => 'string',
        'lat' => 'double',
        'long' => 'double'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'id' => 'required',
        'user_id' => 'required',
        'ride_id' => 'required',
        'name' => 'required',
        'lat' => 'required',
        'long' => 'required'
    ];

    public function ride()
    {
        return $this->belongsTo(Ride::class);
    }

    public function user(){
        return $this->belongsTo(User::class);
    }

}
