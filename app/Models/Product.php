<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Product
 * @package App\Models
 * @version December 6, 2021, 11:37 am UTC
 *
 * @property integer $store_id
 * @property string $product_type
 * @property string $name
 * @property string $description
 * @property string $image
 * @property string $unit
 * @property string $price
 * @property string $discount_price
 * @property integer $store_category_id
 * @property integer $sku
 * @property string $product_tags
 * @property boolean $active
 */
class Product extends Model
{
    use HasFactory;

    protected $with = ['variants'];

    protected $appends = ['parent_category_id'];

    public $table = 'products';

    public $fillable = [
        'store_id',
        'product_type',
        'name',
        'description',
        'image',
        'unit',
        'price',
        'discount_price',
        'category_id',
        'parent_category_id',
        'sku',
        'tax',
        'product_tags',
        'opening_time',
        'closing_time',
        'active',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'store_id' => 'integer',
        'product_type' => 'string',
        'name' => 'string',
        'description' => 'string',
        'image' => 'string',
        'unit' => 'string',
        'price' => 'double',
        'discount_price' => 'double',
        'category_id' => 'integer',
        'parent_category_id' => 'integer',
        'sku' => 'integer',
        'tax' => 'double',
        'product_tags' => 'string',
        'opening_time' => 'string',
        'closing_time' => 'string',
        'active' => 'boolean'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'store_id' => 'required',
        'product_type' => 'nullable',
        'name' => 'required',
        'description' => 'required',
        'image' => 'image|max:2048|mimes:jpeg,png,jpg',
        'unit' => 'required',
        'price' => 'required',
        'discount_price' => 'nullable',
        'category_id' => 'required',
        'sku' => 'required',
        'tax' => 'nullable',
        'product_tags' => 'required',
        'active' => 'nullable'
    ];

    public static $updateRules = [
        'store_id' => 'nullable',
        'product_type' => 'nullable',
        'name' => 'nullable',
        'description' => 'nullable',
        'image' => 'image|max:2048|mimes:jpeg,png,jpg',
        'unit' => 'nullable',
        'price' => 'nullable',
        'discount_price' => 'nullable',
        'category_id' => 'nullable',
        'sku' => 'nullable',
        'tax' => 'nullable',
        'product_tags' => 'nullable',
        'opening_time' => 'nullable',
        'closing_time' => 'nullable',
        'active' => 'nullable'
    ];

    public function variants()
    {
        return $this->hasMany(ProductVariant::class);
    }

    public function store()
    {
        return $this->belongsTo(Store::class, 'store_id');
    }
    // get product categories realted to product
    public function categories()
    {
        return $this->belongsTo(Category::class, 'category_id','id');
    }

    public function getParentCategoryIdAttribute(){
        $cat = Category::find($this->category_id);
        return $cat->parent ?? null;
    }

}
