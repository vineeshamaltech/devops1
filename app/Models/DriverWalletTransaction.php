<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class DriverWalletTransaction
 * @package App\Models
 * @version December 17, 2021, 8:41 am UTC
 *
 * @property integer $user_id
 * @property string $type
 * @property string $remarks
 */
class DriverWalletTransaction extends Model
{

    public $table = 'driver_wallet_transactions';

    public $fillable = [
        'driver_id',
        'type',
        'remarks',
        'amount',
        'opening_balance',
        'closing_balance',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'driver_id' => 'integer',
        'type' => 'string',
        'amount' => 'double',
        'opening_balance' => 'double',
        'closing_balance' => 'double',
        'remarks' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'driver_id' => 'required',
        'type' => 'required',
        'amount' => 'required',
        'remarks' => 'nullable'
    ];


}
