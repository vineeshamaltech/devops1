<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CartItem extends Model
{
    use HasFactory;

    protected $fillable = [
        'cart_id',
        'product_id',
        'quantity',
        'product_price',
        'cart_price',
        'product_name',
        'product_description',
        'variant_ids'
    ];

    public $appends = ['variants'];

    public function product(){
        return $this->belongsTo(Product::class);
    }

    public function getVariantsAttribute(){
        $collection = collect(explode(',', $this->variant_ids));
        if ($collection->isNotEmpty()) {
            return $collection->map(function($id){
                return ProductVariant::find($id);
            });
        }
    }
}
