<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class ServiceCategory
 * @package App\Models
 * @version December 2, 2021, 10:58 am UTC
 *
 * @property string $icon
 * @property string $bg_image
 * @property integer $home_view
 * @property string $name
 * @property string $description
 * @property integer $type
 * @property number $discount
 * @property number $commission
 * @property string $vehicles
 * @property string $min_price
 * @property string $driver_radius
 * @property number $base_distance
 * @property number $base_price
 * @property number $base_km
 * @property number $price_km
 * @property number $max_order_distance
 * @property string $payment_methods
 * @property boolean $active
 */

class ServiceCategory extends Model
{
    public $table = 'service_categories';

    public $fillable = [
        'icon',
        'bg_image',
        'home_view',
        'name',
        'description',
        'service_type_id',
        'discount',
        'commission',
        'vehicles',
        'min_price',
        'driver_radius',
        'base_distance',
        'base_price',
        'price_km',
        'tax',
        'max_order_distance',
        'payment_methods',
        'active'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'icon' => 'string',
        'bg_image' => 'string',
        'home_view' => 'string',
        'name' => 'string',
        'description' => 'string',
        'service_type_id' => 'integer',
        'discount' => 'double',
        'commission' => 'double',
        'vehicles' => 'string',
        'min_price' => 'string',
        'driver_radius' => 'string',
        'base_distance' => 'double',
        'base_price' => 'double',
        'base_km' => 'double',
        'price_km' => 'double',
        'max_order_distance' => 'double',
        'payment_methods' => 'string',
        'active' => 'boolean'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'icon' => 'nullable|image|max:2048',
        'bg_image' => 'nullable|image|max:2048',
        'home_view' => 'required|string',
        'name' => 'required',
        'description' => 'nullable',
        'service_type_id' => 'required',
        'discount' => 'nullable',
        'commission' => 'nullable',
        'vehicles' => 'required',
        'min_price' => 'nullable',
        'driver_radius' => 'required',
        'base_distance' => 'nullable',
        'base_price' => 'nullable',
        'base_km' => 'nullable',
        'price_km' => 'nullable',
        'max_order_distance' => 'required',
        'payment_methods' => 'required',
        'active' => 'nullable'
    ];

    public function service_type()
    {
        return $this->belongsTo(ServiceType::class);
    }

    public function payment_methods()
    {
        return $this->belongsToMany(PaymentType::class)->using(ServiceCategoryPaymentMethod::class);
    }

    public function vehicles()
    {
        return $this->hasMany(ServiceCategoryVehicleType::class);
    }

}
