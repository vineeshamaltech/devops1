<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class UserWalletTransaction
 * @package App\Models
 * @version December 15, 2021, 7:10 am UTC
 *
 * @property integer $user_id
 * @property string $type
 * @property string $remarks
 */
class UserWalletTransaction extends Model
{

    public $table = 'user_wallet_transactions';

    public $fillable = [
        'user_id',
        'type',
        'amount',
        'opening_balance',
        'closing_balance',
        'remarks'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'user_id' => 'integer',
        'type' => 'string',
        'amount' => 'double',
        'opening_balance' => 'double',
        'closing_balance' => 'double',
        'remarks' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'user_id' => 'required',
        'type' => 'required',
        'amount' => 'required',
        'opening_balance' => 'nullable',
        'closing_balance' => 'nullable',
        'remarks' => 'nullable'
    ];

    public function user(){
        return $this->belongsTo(User::class);
    }
}
