<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Casts\Attribute;
use Eloquent as Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Lab404\Impersonate\Services\ImpersonateManager;
use Laravel\Sanctum\HasApiTokens;
use Spatie\Permission\Traits\HasRoles;

/**
 * Class Admin
 * @package App\Models
 * @version November 30, 2021, 11:48 am UTC
 *
 * @property string $name
 * @property string $email
 * @property string $password
 * @property boolean $is_super
 */
class Admin extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable, HasRoles;

    public $table = 'admins';

    protected $guard_name = 'web';

    public $fillable = [
        'name',
        'email',
        'country_code',
        'mobile',
        'password',
        'fcm_token',
        'type',
        'bank_name',
        'account_no',
        'account_name',
        'ifsc_code',
        'branch_name',
        'active',
        'verified',
        'zones'
    ];

    protected $hidden = [
        'password',
        'remember_token',
        'type'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'email' => 'string',
        'password' => 'string',
        'is_super' => 'boolean'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required',
        'email' => 'required',
        'mobile' => 'required',
        'password' => 'required',
        'zones' => 'required',
        'fcm_token' => 'nullable',
        'type' => 'nullable',
        'bank_name' => 'nullable',
        'account_no' => 'nullable',
        'account_name' => 'nullable',
        'ifsc_code' => 'nullable',
        'branch_name' => 'nullable',
    ];

    public static $updateRules = [
        'zones' => 'nullable',
        'name' => 'nullable',
        'fcm_token' => 'nullable',
        'type' => 'nullable',
        'bank_name' => 'nullable',
        'account_no' => 'nullable',
        'account_name' => 'nullable',
        'ifsc_code' => 'nullable',
        'branch_name' => 'nullable',
    ];

    /**
     * Check if the current user is impersonated.
     *
     * @param void
     * @return  bool
     */
    public function isImpersonated()
    {
        return app(ImpersonateManager::class)->isImpersonating();
    }

    /**
     * @return bool
     */
    public function canImpersonate()
    {
        // For example
        return $this->type == 'Super Admin';
    }

    /**
     * @return bool
     */
    public function canBeImpersonated()
    {
        // For example
        return $this->type == 'Vendor';
    }

    /**
     * Route notifications for the FCM channel.
     *
     * @param \Illuminate\Notifications\Notification $notification
     * @return string
     */
    public function routeNotificationForFcm($notification)
    {
        return $this->fcm_token;
    }

    /**
     * Route notifications for the Msg91 channel.
     *
     * @param \Illuminate\Notifications\Notification $notification
     * @return string
     */
    public function routeNotificationForMsg91($notification)
    {
        return "91" . $this->mobile;
    }

    public function kyc()
    {
        return $this->hasMany(VendorKyc::class, 'admin_id', 'id');
    }

    protected function mobile(): Attribute
    {
        return new Attribute(
            // get: fn ($value) => $this->country_code.$value,
        );
    }

}

