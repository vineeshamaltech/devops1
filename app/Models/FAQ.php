<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class FAQ
 * @package App\Models
 * @version February 21, 2022, 11:16 am IST
 *
 * @property string $question
 * @property string $answer
 * @property boolean $active
 */
class FAQ extends Model
{

    public $table = 'faqs';

    public $fillable = [
        'question',
        'answer',
        'active'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'question' => 'string',
        'answer' => 'string',
        'active' => 'boolean'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'question' => 'required',
        'answer' => 'required',
        'active' => 'required'
    ];


}
