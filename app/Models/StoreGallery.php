<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class StoreGallery
 * @package App\Models
 * @version December 4, 2021, 12:33 pm UTC
 *
 * @property integer $store_id
 * @property string $title
 * @property string $description
 * @property string $image
 */
class StoreGallery extends Model
{

    public $table = 'store_galleries';
    



    public $fillable = [
        'store_id',
        'title',
        'description',
        'image'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'store_id' => 'integer',
        'title' => 'string',
        'description' => 'string',
        'image' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'store_id' => 'required',
        'title' => 'nullable',
        'description' => 'nullable',
        'image' => 'required'
    ];

    
}
