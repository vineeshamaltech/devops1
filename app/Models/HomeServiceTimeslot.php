<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HomeServiceTimeslot extends Model
{
    use HasFactory;
    public $table = 'home_services_timeslot';

    public $fillable = [
        'home_service_id',
        'day',
        'open',
        'close'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'home_service_id' => 'integer',
        'day' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'home_service_id' => 'required',
    ];

    public function homeService()
    {
        return $this->belongsTo(homeService::class);
    }
}
