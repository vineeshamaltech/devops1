<?php

namespace App\Repositories;

use App\Models\Review;
use Prettus\Repository\Eloquent\BaseRepository;

/**
 * Class ReviewRepository
 * @package App\Repositories
 * @version January 15, 2022, 2:07 pm IST
*/

class ReviewRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id',
        'order_id',
        'store_rating',
        'driver_rating',
        'store_review',
        'driver_review'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Review::class;
    }
}
