<?php

namespace App\Repositories;

use App\Models\Driver;
use Prettus\Repository\Eloquent\BaseRepository;

/**
 * Class DriverRepository
 * @package App\Repositories
 * @version December 6, 2021, 11:39 am UTC
*/

class DriverRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'mobile',
        'cash_in_hand',
        'vehicle_type',
        'vehicle_reg_no',
        'vehicle_color',
        'vehicle_img',
        'dl_photo',
        'aadhar_photo',
        'service_type_id',
        'vehicle_type_id',
        'bank_name',
        'account_no',
        'ifsc_code',
        'branch_name',
        'active',
        'is_verified'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }
    public function getshow()
    {

    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Driver::class;
    }
}
