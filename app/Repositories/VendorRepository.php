<?php

namespace App\Repositories;

use App\Models\Admin;
use App\Models\Vendor;
use Prettus\Repository\Eloquent\BaseRepository;

/**
 * Class VendorRepository
 * @package App\Repositories
 * @version December 2, 2021, 6:33 am UTC
*/

class VendorRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'mobile',
        'email',
        'password',
        'fcm_token',
        'active',
        'verified'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Admin::class;
    }
}
