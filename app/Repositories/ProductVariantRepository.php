<?php

namespace App\Repositories;

use App\Models\ProductVariant;
use Prettus\Repository\Eloquent\BaseRepository;

/**
 * Class ProductVariantRepository
 * @package App\Repositories
 * @version December 6, 2021, 11:39 am UTC
*/

class ProductVariantRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'product_id',
        'name',
        'price',
        'sku',
        'image'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return ProductVariant::class;
    }
}
