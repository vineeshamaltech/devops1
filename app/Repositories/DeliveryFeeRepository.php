<?php

namespace App\Repositories;

use App\Models\DeliveryFee;
use Prettus\Repository\Eloquent\BaseRepository;

/**
 * Class DeliveryFeeRepository
 * @package App\Repositories
 * @version December 10, 2021, 8:57 am UTC
*/

class DeliveryFeeRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'from_km',
        'to_km',
        'delivery_fee'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return DeliveryFee::class;
    }
}
