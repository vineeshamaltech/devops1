<?php

namespace App\Repositories;

use App\Models\ServiceCategory;
use Prettus\Repository\Eloquent\BaseRepository;

/**
 * Class ServiceCategoryRepository
 * @package App\Repositories
 * @version December 2, 2021, 10:58 am UTC
*/

class ServiceCategoryRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'icon',
        'name',
        'service_type_id',
        'description',
        'type',
        'discount',
        'commission',
        'vehicles',
        'min_price',
        'active'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return ServiceCategory::class;
    }
}
