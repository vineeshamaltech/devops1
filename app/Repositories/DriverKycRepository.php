<?php

namespace App\Repositories;

use App\Models\DriverKyc;
use Prettus\Repository\Eloquent\BaseRepository;

/**
 * Class VendorKycRepository
 * @package App\Repositories
 * @version December 2, 2021, 6:33 am UTC
*/

class DriverKycRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'driver_id',
        'doc_type',
        'photo',
        'status'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return DriverKyc::class;
    }
}
