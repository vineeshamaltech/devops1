<?php

namespace App\Repositories;

use App\Models\StoreGallery;
use Prettus\Repository\Eloquent\BaseRepository;

/**
 * Class StoreGalleryRepository
 * @package App\Repositories
 * @version December 4, 2021, 12:33 pm UTC
*/

class StoreGalleryRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'store_id',
        'title',
        'description',
        'image'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return StoreGallery::class;
    }
}
