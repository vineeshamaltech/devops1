<?php

namespace App\Repositories;

use App\Models\Earning;
use Prettus\Repository\Eloquent\BaseRepository;

/**
 * Class EarningRepository
 * @package App\Repositories
 * @version December 17, 2021, 8:43 am UTC
*/

class EarningRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'payment_id',
        'credited_to',
        'remarks',
        'is_settled'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Earning::class;
    }
}
