<?php

namespace App\Repositories;

use App\Models\Banner;
use Prettus\Repository\Eloquent\BaseRepository;

/**
 * Class BannerRepository
 * @package App\Repositories
 * @version December 4, 2021, 9:10 am UTC
*/

class BannerRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'store_id',
        'type',
        'title',
        'service_category_id',
        'home_service_category_id',
        'top_picks_enabled',
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    public function boot(){

    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Banner::class;
    }
}
