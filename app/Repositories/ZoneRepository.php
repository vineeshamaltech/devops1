<?php

namespace App\Repositories;

use App\Models\Zone;
use Prettus\Repository\Eloquent\BaseRepository;

/**
 * Class ZoneRepository
 * @package App\Repositories
 * @version February 2, 2022, 6:04 pm IST
*/

class ZoneRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id',
        'title',
        'polygon',
        'active'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Zone::class;
    }
}
