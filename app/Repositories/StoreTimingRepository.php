<?php

namespace App\Repositories;

use App\Models\StoreTiming;
use Prettus\Repository\Eloquent\BaseRepository;

/**
 * Class StoreTimingRepository
 * @package App\Repositories
 * @version December 6, 2021, 10:26 am UTC
*/

class StoreTimingRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'store_id',
        'day',
        'open',
        'close'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return StoreTiming::class;
    }
}
