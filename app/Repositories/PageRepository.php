<?php

namespace App\Repositories;

use App\Models\Page;
use Prettus\Repository\Eloquent\BaseRepository;

/**
 * Class PageRepository
 * @package App\Repositories
 * @version January 28, 2022, 3:08 pm IST
*/

class PageRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id',
        'title',
        'slug',
        'content'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Page::class;
    }
}
