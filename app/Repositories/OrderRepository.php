<?php

namespace App\Repositories;

use App\Models\Order;
use App\Models\User;
use Prettus\Repository\Eloquent\BaseRepository;

/**
 * Class OrderRepository
 * @package App\Repositories
 * @version December 13, 2021, 7:19 am UTC
*/

class OrderRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'user_id',
        'store_id',
        'coupon_data',
        'order_type',
        'total_price',
        'discount_type',
        'delivery_fee',
        'tax',
        'grand_total',
        'pickup_address',
        'delivery_address',
        'distance_travelled',
        'travel_time',
        'payment_id',
        'driver_id',
        'order_status',
        'json_data'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Order::class;
    }
}
