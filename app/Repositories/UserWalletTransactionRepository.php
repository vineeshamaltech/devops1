<?php

namespace App\Repositories;
use App\Models\User;
use App\Models\UserWalletTransaction;
use Prettus\Repository\Eloquent\BaseRepository;

/**
 * Class UserWalletTransactionRepository
 * @package App\Repositories
 * @version December 15, 2021, 7:10 am UTC
*/

class UserWalletTransactionRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'user_id',
        'type',
        'remarks'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return UserWalletTransaction::class;
    }
}
