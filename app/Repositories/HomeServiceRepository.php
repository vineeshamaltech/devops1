<?php

namespace App\Repositories;

use App\Models\HomeService;
use Prettus\Repository\Eloquent\BaseRepository;

/**
 * Class HomeServiceRepository
 * @package App\Repositories
 * @version February 4, 2022, 2:23 pm IST
*/

class HomeServiceRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'description',
        'short_description',
        'home_service_category_id',
        'image',
        'original_price',
        'discount_price'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return HomeService::class;
    }
}
