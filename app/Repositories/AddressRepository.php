<?php

namespace App\Repositories;

use App\Models\Address;
use Prettus\Repository\Eloquent\BaseRepository;

/**
 * Class AddressRepository
 * @package App\Repositories
 * @version December 6, 2021, 11:36 am UTC
*/

class AddressRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'user_id',
        'name',
        'phone',
        'address',
        'description',
        'is_default'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Address::class;
    }
}
