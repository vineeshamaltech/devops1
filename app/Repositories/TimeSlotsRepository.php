<?php

namespace App\Repositories;

use App\Models\TimeSlots;
use Prettus\Repository\Eloquent\BaseRepository;

/**
 * Class StoreTimingRepository
 * @package App\Repositories
 * @version December 6, 2021, 10:26 am UTC
*/

class TimeSlotsRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id',
        'day',
        'from',
        'to'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return TimeSlots::class;
    }
}
