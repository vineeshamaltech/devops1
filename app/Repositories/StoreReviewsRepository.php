<?php

namespace App\Repositories;

use App\Models\StoreReviews;
use Prettus\Repository\Eloquent\BaseRepository;

/**
 * Class StoreReviewsRepository
 * @package App\Repositories
 * @version December 4, 2021, 12:36 pm UTC
*/

class StoreReviewsRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'store_id',
        'title',
        'description',
        'rating'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return StoreReviews::class;
    }
}
