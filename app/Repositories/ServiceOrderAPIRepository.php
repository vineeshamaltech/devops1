<?php

namespace App\Repositories;

use App\Models\ServiceOrder;
use Prettus\Repository\Eloquent\BaseRepository;

class ServiceOrderAPIRepository extends BaseRepository
{

    protected $fieldSearchable = [
        'user_id',
        'store_id',
        'coupon_data',
        'vendor_id' ,
        'service_category_id',
        'order_type',
        'total_price',
        'discount_value',
        'delivery_fee',
        'pickup_from_store',
        'tax',
        'tip',
        'grand_total',
        'pickup_address',
        'delivery_address',
        'distance_travelled',
        'travel_time',
        'preparation_time',
        'payment_id',
        'driver_id',
        'order_status',
        'payment_status',
        'payment_method',
        'json_data',
        'pickup_otp',
        'delivery_otp'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    public function model()
    {
        return ServiceOrder::class;
    }
}
