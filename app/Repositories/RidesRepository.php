<?php

namespace App\Repositories;

use App\Models\Ride;
use Prettus\Repository\Eloquent\BaseRepository;

/**
 * Class RidesRepository
 * @package App\Repositories
 * @version January 27, 2022, 1:35 pm IST
*/

class RidesRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id',
        'user_id',
        'from_city',
        'to_city',
        'from_address',
        'to_address',
        'from_lat',
        'from_long',
        'to_lat',
        'to_long',
        'travel_datetime',
        'route',
        'can_book_instantly',
        'price',
        'status',
        'created_at',
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Ride::class;
    }

}
