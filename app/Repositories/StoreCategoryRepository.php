<?php

namespace App\Repositories;

use App\Models\StoreCategory;
use Prettus\Repository\Eloquent\BaseRepository;

/**
 * Class StoreCategoryRepository
 * @package App\Repositories
 * @version December 2, 2021, 10:59 am UTC
*/

class StoreCategoryRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'image',
        'service_category_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return StoreCategory::class;
    }
}
