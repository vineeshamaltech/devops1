<?php

namespace App\Repositories;

use App\Models\HomeServiceCategory;
use Prettus\Repository\Eloquent\BaseRepository;

/**
 * Class HomeServiceCategoryRepository
 * @package App\Repositories
 * @version February 4, 2022, 2:14 pm IST
*/

class HomeServiceCategoryRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id',
        'name',
        'icon',
        'background_image',
        'view',
        'parent',
        'tax'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return HomeServiceCategory::class;
    }
}
