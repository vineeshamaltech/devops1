<?php

namespace App\Repositories;

use App\Models\ServiceType;
use Prettus\Repository\Eloquent\BaseRepository;

/**
 * Class ServiceTypeRepository
 * @package App\Repositories
 * @version December 2, 2021, 8:27 am UTC
*/

class ServiceTypeRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return ServiceType::class;
    }
}
