<?php

namespace App\Repositories;

use App\Models\FAQ;
use Prettus\Repository\Eloquent\BaseRepository;

/**
 * Class FAQRepository
 * @package App\Repositories
 * @version February 21, 2022, 11:16 am IST
*/

class FAQRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'question',
        'answer',
        'active'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return FAQ::class;
    }
}
