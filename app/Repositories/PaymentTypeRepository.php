<?php

namespace App\Repositories;

use App\Models\PaymentType;
use Prettus\Repository\Eloquent\BaseRepository;

/**
 * Class PaymentTypeRepository
 * @package App\Repositories
 * @version December 2, 2021, 8:10 am UTC
*/

class PaymentTypeRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'description',
        'icon'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return PaymentType::class;
    }
}
