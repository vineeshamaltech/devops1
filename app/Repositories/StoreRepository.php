<?php

namespace App\Repositories;

use App\Models\Store;
use Prettus\Repository\Eloquent\BaseRepository;

/**
 * Class StoreRepository
 * @package App\Repositories
 * @version December 4, 2021, 12:13 pm UTC
*/

class StoreRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'admin_id',
        'service_category_id',
        'store_categories',
        'name' =>'like',
        'description'=>'like',
        'address',
        'delivery_range',
        'mobile',
        'email',
        'order_count',
        'is_open',
        'active'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
       // dd("in model so");
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Store::class;
    }
}
