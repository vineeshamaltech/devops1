<?php

namespace App\Repositories;

use App\Models\Admin;
use Prettus\Repository\Eloquent\BaseRepository;

/**
 * Class AdminRepository
 * @package App\Repositories
 * @version November 30, 2021, 11:48 am UTC
*/

class AdminRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'email',
        'password',
        'is_super',
        'type'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Admin::class;
    }
}
