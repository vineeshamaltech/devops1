<?php

namespace App\Repositories;

use App\Models\DriverWalletTransaction;
use Prettus\Repository\Eloquent\BaseRepository;

/**
 * Class DriverWalletTransactionRepository
 * @package App\Repositories
 * @version December 17, 2021, 8:41 am UTC
*/

class DriverWalletTransactionRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'driver_id',
        'type',
        'remarks'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return DriverWalletTransaction::class;
    }
}
