<?php

namespace App\Repositories;

use App\Models\Coupon;
use Prettus\Repository\Eloquent\BaseRepository;

/**
 * Class CouponRepository
 * @package App\Repositories
 * @version December 7, 2021, 11:27 am UTC
*/

class CouponRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'store_id',
        'image',
        'code',
        'description',
        'max_use_per_user',
        'min_cart_value',
        'start_date',
        'end_date',
        'discount_type',
        'discount_value',
        'active'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Coupon::class;
    }
}
