<?php

namespace App\Repositories;

use App\Models\VehicleType;
use Prettus\Repository\Eloquent\BaseRepository;

/**
 * Class VehicleTypeRepository
 * @package App\Repositories
 * @version December 2, 2021, 8:42 am UTC
*/

class VehicleTypeRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'icon',
        'active'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return VehicleType::class;
    }
}
