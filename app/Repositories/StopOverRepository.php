<?php

namespace App\Repositories;

use App\Models\StopOver;
use Prettus\Repository\Eloquent\BaseRepository;

/**
 * Class StopOverRepository
 * @package App\Repositories
 * @version January 27, 2022, 1:37 pm IST
*/

class StopOverRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id',
        'user_id',
        'ride_id',
        'name',
        'lat',
        'long'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return StopOver::class;
    }
}
