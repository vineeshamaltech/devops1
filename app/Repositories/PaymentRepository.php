<?php

namespace App\Repositories;

use App\Models\Payment;
use Prettus\Repository\Eloquent\BaseRepository;

/**
 * Class PaymentRepository
 * @package App\Repositories
 * @version December 9, 2021, 11:39 am UTC
*/

class PaymentRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'ref_no',
        'payment_method',
        'amount',
        'gateway_response'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Payment::class;
    }
}
