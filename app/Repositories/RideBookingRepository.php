<?php

namespace App\Repositories;

use App\Models\RideBooking;
use Prettus\Repository\Eloquent\BaseRepository;

/**
 * Class RideBookingRepository
 * @package App\Repositories
 * @version January 27, 2022, 1:38 pm IST
*/

class RideBookingRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id',
        'user_id',
        'ride_id',
        'from_name',
        'to_name',
        'from_lat',
        'from_long',
        'to_lat',
        'to_long',
        'price'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return RideBooking::class;
    }
}
