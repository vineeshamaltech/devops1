<?php

namespace App\Jobs;

use App\Mail\ExportGenerated;
use App\Mail\FileImported;
use App\Mail\ImportExportFailed;
use App\Models\Category;
use App\Models\Product;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;

class ImportJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $file;
    private $type;
    private $email;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($file, $type, $email)
    {
        $this->file = $file;
        $this->type = $type;
        $this->email = $email;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try{
            if ($this->type == 'category'){
                $this->importCategories($this->file);
            } elseif ($this->type == 'product'){
                $this->importProducts($this->file);
            }
        }catch (\Exception $e){
            Mail::to($this->email)
                ->queue(new ImportExportFailed('Import failed','The file you submitted for import has failed. Please try again.',$this->file));
        }
    }

    public function importCategories($file){
        $file = public_path($file);
        $success_file = fopen(str_replace('.csv','',$file). '_success.csv', 'w');
        $error_file = fopen(str_replace('.csv','',$file). '_error.csv', 'w');
        $file = fopen($file, 'r');
        $categories = [];
        $keys = [];
        $i = 0;
        while(!feof($file)){
            if ($i==0){
                $i++;
                $keys = fgetcsv($file);
                continue;
            }
            $line = fgetcsv($file);
            if (!empty($line)){
                $categories[] = $line;
            }
        }
        fclose($file);
        fputcsv($success_file, $keys);
        fputcsv($error_file, $keys);
        $categories = collect($categories);
        $categories->each(function ($category) use ($keys,$error_file,$success_file){
            $category = collect($category);
            if (in_array('id', $keys) !== false){
                //already in database, update record
                try {
                    $cat = Category::find(array_search('id', $keys));
                    $cat->name = $category[array_search('name', $keys)];
                    $cat->image = $category[array_search('image', $keys)];
                    $cat->parent = $category[array_search('parent', $keys)];
                    $cat->store_id = $category[array_search('store_id', $keys)];
                    $cat->save();
                    fputcsv($success_file, $category->toArray());
                }catch (\Exception $e){
                    $category[] = 'Error: ' . $e->getMessage();
                    fputcsv($error_file, $category->toArray());
                }
            }else{
                //id does not exist, create new record
                try {
                    $cat = new Category();
                    $cat->name = $category[array_search('name', $keys)];
                    $cat->image = $category[array_search('image', $keys)];
                    $cat->parent = $category[array_search('parent', $keys)];
                    $cat->store_id = $category[array_search('store_id', $keys)];
                    $cat->save();
                    fputcsv($success_file, $category->toArray());
                }catch (\Exception $e){
                    $category[] = 'Error: ' . $e->getMessage();
                    fputcsv($error_file, $category->toArray());
                }
            }
        });
        fclose($success_file);
        fclose($error_file);
        Mail::to($this->email)
            ->queue(new FileImported($success_file,$error_file));
    }

    public function importProducts($file){
        $file = public_path($file);
        $success_file = fopen(str_replace('.csv','',$file). '_success.csv', 'w');
        $error_file = fopen(str_replace('.csv','',$file). '_error.csv', 'w');
        $file = fopen($file, 'r');
        $products = [];
        $keys = [];
        $i = 0;
        while(!feof($file)){
            if ($i==0){
                $i++;
                $keys = fgetcsv($file);
                continue;
            }
            $line = fgetcsv($file);
            if (!empty($line)){
                $products[] = $line;
            }
        }
        fclose($file);
        fputcsv($success_file, $keys);
        fputcsv($error_file, $keys);
        $products = collect($products);
        $products->each(function ($product) use ($keys,$error_file,$success_file){
            $product = $this->format_array(collect($product)->toArray(),$keys);
            if (in_array('id', $keys) !== false){
                //already in database, update record
                try {
                    $prod = Product::find(array_search('id', $keys));
                    unset($keys['id']);
                    array_shift($product);
                    Product::where('id',$prod->id)->update($product);
                    fputcsv($success_file, $product);
                }catch (\Exception $e){
                    $product[] = 'Error: ' . $e->getMessage();
                    fputcsv($error_file, $product);
                }
            }else{
                //id does not exist, create new record
                try {
                    $prod = Product::create($product);
                    fputcsv($success_file, $prod->toArray());
                }catch (\Exception $e){
                    $category[] = 'Error: ' . $e->getMessage();
                    fputcsv($error_file, $product->toArray());
                }
            }
        });
        fclose($success_file);
        fclose($error_file);
        Mail::to($this->email)
            ->queue(new FileImported($success_file,$error_file));
    }

    public function format_array($arr,$keys) : array{
        $new_arr = [];
        foreach ($arr as $key => $value){
            $new_arr[$keys[$key]] = $value;
        }
        return $new_arr;
    }
}
