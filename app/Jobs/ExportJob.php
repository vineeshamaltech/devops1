<?php

namespace App\Jobs;

use App\Mail\ExportGenerated;
use App\Mail\ImportExportFailed;
use App\Models\Category;
use App\Models\Product;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Schema;
use Laracasts\Flash\Flash;

class ExportJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    private $type;
    private $email;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($type,$email)
    {
        $this->type = $type;
        $this->email = $email;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(){
        $tablename = null;
        switch ($this->type){
            case 'categories':
                $tablename = "categories";
                break;
            case 'products':
                $tablename = "products";
                break;
            case 'coupon':
                $tablename = "coupons";
                break;
            case 'order':
                $tablename = "orders";
                break;
            case 'order_item':
                $tablename = "order_items";
                break;
            case 'customer':
                $tablename = "users";
                break;
            case 'address':
                $tablename = "addresses";
                break;
            case 'banners':
                $tablename = "banners";
                break;
            case 'drivers':
                $tablename = "drivers";
                break;
            case 'payments':
                $tablename = "payments";
                break;
            case 'stores':
                $tablename = "stores";
                break;
            case 'store_timings':
                $tablename = "store_timings";
                break;
            default:
                Flash::error('Invalid type');
                throw new \Exception('Invalid type');
        }
        try{
            $fileName = $tablename.time()."-".rand(1,9999).'.csv';
            $destinationPath = public_path('/storage/exports/'.$fileName);
            $columns = Schema::getColumnListing($tablename);
            $file = fopen($destinationPath, 'w');
            fputcsv($file, $columns);
            DB::table($tablename)->orderBy('id')->chunk(100, function ($alldata) use ($file) {
                foreach ($alldata as $row) {
                    fputcsv($file, (array) $row);
                }
            });
            fclose($file);
            Mail::to($this->email)
                ->queue(new ExportGenerated($destinationPath));
            //unlink($this->exportReq['file_path']);
        }catch (\Exception $e){
            Mail::to($this->email)
                ->queue(new ImportExportFailed('Export failed','The file you requested for export has failed. Please try again.'));
        }
    }
}
