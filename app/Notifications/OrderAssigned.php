<?php

namespace App\Notifications;

use Benwilkins\FCM\FcmMessage;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class OrderAssigned extends Notification implements ShouldQueue
{
    use Queueable;
    protected $title;
    protected $order;
    protected $is_auto;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($title,$order,$is_auto = true)
    {
        $this->title = $title;
        $this->order = $order;
        $this->is_auto = $is_auto;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['fcm','database'];
    }

    public function toFcm($notifiable)
    {
        $message = new FcmMessage();
        $message->content([
            'title'        => $this->title."",
            'body'         => 'Click here to see the order details',
            'sound'        => '', // Optional
            'icon'         => '', // Optional
            'click_action' => '' // Optional
        ])->data([
            'type' => 'order_assigned',
            'is_auto_assigned' => $this->is_auto,
            'click_action' => 'FLUTTER_NOTIFICATION_CLICK',
            'id' => $this->order->id
        ])->priority(FcmMessage::PRIORITY_HIGH); // Optional - Default is 'normal'.

        return $message;
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'title' => '# '.$this->order->id.' Order assigned to you',
            'id' => $this->order->id
        ];
    }
}
