<?php

namespace App\Notifications;

use Craftsys\Notifications\Messages\Msg91SMS;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Notification;
use Benwilkins\FCM\FcmMessage;

class NewOrder extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * @var Order
     */
    private $order;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($order)
    {
        //
        $this->order = $order;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        //return ['database','fcm','msg91'];
        return ['database','fcm'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line('The introduction to the notification.')
                    ->action('Notification Action', url('/'))
                    ->line('Thank you for using our application!');
    }

    public function toFcm($notifiable)
    {
        $message = new FcmMessage();

        $message->content([
            'title'        => 'Order placed successfully',
            'body'         => 'Click here to see the order details',
            'sound'        => '', // Optional
            'icon'         => '', // Optional
            'click_action' => '' // Optional
        ])->data([
            'page' => 'order',
            'click_action' => 'FLUTTER_NOTIFICATION_CLICK',
            'date' => $this->order->id
        ])->priority(FcmMessage::PRIORITY_HIGH); // Optional - Default is 'normal'.

        return $message;
    }

    /**
     * Get the Msg91 / SMS representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Craftsys\Notifications\Messages\Msg91SMS
     */
    public function toMsg91($notifiable)
    {
        return (new Msg91SMS)
            ->flow('6156e7d8815ece592568c0b6')
            ->variable('order_id', $this->order->id)
            ->variable('name',$this->order->user->name ?? "User")
            ->variable('amount',$this->order->grand_total)
            ->variable('quantity',"1");
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'order_id' => $this->order->id,
            'status' => $this->order->status,
        ];
    }
}
