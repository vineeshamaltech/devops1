<?php

namespace App\Notifications;

use Benwilkins\FCM\FcmMessage;
use Craftsys\Notifications\Messages\Msg91SMS;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class VendorVerified extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail','fcm'];
    }

    public function toFcm($notifiable)
    {
        $message = new FcmMessage();
        $message->content([
            'title'        => 'Congratulations! Your account has been verified.',
            'body'         => 'You can now use the app to add products and manage your orders.',
            'sound'        => '', // Optional
            'icon'         => '', // Optional
            'click_action' => '' // Optional
        ])->data([
            'param1' => 'baz', // Optional
            'click_action' => 'FLUTTER_NOTIFICATION_CLICK'
        ])->priority(FcmMessage::PRIORITY_HIGH); // Optional - Default is 'normal'.
        return $message;
    }

    /**
     * Get the Msg91 / SMS representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Craftsys\Notifications\Messages\Msg91SMS
     */
    public function toMsg91($notifiable)
    {
        return (new Msg91SMS)
            ->flow('6156e7d8815ece592568c0b6')
            ->variable('order_id', $this->order->id)
            ->variable('name',$this->order->store->name ?? "Store")
            ->variable('amount',$this->order->total)
            ->variable('quantity',count($this->order->items));
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line('Your account has been verified.')
                    ->line('You can now use the app to add products and manage your orders.')
                    ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
