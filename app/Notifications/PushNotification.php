<?php

namespace App\Notifications;

use Benwilkins\FCM\FcmMessage;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class PushNotification extends Notification implements ShouldQueue
{
    use Queueable;

    private $data;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['fcm','database'];
    }

    public function toFcm($notifiable)
    {
        $message = new FcmMessage();
        $message->content([
            'title'        => $this->data['title'],
            'body'         => $this->data['description'],
            'sound'        => '', // Optional
            'icon'         => '', // Optional
            'big_picture'  => $this->data['image'] ?? "",
            'click_action' => '' // Optional
        ])->data([
            'image' => $this->data['image'] ?? "",
            'click_action' => 'FLUTTER_NOTIFICATION_CLICK',
            'page' => 'notification' // Optional
        ])->priority(FcmMessage::PRIORITY_HIGH);
       
        return $message;
    }


    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {

        return [
            'title' => $this->data['title'],
            'body' => $this->data['description'],
            'data' => [
                'image'=>$this->data['image'] ?? "",
                'page' => 'notification' // Optional
            ]
        ];
    }
}
