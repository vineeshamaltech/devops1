<?php

namespace App\Notifications;

use App\Mail\InvoiceEmail;
use App\Mail\OrderStatusChanged;
use App\Models\DriverCashTransaction;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use App\Helpers\Utils;

class OrderInvoice extends Notification implements ShouldQueue
{
    use Queueable;

    public $order;
    public $invoice_file_name;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($order,$invoice_file_name)
    {
        $this->invoice_file_name = $invoice_file_name;
        $this->order = $order;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return InvoiceEmail
     */
    public function toMail($notifiable)
    {
//        return (new MailMessage)
//                    ->view('emails.invoice',['order'=>$this->order])
//                    ->attach(public_path().'/storage/invoice/'.$this->invoice_file_name, [
//                        'as' => 'invoice.pdf',
//                        'mime' => 'application/pdf',
//                    ]);
        $cashTransactions = DriverCashTransaction::where('order_id',$this->order->id)->get();
        $mail = (new InvoiceEmail($this->order))
            ->to($notifiable->email);
        foreach ($cashTransactions as $cashTransaction){
            foreach ($cashTransaction->images as $image){
                $mail->attach(public_path().$image);
            }
        }
        $mail->attach(public_path().'/storage/invoice/'.$this->invoice_file_name, [
                'as' => 'invoice.pdf',
                'mime' => 'application/pdf',
            ]);
        return $mail;
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
