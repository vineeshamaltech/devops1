<?php

namespace App\Notifications;

use Benwilkins\FCM\FcmMessage;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class KycRejected extends Notification implements ShouldQueue
{
    use Queueable;

    private $kyc;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($kyc)
    {
        $this->kyc = $kyc;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['fcm','database'];
    }

    public function toFcm($notifiable)
    {
        $message = new FcmMessage();
        $message->content([
            'title'        => 'KYC Document Rejected',
            'body'         => 'Your KYC document ('.$this->kyc->doc_type.') has been rejected. Please upload a new document.',
            'sound'        => '', // Optional
            'icon'         => '', // Optional
            'click_action' => '' // Optional
        ])->data([
            'screen' => 'kyc_rejected',
            'click_action' => 'FLUTTER_NOTIFICATION_CLICK',
            'data' => $this->kyc->id
        ])->priority(FcmMessage::PRIORITY_HIGH); // Optional - Default is 'normal'.
      
        return $message;
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'title' => 'Document Rejected',
            'body' => 'Your KYC document ('.$this->kyc->doc_type.') has been rejected. Please upload a new document.',
        ];
    }
}
