<?php

namespace App\Notifications;

use Benwilkins\FCM\FcmMessage;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class KycStatusChanged extends Notification implements ShouldQueue
{
    use Queueable;

    private $kyc;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($kyc)
    {
        $this->kyc = $kyc;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['fcm','database'];
    }

    public function toFcm($notifiable)
    {
        if ($this->kyc->status == 'APPROVED') {
            $title = 'KYC Approved';
            $body = 'Your KYC document ('.$this->kyc->doc_type.') has been approved.';
            $screen = 'kyc';
        } else {
            $title = 'KYC Rejected';
            $body = 'Your KYC document ('.$this->kyc->doc_type.') has been rejected. Please upload a new document.';
            $screen = 'kyc_rejected';
        }
        $message = new FcmMessage();
        $message->content([
            'title'        => $title,
            'body'         => $body,
            'sound'        => '', // Optional
            'icon'         => '', // Optional
            'click_action' => '' // Optional
        ])->data([
            'screen' => $screen,
            'click_action' => 'FLUTTER_NOTIFICATION_CLICK',
            'data' => $this->kyc->id
        ])->priority(FcmMessage::PRIORITY_HIGH); // Optional - Default is 'normal'.

        return $message;
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        if ($this->kyc->status == 'APPROVED') {
            $title = 'KYC Approved';
            $body = 'Your KYC document ('.$this->kyc->doc_type.') has been approved.';
            $screen = 'kyc';
        } else {
            $title = 'KYC Rejected';
            $body = 'Your KYC document ('.$this->kyc->doc_type.') has been rejected. Please upload a new document.';
            $screen = 'kyc_rejected';
        }
        return [
            'title' => $title,
            'body' => $body,
            'screen' => $screen,
        ];
    }
}
