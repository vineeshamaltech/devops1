<?php

namespace App\Criteria\Store;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class StoreOfVendorCriteria.
 *
 * @package namespace App\Criteria;
 */
class StoreOfVendorCriteria implements CriteriaInterface
{
    /**
     * Apply criteria in query repository
     *
     * @param string              $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        return $model->where('admin_id', auth()->user()->id);
    }
}
