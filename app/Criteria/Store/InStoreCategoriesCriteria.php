<?php

namespace App\Criteria\Store;

use Illuminate\Http\Request;
use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class InStoreCategoriesCriteria.
 *
 * @package namespace App\Criteria\Store;
 */
class InStoreCategoriesCriteria implements CriteriaInterface
{


    /**
     * @var array
     */
    private $request;

    /**
     * NearCriteria constructor.
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * Apply criteria in query repository
     *
     * @param string              $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        if($this->request->has('store_categories')){
            return $model->whereRaw("FIND_IN_SET(".$this->request->store_categories.", store_categories)");
        }else{
            return $model;
        }

    }
}
