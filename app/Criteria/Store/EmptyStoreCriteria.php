<?php

namespace App\Criteria\Store;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class EmptyStoreCriteria.
 *
 * @package namespace App\Criteria;
 */
class EmptyStoreCriteria implements CriteriaInterface
{
    /**
     * Apply criteria in query repository
     *
     * @param string              $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        return $model->withCount('products')->having('products_count', '>', 0);
    }
}
