<?php

namespace App\Criteria\Store;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class NearCriteria.
 *
 * @package namespace App\Criteria\Markets;
 */
class NearCriteria implements CriteriaInterface
{

    /**
     * @var array
     */
    private $request;

    /**
     * NearCriteria constructor.
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * Apply criteria in query repository
     *
     * @param string $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        if ($this->request->has(['lat', 'long',])) {
            $myLat = $this->request->get('lat');
            $myLon = $this->request->get('long');
            $data = $model->select(DB::raw("SQRT(
                POW(69.1 * (latitude - $myLat), 2) +
                POW(69.1 * ($myLon - longitude) * COS(latitude / 57.3), 2)) * 1.609 AS distance"), "stores.*")
                ->having('distance', '>=', 'stores.delivery_range')
                ->orderBy('distance', 'asc');
            return $data;

        } else {
            return $model->orderBy('is_open');
        }
    }
}
