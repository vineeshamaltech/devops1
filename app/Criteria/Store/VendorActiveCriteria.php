<?php

namespace App\Criteria\Store;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class VendorActiveCriteria.
 *
 * @package namespace App\Criteria\Store;
 */
class VendorActiveCriteria implements CriteriaInterface
{
    /**
     * Apply criteria in query repository
     *
     * @param string              $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        return $model->whereHas('vendor', function ($query) {
            $query->where('active', 1)->where('verified',1);
        });
    }
}
