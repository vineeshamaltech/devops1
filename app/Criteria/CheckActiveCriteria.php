<?php

namespace App\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;
use Carbon\Carbon;
/**
 * Class ActiveCriteria.
 *
 * @package namespace App\Criteria;
 */
class CheckActiveCriteria implements CriteriaInterface
{
    /**
     * Apply criteria in query repository
     *
     * @param string              $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
      $data= $model->where('fcm_token','!=',null)->where('updated_at', '>', Carbon::now()->subMinutes(5)->toDateTimeString());

      return $data;
    }
}
