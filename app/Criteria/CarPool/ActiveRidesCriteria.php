<?php

namespace App\Criteria\CarPool;

use Carbon\Carbon;
use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class ActiveRidesCriteria.
 *
 * @package namespace App\Criteria;
 */
class ActiveRidesCriteria implements CriteriaInterface
{

    private $request;

    public function __construct($request)
    {
        $this->request = $request;
    }

    /**
     * Apply criteria in query repository
     *
     * @param string              $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        if ($this->request->has('passengers')){
            return $model->where('active', 1)->where('status','PUBLISHED')->whereDate('rides.travel_datetime','=',Carbon::parse($this->request->date))->whereRaw(
                'max_seats >= rides.passenger_count + '.$this->request->passengers
            );
        }else{
            return $model->where('active', 1)->where('status','PUBLISHED')->whereDate('rides.travel_datetime','=',Carbon::parse($this->request->date))->whereRaw(
                'max_seats >= rides.passenger_count'
            );
        }

    }
}
