<?php

namespace App\Criteria\HomeService;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class NearCriteria.
 *
 * @package namespace App\Criteria\Markets;
 */
class InZoneCriteria implements CriteriaInterface
{

    private $zoneId;

    /**
     * NearCriteria constructor.
     */
    public function __construct($zoneId)
    {
        $this->zoneId = $zoneId;
    }

    /**
     * Apply criteria in query repository
     *
     * @param string $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
      
        if ($this->zoneId!=null) {
            $re = $model->whereRaw('FIND_IN_SET('.$this->zoneId.', zones)');
           return $re;
        } else {
            return $model;
        }
    }
}
