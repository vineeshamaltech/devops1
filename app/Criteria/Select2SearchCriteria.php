<?php

namespace App\Criteria;

use Illuminate\Http\Request;
use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class Select2SearchCriteria.
 *
 * @package namespace App\Criteria;
 */
class Select2SearchCriteria implements CriteriaInterface
{
    private $request;

    /**
     * NearCriteria constructor.
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * Apply criteria in query repository
     *
     * @param string              $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        if ($this->request->has(['q'])) {
            return $model->where('name', 'LIKE', '%' . request('q') . '%')->orWhere('mobile', 'LIKE', '%' . request('q') . '%');
        }else{
            return $model;
        }

    }
}
