<?php

namespace App\Http\Requests\API;

use App\Models\Ride;
use InfyOm\Generator\Request\APIRequest;

class UpdateRidesAPIRequest extends APIRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = Ride::$rules;

        return $rules;
    }
}
