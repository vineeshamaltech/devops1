<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Auth;
use Debugbar;

class IsAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if(!Auth::check()){
            return redirect()->route('login');
        }

        if(Auth::user()->hasRole('admin') || Auth::user()->hasRole('Super Admin')){
            return $next($request);
        }
        return redirect()->back()->with('unauthorised', 'You are
                  unauthorised to access this page');
    }
}
