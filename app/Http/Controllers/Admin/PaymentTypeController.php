<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\PaymentTypeDataTable;
use App\Http\Requests;
use App\Http\Requests\CreatePaymentTypeRequest;
use App\Http\Requests\UpdatePaymentTypeRequest;
use App\Repositories\PaymentTypeRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class PaymentTypeController extends AppBaseController
{
    /** @var  PaymentTypeRepository */
    private $paymentTypeRepository;

    public function __construct(PaymentTypeRepository $paymentTypeRepo)
    {
        $this->paymentTypeRepository = $paymentTypeRepo;
    }

    /**
     * Display a listing of the PaymentType.
     *
     * @param PaymentTypeDataTable $paymentTypeDataTable
     * @return Response
     */
    public function index(PaymentTypeDataTable $paymentTypeDataTable)
    {
        abort_if(\Gate::denies('paymentMethods.index'), 403, '403 Forbidden');
        return $paymentTypeDataTable->render('admin.payment_types.index');
    }

    /**
     * Show the form for creating a new PaymentType.
     *
     * @return Response
     */
    public function create()
    {
        abort_if(\Gate::denies('paymentMethods.create'), 403, '403 Forbidden');
        return view('admin.payment_types.create');
    }

    /**
     * Store a newly created PaymentType in storage.
     *
     * @param CreatePaymentTypeRequest $request
     *
     * @return Response
     */
    public function store(CreatePaymentTypeRequest $request)
    {
        abort_if(\Gate::denies('paymentMethods.create'), 403, '403 Forbidden');
        $input = $request->all();
        if($request->service == 'on'){
            $input['service'] = 1;
        }else{
            $input['service'] = 0;
        }
        if($request->home_service == 'on'){
            $input['home_service'] = 1;
        }else{
            $input['home_service'] = 0;
        }
        if($request->hasFile('icon')){
            $input['icon'] = \Helpers::uploadImage($request->file('icon'), 'payment_types');
        }
        $paymentType = $this->paymentTypeRepository->create($input);

        Flash::success(__('lang.messages.saved', ['model' => __('models/paymentTypes.singular')]));

        return redirect(route('admin.paymentTypes.index'));
    }

    /**
     * Display the specified PaymentType.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        abort_if(\Gate::denies('paymentMethods.view'), 403, '403 Forbidden');
        $paymentType = $this->paymentTypeRepository->find($id);

        if (empty($paymentType)) {
            Flash::error(__('lang.messages.not_found', ['model' => __('models/paymentTypes.singular')]));

            return redirect(route('admin.paymentTypes.index'));
        }

        return view('admin.payment_types.show')->with('paymentType', $paymentType);
    }

    /**
     * Show the form for editing the specified PaymentType.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        abort_if(\Gate::denies('paymentMethods.edit'), 403, '403 Forbidden');
        $paymentType = $this->paymentTypeRepository->find($id);

        if (empty($paymentType)) {
            Flash::error(__('lang.messages.not_found', ['model' => __('models/paymentTypes.singular')]));

            return redirect(route('admin.paymentTypes.index'));
        }

        return view('admin.payment_types.edit')->with('paymentType', $paymentType);
    }

    /**
     * Update the specified PaymentType in storage.
     *
     * @param  int              $id
     * @param UpdatePaymentTypeRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePaymentTypeRequest $request)
    {
        abort_if(\Gate::denies('paymentMethods.edit'), 403, '403 Forbidden');
        $paymentType = $this->paymentTypeRepository->find($id);
        $input = $request->all();
        if($request->service == 'on'){
            $input['service'] = 1;
        }else{
            $input['service'] = 0;
        }
        if($request->home_service == 'on'){
            $input['home_service'] = 1;
        }else{
            $input['home_service'] = 0;
        }
        if (empty($paymentType)) {
            Flash::error(__('lang.messages.not_found', ['model' => __('models/paymentTypes.singular')]));
            return redirect(route('admin.paymentTypes.index'));
        }

        if($request->hasFile('icon')){
            \Helpers::deleteImage($paymentType->icon);
            $input['icon'] = \Helpers::uploadImage($request->file('icon'), 'payment_types');
        }
        $paymentType = $this->paymentTypeRepository->update($input, $id);
        Flash::success(__('lang.messages.updated', ['model' => __('models/paymentTypes.singular')]));
        return redirect(route('admin.paymentTypes.index'));
    }

    /**
     * Remove the specified PaymentType from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        abort_if(\Gate::denies('paymentMethods.delete'), 403, '403 Forbidden');
        $paymentType = $this->paymentTypeRepository->find($id);

        if (empty($paymentType)) {
            Flash::error(__('lang.messages.not_found', ['model' => __('models/paymentTypes.singular')]));
            return redirect(route('admin.paymentTypes.index'));
        }

        $this->paymentTypeRepository->delete($id);
        Flash::success(__('lang.messages.deleted', ['model' => __('models/paymentTypes.singular')]));
        return redirect(route('admin.paymentTypes.index'));
    }
}
