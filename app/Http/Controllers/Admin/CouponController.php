<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\CouponDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateCouponRequest;
use App\Http\Requests\UpdateCouponRequest;
use App\Models\Store;
use App\Repositories\CouponRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Response;

class CouponController extends AppBaseController
{
    /** @var  CouponRepository */
    private $couponRepository;

    public function __construct(CouponRepository $couponRepo)
    {
        $this->couponRepository = $couponRepo;
    }

    /**
     * Display a listing of the Coupon.
     *
     * @param CouponDataTable $couponDataTable
     * @return Response
     */
    public function index(CouponDataTable $couponDataTable)
    {
        abort_if(\Gate::denies('coupons.index'), 403, '403 Forbidden');
        return $couponDataTable->render('admin.coupons.index');
    }

    /**
     * Show the form for creating a new Coupon.
     *
     * @return Response
     */
    public function create()
    {
        abort_if(\Gate::denies('coupons.create'), 403, '403 Forbidden');
        $stores = \Helpers::get_user_stores();
        $stores = Store::whereIn('id', $stores)->get();
        return view('admin.coupons.create',compact('stores'));
    }

    /**
     * Store a newly created Coupon in storage.
     *
     * @param CreateCouponRequest $request
     *
     * @return Response
     */
    public function store(CreateCouponRequest $request)
    {
        abort_if(\Gate::denies('coupons.create'), 403, '403 Forbidden');
        $input = $request->all();
        if ($request->hasFile('image')) {
            $input['image'] = \Helpers::uploadImage($request->file('image'),'coupons');
        }
        // if($request->discount_type == 'FREE DELIVERY')
        //     $input['discount_value'] = 0;
        $input['start_date'] = Carbon::parse($request->start_date)->toDateTimeString();
        $input['end_date'] = Carbon::parse($request->end_date)->toDateTimeString();
        $coupon = $this->couponRepository->create($input);

        Flash::success(__('lang.messages.saved', ['model' => __('models/coupons.singular')]));

        return redirect(route('admin.coupons.index'));
    }

    /**
     * Display the specified Coupon.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        abort_if(\Gate::denies('coupons.view'), 403, '403 Forbidden');
        $coupon = $this->couponRepository->find($id);

        if (empty($coupon)) {
            Flash::error(__('lang.messages.not_found', ['model' => __('models/coupons.singular')]));

            return redirect(route('admin.coupons.index'));
        }

        return view('admin.coupons.show')->with('coupon', $coupon);
    }

    /**
     * Show the form for editing the specified Coupon.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        abort_if(\Gate::denies('coupons.edit'), 403, '403 Forbidden');
        $coupon = $this->couponRepository->find($id);
        if (empty($coupon)) {
            Flash::error(__('lang.messages.not_found', ['model' => __('models/coupons.singular')]));
            return redirect(route('admin.coupons.index'));
        }
        $coupon->start_date = Carbon::parse($coupon->start_date)->format('m/d/Y H:i:s');
        $coupon->end_date = Carbon::parse($coupon->end_date)->format('m/d/Y H:i:s');
        $stores = \Helpers::get_user_stores();
        $stores = Store::whereIn('id', $stores)->get();
        return view('admin.coupons.edit',compact('stores','coupon'));
    }

    /**
     * Update the specified Coupon in storage.
     *
     * @param  int              $id
     * @param UpdateCouponRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateCouponRequest $request)
    {
        abort_if(\Gate::denies('coupons.edit'), 403, '403 Forbidden');
        $coupon = $this->couponRepository->find($id);

        if (empty($coupon)) {
            Flash::error(__('lang.messages.not_found', ['model' => __('models/coupons.singular')]));
            return redirect(route('admin.coupons.index'));
        }

        $input = $request->all();
        if ($request->hasFile('image')) {
            \Helpers::deleteImage($coupon->image);
            $input['image'] = \Helpers::uploadImage($request->file('image'),'coupons');
        }
        //  if($request->discount_type == 'FREE DELIVERY')
        //     $input['discount_value'] = 0;
        $input['start_date'] = Carbon::parse($request->start_date)->toDateTimeString();
        $input['end_date'] = Carbon::parse($request->end_date)->toDateTimeString();

        $coupon = $this->couponRepository->update($input, $id);
        Flash::success(__('lang.messages.updated', ['model' => __('models/coupons.singular')]));
        return redirect(route('admin.coupons.index'));
    }

    /**
     * Remove the specified Coupon from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        abort_if(\Gate::denies('coupons.delete'), 403, '403 Forbidden');
        $coupon = $this->couponRepository->find($id);

        if (empty($coupon)) {
            Flash::error(__('lang.messages.not_found', ['model' => __('models/coupons.singular')]));
            return redirect(route('admin.coupons.index'));
        }

        $this->couponRepository->delete($id);
        Flash::success(__('lang.messages.deleted', ['model' => __('models/coupons.singular')]));
        return redirect(route('admin.coupons.index'));
    }

    /**
     * Remove the list of Categories from storage.
     *
     * @param  array $ids
     *
     * @return Response
     */
    public function destroy_multiple(Request $request)
    {
        abort_if(\Gate::denies('coupons.delete'), 403, '403 Forbidden');
        if($request->delete){
            $coupons = $this->couponRepository->findWhereIn('id',$request->delete);
            if (empty($coupons)) {
                Flash::error(__('lang.messages.not_found', ['model' => __('models/coupons.singular')]));
                return redirect(route('admin.coupons.index'));
            }
            foreach($coupons as $coupon){
                $this->couponRepository->delete($coupon->id);
            }
            Flash::success(__('lang.messages.deleted', ['model' => __('models/coupons.singular')]));
            return redirect(route('admin.coupons.index'));
        }else{
            Flash::error(__('lang.messages.no_data_selected', ['model' => __('models/coupons.singular')]));
            return redirect(route('admin.coupons.index'));
        }
        
    }
}
