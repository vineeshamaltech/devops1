<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AppBaseController;
use App\Mail\SendMail;
use App\Models\Admin;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Laracasts\Flash\Flash;

class SendOutMailController extends AppBaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        \Log::info('This is in sendout mail controll  request');
        return view('admin.sendmail.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function send(Request $request){
        if($request->type == 'all'){
            $request->validate([
                'type' => 'required',
                'subject' => 'required',
                'body' => 'required',
            ]);
        }else{
            $request->validate([
                'type' => 'required',
                'recepient' => 'required',
                'subject' => 'required',
                'body' => 'required',
            ]);
        }
        $subject = $request->subject;
        $body = $request->body;
        $type=$request->type;
        if($type == 'all'){
            $recepient = Admin::whereIn('type',['Admin','Vendor'])->select('email')->get();
            foreach($recepient as $to){
                Mail::to($to)
                ->queue(new SendMail($subject,$body));
            }
        }
        else if($type == 'admin')
        {
            if(in_array("0",$request->recepient)){
                $recepient = Admin::where('type','Admin')->get();    
            }else{
                $recepientIds=$request->recepient;
                $recepient = Admin::whereIn('id',$recepientIds)->select('email')->get();
            }
            foreach($recepient as $to){
                Mail::to($to)
                ->queue(new SendMail($subject,$body));
            }
        }
        else if($type == 'vendor')
        {
            if(in_array("0",$request->recepient)){
                $recepient = Admin::where('type','Vendor')->get();    
            }else{
                $recepientIds=$request->recepient;
                $recepient = Admin::whereIn('id',$recepientIds)->select('email')->get();
            }
            foreach($recepient as $to){
                Mail::to($to)
                ->queue(new SendMail($subject,$body));
            }
        }
        else if($type == 'customer')
        {
          if(in_array('0',$request->recepient)){
            $recepient = User::where('email','!=',null)->get();
        } else {
            $recepientIds=$request->recepient;
            $recepient = User::whereIn('id',$recepientIds)->select('email')->get();
        }
        // dd($recepient);
        foreach($recepient as $to){
           $res = Mail::to($to)
            ->queue(new SendMail($subject,$body));
        
        }
    }
         Flash::success('Mail has been sent successfully.');
         return redirect(route('admin.sendmails.index'));
         }
}
