<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\VehicleTypeDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateVehicleTypeRequest;
use App\Http\Requests\UpdateVehicleTypeRequest;
use App\Repositories\VehicleTypeRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Response;

class VehicleTypeController extends AppBaseController
{
    /** @var  VehicleTypeRepository */
    private $vehicleTypeRepository;

    public function __construct(VehicleTypeRepository $vehicleTypeRepo)
    {
        $this->vehicleTypeRepository = $vehicleTypeRepo;
    }

    /**
     * Display a listing of the VehicleType.
     *
     * @param VehicleTypeDataTable $vehicleTypeDataTable
     * @return Response
     */
    public function index(VehicleTypeDataTable $vehicleTypeDataTable)
    {
        \Log::info('This is in vehicle type controll  request');
        abort_if(\Gate::denies('vehicleTypes.index'), 403, '403 Forbidden');
        return $vehicleTypeDataTable->render('admin.vehicle_types.index');
    }

    /**
     * Show the form for creating a new VehicleType.
     *
     * @return Response
     */
    public function create()
    {
        abort_if(\Gate::denies('vehicleTypes.create'), 403, '403 Forbidden');
        return view('admin.vehicle_types.create');
    }

    /**
     * Store a newly created VehicleType in storage.
     *
     * @param CreateVehicleTypeRequest $request
     *
     * @return Response
     */
    public function store(CreateVehicleTypeRequest $request)
    {
        abort_if(\Gate::denies('vehicleTypes.create'), 403, '403 Forbidden');
        $input = $request->all();
        if($request->hasFile('icon')){
            $input['icon'] = \Helpers::uploadImage($request->file('icon'), 'vehicle_types');
        }
        if ($request->hasFile('marker_icon')) {
            $input['marker_icon'] = \Helpers::uploadImage($request->file('marker_icon'), 'vehicle_types');
        }
        $vehicleType = $this->vehicleTypeRepository->create($input);
        Flash::success(__('lang.messages.saved', ['model' => __('models/vehicleTypes.singular')]));

        return redirect(route('admin.vehicleTypes.index'));
    }

    /**
     * Display the specified VehicleType.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        abort_if(\Gate::denies('vehicleTypes.view'), 403, '403 Forbidden');
        $vehicleType = $this->vehicleTypeRepository->find($id);

        if (empty($vehicleType)) {
            Flash::error(__('lang.messages.not_found', ['model' => __('models/vehicleTypes.singular')]));

            return redirect(route('admin.vehicleTypes.index'));
        }

        return view('admin.vehicle_types.show')->with('vehicleType', $vehicleType);
    }

    /**
     * Show the form for editing the specified VehicleType.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        abort_if(\Gate::denies('vehicleTypes.edit'), 403, '403 Forbidden');
        $vehicleType = $this->vehicleTypeRepository->find($id);

        if (empty($vehicleType)) {
            Flash::error(__('lang.messages.not_found', ['model' => __('models/vehicleTypes.singular')]));

            return redirect(route('admin.vehicleTypes.index'));
        }

        return view('admin.vehicle_types.edit')->with('vehicleType', $vehicleType);
    }

    /**
     * Update the specified VehicleType in storage.
     *
     * @param  int              $id
     * @param UpdateVehicleTypeRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateVehicleTypeRequest $request)
    {
        abort_if(\Gate::denies('vehicleTypes.edit'), 403, '403 Forbidden');
        $vehicleType = $this->vehicleTypeRepository->find($id);
        $input = $request->all();
        if (empty($vehicleType)) {
            Flash::error(__('lang.messages.not_found', ['model' => __('models/vehicleTypes.singular')]));
            return redirect(route('admin.vehicleTypes.index'));
        }
        if($request->hasFile('icon')){
            try{
                unlink(public_path('uploads/vehicle_types/'.$vehicleType->icon));
            }catch (\Exception $e){
                \Log::error($e->getMessage());
            }
            $input['icon'] = \Helpers::uploadImage($request->file('icon'), 'vehicle_types');
        }
        if ($request->hasFile('marker_icon')) {
            try{
                unlink(public_path('uploads/vehicle_types/'.$vehicleType->marker_icon));
            }catch (\Exception $e){
                \Log::error($e->getMessage());
            }
            $input['marker_icon'] = \Helpers::uploadImage($request->file('marker_icon'), 'vehicle_types');
        }
        $vehicleType = $this->vehicleTypeRepository->update($input, $id);
        Flash::success(__('lang.messages.updated', ['model' => __('models/vehicleTypes.singular')]));

        return redirect(route('admin.vehicleTypes.index'));
    }

    /**
     * Remove the specified VehicleType from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        abort_if(\Gate::denies('vehicleTypes.delete'), 403, '403 Forbidden');
        $vehicleType = $this->vehicleTypeRepository->find($id);

        if (empty($vehicleType)) {
            Flash::error(__('lang.messages.not_found', ['model' => __('models/vehicleTypes.singular')]));

            return redirect(route('admin.vehicleTypes.index'));
        }

        $this->vehicleTypeRepository->delete($id);

        Flash::success(__('lang.messages.deleted', ['model' => __('models/vehicleTypes.singular')]));

        return redirect(route('admin.vehicleTypes.index'));
    }

    /**
     * Remove the list of vehicleTypes from storage.
     *
     * @param  array $ids
     *
     * @return Response
     */
    public function destroy_multiple(Request $request)
    {
        abort_if(\Gate::denies('vehicleTypes.delete'), 403, '403 Forbidden');
        if($request->delete){
            $vehicleTypes = $this->vehicleTypeRepository->findWhereIn('id',$request->delete);
            if (empty($vehicleTypes)) {
                Flash::error(__('lang.messages.not_found', ['model' => __('models/vehicleTypes.singular')]));
                return redirect(route('admin.vehicleTypes.index'));
            }
            foreach($vehicleTypes as $vehicleType){
                $this->vehicleTypeRepository->delete($vehicleType->id);
            }
            Flash::success(__('lang.messages.deleted', ['model' => __('models/vehicleTypes.singular')]));
            return redirect(route('admin.vehicleTypes.index'));
        }else{
            Flash::error(__('lang.messages.no_data_selected', ['model' => __('models/vehicleTypes.singular')]));
            return redirect(route('admin.vehicleTypes.index'));
        }
        
    }
}
