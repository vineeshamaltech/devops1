<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\DeliveryFeeDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateDeliveryFeeRequest;
use App\Http\Requests\UpdateDeliveryFeeRequest;
use App\Repositories\DeliveryFeeRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Response;

class DeliveryFeeController extends AppBaseController
{
    /** @var  DeliveryFeeRepository */
    private $deliveryFeeRepository;

    public function __construct(DeliveryFeeRepository $deliveryFeeRepo)
    {
        $this->deliveryFeeRepository = $deliveryFeeRepo;
    }

    /**
     * Display a listing of the DeliveryFee.
     *
     * @param DeliveryFeeDataTable $deliveryFeeDataTable
     * @return Response
     */
    public function index(DeliveryFeeDataTable $deliveryFeeDataTable)
    {
        abort_if(\Gate::denies('deliveryFees.index'), 403, '403 Forbidden');
        return $deliveryFeeDataTable->render('admin.delivery_fees.index');
    }

    /**
     * Show the form for creating a new DeliveryFee.
     *
     * @return Response
     */
    public function create()
    {
        abort_if(\Gate::denies('deliveryFees.create'), 403, '403 Forbidden');
        return view('admin.delivery_fees.create');
    }

    /**
     * Store a newly created DeliveryFee in storage.
     *
     * @param CreateDeliveryFeeRequest $request
     *
     * @return Response
     */
    public function store(CreateDeliveryFeeRequest $request)
    {
        abort_if(\Gate::denies('deliveryFees.create'), 403, '403 Forbidden');
        $input = $request->all();

        $deliveryFee = $this->deliveryFeeRepository->create($input);

        Flash::success(__('lang.messages.saved', ['model' => __('models/deliveryFees.singular')]));

        return redirect(route('admin.deliveryFees.index'));
    }

    /**
     * Display the specified DeliveryFee.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        abort_if(\Gate::denies('deliveryFees.view'), 403, '403 Forbidden');
        $deliveryFee = $this->deliveryFeeRepository->find($id);

        if (empty($deliveryFee)) {
            Flash::error(__('lang.messages.not_found', ['model' => __('models/deliveryFees.singular')]));

            return redirect(route('admin.deliveryFees.index'));
        }

        return view('admin.delivery_fees.show')->with('deliveryFee', $deliveryFee);
    }

    /**
     * Show the form for editing the specified DeliveryFee.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        abort_if(\Gate::denies('deliveryFees.edit'), 403, '403 Forbidden');
        $deliveryFee = $this->deliveryFeeRepository->find($id);

        if (empty($deliveryFee)) {
            Flash::error(__('lang.messages.not_found', ['model' => __('models/deliveryFees.singular')]));

            return redirect(route('admin.deliveryFees.index'));
        }

        return view('admin.delivery_fees.edit')->with('deliveryFee', $deliveryFee);
    }

    /**
     * Update the specified DeliveryFee in storage.
     *
     * @param  int              $id
     * @param UpdateDeliveryFeeRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateDeliveryFeeRequest $request)
    {
        abort_if(\Gate::denies('deliveryFees.edit'), 403, '403 Forbidden');
        $deliveryFee = $this->deliveryFeeRepository->find($id);

        if (empty($deliveryFee)) {
            Flash::error(__('lang.messages.not_found', ['model' => __('models/deliveryFees.singular')]));

            return redirect(route('admin.deliveryFees.index'));
        }

        $deliveryFee = $this->deliveryFeeRepository->update($request->all(), $id);

        Flash::success(__('lang.messages.updated', ['model' => __('models/deliveryFees.singular')]));

        return redirect(route('admin.deliveryFees.index'));
    }

    /**
     * Remove the specified DeliveryFee from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        abort_if(\Gate::denies('deliveryFees.delete'), 403, '403 Forbidden');
        $deliveryFee = $this->deliveryFeeRepository->find($id);

        if (empty($deliveryFee)) {
            Flash::error(__('lang.messages.not_found', ['model' => __('models/deliveryFees.singular')]));

            return redirect(route('admin.deliveryFees.index'));
        }

        $this->deliveryFeeRepository->delete($id);

        Flash::success(__('lang.messages.deleted', ['model' => __('models/deliveryFees.singular')]));

        return redirect(route('admin.deliveryFees.index'));
    }

    /**
     * Remove the list of Categories from storage.
     *
     * @param  array $ids
     *
     * @return Response
     */
    public function destroy_multiple(Request $request)
    {
        abort_if(\Gate::denies('deliveryFees.delete'), 403, '403 Forbidden');
        if($request->delete){
            $deliveryFees = $this->deliveryFeeRepository->findWhereIn('id',$request->delete);
            if (empty($deliveryFees)) {
                Flash::error(__('lang.messages.not_found', ['model' => __('models/deliveryFees.singular')]));
                return redirect(route('admin.deliveryFees.index'));
            }
            foreach($deliveryFees as $deliveryFee){
                $this->deliveryFeeRepository->delete($deliveryFee->id);
            }
            Flash::success(__('lang.messages.deleted', ['model' => __('models/deliveryFees.singular')]));
            return redirect(route('admin.deliveryFees.index'));
        }else{
            Flash::error(__('lang.messages.no_data_selected', ['model' => __('models/deliveryFees.singular')]));
            return redirect(route('admin.deliveryFees.index'));
        }
        
    }
    
}
