<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Driver;
use App\Models\Store;
use App\Models\Zone;
class TrackingMapsController extends Controller
{



    public function getTrackingMaps(Request $request){

      $query = Driver::query();
      \Log::info('This is in tracking maps driver');
 
        if($request->has('zones') && $request->zones){
          $query=$query->where('zones',$request->zones); 
        }
        $data= $query->get();
        $zones= Zone::all();
        
        $locations = []; 
        foreach($data as $user){
          $locations[] = ['latitude'=>$user->latitude,'longitude'=>$user->longitude,'name'=>$user->name];
        }
       return view('admin.trackingmaps.index',['data'=>$data,'locations'=>$locations,'zones'=>$zones]);
     }

     public function getTrackingMerchantes(Request $request){
      \Log::info('This is in tracking maps merchants');
      $query = Store::query();
      if($request->has('zone') && $request->zone){
        $query=$query->where('zone',$request->zone); 
      }
       
      $stores= $query->get();
        $zones= Zone::all();

        $locations = [];
        foreach($stores as $user){
          $locations[] = ['latitude'=>$user->latitude,'longitude'=>$user->longitude,'name'=>$user->name];
        }

       return view('admin.trackingmaps.merchants',['data'=>$stores,'locations'=>$locations,'zones'=>$zones]);


     }
}
