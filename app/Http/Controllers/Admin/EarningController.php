<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\EarningDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateEarningRequest;
use App\Http\Requests\UpdateEarningRequest;
use App\Repositories\EarningRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use App\Models\Earning;
use Illuminate\Http\Request;
use Response;

class EarningController extends AppBaseController
{
    /** @var  EarningRepository */
    private $earningRepository;

    public function __construct(EarningRepository $earningRepo)
    {
        $this->earningRepository = $earningRepo;
    }

    /**
     * Display a listing of the Earning.
     *
     * @param EarningDataTable $earningDataTable
     * @return Response
     */
    public function index(EarningDataTable $earningDataTable)
    {
        abort_if(\Gate::denies('earnings.index'), 403, '403 Forbidden');
        $startDate=date('y-m-d 00:00:01');
        $endDate=date('y-m-d 23:59:59');
        $data['total_admin_commision'] = Earning::where('remarks', 'like', '%Admin Commission%')->sum('amount');
        $data['today_admin_commision'] = Earning::whereBetween('created_at', [$startDate, $endDate])->where('remarks', 'like', '%Admin Commission%')->sum('amount');
        $data['total_store_earning'] = Earning::where('remarks', 'like', '%Store earning%')->sum('amount');
        $data['today_store_earning'] = Earning::whereBetween('created_at', [$startDate, $endDate])->where('remarks', 'like', '%Store earning%')->sum('amount');
        $data['total_driver_earning'] = Earning::where('remarks', 'like', '%Driver earning%')->sum('amount');
        $data['today_driver_earning'] = Earning::whereBetween('created_at', [$startDate, $endDate])->where('remarks', 'like', '%Driver earning%')->sum('amount');
        $data['total_incentive'] = Earning::where('remarks', 'like', '%Incentive%')->sum('amount');
        $data['today_incentive'] = Earning::whereBetween('created_at', [$startDate, $endDate])->where('remarks', 'like', '%Incentive%')->sum('amount');
                
        return $earningDataTable->render('admin.earnings.index',compact('data'));
    }
  
    /**
     * Show the form for creating a new Earning.
     *
     * @return Response
     */
    public function create()
    {
        abort_if(\Gate::denies('earnings.create'), 403, '403 Forbidden');
        return view('admin.earnings.create');
    }

    /**
     * Store a newly created Earning in storage.
     *
     * @param CreateEarningRequest $request
     *
     * @return Response
     */
    public function store(CreateEarningRequest $request)
    {
        abort_if(\Gate::denies('earnings.create'), 403, '403 Forbidden');
        $input = $request->all();

        $earning = $this->earningRepository->create($input);

        Flash::success(__('lang.messages.saved', ['model' => __('models/earnings.singular')]));

        return redirect(route('admin.earnings.index'));
    }

    /**
     * Display the specified Earning.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        abort_if(\Gate::denies('earnings.view'), 403, '403 Forbidden');
        $earning = $this->earningRepository->find($id);

        if (empty($earning)) {
            Flash::error(__('lang.messages.not_found', ['model' => __('models/earnings.singular')]));

            return redirect(route('admin.earnings.index'));
        }

        return view('admin.earnings.show')->with('earning', $earning);
    }

    /**
     * Show the form for editing the specified Earning.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        abort_if(\Gate::denies('earnings.edit'), 403, '403 Forbidden');
        $earning = $this->earningRepository->find($id);

        if (empty($earning)) {
            Flash::error(__('lang.messages.not_found', ['model' => __('models/earnings.singular')]));

            return redirect(route('admin.earnings.index'));
        }

        return view('admin.earnings.edit')->with('earning', $earning);
    }

    /**
     * Update the specified Earning in storage.
     *
     * @param  int              $id
     * @param UpdateEarningRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateEarningRequest $request)
    {
        abort_if(\Gate::denies('earnings.edit'), 403, '403 Forbidden');
        $earning = $this->earningRepository->find($id);

        if (empty($earning)) {
            Flash::error(__('lang.messages.not_found', ['model' => __('models/earnings.singular')]));

            return redirect(route('admin.earnings.index'));
        }

        $earning = $this->earningRepository->update($request->all(), $id);

        Flash::success(__('lang.messages.updated', ['model' => __('models/earnings.singular')]));

        return redirect(route('admin.earnings.index'));
    }

    /**
     * Remove the specified Earning from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        abort_if(\Gate::denies('earnings.delete'), 403, '403 Forbidden');
        $earning = $this->earningRepository->find($id);

        if (empty($earning)) {
            Flash::error(__('lang.messages.not_found', ['model' => __('models/earnings.singular')]));

            return redirect(route('admin.earnings.index'));
        }

        $this->earningRepository->delete($id);

        Flash::success(__('lang.messages.deleted', ['model' => __('models/earnings.singular')]));

        return redirect(route('admin.earnings.index'));
    }

    /**
     * Remove the list of earnings from storage.
     *
     * @param  array $ids
     *
     * @return Response
     */
    public function destroy_multiple(Request $request)
    {
        abort_if(\Gate::denies('earnings.delete'), 403, '403 Forbidden');
        if($request->delete){
            $earnings = $this->earningRepository->findWhereIn('id',$request->delete);
            if (empty($earnings)) {
                Flash::error(__('lang.messages.not_found', ['model' => __('models/earnings.singular')]));
                return redirect(route('admin.earnings.index'));
            }
            foreach($earnings as $earning){
                $this->earningRepository->delete($earning->id);
            }
            Flash::success(__('lang.messages.deleted', ['model' => __('models/earnings.singular')]));
            return redirect(route('admin.earnings.index'));
        }else{
            Flash::error(__('lang.messages.no_data_selected', ['model' => __('models/earnings.singular')]));
            return redirect(route('admin.earnings.index'));
        }
        
    }
}
