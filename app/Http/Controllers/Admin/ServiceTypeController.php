<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\ServiceTypeDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateServiceTypeRequest;
use App\Http\Requests\UpdateServiceTypeRequest;
use App\Repositories\ServiceTypeRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class ServiceTypeController extends AppBaseController
{
    /** @var  ServiceTypeRepository */
    private $serviceTypeRepository;

    public function __construct(ServiceTypeRepository $serviceTypeRepo)
    {
        $this->serviceTypeRepository = $serviceTypeRepo;
    }

    /**
     * Display a listing of the ServiceType.
     *
     * @param ServiceTypeDataTable $serviceTypeDataTable
     * @return Response
     */
    public function index(ServiceTypeDataTable $serviceTypeDataTable)
    {
        \Log::info('This is in servie type controll  request');
        abort_if(\Gate::denies('service_types.index'), 403, '403 Forbidden');
        return $serviceTypeDataTable->render('admin.service_types.index');
    }

    /**
     * Show the form for creating a new ServiceType.
     *
     * @return Response
     */
    public function create()
    {
        abort_if(\Gate::denies('service_types.create'), 403, '403 Forbidden');
        return view('admin.service_types.create');
    }

    /**
     * Store a newly created ServiceType in storage.
     *
     * @param CreateServiceTypeRequest $request
     *
     * @return Response
     */
    public function store(CreateServiceTypeRequest $request)
    {
        abort_if(\Gate::denies('service_types.create'), 403, '403 Forbidden');
        $input = $request->all();

        $serviceType = $this->serviceTypeRepository->create($input);

        Flash::success(__('lang.messages.saved', ['model' => __('models/serviceTypes.singular')]));

        return redirect(route('admin.serviceTypes.index'));
    }

    /**
     * Display the specified ServiceType.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        abort_if(\Gate::denies('service_types.view'), 403, '403 Forbidden');
        $serviceType = $this->serviceTypeRepository->find($id);

        if (empty($serviceType)) {
            Flash::error(__('lang.messages.not_found', ['model' => __('models/serviceTypes.singular')]));

            return redirect(route('admin.serviceTypes.index'));
        }

        return view('admin.service_types.show')->with('serviceType', $serviceType);
    }

    /**
     * Show the form for editing the specified ServiceType.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        abort_if(\Gate::denies('service_types.edit'), 403, '403 Forbidden');
        $serviceType = $this->serviceTypeRepository->find($id);

        if (empty($serviceType)) {
            Flash::error(__('lang.messages.not_found', ['model' => __('models/serviceTypes.singular')]));

            return redirect(route('admin.serviceTypes.index'));
        }

        return view('admin.service_types.edit')->with('serviceType', $serviceType);
    }

    /**
     * Update the specified ServiceType in storage.
     *
     * @param  int              $id
     * @param UpdateServiceTypeRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateServiceTypeRequest $request)
    {
        abort_if(\Gate::denies('service_types.edit'), 403, '403 Forbidden');
        $serviceType = $this->serviceTypeRepository->find($id);

        if (empty($serviceType)) {
            Flash::error(__('lang.messages.not_found', ['model' => __('models/serviceTypes.singular')]));

            return redirect(route('admin.serviceTypes.index'));
        }

        $serviceType = $this->serviceTypeRepository->update($request->all(), $id);

        Flash::success(__('lang.messages.updated', ['model' => __('models/serviceTypes.singular')]));

        return redirect(route('admin.serviceTypes.index'));
    }

    /**
     * Remove the specified ServiceType from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        abort_if(\Gate::denies('service_types.delete'), 403, '403 Forbidden');
        $serviceType = $this->serviceTypeRepository->find($id);

        if (empty($serviceType)) {
            Flash::error(__('lang.messages.not_found', ['model' => __('models/serviceTypes.singular')]));

            return redirect(route('admin.serviceTypes.index'));
        }

        $this->serviceTypeRepository->delete($id);

        Flash::success(__('lang.messages.deleted', ['model' => __('models/serviceTypes.singular')]));

        return redirect(route('admin.serviceTypes.index'));
    }
}
