<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\HomeServiceOrderDataTable;
use App\Events\OrderChanged;
use App\Http\Controllers\AppBaseController;
use App\Http\Requests\CreateOrderRequest;
use App\Http\Requests\UpdateOrderRequest;
use App\Models\Driver;
use App\Models\DriverHomeServices;
use App\Models\HomeService;
use App\Models\OrderStatusTimestamp;
use App\Models\ServiceOrder;
use App\Models\Store;
use App\Notifications\PushNotification;
use App\Repositories\OrderRepository;
use App\Services\HomeOrderService;
use App\Services\OrderService;
use Flash;
use Illuminate\Http\Request;
use Notification;
use Response;

class HomeOrderController extends AppBaseController
{
    /** @var  OrderRepository */
    private $orderRepository;

    public function __construct(OrderRepository $orderRepo)
    {
        $this->orderRepository = $orderRepo;
    }

    /**
     * Display a listing of the Order.
     *
     * @param OrderDataTable $orderDataTable
     * @return Response
     */
    public function index(HomeServiceOrderDataTable $orderDataTable)
    {

        \Log::info('This is in Home order controller');
        abort_if(\Gate::denies('homeServiceOrders.index'), 403, '403 Forbidden');
          $drivers = Driver::where('active', 1)->where('is_busy',0)->where('on_duty',1)->where('home_service_enabled',1)->get();
        return $orderDataTable->render('admin.service_orders.index',compact('drivers'));
    }

    /**
     * Show the form for creating a new Order.
     *
     * @return Response
     */
    public function create(){
        abort_if(\Gate::denies('homeServiceOrders.create'), 403, '403 Forbidden');
        return view('admin.homeOrders.create');
    }

    /**
     * Store a newly created Order in storage.
     *
     * @param CreateOrderRequest $request
     *
     * @return Response
     */
    public function store(CreateOrderRequest $request)
    {
        abort_if(\Gate::denies('homeServiceOrders.create'), 403, '403 Forbidden');
        $input = $request->all();
        $order = $this->orderRepository->create($input);
        if (!empty($input['driver_id'])) {
            $driver = \App\Models\Driver::find($input['driver_id']);
            $driver->notify(new \App\Notifications\OrderAssigned("Order #" . $order->id . " has been assigned to you.", $order));
        }
        Flash::success(__('lang.messages.saved', ['model' => __('models/orders.singular')]));
        return redirect(route('admin.homeOrders.index'));
    }

    /**
     * Display the specified Order.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        abort_if(\Gate::denies('homeServiceOrders.view'), 403, '403 Forbidden');
        $order = ServiceOrder::find($id);
        if (empty($order)) {
            Flash::error(__('lang.messages.not_found', ['model' => __('models/orders.singular')]));
            return redirect(route('admin.homeOrders.index'));
        }

        return view('admin.service_orders.show')->with('order', $order);
    }

    /**
     * Show the form for editing the specified Order.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        abort_if(\Gate::denies('homeServiceOrders.edit'), 403, '403 Forbidden');
        $order = ServiceOrder::find($id);
 
        if (empty($order)) {
            Flash::error(__('lang.messages.not_found', ['model' => __('models/orders.singular')]));
            return redirect(route('admin.homeOrders.index'));
        }
        $drivers = \Helpers::getDriversByHomeServiceCategoryId($order->service_category_id,$order->zone_id);
      
        foreach ($drivers as $driver) {
             $append = "";
             $driver->name = $driver->name . ' (' . $driver->mobile. ')'.$append;
        }
        return view('admin.service_orders.edit',compact('drivers'))->with('order', $order);
    }

    /**
     * Update the specified Order in storage.
     *
     * @param  int              $id
     * @param UpdateOrderRequest $request
     *
     * @return Response
     */
    public function update($id, Request $request, HomeOrderService $orderService)
    {
        abort_if(\Gate::denies('homeServiceOrders.edit'), 403, '403 Forbidden');
        $order = ServiceOrder::find($id);
        $input = $request->all();
        if (empty($order)) {
            Flash::error(__('lang.messages.not_found', ['model' => __('models/orders.singular')]));
            return redirect(route('admin.homeOrders.index'));
        }
        if (!empty($input['driver_id']) && $order->driver_id != $input['driver_id']) {
            $orderService->assignDriverToHomeServiceOrder($order, $input['driver_id']);
        }
        if (empty($input['driver_id'])){
            $input['order_status'] = 'RECEIVED';
        }else{
            $input['order_status'] = 'ASSIGNED';
        }
        if (!empty($input['order_status']) && $order->order_status != $input['order_status']) {
             $orderService->changeOrderStatus($order, $input['order_status']);
        }

        $order = ServiceOrder::where('id', $id)->update(['payment_status' => $input['payment_status'], 'payment_method' => $input['payment_method']]);
        // event(new OrderChanged($order));

        Flash::success(__('lang.messages.updated', ['model' => __('models/orders.singular')]));
        return redirect(route('admin.homeOrders.index'));
    }

    public function changeStatus(Request $req, OrderService $orderService)
    {
        Order::where('id', $req->orderid)->update([
            'order_status' => $req->status,
        ]);
        OrderStatusTimestamp::create([
            'order_id' => $req->orderid,
            'status' => $req->status,
            'comment' => 'Status Changed by Admin',
        ]);
        $order = Order::find($req->orderid);
        $orderService->changeOrderStatus($order, $req->status);
        //Add order event here
        //event(new OrderChanged($order));
        Notification::send($order->user, new PushNotification(['title' => 'Order Status Changed', 'description' => 'Your order status has been changed to ' . $req->status]));
        Flash::success('Driver assigned successfully.');
        return redirect(route('admin.homeOrders.index'));
    }

    public function assignServiceDriver(Request $req, OrderService $orderService)
    {
        $req->validate([
            'orderid' => 'required',
            'driver_id' => 'required',
        ]);
        $order = ServiceOrder::find($req->orderid);
        $orderService->assignDriverToOrder($order, $req->driver_id);
        //Add order event here
        Flash::success('Driver assigned successfully.');
        return redirect(route('admin.homeOrders.index'));
    }

    /**
     * Remove the specified Order from storage.
     *
     * @param  int $id
     *
     * @return Response
     */

    public function destroy($id)
    {
        abort_if(\Gate::denies('homeServiceOrders.delete'), 403, '403 Forbidden');
       
        $order = $this->orderRepository->find((int)$id);
        //  dd($order);
        if (empty($order)) {
            Flash::error(__('lang.messages.not_found', ['model' => __('models/orders.singular')]));
            return redirect(route('admin.homeOrders.index'));
        }
        ServiceOrder::destroy($id);
        // $this->orderRepository->delete($id);
        Flash::success(__('lang.messages.deleted', ['model' => __('models/orders.singular')]));

        return redirect(route('admin.homeOrders.index'));
    }
}
