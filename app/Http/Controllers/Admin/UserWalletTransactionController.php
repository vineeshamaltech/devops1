<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\UserWalletTransactionDataTable;
use App\Helpers\WalletHelper;
use App\Http\Requests;
use App\Http\Requests\CreateUserWalletTransactionRequest;
use App\Http\Requests\UpdateUserWalletTransactionRequest;
use App\Repositories\UserWalletTransactionRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Response;

class UserWalletTransactionController extends AppBaseController
{
    /** @var  UserWalletTransactionRepository */
    private $userWalletTransactionRepository;

    public function __construct(UserWalletTransactionRepository $userWalletTransactionRepo)
    {
        $this->userWalletTransactionRepository = $userWalletTransactionRepo;
    }

    /**
     * Display a listing of the UserWalletTransaction.
     *
     * @param UserWalletTransactionDataTable $userWalletTransactionDataTable
     * @return Response
     */
    public function index(UserWalletTransactionDataTable $userWalletTransactionDataTable)
    {
        \Log::info('This is in user wallet controll  request');
        abort_if(\Gate::denies('userWalletTransactions.index'), 403, '403 Forbidden');
        return $userWalletTransactionDataTable->render('admin.user_wallet_transactions.index');
    }

    /**
     * Show the form for creating a new UserWalletTransaction.
     *
     * @return Response
     */
    public function create()
    {
        abort_if(\Gate::denies('userWalletTransactions.create'), 403, '403 Forbidden');
        return view('admin.user_wallet_transactions.create');
    }

    /**
     * Store a newly created UserWalletTransaction in storage.
     *
     * @param CreateUserWalletTransactionRequest $request
     *
     * @return Response
     */
    public function store(CreateUserWalletTransactionRequest $request)
    {
        abort_if(\Gate::denies('userWalletTransactions.create'), 403, '403 Forbidden');

        $input = $request->all();

        if ($input['type'] == 'CREDIT') {
            WalletHelper::creditUserWallet($input['user_id'], $input['amount'], $input['remarks']);
        }else{
            WalletHelper::debitUserWallet($input['user_id'], $input['amount'], $input['remarks']);
        }
//dd($input['user_id']);
        Flash::success(__('lang.messages.saved', ['model' => __('models/userWalletTransactions.singular')]));
        return redirect(route('admin.userWalletTransactions.index'));
    }

    /**
     * Display the specified UserWalletTransaction.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        abort_if(\Gate::denies('userWalletTransactions.view'), 403, '403 Forbidden');
        $userWalletTransaction = $this->userWalletTransactionRepository->find($id);

        if (empty($userWalletTransaction)) {
            Flash::error(__('lang.messages.not_found', ['model' => __('models/userWalletTransactions.singular')]));

            return redirect(route('admin.userWalletTransactions.index'));
        }

        return view('admin.user_wallet_transactions.show')->with('userWalletTransaction', $userWalletTransaction);
    }

    /**
     * Remove the list of user_wallet_transactions from storage.
     *
     * @param  array $ids
     *
     * @return Response
     */
    public function destroy_multiple(Request $request)
    {
        abort_if(\Gate::denies('user_wallet_transactions.delete'), 403, '403 Forbidden');
        if($request->delete){
            $user_wallet_transactions = $this->userWalletTransactionRepository->findWhereIn('id',$request->delete);
            if (empty($user_wallet_transactions)) {
                Flash::error(__('lang.messages.not_found', ['model' => __('models/user_wallet_transactions.singular')]));
                return redirect(route('admin.userWalletTransactions.index'));
            }
            foreach($user_wallet_transactions as $user_wallet_transaction){
                $this->userWalletTransactionRepository->delete($user_wallet_transaction->id);
            }
            Flash::success(__('lang.messages.deleted', ['model' => __('models/user_wallet_transactions.singular')]));
            return redirect(route('admin.userWalletTransactions.index'));
        }else{
            Flash::error(__('lang.messages.no_data_selected', ['model' => __('models/user_wallet_transactions.singular')]));
            return redirect(route('admin.userWalletTransactions.index'));
        }
        
    }
}
