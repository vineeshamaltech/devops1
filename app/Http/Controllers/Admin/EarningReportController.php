<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Mail\ExportGenerated;
use App\Mail\ImportExportFailed;
use App\Models\Earning;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Schema;
use Laracasts\Flash\Flash;

class EarningReportController extends Controller
{
    public function index(){
        return view('admin.earning_report.index');
    }

    public function generate(Request $request){
        
        if (empty($request->report_of)) {
            Flash::error('Please select Type.');
            return redirect(route('admin.earnings.report.index'));
        }

        if (empty($request->reportable_id)) {
            Flash::error('Please select Admin/Vendor.');
            return redirect(route('admin.earnings.report.index'));
        }

        if($request->exportType == 'mail'){
            if (empty($request->email)) {
                Flash::error('Please input email.');
                return redirect(route('admin.earnings.report.index'));
            }

            

            try {

                $type = $request->report_of;
                $id = $request->reportable_id;
             
                $daterange = explode('-', $request->daterange);
                $startDate = Carbon::parse($daterange[0]);
                $endDate = Carbon::parse($daterange[1]);
             
            
    
                if ($type == 'vendor'){
                    $type = 'App\\Models\\Store';
                }else if ($type == 'driver'){
                    $type = 'App\\Models\\Driver';
                }
                if ($type == 'App\\Models\\Store'){
                    $data = Earning::select(['entity_id','order_id','amount','remarks','created_at'])->whereIn('entity_id', \Helpers::get_stores_by_user($id)->pluck('id'))->where('entity_type', $type)->whereBetween('created_at', [$startDate, $endDate])->get()->toArray();
                   $service_data =Earning::select(['entity_id','order_id','amount','remarks','created_at'])->where('entity_type', $type)->where('type', "SERVICE ORDER")->where('entity_id', $id)->whereBetween('created_at', [$startDate, $endDate])->get()->toArray();
          
                }else {
                 
                    $data = Earning::select(['entity_id','order_id','amount','remarks','created_at'])->select('entity_id', 'order_id', 'amount', 'remarks', 'created_at')->where('entity_type', $type)->where('type','!=', "SERVICE ORDER")->where('entity_id', $id)->whereBetween('created_at', [$startDate, $endDate])->get()->toArray();
                    $service_data =Earning::select(['entity_id','order_id','amount','remarks','created_at'])->where('entity_type', $type)->where('type', "SERVICE ORDER")->where('entity_id', $id)->whereBetween('created_at', [$startDate, $endDate])->get()->toArray();
                 
                }
                $service_fileName = "service_report-".rand(1,9999).'.csv';
                $destinationPath2 = public_path('storage/exports/'.$service_fileName);
                $file2 = fopen($destinationPath2, 'w');
                fputcsv($file2, ['entity_id','order_id','amount','remarks','created_at']);
                foreach ($service_data as $row) {
                    fputcsv($file2, (array) $row);
                }
                
                fclose($file2);
    
              
                $fileName = "report-".rand(1,9999).'.csv';
                $destinationPath = public_path('storage/exports/'.$fileName);
                 
                $file = fopen($destinationPath, 'w');
               
                fputcsv($file, ['entity_id','order_id','amount','remarks','created_at']);
                foreach ($data as $row) {
                    fputcsv($file, (array) $row);
                }
                fclose($file);

              
            $rs =  Mail::to($request->email)
                    ->queue(new ExportGenerated($destinationPath,$destinationPath2))
                    ;
              
                Flash::success('Report generated and sent successfully');
                return redirect()->back();
    
            }catch (\Exception $e) {
                Flash::error('Error: '.$e->getMessage());
                return redirect()->back();
            }
        }else if($request->exportType == 'service'){
            try {

                $type = $request->report_of;
                $id = $request->reportable_id;
             
                $daterange = explode('-', $request->daterange);
                $startDate = Carbon::parse($daterange[0]);
                $endDate = Carbon::parse($daterange[1]);
            
    
                if ($type == 'vendor'){
                    $type = 'App\\Models\\Store';
                }else if ($type == 'driver'){
                    $type = 'App\\Models\\Driver';
                }
                if ($type == 'App\\Models\\Store'){
                    $data = Earning::select(['entity_id','order_id','amount','remarks','created_at'])->whereIn('entity_id', \Helpers::get_stores_by_user($id)->pluck('id'))->where('entity_type', $type)->whereBetween('created_at', [$startDate, $endDate])->get()->toArray();
                   $service_data =Earning::select(['entity_id','order_id','amount','remarks','created_at'])->where('entity_type', $type)->where('type', "SERVICE ORDER")->where('entity_id', $id)->whereBetween('created_at', [$startDate, $endDate])->get()->toArray();
          
                }else {
                 
                    $data = Earning::select(['entity_id','order_id','amount','remarks','created_at'])->select('entity_id', 'order_id', 'amount', 'remarks', 'created_at')->where('entity_type', $type)->where('type','!=', "SERVICE ORDER")->where('entity_id', $id)->whereBetween('created_at', [$startDate, $endDate])->get()->toArray();
                    $service_data =Earning::select(['entity_id','order_id','amount','remarks','created_at'])->where('entity_type', $type)->where('type', "SERVICE ORDER")->where('entity_id', $id)->whereBetween('created_at', [$startDate, $endDate])->get()->toArray();
                 
                }
                $service_fileName = "service_report-".rand(1,9999).'.csv';
                $destinationPath2 = public_path('storage/exports/'.$service_fileName);
                $file2 = fopen($destinationPath2, 'w');
                fputcsv($file2, ['entity_id','order_id','amount','remarks','created_at']);
                foreach ($service_data as $row) {
                    fputcsv($file2, (array) $row);
                }
                
                fclose($file2);
    
              
                $fileName = "report-".rand(1,9999).'.csv';
                $destinationPath = public_path('storage/exports/'.$fileName);
                 
                $file = fopen($destinationPath, 'w');
               
                fputcsv($file, ['entity_id','order_id','amount','remarks','created_at']);
                foreach ($data as $row) {
                    fputcsv($file, (array) $row);
                }
                fclose($file);
         
                $headers = array(
                    'Content-Type' => 'text/csv',
                );
                return response()->download($destinationPath2, $service_fileName.date("d-m-Y H:i:s").'.csv', $headers);

            }catch (\Exception $e) {
                Flash::error('Error: '.$e->getMessage());
                return redirect()->back();
            }
        }else if($request->exportType == 'report'){
            try {

                $type = $request->report_of;
                $id = $request->reportable_id;
             
                $daterange = explode('-', $request->daterange);
                $startDate = Carbon::parse($daterange[0]);
                $endDate = Carbon::parse($daterange[1]);
            
    
                if ($type == 'vendor'){
                    $type = 'App\\Models\\Store';
                }else if ($type == 'driver'){
                    $type = 'App\\Models\\Driver';
                }
                if ($type == 'App\\Models\\Store'){
                    $data = Earning::select(['entity_id','order_id','amount','remarks','created_at'])->whereIn('entity_id', \Helpers::get_stores_by_user($id)->pluck('id'))->where('entity_type', $type)->whereBetween('created_at', [$startDate, $endDate])->get()->toArray();
                   $service_data =Earning::select(['entity_id','order_id','amount','remarks','created_at'])->where('entity_type', $type)->where('type', "SERVICE ORDER")->where('entity_id', $id)->whereBetween('created_at', [$startDate, $endDate])->get()->toArray();
          
                }else {
                 
                    $data = Earning::select(['entity_id','order_id','amount','remarks','created_at'])->select('entity_id', 'order_id', 'amount', 'remarks', 'created_at')->where('entity_type', $type)->where('type','!=', "SERVICE ORDER")->where('entity_id', $id)->whereBetween('created_at', [$startDate, $endDate])->get()->toArray();
                    $service_data =Earning::select(['entity_id','order_id','amount','remarks','created_at'])->where('entity_type', $type)->where('type', "SERVICE ORDER")->where('entity_id', $id)->whereBetween('created_at', [$startDate, $endDate])->get()->toArray();
                 
                }
                $service_fileName = "service_report-".rand(1,9999).'.csv';
                $destinationPath2 = public_path('storage/exports/'.$service_fileName);
                $file2 = fopen($destinationPath2, 'w');
                fputcsv($file2, ['entity_id','order_id','amount','remarks','created_at']);
                foreach ($service_data as $row) {
                    fputcsv($file2, (array) $row);
                }
                
                fclose($file2);
    
              
                $fileName = "report-".rand(1,9999).'.csv';
                $destinationPath = public_path('storage/exports/'.$fileName);
                 
                $file = fopen($destinationPath, 'w');
               
                fputcsv($file, ['entity_id','order_id','amount','remarks','created_at']);
                foreach ($data as $row) {
                    fputcsv($file, (array) $row);
                }
                fclose($file);
         
                $headers = array(
                    'Content-Type' => 'text/csv',
                );
                return response()->download($destinationPath, $fileName.date("d-m-Y H:i:s").'.csv', $headers);

            }catch (\Exception $e) {
                Flash::error('Error: '.$e->getMessage());
                return redirect()->back();
            }
        }
        
    }
}
