<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\VendorDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateVendorRequest;
use App\Http\Requests\UpdateVendorRequest;
use App\Repositories\VendorRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Illuminate\Support\Facades\Hash;
use Response;

class VendorController extends AppBaseController
{
    /** @var  VendorRepository */
    private $vendorRepository;

    public function __construct(VendorRepository $vendorRepo)
    {
        $this->vendorRepository = $vendorRepo;
    }

    /**
     * Display a listing of the Vendor.
     *
     * @param VendorDataTable $vendorDataTable
     * @return Response
     */
    public function index(VendorDataTable $vendorDataTable)
    {
        \Log::info('This is in vendor controll  request');
        abort_if(\Gate::denies('vendors.index'), 403, '403 Forbidden');
        return $vendorDataTable->render('admin.vendors.index');
    }

    /**
     * Show the form for creating a new Vendor.
     *
     * @return Response
     */
    public function create()
    {
        abort_if(\Gate::denies('vendors.create'), 403, '403 Forbidden');
        return view('admin.vendors.create');
    }

    /**
     * Store a newly created Vendor in storage.
     *
     * @param CreateVendorRequest $request
     *
     * @return Response
     */
    public function store(CreateVendorRequest $request)
    {
        abort_if(\Gate::denies('vendors.create'), 403, '403 Forbidden');
        $input = $request->all();
        $input['password'] = Hash::make($input['password']);
        $vendor = $this->vendorRepository->create($input);

        Flash::success(__('lang.messages.saved', ['model' => __('models/vendors.singular')]));

        return redirect(route('admin.vendors.index'));
    }

    /**
     * Display the specified Vendor.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        abort_if(\Gate::denies('vendors.view'), 403, '403 Forbidden');
        $vendor = $this->vendorRepository->find($id);

        if (empty($vendor)) {
            Flash::error(__('lang.messages.not_found', ['model' => __('models/vendors.singular')]));

            return redirect(route('admin.vendors.index'));
        }

        return view('admin.vendors.show')->with('vendor', $vendor);
    }

    /**
     * Show the form for editing the specified Vendor.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        abort_if(\Gate::denies('vendors.edit'), 403, '403 Forbidden');
        $vendor = $this->vendorRepository->find($id);

        if (empty($vendor)) {
            Flash::error(__('lang.messages.not_found', ['model' => __('models/vendors.singular')]));

            return redirect(route('admin.vendors.index'));
        }

        return view('admin.vendors.edit')->with('vendor', $vendor);
    }

    /**
     * Update the specified Vendor in storage.
     *
     * @param  int              $id
     * @param UpdateVendorRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateVendorRequest $request)
    {
        abort_if(\Gate::denies('vendors.edit'), 403, '403 Forbidden');
        $vendor = $this->vendorRepository->find($id);
        $input = $request->all();
        if (empty($vendor)) {
            Flash::error(__('lang.messages.not_found', ['model' => __('models/vendors.singular')]));

            return redirect(route('admin.vendors.index'));
        }
        if($input['password'] != null) {
            $input['password'] = Hash::make($input['password']);
        }
        $vendor = $this->vendorRepository->update($input, $id);

        Flash::success(__('lang.messages.updated', ['model' => __('models/vendors.singular')]));

        return redirect(route('admin.vendors.index'));
    }

    /**
     * Remove the specified Vendor from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        abort_if(\Gate::denies('vendors.delete'), 403, '403 Forbidden');
        $vendor = $this->vendorRepository->find($id);

        if (empty($vendor)) {
            Flash::error(__('lang.messages.not_found', ['model' => __('models/vendors.singular')]));

            return redirect(route('admin.vendors.index'));
        }

        $this->vendorRepository->delete($id);

        Flash::success(__('lang.messages.deleted', ['model' => __('models/vendors.singular')]));

        return redirect(route('admin.vendors.index'));
    }
}
