<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\ZoneDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateZoneRequest;
use App\Http\Requests\UpdateZoneRequest;
use App\Repositories\ZoneRepository;
use App\Models\ZoneHomeServices;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class ZoneController extends AppBaseController
{
    /** @var  ZoneRepository */
    private $zoneRepository;

    public function __construct(ZoneRepository $zoneRepo)
    {
        $this->zoneRepository = $zoneRepo;
    }

    /**
     * Display a listing of the Zone.
     *
     * @param ZoneDataTable $zoneDataTable
     * @return Response
     */
    public function index(ZoneDataTable $zoneDataTable)
    {
        \Log::info('This is in zone controll  request');
        abort_if(\Gate::denies('zones.index'), 403, '403 Forbidden');
        return $zoneDataTable->render('admin.zones.index');
       
    }

    /**
     * Show the form for creating a new Zone.
     *
     * @return Response
     */
    public function create()
    {
        abort_if(\Gate::denies('zones.create'), 403, '403 Forbidden');
        return view('admin.zones.create');
    }

    /**
     * Store a newly created Zone in storage.
     *
     * @param CreateZoneRequest $request
     *
     * @return Response
     */
    public function store(CreateZoneRequest $request)
    {
        abort_if(\Gate::denies('zones.create'), 403, '403 Forbidden');
        $input = $request->all();

        $input['polygon'] = json_decode($input['polygon']);
        $zone = $this->zoneRepository->create($input);

        Flash::success(__('lang.messages.saved', ['model' => __('models/zones.singular')]));

        return redirect(route('admin.zones.index'));
    }

    /**
     * Display the specified Zone.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        abort_if(\Gate::denies('zones.view'), 403, '403 Forbidden');
        $zone = $this->zoneRepository->find($id);

        if (empty($zone)) {
            Flash::error(__('lang.messages.not_found', ['model' => __('models/zones.singular')]));

            return redirect(route('admin.zones.index'));
        }
         
        return view('admin.zones.show')->with('zone', $zone);
    }

    /**
     * Show the form for editing the specified Zone.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        abort_if(\Gate::denies('zones.edit'), 403, '403 Forbidden');
        $zone = $this->zoneRepository->find($id);

        if (empty($zone)) {
            Flash::error(__('lang.messages.not_found', ['model' => __('models/zones.singular')]));

            return redirect(route('admin.zones.index'));
        }

        return view('admin.zones.edit')->with('zone', $zone);
    }

    /**
     * Update the specified Zone in storage.
     *
     * @param  int              $id
     * @param UpdateZoneRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateZoneRequest $request)
    {
        abort_if(\Gate::denies('zones.edit'), 403, '403 Forbidden');
        $zone = $this->zoneRepository->find($id);
   
        if (empty($zone)) {
            Flash::error(__('lang.messages.not_found', ['model' => __('models/zones.singular')]));

            return redirect(route('admin.zones.index'));
        }
        $input = $request->all();
       
        $input['polygon'] = json_decode($input['polygon']);
        $zone = $this->zoneRepository->update($input, $id);

        Flash::success(__('lang.messages.updated', ['model' => __('models/zones.singular')]));

        return redirect(route('admin.zones.index'));
    }

    /**
     * Remove the specified Zone from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        abort_if(\Gate::denies('zones.delete'), 403, '403 Forbidden');
        $zone = $this->zoneRepository->find($id);

        if (empty($zone)) {
            Flash::error(__('lang.messages.not_found', ['model' => __('models/zones.singular')]));

            return redirect(route('admin.zones.index'));
        }

         ZoneHomeServices::where('zone_id',$id)->delete();  

        $this->zoneRepository->delete($id);

        Flash::success(__('lang.messages.deleted', ['model' => __('models/zones.singular')]));

        return redirect(route('admin.zones.index'));
    }
}
