<?php

namespace App\Http\Controllers\Admin;
use App\Models\DriverHomeServices;
use App\Models\VehicleType;
use App\Models\Zone;
use Carbon\Carbon;
use App\DataTables\DriverDataTable;
use App\DataTables\DriverActiveDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateDriverRequest;
use App\Http\Requests\UpdateDriverRequest;
use App\Notifications\KycStatusChanged;
use App\Models\DriverKyc;
use App\Models\Driver;
use App\Models\DriverDutySession;
use App\Repositories\DriverRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use App\Models\Order;
use Notification;
use Response;

class DriverController extends AppBaseController
{
    /** @var  DriverRepository */
    private $driverRepository;

    public function __construct(DriverRepository $driverRepo)
    {
        $this->driverRepository = $driverRepo;
    }

    /**
     * Display a listing of the Driver.
     *
     * @param DriverDataTable $driverDataTable
     * @return Response
     */
    public function index(DriverDataTable $driverDataTable)
    {

        \Log::info('This is in Driver controlller');
        abort_if(\Gate::denies('drivers.index'), 403, '403 Forbidden');
        return $driverDataTable->render('admin.drivers.index');
    }

    /**
     * Show the form for creating a new Driver.
     *
     * @return Response
     */
    public function create()
    {
        abort_if(\Gate::denies('drivers.create'), 403, '403 Forbidden');
        $homeServiceCategories = \App\Models\HomeServiceCategory::getHierarchy();
        $zones = Zone::where('active',1)->get();
        foreach ($zones as $zone){
            $zone->selected = false;
        }
        return view('admin.drivers.create',compact('homeServiceCategories','zones'));
    }

    /**
     * Store a newly created Driver in storage.
     *
     * @param CreateDriverRequest $request
     *
     * @return Response
     */
    public function store(CreateDriverRequest $request)
    {
        abort_if(\Gate::denies('drivers.create'), 403, '403 Forbidden');
        $input = $request->all();

        if(!Driver::where('mobile',$input['mobile'])->exists()){
            $vehicleType = VehicleType::find($input['vehicle_type']);
            if (empty($vehicleType)) {
                Flash::error('Vehicle Type not found');
                return redirect(route('admin.drivers.create'));
            }
            $input['vehicle_type_id'] = $vehicleType->id;
            $input['vehicle_type'] = strtoupper($vehicleType->name);
            $input['zones'] = implode(',',$input['zones'] ?? []);
            $driver = $this->driverRepository->create($input);
             if(@$input['home_service_categories'] )
             {
               foreach ($input['home_service_categories'] as $value){
                $driverHomeService = new DriverHomeServices();
                $driverHomeService->driver_id = $driver->id;
                $driverHomeService->home_service_category_id = $value;
                $driverHomeService->save();
            }
             }
          

            Flash::success(__('lang.messages.saved', ['model' => __('models/drivers.singular')]));
            return redirect(route('admin.drivers.index'));
        }else{
            Flash::error(__('lang.messages.registerd', ['model' => __('models/drivers.singular')]));
            return redirect(route('admin.drivers.index'));
        }
    }

    /**
     * Display the specified Driver.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        abort_if(\Gate::denies('drivers.view'), 403, '403 Forbidden');

        $driver = $this->driverRepository->find($id);

       $driver->kycdocs =  DriverKyc::where('driver_id',$id)->get();

        if (empty($driver)) {
            Flash::error(__('lang.messages.not_found', ['model' => __('models/drivers.singular')]));
            return redirect(route('admin.drivers.index'));
        }
        return view('admin.drivers.show')->with('driver', $driver);
    }

    public function logintime($id,DriverActiveDataTable $driverActiveDataTable){
        abort_if(\Gate::denies('drivers.view'), 403, '403 Forbidden');
        $driver = Driver::find($id);
        if (empty($driver)) {
            Flash::error(__('lang.messages.not_found', ['model' => __('models/drivers.singular')]));
            return redirect(route('admin.drivers.index'));
        }
        $driverDuty =  DriverDutySession::selectRaw('SUM(TIMESTAMPDIFF(SECOND,start_time,end_time)) as time_diff')->where('driver_id',$id)->first()->time_diff;
        $today_time = DriverDutySession::selectRaw('SUM(TIMESTAMPDIFF(SECOND,start_time,end_time)) as time_diff')->where('driver_id',$id)->whereDate('created_at',Carbon::today())->first()->time_diff;

        $today_time = seconds_to_hms($today_time);
        $driverDuty = seconds_to_hms($driverDuty);

        return $driverActiveDataTable->with(['id'=>$id])->render('admin.driver_activetime.index',compact('driverDuty','today_time'));
    }


    /**
     * Show the form for editing the specified Driver.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        abort_if(\Gate::denies('drivers.edit'), 403, '403 Forbidden');
        $driver = $this->driverRepository->find($id);

        if (empty($driver)) {
            Flash::error(__('lang.messages.not_found', ['model' => __('models/drivers.singular')]));
            return redirect(route('admin.drivers.index'));
        }

       
        $driverZones = explode(',',$driver->zones);
        $zones = Zone::where('active',1)->get();
        foreach ($zones as $zone){
            if (in_array($zone->id,$driverZones)){
                $zone->selected = true;
            }else{
                $zone->selected = false;
            }
        }
        
         $homeServiceCategories = \App\Models\HomeServiceCategory::getHierarchy();
        foreach ($homeServiceCategories as $value) {
            if (DriverHomeServices::where('driver_id',$id)->where('home_service_category_id',$value->id)->exists()){
                $value->selected = true;
            }else{
                $value->selected = false;
            }
        }
        return view('admin.drivers.edit',compact('homeServiceCategories','zones'))->with('driver', $driver);
    }

    /**
     * Update the specified Driver in storage.
     *
     * @param  int              $id
     * @param UpdateDriverRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateDriverRequest $request)
    {
        abort_if(\Gate::denies('drivers.edit'), 403, '403 Forbidden');
        $driver = $this->driverRepository->find($id);
        if (empty($driver)) {
            Flash::error(__('lang.messages.not_found', ['model' => __('models/drivers.singular')]));
            return redirect(route('admin.drivers.index'));
        }

        $input = $request->all();
        if (!empty($input['is_verified'])){
            if ($input['is_verified'] != $driver->is_verified) {
                //Notification::send($driver,new KycStatusChanged())
            }
        }
        $vehicleType = VehicleType::find($input['vehicle_type']);
        if (empty($vehicleType)) {
            Flash::error('Vehicle Type not found');
            return redirect(route('admin.drivers.create'));
        }
        $input['zones'] = implode(',',$input['zones'] ?? []);
        $input['vehicle_type_id'] = $vehicleType->id;
        $input['vehicle_type'] = strtoupper($vehicleType->name);
        if (Driver::where('mobile',$input['mobile'])->where('id','!=',$id)->exists()){
            $this->sendError('Driver with this mobile number already exists',422);
        }
        $driver = $this->driverRepository->update($input, $id);
        DriverHomeServices::where('driver_id',$id)->delete();
        if (!empty($input['home_service_categories'])){
            foreach ($input['home_service_categories'] as $value){
                $driverHomeService = new DriverHomeServices();
                $driverHomeService->driver_id = $id;
                $driverHomeService->home_service_category_id = $value;
                $driverHomeService->save();
            }
        }

        Flash::success(__('lang.messages.updated', ['model' => __('models/drivers.singular')]));

        return redirect(route('admin.drivers.index'));
    }

    /**
     * Remove the specified Driver from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        abort_if(\Gate::denies('drivers.delete'), 403, '403 Forbidden');
        $check_for_used = Order::where('driver_id',$id)->get();
        if(count($check_for_used)>0){
            Flash::error("This Driver can't be deleted as it has been used on some orders.");
            return redirect(route('admin.drivers.index'));
        }
        
        $driver = $this->driverRepository->find($id);

        if (empty($driver)) {
            Flash::error(__('lang.messages.not_found', ['model' => __('models/drivers.singular')]));

            return redirect(route('admin.drivers.index'));
        }

        $this->driverRepository->delete($id);

        Flash::success(__('lang.messages.deleted', ['model' => __('models/drivers.singular')]));

        return redirect(route('admin.drivers.index'));
    }
}
