<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\OrderDataTable;
use App\Events\OrderChanged;
use App\Events\Sockets\OrderTracking;
use App\Helpers\Utils;
use App\Http\Requests;
use App\Http\Requests\CreateOrderRequest;
use App\Http\Requests\UpdateOrderRequest;
use App\Models\Customer;
use App\Models\Driver;
use App\Models\Order;
use App\Models\Orders;
use App\Models\OrderStatusTimestamp;
use App\Models\Store;
use App\Models\Admin;
use App\Notifications\OrderAssigned;
use App\Notifications\PushNotification;
use App\Repositories\OrderRepository;
use App\Services\OrderService;
use Flash;
use App\Http\Controllers\AppBaseController;
use App\Models\Zone;
use Illuminate\Http\Request;
use Notification;
use Response;

class OrderController extends AppBaseController
{
    /** @var  OrderRepository */
    private $orderRepository;

    public function __construct(OrderRepository $orderRepo)
    {
        $this->orderRepository = $orderRepo;
    }

    /**
     * Display a listing of the Order.
     *
     * @param OrderDataTable $orderDataTable
     * @return Response
     */
    public function index(OrderDataTable $orderDataTable,Request $request)
    {
//dd($request);

        \Log::info('This is in order controllerr');
        abort_if(\Gate::denies('orders.index'), 403, '403 Forbidden');
        $user=auth()->user();
       
        if($user->type == 'Vendor' || $user->type == 'Franchisee' || $user->type == 'Admin')
        {
            $stores = \Helpers::get_user_stores($user->id); 
            Order::whereIn('store_id',  $stores)->update(['seen'=> 1]);
            $Userzones = \Helpers::get_user_zones();
            $zones = Zone::whereIn('id',$Userzones)->get();

            $zone = implode(',',$Userzones);
            $data = Driver::whereRaw('zones IN('.$zone.')');
              $drivers = $data->where('active', 1)->where('is_busy',0)->where('on_duty',1)->get();
        }else{
            $zones = Zone::where('active',1)->get();
              $drivers = Driver::where('active', 1)->where('is_busy',0)->where('on_duty',1)->get();
        }
        
        $orderDataTable->with('zone',$request->zone)->with('daterange',$request->daterange);
        return $orderDataTable->render('admin.orders.index',compact('zones'),compact('drivers'));
    }

    /**
     * Show the form for creating a new Order.
     *
     * @return Response
     */
    public function create()
    {
        abort_if(\Gate::denies('orders.create'), 403, '403 Forbidden');
        return view('admin.orders.create');
    }

    /**
     * Store a newly created Order in storage.
     *
     * @param CreateOrderRequest $request
     *
     * @return Response
     */
    public function store(CreateOrderRequest $request)
    {   
       // dd($request);
        abort_if(\Gate::denies('orders.create'), 403, '403 Forbidden');
        $input = $request->all();
        $order = $this->orderRepository->create($input);
        if (!empty($input['driver_id'])) {
            $driver = \App\Models\Driver::find($input['driver_id']);
            $driver->notify(new \App\Notifications\OrderAssigned("Order #".$order->id." has been assigned to you.",$order));
        }
        Flash::success(__('lang.messages.saved', ['model' => __('models/orders.singular')]));
        return redirect(route('admin.orders.index'));
    }

    /**
     * Display the specified Order.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        abort_if(\Gate::denies('orders.view'), 403, '403 Forbidden');
        $order = $this->orderRepository->with('earnings')->find($id);
    
        if (empty($order)) {
            Flash::error(__('lang.messages.not_found', ['model' => __('models/orders.singular')]));
            return redirect(route('admin.orders.index'));
        }
        return view('admin.orders.show')->with('order', $order);
    }

    /**
     * Show the form for editing the specified Order.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        abort_if(\Gate::denies('orders.edit'), 403, '403 Forbidden');
        $order = $this->orderRepository->find($id);

        if (empty($order)) {
            Flash::error(__('lang.messages.not_found', ['model' => __('models/orders.singular')]));
            return redirect(route('admin.orders.index'));
        }
        $stores = \Helpers::get_user_stores();
        $stores = Store::whereIn('id', $stores)->get();
        $drivers = \App\Models\Driver::get();
        foreach ($drivers as $driver) {
            $append = "";
            $disabled = false;
            if ($driver->is_busy) {
                $append = " (Busy)";
                $disabled = true;
            }
            if ($driver->on_duty == 0) {
                $append = " (Off Duty)";
                $disabled = true;
            }
            if ($driver->cash_in_hand_overflow) {
                $append = " (BootCash)";
                $disabled = true;
            }
            if ($driver->active == 0) {
                $append = " (InActive)";
                $disabled = true;
            }
            if ($driver->is_verified == 0){
                $append = " (Not Verified)";
                $disabled = true;
            }
            $driver->name = $driver->name . ' (' . $driver->mobile. ')'.$append;
            $driver->disabled = $disabled;
        }
       
        return view('admin.orders.edit',compact('stores','drivers'))->with('order', $order);
    }

    /**
     * Update the specified Order in storage.
     *
     * @param  int              $id
     * @param UpdateOrderRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateOrderRequest $request, OrderService $orderService)
    {
       
        abort_if(\Gate::denies('orders.edit'), 403, '403 Forbidden');
        $order = $this->orderRepository->find($id);
        $input = $request->all();
        if (empty($order)) {
            Flash::error(__('lang.messages.not_found', ['model' => __('models/orders.singular')]));
            return redirect(route('admin.orders.index'));
        }
        if (!empty($input['driver_id']) && $order->driver_id != $input['driver_id']) {
            $orderService->assignDriverToOrder($order, $input['driver_id']);
            $input['order_status'] = 'ASSIGNED';
        }
        if (!empty($input['order_status']) && $order->order_status != $input['order_status']) {
            $orderService->changeOrderStatus($order, $input['order_status']);
        }
        $order = $this->orderRepository->update($input, $id);
        event(new OrderChanged($order));
        event(new OrderTracking($order->id,$order));
        Flash::success(__('lang.messages.updated', ['model' => __('models/orders.singular')]));
        return redirect(route('admin.orders.index'));
    }

    public function changeStatus(Request $req,OrderService $orderService){
        Order::where('id',$req->orderid)->update([
            'order_status' => $req->status
        ]);
        OrderStatusTimestamp::create([
            'order_id' => $req->orderid,
            'status' => $req->status,
            'comment' => 'Status Changed by Admin'
        ]);
        $order = Order::find($req->orderid);
        $orderService->changeOrderStatus($order, $req->status);
        event(new OrderChanged($order));
        event(new OrderTracking($order->id,$order));
        Notification::send($order->user, new PushNotification(['title' => 'Order Status Changed', 'description' => 'Your order status has been changed to '.$req->status]));
        Flash::success('Driver assigned successfully.');
        return redirect(route('orders.index'));
    }

    public function assignDriver(Request $req, OrderService $orderService){
        $req->validate([
            'orderid' => 'required',
            'driver_id' => 'required'
        ]);
        $order = Order::find($req->orderid);
        $orderService->assignDriverToOrder($order, $req->driver_id);
        event(new OrderChanged($order));
        Flash::success('Driver assigned successfully.');
        return redirect(route('admin.orders.index'));
    }
    
    public function pingDriver(Request $req, OrderService $orderService){
        $req->validate([
            'oorderid' => 'required',
        ]);
        $order = Order::find($req->oorderid);

        $orderService->pingAssignDriverToOrder($order, $order->driver_id);
        Flash::success('Driver ping successfully.');
        return redirect(route('admin.orders.index'));
    }

    /**
     * Remove the specified Order from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        abort_if(\Gate::denies('orders.delete'), 403, '403 Forbidden');
        $order = $this->orderRepository->find($id);
        if (empty($order)) {
            Flash::error(__('lang.messages.not_found', ['model' => __('models/orders.singular')]));
            return redirect(route('admin.orders.index'));
        }
        $this->orderRepository->delete($id);
        Flash::success(__('lang.messages.deleted', ['model' => __('models/orders.singular')]));

        return redirect(route('admin.orders.index'));
    }
}
