<?php

namespace App\Http\Controllers\Admin;

use App\Exports\ProductsExport;
use App\Http\Controllers\Controller;
use App\Jobs\ExportJob;
use App\Jobs\ImportJob;
use App\Mail\FileImported;
use App\Models\Category;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Schema;
use Laracasts\Flash\Flash;
use Maatwebsite\Excel\Facades\Excel;

class ImportExportController extends Controller
{
    public function index()
    {
        return view('admin.import_export.index');
    }

    public function import(Request $request){

        $request->validate([
            'file' => 'required',
            // 'email' => 'required|email',
            'type' => 'required',
        ]);
        if ($request->hasFile('file')){
            $path = \Helpers::uploadCsv($request->file('file'),'imports');
            //ImportJob::dispatch($path,$request->type,$request->email);
            if($request->type == 'product'){
                $this->importProducts($path);

            }else if ($request->type == 'category'){
                $this->importCategories($path);
            }
            Flash::success('File is being successfully imported.');
        }else{
            Flash::error('File not found');
        }
        return redirect()->back();
    }

    public function importCategories($file,$email=null){
        $file = public_path($file);
        // $success_file_path = str_replace('.csv','',$file). '_success.csv';
        // $error_file_path = str_replace('.csv','',$file). '_error.csv';
        // $success_file = fopen($success_file_path, 'w');
        // $error_file = fopen($error_file_path, 'w');
        $file = fopen($file, 'r');
        $categories = [];
        $keys = [];
        $i = 0;
        while(!feof($file)){
            if ($i==0){
                $i++;
                $keys = fgetcsv($file);
                continue;
            }
            $line = fgetcsv($file);
            if (!empty($line)){
                $categories[] = $line;
            }
        }
        fclose($file);
        // fputcsv($success_file, $keys);
        // fputcsv($error_file, $keys);
        $categories = collect($categories);
        $categories->each(function ($category) use ($keys){
            $category = $this->format_array(collect($category)->toArray(),$keys);
            
                if (!empty($category)){
                    try {
                        $cat = Category::create($category);
                        // fputcsv($success_file, $cat->toArray());
                    }catch (\Exception $e){
                    //     dd('error');
                        $cat = 'Error: ' . $e->getMessage();
                    //     // fputcsv($error_file, $product);
                    }
                }
        });
    }

    public function importProducts($file,$email=null){
        $file = public_path($file);
        // $success_file_path = str_replace('.csv','',$file). '_success.csv';
        // $error_file_path = str_replace('.csv','',$file). '_error.csv';
        // $success_file = fopen($success_file_path, 'w');
        // $error_file = fopen($error_file_path, 'w');
        $file = fopen($file, 'r');
        $products = [];
        $keys = [];
        $i = 0;
        while(!feof($file)){
            if ($i==0){
                $i++;
                $keys = fgetcsv($file);
                continue;
            }
            $line = fgetcsv($file);
            if (!empty($line)){
                $products[] = $line;
            }
        }
        fclose($file);
        // fputcsv($success_file, $keys);
        // fputcsv($error_file, $keys);
        $products = collect($products);
        $products->each(function ($product) use ($keys){
            $product = $this->format_array(collect($product)->toArray(),$keys);
            // if (in_array('id', $keys) !== false){
                //already in database, update record
            //     try {
            //         $prod = Product::find(array_search('id', $keys));
            //         unset($keys['id']);
            //         array_shift($product);
            //         Product::where('id',$prod->id)->update($product);
            //         fputcsv($success_file, $product);
            //     }catch (\Exception $e){
            //         $product[] = 'Error: ' . $e->getMessage();
            //         fputcsv($error_file, $product);
            //     }
            // }else{
                //id does not exist, create new record
                //$product = $this->format_array(collect($product)->toArray(),$keys);
                if (!empty($product)){
                    try {
                        // dd($product);
                        $prod = Product::create($product);
                        
                        // fputcsv($success_file, $prod->toArray());
                    }catch (\Exception $e){
                    //     dd('error');
                        $prod = 'Error: ' . $e->getMessage();
                    //     // fputcsv($error_file, $product);
                    }
                }
            // }
        });
        // fclose($success_file);
        // fclose($error_file);
        // Mail::to($email)
        //     ->send(new FileImported($success_file_path,$error_file_path));
    }

    public function format_array($arr,$keys,$index = 0) : array{
        $new_arr = [];
        foreach ($arr as $key => $value){
            try {
                $new_arr = array_merge($new_arr, [trim($keys[$key]) => trim($value)]);
            }catch (\Exception $e){
                continue;
            }
        }
        return $new_arr;
    }

    public function export(Request $request){
        $request->validate([
            'type' => 'required'
        ]);

        $tablename = null;
        switch ($request->type){
            case 'categories':
                $tablename = "categories";
                break;
            case 'products':
                $tablename = "products";
                break;
            case 'coupon':
                $tablename = "coupons";
                break;
            case 'order':
                $tablename = "orders";
                break;
            case 'order_item':
                $tablename = "order_items";
                break;
            case 'customer':
                $tablename = "users";
                break;
            case 'address':
                $tablename = "addresses";
                break;
            case 'banners':
                $tablename = "banners";
                break;
            case 'drivers':
                $tablename = "drivers";
                break;
            case 'payments':
                $tablename = "payments";
                break;
            case 'stores':
                $tablename = "stores";
                break;
            case 'store_timings':
                $tablename = "store_timings";
                break;
            default:
                Flash::error('Invalid type');
                throw new \Exception('Invalid type');
        }
        $fileName = $tablename.time()."-".rand(1,9999).'.csv';
        $destinationPath = public_path('/storage/exports/'.$fileName);
        $columns = Schema::getColumnListing($tablename);
        $file = fopen($destinationPath, 'w');
        fputcsv($file, $columns);
        DB::table($tablename)->orderBy('id')->chunk(100, function ($alldata) use ($file) {
            foreach ($alldata as $row) {
                fputcsv($file, (array) $row);
            }
        });
        fclose($file);
        $headers = array(
            'Content-Type' => 'text/csv',
        );
        return response()->download($destinationPath, $tablename.date("d-m-Y H:i:s").'.csv', $headers);

    }

}
