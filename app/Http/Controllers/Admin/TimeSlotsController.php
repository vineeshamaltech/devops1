<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\TimeSlotsDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateTimeSlotsRequest;
use App\Http\Requests\UpdateTimeSlotsRequest;
use App\Models\Store;
use App\Models\TimeSlots;
use App\Repositories\TimeSlotsRepository;
use Carbon\Carbon;
use Flash;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Response;

class TimeSlotsController extends AppBaseController
{
    /** @var  StoreTimingRepository */
    private $timeSlotsRepository;

    public function __construct(TimeSlotsRepository $timeSlotsRepository)
    {
        $this->timeSlotsRepository = $timeSlotsRepository;
    }

    /**
     * Display a listing of the StoreTiming.
     *
     * @param StoreTimingDataTable $storeTimingDataTable
     * @return Response
     */
    public function index()
    {
        \Log::info('This is in timeslots controll  request');
        abort_if(\Gate::denies('homeServiceTimeSlots.index'), 403, '403 Forbidden');
        $timeslots = TimeSlots::all();
        return view('admin.time_slots.index',compact('timeslots'));
    }

    /**
     * Show the form for creating a new StoreTiming.
     *
     * @return Response
     */
    public function create()
    {
        abort_if(\Gate::denies('homeServiceTimeSlots.create'), 403, '403 Forbidden');
        $timeslots = TimeSlots::all();
        $monday = TimeSlots::where('day','Monday')->get();
        $tuesday = TimeSlots::where('day','Tuesday')->get();
        $wednesday = TimeSlots::where('day','Wednesday')->get();
        $thursday = TimeSlots::where('day','Thursday')->get();
        $friday = TimeSlots::where('day','Friday')->get();
        $saturday = TimeSlots::where('day','Saturday')->get();
        $sunday = TimeSlots::where('day','Sunday')->get();
        
        if (empty($timeslots)) {
            Flash::error(__('lang.messages.not_found', ['model' => __('models/time_slots.singular')]));
            return redirect(route('admin.storeTimings.index'));
        }
        return view('admin.time_slots.create',compact('timeslots','sunday','monday','tuesday','wednesday','thursday','friday','saturday','sunday'));
    }

    /**
     * Store a newly created StoreTiming in storage.
     *
     * @param CreateStoreTimingRequest $request
     *
     * @return Response
     */
    public function store(Request $request)
    {
        abort_if(\Gate::denies('homeServiceTimeSlots.create'), 403, '403 Forbidden');
        $input = $request->all();
        TimeSlots::truncate();
        $days = [
            'monday',
            'tuesday',
            'wednesday',
            'thursday',
            'friday',
            'saturday',
            'sunday',
        ];
        foreach($days as $day){
            $days_array = $input[$day.'_open'];
            for($i=0;$i<count($days_array);$i++){
                $input['day'] = ucfirst($day);
                $input['from'] = Carbon::parse($input[$day.'_open'][$i])->format('H:i');
                $input['to'] = Carbon::parse($input[$day.'_close'][$i])->format('H:i');
                $input['max_bookings'] = $input[$day.'_max_booking'][$i];
                $input['enabled'] = 1;
                $timeslots = TimeSlots::create($input);
            }
        }
        Flash::success(__('lang.messages.saved', ['model' => __('models/time_slots.singular')]));

        return redirect(route('admin.timeSlots.index'));
    }

    /**
     * Display the specified StoreTiming.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        abort_if(\Gate::denies('homeServiceTimeSlots.view'), 403, '403 Forbidden');
        $timeslots = TimeSlots::find($id);

        if (empty($timeslots)) {
            Flash::error(__('lang.messages.not_found', ['model' => __('models/time_slots.singular')]));

            return redirect(route('admin.timeSlots.index'));
        }

        return view('admin.time_slots.show')->with('timeSlots', $timeslots);
    }

    /**
     * Show the form for editing the specified StoreTiming.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        abort_if(\Gate::denies('homeServiceTimeSlots.edit'), 403, '403 Forbidden');
        $timeslots = TimeSlots::find($id);
        if (empty($timeslots)) {
            Flash::error(__('lang.messages.not_found', ['model' => __('models/time_slots.singular')]));
            return redirect(route('admin.storeTimings.index'));
        }
     
        return view('admin.time_slots.edit',compact('timeslots'));
    }

    /**
     * Update the specified StoreTiming in storage.
     *
     * @param  int              $id
     * @param UpdateStoreTimingRequest $request
     *
     * @return Response
     */
    public function update($id, CreateTimeSlotsRequest $request)
    {
        abort_if(\Gate::denies('homeServiceTimeSlots.edit'), 403, '403 Forbidden');
        $timeslots = $this->timeSlotsRepository->find($id);

        if (empty($timeslots)) {
            Flash::error(__('lang.messages.not_found', ['model' => __('models/time_slots.singular')]));

            return redirect(route('admin.timeSlots.index'));
        }

        $input = $request->all();
        $input['from'] = Carbon::parse($input['from'])->format('H:i');
        $input['to'] = Carbon::parse($input['to'])->format('H:i');

        $slots = $this->timeSlotsRepository->update($input, $id);

        Flash::success(__('lang.messages.updated', ['model' => __('models/time_slots.singular')]));

        return redirect(route('admin.timeSlots.index'));
    }

    /**
     * Remove the specified StoreTiming from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        abort_if(\Gate::denies('homeServiceTimeSlots.delete'), 403, '403 Forbidden');
        $slots = $this->timeSlotsRepository->find($id);

        if (empty($slots)) {
            Flash::error(__('lang.messages.not_found', ['model' => __('models/time_slots.singular')]));

            return redirect(route('admin.storeTimings.index'));
        }

        $this->timeSlotsRepository->delete($id);

        Flash::success(__('lang.messages.deleted', ['model' => __('models/time_slots.singular')]));

        return redirect(route('admin.timeSlots.index'));
    }

}
