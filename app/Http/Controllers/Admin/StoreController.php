<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\StoreDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateStoreRequest;
use App\Http\Requests\UpdateStoreRequest;
use App\Models\StoreCategoriesAssoc;
use App\Repositories\StoreRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use App\Models\Store;
use App\Models\Zone;
use App\Models\StoreTiming;
use Response;

class StoreController extends AppBaseController
{
    /** @var  StoreRepository */
    private $storeRepository;

    public function __construct(StoreRepository $storeRepo)
    {
        $this->storeRepository = $storeRepo;
    }

    /**
     * Display a listing of the Store.
     *
     * @param StoreDataTable $storeDataTable
     * @return Response
     */
    // public function index(StoreDataTable $storeDataTable)
    // {
    //     \Log::info('This is in store controll  request');
    //     abort_if(\Gate::denies('stores.index'), 403, '403 Forbidden');
    //     return $storeDataTable->render('admin.stores.index');
    // }
  public function index(StoreDataTable $storeDataTable)
    {
        
            $day = date('l');
        //echo CURRENT_TIME;

        Store::where('is_open',true)->update(['is_open'=>false]);
        //die();
        $stores = Store::all();
        foreach($stores as $store){
            $store_timing = StoreTiming::where('store_id',$store->id)->where('day', 'like', $day)->first();
            if($store_timing){
                if($store_timing->open < date("H:i:s") && $store_timing->close > date("H:i:s")){
                    Store::where('id',$store->id)->update(['is_open'=>true]);
                }
            }
        }
        // $count = Store::whereHas('store_timings', function ($query) use ($day) {
        //     $query->where('day', 'like', $day);
        //     $query->whereRaw('CURRENT_TIME < close && CURRENT_TIME > open');
        // })->update(['is_open'=>1]);
       // $this->info($count.' Store activated');
            \Log::info('This is in index StoreController');
        abort_if(\Gate::denies('stores.index'), 403, '403 Forbidden');
        return $storeDataTable->render('admin.stores.index');
    }

    /**
     * Show the form for creating a new Store.
     *
     * @return Response
     */
    public function create()
    {
        abort_if(\Gate::denies('stores.create'), 403, '403 Forbidden');
        $payment_methods = [];
        $zones = Zone::all();
        return view('admin.stores.create',compact('payment_methods','zones'));
    }

    /**
     * Store a newly created Store in storage.
     *
     * @param CreateStoreRequest $request
     *
     * @return Response
     */
    public function store(CreateStoreRequest $request)
    {   
        //dd($request);
        abort_if(\Gate::denies('stores.create'), 403, '403 Forbidden');
        $input = $request->all();
        if (\Helpers::is_super_admin() || \Helpers::is_admin()){
            if(empty($input['admin_id'])){
                Flash::error('Please select a vendor');
                return redirect()->back()->withInput();
            }
        }else{
            $input['admin_id'] = \Auth::user()->id;
        }
        $input['zone'] = implode(",",$request->zone);
        $storeCategories = $input['store_categories'];
        $input['store_categories'] = implode(',', $storeCategories);
        if ($request->hasFile('image')) {
            $input['image'] = \Helpers::uploadImage($request->file('image'), 'stores');
        }else{
            Flash::error('Store image is required');
            return redirect()->back()->withInput();
        }
        $input['payment_methods'] = implode(',', $input['payment_methods']);
        $store = $this->storeRepository->create($input);
        foreach ($storeCategories as $category) {
            StoreCategoriesAssoc::create([
                'store_id' => $store->id,
                'store_category_id' => $category
            ]);
        }
        Flash::success(__('lang.messages.saved', ['model' => __('models/stores.singular')]));

        return redirect(route('admin.stores.index'));
    }

    /**
     * Display the specified Store.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        abort_if(\Gate::denies('stores.view'), 403, '403 Forbidden');
        $store = $this->storeRepository->with(['vendor','serviceCategory','store_categories'])->find($id);
        $storeCategories = StoreCategoriesAssoc::where('store_id', $id)->pluck('store_category_id')->toArray();
        if ($store->admin_id != \Auth::user()->id && !\Helpers::is_super_admin() && !\Helpers::is_admin() && !\Helpers::is_Franchisee()) {
            Flash::error('You are not authorized to view this store');
            return redirect(route('admin.stores.index'));
        }
        if (empty($store)) {
            Flash::error(__('lang.messages.not_found', ['model' => __('models/stores.singular')]));

            return redirect(route('admin.stores.index'));
        }

        return view('admin.stores.show',compact(['store','storeCategories']));
    }

    /**
     * Show the form for editing the specified Store.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        abort_if(\Gate::denies('stores.edit'), 403, '403 Forbidden');
        $store = $this->storeRepository->find($id);
        //dd($store->toArray());
        if (empty($store)) {
            Flash::error(__('lang.messages.not_found', ['model' => __('models/stores.singular')]));

            return redirect(route('admin.stores.index'));
        }
        if ($store->admin_id != \Auth::user()->id && !\Helpers::is_super_admin() && !\Helpers::is_admin() && !\Helpers::is_Franchisee()) {
            Flash::error('You are not authorized to edit this store');
            return redirect(route('admin.stores.index'));
        }
        $storeCategories = StoreCategoriesAssoc::where('store_id', $id)->pluck('store_category_id')->toArray();
        $payment_methods = explode(',', $store->payment_methods);
        
        $zones = Zone::where('active', 1)->get();
        $zonesSelected = explode(",",$store->zone);
       // dd($zones);
        foreach ($zones as $zone){
            if (in_array($zone->id,$zonesSelected)){
                $zone->selected = true;
            }else{
                $zone->selected = false;
            }
        }
        return view('admin.stores.edit',compact('storeCategories','store','payment_methods','zones'));
    }

    /**
     * Update the specified Store in storage.
     *
     * @param  int              $id
     * @param UpdateStoreRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateStoreRequest $request)
    {
        //dd($request);
        abort_if(\Gate::denies('stores.edit'), 403, '403 Forbidden');
        $store = $this->storeRepository->find($id);
        $input = $request->all();
       // dd($input);
        if (empty($store)) {
            Flash::error(__('lang.messages.not_found', ['model' => __('models/stores.singular')]));
            return redirect(route('admin.stores.index'));
        }
        if ($store->admin_id != \Auth::user()->id && !\Helpers::is_super_admin() && !\Helpers::is_admin() && !\Helpers::is_Franchisee()) {
            Flash::error('You are not authorized to edit this store');
            return redirect(route('admin.stores.index'));
        }
        $input['zone'] = implode(",",$request->zone);

        $storeCategories = $input['store_categories'];
        $input['store_categories'] = implode(',', $storeCategories);
        if ($request->hasFile('image')) {
            $input['image'] = \Helpers::uploadImage($request->file('image'), 'stores');
        }
        $input['payment_methods'] = implode(',', $input['payment_methods']);
        $store = $this->storeRepository->update($input, $id);
       // dd($store);
        StoreCategoriesAssoc::where('store_id', $id)->delete();
        foreach ($storeCategories as $category) {
            StoreCategoriesAssoc::create([
                'store_id' => $id,
                'store_category_id' => $category
            ]);
        }
        Flash::success(__('lang.messages.updated', ['model' => __('models/stores.singular')]));
        return redirect(route('admin.stores.index'));
    }

    /**
     * Remove the specified Store from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        abort_if(\Gate::denies('stores.delete'), 403, '403 Forbidden');
        $store = $this->storeRepository->find($id);
        if (empty($store)) {
            Flash::error(__('lang.messages.not_found', ['model' => __('models/stores.singular')]));
            return redirect(route('admin.stores.index'));
        }
            $banners = \DB::table('banners')->where('store_id',$id)->get();
          if(@$banners[0])
              DB::table('banners')->delete($id);
          
        $this->storeRepository->delete($id);
        Flash::success(__('lang.messages.deleted', ['model' => __('models/stores.singular')]));
        return redirect(route('admin.stores.index'));
    }

    public function get_store(){
        $user = auth()->user()->id;
        $stores = Store::Where('admin_id',$user)->get();
         dd($store);
        $headers = array(
            "Content-type" => "text/csv",
            "Content-Disposition" => "attachment; filename=file.csv",
            "Pragma" => "no-cache",
            "Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
            "Expires" => "0"
        );
        if(count($stores) < 1){
            $stores = Store::get();
        }
        $columns = array('Store Id', 'Store Name');
        $callback = function() use ($stores, $columns)
        {
            $file = fopen('php://output', 'w');
            fputcsv($file, $columns);
            foreach($stores as $store) {
                fputcsv($file, array($store->id, $store->name));
            }
            fclose($file);
        };
        return response()->stream($callback, 200, $headers);
    }
}
