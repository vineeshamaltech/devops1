<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\DriverKycDataTable;
use App\Http\Requests;
use App\Http\Requests\CreatedriverKycRequest;
use App\Http\Requests\UpdatedriverKycRequest;
use App\Models\Admin;
use App\Models\Driver;
use App\Models\DriverKyc;
use App\Notifications\KycStatusChanged;
use App\Notifications\KycRejected;
use App\Repositories\DriverKycRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Notification;
use Response;

class DriverKycController extends AppBaseController
{
    /** @var  driverKycRepository */
    private $driverKycRepository;

    public function __construct(DriverKycRepository $driverKycRepo)
    {
        $this->driverKycRepository = $driverKycRepo;
    }

    /**
     * Display a listing of the driverKyc.
     *
     * @param driverKycDataTable $driverKycDataTable
     * @return Response
     */
    public function index(DriverKycDataTable $DriverKycDataTable)
    {
        \Log::info('This is in Driver controller');
        abort_if(\Gate::denies('driverKycs.index'), 403, '403 Forbidden');
        return $DriverKycDataTable->render('admin.driver_kycs.index');
    }

    /**
     * Show the form for creating a new driverKyc.
     *
     * @return Response
     */
    public function create()
    {
        abort_if(\Gate::denies('driverKycs.create'), 403, '403 Forbidden');
        return view('admin.driver_kycs.create');
    }

    /**
     * Store a newly created driverKyc in storage.
     *
     * @param CreatedriverKycRequest $request
     *
     * @return Response
     */
    public function store(CreatedriverKycRequest $request)
    {
        abort_if(\Gate::denies('driverKycs.create'), 403, '403 Forbidden');
        $input = $request->all();

        $this->driverKycRepository->deleteWhere(['driver_id' => $input['driver_id']]);

        Driver::where('id', $input['driver_id'])->update(['is_verified' => false]);

        if($request->hasFile('aadhar')){
            $input['doc_type'] = 'AADHAR';
            $input['photo'] = \Helpers::uploadImage($request->file('aadhar'), 'driver_kyc');
            Driver::where('id', $request->driver_id)->update(['aadhar_photo' => $input['photo']]);
            $this->driverKycRepository->create($input);
        }

        if($request->hasFile('aadhar_back')){
            $input['doc_type'] = 'AADHARBACK';
            $input['photo'] = \Helpers::uploadImage($request->file('aadhar_back'), 'driver_kyc');
            Driver::where('id', $request->driver_id)->update(['aadhar_photo_back' => $input['photo']]);
            $this->driverKycRepository->create($input);
        }

        if($request->hasFile('dl')){
            $input['doc_type'] = 'DL';
            $input['photo'] = \Helpers::uploadImage($request->file('dl'), 'driver_kyc');
            Driver::where('id', $request->driver_id)->update(['dl_photo' => $input['photo']]);
            $this->driverKycRepository->create($input);
        }

        if($request->hasFile('vehicle_img')){
            $input['doc_type'] = 'VEHICLEIMG';
            $input['photo'] = \Helpers::uploadImage($request->file('vehicle_img'), 'driver_kyc');
            Driver::where('id', $request->driver_id)->update(['vehicle_img' => $input['photo']]);
            $this->driverKycRepository->create($input);
        }

        Flash::success(__('lang.messages.saved', ['model' => __('models/driverKycs.singular')]));

        return redirect(route('admin.driverKycs.index'));
    }

    /**
     * Display the specified driverKyc.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        abort_if(\Gate::denies('driverKycs.view'), 403, '403 Forbidden');

        $driverKyc = DriverKyc::where('driver_id',$id)->get();


        if (empty($driverKyc)) {
            Flash::error(__('lang.messages.not_found', ['model' => __('models/driverKycs.singular')]));

            return redirect(route('admin.driverKycs.index'));
        }

        return view('admin.driver_kycs.show')->with('driverKyc', $driverKyc);
    }

    /**
     * Show the form for editing the specified driverKyc.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        abort_if(\Gate::denies('driverKycs.edit'), 403, '403 Forbidden');
        $driverKyc = $this->driverKycRepository->find($id);

        if (empty($driverKyc)) {
            Flash::error(__('lang.messages.not_found', ['model' => __('models/driverKycs.singular')]));

            return redirect(route('admin.driverKycs.index'));
        }

        $doc = $this->driverKycRepository->findWhere(['driver_id' => $driverKyc->driver_id]);
        $documents = [];
        foreach ($doc as $document) {
            $document[$document->type] = $document->photo;
        }

        return view('admin.driver_kycs.edit',compact('documents'))->with('driverKyc', $driverKyc);
    }

    /**
     * Update the specified driverKyc in storage.
     *
     * @param  int              $id
     * @param UpdatedriverKycRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatedriverKycRequest $request)
    {
        abort_if(\Gate::denies('driverKycs.edit'), 403, '403 Forbidden');
        $driverKyc = $this->driverKycRepository->find($id);
        $input = $request->all();
        if (empty($driverKyc)) {
            Flash::error(__('lang.messages.not_found', ['model' => __('models/driverKycs.singular')]));
            return redirect(route('admin.driverKycs.index'));
        }
        if($request->hasFile('photo')){
            $input['photo'] = \Helpers::uploadImage($request->file('photo'), 'vendor_kyc');
        }
        $driverKyc = $this->driverKycRepository->update($input, $id);
        Flash::success(__('lang.messages.updated', ['model' => __('models/driverKycs.singular')]));
        return redirect(route('admin.driverKycs.index'));
    }

    public function approve($id){
        //approve admin document
        $driverKyc = $this->driverKycRepository->find($id);
        driverKyc::where('id',$id)->update(['status'=>'APPROVED']);
        $driver = Driver::find($driverKyc->driver_id);
        // Notification::send($driver,new KycStatusChanged($driverKyc));
        $allkyc = driverKyc::where('driver_id',$driver->id)->get();
        $shouldActivate = false;
        foreach ($allkyc as $kyc){
            if($kyc->status != 'APPROVED'){
                $shouldActivate = false;
            }else{
                $shouldActivate = true;
            }
        }
        if ($shouldActivate){
            Driver::where('id',$driver->id)->update(['is_verified'=>1]);
        }
        Flash::success(__('lang.messages.approved', ['model' => __('models/driverKycs.singular')]));
        return redirect()->back();
    }

    public function approveall($id){
      
        //approve admin document
        $driverKycs = DriverKyc::where('driver_id',$id)->update(['status'=>'APPROVED']);
        $driverKyc = DriverKyc::where('driver_id',$id)->orderBy('id','desc')->first();
        $driver = Driver::find($id);
        $driver->is_verified = 1;
        $driver->save();
        Notification::send($driver,new KycStatusChanged($driverKyc));
        Flash::success(__('lang.messages.approved', ['model' => __('models/driverKycs.singular')]));
        return redirect()->back();
    }

    public function reject($id){
        $driverKyc = $this->driverKycRepository->find($id);
        driverKyc::where('id',$id)->update(['status'=>'REJECTED']);
        $driver = Driver::find($driverKyc->driver_id);

        Driver::where('id',$driver->id)->update(['is_verified'=>0]);
        Notification::send($driver,new KycStatusChanged($driverKyc));

        Flash::success(__('Kyc Rejected Success', ['model' => __('models/driverKycs.singular')]));
        return redirect()->back();
    }

    public function rejectall($id){
        //approve admin document
        $driverKycs = DriverKyc::where('driver_id',$id)->update(['status'=>'REJECTED']);
        $driverKyc = DriverKyc::where('driver_id',$id)->orderBy('id','desc')->first();
        $driver = Driver::find($id);
        $driver->is_verified = 0;
        $driver->save();
        Notification::send($driver,new KycStatusChanged($driverKyc));
        Flash::success(__('Kyc Rejected Success', ['model' => __('models/driverKycs.singular')]));
        return redirect()->back();
    }

    /**
     * Remove the specified driverKyc from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        abort_if(\Gate::denies('driverKycs.delete'), 403, '403 Forbidden');
        $driverKyc = $this->driverKycRepository->find($id);

        if (empty($driverKyc)) {
            Flash::error(__('lang.messages.not_found', ['model' => __('models/driverKycs.singular')]));

            return redirect(route('admin.driverKycs.index'));
        }

        $this->driverKycRepository->delete($id);

        Flash::success(__('lang.messages.deleted', ['model' => __('models/driverKycs.singular')]));

        return redirect(route('admin.driverKycs.index'));
    }
}
