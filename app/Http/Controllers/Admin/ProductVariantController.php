<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\ProductVariantDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateProductVariantRequest;
use App\Http\Requests\UpdateProductVariantRequest;
use App\Models\Product;
use App\Models\Store;
use App\Models\Category;
use App\Repositories\ProductVariantRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Illuminate\Support\Facades\DB;
use Response;

class ProductVariantController extends AppBaseController
{
    /** @var  ProductVariantRepository */
    private $productVariantRepository;

    public function __construct(ProductVariantRepository $productVariantRepo)
    {
        $this->productVariantRepository = $productVariantRepo;
    }

    /**
     * Display a listing of the ProductVariant.
     *
     * @param ProductVariantDataTable $productVariantDataTable
     * @return Response
     */
    public function index(ProductVariantDataTable $productVariantDataTable)
    {
        \Log::info('This is in product variant controll  request');
        abort_if(\Gate::denies('productVariants.index'), 403, '403 Forbidden');
        return $productVariantDataTable->render('admin.product_variants.index');
    }

    /**
     * Show the form for creating a new ProductVariant.
     *
     * @return Response
     */
    public function create()
    {
        
            abort_if(\Gate::denies('productVariants.create'), 403, '403 Forbidden');
        $stores = \Helpers::get_user_stores();
        $stores = Store::whereIn('id', $stores)->get();
        $products = DB::table('products')
            ->join('stores', 'products.store_id', '=', 'stores.id')
            ->select('products.id','products.name', 'stores.name as store_name')
            ->where('products.product_type','!=','SIMPLE')
            // ->whereIn('products.store_id',$product_id)
            ->get();
        return view('admin.product_variants.create',compact('products','stores'));

        // abort_if(\Gate::denies('productVariants.create'), 403, '403 Forbidden');
        // $stores = \Helpers::get_user_stores();
        // //$products = Product::with('store')->whereIn('store_id',$stores)->get();
        // $products = DB::table('products')
        //     ->join('stores', 'products.store_id', '=', 'stores.id')
        //     ->select('products.id','products.name', 'stores.name as store_name')
        //     ->where('products.product_type','!=','SIMPLE')
        //     ->whereIn('products.store_id',$stores)
        //     ->get();

        // return view('admin.product_variants.create',compact('products'));
    }

    /**
     * Store a newly created ProductVariant in storage.
     *
     * @param CreateProductVariantRequest $request
     *
     * @return Response
     */
    public function store(CreateProductVariantRequest $request)
    {
        abort_if(\Gate::denies('productVariants.create'), 403, '403 Forbidden');
        $input = $request->all();
        if ($request->hasFile('image')){
            $input['image'] = \Helpers::uploadImage($request->image, 'productVariants');
        }
        $productVariant = $this->productVariantRepository->create($input);

        Flash::success(__('lang.messages.saved', ['model' => __('models/productVariants.singular')]));

        return redirect(route('admin.productVariants.index'));
    }

    /**
     * Display the specified ProductVariant.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        abort_if(\Gate::denies('productVariants.view'), 403, '403 Forbidden');
        $productVariant = $this->productVariantRepository->find($id);

        if (empty($productVariant)) {
            Flash::error(__('lang.messages.not_found', ['model' => __('models/productVariants.singular')]));

            return redirect(route('admin.productVariants.index'));
        }

        return view('admin.product_variants.show')->with('productVariant', $productVariant);
    }

    /**
     * Show the form for editing the specified ProductVariant.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        
        abort_if(\Gate::denies('productVariants.edit'), 403, '403 Forbidden');
        $productVariant = $this->productVariantRepository->find($id);

        if (empty($productVariant)) {
            Flash::error(__('lang.messages.not_found', ['model' => __('models/productVariants.singular')]));
            return redirect(route('admin.productVariants.index'));
        }
    $stores = \Helpers::get_user_stores();
        $stores = Store::whereIn('id', $stores)->get();
       // $stores = \Helpers::get_user_stores();
        $products = DB::table('products')
            ->join('stores', 'products.store_id', '=', 'stores.id')
            ->select('products.id','products.name', 'stores.name as store_name')
            ->where('products.product_type','!=','SIMPLE')
          //  ->whereIn('products.store_id',$stores)
            ->get();

        return view('admin.product_variants.edit',compact('products','stores'))->with('productVariant', $productVariant);
        // $stores = \Helpers::get_user_stores();
        // $products = DB::table('products')
        //     ->join('stores', 'products.store_id', '=', 'stores.id')
        //     ->select('products.id','products.name', 'stores.name as store_name')
        //     ->where('products.product_type','!=','SIMPLE')
        //     ->whereIn('products.store_id',$stores)
        //     ->get();

        // return view('admin.product_variants.edit',compact('products'))->with('productVariant', $productVariant);
    }

    /**
     * Update the specified ProductVariant in storage.
     *
     * @param  int              $id
     * @param UpdateProductVariantRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateProductVariantRequest $request)
    {
        abort_if(\Gate::denies('productVariants.edit'), 403, '403 Forbidden');
        $productVariant = $this->productVariantRepository->find($id);
        $input = $request->all();
        if (empty($productVariant)) {
            Flash::error(__('lang.messages.not_found', ['model' => __('models/productVariants.singular')]));

            return redirect(route('admin.productVariants.index'));
        }

        if ($request->hasFile('image')){
           // dd('yes');
            \Helpers::deleteImage($productVariant->image);
            $input['image'] = \Helpers::uploadImage($request->image, 'productVariants');
        }

        $productVariant = $this->productVariantRepository->update($input, $id);

        Flash::success(__('lang.messages.updated', ['model' => __('models/productVariants.singular')]));

        return redirect(route('admin.productVariants.index'));
    }

    /**
     * Remove the specified ProductVariant from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        abort_if(\Gate::denies('productVariants.delete'), 403, '403 Forbidden');
        $productVariant = $this->productVariantRepository->find($id);
        if (empty($productVariant)) {
            Flash::error(__('lang.messages.not_found', ['model' => __('models/productVariants.singular')]));

            return redirect(route('admin.productVariants.index'));
        }
        $this->productVariantRepository->delete($id);
        Flash::success(__('lang.messages.deleted', ['model' => __('models/productVariants.singular')]));
        return redirect(route('admin.productVariants.index'));
    }
}
