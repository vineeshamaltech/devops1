<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\PaymentDataTable;
use App\Http\Requests;
use App\Http\Requests\CreatePaymentRequest;
use App\Http\Requests\UpdatePaymentRequest;
use App\Repositories\PaymentRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class PaymentController extends AppBaseController
{
    /** @var  PaymentRepository */
    private $paymentRepository;

    public function __construct(PaymentRepository $paymentRepo)
    {
        $this->paymentRepository = $paymentRepo;
    }

    /**
     * Display a listing of the Payment.
     *
     * @param PaymentDataTable $paymentDataTable
     * @return Response
     */
    public function index(PaymentDataTable $paymentDataTable)
    {
        \Log::info('This is in payment controlller');
        abort_if(\Gate::denies('payments.index'), 403, '403 Forbidden');
        return $paymentDataTable->render('admin.payments.index');
    }

    /**
     * Show the form for creating a new Payment.
     *
     * @return Response
     */
    public function create()
    {
        abort_if(\Gate::denies('payments.create'), 403, '403 Forbidden');
        return view('admin.payments.create');
    }

    /**
     * Store a newly created Payment in storage.
     *
     * @param CreatePaymentRequest $request
     *
     * @return Response
     */
    public function store(CreatePaymentRequest $request)
    {
        \Log::info('This is in payment controll create payment request');
        abort_if(\Gate::denies('payments.create'), 403, '403 Forbidden');
        $input = $request->all();

        $payment = $this->paymentRepository->create($input);

        Flash::success(__('lang.messages.saved', ['model' => __('models/payments.singular')]));

        return redirect(route('admin.payments.index'));
    }

    /**
     * Display the specified Payment.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        \Log::info('This is in payment controll show payment request');
        abort_if(\Gate::denies('payments.view'), 403, '403 Forbidden');
        $payment = $this->paymentRepository->find($id);

        if (empty($payment)) {
            Flash::error(__('lang.messages.not_found', ['model' => __('models/payments.singular')]));

            return redirect(route('admin.payments.index'));
        }

        return view('admin.payments.show')->with('payment', $payment);
    }

    /**
     * Show the form for editing the specified Payment.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        \Log::info('This is in payment controll edit request');
        abort_if(\Gate::denies('payments.edit'), 403, '403 Forbidden');
        $payment = $this->paymentRepository->find($id);

        if (empty($payment)) {
            Flash::error(__('lang.messages.not_found', ['model' => __('models/payments.singular')]));

            return redirect(route('admin.payments.index'));
        }

        return view('admin.payments.edit')->with('payment', $payment);
    }

    /**
     * Update the specified Payment in storage.
     *
     * @param  int              $id
     * @param UpdatePaymentRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePaymentRequest $request)
    {
        \Log::info('This is in payment controll update payment request');
        abort_if(\Gate::denies('payments.edit'), 403, '403 Forbidden');
        $payment = $this->paymentRepository->find($id);

        if (empty($payment)) {
            Flash::error(__('lang.messages.not_found', ['model' => __('models/payments.singular')]));

            return redirect(route('admin.payments.index'));
        }

        $payment = $this->paymentRepository->update($request->all(), $id);

        Flash::success(__('lang.messages.updated', ['model' => __('models/payments.singular')]));

        return redirect(route('admin.payments.index'));
    }

    /**
     * Remove the specified Payment from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        \Log::info('This is in payment controll destroy payment request');
        abort_if(\Gate::denies('payments.delete'), 403, '403 Forbidden');
        $payment = $this->paymentRepository->find($id);

        if (empty($payment)) {
            Flash::error(__('lang.messages.not_found', ['model' => __('models/payments.singular')]));

            return redirect(route('admin.payments.index'));
        }

        $this->paymentRepository->delete($id);

        Flash::success(__('lang.messages.deleted', ['model' => __('models/payments.singular')]));

        return redirect(route('admin.payments.index'));
    }
}
