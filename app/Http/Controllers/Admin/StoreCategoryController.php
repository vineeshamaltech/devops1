<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\StoreCategoryDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateStoreCategoryRequest;
use App\Http\Requests\UpdateStoreCategoryRequest;
use App\Models\ServiceCategory;
use App\Repositories\StoreCategoryRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Response;

class StoreCategoryController extends AppBaseController
{
    /** @var  StoreCategoryRepository */
    private $storeCategoryRepository;

    public function __construct(StoreCategoryRepository $storeCategoryRepo)
    {
        $this->storeCategoryRepository = $storeCategoryRepo;
    }

    /**
     * Display a listing of the StoreCategory.
     *
     * @param StoreCategoryDataTable $storeCategoryDataTable
     * @return Response
     */
    public function index(StoreCategoryDataTable $storeCategoryDataTable)
    {
        \Log::info('This is in store category control  request');
        abort_if(\Gate::denies('storeCategories.index'), 403, '403 Forbidden');
        return $storeCategoryDataTable->render('admin.store_categories.index');
    }

    /**
     * Show the form for creating a new StoreCategory.
     *
     * @return Response
     */
    public function create()
    {
        abort_if(\Gate::denies('storeCategories.create'), 403, '403 Forbidden');
        $serviceCategories = ServiceCategory::get();
        return view('admin.store_categories.create');
    }

    /**
     * Store a newly created StoreCategory in storage.
     *
     * @param CreateStoreCategoryRequest $request
     *
     * @return Response
     */
    public function store(CreateStoreCategoryRequest $request)
    {
        abort_if(\Gate::denies('storeCategories.create'), 403, '403 Forbidden');
        $input = $request->all();

        if($request->has('image')){
            $input['image'] = \Helpers::uploadImage($request->file('image'), 'store_categories');
        }else{
            Flash::error('Image is required');
            return redirect()->back()->withInput();
        }


        $storeCategory = $this->storeCategoryRepository->create($input);

        Flash::success(__('lang.messages.saved', ['model' => __('models/storeCategories.singular')]));

        return redirect(route('admin.storeCategories.index'));
    }

    /**
     * Display the specified StoreCategory.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        abort_if(\Gate::denies('storeCategories.view'), 403, '403 Forbidden');
        $storeCategory = $this->storeCategoryRepository->find($id);

        if (empty($storeCategory)) {
            Flash::error(__('lang.messages.not_found', ['model' => __('models/storeCategories.singular')]));

            return redirect(route('admin.storeCategories.index'));
        }

        return view('admin.store_categories.show')->with('storeCategory', $storeCategory);
    }

    /**
     * Show the form for editing the specified StoreCategory.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        abort_if(\Gate::denies('storeCategories.edit'), 403, '403 Forbidden');
        $storeCategory = $this->storeCategoryRepository->find($id);

        if (empty($storeCategory)) {
            Flash::error(__('lang.messages.not_found', ['model' => __('models/storeCategories.singular')]));

            return redirect(route('admin.storeCategories.index'));
        }

        return view('admin.store_categories.edit')->with('storeCategory', $storeCategory);
    }

    /**
     * Update the specified StoreCategory in storage.
     *
     * @param  int              $id
     * @param UpdateStoreCategoryRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateStoreCategoryRequest $request)
    {
        abort_if(\Gate::denies('storeCategories.edit'), 403, '403 Forbidden');
        $storeCategory = $this->storeCategoryRepository->find($id);

        if (empty($storeCategory)) {
            Flash::error(__('lang.messages.not_found', ['model' => __('models/storeCategories.singular')]));

            return redirect(route('admin.storeCategories.index'));
        }

        $storeCategory = $this->storeCategoryRepository->update($request->all(), $id);

        Flash::success(__('lang.messages.updated', ['model' => __('models/storeCategories.singular')]));

        return redirect(route('admin.storeCategories.index'));
    }

    /**
     * Remove the specified StoreCategory from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        abort_if(\Gate::denies('storeCategories.delete'), 403, '403 Forbidden');
        $storeCategory = $this->storeCategoryRepository->find($id);

        if (empty($storeCategory)) {
            Flash::error(__('lang.messages.not_found', ['model' => __('models/storeCategories.singular')]));

            return redirect(route('admin.storeCategories.index'));
        }

        $this->storeCategoryRepository->delete($id);

        Flash::success(__('lang.messages.deleted', ['model' => __('models/storeCategories.singular')]));

        return redirect(route('admin.storeCategories.index'));
    }

    /**
     * Remove the list of storeCategories from storage.
     *
     * @param  array $ids
     *
     * @return Response
     */
    public function destroy_multiple(Request $request)
    {
        abort_if(\Gate::denies('storeCategories.delete'), 403, '403 Forbidden');
        if($request->delete){
            $storeCategories = $this->storeCategoryRepository->findWhereIn('id',$request->delete);
            if (empty($storeCategories)) {
                Flash::error(__('lang.messages.not_found', ['model' => __('models/storeCategories.singular')]));
                return redirect(route('admin.storeCategories.index'));
            }
            foreach($storeCategories as $category){
                $this->storeCategoryRepository->delete($category->id);
            }
            Flash::success(__('lang.messages.deleted', ['model' => __('models/storeCategories.singular')]));
            return redirect(route('admin.storeCategories.index'));
        }else{
            Flash::error(__('lang.messages.no_data_selected', ['model' => __('models/storeCategories.singular')]));
            return redirect(route('admin.storeCategories.index'));
        }
        
    }
}
