<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Earning;
use App\Models\Order;
use App\Models\ServiceOrder;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ReportController extends Controller
{
    public function index()
    {
        \Log::info('This is in report controll  request');
        return view('admin.reports.index');
    }

    public function generate(Request $request)
    {
        $this->validate($request, [
            'report_of' => 'required',
            'reportable_id' => 'required',
            'type' => 'nullable',
            'interval' => 'nullable',
            'daterange' => 'required',
        ]);
        $daterange = explode('-', $request->daterange);
        $report = [];
        switch ($request->report_of) {
            case 'admin':
                $startDate = Carbon::parse($daterange[0]);
                $endDate = Carbon::parse($daterange[1]);
                if ($request->type == 'earnings') {
                    $report = $this->vendorEarningsReport($request->reportable_id, $startDate, $endDate, $request->interval);
                } else {
                    $report = $this->vendorOrderReport($request->reportable_id, $startDate, $endDate, $request->interval);
                }
                break;
            case 'vendor':
                $startDate = Carbon::parse($daterange[0]);
                $endDate = Carbon::parse($daterange[1]);
                if ($request->type == 'earnings') {
                    $report = $this->vendorEarningsReport($request->reportable_id, $startDate, $endDate, $request->interval);
                } else {
                    $report = $this->vendorOrderReport($request->reportable_id, $startDate, $endDate, $request->interval);
                }
                break;
            case 'driver':
                $startDate = Carbon::parse($daterange[0]);
                $endDate = Carbon::parse($daterange[1]);
                $report = $this->driverReport($request->reportable_id, $startDate, $endDate);
                break;
        }
        return view('admin.reports.index', $report);
    }

    // public function vendorEarningsReport($id, $startDate, $endDate, $interval = "day")
    // {
    //     $data_title = "Earning Report for " . $startDate->format('d-m-Y') . " to " . $endDate->format('d-m-Y');
    //     $stores = \Helpers::get_stores_by_user($id);
    //     $orders = Order::whereIn('store_id', $stores->pluck('id'))->whereBetween('created_at', [$startDate, $endDate])->get();
    //     $total_orders = count($orders);
    //     $total_amount = Order::whereIn('store_id', $stores->pluck('id'))->whereBetween('created_at', [$startDate, $endDate])->sum('total_price');
    //     $earnings = Earning::where('entity_type', 'App\Models\Store')->whereIn('order_id', $orders->pluck('id'))->whereBetween('created_at', [$startDate, $endDate])->sum('amount');
    //     if ($interval == "day") {
    //         $earning_loop = Earning::selectRaw('sum(amount) as amount, DATE(created_at) as date')->where('entity_type', 'App\Models\Store')->whereIn('order_id', $orders->pluck('id'))->whereBetween('created_at', [$startDate, $endDate])->groupBy('date')->get()->pluck('amount', 'date')->toArray();
    //     } else if ($interval == "week") {
    //         $earning_loop = Earning::selectRaw('sum(amount) as amount, DATE_FORMAT(DATE(created_at),"%u %Y") as date')->where('entity_type', 'App\Models\Store')->whereIn('order_id', $orders->pluck('id'))->whereBetween('created_at', [$startDate, $endDate])->groupBy(DB::raw('DATE_FORMAT(DATE(created_at),"%u %Y")'))->get()->pluck('amount', 'date')->toArray();
    //     } else if ($interval == "month") {
    //         $earning_loop = Earning::selectRaw('sum(amount) as amount, DATE_FORMAT(DATE(created_at),"%M %Y") as date')->where('entity_type', 'App\Models\Store')->whereIn('order_id', $orders->pluck('id'))->whereBetween('created_at', [$startDate, $endDate])->groupBy(DB::raw('DATE_FORMAT(DATE(created_at),"%M %Y")'))->get()->pluck('amount', 'date')->toArray();
    //     } else if ($interval == "year") {
    //         $earning_loop = Earning::selectRaw('sum(amount) as amount, DATE_FORMAT(DATE(created_at),"%Y") as date')->where('entity_type', 'App\Models\Store')->whereIn('order_id', $orders->pluck('id'))->whereBetween('created_at', [$startDate, $endDate])->groupBy(DB::raw('DATE_FORMAT(DATE(created_at),"%Y")'))->get()->pluck('amount', 'date')->toArray();
    //     }

    //     //loop through the start and end dates and create an array of dates
    //     $chart_data = [];
    //     while ($startDate <= $endDate) {
    //         if ($interval == "day") {
    //             $chart_data[$startDate->format('Y-m-d')] = $earning_loop[$startDate->format('Y-m-d')] ?? 0;
    //             $startDate->addDay();
    //         } else if ($interval == "week") {
    //             $chart_data[$startDate->format('W Y')] = $earning_loop[$startDate->format('W Y')] ?? 0;
    //             $startDate->addWeek();
    //         } else if ($interval == "month") {
    //             $chart_data[$startDate->format('M Y')] = $earning_loop[$startDate->format('F Y')] ?? 0;
    //             $startDate->addMonth();
    //         } else if ($interval == "year") {
    //             $chart_data[$startDate->format('Y')] = $earning_loop[$startDate->format('Y')] ?? 0;
    //             $startDate->addYear();
    //         }
    //     }
    //     return compact('total_orders', 'total_amount', 'earnings', 'chart_data', 'data_title');
    // }

    public function vendorOrderReport($id, $startDate, $endDate, $interval = "day")
    {
        $data_title = "Order Report for " . $startDate->format('d-m-Y') . " to " . $endDate->format('d-m-Y');
        $stores = \Helpers::get_stores_by_user($id);

        if ($interval == "day") {
            $c_data = Order::selectRaw('count(*) as total, DATE(created_at) as date')->groupBy('date');
        } else if ($interval == "week") {
            $c_data = Order::selectRaw('count(*) as total, DATE_FORMAT(DATE(created_at),"%u %Y") as date')->groupBy(DB::raw('DATE_FORMAT(DATE(created_at),"%u %Y")'));
        } else if ($interval == "month") {
            $c_data = Order::selectRaw('count(*) as total, DATE_FORMAT(DATE(created_at),"%M %Y") as date')->groupBy(DB::raw('DATE_FORMAT(DATE(created_at),"%M %Y")'));
        } else if ($interval == "year") {
            $c_data = Order::selectRaw('count(*) as total, DATE_FORMAT(DATE(created_at),"%Y") as date')->groupBy(DB::raw('DATE_FORMAT(DATE(created_at),"%Y")'));
        }

        if ($interval == "day") {
            $sc_data = ServiceOrder::selectRaw('count(*) as total, DATE(created_at) as date')->groupBy('date');
        } else if ($interval == "week") {
            $sc_data = ServiceOrder::selectRaw('count(*) as total, DATE_FORMAT(DATE(created_at),"%u %Y") as date')->groupBy(DB::raw('DATE_FORMAT(DATE(created_at),"%u %Y")'));
        } else if ($interval == "month") {
            $sc_data = ServiceOrder::selectRaw('count(*) as total, DATE_FORMAT(DATE(created_at),"%M %Y") as date')->groupBy(DB::raw('DATE_FORMAT(DATE(created_at),"%M %Y")'));
        } else if ($interval == "year") {
            $sc_data = ServiceOrder::selectRaw('count(*) as total, DATE_FORMAT(DATE(created_at),"%Y") as date')->groupBy(DB::raw('DATE_FORMAT(DATE(created_at),"%Y")'));
        }

        if ($endDate == $startDate) {
            $c_data = $c_data->whereDate('created_at', Carbon::now());
            $orders = Order::whereDate('created_at', Carbon::now());
            $total_amount = Order::whereDate('created_at', Carbon::now());
            $sc_data = $sc_data->whereDate('created_at', Carbon::now());
            $service_orders = ServiceOrder::whereDate('created_at', Carbon::now());
        } else {
            $c_data = $c_data->whereBetween('created_at', [$startDate, $endDate]);
            $orders = Order::whereBetween('created_at', [$startDate, $endDate]);
            $total_amount = Order::whereBetween('created_at', [$startDate, $endDate]);
            $sc_data = $sc_data->whereBetween('created_at', [$startDate, $endDate]);
            $service_orders = ServiceOrder::whereBetween('created_at', [$startDate, $endDate]);
        }

        $orders = $orders->whereIn('store_id', $stores->pluck('id'))->get();

        $c_data = $c_data->whereIn('store_id', $stores->pluck('id'))->get()->pluck('total', 'date')->toArray();
        $earnings = Earning::whereIn('order_id', $orders->pluck('id'))->where('entity_type', 'App\Models\Store')->sum('amount');
        $total_amount = $total_amount->whereIn('store_id', $stores->pluck('id'))->sum('total_price');
        
        $total_orders = count($orders);
        $total_tax = $orders->sum('tax');
        $admin_commission = $orders->sum('admin_commission');

        //loop through the start and end dates and create an array of dates
        $chart_data = [];
        while ($startDate <= $endDate) {
            if ($interval == "day") {
                $chart_data[$startDate->format('Y-m-d')] = $c_data[$startDate->format('Y-m-d')] ?? 0;
                $startDate->addDay();
            } else if ($interval == "week") {
                $chart_data[$startDate->format('W Y')] = $c_data[$startDate->format('W Y')] ?? 0;
                $startDate->addWeek();
            } else if ($interval == "month") {
                $chart_data[$startDate->format('M Y')] = $c_data[$startDate->format('F Y')] ?? 0;
                $startDate->addMonth();
            } else if ($interval == "year") {
                $chart_data[$startDate->format('Y')] = $c_data[$startDate->format('Y')] ?? 0;
                $startDate->addYear();
            }
        }
        $service_data_title = "Home Service Order Report for " . $startDate->format('d-m-Y') . " to " . $endDate->format('d-m-Y');
        // $stores = \Helpers::get_stores_by_user($id);
        $service_orders = $service_orders->where('vendor_id', $id)->get();
        $sc_data = $sc_data->get()->pluck('total', 'date')->toArray();
        $service_earnings = Earning::where('entity_type', 'App\Models\Vendor')->whereIn('order_id', $service_orders->pluck('id'));
        $service_earnings = $service_earnings->sum('amount');
        $service_total_amount = $service_orders->whereIn('vendor_id', $id)->sum('total_price');

        $service_total_orders = count($service_orders);
        $service_total_tax = $service_orders->sum('tax');

        //loop through the start and end dates and create an array of dates

        $service_chart_data = [];
        while ($startDate <= $endDate) {
            if ($interval == "day") {
                $service_chart_data[$startDate->format('Y-m-d')] = $sc_data[$startDate->format('Y-m-d')] ?? 0;
                $startDate->addDay();
            } else if ($interval == "week") {
                $service_chart_data[$startDate->format('W Y')] = $sc_data[$startDate->format('W Y')] ?? 0;
                $startDate->addWeek();
            } else if ($interval == "month") {
                $service_chart_data[$startDate->format('M Y')] = $sc_data[$startDate->format('F Y')] ?? 0;
                $startDate->addMonth();
            } else if ($interval == "year") {
                $service_chart_data[$startDate->format('Y')] = $sc_data[$startDate->format('Y')] ?? 0;
                $startDate->addYear();
            }
        }

        return compact('service_total_amount', 'service_earnings', 'service_chart_data', 'service_total_orders', 'service_data_title', 'service_orders', 'total_orders', 'total_amount', 'total_tax', 'orders', 'earnings', 'chart_data', 'data_title','admin_commission');
    }

//     public function vendorReport($id, $startDate, $endDate)
//     {
//         $data_title = "Driver Report for " . $startDate->format('d-m-Y') . " to " . $endDate->format('d-m-Y');
//         $stores = \Helpers::get_stores_by_user($id);
//         $orders = Order::with(['store' => function ($query) {
//             $query->select('id', 'name');
//         }])->whereIn('store_id', $stores->pluck('id'))->whereBetween('created_at', [$startDate, $endDate])->get();
//         $total_orders = count($orders);
//         $c_data = Order::selectRaw('count(*) as total, DATE(created_at) as date')->whereIn('store_id', $stores->pluck('id'))->whereBetween('created_at', [$startDate, $endDate])->groupBy('date')->get()->pluck('total', 'date')->toArray();
//         $total_amount = Order::whereIn('store_id', $stores->pluck('id'))->whereBetween('created_at', [$startDate, $endDate])->sum('total_price');
//         $earnings = Earning::where('entity_type', 'App\Models\Store')->whereIn('order_id', $orders->pluck('id'))->whereBetween('created_at', [$startDate, $endDate])->get();
//         //loop through the start and end dates and create an array of dates
//         $chart_data = [];
//         while ($startDate <= $endDate) {
//             $chart_data[$startDate->format('Y-m-d')] = $c_data[$startDate->format('Y-m-d')] ?? 0;
//             $startDate->addDay();
//         }

// //   Home service vendor report

//         return compact('total_orders', 'total_amount', 'earnings', 'chart_data', 'data_title');
//     }

    public function driverReport($id, $startDate, $endDate)
    {

        $data_title = "Driver Report for " . $startDate->format('d-m-Y') . " to " . $endDate->format('d-m-Y');
        if ($startDate == $endDate)
         {

             $orders = Order::where('driver_id', $id)->whereDate('created_at', Carbon::now())->get();
             $total_amount = Order::where('driver_id', $id)->whereDate('created_at', Carbon::now())->sum('total_price');
            $c_data = Order::selectRaw('count(*) as total, DATE(created_at) as date')->where('driver_id', $id)->whereDate('created_at', Carbon::now())->groupBy('date')->get()->pluck('total', 'date')->toArray();
            $earnings = Earning::where('entity_type', 'App\Models\Driver')->whereIn('order_id', $orders->pluck('id'))->whereDate('created_at', Carbon::now())->sum('amount');


            $service_orders = ServiceOrder::where('driver_id', $id)->whereDate('created_at', Carbon::now())->get();

            $service_total_orders = count($service_orders);
            $service_total_amount = ServiceOrder::where('driver_id', $id)->whereDate('created_at', Carbon::now())->sum('total_price');
            $sc_data = ServiceOrder::selectRaw('count(*) as total, DATE(created_at) as date')->where('driver_id', $id)->whereDate('created_at', Carbon::now())->groupBy('date')->get()->pluck('total', 'date')->toArray();
            $service_earnings = Earning::where('entity_type', 'App\Models\Driver')->whereIn('order_id', $service_orders->pluck('id'))->whereDate('created_at', Carbon::now())->sum('amount');

        } else {
            $orders = Order::where('driver_id', $id)->whereBetween('created_at', [$startDate, $endDate])->get();

            $total_amount = Order::where('driver_id', $id)->whereBetween('created_at', [$startDate, $endDate])->sum('total_price');
            $c_data = Order::selectRaw('count(*) as total, DATE(created_at) as date')->where('driver_id', $id)->whereBetween('created_at', [$startDate, $endDate])->groupBy('date')->get()->pluck('total', 'date')->toArray();
            $earnings = Earning::where('entity_type', 'App\Models\Driver')->whereIn('order_id', $orders->pluck('id'))->whereBetween('created_at', [Carbon::parse($startDate), Carbon::parse($endDate)])->sum('amount');

            $service_orders = ServiceOrder::where('driver_id', $id)->whereBetween('created_at', [$startDate, $endDate])->get();

            $service_total_orders = count($service_orders);
            $service_total_amount = ServiceOrder::where('driver_id', $id)->whereBetween('created_at', [$startDate, $endDate])->sum('total_price');
            $sc_data = ServiceOrder::selectRaw('count(*) as total, DATE(created_at) as date')->where('driver_id', $id)->whereBetween('created_at', [$startDate, $endDate])->groupBy('date')->get()->pluck('total', 'date')->toArray();
            $service_earnings = Earning::where('entity_type', 'App\Models\Driver')->whereIn('order_id', $service_orders->pluck('id'))->whereBetween('created_at', [Carbon::parse($startDate), Carbon::parse($endDate)])->sum('amount');

        }

        $total_orders = count($orders);
        //loop through the start and end dates and create an array of dates
        $chart_data = [];
        $service_chart_data = [];

        while ($startDate <= $endDate) {
            $chart_data[$startDate->format('Y-m-d')] = $c_data[$startDate->format('Y-m-d')] ?? 0;
            $service_chart_data[$startDate->format('Y-m-d')] = $sc_data[$startDate->format('Y-m-d')] ?? 0;
            $startDate->addDay();
        }
        //Home service driver report
        $service_data_title = "Home Service Driver Report for " . $startDate->format('d-m-Y') . " to " . $endDate->format('d-m-Y');
        //loop through the start and end dates and create an array of dates
        return compact('service_total_orders', 'service_total_amount', 'service_earnings', 'service_chart_data', 'service_data_title', 'total_orders', 'total_amount', 'earnings', 'chart_data', 'data_title');
    }

}
