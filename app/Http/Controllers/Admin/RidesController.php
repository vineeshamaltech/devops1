<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\RidesDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateRidesRequest;
use App\Http\Requests\UpdateRidesRequest;
use App\Repositories\RidesRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class RidesController extends AppBaseController
{
    /** @var  RidesRepository */
    private $ridesRepository;

    public function __construct(RidesRepository $ridesRepo)
    {
        $this->ridesRepository = $ridesRepo;
    }

    /**
     * Display a listing of the Ride.
     *
     * @param RidesDataTable $ridesDataTable
     * @return Response
     */
    public function index(RidesDataTable $ridesDataTable)
    {
        \Log::info('This is in rides controll  request');
        abort_if(\Gate::denies('rides.index'), 403, '403 Forbidden');
        return $ridesDataTable->render('admin.rides.index');
    }

    /**
     * Show the form for creating a new Ride.
     *
     * @return Response
     */
    public function create()
    {
        abort_if(\Gate::denies('rides.create'), 403, '403 Forbidden');
        return view('admin.rides.create');
    }

    /**
     * Store a newly created Ride in storage.
     *
     * @param CreateRidesRequest $request
     *
     * @return Response
     */
    public function store(CreateRidesRequest $request)
    {
        abort_if(\Gate::denies('rides.create'), 403, '403 Forbidden');
        $input = $request->all();

        $rides = $this->ridesRepository->create($input);

        Flash::success(__('lang.messages.saved', ['model' => __('models/rides.singular')]));

        return redirect(route('admin.rides.index'));
    }

    /**
     * Display the specified Ride.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        abort_if(\Gate::denies('rides.view'), 403, '403 Forbidden');
        $rides = $this->ridesRepository->find($id);

        if (empty($rides)) {
            Flash::error(__('lang.messages.not_found', ['model' => __('models/rides.singular')]));

            return redirect(route('admin.rides.index'));
        }

        return view('admin.rides.show')->with('rides', $rides);
    }

    /**
     * Show the form for editing the specified Ride.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        abort_if(\Gate::denies('rides.edit'), 403, '403 Forbidden');
        $rides = $this->ridesRepository->find($id);

        if (empty($rides)) {
            Flash::error(__('lang.messages.not_found', ['model' => __('models/rides.singular')]));

            return redirect(route('admin.rides.index'));
        }

        return view('admin.rides.edit')->with('rides', $rides);
    }

    /**
     * Update the specified Ride in storage.
     *
     * @param  int              $id
     * @param UpdateRidesRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateRidesRequest $request)
    {
        abort_if(\Gate::denies('rides.edit'), 403, '403 Forbidden');
        $rides = $this->ridesRepository->find($id);

        if (empty($rides)) {
            Flash::error(__('lang.messages.not_found', ['model' => __('models/rides.singular')]));

            return redirect(route('admin.rides.index'));
        }

        $rides = $this->ridesRepository->update($request->all(), $id);

        Flash::success(__('lang.messages.updated', ['model' => __('models/rides.singular')]));

        return redirect(route('admin.rides.index'));
    }

    /**
     * Remove the specified Ride from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        abort_if(\Gate::denies('rides.delete'), 403, '403 Forbidden');
        $rides = $this->ridesRepository->find($id);

        if (empty($rides)) {
            Flash::error(__('lang.messages.not_found', ['model' => __('models/rides.singular')]));

            return redirect(route('admin.rides.index'));
        }

        $this->ridesRepository->delete($id);

        Flash::success(__('lang.messages.deleted', ['model' => __('models/rides.singular')]));

        return redirect(route('admin.rides.index'));
    }
}
