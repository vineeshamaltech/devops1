<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\StoreGalleryDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateStoreGalleryRequest;
use App\Http\Requests\UpdateStoreGalleryRequest;
use App\Repositories\StoreGalleryRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class StoreGalleryController extends AppBaseController
{
    /** @var  StoreGalleryRepository */
    private $storeGalleryRepository;

    public function __construct(StoreGalleryRepository $storeGalleryRepo)
    {
        $this->storeGalleryRepository = $storeGalleryRepo;
    }

    /**
     * Display a listing of the StoreGallery.
     *
     * @param StoreGalleryDataTable $storeGalleryDataTable
     * @return Response
     */
    public function index(StoreGalleryDataTable $storeGalleryDataTable)
    {
        \Log::info('This is in store gallery controll  request');
        abort_if(\Gate::denies('store_galleries.index'), 403, '403 Forbidden');
        return $storeGalleryDataTable->render('admin.store_galleries.index');
    }

    /**
     * Show the form for creating a new StoreGallery.
     *
     * @return Response
     */
    public function create()
    {
        abort_if(\Gate::denies('store_galleries.create'), 403, '403 Forbidden');
        return view('admin.store_galleries.create');
    }

    /**
     * Store a newly created StoreGallery in storage.
     *
     * @param CreateStoreGalleryRequest $request
     *
     * @return Response
     */
    public function store(CreateStoreGalleryRequest $request)
    {
        abort_if(\Gate::denies('store_galleries.create'), 403, '403 Forbidden');
        $input = $request->all();

        $storeGallery = $this->storeGalleryRepository->create($input);

        Flash::success(__('lang.messages.saved', ['model' => __('models/storeGalleries.singular')]));

        return redirect(route('admin.storeGalleries.index'));
    }

    /**
     * Display the specified StoreGallery.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        abort_if(\Gate::denies('store_galleries.view'), 403, '403 Forbidden');
        $storeGallery = $this->storeGalleryRepository->find($id);

        if (empty($storeGallery)) {
            Flash::error(__('lang.messages.not_found', ['model' => __('models/storeGalleries.singular')]));

            return redirect(route('admin.storeGalleries.index'));
        }

        return view('admin.store_galleries.show')->with('storeGallery', $storeGallery);
    }

    /**
     * Show the form for editing the specified StoreGallery.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        abort_if(\Gate::denies('store_galleries.edit'), 403, '403 Forbidden');
        $storeGallery = $this->storeGalleryRepository->find($id);

        if (empty($storeGallery)) {
            Flash::error(__('lang.messages.not_found', ['model' => __('models/storeGalleries.singular')]));

            return redirect(route('admin.storeGalleries.index'));
        }

        return view('admin.store_galleries.edit')->with('storeGallery', $storeGallery);
    }

    /**
     * Update the specified StoreGallery in storage.
     *
     * @param  int              $id
     * @param UpdateStoreGalleryRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateStoreGalleryRequest $request)
    {
        abort_if(\Gate::denies('store_galleries.edit'), 403, '403 Forbidden');
        $storeGallery = $this->storeGalleryRepository->find($id);

        if (empty($storeGallery)) {
            Flash::error(__('lang.messages.not_found', ['model' => __('models/storeGalleries.singular')]));

            return redirect(route('admin.storeGalleries.index'));
        }

        $storeGallery = $this->storeGalleryRepository->update($request->all(), $id);

        Flash::success(__('lang.messages.updated', ['model' => __('models/storeGalleries.singular')]));

        return redirect(route('admin.storeGalleries.index'));
    }

    /**
     * Remove the specified StoreGallery from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        abort_if(\Gate::denies('store_galleries.delete'), 403, '403 Forbidden');
        $storeGallery = $this->storeGalleryRepository->find($id);

        if (empty($storeGallery)) {
            Flash::error(__('lang.messages.not_found', ['model' => __('models/storeGalleries.singular')]));

            return redirect(route('admin.storeGalleries.index'));
        }

        $this->storeGalleryRepository->delete($id);

        Flash::success(__('lang.messages.deleted', ['model' => __('models/storeGalleries.singular')]));

        return redirect(route('admin.storeGalleries.index'));
    }
}
