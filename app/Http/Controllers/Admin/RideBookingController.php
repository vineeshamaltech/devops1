<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\RideBookingDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateRideBookingRequest;
use App\Http\Requests\UpdateRideBookingRequest;
use App\Repositories\RideBookingRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class RideBookingController extends AppBaseController
{
    /** @var  RideBookingRepository */
    private $rideBookingRepository;

    public function __construct(RideBookingRepository $rideBookingRepo)
    {
        $this->rideBookingRepository = $rideBookingRepo;
    }

    /**
     * Display a listing of the RideBooking.
     *
     * @param RideBookingDataTable $rideBookingDataTable
     * @return Response
     */
    public function index(RideBookingDataTable $rideBookingDataTable)
    {

        \Log::info('This is in ride booking controll  request');
        abort_if(\Gate::denies('rideBookings.index'), 403, '403 Forbidden');
        return $rideBookingDataTable->render('admin.ride_bookings.index');
    }

    /**
     * Show the form for creating a new RideBooking.
     *
     * @return Response
     */
    public function create()
    {
        abort_if(\Gate::denies('rideBookings.create'), 403, '403 Forbidden');
        return view('admin.ride_bookings.create');
    }

    /**
     * Store a newly created RideBooking in storage.
     *
     * @param CreateRideBookingRequest $request
     *
     * @return Response
     */
    public function store(CreateRideBookingRequest $request)
    {
        abort_if(\Gate::denies('rideBookings.create'), 403, '403 Forbidden');
        $input = $request->all();

        $rideBooking = $this->rideBookingRepository->create($input);

        Flash::success(__('lang.messages.saved', ['model' => __('models/rideBookings.singular')]));

        return redirect(route('admin.rideBookings.index'));
    }

    /**
     * Display the specified RideBooking.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        abort_if(\Gate::denies('rideBookings.view'), 403, '403 Forbidden');
        $rideBooking = $this->rideBookingRepository->find($id);

        if (empty($rideBooking)) {
            Flash::error(__('lang.messages.not_found', ['model' => __('models/rideBookings.singular')]));

            return redirect(route('admin.rideBookings.index'));
        }

        return view('admin.ride_bookings.show')->with('rideBooking', $rideBooking);
    }

    /**
     * Show the form for editing the specified RideBooking.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        abort_if(\Gate::denies('rideBookings.edit'), 403, '403 Forbidden');
        $rideBooking = $this->rideBookingRepository->find($id);

        if (empty($rideBooking)) {
            Flash::error(__('lang.messages.not_found', ['model' => __('models/rideBookings.singular')]));

            return redirect(route('admin.rideBookings.index'));
        }

        return view('admin.ride_bookings.edit')->with('rideBooking', $rideBooking);
    }

    /**
     * Update the specified RideBooking in storage.
     *
     * @param  int              $id
     * @param UpdateRideBookingRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateRideBookingRequest $request)
    {
        abort_if(\Gate::denies('rideBookings.edit'), 403, '403 Forbidden');
        $rideBooking = $this->rideBookingRepository->find($id);

        if (empty($rideBooking)) {
            Flash::error(__('lang.messages.not_found', ['model' => __('models/rideBookings.singular')]));

            return redirect(route('admin.rideBookings.index'));
        }

        $rideBooking = $this->rideBookingRepository->update($request->all(), $id);

        Flash::success(__('lang.messages.updated', ['model' => __('models/rideBookings.singular')]));

        return redirect(route('admin.rideBookings.index'));
    }

    /**
     * Remove the specified RideBooking from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        abort_if(\Gate::denies('rideBookings.delete'), 403, '403 Forbidden');
        $rideBooking = $this->rideBookingRepository->find($id);

        if (empty($rideBooking)) {
            Flash::error(__('lang.messages.not_found', ['model' => __('models/rideBookings.singular')]));

            return redirect(route('admin.rideBookings.index'));
        }

        $this->rideBookingRepository->delete($id);

        Flash::success(__('lang.messages.deleted', ['model' => __('models/rideBookings.singular')]));

        return redirect(route('admin.rideBookings.index'));
    }
}
