<?php

namespace App\Http\Controllers\Admin;

use App\Models\User;
use App\Models\Admin;
use App\Models\Driver;
use App\Http\Requests;
use App\Http\Requests\CreateBannerRequest;
use App\Http\Requests\UpdateBannerRequest;
use Illuminate\Support\Facades\DB;
use Flash;
use App\Models\Store;
use App\Repositories\BannerRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Requests as GlobalRequests;
use Response;
use App\Notifications\PushNotification;
use Illuminate\Notifications\Notification as NotificationsNotification;
use Notification;

class NotificationController extends AppBaseController
{
    /** @var  BannerRepository */

    /**
     * Display a listing of the Driver.
     *
     * @param DriverDataTable $driverDataTable
     * @return Response
     */


    public function index()
    {
        abort_if(\Gate::denies('notification.index'), 403, '403 Forbidden');
        return view('admin.notification.notification');
    }

    public function store(Request $request)
    {
      
        $input = $request->all();
        $user = [];
        $tmpimage = '';
        $model = "";
        switch ($input['user_type']) {
            case 'user':
                $model = 'App\Models\User';
                break;
            case 'vendor':
                $model = 'App\Models\Admin';
                break;
            case 'driver':
                $model = 'App\Models\Driver';
                break;
            case 'all':
                $model = 'all';
                break;
        }
        if ($request->hasFile('image')) {
            $tmpimg = \Helpers::uploadImage($request->file('image'), 'notification_images');
            $tmpimage = url($tmpimg);
        }
        $fields = array(
            'title' => $request->title,
            'description' => $request->description,
            'image' => $tmpimage
        );
        if ($model == ""){
            Flash::error('Invalid User Type');
            return redirect()->back()->withInput();
        }
        
        if($model == "all"){
            
            $user = User::select('fcm_token')->get();
            $admin = Admin::select('fcm_token')->get();
            $driver = Driver::select('fcm_token')->get();
            $this->push_curl($user, $request, $tmpimage);
            $this->push_curl($admin, $request, $tmpimage);
            $this->push_curl($driver, $request, $tmpimage);
            // Notification::send($user, new PushNotification($fields));
            // Notification::send($admin, new PushNotification($fields));
            // $result = Notification::send($driver, new PushNotification($fields));
        }else{
           
            if ($request->user[0] == "all") {
                $user = $model::get();
                 $this->push_curl($user, $request, $tmpimage);
            } else {
              
                if($input['user_type'] == 'driver')
                {
                      $user =$model::find($input['user']);
                        $this->push_curl($user, $request, $tmpimage); 
                    //   Notification::send($user,new PushNotification($fields));
                }else{
                    //   dd($input['user']);
                      foreach ($input['user'] as $key => $value) {
                        $user[$key] = $model::where('id', $input['user'])->first();
                      }
                
                 $this->push_curl($user, $request, $tmpimage); 
                }
                
            }
        //   dd($user);
       
           
            // $result = Notification::send($user, new PushNotification($fields));
         
        }
        // return $result;
        Flash::success(__('Notification send successfully', ['model' => __('models/banners.singular')]));
        return view('admin.notification.notification');
    }

    public function push_curl($alltoken, $request, $image) {
       
         $title = $request->title;
         $body = $request->description;
         $imageurl = $image;
        $tokens = array();
        foreach($alltoken as $token){
            array_push($tokens,$token['fcm_token']);
        }
          
          $fcmkey =  DB::table('settings')->where('key_name','fcm_key')->first();
           
        $result = array();
        $chunks = array_chunk($tokens,999);
       
        foreach($chunks as $chunk){
               $url = 'https://fcm.googleapis.com/fcm/send';    
            $dataArr = array('click_action' => 'FLUTTER_NOTIFICATION_CLICK');
    
            $notification = array('title' =>$title, 'body' => $body,  'sound' => 'default', 'badge' => '1', 'image'=>$imageurl);
    
            $arrayToSend = array('registration_ids' => $chunk, 'notification' => $notification, 'data' => $dataArr, 'priority'=>'high' );
    
            $tempArr = $arrayToSend;
    
            $fields = json_encode($arrayToSend);
            
            $headers = array (
                'Authorization: key=' . $fcmkey->key_value,
                'Content-Type: application/json'
    
            );
    
            //Register notification in database
            $ch = curl_init ();
    
            curl_setopt ( $ch, CURLOPT_URL, $url );
    
            curl_setopt ( $ch, CURLOPT_POST, true );
    
            curl_setopt ( $ch, CURLOPT_HTTPHEADER, $headers );
    
            curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );
    
            curl_setopt ( $ch, CURLOPT_POSTFIELDS, $fields );
            $result[] = curl_exec ( $ch );
   
            //   dd($result);
            curl_close ( $ch );
           return 1;
        }
    }

    public function test(Request $request){
        $customer = User::where('id', 43)->first();
        $vendor = Admin::where('id', 190)->first();
        $driver = Driver::where('id',149)->first();
        Notification::send($customer, new PushNotification(['title' => 'test', 'description' => 'test']));
        Notification::send($vendor, new PushNotification(['title' => 'test', 'description' => 'test']));
        //Notification::send($driver, new PushNotification(['title' => 'test', 'description' => 'test']));
    }

}
