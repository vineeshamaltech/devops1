<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\FAQDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateFAQRequest;
use App\Http\Requests\UpdateFAQRequest;
use App\Repositories\FAQRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class FAQController extends AppBaseController
{
    /** @var  FAQRepository */
    private $fAQRepository;

    public function __construct(FAQRepository $fAQRepo)
    {
        $this->fAQRepository = $fAQRepo;
    }

    /**
     * Display a listing of the FAQ.
     *
     * @param FAQDataTable $fAQDataTable
     * @return Response
     */
    public function index(FAQDataTable $fAQDataTable)
    {
        abort_if(\Gate::denies('faqs.index'), 403, '403 Forbidden');
        return $fAQDataTable->render('admin.f_a_q_s.index');
    }

    /**
     * Show the form for creating a new FAQ.
     *
     * @return Response
     */
    public function create()
    {
        abort_if(\Gate::denies('faqs.create'), 403, '403 Forbidden');
        return view('admin.f_a_q_s.create');
    }

    /**
     * Store a newly created FAQ in storage.
     *
     * @param CreateFAQRequest $request
     *
     * @return Response
     */
    public function store(CreateFAQRequest $request)
    {
        abort_if(\Gate::denies('faqs.create'), 403, '403 Forbidden');
        $input = $request->all();

        $fAQ = $this->fAQRepository->create($input);

        Flash::success(__('lang.messages.saved', ['model' => __('models/fAQS.singular')]));

        return redirect(route('admin.faqs.index'));
    }

    /**
     * Display the specified FAQ.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        abort_if(\Gate::denies('faqs.view'), 403, '403 Forbidden');
        $fAQ = $this->fAQRepository->find($id);

        if (empty($fAQ)) {
            Flash::error(__('lang.messages.not_found', ['model' => __('models/fAQS.singular')]));

            return redirect(route('admin.faqs.index'));
        }

        return view('admin.f_a_q_s.show')->with('fAQ', $fAQ);
    }

    /**
     * Show the form for editing the specified FAQ.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        abort_if(\Gate::denies('faqs.edit'), 403, '403 Forbidden');
        $fAQ = $this->fAQRepository->find($id);

        if (empty($fAQ)) {
            Flash::error(__('lang.messages.not_found', ['model' => __('models/fAQS.singular')]));

            return redirect(route('admin.faqs.index'));
        }

        return view('admin.f_a_q_s.edit')->with('fAQ', $fAQ);
    }

    /**
     * Update the specified FAQ in storage.
     *
     * @param  int              $id
     * @param UpdateFAQRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateFAQRequest $request)
    {
        abort_if(\Gate::denies('faqs.edit'), 403, '403 Forbidden');
        $fAQ = $this->fAQRepository->find($id);

        if (empty($fAQ)) {
            Flash::error(__('lang.messages.not_found', ['model' => __('models/fAQS.singular')]));

            return redirect(route('admin.faqs.index'));
        }

        $fAQ = $this->fAQRepository->update($request->all(), $id);

        Flash::success(__('lang.messages.updated', ['model' => __('models/fAQS.singular')]));

        return redirect(route('admin.faqs.index'));
    }

    /**
     * Remove the specified FAQ from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        abort_if(\Gate::denies('faqs.delete'), 403, '403 Forbidden');
        $fAQ = $this->fAQRepository->find($id);

        if (empty($fAQ)) {
            Flash::error(__('lang.messages.not_found', ['model' => __('models/fAQS.singular')]));

            return redirect(route('admin.faqs.index'));
        }

        $this->fAQRepository->delete($id);

        Flash::success(__('lang.messages.deleted', ['model' => __('models/fAQS.singular')]));

        return redirect(route('admin.faqs.index'));
    }
}
