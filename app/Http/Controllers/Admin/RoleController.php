<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\RoleDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateRoleRequest;
use App\Http\Requests\UpdateRoleRequest;
use App\Repositories\RoleRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Response;

class RoleController extends AppBaseController
{
    /** @var  RoleRepository */
    private $roleRepository;

    public function __construct(RoleRepository $roleRepo)
    {
        $this->roleRepository = $roleRepo;
    }

    /**
     * Display a listing of the Role.
     *
     * @param RoleDataTable $roleDataTable
     * @return Response
     */
    public function index(RoleDataTable $roleDataTable)
    {
        \Log::info('This is in role controll  request');
        abort_if(\Gate::denies('roles.index'), 403, '403 Forbidden');
        return $roleDataTable->render('admin.roles.index');
    }

    /**
     * Show the form for creating a new Role.
     *
     * @return Response
     */
    public function create()
    {
        abort_if(\Gate::denies('roles.create'), 403, '403 Forbidden');
        return view('admin.roles.create');
    }

    /**
     * Store a newly created Role in storage.
     *
     * @param CreateRoleRequest $request
     *
     * @return Response
     */
    public function store(CreateRoleRequest $request)
    {
        $input = $request->all();
        Role::create($input);
        Flash::success(__('lang.messages.saved', ['model' => __('models/roles.singular')]));

        return redirect(route('admin.roles.index'));
    }

    /**
     * Display the specified Role.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        abort_if(\Gate::denies('roles.view'), 403, '403 Forbidden');
        $role = Role::findById($id);
        if (empty($role)) {
            Flash::error(__('lang.messages.not_found', ['model' => __('models/roles.singular')]));

            return redirect(route('admin.roles.index'));
        }
        return view('admin.roles.show')->with('role', $role);
    }

    /**
     * Show the form for editing the specified Role.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id,Request $request)
    {
        // Permission::create(['name' => 'payments.view']);
        abort_if(\Gate::denies('roles.edit'), 403, '403 Forbidden');
        $role = Role::findById($id,$request->guard);
        if (empty($role)) {
            Flash::error(__('lang.messages.not_found', ['model' => __('models/roles.singular')]));
            return redirect(route('admin.roles.index'));
        }
        $permissions = Permission::where('guard_name',$role->guard_name)->orderBy('name')->get();
        // dd($permissions->toArray());
        $unique_permissions = [];

        foreach($permissions as $permission){
            $per_group = substr($permission->name,0,strpos($permission->name,'.'));
            if($this->checkFeatureDisabled($per_group)){
                continue;
            }
            $unique_permissions[$per_group][] = [
                'name' => $permission->name,
                'granted' => $role->hasPermissionTo($permission->name)
            ];
        }
        return view('admin.roles.edit',['permissions'=>$unique_permissions,'role'=>$role]);
    }

    public function checkFeatureDisabled($permission) : bool{
        if (in_array($permission,config('constants.carpool_permissions_list',[]))){
            return !config('constants.features.car_pool_booking',true);
        }
        if (in_array($permission,config('constants.purchasing_service_permissions_list',[]))){
            return !config('constants.features.purchasing_service',true);
        }
        if (in_array($permission,config('constants.home_service_permissions_list',[]))){
            return !config('constants.features.home_service_booking',true);
        }
        return false;
    }

    /**
     * Update the specified Role in storage.
     *
     * @param  int              $id
     * @param UpdateRoleRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateRoleRequest $request)
    {
        $role = Role::findById($id);
        if (empty($role)) {
            Flash::error(__('lang.messages.not_found', ['model' => __('models/roles.singular')]));
            return redirect(route('admin.roles.index'));
        }
        $role->update($request->all());
        Flash::success(__('lang.messages.updated', ['model' => __('models/roles.singular')]));
        return redirect(route('admin.roles.index'));
    }

    /**
     * Remove the specified Role from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        abort_if(\Gate::denies('roles.delete'), 403, '403 Forbidden');
        $role = Role::findById($id);
        if (empty($role)) {
            Flash::error(__('lang.messages.not_found', ['model' => __('models/roles.singular')]));
            return redirect(route('admin.roles.index'));
        }

        $role->delete($id);

        Flash::success(__('lang.messages.deleted', ['model' => __('models/roles.singular')]));

        return redirect(route('admin.roles.index'));
    }
}
