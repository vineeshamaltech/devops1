<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\AdminDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateAdminRequest;
use App\Http\Requests\UpdateAdminRequest;
use App\Models\Admin;
use App\Models\Store;
use App\Repositories\AdminRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use App\Models\Zone;
use Response;
use Spatie\Permission\Models\Role;

class AdminController extends AppBaseController
{
    /** @var  AdminRepository */
    private $adminRepository;

    public function __construct(AdminRepository $adminRepo)
    {
        $this->adminRepository = $adminRepo;
    }

    /**
     * Display a listing of the Admin.
     *
     * @param AdminDataTable $adminDataTable
     * @return Response
     */
    public function index(AdminDataTable $adminDataTable)
    {
        \Log::info('This is in Admin controller');
        abort_if(\Gate::denies('admins.index'), 403, '403 Forbidden');
        return $adminDataTable->render('admin.admins.index');
    }

    /**
     * Show the form for creating a new Admin.
     *
     * @return Response
     */
    public function create()
    {
        abort_if(\Gate::denies('admins.create'), 403, '403 Forbidden');
        $zones = Zone::all();
        return view('admin.admins.create',compact('zones'));
    }

    /**
     * Store a newly created Admin in storage.
     *
     * @param CreateAdminRequest $request
     *
     * @return Response
     */
    public function store(CreateAdminRequest $request)
    {
      //  dd('error in create admin');
        \Log::info('This is in Admins controller store create');
        abort_if(\Gate::denies('admins.create'), 403, '403 Forbidden');
        $input = $request->all();
        //dd($input);
        if ($request->has('password')) {
            $input['password'] = \Hash::make($input['password']);
        }else{
            Flash::error('Password is required');
            return redirect()->back()->withInput();
        }
        if ($request->has('zones')) {
            $input['zones'] = implode(",",$request->zones);
        }
        $country_code = substr($request->mobile, 0, 3);
        $mobile = substr($request->mobile, 3);
        if (Admin::where('mobile', $mobile)->where('country_code', $country_code)->first()) {
            Flash::error('Mobile Number already exists');
            return redirect()->back()->withInput();
        }

        if (Admin::where('email', $input['email'])->first()) {
            Flash::error('Email already exists');
            return redirect()->back()->withInput();
        }

        $input['mobile'] = $mobile;
        $input['country_code'] = $country_code;
        $input['type'] = $request->type[0];
        $admin = $this->adminRepository->create($input);
          //  dd($admin);  
        foreach ($request->type as $key => $value) {
            $role = Role::findOrCreate($value);
            $admin->assignRole($role);
        }
      
        Flash::success(__('lang.messages.saved', ['model' => __('models/admins.singular')]));
        return redirect(route('admin.admins.index'));
    }

    /**
     * Display the specified Admin.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        abort_if(\Gate::denies('admins.view'), 403, '403 Forbidden');
        $admin = $this->adminRepository->find($id);
        // dd($admin->kyc->toArray());
        if (empty($admin)) {
            Flash::error(__('lang.messages.not_found', ['model' => __('models/admins.singular')]));
            return redirect(route('admin.admins.index'));
        }
        return view('admin.admins.show')->with('admin', $admin);
    }


    /**
     * Show the form for editing the specified Admin.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        abort_if(\Gate::denies('admins.edit'), 403, '403 Forbidden');
        $admin = $this->adminRepository->find($id);
        $userZones=\Helpers::get_user_zones($id);
        $zone_se = explode(",",$admin->zones);
        $zones = Zone::where('active',1)->get();
        foreach ($zones as $zone){
            if (in_array($zone->id,$zone_se)){
                $zone->selected = true;
            }else{
                $zone->selected = false;
            }
        }
        if (empty($admin)) {
            Flash::error(__('lang.messages.not_found', ['model' => __('models/admins.singular')]));

            return redirect(route('admin.admins.index'));
        }

        return view('admin.admins.edit')->with('admin', $admin)->with('zone_se',$zone_se)->with('zones',$zones);
    }

    /**
     * Update the specified Admin in storage.
     *
     * @param  int              $id
     * @param UpdateAdminRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateAdminRequest $request)
    {
        abort_if(\Gate::denies('admins.edit'), 403, '403 Forbidden');
        $admin = $this->adminRepository->find($id);

        $input = $request->all();
        if (empty($admin)) {
            Flash::error(__('lang.messages.not_found', ['model' => __('models/admins.singular')]));
            return redirect(route('admin.admins.index'));
        }

        $country_code = substr($request->mobile, 0, 3);
        $mobile = substr($request->mobile, 3);
        if (Admin::where('mobile', $mobile)->where('country_code', $country_code)->where('id','!=',$id)->exists()) {
            Flash::error('Mobile Number already exists');
            return redirect()->back()->withInput();
        }

        if (!empty($input['email']) && Admin::where('email', $input['email'])->where('id','!=',$id)->exists()) {
            Flash::error('Email already exists');
            return redirect()->back()->withInput();
        }

        if(!empty($input['password'])) {
            $input['password'] = \Hash::make($input['password']);
        }else {
            unset($input['password']);
        }
        $input['country_code'] = $country_code;
        $input['mobile'] = $mobile;
        if(@$request->type)
        {
             $input['type'] = $request->type[0];
      
        foreach ($request->type as $key => $value) {
            $admin->removeRole($admin->type);
            $admin->assignRole($value);
        }

        }
          if ($request->has('zones')) {
            $input['zones'] = implode(",",$request->zones);
        }
       
        $admin = $this->adminRepository->update($input, $id);
        
        // $admin->syncRoles($input['type']);
        Flash::success(__('lang.messages.updated', ['model' => __('models/admins.singular')]));

        return redirect(route('admin.admins.index'));
    }

    /**
     * Remove the specified Admin from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        abort_if(\Gate::denies('admins.delete'), 403, '403 Forbidden');
        $admin = $this->adminRepository->find($id);
        if (empty($admin)) {
            Flash::error(__('lang.messages.not_found', ['model' => __('models/admins.singular')]));
            return redirect(route('admin.admins.index'));
        }

        $storecount = Store::where('admin_id', $id)->count();
        if ($storecount> 0) {
            Flash::error('Admin has '.$storecount.' stores, can not delete. Please delete stores first');
            return redirect(route('admin.admins.index'));
        }else{
            $this->adminRepository->delete($id);
            Flash::success(__('lang.messages.deleted', ['model' => __('models/admins.singular')]));
            return redirect(route('admin.admins.index'));
        }

    }
}
