<?php

namespace App\Http\Controllers\Admin\HomeServiceBooking;

use App\DataTables\HomeServiceDataTable;
use App\Http\Controllers\AppBaseController;
use App\Http\Requests\CreateHomeServiceRequest;
use App\Http\Requests\UpdateHomeServiceRequest;
use App\Models\HomeServiceTimeslot;
use App\Repositories\HomeServiceRepository;
use Carbon\Carbon;
use Flash;
use Illuminate\Http\Request;
use Response;
use function __;
use function abort_if;
use function redirect;
use function view;

class HomeServiceController extends AppBaseController
{
    /** @var  HomeServiceRepository */
    private $homeServiceRepository;

    public function __construct(HomeServiceRepository $homeServiceRepo)
    {
        $this->homeServiceRepository = $homeServiceRepo;
    }

    /**
     * Display a listing of the HomeService.
     *
     * @param HomeServiceDataTable $homeServiceDataTable
     * @return Response
     */
    public function index(HomeServiceDataTable $homeServiceDataTable)
    {
        \Log::info('This is in Home service controller');
        abort_if(\Gate::denies('homeServices.index'), 403, '403 Forbidden');
        return $homeServiceDataTable->render('admin.home_services.index');
    }

    /**
     * Show the form for creating a new HomeService.
     *
     * @return Response
     */
    public function create()
    {
        abort_if(\Gate::denies('homeServices.create'), 403, '403 Forbidden');
        return view('admin.home_services.create');
    }

    /**
     * Store a newly created HomeService in storage.
     *
     * @param CreateHomeServiceRequest $request
     *
     * @return Response
     */
    public function store(CreateHomeServiceRequest $request)
    {  
        abort_if(\Gate::denies('homeServices.create'), 403, '403 Forbidden');
        $validated = $request->validate([
            'home_service_category_id' => 'required',
        ]);
       
        $input = $request->all();
       
        //  $input['payment_method'] = implode(',', $input['payment_methods']);
        if ($request->hasFile('image')) {
            $input['image'] = \Helpers::uploadImage($request->file('image'), 'home_service_categories');
        }
         if(@$request->discount_price == null)
            $input['discount_price'] = $request->original_price;
        $homeService = $this->homeServiceRepository->create($input);
    
        Flash::success(__('lang.messages.saved', ['model' => __('models/homeServices.singular')]));

        return redirect(route('admin.homeServices.index'));
    }

    /**
     * Display the specified HomeService.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        abort_if(\Gate::denies('homeServices.view'), 403, '403 Forbidden');
        $homeService = $this->homeServiceRepository->find($id);

        if (empty($homeService)) {
            Flash::error(__('lang.messages.not_found', ['model' => __('models/homeServices.singular')]));

            return redirect(route('admin.homeServices.index'));
        }
        return view('admin.home_services.show')->with('homeService', $homeService);
    }

    /**
     * Show the form for editing the specified HomeService.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        abort_if(\Gate::denies('homeServices.edit'), 403, '403 Forbidden');
        $homeService = $this->homeServiceRepository->find($id);
 
    
        if (empty($homeService)) {
            Flash::error(__('lang.messages.not_found', ['model' => __('models/homeServices.singular')]));
            return redirect(route('admin.homeServices.index'));
        }
        return view('admin.home_services.edit')->with('homeService', $homeService);
    }

    /**
     * Update the specified HomeService in storage.
     *
     * @param  int              $id
     * @param UpdateHomeServiceRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateHomeServiceRequest $request)
    {
        abort_if(\Gate::denies('homeServices.edit'), 403, '403 Forbidden');
        $homeService = $this->homeServiceRepository->find($id);

        if (empty($homeService)) {
            Flash::error(__('lang.messages.not_found', ['model' => __('models/homeServices.singular')]));
            return redirect(route('admin.homeServices.index'));
        }

        $input = $request->all();
      
        if ($request->hasFile('image')) {
            $input['image'] = \Helpers::uploadImage($request->file('image'), 'home_service_categories');
        }
           if(@$request->discount_price == null)
            $input['discount_price'] = $request->original_price;
        $homeService = $this->homeServiceRepository->update($input, $id);
        Flash::success(__('lang.messages.updated', ['model' => __('models/homeServices.singular')]));
        return redirect(route('admin.homeServices.index'));
    }

    /**
     * Remove the specified HomeService from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        abort_if(\Gate::denies('homeServices.delete'), 403, '403 Forbidden');
        $homeService = $this->homeServiceRepository->find($id);

        if (empty($homeService)) {
            Flash::error(__('lang.messages.not_found', ['model' => __('models/homeServices.singular')]));

            return redirect(route('admin.homeServices.index'));
        }

        $this->homeServiceRepository->delete($id);

        Flash::success(__('lang.messages.deleted', ['model' => __('models/homeServices.singular')]));

        return redirect(route('admin.homeServices.index'));
    }

    /**
     * Set multiple HomeService Timings.
     *
     * @param  int $id - HomeService Id
     *
     * @return Response
     */
    public function homeServiceTiming($id)
    {
        abort_if(\Gate::denies('homeServices.timings'), 403, '403 Forbidden');
        $homeService = HomeServiceTimeslot::where('home_service_id',$id)->get();
        $timing_data = [];
        $all_days = [
            'Monday'=>$timing_data,
            'Tuesday'=>$timing_data,
            'Wednesday'=>$timing_data,
            'Thursday'=>$timing_data,
            'Friday'=>$timing_data,
            'Saturday'=>$timing_data,
            'Sunday'=>$timing_data
        ];
        foreach($all_days as $key => $day){
            foreach($homeService as $time){
                if($key == $time->day){
                    $temp = [
                        "open"=>$time->open,    
                        "close"=>$time->close
                    ];
                    array_push($all_days[$key],$temp);
                }
            }
        }
        // dd($all_days);
        if (empty($homeService)) {
            Flash::error(__('lang.messages.not_found', ['model' => __('models/homeServices.singular')]));

            return redirect(route('admin.homeServices.index'));
        }

        // $this->homeServiceRepository->delete($id);
        // Flash::success(__('lang.messages.deleted', ['model' => __('models/homeServices.singular')]));
        return view('admin.home_services.timings')->with('id',$id)->with('all_days',$all_days);
    }

    /**
     * Set multiple HomeService Timings.
     *
     * @param  int $id - HomeService Id
     *
     * @return Response
     */
    public function setTiming(Request $request)
    {
        abort_if(\Gate::denies('homeServices.timings'), 403, '403 Forbidden');
        $input = $request->all();
        HomeServiceTimeslot::where('home_service_id',$request->id)->delete();
        $days = [
            'monday',
            'tuesday',
            'wednesday',
            'thursday',
            'friday',
            'saturday',
            'sunday',
        ];

        foreach($days as $day){
            $days_array = $input[$day.'_open'];
            for($i=0;$i<count($days_array);$i++){
                $input['home_service_id'] = $request->id;
                $input['day'] = ucfirst($day);
                $input['open'] = Carbon::parse($input[$day.'_open'][$i])->format('H:i:s');
                $input['close'] = Carbon::parse($input[$day.'_close'][$i])->format('H:i:s');
                HomeServiceTimeslot::create($input);
            }
        }
        Flash::success(__('lang.messages.updated', ['model' => __('models/homeServices.singular')]));
        return redirect(route('admin.homeServices.index'));
    }

    
}
