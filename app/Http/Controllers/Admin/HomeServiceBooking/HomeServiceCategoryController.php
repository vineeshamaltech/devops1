<?php

namespace App\Http\Controllers\Admin\HomeServiceBooking;

use App\DataTables\HomeServiceCategoryDataTable;
use App\Http\Controllers\AppBaseController;
use App\Http\Requests\CreateHomeServiceCategoryRequest;
use App\Http\Requests\UpdateHomeServiceCategoryRequest;
use App\Models\DriverHomeServices;
use App\Models\HomeServiceCategory;
use App\Models\Zone;
use App\Models\ZoneHomeServices;
use App\Repositories\HomeServiceCategoryRepository;
use Flash;
use Response;
use function __;
use function abort_if;
use function redirect;
use function view;

class HomeServiceCategoryController extends AppBaseController
{
    /** @var  HomeServiceCategoryRepository */
    private $homeServiceCategoryRepository;

    public function __construct(HomeServiceCategoryRepository $homeServiceCategoryRepo)
    {
        $this->homeServiceCategoryRepository = $homeServiceCategoryRepo;
    }

    /**
     * Display a listing of the HomeServiceCategory.
     *
     * @param HomeServiceCategoryDataTable $homeServiceCategoryDataTable
     * @return Response
     */
    public function index(HomeServiceCategoryDataTable $homeServiceCategoryDataTable)
    {   
        \Log::info('This is in Home service category control');
        // dd(HomeServiceCategory::with('parent_cat')->get()->toArray());
        abort_if(\Gate::denies('homeServiceCategories.index'), 403, '403 Forbidden');
        return $homeServiceCategoryDataTable->render('admin.home_service_categories.index');
    }

    /**
     * Show the form for creating a new HomeServiceCategory.
     *
     * @return Response
     */
    public function create()
    {
        \Log::info('This is in Home service category control');
        abort_if(\Gate::denies('homeServiceCategories.create'), 403, '403 Forbidden');
        $zones = Zone::where('active', 1)->get();
        return view('admin.home_service_categories.create',compact('zones'));
    }

    /**
     * Store a newly created HomeServiceCategory in storage.
     *
     * @param CreateHomeServiceCategoryRequest $request
     *
     * @return Response
     */
    public function store(CreateHomeServiceCategoryRequest $request)
    {

        \Log::info('This is in Home service control category');
        abort_if(\Gate::denies('homeServiceCategories.create'), 403, '403 Forbidden');
        $input = $request->all();
        if ($request->hasFile('icon')) {
            $input['icon'] = \Helpers::uploadImage($request->file('icon'), 'home_service_categories');
        }
        if ($request->hasFile('background_image')) {
            $input['background_image'] = \Helpers::uploadImage($request->file('background_image'), 'home_service_categories_bg');
        }
         $zone = $input['zones'];
         $input['zones'] = implode(',',$zone);
        //  dd();
        $input['payment_method'] = implode(',', $input['payment_methods']);
        $homeServiceCategory = $this->homeServiceCategoryRepository->create($input);
         
        foreach ($zone as $value){
            $driverHomeService = new ZoneHomeServices();
            $driverHomeService->zone_id = $value;
            $driverHomeService->home_service_category_id = $homeServiceCategory->id;
            $driverHomeService->save();
        }

        Flash::success(__('lang.messages.saved', ['model' => __('models/homeServiceCategories.singular')]));
        return redirect(route('admin.homeServiceCategories.index'));
    }

    /**
     * Display the specified HomeServiceCategory.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        \Log::info('This is in Home service control category');
        abort_if(\Gate::denies('homeServiceCategories.view'), 403, '403 Forbidden');
        $homeServiceCategory = $this->homeServiceCategoryRepository->find($id);

        if (empty($homeServiceCategory)) {
            Flash::error(__('lang.messages.not_found', ['model' => __('models/homeServiceCategories.singular')]));

            return redirect(route('admin.homeServiceCategories.index'));
        }

        return view('admin.home_service_categories.show')->with('homeServiceCategory', $homeServiceCategory);
    }

    /**
     * Show the form for editing the specified HomeServiceCategory.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        abort_if(\Gate::denies('homeServiceCategories.edit'), 403, '403 Forbidden');
        $homeServiceCategory = $this->homeServiceCategoryRepository->find($id);

        if (empty($homeServiceCategory)) {
            Flash::error(__('lang.messages.not_found', ['model' => __('models/homeServiceCategories.singular')]));

            return redirect(route('admin.homeServiceCategories.index'));
        }
        $zones = Zone::where('active', 1)->get();
          $payment= explode(',', $homeServiceCategory['payment_method']);
        foreach ($zones as $zone){
            if (ZoneHomeServices::where('zone_id', $zone->id)->where('home_service_category_id', $id)->exists()){
                $zone->selected = true;
            }else{
                $zone->selected = false;
            }
        }
        return view('admin.home_service_categories.edit',compact('zones'))->with('homeServiceCategory', $homeServiceCategory)->with('payment', $payment);
    }

    /**
     * Update the specified HomeServiceCategory in storage.
     *
     * @param  int              $id
     * @param UpdateHomeServiceCategoryRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateHomeServiceCategoryRequest $request)
    {
        abort_if(\Gate::denies('homeServiceCategories.edit'), 403, '403 Forbidden');
        $homeServiceCategory = $this->homeServiceCategoryRepository->find($id);

        if (empty($homeServiceCategory)) {
            Flash::error(__('lang.messages.not_found', ['model' => __('models/homeServiceCategories.singular')]));
            return redirect(route('admin.homeServiceCategories.index'));
        }

        $input = $request->all();
        if ($request->hasFile('icon')) {
            $input['icon'] = \Helpers::uploadImage($request->file('icon'), 'home_service_categories');
        }
        if ($request->hasFile('background_image')) {
            $input['background_image'] = \Helpers::uploadImage($request->file('background_image'), 'home_service_categories_bg');
        }
        // dd($request->zones);
        foreach ($input['zones'] as $value){
            $driverHomeService = new ZoneHomeServices();
            $driverHomeService->zone_id = $value;
            $driverHomeService->home_service_category_id = $id;
            $driverHomeService->save();
        }
          $input['payment_method'] = implode(',', $input['payment_methods']);
        $input['zones'] = implode(',',$request->zones);
        $homeServiceCategory = $this->homeServiceCategoryRepository->update($input, $id);

       

        Flash::success(__('lang.messages.updated', ['model' => __('models/homeServiceCategories.singular')]));

        return redirect(route('admin.homeServiceCategories.index'));
    }

    /**
     * Remove the specified HomeServiceCategory from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        abort_if(\Gate::denies('homeServiceCategories.delete'), 403, '403 Forbidden');
        $homeServiceCategory = $this->homeServiceCategoryRepository->with('children')->find($id);
        if (empty($homeServiceCategory)) {
            Flash::error(__('lang.messages.not_found', ['model' => __('models/homeServiceCategories.singular')]));
            return redirect(route('admin.homeServiceCategories.index'));
        }

        ZoneHomeServices::where('home_service_category_id', $id)->delete();
        DriverHomeServices::where('home_service_category_id', $id)->delete();
        HomeServiceCategory::where('parent', $id)->delete();
        $this->homeServiceCategoryRepository->delete($id);

        Flash::success(__('lang.messages.deleted', ['model' => __('models/homeServiceCategories.singular')]));

        return redirect(route('admin.homeServiceCategories.index'));
    }
}
