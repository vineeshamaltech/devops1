<?php
namespace App\Http\Controllers\Admin;
use App\DataTables\ProductDataTable;
use App\Http\Requests\CreateProductRequest;
use App\Http\Requests\UpdateProductRequest;
use App\Models\Store;
use App\Models\Category;
use App\Repositories\ProductRepository;
use Carbon\Carbon;
use Flash;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Response;

class ProductController extends AppBaseController
{
    /** @var  ProductRepository */
    private $productRepository;

    public function __construct(ProductRepository $productRepo)
    {
        $this->productRepository = $productRepo;
    }

    /**
     * Display a listing of the Product.
     *
     * @param ProductDataTable $productDataTable
     * @return Response
     */
    public function index(ProductDataTable $productDataTable)
    {
        \Log::info('This is in product controll  request');
        abort_if(\Gate::denies('products.index'), 403, '403 Forbidden');
        return $productDataTable->render('admin.products.index');
    }

    /**
     * Show the form for creating a new Product.
     *
     * @return Response
     */
    public function create()
    {
        \Log::info('This is in product controll create request');
        abort_if(\Gate::denies('products.create'), 403, '403 Forbidden');
        $stores = \Helpers::get_user_stores();
        $stores = Store::whereIn('id', $stores)->get();
        $categories = Category::all();
        if (empty($stores) && \Auth::user()->hasRole('Vendor')) {
            Flash::error('You do not have any stores to add a product to. Please create a store first.');
            return redirect(route('admin.stores.create'));
        }
        return view('admin.products.create',compact('stores','categories'));
    }

    /**
     * Store a newly created Product in storage.
     *
     * @param CreateProductRequest $request
     *
     * @return Response
     */
    public function store(Request $request)
    {
       // dd($request);
        \Log::info('This is in product controll store  request');
        abort_if(\Gate::denies('products.create'), 403, '403 Forbidden');
        $input = $request->all();
//dd($input);
        $request->validate([
            'store_id' => 'required',
            'product_type' => 'nullable',
            'name' => 'required',
            'description' => 'required',
            'image' => 'image|max:2048|mimes:jpeg,png,jpg',
            'unit' => 'required',
            'price' => 'required',
            'discount_price' => 'nullable',
            'category_id' => 'required',
            'sku' => 'required ',
            'product_tags' => 'required',
            'active' => 'required'
        ],[
            'image.max' =>  'Image size can not be greater than 2 MB'
        ]);

           dd($request);
        if($request->hasFile('image')){
            $input['image'] = \Helpers::uploadImage($request->file('image'), 'products');
        }
        if(empty($input['sub_category_id'])){
            $input['parent_category_id'] = $request->category_id;
            $input['category_id'] = $request->category_id;
        }else{
            $input['parent_category_id'] = $request->category_id;
            $input['category_id'] = $request->sub_category_id;
        }
        $input['unit'] = $request->unit;
        $input['opening_time'] = Carbon::parse($input['opening_time'])->format('H:i:s');
        $input['closing_time'] = Carbon::parse($input['closing_time'])->format('H:i:s');
        $product = $this->productRepository->create($input);

        Flash::success(__('lang.messages.saved', ['model' => __('models/products.singular')]));

        return redirect(route('admin.products.index'));
    }

    /**
     * Display the specified Product.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        \Log::info('This is in product controll show request');
        abort_if(\Gate::denies('products.view'), 403, '403 Forbidden');
        $product = $this->productRepository->with('categories','store')->find($id);
// dd($product);
        if (empty($product)) {
            Flash::error(__('lang.messages.not_found', ['model' => __('models/products.singular')]));

            return redirect(route('admin.products.index'));
        }

        return view('admin.products.show')->with('product', $product);
    }

    /**
     * Show the form for editing the specified Product.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        \Log::info('This is in product controll edit  request');
        abort_if(\Gate::denies('products.edit'), 403, '403 Forbidden');
        $product = $this->productRepository->find($id);
        if (empty($product)) {
            Flash::error(__('lang.messages.not_found', ['model' => __('models/products.singular')]));
            return redirect(route('admin.products.index'));
        }
        $stores = \Helpers::get_user_stores();
        $stores = Store::whereIn('id', $stores)->get();
        // dd($product);
        $cat = Category::where('store_id',$product->store_id)->get();
        // dd($product->toArray());  
        return view('admin.products.edit',compact('stores','product','cat'));
    }

    /**
     * Update the specified Product in storage.
     *
     * @param  int              $id
     * @param UpdateProductRequest $request
     *
     * @return Response
     */
    public function update($id, Request $request)
    {
        abort_if(\Gate::denies('products.edit'), 403, '403 Forbidden');
        $product = $this->productRepository->find($id);
        $input = $request->all();
        $request->validate([
            'store_id' => 'nullable',
            'product_type' => 'nullable',
            'name' => 'nullable',
            'description' => 'nullable',
            'image' => 'image|max:2048|mimes:jpeg,png,jpg',
            'unit' => 'nullable',
            'price' => 'nullable',
            'discount_price' => 'nullable',
            'category_id' => 'nullable',
            'sku' => 'nullable',
            'product_tags' => 'required',
            'opening_time' => 'nullable',
            'closing_time' => 'nullable',
            'active' => 'nullable'
        ],[
            'image.max' =>  'Image size can not be greater than 2 MB'
        ]);
        if (empty($product)) {
            Flash::error(__('lang.messages.not_found', ['model' => __('models/products.singular')]));
            return redirect(route('admin.products.index'));
        }
        if($request->hasFile('image')){
            \Helpers::deleteImage($product->image);
            $input['image'] = \Helpers::uploadImage($request->file('image'), 'products');
        }
        if(@$request->sub_category_id)
        {
            $input['parent_category_id'] = $request->sub_category_id;
        }      
    
        $input['unit'] = $request->unit;
        $input['opening_time'] = Carbon::parse($input['opening_time'])->format('H:i:s');
        $input['closing_time'] = Carbon::parse($input['closing_time'])->format('H:i:s');
        $product = $this->productRepository->update($input, $id);
        Flash::success(__('lang.messages.updated', ['model' => __('models/products.singular')]));
        return redirect(route('admin.products.index'));
    }

    /**
     * Remove the specified Product from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        \Log::info('This is in product controll destroy request');
        abort_if(\Gate::denies('products.delete'), 403, '403 Forbidden');
        $product = $this->productRepository->find($id);
        if (empty($product)) {
            Flash::error(__('lang.messages.not_found', ['model' => __('models/products.singular')]));
            return redirect(route('admin.products.index'));
        }
        $this->productRepository->delete($id);
        Flash::success(__('lang.messages.deleted', ['model' => __('models/products.singular')]));
        return redirect(route('admin.products.index'));
    }

    /**
     * Remove the list of products from storage.
     *
     * @param  array $ids
     *
     * @return Response
     */
    public function destroy_multiple(Request $request)
    {
        abort_if(\Gate::denies('products.delete'), 403, '403 Forbidden');
        if($request->delete){
            $products = $this->productRepository->findWhereIn('id',$request->delete);
            if (empty($products)) {
                Flash::error(__('lang.messages.not_found', ['model' => __('models/products.singular')]));
                return redirect(route('admin.products.index'));
            }
            foreach($products as $product){
                $this->productRepository->delete($product->id);
            }
            Flash::success(__('lang.messages.deleted', ['model' => __('models/products.singular')]));
            return redirect(route('admin.products.index'));
        }else{
            Flash::error(__('lang.messages.no_data_selected', ['model' => __('models/products.singular')]));
            return redirect(route('admin.products.index'));
        }
        
    }
}
