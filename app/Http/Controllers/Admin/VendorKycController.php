<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\VendorKycDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateVendorKycRequest;
use App\Http\Requests\UpdateVendorKycRequest;
use App\Models\Admin;
use App\Models\Driver;
use App\Models\DriverKyc;
use App\Models\Vendor;
use App\Models\VendorKyc;
use App\Notifications\KycStatusChanged;
use App\Repositories\VendorKycRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Notification;
use Response;

class VendorKycController extends AppBaseController
{
    /** @var  VendorKycRepository */
    private $vendorKycRepository;

    public function __construct(VendorKycRepository $vendorKycRepo)
    {
        $this->vendorKycRepository = $vendorKycRepo;
    }

    /**
     * Display a listing of the VendorKyc.
     *
     * @param VendorKycDataTable $vendorKycDataTable
     * @return Response
     */
    public function index(VendorKycDataTable $vendorKycDataTable)
    {
        \Log::info('This is in vendor kyc controll  request');
        abort_if(\Gate::denies('vendorsKyc.index'), 403, '403 Forbidden');
        return $vendorKycDataTable->render('admin.vendor_kycs.index');
    }

    /**
     * Show the form for creating a new VendorKyc.
     *
     * @return Response
     */
    public function create()
    {
        abort_if(\Gate::denies('vendorsKyc.create'), 403, '403 Forbidden');
        return view('admin.vendor_kycs.create');
    }

    /**
     * Store a newly created VendorKyc in storage.
     *
     * @param CreateVendorKycRequest $request
     *
     * @return Response
     */
    public function store(CreateVendorKycRequest $request)
    {
        abort_if(\Gate::denies('vendorsKyc.create'), 403, '403 Forbidden');
        $input = $request->all();

        if($request->hasFile('aadhar')){
            $input['doc_type'] = 'AADHAR';
            $input['photo'] = \Helpers::uploadImage($request->file('aadhar'), 'vendor_kyc');
            $vendorKyc = $this->vendorKycRepository->updateOrCreate([
                'doc_type'  => $input['doc_type'],
                'admin_id' => $input['admin_id'],
            ],$input);
        }

        if($request->hasFile('pan')){
            $input['doc_type'] = 'PAN';
            $input['photo'] = \Helpers::uploadImage($request->file('pan'), 'vendor_kyc');
            $vendorKyc = $this->vendorKycRepository->updateOrCreate([
                'doc_type'  => $input['doc_type'],
                'admin_id' => $input['admin_id'],
            ],$input);
        }

        if($request->hasFile('fssai')){
            $input['doc_type'] = 'FSSAI';
            $input['photo'] = \Helpers::uploadImage($request->file('fssai'), 'vendor_kyc');
            $vendorKyc = $this->vendorKycRepository->updateOrCreate([
                'doc_type'  => $input['doc_type'],
                'admin_id' => $input['admin_id'],
            ],$input);
        }

        if($request->hasFile('gst')){
            $input['doc_type'] = 'GST';
            $input['photo'] = \Helpers::uploadImage($request->file('gst'), 'vendor_kyc');
            $vendorKyc = $this->vendorKycRepository->updateOrCreate([
                'doc_type'  => $input['doc_type'],
                'admin_id' => $input['admin_id'],
            ],$input);
        }

        if($request->hasFile('other')){
            $input['doc_type'] = 'OTHER';
            $input['photo'] = \Helpers::uploadImage($request->file('other'), 'vendor_kyc');
            $vendorKyc = $this->vendorKycRepository->updateOrCreate([
                'doc_type'  => $input['doc_type'],
                'admin_id' => $input['admin_id'],
            ],$input);
        }

        if($request->hasFile('photo')){
            $input['doc_type'] = 'PHOTO';
            $input['photo'] = \Helpers::uploadImage($request->file('photo'), 'vendor_kyc');
            $vendorKyc = $this->vendorKycRepository->updateOrCreate([
                'doc_type'  => $input['doc_type'],
                'admin_id' => $input['admin_id'],
            ],$input);
        }

        Flash::success(__('lang.messages.saved', ['model' => __('models/vendorKycs.singular')]));

        return redirect(route('admin.vendorKycs.index'));
    }

    /**
     * Display the specified VendorKyc.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        abort_if(\Gate::denies('vendorsKyc.view'), 403, '403 Forbidden');
        $vendorKyc = $this->vendorKycRepository->find($id);

        if (empty($vendorKyc)) {
            Flash::error(__('lang.messages.not_found', ['model' => __('models/vendorKycs.singular')]));

            return redirect(route('admin.vendorKycs.index'));
        }

        return view('admin.vendor_kycs.show')->with('vendorKyc', $vendorKyc);
    }

    /**
     * Show the form for editing the specified VendorKyc.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        abort_if(\Gate::denies('vendorsKyc.edit'), 403, '403 Forbidden');
        $vendorKyc = $this->vendorKycRepository->find($id);

        if (empty($vendorKyc)) {
            Flash::error(__('lang.messages.not_found', ['model' => __('models/vendorKycs.singular')]));

            return redirect(route('admin.vendorKycs.index'));
        }

        return view('admin.vendor_kycs.edit')->with('vendorKyc', $vendorKyc);
    }

    /**
     * Update the specified VendorKyc in storage.
     *
     * @param  int              $id
     * @param UpdateVendorKycRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateVendorKycRequest $request)
    {
        abort_if(\Gate::denies('vendorsKyc.edit'), 403, '403 Forbidden');
        $vendorKyc = $this->vendorKycRepository->find($id);
        $input = $request->all();

        if (empty($vendorKyc)) {
            Flash::error(__('lang.messages.not_found', ['model' => __('models/vendorKycs.singular')]));
            return redirect(route('admin.vendorKycs.index'));
        }

        if($request->hasFile('photo')){
            $input['photo'] = \Helpers::uploadImage($request->file('photo'), 'vendor_kyc');
        }
        $vendorKyc = $this->vendorKycRepository->update($input, $id);
        Flash::success(__('lang.messages.updated', ['model' => __('models/vendorKycs.singular')]));
        return redirect(route('admin.vendorKycs.index'));
    }

    public function approve($id){
        //approve admin document
        $vendorKyc = $this->vendorKycRepository->find($id);
        VendorKyc::where('id',$id)->update(['status'=>'APPROVED']);
        $vendor = Admin::find($vendorKyc->admin_id);
        Notification::send($vendor,new KycStatusChanged($vendorKyc));
        $allkyc = VendorKyc::where('admin_id',$vendor->id)->get();
        $shouldActivate = false;
        foreach ($allkyc as $kyc){
            if($kyc->status != 'APPROVED'){
                $shouldActivate = false;
            }else{
                $shouldActivate = true;
            }
        }
        if ($shouldActivate){
            Admin::where('id',$vendor->id)->update(['verified'=>1]);
        }
        Flash::success(__('lang.messages.approved', ['model' => __('models/vendorKycs.singular')]));
        return redirect()->back();
    }

    public function reject($id){
        //approve admin document
        $vendorKyc = $this->vendorKycRepository->find($id);
        VendorKyc::where('id',$id)->update(['status'=>'REJECTED']);
        $vendor = Admin::find($vendorKyc->admin_id);
        Admin::where('id',$vendor->id)->update(['verified'=>0]);
        Notification::send($vendor,new KycStatusChanged($vendorKyc));
        Flash::success(__('lang.messages.approved', ['model' => __('models/vendorKycs.singular')]));
        return redirect()->back();
    }

    public function approveall($id){
        //approve admin document
        VendorKyc::where('admin_id',$id)->update(['status'=>'APPROVED']);
        $vendorKyc = VendorKyc::where('admin_id',$id)->orderBy('id','desc')->first();
        $vendor = Admin::find($id);
        $vendor->verified = 1;
        $vendor->save();
        Notification::send($vendor,new KycStatusChanged($vendorKyc));
        Flash::success(__('lang.messages.approved', ['model' => __('models/driverKycs.singular')]));
        return redirect()->back();
    }

    public function rejectall($id){
        //approve admin document
        VendorKyc::where('admin_id',$id)->update(['status'=>'REJECTED']);
        $vendorKyc = VendorKyc::where('admin_id',$id)->orderBy('id','desc')->first();
        $vendor = Admin::find($id);
        $vendor->verified = 0;
        $vendor->save();
        Notification::send($vendor,new KycStatusChanged($vendorKyc));
        Flash::success(__('Kyc Rejected Success', ['model' => __('models/driverKycs.singular')]));
        return redirect()->back();
    }

    /**
     * Remove the specified VendorKyc from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        abort_if(\Gate::denies('vendorsKyc.delete'), 403, '403 Forbidden');
        $vendorKyc = $this->vendorKycRepository->find($id);

        if (empty($vendorKyc)) {
            Flash::error(__('lang.messages.not_found', ['model' => __('models/vendorKycs.singular')]));

            return redirect(route('admin.vendorKycs.index'));
        }

        $this->vendorKycRepository->delete($id);

        Flash::success(__('lang.messages.deleted', ['model' => __('models/vendorKycs.singular')]));

        return redirect(route('admin.vendorKycs.index'));
    }

    public function delete($id)
    {
        $vendorKyc = $this->vendorKycRepository->find($id);

        if (empty($vendorKyc)) {
            Flash::error(__('lang.messages.not_found', ['model' => __('models/vendorKycs.singular')]));

            return redirect(route('admin.vendorKycs.index'));
        }
        $vendorKyc->photo = '';
        $vendorKyc->save();

        Flash::success(__('lang.messages.deleted', ['model' => __('models/vendorKycs.singular')]));
        return redirect()->back();
    }

    public function storeImage($id, Request $request)
    {
        $input = $request->all();
        if($request->hasFile('photo')){
            $input['photo'] = \Helpers::uploadImage($request->file('photo'), 'vendor_kyc');
            $vendorKyc = $this->vendorKycRepository->updateOrCreate([
                'id' => $id,
            ],$input);
        }
        Flash::success(__('lang.messages.saved', ['model' => __('models/vendorKycs.singular')]));
        return redirect()->back();
    }

    public function download($id)
    {
        $kyc_doc = VendorKyc::find($id);
        $headers = array(
            'Content-Type' => 'image/jpeg',
        );
        return response()->download($kyc_doc->photo, $kyc_doc->type.date("d-m-Y H:i:s").'.csv', $headers);
    }
}
