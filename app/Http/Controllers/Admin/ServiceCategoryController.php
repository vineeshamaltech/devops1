<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\ServiceCategoryDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateServiceCategoryRequest;
use App\Http\Requests\UpdateServiceCategoryRequest;
use App\Models\ServiceCategory;
use App\Models\ServiceCategoryPaymentMethod;
use App\Models\ServiceCategoryVehicleType;
use App\Repositories\ServiceCategoryRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use App\Models\Store;
use Response;

class ServiceCategoryController extends AppBaseController
{
    /** @var  ServiceCategoryRepository */
    private $serviceCategoryRepository;

    public function __construct(ServiceCategoryRepository $serviceCategoryRepo)
    {
        $this->serviceCategoryRepository = $serviceCategoryRepo;
    }

    /**
     * Display a listing of the ServiceCategory.
     *
     * @param ServiceCategoryDataTable $serviceCategoryDataTable
     * @return Response
     */
    public function index(ServiceCategoryDataTable $serviceCategoryDataTable)
    {
        \Log::info('This is in service category controll  request');
        abort_if(\Gate::denies('serviceCategories.index'), 403, '403 Forbidden');
        return $serviceCategoryDataTable->render('admin.service_categories.index');
    }

    /**
     * Show the form for creating a new ServiceCategory.
     *
     * @return Response
     */
    public function create()
    {
        abort_if(\Gate::denies('serviceCategories.create'), 403, '403 Forbidden');
        return view('admin.service_categories.create');
    }

    /**
     * Store a newly created ServiceCategory in storage.
     *
     * @param CreateServiceCategoryRequest $request
     *
     * @return Response
     */
    public function store(CreateServiceCategoryRequest $request)
    {
        abort_if(\Gate::denies('serviceCategories.create'), 403, '403 Forbidden');
        $input = $request->all();

        if($request->hasFile('icon')){
            $input['icon'] = \Helpers::uploadImage($request->file('icon'), 'service_category');
        }

        if($request->hasFile('bg_image')){
            $input['bg_image'] = \Helpers::uploadImage($request->file('bg_image'), 'service_category');
        }
        $payment_methods = $input['payment_methods'] ?? [];
        unset($input['payment_methods']);
        $vehicles = $input['vehicles'] ?? [];
        unset($input['vehicles']);
        $serviceCategory = $this->serviceCategoryRepository->create($input);
        foreach ($payment_methods as $payment_method) {
            ServiceCategoryPaymentMethod::create([
                'service_category_id' => $serviceCategory->id,
                'payment_type_id' => $payment_method
            ]);
        }
        foreach ($vehicles as $vehicle) {
            ServiceCategoryVehicleType::create([
                'service_category_id' => $serviceCategory->id,
                'vehicle_type_id' => $vehicle
            ]);
        }

        Flash::success(__('lang.messages.saved', ['model' => __('models/serviceCategories.singular')]));

        return redirect(route('admin.serviceCategories.index'));
    }

    /**
     * Display the specified ServiceCategory.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        abort_if(\Gate::denies('serviceCategories.view'), 403, '403 Forbidden');
        $serviceCategory = $this->serviceCategoryRepository->find($id);

        if (empty($serviceCategory)) {
            Flash::error(__('lang.messages.not_found', ['model' => __('models/serviceCategories.singular')]));

            return redirect(route('admin.serviceCategories.index'));
        }

        return view('admin.service_categories.show')->with('serviceCategory', $serviceCategory);
    }

    /**
     * Show the form for editing the specified ServiceCategory.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        abort_if(\Gate::denies('serviceCategories.edit'), 403, '403 Forbidden');
        $serviceCategory = $this->serviceCategoryRepository->with(['vehicles','service_type','payment_methods'])->find($id);
        if (empty($serviceCategory)) {
            Flash::error(__('lang.messages.not_found', ['model' => __('models/serviceCategories.singular')]));
            return redirect(route('admin.serviceCategories.index'));
        }
        $vehicles = ServiceCategoryVehicleType::where('service_category_id',$id)->pluck('vehicle_type_id')->toArray();
        $payment_methods = ServiceCategoryPaymentMethod::where('service_category_id',$id)->pluck('payment_type_id')->toArray();
        return view('admin.service_categories.edit',compact('vehicles','payment_methods','serviceCategory'));
    }

    /**
     * Update the specified ServiceCategory in storage.
     *
     * @param  int              $id
     * @param UpdateServiceCategoryRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateServiceCategoryRequest $request)
    {
        abort_if(\Gate::denies('serviceCategories.edit'), 403, '403 Forbidden');
        $serviceCategory = $this->serviceCategoryRepository->find($id);

        if (empty($serviceCategory)) {
            Flash::error(__('lang.messages.not_found', ['model' => __('models/serviceCategories.singular')]));

            return redirect(route('admin.serviceCategories.index'));
        }
        $input = $request->all();
        if($request->hasFile('icon')){
            $input['icon'] = \Helpers::uploadImage($request->file('icon'), 'service_category');
        }
        if ($request->hasFile('bg_image')) {
            $input['bg_image'] = \Helpers::uploadImage($request->file('bg_image'), 'service_category');
        }
        $payment_methods = $input['payment_methods'];
        unset($input['payment_methods']);
        $vehicles = $input['vehicles'];
        unset($input['vehicles']);
        $serviceCategory = $this->serviceCategoryRepository->update($input, $id);
        ServiceCategoryPaymentMethod::where('service_category_id',$id)->delete();
        ServiceCategoryVehicleType::where('service_category_id',$id)->delete();
        foreach ($payment_methods as $payment_method) {
            ServiceCategoryPaymentMethod::create([
                'service_category_id' => $serviceCategory->id,
                'payment_type_id' => $payment_method
            ]);
        }
        foreach ($vehicles as $vehicle) {
            ServiceCategoryVehicleType::create([
                'service_category_id' => $serviceCategory->id,
                'vehicle_type_id' => $vehicle
            ]);
        }
        Flash::success(__('lang.messages.updated', ['model' => __('models/serviceCategories.singular')]));
        return redirect(route('admin.serviceCategories.index'));
    }

    /**
     * Remove the specified ServiceCategory from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        abort_if(\Gate::denies('serviceCategories.delete'), 403, '403 Forbidden');
        $check_for_used = Store::where('service_category_id',$id)->get();
        if(count($check_for_used)>0){
            Flash::error("This category can't be deleted as it has products in it.");
            return redirect(route('admin.serviceCategories.index'));
        }
        $serviceCategory = $this->serviceCategoryRepository->find($id);

        if (empty($serviceCategory)) {
            Flash::error(__('lang.messages.not_found', ['model' => __('models/serviceCategories.singular')]));

            return redirect(route('admin.serviceCategories.index'));
        }

        $this->serviceCategoryRepository->delete($id);

        Flash::success(__('lang.messages.deleted', ['model' => __('models/serviceCategories.singular')]));

        return redirect(route('admin.serviceCategories.index'));
    }
}
