<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\AddressDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateAddressRequest;
use App\Http\Requests\UpdateAddressRequest;
use App\Repositories\AddressRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class AddressController extends AppBaseController
{
    /** @var  AddressRepository */
    private $addressRepository;

    public function __construct(AddressRepository $addressRepo)
    {
        $this->addressRepository = $addressRepo;
    }

    /**
     * Display a listing of the Address.
     *
     * @param AddressDataTable $addressDataTable
     * @return Response
     */
    public function index(AddressDataTable $addressDataTable)
    {

        \Log::info('This is in Address controller');
        abort_if(\Gate::denies('addresses.index'), 403, '403 Forbidden');
        return $addressDataTable->render('admin.addresses.index');
    }

    /**
     * Show the form for creating a new Address.
     *
     * @return Response
     */
    public function create()
    {
        \Log::info('This is in address create');
        abort_if(\Gate::denies('addresses.create'), 403, '403 Forbidden');
        return view('admin.addresses.create');
    }

    /**
     * Store a newly created Address in storage.
     *
     * @param CreateAddressRequest $request
     *
     * @return Response
     */
    public function store(CreateAddressRequest $request)
    {
        abort_if(\Gate::denies('addresses.create'), 403, '403 Forbidden');
        $input = $request->all();

        $address = $this->addressRepository->create($input);

        Flash::success(__('lang.messages.saved', ['model' => __('models/addresses.singular')]));

        return redirect(route('admin.addresses.index'));
    }

    /**
     * Display the specified Address.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        abort_if(\Gate::denies('addresses.view'), 403, '403 Forbidden');
        $address = $this->addressRepository->find($id);

        if (empty($address)) {
            Flash::error(__('lang.messages.not_found', ['model' => __('models/addresses.singular')]));

            return redirect(route('admin.addresses.index'));
        }

        return view('admin.addresses.show')->with('address', $address);
    }

    /**
     * Show the form for editing the specified Address.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        abort_if(\Gate::denies('addresses.edit'), 403, '403 Forbidden');
        $address = $this->addressRepository->find($id);

        if (empty($address)) {
            Flash::error(__('lang.messages.not_found', ['model' => __('models/addresses.singular')]));

            return redirect(route('admin.addresses.index'));
        }

        return view('admin.addresses.edit')->with('address', $address);
    }

    /**
     * Update the specified Address in storage.
     *
     * @param  int              $id
     * @param UpdateAddressRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateAddressRequest $request)
    {
        abort_if(\Gate::denies('addresses.edit'), 403, '403 Forbidden');
        $address = $this->addressRepository->find($id);

        if (empty($address)) {
            Flash::error(__('lang.messages.not_found', ['model' => __('models/addresses.singular')]));

            return redirect(route('admin.addresses.index'));
        }

        $address = $this->addressRepository->update($request->all(), $id);

        Flash::success(__('lang.messages.updated', ['model' => __('models/addresses.singular')]));

        return redirect(route('admin.addresses.index'));
    }

    /**
     * Remove the specified Address from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        abort_if(\Gate::denies('addresses.delete'), 403, '403 Forbidden');
        $address = $this->addressRepository->find($id);

        if (empty($address)) {
            Flash::error(__('lang.messages.not_found', ['model' => __('models/addresses.singular')]));

            return redirect(route('admin.addresses.index'));
        }

        $this->addressRepository->delete($id);

        Flash::success(__('lang.messages.deleted', ['model' => __('models/addresses.singular')]));

        return redirect(route('admin.addresses.index'));
    }
}
