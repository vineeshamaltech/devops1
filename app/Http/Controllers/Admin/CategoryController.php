<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\CategoryDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateCategoryRequest;
use App\Http\Requests\UpdateCategoryRequest;
use App\Models\Category;
use App\Models\Store;
use App\Repositories\CategoryRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Response;

class CategoryController extends AppBaseController
{
    /** @var  CategoryRepository */
    private $categoryRepository;

    public function __construct(CategoryRepository $categoryRepo)
    {
        $this->categoryRepository = $categoryRepo;
    }

    /**
     * Display a listing of the Category.
     *
     * @param CategoryDataTable $categoryDataTable
     * @return Response
     */
    public function index(CategoryDataTable $categoryDataTable)
    {
        abort_if(\Gate::denies('categories.index'), 403, '403 Forbidden');
        return $categoryDataTable->render('admin.categories.index');
    }

    /**
     * Show the form for creating a new Category.
     *
     * @return Response
     */
    public function create()
    {
        abort_if(\Gate::denies('categories.create'), 403, '403 Forbidden');
        $categories = Category::where('parent',0)->get();
        $stores = \Helpers::get_user_stores();
        $stores = Store::whereIn('id', $stores)->get();
        if (empty($stores->toArray()) && \Auth::user()->hasRole('Vendor')) {
            Flash::error('You do not have any stores to add a product to. Please create a store first.');
            return redirect(route('admin.stores.create'));
        }
        return view('admin.categories.create',compact('categories','stores'));
    }

    /**
     * Store a newly created Category in storage.
     *
     * @param CreateCategoryRequest $request
     *
     * @return Response
     */
    public function store(CreateCategoryRequest $request)
    {
        abort_if(\Gate::denies('categories.create'), 403, '403 Forbidden');
        $input = $request->all();
        if ($request->hasFile('image')) {
            $input['image'] = \Helpers::uploadImage($request->file('image'),'categories');
        }
        $category = $this->categoryRepository->create($input);

        Flash::success(__('lang.messages.saved', ['model' => __('models/categories.singular')]));

        return redirect(route('admin.categories.index'));
    }

    /**
     * Display the specified Category.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        abort_if(\Gate::denies('categories.view'), 403, '403 Forbidden');
        $category = $this->categoryRepository->find($id);

        if (empty($category)) {
            Flash::error(__('lang.messages.not_found', ['model' => __('models/categories.singular')]));
            return redirect(route('admin.categories.index'));
        }
        return view('admin.categories.show')->with('category', $category);
    }

    /**
     * Show the form for editing the specified Category.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        abort_if(\Gate::denies('categories.edit'), 403, '403 Forbidden');
        $category = $this->categoryRepository->find($id);
        if (empty($category)) {
            Flash::error(__('lang.messages.not_found', ['model' => __('models/categories.singular')]));
            return redirect(route('admin.categories.index'));
        }
        
        $categories = Category::where('parent',0)->where('store_id',$category->store_id)->get();
        $parent_category = Category::where('id',$category->parent)->get();
        $stores = \Helpers::get_user_stores();
        $stores = Store::whereIn('id', $stores)->get();
        return view('admin.categories.edit',compact('stores','categories','category','parent_category'));
    }

    /**
     * Update the specified Category in storage.
     *
     * @param  int              $id
     * @param UpdateCategoryRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateCategoryRequest $request)
    {
        abort_if(\Gate::denies('categories.edit'), 403, '403 Forbidden');
        $category = $this->categoryRepository->find($id);
        if (empty($category)) {
            Flash::error(__('lang.messages.not_found', ['model' => __('models/categories.singular')]));
            return redirect(route('admin.categories.index'));
        }
        $input = $request->all();
        if ($request->hasFile('image')) {
            \Helpers::deleteImage($category->image);
            $input['image'] = \Helpers::uploadImage($request->file('image'),'categories');
        }
        $category = $this->categoryRepository->update($input, $id);
        Flash::success(__('lang.messages.updated', ['model' => __('models/categories.singular')]));
        return redirect(route('admin.categories.index'));
    }

    /**
     * Remove the specified Category from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        abort_if(\Gate::denies('categories.delete'), 403, '403 Forbidden');
        $category = $this->categoryRepository->find($id);

        if (empty($category)) {
            Flash::error(__('lang.messages.not_found', ['model' => __('models/categories.singular')]));

            return redirect(route('admin.categories.index'));
        }

        $this->categoryRepository->delete($id);

        Flash::success(__('lang.messages.deleted', ['model' => __('models/categories.singular')]));

        return redirect(route('admin.categories.index'));
    }

    /**
     * Remove the list of Categories from storage.
     *
     * @param  array $ids
     *
     * @return Response
     */
    public function destroy_multiple(Request $request)
    {
        abort_if(\Gate::denies('categories.delete'), 403, '403 Forbidden');
        if($request->delete){
            $categories = $this->categoryRepository->findWhereIn('id',$request->delete);
            if (empty($categories)) {
                Flash::error(__('lang.messages.not_found', ['model' => __('models/categories.singular')]));
                return redirect(route('admin.categories.index'));
            }
            foreach($categories as $category){
                $this->categoryRepository->delete($category->id);
            }
            Flash::success(__('lang.messages.deleted', ['model' => __('models/categories.singular')]));
            return redirect(route('admin.categories.index'));
        }else{
            Flash::error(__('lang.messages.no_data_selected', ['model' => __('models/categories.singular')]));
            return redirect(route('admin.categories.index'));
        }
        
    }

    public function get_categories(){
        $user = auth()->user()->id;
        $stores = Store::Where('admin_id',$user)->get();
        $headers = array(
            "Content-type" => "text/csv",
            "Content-Disposition" => "attachment; filename=file.csv",
            "Pragma" => "no-cache",
            "Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
            "Expires" => "0"
        );
        if(count($stores) < 1){
            $stores = Store::get();
        }
        foreach($stores as $store) {
            $categoryIds[] = $store->id;
        }
        $categories = Category::whereIn('store_id',$categoryIds)->get();
        
        $columns = array('Store Id','Category Id', 'Category Name');
        $callback = function() use ($categories, $columns)
        {
            $file = fopen('php://output', 'w');
            fputcsv($file, $columns);
            foreach($categories as $category){
                fputcsv($file, array($category->store_id,$category->id, $category->name));
            }
            fclose($file);
        };
        return response()->stream($callback, 200, $headers);
    }
}
