<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\ReviewDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateReviewRequest;
use App\Http\Requests\UpdateReviewRequest;
use App\Repositories\ReviewRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use App\Models\Driver;
use App\Models\Order;
use Illuminate\Http\Request;
use Response;

class ReviewController extends AppBaseController
{
    /** @var  ReviewRepository */
    private $reviewRepository;

    public function __construct(ReviewRepository $reviewRepo)
    {
        $this->reviewRepository = $reviewRepo;
    }

    /**
     * Display a listing of the Review.
     *
     * @param ReviewDataTable $reviewDataTable
     * @return Response
     */
    public function index(ReviewDataTable $reviewDataTable)
    {

        \Log::info('This is in review controll  request');
        abort_if(\Gate::denies('reviews.index'), 403, '403 Forbidden');
        return $reviewDataTable->render('admin.reviews.index');
    }

    /**
     * Show the form for creating a new Review.
     *
     * @return Response
     */
    public function create()
    {
        abort_if(\Gate::denies('reviews.create'), 403, '403 Forbidden');
        $orders = Order::all();
        $drivers = Driver::all();
        return view('admin.reviews.create',compact('orders','drivers'));
    }

    /**
     * Store a newly created Review in storage.
     *
     * @param CreateReviewRequest $request
     *
     * @return Response
     */
    public function store(CreateReviewRequest $request)
    {
        abort_if(\Gate::denies('reviews.create'), 403, '403 Forbidden');
        $input = $request->all();

        $review = $this->reviewRepository->create($input);

        Flash::success(__('lang.messages.saved', ['model' => __('models/reviews.singular')]));

        return redirect(route('admin.reviews.index'));
    }

    /**
     * Display the specified Review.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        abort_if(\Gate::denies('reviews.view'), 403, '403 Forbidden');
        $review = $this->reviewRepository->with('driver')->find($id);
        if (empty($review)) {
            Flash::error(__('lang.messages.not_found', ['model' => __('models/reviews.singular')]));

            return redirect(route('admin.reviews.index'));
        }

        return view('admin.reviews.show')->with('review', $review);
    }

    /**
     * Show the form for editing the specified Review.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        abort_if(\Gate::denies('reviews.edit'), 403, '403 Forbidden');
        $review = $this->reviewRepository->find($id);
        $orders = Order::all();
        $drivers = Driver::all();
        if (empty($review)) {
            Flash::error(__('lang.messages.not_found', ['model' => __('models/reviews.singular')]));

            return redirect(route('admin.reviews.index'));
        }

        return view('admin.reviews.edit',compact('review','orders','drivers'));
    }

    /**
     * Update the specified Review in storage.
     *
     * @param  int              $id
     * @param UpdateReviewRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateReviewRequest $request)
    {
        abort_if(\Gate::denies('reviews.edit'), 403, '403 Forbidden');
        $review = $this->reviewRepository->find($id);

        if (empty($review)) {
            Flash::error(__('lang.messages.not_found', ['model' => __('models/reviews.singular')]));

            return redirect(route('admin.reviews.index'));
        }

        $review = $this->reviewRepository->update($request->all(), $id);

        Flash::success(__('lang.messages.updated', ['model' => __('models/reviews.singular')]));

        return redirect(route('admin.reviews.index'));
    }

    /**
     * Remove the specified Review from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        abort_if(\Gate::denies('reviews.delete'), 403, '403 Forbidden');
        $review = $this->reviewRepository->find($id);

        if (empty($review)) {
            Flash::error(__('lang.messages.not_found', ['model' => __('models/reviews.singular')]));

            return redirect(route('admin.reviews.index'));
        }

        $this->reviewRepository->delete($id);

        Flash::success(__('lang.messages.deleted', ['model' => __('models/reviews.singular')]));

        return redirect(route('admin.reviews.index'));
    }

    /**
     * Remove the list of reviews from storage.
     *
     * @param  array $ids
     *
     * @return Response
     */
    public function destroy_multiple(Request $request)
    {
        abort_if(\Gate::denies('reviews.delete'), 403, '403 Forbidden');
        if($request->delete){
            $reviews = $this->reviewRepository->findWhereIn('id',$request->delete);
            if (empty($reviews)) {
                Flash::error(__('lang.messages.not_found', ['model' => __('models/reviews.singular')]));
                return redirect(route('admin.reviews.index'));
            }
            foreach($reviews as $review){
                $this->reviewRepository->delete($review->id);
            }
            Flash::success(__('lang.messages.deleted', ['model' => __('models/reviews.singular')]));
            return redirect(route('admin.reviews.index'));
        }else{
            Flash::error(__('lang.messages.no_data_selected', ['model' => __('models/reviews.singular')]));
            return redirect(route('admin.reviews.index'));
        }
        
    }
}
