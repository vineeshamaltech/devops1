<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\StoreTimingDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateStoreTimingRequest;
use App\Http\Requests\UpdateStoreTimingRequest;
use App\Models\Store;
use App\Repositories\StoreTimingRepository;
use Carbon\Carbon;
use Flash;
use App\Http\Controllers\AppBaseController;
use App\Models\StoreTiming;
use Helpers;
use Illuminate\Session\Store as SessionStore;
use Response;

class StoreTimingController extends AppBaseController
{
    /** @var  StoreTimingRepository */
    private $storeTimingRepository;

    public function __construct(StoreTimingRepository $storeTimingRepo)
    {
        $this->storeTimingRepository = $storeTimingRepo;
    }

    /**
     * Display a listing of the StoreTiming.
     *
     * @param StoreTimingDataTable $storeTimingDataTable
     * @return Response
     */
    public function index(StoreTimingDataTable $storeTimingDataTable)
    {
        \Log::info('This is in store timing controll  request');
        //->update(['is_open'=>true])
        // $stores = \Helpers::get_user_stores();
        // $return = Store::with('store_timings')->select('stores.*')->whereIn('id',$stores)->get();
        // dd($return->toArray());
        abort_if(\Gate::denies('storeTimings.index'), 403, '403 Forbidden');
        return $storeTimingDataTable->render('admin.store_timings.index');
    }

    /**
     * Show the form for creating a new StoreTiming.
     *
     * @return Response
     */
    public function create()
    {
        abort_if(\Gate::denies('storeTimings.create'), 403, '403 Forbidden');
        $stores = \Helpers::get_user_stores();
        $stores = Store::whereIn('id', $stores)->get();
        return view('admin.store_timings.create',compact('stores'));
    }

    /**
     * Store a newly created StoreTiming in storage.
     *
     * @param CreateStoreTimingRequest $request
     *
     * @return Response
     */
    public function store(CreateStoreTimingRequest $request)
    {
        abort_if(\Gate::denies('storeTimings.create'), 403, '403 Forbidden');
        $input = $request->all();
        $this->storeTimingRepository->where('store_id',$request->store_id)->delete();
        $days = [
            'monday',
            'tuesday',
            'wednesday',
            'thursday',
            'friday',
            'saturday',
            'sunday',
        ];

        foreach($days as $day){
            $days_array = $input[$day.'_open'];
            for($i=0;$i<count($days_array);$i++){
                $input['store_id'] = $request->store_id;
                $input['day'] = ucfirst($day);
                $input['open'] = Carbon::parse($input[$day.'_open'][$i])->format('H:i:s');
                $input['close'] = Carbon::parse($input[$day.'_close'][$i])->format('H:i:s');
                $storeTiming = $this->storeTimingRepository->create($input);
            }
        }
        Flash::success(__('lang.messages.saved', ['model' => __('models/storeTimings.singular')]));

        return redirect(route('admin.storeTimings.index'));
    }

    /**
     * Display the specified StoreTiming.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        abort_if(\Gate::denies('storeTimings.view'), 403, '403 Forbidden');
        $storeTiming = Store::with('store_timings')->where('id',$id)->get();
        $timing_data = [];
        $all_days = [
            'Monday'=>$timing_data,
            'Tuesday'=>$timing_data,
            'Wednesday'=>$timing_data,
            'Thursday'=>$timing_data,
            'Friday'=>$timing_data,
            'Saturday'=>$timing_data,
            'Sunday'=>$timing_data
        ];
        foreach($all_days as $key => $day){
            foreach($storeTiming[0]->store_timings as $time){
                if($key == $time->day){
                    array_push($all_days[$key],$time->open);
                    array_push($all_days[$key],$time->close);
                }
            }
        }

        if (empty($storeTiming)) {
            Flash::error(__('lang.messages.not_found', ['model' => __('models/storeTimings.singular')]));

            return redirect(route('admin.storeTimings.index'));
        }

        return view('admin.store_timings.show',compact('storeTiming','all_days'));
    }

    /**
     * Show the form for editing the specified StoreTiming.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        abort_if(\Gate::denies('storeTimings.edit'), 403, '403 Forbidden');
        $storeTiming = Store::with('store_timings')->where('id',$id)->get();
        $timing_data = [];
        $all_days = [
            'Monday'=>$timing_data,
            'Tuesday'=>$timing_data,
            'Wednesday'=>$timing_data,
            'Thursday'=>$timing_data,
            'Friday'=>$timing_data,
            'Saturday'=>$timing_data,
            'Sunday'=>$timing_data
        ];
        foreach($all_days as $key => $day){
            foreach($storeTiming[0]->store_timings as $time){
                if($key == $time->day){
                    $temp = [
                        "open"=>$time->open,    
                        "close"=>$time->close
                    ];
                    array_push($all_days[$key],$temp);
                }
            }
        }
        // dd(count($all_days["Monday"]));
        if (empty($storeTiming)) {
            Flash::error(__('lang.messages.not_found', ['model' => __('models/storeTimings.singular')]));
            return redirect(route('admin.storeTimings.index'));
        }
        $stores = \Helpers::get_user_stores();
        $stores = Store::whereIn('id', $stores)->get();
        return view('admin.store_timings.edit',compact('stores','storeTiming','all_days'));
    }

    /**
     * Update the specified StoreTiming in storage.
     *
     * @param  int              $id
     * @param UpdateStoreTimingRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateStoreTimingRequest $request)
    {
        abort_if(\Gate::denies('storeTimings.edit'), 403, '403 Forbidden');
        $input = $request->all();
        $this->storeTimingRepository->where('store_id',$id)->delete();
        $days = [
            'monday',
            'tuesday',
            'wednesday',
            'thursday',
            'friday',
            'saturday',
            'sunday',
        ];

        foreach($days as $day){
            $days_array = $input[$day.'_open'];
            for($i=0;$i<count($days_array);$i++){
                $input['store_id'] = $request->store_id;
                $input['day'] = ucfirst($day);
                $input['open'] = Carbon::parse($input[$day.'_open'][$i])->format('H:i:s');
                $input['close'] = Carbon::parse($input[$day.'_close'][$i])->format('H:i:s');
                $storeTiming = $this->storeTimingRepository->create($input);
            }
        }

        Flash::success(__('lang.messages.updated', ['model' => __('models/storeTimings.singular')]));

        return redirect(route('admin.storeTimings.index'));
    }

    /**
     * Remove the specified StoreTiming from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        abort_if(\Gate::denies('storeTimings.delete'), 403, '403 Forbidden');
        $storeTiming = $this->storeTimingRepository->where('store_id',$id);
        if (empty($storeTiming)) {
            Flash::error(__('lang.messages.not_found', ['model' => __('models/storeTimings.singular')]));

            return redirect(route('admin.storeTimings.index'));
        }

        $storeTiming->delete();

        Flash::success(__('lang.messages.deleted', ['model' => __('models/storeTimings.singular')]));

        return redirect(route('admin.storeTimings.index'));
    }
}
