<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Setting;
use Illuminate\Http\Request;
use Laracasts\Flash\Flash;

class SettingsController extends Controller
{
    public function index()
    {
        \Log::info('This is in settings controll  request');
        $groups = Setting::selectRaw('DISTINCT(group_name)')->get();
        foreach ($groups as $group) {
            $group['settings'] = Setting::where('group_name', $group['group_name'])->orderBy('row_order')->get();
        }
        return view('admin.settings.index',compact('groups'));
    }

    public function update(Request $request)
    {
        $input = $request->all();
        unset($input['_token']);
        foreach ($input as $key => $value) {
            $setting = Setting::where('key_name',$key)->orderBy('row_order','ASC')->first();
            $setting->key_value = $value;
            $setting->save();
        }
        Flash::success('Settings updated successfully.');
        return redirect()->back();
    }
}
