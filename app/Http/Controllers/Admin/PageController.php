<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\PageDataTable;
use App\Http\Requests;
use App\Http\Requests\CreatePageRequest;
use App\Http\Requests\UpdatePageRequest;
use App\Repositories\PageRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Illuminate\Support\Str;
use Response;

class PageController extends AppBaseController
{
    /** @var  PageRepository */
    private $pageRepository;

    public function __construct(PageRepository $pageRepo)
    {
        $this->pageRepository = $pageRepo;
    }

    /**
     * Display a listing of the Page.
     *
     * @param PageDataTable $pageDataTable
     * @return Response
     */
    public function index(PageDataTable $pageDataTable)
    {
        \Log::info('This is in page controller');
        abort_if(\Gate::denies('pages.index'), 403, '403 Forbidden');
        return $pageDataTable->render('admin.pages.index');
    }

    /**
     * Show the form for creating a new Page.
     *
     * @return Response
     */
    public function create()
    {
        abort_if(\Gate::denies('pages.create'), 403, '403 Forbidden');
        return view('admin.pages.create');
    }

    /**
     * Store a newly created Page in storage.
     *
     * @param CreatePageRequest $request
     *
     * @return Response
     */
    public function store(CreatePageRequest $request)
    {
        abort_if(\Gate::denies('pages.create'), 403, '403 Forbidden');
        $input = $request->all();
        $input['slug'] = Str::slug($input['title']);
        $page = $this->pageRepository->create($input);

        Flash::success(__('lang.messages.saved', ['model' => __('models/pages.singular')]));

        return redirect(route('admin.pages.index'));
    }

    /**
     * Display the specified Page.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($slug)
    {
        $page = $this->pageRepository->findWhere(['slug' => $slug])->first();

        if (empty($page)) {
            Flash::error(__('lang.messages.not_found', ['model' => __('models/pages.singular')]));
            return redirect(route('admin.pages.index'));
        }

        return view('admin.pages.show')->with('page', $page);
    }

    /**
     * Show the form for editing the specified Page.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        abort_if(\Gate::denies('pages.edit'), 403, '403 Forbidden');
        $page = $this->pageRepository->find($id);

        if (empty($page)) {
            Flash::error(__('lang.messages.not_found', ['model' => __('models/pages.singular')]));

            return redirect(route('admin.pages.index'));
        }

        return view('admin.pages.edit')->with('page', $page);
    }

    /**
     * Update the specified Page in storage.
     *
     * @param  int              $id
     * @param UpdatePageRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePageRequest $request)
    {
        abort_if(\Gate::denies('pages.edit'), 403, '403 Forbidden');
        $page = $this->pageRepository->find($id);

        if (empty($page)) {
            Flash::error(__('lang.messages.not_found', ['model' => __('models/pages.singular')]));

            return redirect(route('admin.pages.index'));
        }

        $page = $this->pageRepository->update($request->all(), $id);

        Flash::success(__('lang.messages.updated', ['model' => __('models/pages.singular')]));

        return redirect(route('admin.pages.index'));
    }

    /**
     * Remove the specified Page from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        abort_if(\Gate::denies('pages.delete'), 403, '403 Forbidden');
        $page = $this->pageRepository->find($id);

        if (empty($page)) {
            Flash::error(__('lang.messages.not_found', ['model' => __('models/pages.singular')]));

            return redirect(route('admin.pages.index'));
        }

        $this->pageRepository->delete($id);

        Flash::success(__('lang.messages.deleted', ['model' => __('models/pages.singular')]));

        return redirect(route('admin.pages.index'));
    }
}
