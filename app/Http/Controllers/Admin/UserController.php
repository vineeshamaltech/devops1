<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\UserDataTable;
use App\Helpers\WalletHelper;
use App\Http\Requests\CreateUserRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Models\Payment;
use App\Models\User;
use App\Repositories\UserRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use http\Env\Request;
use Illuminate\Support\Facades\Hash;
use Response;

class UserController extends AppBaseController
{
    /** @var  UserRepository */
    private $userRepository;

    public function __construct(UserRepository $userRepo)
    {
        $this->userRepository = $userRepo;
    }

    /**
     * Display a listing of the User.
     *
     * @param UserDataTable $userDataTable
     * @return Response
     */
    public function index(UserDataTable $userDataTable)
    {
        \Log::info('This is in user controll  request');
        return $userDataTable->render('admin.users.index');
    }

    /**
     * Show the form for creating a new User.
     *
     * @return Response
     */
    public function create()
    { 
       
        return view('admin.users.create');
    }

    /**
     * Store a newly created User in storage.
     *
     * @param CreateUserRequest $request
     *
     * @return Response
     */
    public function store(CreateUserRequest $request)
    {
       //dd($request);
        $input = $request->all();
        $input['password'] = Hash::make($input['password']);
        if ($request->hasFile('profile_img')){
            $input['profile_img'] = \Helpers::uploadImage($request->file('profile_img'), 'users');
        }
        
        $user = $this->userRepository->create($input);
       // dd($user);
        User::where('id',$user->id)->update(['ref_code' => \Helpers::randomString(6).$user->id]);
        Flash::success(__('lang.messages.saved', ['model' => __('models/users.singular')]));
        return redirect(route('admin.users.index'));
    }

    /**
     * Display the specified User.
     *
     * @param  int $id
     *
     * @return Response
     */

    public function show($id){
       // dd($id);
        $user = $this->userRepository->find($id);
       // dd($user);
        if (empty($user)) {
            Flash::error(__('lang.messages.not_found', ['model' => __('models/users.singular')]));
            return redirect(route('admin.users.index'));
        }
        return view('admin.users.show')->with('user', $user);
    }

    /**
     * Show the form for editing the specified User.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
       //dd($id);
        $user = $this->userRepository->find($id);
        //dd($user);
        if (empty($user)) {
            Flash::error(__('lang.messages.not_found', ['model' => __('models/users.singular')]));
            return redirect(route('admin.users.index'));
        }
        return view('admin.users.edit')->with('user', $user);
    }

    /**
     * Update the specified User in storage.
     *
     * @param  int              $id
     * @param UpdateUserRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateUserRequest $request)
    {
       // dd($request);
        $request->validate([
            'name' => 'nullable|string|max:255',
            'mobile' => 'required|unique:users,mobile,'.$id,
            'email' => 'nullable|email|unique:users,email,'.$id,
            'password' => 'nullable|confirmed',
        ]);
        $user = $this->userRepository->find($id);
        dd($user);
        if (empty($user)) {
            Flash::error(__('lang.messages.not_found', ['model' => __('models/users.singular')]));
            return redirect(route('admin.users.index'));
        }
      
        $input = $request->all();
       // dd($input);
        if(!empty($input['password'])){
            $input['password'] = Hash::make($input['password']);
        }else{
            unset($input['password']);
        }
        if (empty($input['wallet_amount'])){
            $input['wallet_amount'] = 0;
        }
        if ($request->hasFile('profile_img')){
            $input['profile_img'] = \Helpers::uploadImage($request->file('profile_img'), 'users');
        }
        $user = $this->userRepository->update($input, $id);
       // dd($user);
        Flash::success(__('lang.messages.updated', ['model' => __('models/users.singular')]));
        return redirect(route('admin.users.index'));
    }

    /**
     * Remove the specified User from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
       // dd($id);
        $user = $this->userRepository->find($id);
//dd($user);
        if (empty($user)) {
            Flash::error(__('lang.messages.not_found', ['model' => __('models/users.singular')]));

            return redirect(route('admin.users.index'));
        }

        $this->userRepository->delete($id);
//dd($id);
        Flash::success(__('lang.messages.deleted', ['model' => __('models/users.singular')]));

        return redirect(route('admin.users.index'));
    }
}
