<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\DriverWalletTransactionDataTable;
use App\Http\Requests;
use App\Models\Driver;
use App\Http\Requests\CreateDriverWalletTransactionRequest;
use App\Http\Requests\UpdateDriverWalletTransactionRequest;
use App\Repositories\DriverWalletTransactionRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use App\Helpers\WalletHelper;
use Illuminate\Http\Request;
use Response;

class DriverWalletTransactionController extends AppBaseController
{
    /** @var  DriverWalletTransactionRepository */
    private $driverWalletTransactionRepository;

    public function __construct(DriverWalletTransactionRepository $driverWalletTransactionRepo)
    {
        $this->driverWalletTransactionRepository = $driverWalletTransactionRepo;
    }

    /**
     * Display a listing of the DriverWalletTransaction.
     *
     * @param DriverWalletTransactionDataTable $driverWalletTransactionDataTable
     * @return Response
     */
    public function index(DriverWalletTransactionDataTable $driverWalletTransactionDataTable)
    {
        abort_if(\Gate::denies('driverWalletTransactions.index'), 403, '403 Forbidden');
        return $driverWalletTransactionDataTable->render('admin.driver_wallet_transactions.index');
    }

    /**
     * Show the form for creating a new DriverWalletTransaction.
     *
     * @return Response
     */
    public function create()
    {
        abort_if(\Gate::denies('driverWalletTransactions.create'), 403, '403 Forbidden');
        return view('admin.driver_wallet_transactions.create');
    }

    /**
     * Store a newly created DriverWalletTransaction in storage.
     *
     * @param CreateDriverWalletTransactionRequest $request
     *
     * @return Response
     */
    public function store(CreateDriverWalletTransactionRequest $request)
    {
        abort_if(\Gate::denies('driverWalletTransactions.create'), 403, '403 Forbidden');
        $input = $request->all();
        if($input['driver_id'] == -1)
      {
        $all= Driver::select('id','name','mobile')->get();
        foreach ($all as $key => $value) {
             
            if ($input['type'] == 'CREDIT') {
                WalletHelper::creditDriverWallet( $value->id, $input['amount'], $input['remarks']);
                }else{
                WalletHelper::debitDriverWallet( $value->id, $input['amount'], $input['remarks']);
        }
      
     }
    
      }
      else{
        if ($input['type'] == 'CREDIT') {
            WalletHelper::creditDriverWallet($input['driver_id'], $input['amount'], $input['remarks']);
        }else{
            WalletHelper::debitDriverWallet($input['driver_id'], $input['amount'], $input['remarks']);
        }
      }
      
          
        // $driverWalletTransaction = $this->driverWalletTransactionRepository->create($input);

        Flash::success(__('lang.messages.saved', ['model' => __('models/driverWalletTransactions.singular')]));

        return redirect(route('admin.driverWalletTransactions.index'));
    }

    /**
     * Display the specified DriverWalletTransaction.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        abort_if(\Gate::denies('driverWalletTransactions.view'), 403, '403 Forbidden');
        $driverWalletTransaction = $this->driverWalletTransactionRepository->find($id);

        if (empty($driverWalletTransaction)) {
            Flash::error(__('lang.messages.not_found', ['model' => __('models/driverWalletTransactions.singular')]));

            return redirect(route('admin.driverWalletTransactions.index'));
        }

        return view('admin.driver_wallet_transactions.show')->with('driverWalletTransaction', $driverWalletTransaction);
    }

    /**
     * Show the form for editing the specified DriverWalletTransaction.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        abort_if(\Gate::denies('driverWalletTransactions.edit'), 403, '403 Forbidden');
        $driverWalletTransaction = $this->driverWalletTransactionRepository->find($id);

        if (empty($driverWalletTransaction)) {
            Flash::error(__('lang.messages.not_found', ['model' => __('models/driverWalletTransactions.singular')]));

            return redirect(route('admin.driverWalletTransactions.index'));
        }

        return view('admin.driver_wallet_transactions.edit')->with('driverWalletTransaction', $driverWalletTransaction);
    }

    /**
     * Update the specified DriverWalletTransaction in storage.
     *
     * @param  int              $id
     * @param UpdateDriverWalletTransactionRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateDriverWalletTransactionRequest $request)
    {
        abort_if(\Gate::denies('driverWalletTransactions.edit'), 403, '403 Forbidden');
        $driverWalletTransaction = $this->driverWalletTransactionRepository->find($id);

        if (empty($driverWalletTransaction)) {
            Flash::error(__('lang.messages.not_found', ['model' => __('models/driverWalletTransactions.singular')]));

            return redirect(route('admin.driverWalletTransactions.index'));
        }

        $driverWalletTransaction = $this->driverWalletTransactionRepository->update($request->all(), $id);

        Flash::success(__('lang.messages.updated', ['model' => __('models/driverWalletTransactions.singular')]));

        return redirect(route('admin.driverWalletTransactions.index'));
    }

    /**
     * Remove the specified DriverWalletTransaction from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        abort_if(\Gate::denies('driverWalletTransactions.delete'), 403, '403 Forbidden');
        $driverWalletTransaction = $this->driverWalletTransactionRepository->find($id);

        if (empty($driverWalletTransaction)) {
            Flash::error(__('lang.messages.not_found', ['model' => __('models/driverWalletTransactions.singular')]));

            return redirect(route('admin.driverWalletTransactions.index'));
        }

        $this->driverWalletTransactionRepository->delete($id);

        Flash::success(__('lang.messages.deleted', ['model' => __('models/driverWalletTransactions.singular')]));

        return redirect(route('admin.driverWalletTransactions.index'));
    }

    /**
     * Remove the list of driverWalletTransactions from storage.
     *
     * @param  array $ids
     *
     * @return Response
     */
    public function destroy_multiple(Request $request)
    {
        abort_if(\Gate::denies('driverWalletTransactions.delete'), 403, '403 Forbidden');
        if($request->delete){
            $driverWalletTransactions = $this->driverWalletTransactionRepository->findWhereIn('id',$request->delete);
            if (empty($driverWalletTransactions)) {
                Flash::error(__('lang.messages.not_found', ['model' => __('models/driverWalletTransactions.singular')]));
                return redirect(route('admin.driverWalletTransactions.index'));
            }
            foreach($driverWalletTransactions as $driverWalletTransaction){
                $this->driverWalletTransactionRepository->delete($driverWalletTransaction->id);
            }
            Flash::success(__('lang.messages.deleted', ['model' => __('models/driverWalletTransactions.singular')]));
            return redirect(route('admin.driverWalletTransactions.index'));
        }else{
            Flash::error(__('lang.messages.no_data_selected', ['model' => __('models/driverWalletTransactions.singular')]));
            return redirect(route('admin.driverWalletTransactions.index'));
        }
        
    }
}
