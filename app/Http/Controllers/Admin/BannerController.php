<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\BannerDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateBannerRequest;
use App\Http\Requests\UpdateBannerRequest;
use App\Models\Store;
use App\Repositories\BannerRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use App\Models\Zone;
use Illuminate\Http\Request;
use Response;

class BannerController extends AppBaseController
{
    /** @var  BannerRepository */
    private $bannerRepository;

    public function __construct(BannerRepository $bannerRepo)
    {
        $this->bannerRepository = $bannerRepo;
    }

    /**
     * Display a listing of the Banner.
     *
     * @param BannerDataTable $bannerDataTable
     * @return Response
     */
    public function index(BannerDataTable $bannerDataTable)
    {
        // dd(auth()->user());
        abort_if(\Gate::denies('banners.index'), 403, '403 Forbidden');
        return $bannerDataTable->render('admin.banners.index');
    }

    /**
     * Show the form for creating a new Banner.
     *
     * @return Response
     */
    public function create()
    {
        abort_if(\Gate::denies('banners.create'), 403, '403 Forbidden');
        $stores = \Helpers::get_user_stores();
        $stores = Store::whereIn('id', $stores)->get();
        $zones = Zone::all();
        if (empty($stores->toArray()) && \Auth::user()->hasRole('Vendor')) {
            Flash::error('You do not have any stores to add a banner to. Please create a store first.');
            return redirect(route('admin.stores.create'));
        }
        return view('admin.banners.create',compact('stores','zones'));
    }

    /**
     * Store a newly created Banner in storage.
     *
     * @param CreateBannerRequest $request
     *
     * @return Response
     */
    public function store(CreateBannerRequest $request)
    {
        abort_if(\Gate::denies('banners.create'), 403, '403 Forbidden');
        $input = $request->all();
        if($request->hasFile('image')){
            $input['image'] = \Helpers::uploadImage($request->file('image'), 'banners');
        }
        if ($input['store_id'] == 0) {
            $input['store_id'] = null;
        }
        if ($input['service_category_id'] == 0) {
            $input['service_category_id'] = null;
        }
        if ($request->has('zones')) {
            $input['zones'] = implode(",",$request->zones);
        }
        $banner = $this->bannerRepository->create($input);

        Flash::success(__('lang.messages.saved', ['model' => __('models/banners.singular')]));

        return redirect(route('admin.banners.index'));
    }

    /**
     * Display the specified Banner.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        abort_if(\Gate::denies('banners.view'), 403, '403 Forbidden');
        $banner = $this->bannerRepository->find($id);

        if (empty($banner)) {
            Flash::error(__('lang.messages.not_found', ['model' => __('models/banners.singular')]));

            return redirect(route('admin.banners.index'));
        }

        return view('admin.banners.show')->with('banner', $banner);
    }

    /**
     * Show the form for editing the specified Banner.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        abort_if(\Gate::denies('banners.edit'), 403, '403 Forbidden');
        $banner = $this->bannerRepository->find($id);
        if (empty($banner)) {
            Flash::error(__('lang.messages.not_found', ['model' => __('models/banners.singular')]));
            return redirect(route('admin.banners.index'));
        }
        $stores = \Helpers::get_user_stores();
        $stores = Store::whereIn('id', $stores)->get();
        $zone_se = explode(",",$banner->zones);
        $zones = Zone::all();
        foreach ($zones as $zone){
            if (in_array($zone->id,$zone_se)){
                $zone->selected = true;
            }else{
                $zone->selected = false;
            }
        }
        return view('admin.banners.edit',compact('stores','banner','zones'));
    }

    /**
     * Update the specified Banner in storage.
     *
     * @param  int              $id
     * @param UpdateBannerRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateBannerRequest $request)
    {
        abort_if(\Gate::denies('banners.edit'), 403, '403 Forbidden');
        $banner = $this->bannerRepository->find($id);
        $input = $request->all();
        if (empty($banner)) {
            Flash::error(__('lang.messages.not_found', ['model' => __('models/banners.singular')]));

            return redirect(route('admin.banners.index'));
        }
        if($request->hasFile('image')){
            $input['image'] = \Helpers::uploadImage($request->file('image'), 'banners');
        }
        if ($input['store_id'] == 0) {
            $input['store_id'] = null;
        }
        if ($input['service_category_id'] == 0) {
            $input['service_category_id'] = null;
        }
        if ($request->has('zones')) {
            $input['zones'] = implode(",",$request->zones);
        }
        $banner = $this->bannerRepository->update($input, $id);

        Flash::success(__('lang.messages.updated', ['model' => __('models/banners.singular')]));

        return redirect(route('admin.banners.index'));
    }

    /**
     * Remove the specified Banner from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        abort_if(\Gate::denies('banners.delete'), 403, '403 Forbidden');
        $banner = $this->bannerRepository->find($id);

        if (empty($banner)) {
            Flash::error(__('lang.messages.not_found', ['model' => __('models/banners.singular')]));

            return redirect(route('admin.banners.index'));
        }

        $this->bannerRepository->delete($id);

        Flash::success(__('lang.messages.deleted', ['model' => __('models/banners.singular')]));

        return redirect(route('admin.banners.index'));
    }

    /**
     * Remove the list of banners from storage.
     *
     * @param  array $ids
     *
     * @return Response
     */
    public function destroy_multiple(Request $request)
    {
        abort_if(\Gate::denies('banners.delete'), 403, '403 Forbidden');
        if($request->delete){
            $banners = $this->bannerRepository->findWhereIn('id',$request->delete);
            if (empty($banners)) {
                Flash::error(__('lang.messages.not_found', ['model' => __('models/banners.singular')]));
                return redirect(route('admin.banners.index'));
            }
            foreach($banners as $banner){
                $this->bannerRepository->delete($banner->id);
            }
            Flash::success(__('lang.messages.deleted', ['model' => __('models/banners.singular')]));
            return redirect(route('admin.banners.index'));
        }else{
            Flash::error(__('lang.messages.no_data_selected', ['model' => __('models/banners.singular')]));
            return redirect(route('admin.banners.index'));
        }
        
    }
}
