<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\AppBaseController;
use App\Http\Controllers\Controller;
use App\Models\Payment;
use App\Models\PaymentRequests;
use App\Models\PaymentType;
use App\Models\User;
use Illuminate\Http\Request;
use DB;
/**
 * Class RazorpayAPIController
 * @package App\Http\Controllers\API
 * @group RazorpayPayment
 */
class RazorpayAPIController extends AppBaseController
{
    public function __construct()
    {
        $this->middleware('auth:sanctum');
    }

    /**
     * Create Order
     *
     * @authenticated
     * @bodyParam amount integer required Amount to be paid.
     * @bodyParam payment_type_id integer required Payment Type Id.
     * @bodyParam remarks string nullable Mention purpose of the payment i.e ORDER_PAYMENT, WALLET_RECHARGE etc.
     */
    public function createOrder(Request $request){
     
        $request->validate([
            'amount' => 'required|numeric',
            'payment_type_id' => 'required|numeric',
            'remarks' => 'required|string',
        ]);
        
    
        $user = auth()->user();
      
        try {
            $paymentMethod = PaymentType::find($request->payment_type_id);
             
            $razor_key =   DB::table('settings')->where('key_name','razorpay_key')->first();
    
            $razor_secret = DB::table('settings')->where('key_name','razorpay_secret')->first();
             $key = $razor_key->key_value;
             $secret = $razor_secret->key_value;
            // $key = $paymentMethod->payment_key;
            // $secret = $paymentMethod->payment_secret;
             
            $api = new \Razorpay\Api\Api($key, $secret);
        //   dd($api,$key,$secret);
            $receipt_id = \Helpers::invoice_id();
            $orderData = [
                'receipt'         => $receipt_id,
                'amount'          => $request->amount,
                'currency'        => 'INR',
            ];
           // dd($orderData);
            $order = $api->order->create($orderData);
           // dd($order);
            PaymentRequests::create([
                'invoice_id' => $receipt_id,
                'user_id' => $user->id,
                'user_type' => get_class($user),
                'order_id' => null,
                'remarks' => $request->remarks,
                'amount' => $request->amount,
                'gateway' => 'RazorPay',
                'gateway_order_id' => $order->id,
                'gateway_response' => json_encode($order->toArray()),
            ]);
            return $this->sendResponse($order->toArray(), 'Razorpay order created successfully.');
        }catch (\Exception $e) {
            return $this->sendError($e->getMessage(),500);
        }
    }

    /**
     * Get/Capture Order
     *
     * @authenticated
     * @bodyParam payment_id string required Razorpay payment id.
     * @bodyParam payment_type_id integer required Payment Type Id.
     * @bodyParam remarks string nullable Mention purpose of the payment i.e ORDER_PAYMENT, WALLET_RECHARGE etc.
     * @return \Illuminate\Http\Response
     */

    public function getPayment(Request $req){
    //dd(request);
        $req->validate([
            'payment_id' => 'required|string',
            'payment_type_id' => 'required|numeric',
        ]);
        try{
            $user = auth()->user();
            $paymentMethod = PaymentType::find($req->payment_type_id);
            $key = $paymentMethod->payment_key;
            //dd($key);
            $secret = $paymentMethod->payment_secret;
            $api = new \Razorpay\Api\Api($key, $secret);
          // dd($api);
            $response = $api->payment->fetch($req->payment_id);
           //dd($response);
            if ($response->captured == false){
                $response = $api->payment->fetch($req->payment_id)->capture(array('amount'=> $response->amount ,'currency' => 'INR'));
            }
            if($response->captured){
                $paymentReq = PaymentRequests::where('gateway_order_id',$response->order_id)->first();
               // dd($paymentReq);
                $payment = Payment::create([
                    'ref_no' => $response->id,
                    'user_id' => $user->id,
                    'user_type' => get_class($user),
                    'payment_request_id' => $paymentReq->id,
                    'remarks' => $paymentReq->remarks,
                    'payment_method' => "ONLINE",
                    'amount' => $response->amount,
                    'gateway_response' => $response->toArray(),
                ]);
                return $this->sendResponse($payment, 'Payment loaded successfully.');
            }else{
                return $this->sendError('Payment could not be captured.',500);
            }
        }catch(\Exception $e){
            return $this->sendError($e->getMessage(),500);
        }
    }

    public function testPayment($payment_id){
        $paymentMethod = PaymentType::find(1);
        $key = $paymentMethod->payment_key;
        $secret = $paymentMethod->payment_secret;
        $api = new \Razorpay\Api\Api($key, $secret);
        $response = $api->payment->fetch($payment_id);
        $paymentReq = PaymentRequests::where('gateway_order_id',$response->order_id)->first();
        $payment = Payment::create([
            'ref_no' => $response->id,
            'user_id' => 1,
            'user_type' => get_class(User::find(1)),
            'payment_request_id' => $paymentReq->id,
            'remarks' => $paymentReq->remarks,
            'payment_method' => "ONLINE",
            'amount' => $response->amount,
            'gateway_response' => $response->toArray(),
        ]);
        return $this->sendResponse($payment, 'Payment loaded successfully.');
    }
     public function Webhook(Request $request)
    {
        //dd($request);
        $payload = $request->getContent();
        //dd($payload);
        $signature = $request->header('Razorpay-Signature');
    
        $webhookSecret = config('services.razorpay.webhook_secret');
        $expectedSignature = hash_hmac('sha256', $payload, $webhookSecret);
    
        if ($signature !== $expectedSignature) {
            return response()->json(['error' => 'Invalid webhook signature'], 400);
        }
    
        $event = json_decode($payload, true);
        $eventType = $event['event'];
    
        switch ($eventType) {
            case 'payment.captured':
                // Handle captured payment event
                $this->handleCapturedPayment($event);
                break;
            case 'payment.failed':
                // Handle failed payment event
                $this->handleFailedPayment($event);
                break;
            // Add more cases to handle other types of events
            default:
                // Ignore other events
                break;
        }
    
        return response()->json(['success' => true]);
    }
    
    protected function handleCapturedPayment(array $event)
    {
        $paymentId = $event['payload']['payment']['entity']['id'];
        $payment = Payment::where('ref_no', $paymentId)->first();
    
        if ($payment) {
            // Payment has already been processed, do nothing
            return;
        }
    
        // Process the payment
        $amount = $event['payload']['payment']['entity']['amount'] / 100; // convert amount to rupees
        $refNo = $event['payload']['payment']['entity']['order_id'];
        $payment = new Payment();
        $payment->ref_no = $refNo;
        $payment->amount = $amount;
        $payment->status = 'captured';
        $payment->save();
    
        // Send payment confirmation email to customer
        Mail::to($event['payload']['payment']['entity']['email'])->send(new PaymentConfirmationEmail($amount));
    }
    
    protected function handleFailedPayment(array $event)
    {
        $paymentId = $event['payload']['payment']['entity']['id'];
        $payment = Payment::where('ref_no', $paymentId)->first();
    
        if ($payment) {
            // Update payment status to failed
            $payment->status = 'failed';
            $payment->save();
        }
    }
    
}
