<?php

namespace App\Http\Controllers\API;

use App\Criteria\Driver\NearCriteria;
use App\Criteria\LimitOffsetCriteria;
use App\Criteria\CheckActiveCriteria;
use App\Events\Sockets\FindCabDrivers;
use App\Http\Requests\API\CreateDriverAPIRequest;
use App\Http\Requests\API\UpdateDriverAPIRequest;
use App\Mail\ExportGenerated;
use App\Models\Driver;
use App\Models\DriverKyc;
use App\Models\Order;
use App\Helpers\WalletHelper;
use App\Models\DriverDutySession;
use App\Models\Earning;
use App\Models\ServiceOrder;
use App\Notifications\OrderAssigned;
use App\Notifications\PushNotification;
use App\Repositories\DriverRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Illuminate\Support\Facades\Hash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use Notification;

/**
 * Class DriverController
 * @package App\Http\Controllers\API
 * @group Driver
 */

class DriverAPIController extends AppBaseController
{
    /** @var  DriverRepository */
    private $driverRepository;

    public function __construct(DriverRepository $driverRepo)
    {
        $this->driverRepository = $driverRepo;
    }

    /**
     * Display a listing of the active, not busy and not on duty Driver.
     * GET|HEAD /drivers
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->driverRepository->pushCriteria(new RequestCriteria($request));
        $this->driverRepository->pushCriteria(new LimitOffsetCriteria($request));
        $this->driverRepository->pushCriteria(new NearCriteria($request));
        $drivers = $this->driverRepository->where('active', 1)->where('is_busy',0)->where('on_duty',1)->get();
        // $this->driverRepository->pushCriteria(new CheckActiveCriteria($request));
       
     
        if ($request->assignable == 1){
            foreach ($drivers as $driver) {
                $append = "";
                $disabled = false;
                if ($driver->is_busy) {
                    $append = " (Busy)";
                    $disabled = true;
                }
                if ($driver->on_duty == 0) {
                    $append = " (Off Duty)";
                    $disabled = true;
                }
                if ($driver->cash_in_hand_overflow) {
                    $append = " (BootCash)";
                    $disabled = true;
                }
                if ($driver->active == 0) {
                    $append = " (InActive)";
                    $disabled = true;
                }
                if ($driver->is_verified == 0){
                    $append = " (Not Verified)";
                    $disabled = true;
                }
                $driver->name = $driver->name . ' (' . $driver->mobile. ')'.$append;
                $driver->disabled = $disabled;
            }
        }
        return $this->sendResponse(
            $drivers->toArray(),
            __('lang.messages.retrieved', ['model' => __('models/drivers.plural')])
        );
    }

    /**
     * Display a listing of all the Driver.
     * GET|HEAD /drivers
     *
     * @param Request $request
     * @return Response
     */
    public function get_all_driver(Request $request)
    {
        $this->driverRepository->pushCriteria(new RequestCriteria($request));
        $this->driverRepository->pushCriteria(new LimitOffsetCriteria($request));
        $this->driverRepository->pushCriteria(new NearCriteria($request));
        $drivers = $this->driverRepository->where('active', 1)->get();
        return $this->sendResponse(
            $drivers->toArray(),
            __('lang.messages.retrieved', ['model' => __('models/drivers.plural')])
        );
    }

    public function getHomeServiceDrivers(Request $request){
        $order = ServiceOrder::find($request->order_id);
        $drivers = \Helpers::getDriversByHomeServiceCategoryId($order->service_category_id,$order->zone_id);
      
//        foreach ($drivers as $driver) {
//            $append = "";
//            $disabled = false;
//            if ($driver->is_busy) {
//                $append = " (Busy)";
//                $disabled = true;
//            }
//            if ($driver->on_duty == 0) {
//                $append = " (Off Duty)";
//                $disabled = true;
//            }
//            if ($driver->cash_in_hand_overflow) {
//                $append = " (BootCash)";
//                $disabled = true;
//            }
//            if ($driver->active == 0) {
//                $append = " (InActive)";
//                $disabled = true;
//            }
//            $driver->name = $driver->name . ' (' . $driver->mobile. ')'.$append;
//            $driver->disabled = $disabled;
//        }
        return $this->sendResponse(
            $drivers,
            __('lang.messages.retrieved', ['model' => __('models/drivers.plural')])
        );
    }

    /**
     * Store a newly created Driver in storage.
     * POST /drivers
     *
     * @param CreateDriverAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateDriverAPIRequest $request)
    {
        $input = $request->all();
        $upload=[];
        if (Driver::where('mobile',$input['mobile'])->exists()){
            $this->sendError('Driver with this mobile number already exists',422);
        }
        if ($request->hasFile('vehicle_img')){
            $input['vehicle_img'] = \Helpers::uploadImage($request->file('vehicle_img'), 'drivers');
            $upload1 = array('url' => $input['vehicle_img'] ,'type'=>'vehicle_img',  );
            array_push($upload,$upload1);
        }

        if ($request->hasFile('dl_photo')){
            $input['dl_photo'] = \Helpers::uploadImage($request->file('dl_photo'), 'drivers');
            $upload1 = array('url' => $input['dl_photo'] ,'type'=>'dl_photo', );
            array_push($upload,$upload1);
        }

        if ($request->hasFile('aadhar_photo')){
            $input['aadhar_photo'] = \Helpers::uploadImage($request->file('aadhar_photo'), 'drivers');
            $upload1 = array('url' => $input['aadhar_photo'] ,'type'=>'aadhar_photo', );
            array_push($upload,$upload1);
        }

        if ($request->hasFile('aadhar_photo_back')){
            $input['aadhar_photo_back'] = \Helpers::uploadImage($request->file('aadhar_photo_back'), 'drivers');
            $upload1 = array('url' => $input['aadhar_photo_back'] ,'type'=>'aadhar_photo_back', );
            array_push($upload,$upload1);
        }

        if ($request->hasFile('profile_img')){
            $input['profile_img'] = \Helpers::uploadImage($request->file('profile_img'), 'drivers');
        }

        if ($request->has('password')){
            $input['password'] = Hash::make($request->password);
        }

        $driver = $this->driverRepository->create($input);
        $this->upload_driver_kyc($driver->id,$upload);

        return $this->sendResponse(
            $driver->toArray(),
            __('lang.messages.saved', ['model' => __('models/drivers.singular')])
        );
    }


   public function upload_driver_kyc($driver_id,$input)
   {
       foreach ($input as $key => $value) {
            DriverKyc::create([
                'driver_id' => $driver_id,
                'doc_type' =>$value['type'],
                'photo' =>$value['url'],
                'status' => 'UPLOADED',
            ]);
       }
   }

    public function get_pending_orders(Request $request){
        $driver = auth()->user();
        try {
            $count = Order::where('driver_id',$driver->id)->whereIn('order_status',['ASSIGNED','PREPARED','REACHED','PICKEDUP'])->count();
            if ($count > 0){
                Driver::where('id',$driver->id)->update(['is_busy'=>0]);
            }
            if($request->status) {
                $data = Order::with('store','user','payment','driver','orderType','service_category','status_timestamps','items')->where('driver_id', $driver->id)->where('order_status',$request->status)->orderBy('id', 'desc')->paginate(10);
                return $this->sendResponse($data->toArray(),$request->status.' Order fetched successfully');
            }
            $driver_id = $driver->id;
            $data = Order::with('store','user','payment','driver','orderType','service_category','status_timestamps','items')->where('driver_id', $driver_id)->whereNotIn('order_status',['PLACED', 'ACCEPTED','DRIVERREJECTED','DELIVERED','USERCANCELLED','CANCELLED'])->orderBy('id', 'desc')->paginate(10);
            return $this->sendResponse($data->toArray(),'Pending Order fetched successfully');
        }catch (\Exception $e) {
            return $this->sendError('Error: '.$e->getMessage());
        }
    }

    public function get_order_stats(Request $request){
        $driver = auth()->user();
        try {
            $driver_id = $driver->id;
            $data['pending'] = Order::where('driver_id', $driver_id)->whereNotIn('order_status',['PLACED', 'ACCEPTED','DRIVERREJECTED','DELIVERED','USERCANCELLED','CANCELLED'])->count();
            $data['completed'] = Order::where('driver_id', $driver_id)->where('order_status','DELIVERED')->count();
            $data['upcoming_service_order'] = ServiceOrder::where('driver_id', $driver_id)->whereIn('order_status',['ASSIGNED','ACCEPTED','REACHED','STARTED','FINISHED'])->count();
            $data['completed_service_order'] = ServiceOrder::where('driver_id', $driver_id)->where('order_status',['COMPLETED'])->count();
            return $this->sendResponse($data,'Order Stats fetched successfully');
        }catch (\Exception $e) {
            return $this->sendError('Error: '.$e->getMessage());
        }
    }

    public function get_earnings(Request $request){
        $request->validate([
            'start_date' => 'required',
            'end_date' => 'required',
        ]);
        $driver = auth()->user();
        try {
            $startDate = Carbon::parse($request->start_date)->format("Y-m-d");
            $endDate = Carbon::parse($request->end_date)->addDays(1)->format("Y-m-d");
            $type = 'App\\Models\\Driver';
            $data = Earning::where('entity_type', $type)->where('entity_id', $driver->id)->whereBetween('created_at', [$startDate, $endDate])->get()->toArray();
            $sum = Earning::where('entity_type', $type)->where('entity_id', $driver->id)->whereBetween('created_at', [$startDate, $endDate])->sum('amount');
            return $this->sendResponse(['earnings'=>$sum,'transactions'=>$data],'Report generated from '.$startDate.' to '.$endDate);
        }catch (\Exception $e) {
            return $this->sendError('Error: '.$e->getMessage());
        }
    }

    //Api Driver wallet Recharge by Razorpay ....
    public function wallet_recharge(Request $request){
        $request->validate([
            'payment_type_id' => 'required',
            'amount' => 'required',
        ]);
        $driver = auth()->user();
        try {
            $remarks="Wallet Recharge by Razorpay ,with payment_type_id: ".$request->payment_type_id ;
            $res = WalletHelper::creditDriverWallet($driver->id, $request->amount, $remarks);
            return $this->sendResponse(['data'=>'success'],'Wallet Recharge Success amount '.$request->amount);
        }catch (\Exception $e) {
            return $this->sendError('Error: '.$e->getMessage());
        }
    }

    /**
     * Display the specified Driver.
     * GET|HEAD /drivers/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Driver $driver */
        $driver = $this->driverRepository->find($id);

        if (empty($driver)) {
            return $this->sendError(
                __('lang.messages.not_found', ['model' => __('models/drivers.singular')])
            );
        }

        return $this->sendResponse(
            $driver->toArray(),
            __('lang.messages.retrieved', ['model' => __('models/drivers.singular')])
        );
    }

    /**
     * Update the specified Driver in storage.
     * PUT/PATCH /drivers/{id}
     *
     * @param int $id
     * @param UpdateDriverAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateDriverAPIRequest $request)
    {
        $input = $request->all();

        /** @var Driver $driver */
        $driver = $this->driverRepository->find($id);

        if (empty($driver)) {
            return $this->sendError(
                __('lang.messages.not_found', ['model' => __('models/drivers.singular')])
            );
        }

        if ($request->hasFile('vehicle_img')){
            $input['vehicle_img'] = \Helpers::uploadImage($request->file('vehicle_img'), 'drivers');
        }

        if ($request->hasFile('dl_photo')){
            $input['dl_photo'] = \Helpers::uploadImage($request->file('dl_photo'), 'drivers');
        }

        if ($request->hasFile('aadhar_photo')){
            $input['aadhar_photo'] = \Helpers::uploadImage($request->file('aadhar_photo'), 'drivers');
        }

        if ($request->hasFile('aadhar_photo_back')){
            $input['aadhar_photo_back'] = \Helpers::uploadImage($request->file('aadhar_photo_back'), 'drivers');
        }

        if ($request->hasFile('profile_img')){
            $input['profile_img'] = \Helpers::uploadImage($request->file('profile_img'), 'drivers');
        }

        if ($request->has('password')){
            $input['password'] = Hash::make($request->password);
        }

        $driver = $this->driverRepository->update($input, $id);

        return $this->sendResponse(
            $driver->toArray(),
            __('lang.messages.updated', ['model' => __('models/drivers.singular')])
        );
    }

    public function sendCabOrderNotification(Request $request){
        $request->validate([
            'driver_id' => 'required',
            'order_id' => 'required',
        ]);
        $driver = Driver::find($request->driver_id);
        if (!empty($driver)){
            $order = Order::find($request->order_id);
            if (empty($order)){
                return $this->sendError("Order not found");
            }
            Notification::send($driver, new OrderAssigned("Action required! New Order",$order,true));
            event(new FindCabDrivers($driver->id,$order,"SENT"));
            return $this->sendResponse(null,'Notification Sent Successfully');
        }else{
            return $this->sendError('Driver not found');
        }
    }

    /**
     * Remove the specified Driver from storage.
     * DELETE /drivers/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Driver $driver */
        $driver = $this->driverRepository->find($id);

        if (empty($driver)) {
            return $this->sendError(
                __('lang.messages.not_found', ['model' => __('models/drivers.singular')])
            );
        }

        $driver->delete();

        return $this->sendResponse(
            $id,
            __('lang.messages.deleted', ['model' => __('models/drivers.singular')])
        );
    }
}
