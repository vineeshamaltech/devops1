<?php

namespace App\Http\Controllers\API;

use App\Helpers\WalletHelper;
use App\Http\Requests\API\CreateUserAPIRequest;
use App\Http\Requests\API\UpdateUserAPIRequest;
use App\Models\Payment;
use App\Models\User;
use App\Repositories\UserRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Illuminate\Support\Facades\Hash;
use Kreait\Firebase\Auth;
use Response;

/**
 * Class UserController
 * @package App\Http\Controllers\API
 * @group User
 */

class UserAPIController extends AppBaseController
{
    /** @var  UserRepository */
    private $userRepository;

    public function __construct(UserRepository $userRepo)
    {
        $this->userRepository = $userRepo;
    }

    public function index(Request $request)
    {
        $this->userRepository->pushCriteria(new RequestCriteria($request));
        $users = $this->userRepository->all();

        return $this->sendResponse(
            $users->toArray(),
            __('lang.messages.retrieved', ['model' => __('models/user.plural')])
        );
    }



    /**
     * Display the specified User.
     * GET|HEAD /users/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var User $user */
        $user = $this->userRepository->find($id);

        if (empty($user)) {
            return $this->sendError(
                __('lang.messages.not_found', ['model' => __('models/users.singular')])
            );
        }

        return $this->sendResponse(
            $user->toArray(),
            __('lang.messages.retrieved', ['model' => __('models/users.singular')])
        );
    }

    /**
     * Recharge wallet.
     *
     * @bodyParam payment_id required int Payment id. Example: 1
     * @return Response
     */
    public function rechargeWallet(Request $request)
    {
        $request->validate([
            'payment_id' => 'required',
        ]);
        try{
            $user = auth()->user();
            $payment = Payment::find($request->payment_id);
            WalletHelper::creditUserWallet($user->id,$payment->amount/100,'Wallet recharged via Razorpay. Ref no. '.$payment->ref_no);
            $user = User::find($user->id);
            return $this->sendResponse($user, 'Wallet recharged successfully');
        }catch (\Exception $e){
            return $this->sendError('Unable to verify payment');
        }
    }

    /**
     * Update the specified User in storage.
     * PUT/PATCH /users/{id}
     *
     * @param int $id
     * @param UpdateUserAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateUserAPIRequest $request)
    {
        $id = Auth::user()->id;
        $input = $request->all();
        /** @var User $user */
        $user = $this->userRepository->find($id);
        if (empty($user)) {
            return $this->sendError(
                __('lang.messages.not_found', ['model' => __('models/users.singular')])
            );
        }
        if(isset($input['password'])){
            $input['password'] = Hash::make($input['password']);
        }
        $user = $this->userRepository->update($input, $id);
        return $this->sendResponse(
            $user->toArray(),
            __('lang.messages.updated', ['model' => __('models/users.singular')])
        );
    }

}
