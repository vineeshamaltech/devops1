<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\AppBaseController;
use App\Http\Requests\API\UpdateUserAPIRequest;
use App\Models\Admin;
use App\Models\User;
use App\Notifications\SendOTP;
use Craftsys\Msg91\Facade\Msg91;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Str;

/**
 * Class CustomerAuthAPIController
 * @package App\Http\Controllers\API
 * @group Customer Authentication
 */

class CustomerAuthAPIController extends AppBaseController
{
    /**Login using mobile number and password
     *
     * @bodyParam email string required Email of the user.
     * @bodyParam password string required Password of the user.
     */
    public function login(Request $request)
    {
        $request->validate([
            'mobile' => 'required|string',
            'password' => 'required|string'
        ]);
        $credentials = request(['mobile', 'password']);
        if (!Auth::guard('user')->attempt($credentials)) {
            return $this->sendError( 'Incorrect email or password');
        }
        $user = auth('user')->user();
        if($user == null){
            return $this->sendError('User not found');
        }
        // $user->tokens()->delete();
        $user->token = $user->createToken('user_authentication_token')->plainTextToken;
        $user->token_type = 'Bearer';
        return $this->sendResponse($user, 'reg2 Successful');
    }

    /**
     * Register a new user
     *
     * @bodyParam  name string nullable Name of the user.
     * @bodyParam  mobile string required Phone No of the user in the format +919999999999. Always include the country code.
     * @bodyParam  email string nullable Email of the user.
     * @bodyParam  password string required Password of the user.
     *
     * @return Response
     */
    public function register(Request $request)
    {
        $request->validate([
            'name' => 'nullable|string',
            'mobile' => 'required|string|min:13|max:13',
            'email' => 'nullable|string',
            'password' => 'required|string'
        ]);
        //get country code from phone no
        $country_code = substr($request->mobile, 0, 3);
        $mobile = substr($request->mobile, 3);
        if(User::where('mobile', $mobile)->first()){
            return $this->sendError('Mobile number already exists');
        }
        if($request->has('email')&&User::where('email', $request->email)->first()){
            return $this->sendError('Email already exists');
        }
        $ref_code = \Helpers::randomString(6);
        if(empty($ref_user)){
            $request->ref_code = null;
        }
        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'country_code' => $country_code,
            'mobile' => $mobile,
            'password' => Hash::make($request->password),
            'ref_code' => $ref_code,
            'lat' => $request->lat,
            'long' => $request->long,
            'ref_by' => $ref_user->id ?? null
        ]);
        $user = User::find($user->id);
        $user->token = $user->createToken('user_authentication_token')->plainTextToken;
        $user->token_type = 'Bearer';
        return $this->createdResponse($user->toArray(), 'User created successfully');
    }

    /**
     * Login using phone number
     *
     * This will send a verification code to the user's phone number via MSG91. You can only send the is_voice parameter as true if you are resending the OTP.
     * @bodyParam  mobile string required Phone no of user in the format +919999999999
     * @return Response
     * */
    public function phoneLogin(Request $request)
    {
        $request->validate([
            'mobile' => 'required|string|min:13|max:13',
            'is_voice' => 'nullable|boolean',
            'otp' => 'nullable|string'
        ]);
        $country_code = substr($request->mobile, 0, 3);
        $mobile = substr($request->mobile, 3);
        $user = User::where('mobile', $mobile)->where('country_code',$country_code)->first();
      
        if (in_array($mobile, \Helpers::getTestPhoneNo())) {
            return $this->sendResponse(array('status_code'=>200), 'Test OTP Sent Successfully');
        }
        if($request->is_voice){
            $phone = str_replace('+', '', $request->mobile);
            $res = Msg91::otp()->to($phone)->viaVoice()->resend();
            if ($res->status_code == "200"){
                return $this->sendResponse($res, 'OTP Sent Successfully');
            }else{
                return $this->sendError('Error sending OTP');
            }
        }else{
            
            $phone = str_replace('+', '', $request->mobile);
            $res = Msg91::otp()->to($phone)->send();
          
            if ($res->status_code == "200"){
                return $this->sendResponse($res, 'OTP Sent Successfully');
            }else{
                return $this->sendError('Error sending OTP');
            }
        }
    }
    
     public function userRegister(Request $request){
        $request->validate([
            'mobile' => 'required|string|min:13|max:13',
            // 'otp' => 'required|string|min:4|max:4',
            'fcm_token' => 'nullable|string',
            'ref_code' => 'nullable|string',
            'lat' => 'nullable|string',
            'long' => 'nullable|string'
        ]);
        $country_code = substr($request->mobile, 0, 3);
        $mobile = substr($request->mobile, 3);
        $phone = str_replace('+', '', $request->mobile);

        if (in_array($mobile, \Helpers::getTestPhoneNo())) {
            $user = User::where('mobile', $mobile)->where('country_code',$country_code)->first();
            $ref_user = User::where('ref_code', $request->ref_code)->first();
            $ref_code = \Helpers::randomString(10);
            if ($request->ref_code == null){
                $ref_user = null;
            }
            if(empty($ref_user)){
                $request->ref_code = null;
            }

            if($user == null){
                $user = User::create([
                    'country_code' => $country_code,
                    'mobile' => $mobile,
                    'password' => Hash::make(Str::random(10)),
                    'fcm_token' => $request->fcm_token,
                    'ref_code' => $ref_code,
                    'ref_by' => $ref_user->id ?? null,
                    'lat' => $request->lat,
                    'long' => $request->long,
                ]);
                $user = User::find($user->id);
            }
            // $user->tokens()->delete();
            $user->token = $user->createToken('user_authentication_token')->plainTextToken;
            $user->token_type = 'Bearer';
            return $this->sendResponse($user, 'Test Registration Successful');
        }

        try{
           
                $user = User::where('mobile', $mobile)->where('country_code',$country_code)->first();
                $ref_user = User::where('ref_code', $request->ref_code)->first();
                $ref_code = \Helpers::randomString(10);
                if(empty($ref_user)){
                    $request->ref_code = null;
                }
                if($user == null){
                    $user = User::create([
                        'country_code' => $country_code,
                        'mobile' => $mobile,
                        'password' => Hash::make(Str::random(10)),
                        'fcm_token' => $request->fcm_token,
                        'ref_code' => $ref_code,
                        'ref_by' => $ref_user->id ?? null,
                        'lat' => $request->lat,
                        'long' => $request->long,
                    ]);
                    $user = User::find($user->id);
                }
                // $user->tokens()->delete();
                $user->token = $user->createToken('user_authentication_token')->plainTextToken;
                $user->token_type = 'Bearer';
                return $this->sendResponse($user, 'reg Successful');

        }catch (\Exception $e){
            return $this->sendError($e->getMessage());
        }

    }






    /**
     * Verify Phone No
     *
     * @bodyParam  mobile string required Phone no of user
     * @bodyParam  otp string required OTP sent on user's phone no
     * @return Response
     * */
    public function verifyOtp(Request $request){
        $request->validate([
            'mobile' => 'required|string|min:13|max:13',
            'otp' => 'required|string|min:4|max:4',
            'fcm_token' => 'nullable|string',
            'ref_code' => 'nullable|string'
        ]);
        $country_code = substr($request->mobile, 0, 3);
        $mobile = substr($request->mobile, 3);
        $phone = str_replace('+', '', $request->mobile);
        if (in_array($mobile, \Helpers::getTestPhoneNo())) {
            $user = User::where('mobile', $mobile)->where('country_code',$country_code)->first();
            $ref_user = User::where('ref_code', $request->ref_code)->first();
            $ref_code = \Helpers::randomString(10);
            if ($request->ref_code == null){
                $ref_user = null;
            }
            if(empty($ref_user)){
                $request->ref_code = null;
            }

            if($user == null){
                $user = User::create([
                    'country_code' => $country_code,
                    'mobile' => $mobile,
                    'password' => Hash::make(Str::random(10)),
                    'fcm_token' => $request->fcm_token,
                    'ref_code' => $ref_code,
                    'ref_by' => $ref_user->id ?? null
                ]);
                $user = User::find($user->id);
            }
            // $user->tokens()->delete();
            $user->token = $user->createToken('user_authentication_token')->plainTextToken;
            $user->token_type = 'Bearer';
            return $this->sendResponse($user, 'Test Registration Successful');
        }

        try{
            $res = Msg91::otp((int)$request->otp)->to($phone)->verify();
            if($res->status_code == "200"){
                $user = User::where('mobile', $mobile)->where('country_code',$country_code)->first();
                $ref_user = User::where('ref_code', $request->ref_code)->first();
                $ref_code = \Helpers::randomString(10);
                if(empty($ref_user)){
                    $request->ref_code = null;
                }
                if($user == null){
                    $user = User::create([
                        'country_code' => $country_code,
                        'mobile' => $mobile,
                        'password' => Hash::make(Str::random(10)),
                        'fcm_token' => $request->fcm_token,
                        'ref_code' => $ref_code,
                        'ref_by' => $ref_user->id ?? null
                    ]);
                    $user = User::find($user->id);
                }
                // $user->tokens()->delete();
                $user->token = $user->createToken('user_authentication_token')->plainTextToken;
                $user->token_type = 'Bearer';
                return $this->sendResponse($user, 'reg1 Successful');
            }else{
                return $this->sendError('OTP is not valid');
            }
        }catch (\Exception $e){
            return $this->sendError($e->getMessage());
        }

    }

    /**
     * User's Profile
     *
     * Just pass the Bearer token in the header
     * @authenticated
     * @return Response
     * */
    public function userprofile(Request $request){
        $user = auth()->user();
        if ($user != null) {
            return $this->sendResponse($user->toArray(), 'User profile fetched successfully');
        }else{
            return $this->sendError('Unauthenticated');
        }
    }

    /**
     * Update User.
     * PUT/PATCH /users/{id}
     *
     * @authenticated
     * @param int $id
     * @param UpdateUserAPIRequest $request
     * @return Response
     */
    public function updateProfile(UpdateUserAPIRequest $request)
    {
        $id = Auth::user()->id;
        $input = $request->all();
        /** @var User $user */
        $user = User::find($id);
        if (empty($user)) {
            return $this->sendError(
                __('lang.messages.not_found', ['model' => __('models/users.singular')])
            );
        }
        $country_code = substr($request->mobile, 0, 3);
        $mobile = substr($request->mobile, 3);
        if (User::where('mobile', $mobile)->where('country_code', $country_code)->where('id','!=',$id)->exists()) {
            return $this->sendError('Mobile Number already exists');
        }

        if (!empty($input['email']) && User::where('email', $input['email'])->where('id','!=',$id)->exists()) {
            return $this->sendError('Email already exists');
        }
        if (isset($input['mobile']))
            unset($input['mobile']);
        if(isset($input['password'])){
            $input['password'] = Hash::make($input['password']);
        }
        User::where('id',$id)->update($input);
        $user = User::find($id);
        return $this->sendResponse(
            $user,
            __('lang.messages.updated', ['model' => __('models/users.singular')])
        );
    }

    public function changePassword(Request $request){
        $validatedData = $request->validate([
            'mobile' => 'required|string|min:13|max:13',
            'password' => 'required|string|max:255',
        ]);
        $user = User::where('mobile',$validatedData['mobile'])->first();

        if($user!=null){
            $user->update(['password' => Hash::make($validatedData['password']),]);
            return $this->sendResponse($user, 'Password reset successful');
        }else{
            return $this->sendError('Invalid OTP');
        }
    }

}
