<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateProductVariantAPIRequest;
use App\Http\Requests\API\UpdateProductVariantAPIRequest;
use App\Models\ProductVariant;
use App\Repositories\ProductVariantRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class ProductVariantController
 * @package App\Http\Controllers\API
 * @group Product Variants
 */

class ProductVariantAPIController extends AppBaseController
{
    /** @var  ProductVariantRepository */
    private $productVariantRepository;

    public function __construct(ProductVariantRepository $productVariantRepo)
    {
        $this->productVariantRepository = $productVariantRepo;
    }

    /**
     * Get
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->productVariantRepository->pushCriteria(new RequestCriteria($request));
        $productVariants = $this->productVariantRepository->all();

        return $this->sendResponse(
            $productVariants->toArray(),
            __('lang.messages.retrieved', ['model' => __('models/productVariants.plural')])
        );
    }

    /**
     * Create
     *
     * @param CreateProductVariantAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateProductVariantAPIRequest $request)
    {
        $input = $request->all();
        if($request->has('image')){
            $input['image'] = \Helpers::uploadImage($request->image, 'product_variants');
        }
        $productVariant = $this->productVariantRepository->create($input);
        return $this->sendResponse(
            $productVariant->toArray(),
            __('lang.messages.saved', ['model' => __('models/productVariants.singular')])
        );
    }

    /**
     * Get Variant by ID
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var ProductVariant $productVariant */
        $productVariant = $this->productVariantRepository->find($id);

        if (empty($productVariant)) {
            return $this->sendError(
                __('lang.messages.not_found', ['model' => __('models/productVariants.singular')])
            );
        }

        return $this->sendResponse(
            $productVariant->toArray(),
            __('lang.messages.retrieved', ['model' => __('models/productVariants.singular')])
        );
    }

    /**
     * Update Variant.
     * PUT/PATCH /productVariants/{id}
     *
     * @param int $id
     * @param UpdateProductVariantAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateProductVariantAPIRequest $request)
    {
        $input = $request->all();

        /** @var ProductVariant $productVariant */
        $productVariant = $this->productVariantRepository->find($id);

        if (empty($productVariant)) {
            return $this->sendError(
                __('lang.messages.not_found', ['model' => __('models/productVariants.singular')])
            );
        }

        if($request->has('image')){
            $input['image'] = \Helpers::uploadImage($request->image, 'product_variants');
        }

        $productVariant = $this->productVariantRepository->update($input, $id);

        return $this->sendResponse(
            $productVariant->toArray(),
            __('lang.messages.updated', ['model' => __('models/productVariants.singular')])
        );
    }

    /**
     * Delete a product variant
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var ProductVariant $productVariant */
        $productVariant = $this->productVariantRepository->find($id);

        if (empty($productVariant)) {
            return $this->sendError(
                __('lang.messages.not_found', ['model' => __('models/productVariants.singular')])
            );
        }
        $productVariant->delete();
        return $this->sendResponse(
            $id,
            __('lang.messages.deleted', ['model' => __('models/productVariants.singular')])
        );
    }
}
