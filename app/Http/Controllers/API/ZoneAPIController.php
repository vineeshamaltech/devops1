<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;

class ZoneAPIController extends AppBaseController
{
    public function index(Request $request){
        $request->validate([
           'lat' => 'required',
           'long' => 'required',
        ]);
        $zones = \Helpers::findInZone($request->lat, $request->long);
        return $this->sendResponse($zones, 'Zones retrieved successfully.');
    }
}
