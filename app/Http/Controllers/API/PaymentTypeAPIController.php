<?php

namespace App\Http\Controllers\API;

use App\Criteria\ActiveCriteria;
use App\Http\Requests\API\CreatePaymentTypeAPIRequest;
use App\Http\Requests\API\UpdatePaymentTypeAPIRequest;
use App\Models\PaymentType;
use App\Models\ServiceCategory;
use App\Models\ServiceCategoryPaymentMethod;
use App\Models\Store;
use App\Models\HomeServiceCategory;
use App\Repositories\PaymentTypeRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use DB;
/**
 * Class PaymentTypeController
 * @package App\Http\Controllers\API
 * @group PaymentTypes
 */

class PaymentTypeAPIController extends AppBaseController
{
    /** @var  PaymentTypeRepository */
    private $paymentTypeRepository;

    public function __construct(PaymentTypeRepository $paymentTypeRepo)
    {
        $this->paymentTypeRepository = $paymentTypeRepo;
    }

    /**
     * Display a listing of the PaymentType.
     * GET|HEAD /paymentTypes
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
      
        $paymentTypes = $this->paymentTypeRepository->all();
        
        if($request->has('service')){
             $service_id = $request->service_id;
           $service_cat =  HomeServiceCategory::where('id',$service_id)->first();
               $payment =  $service_cat['payment_method'];
                $payment = explode(',', $payment);
                foreach ($payment as $key => $value) {
                    $payment[$key] =DB::table('payment_methods')->where('id',$value)->first();
                }
                 return $this->sendResponse(
            $payment,
            __('lang.messages.retrieved', ['model' => __('models/paymentTypes.plural')])
        );
           
        }


        if ($request->has('store_id')&& $request->store_id != 'null'&& $request->store_id!=null){
           
            $store = Store::findOrFail($request->store_id);
            $payment_ids = explode(',',$store->payment_methods);
            $paymentTypes = PaymentType::whereIn('id',$payment_ids)->get();
        }else if ($request->has('service_category_id')&&$request->service_category_id!=null){
           
            $service_category = ServiceCategoryPaymentMethod::where('service_category_id',$request->service_category_id)->get()->pluck('payment_type_id');
            $paymentTypes = PaymentType::whereIn('id',$service_category)->get();
        }

        return $this->sendResponse(
            $paymentTypes,
            __('lang.messages.retrieved', ['model' => __('models/paymentTypes.plural')])
        );
    }


}
