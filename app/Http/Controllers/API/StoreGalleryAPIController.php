<?php

namespace App\Http\Controllers\API;

use App\Criteria\ActiveCriteria;
use App\Http\Requests\API\CreateStoreGalleryAPIRequest;
use App\Http\Requests\API\UpdateStoreGalleryAPIRequest;
use App\Models\StoreGallery;
use App\Repositories\StoreGalleryRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class StoreGalleryController
 * @package App\Http\Controllers\API
 */

class StoreGalleryAPIController extends AppBaseController
{
    /** @var  StoreGalleryRepository */
    private $storeGalleryRepository;

    public function __construct(StoreGalleryRepository $storeGalleryRepo)
    {
        $this->storeGalleryRepository = $storeGalleryRepo;
    }

    /**
     * Display a listing of the StoreGallery.
     * GET|HEAD /storeGalleries
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->storeGalleryRepository->pushCriteria(new RequestCriteria($request));
        $storeGalleries = $this->storeGalleryRepository->all();

        return $this->sendResponse(
            $storeGalleries->toArray(),
            __('lang.messages.retrieved', ['model' => __('models/storeGalleries.plural')])
        );
    }

    /**
     * Store a newly created StoreGallery in storage.
     * POST /storeGalleries
     *
     * @param CreateStoreGalleryAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateStoreGalleryAPIRequest $request)
    {
        $input = $request->all();

        $storeGallery = $this->storeGalleryRepository->create($input);

        return $this->sendResponse(
            $storeGallery->toArray(),
            __('lang.messages.saved', ['model' => __('models/storeGalleries.singular')])
        );
    }

    /**
     * Display the specified StoreGallery.
     * GET|HEAD /storeGalleries/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var StoreGallery $storeGallery */
        $storeGallery = $this->storeGalleryRepository->find($id);

        if (empty($storeGallery)) {
            return $this->sendError(
                __('lang.messages.not_found', ['model' => __('models/storeGalleries.singular')])
            );
        }

        return $this->sendResponse(
            $storeGallery->toArray(),
            __('lang.messages.retrieved', ['model' => __('models/storeGalleries.singular')])
        );
    }

    /**
     * Update the specified StoreGallery in storage.
     * PUT/PATCH /storeGalleries/{id}
     *
     * @param int $id
     * @param UpdateStoreGalleryAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateStoreGalleryAPIRequest $request)
    {
        $input = $request->all();

        /** @var StoreGallery $storeGallery */
        $storeGallery = $this->storeGalleryRepository->find($id);

        if (empty($storeGallery)) {
            return $this->sendError(
                __('lang.messages.not_found', ['model' => __('models/storeGalleries.singular')])
            );
        }

        $storeGallery = $this->storeGalleryRepository->update($input, $id);

        return $this->sendResponse(
            $storeGallery->toArray(),
            __('lang.messages.updated', ['model' => __('models/storeGalleries.singular')])
        );
    }

    /**
     * Remove the specified StoreGallery from storage.
     * DELETE /storeGalleries/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var StoreGallery $storeGallery */
        $storeGallery = $this->storeGalleryRepository->find($id);

        if (empty($storeGallery)) {
            return $this->sendError(
                __('lang.messages.not_found', ['model' => __('models/storeGalleries.singular')])
            );
        }

        $storeGallery->delete();

        return $this->sendResponse(
            $id,
            __('lang.messages.deleted', ['model' => __('models/storeGalleries.singular')])
        );
    }
}
