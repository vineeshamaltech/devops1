<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateDriverWalletTransactionAPIRequest;
use App\Http\Requests\API\UpdateDriverWalletTransactionAPIRequest;
use App\Models\DriverWalletTransaction;
use App\Repositories\DriverWalletTransactionRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use Illuminate\Http\Request;
use App\Models\User;
use App\Http\Controllers\AppBaseController;
use Illuminate\Support\Facades\Auth;
use Response;

/**
 * Class DriverWalletTransactionController
 * @package App\Http\Controllers\API
 */

class DriverWalletTransactionAPIController extends AppBaseController
{
    /** @var  DriverWalletTransactionRepository */
    private $driverWalletTransactionRepository;

    public function __construct(DriverWalletTransactionRepository $driverWalletTransactionRepo)
    {
        $this->driverWalletTransactionRepository = $driverWalletTransactionRepo;
    }

    /**
     * Display a listing of the DriverWalletTransaction.
     * GET|HEAD /driverWalletTransactions
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
       
       
        $this->driverWalletTransactionRepository->pushCriteria(new RequestCriteria($request));
        $driverWalletTransactions = $this->driverWalletTransactionRepository->all();

        return $this->sendResponse(
            $driverWalletTransactions->toArray(),
            __('lang.messages.retrieved', ['model' => __('models/driverWalletTransactions.plural')])
        );
    }

    /**
     * Store a newly created DriverWalletTransaction in storage.
     * POST /driverWalletTransactions
     *
     * @param CreateDriverWalletTransactionAPIRequest $request
     *
     * @return Response
     */
    
    public function store(CreateDriverWalletTransactionAPIRequest $request)
    {
        $input = $request->all();

        $driverWalletTransaction = $this->driverWalletTransactionRepository->create($input);

        return $this->sendResponse(
            $driverWalletTransaction->toArray(),
            __('lang.messages.saved', ['model' => __('models/driverWalletTransactions.singular')])
        );
    }

    /**
     * Display the specified DriverWalletTransaction.
     * GET|HEAD /driverWalletTransactions/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var DriverWalletTransaction $driverWalletTransaction */
        $driverWalletTransaction = $this->driverWalletTransactionRepository->find($id);

        if (empty($driverWalletTransaction)) {
            return $this->sendError(
                __('lang.messages.not_found', ['model' => __('models/driverWalletTransactions.singular')])
            );
        }

        return $this->sendResponse(
            $driverWalletTransaction->toArray(),
            __('lang.messages.retrieved', ['model' => __('models/driverWalletTransactions.singular')])
        );
    }

    /**
     * Update the specified DriverWalletTransaction in storage.
     * PUT/PATCH /driverWalletTransactions/{id}
     *
     * @param int $id
     * @param UpdateDriverWalletTransactionAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateDriverWalletTransactionAPIRequest $request)
    {
        $input = $request->all();

        /** @var DriverWalletTransaction $driverWalletTransaction */
        $driverWalletTransaction = $this->driverWalletTransactionRepository->find($id);

        if (empty($driverWalletTransaction)) {
            return $this->sendError(
                __('lang.messages.not_found', ['model' => __('models/driverWalletTransactions.singular')])
            );
        }

        $driverWalletTransaction = $this->driverWalletTransactionRepository->update($input, $id);

        return $this->sendResponse(
            $driverWalletTransaction->toArray(),
            __('lang.messages.updated', ['model' => __('models/driverWalletTransactions.singular')])
        );
    }

    /**
     * Remove the specified DriverWalletTransaction from storage.
     * DELETE /driverWalletTransactions/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var DriverWalletTransaction $driverWalletTransaction */
        $driverWalletTransaction = $this->driverWalletTransactionRepository->find($id);

        if (empty($driverWalletTransaction)) {
            return $this->sendError(
                __('lang.messages.not_found', ['model' => __('models/driverWalletTransactions.singular')])
            );
        }

        $driverWalletTransaction->delete();

        return $this->sendResponse(
            $id,
            __('lang.messages.deleted', ['model' => __('models/driverWalletTransactions.singular')])
        );
    }
}
