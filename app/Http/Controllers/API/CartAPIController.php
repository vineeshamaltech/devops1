<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\AppBaseController;
use App\Models\Address;
use App\Models\CartItem;
use App\Models\Coupon;
use App\Models\CouponUsage;
use App\Models\DeliveryFee;
use App\Models\Product;
use App\Models\ProductVariant;
use App\Models\ServiceCategory;
use App\Models\Store;
use App\Services\CartService;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Models\Cart;
use App\Models\GlobalHelper;

/**
 * Class CartAPIController
 * @package App\Http\Controllers\API
 * @group Cart
 */
class CartAPIController extends AppBaseController
{

    private $cart_service;

    public function __construct(CartService $cart_service){
        $this->cart_service = $cart_service;
    }


    /**
     * Get Cart.
     *
     * @authenticated
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       
        $user = auth()->user();
        if (empty($user)) {
            return $this->sendError('Unauthenticated.',401);
        }
        $this->recalculateCart();
        $cart = Cart::with(['coupon','address','items','store','items.product','service_category'])->where('user_id', $user->id)->first();
        return $this->sendResponse($cart, 'Cart retrieved successfully');
    }

    /**
     * Add to Cart
     *
     * @authenticated
     * @bodyParam product_id int required The id of the product.
     * @bodyParam quantity int required The quantity of the product.
     * @return \Illuminate\Http\Response
     */
    public function addtocart(Request $request){
        $request->validate([
            'product_id' => 'required|integer',
            'quantity' => 'required|integer',
            'variant_ids' => 'nullable|array',
        ]);

        $user = auth()->user();
        if (empty($user)) {
            return $this->sendError('Unauthorized', 401);
        }
        $cart = Cart::where('user_id', $user->id)->first();
        $product = Product::where('id', $request->product_id)->first();
      
        if (empty($product)) {
            return $this->sendError('Product not found.', 404);
        }else if($product->active == "0"){
            return $this->sendError('Product not available.', 404);
        }
        if($product->sku < $request->quantity){
            return $this->sendError('Quantity not available.', 404);
        }
        $store = Store::select('name','service_category_id','mobile','address','latitude','longitude','is_open')->where('id', $product->store_id)->first();
        
        
        if (empty($store)) {
            return $this->sendError('Store not found.', 404);
        }
        // if ($store->is_open == false){
        //     return $this->sendError('Store is closed now.', 404);
        // }
        if(empty($cart)){
            $address = Address::select('id','name','phone','address','latitude','longitude','house_no','building')->where('user_id', $user->id)->where('is_default',1)->first();
            if (empty($address)) {
                return $this->sendError( 'Address not selected', 404);
            }
            $cart = new Cart();
            $cart->service_category_id = $store->service_category_id;
            $cart->user_id = $user->id;
            $cart->store_id = $product->store_id;
            $cart->pickup_address = json_decode($store->toJson());
            $cart->delivery_address = json_decode($address->toJson());
            $cart->address_id = $address->id;
            $cart->save();
            $data = $this->calculateDeliveryCharge();
            if ($data['error_code']!='0'){
                $cart = Cart::where('user_id', $user->id)->first();
                CartItem::where('cart_id', $cart->id)->delete();
                $cart->delete();
                return $this->sendError($data['error_message'], 404);
            }
        }else{
            if ($cart->store_id != $product->store_id) {
                return $this->sendError('You are adding product from another store. Please clear cart.', 404);
            }
        }
        //$variants =  implode(',', $request->variant_ids);
        if($product->product_type=="SIMPLE"){
            $variants = null;
        }elseif ($product->product_type=="VARIABLE"){
            if(count($request->variant_ids??[])<=0){
                //return $this->sendError('Please select at least one variant.', 422);
            }
            $variants = $request->variant_ids[0] ?? null;
        }elseif ($product->product_type=="CUSTOMIZABLE"){
            if(count($request->variant_ids??[])<=0){
                return $this->sendError('Please select at least one variant.', 422);
            }
            $variants =  implode(',', $request->variant_ids);
        }
        $cartItem = CartItem::where('cart_id', $cart->id)->where('product_id', $request->product_id)->where('variant_ids',$variants)->first();
        if (empty($cartItem)) {
            $cartItem = new CartItem();
            $cartItem->cart_id = $cart->id;
            $cartItem->product_id = $request->product_id;
            $cartItem->quantity = $request->quantity;
            $cartItem->product_name = $product->name;
              $cartItem->product_description = $product->description;
            $cartItem->product_price = $product->price;
            $cartItem->cart_price = $product->discount_price;
            if ($variants !=null){
                $cartItem->variant_ids = $variants;
            }
            $cartItem->save();
        }else{
            if ($request->quantity == 0){
                $cartItem->delete();
            }else{
                if ($variants !=null){
                    $cartItem->variant_ids = $variants;
                }else{
                    $cartItem->variant_ids = null;
                }
                $cartItem->quantity = $request->quantity;
                $cartItem->save();
            }
        }
        $this->recalculateCart();
        $cart = Cart::with(['coupon','address','items','store','items.product','service_category'])->where('user_id', $user->id)->first();
        return $this->sendResponse($cart, 'Operation successfully');
    }

    /**
     * Set Tip Amount
     *
     * @authenticated
     * @bodyParam tip_amount double required The tip amount to be added to the cart.
     * @return \Illuminate\Http\Response
     */
    public function setTipAmount(Request $request){
        $request->validate([
            'tip_amount' => 'required|numeric',
        ]);
        $user = auth()->user();
        if (empty($user)) {
            return $this->sendError('Unauthorized', 401);
        }
        $cart = Cart::where('user_id', $user->id)->first();
        if(empty($cart)){
            return $this->sendError('Cart not found', 404);
        }
        $cart->tip = $request->tip_amount;
        $cart->save();
        $this->recalculateCart();
        $cart = Cart::with(['coupon','address','items','store','items.product','service_category'])->where('id',$request->cart_id)->where('user_id', $user->id)->first();
        return $this->sendResponse($cart, 'Tip updated successfully');
    }

    /**
     * Set Pickup From Store
     *
     * @authenticated
     * @bodyParam pickup_from_store int required Pass 0 or 1.
     * @return \Illuminate\Http\Response
     */
    public function setPickupFromStore(Request $request){
        $request->validate([
            'pickup_from_store' => 'nullable',
        ]);
        $user = auth()->user();
        if (empty($user)) {
            return $this->sendError('Unauthorized', 401);
        }
        $cart = Cart::where('user_id', $user->id)->first();
        if(empty($cart)){
            return $this->sendError('Cart not found', 404);
        }
        $cart->pickup_from_store = $request->pickup_from_store;
        $cart->save();
        $this->recalculateCart();
        $cart = Cart::with(['coupon','address','items','store','items.product','service_category'])->where('id',$request->cart_id)->where('user_id', $user->id)->first();
        return $this->sendResponse($cart, 'Tip updated successfully');
    }


    /**
     * Set Vehicle
     *
     * @authenticated
     * @bodyParam vehicle_id string required The vehicle set to the cart.
     * @return \Illuminate\Http\Response
     */
     public function setVehicle(Request $request)
     {
        $request->validate([
            'vehicle_id' => 'required|string',
        ]);

        $user = auth()->user();
        if(empty($user)) {
            return $this->sendError("Unauthorized",401);
        }
        $cart = Cart::where('user_id',$user->id)->first();
        if(empty($cart)) {
            return $this->sendError('Cart not found', 404);
        }
        $cart->vehicle_id = $request->vehicle_id;
        $cart->save();
        $data = $this->calculateDeliveryCharge();
        $this->recalculateCart();
        if ($data['error_code']!='0'){
             Cart::where('user_id',$user->id)->update(['delivery_fee'=>-1]);
             return $this->sendError($data['error_message'], 404);
         }
        $cart = Cart::with(['coupon','address','items','store','items.product','service_category'])->where('user_id', $user->id)->first();
        return $this->sendResponse($cart, 'Vehicle set successfully');
     }

    /**
     * Set Pickup Address
     *
     * @authenticated
     * @bodyParam address string required The address of the pickup location in json format.
     * @return \Illuminate\Http\Response
     */
    public function setPickupAddress(Request $request){
        $request->validate([
            'address' => 'required|string',
        ]);
        $user = auth()->user();

        if (empty($user)) {
         return $this->sendError('Unauthorized', 401);
        }
        $cart = Cart::where('user_id', $user->id)->first();
        if(empty($cart)){
            $cart = new Cart();
            $cart->user_id = $user->id;
            $cart->store_id = null;
            $cart->pickup_address = json_decode($request->address);
            $cart->delivery_address = null;
            $cart->address_id = null;
            $cart->save();
        }else{
            $cart->pickup_address = json_decode($request->address);
            $cart->save();
        }

//        $data = $this->calculateDeliveryCharge();
//        if (isset($data['error_code'])&&$data['error_code']!='0'){
//            $cart = Cart::where('user_id', $user->id)->first();
//            CartItem::where('cart_id', $cart->id)->delete();
//            $cart->delete();
//            return $this->sendError($data['error_message'], 404);
//        }
        $cart = Cart::with(['coupon','address','items','store','items.product','service_category'])->where('id', $cart->id)->first();
        return $this->sendResponse($cart, 'Address changed successfully');
    }

    /**
     * Set JSON Data
     *
     * @authenticated
     * @bodyParam data string required The JSON data to be added to the cart.
     * @return \Illuminate\Http\Response
     */
    public function setJsonData(Request $request){
        $request->validate([
            'data' => 'required|string',
            'images' => 'nullable|array',
        ]);
        $user = auth()->user();
        if (empty($user)) {
            return $this->sendError('Unauthorized', 401);
        }
        $cart = Cart::where('user_id', $user->id)->first();
        if(empty($cart)){
            return $this->sendError('Cart not found', 404);
        }
        $json_data = json_decode($request->data);
        $files = $request->file('images');
        $images = [];
        if ($request->hasFile('images')){
           foreach ($files as $file){
               $images[] = \Helpers::uploadImage($file, 'order_images');
           }
           $json_data->ordered_items = $images;
        }
        $cart->json_data = $json_data;
        $cart->save();
        //$this->calculateDeliveryCharge();
        $cart = Cart::with(['coupon','address','items','store','items.product','service_category'])->where('id', $cart->id)->first();
        return $this->sendResponse($cart, 'Data set successfully');
    }

    /**
     * Clear Cart.
     *
     * @authenticated
     * @return \Illuminate\Http\Response
     */
    public function clearCart(){
        $user = auth()->user();
        if (empty($user)) {
            return $this->sendError('Unauthorized', 401);
        }
        $cart = Cart::where('user_id', $user->id)->first();
        if (empty($cart)) {
            return $this->sendSuccess('Cart already cleared');
        }
        CartItem::where('cart_id', $cart->id)->delete();
        $cart->delete();
        return $this->sendResponse(null, 'Cart cleared successfully');
    }

    public function refresh_cart(){
        $user = auth()->user();
        $data = $this->calculateDeliveryCharge();
        if ($data['error_code']!='0'){
            Cart::where('user_id',$user->id)->update(['delivery_fee'=>-1]);
            return $this->sendError($data['error_message'], 404);
        }
        $this->recalculateCart();
        $cart = Cart::with(['coupon','address','items','store','items.product','service_category'])->where('user_id', $user->id)->first();
        return $this->sendResponse($cart, 'Service changed successfully');
    }

    public function calculateDeliveryCharge(){
       
        $user = auth()->user();
        if (empty($user)) {
            return array('distance' => -1, 'time' => -1, 'error_code'=>'UNAUTHORIZED','error_message'=>'Unauthorized');
        }
        $cart = Cart::where('user_id', $user->id)->first();
       
        if(empty($cart)){
            return array('distance' => -1, 'time' => -1, 'error_code'=>'CART_ERROR','error_message'=>'Cart not found');
        }
        if ($cart->address_id != null){
            $address = Address::where('id', $cart->address_id)->first();
            if (empty($address)) {
                //return $this->sendError('Address not selected', 404);
                return array('distance' => -1, 'time' => -1, 'error_code'=>'ADDRESS_ERROR','error_message'=>'Address not selected');
            }
            $address = $address->toArray();
        }else{
            $address = $cart->delivery_address;
        }

        $store = Store::where('id', $cart->store_id)->first();
        if (empty($store)) {
            if(empty($cart->pickup_address)) {
                return array('distance' => 0, 'time' => 0);
            }else{
                $store_lat = $cart->pickup_address['latitude'];
                $store_lng = $cart->pickup_address['longitude'];
            }
        }else{
            $store_lat = $store->latitude;
            $store_lng = $store->longitude;
        }
        $url = "https://maps.googleapis.com/maps/api/distancematrix/json?destinations=".$store_lat.",".$store_lng."&origins=".$address['latitude'].",".$address['longitude']."&mode=driving&language=en-US&key=".setting('google_map_api_key');
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $response = curl_exec($ch);
        curl_close($ch);
        try{
            $response_a = json_decode($response, true);
            $dist = $response_a['rows'][0]['elements'][0]['distance']['value'];
            $time = $response_a['rows'][0]['elements'][0]['duration']['value'];
            $distKm = $dist/1000;
            $arr = $this->dynamicDeliveryCharge($cart->service_category_id, $distKm);
            if (empty($arr['delivery_fee']) || $arr['delivery_fee'] == -1){
                return array('distance' => $distKm, 'time' => $time, 'error_code'=>$arr['error_code'],'error_message'=>$arr['error_message']);
            }
            $cart->delivery_fee = $arr['delivery_fee'];
            $cart->distance = $dist/1000;
            $cart->travel_time = $time/60;
            $cart->save();
            return array('distance' => $dist, 'time' => $time, 'error_code'=>'0');
        }catch (\Exception $e){
            return array('distance' => -1, 'time' => -1,'error_code'=>'PARSE_ERROR','error_message'=>$e->getMessage()." ".$e->getLine());
        }
    }

    public function getDynamicTax($service_category_id,$amount) : int {
        $dynamic_tax_ids = config('constants.dynamic_tax_ids');
        $service_category = ServiceCategory::find($service_category_id);
        if (empty($service_category)) {
            return 0;
        }
        try{
            if (in_array($service_category->service_type_id,$dynamic_tax_ids)) {
                return $amount * $service_category->tax/100;
            }else{
                return -1;
            }
        }catch (\Exception $e){
            return -1;
        }
    }

    public function dynamicDeliveryCharge($service_category_id, $km_to_travel) : array{
        try{
            $dynamic_pricing_ids = config('constants.dynamic_delivery_ids');
            $service_category = ServiceCategory::find($service_category_id);

            if (empty($service_category)) {
                return array('delivery_fee' => -1, 'error_code'=>'SERVICE_CATEGORY_ERROR','error_message'=>'Service category not found');
            }
            if (in_array($service_category->service_type_id,$dynamic_pricing_ids)){
                //Dynamic pricing logic for Pick & Drop and Anything in Store
                $base_distance = $service_category->base_distance;
                $base_price = $service_category->base_price;
                $price_per_km = $service_category->price_km;
                if ($km_to_travel <= $base_distance) {
                    return array('delivery_fee' => $base_price, 'error_code'=>'0','error_message'=>'01');
                }else {
                    $total = $base_price + max($km_to_travel - $base_distance,0) * $price_per_km;
                    return array('delivery_fee' => $total, 'error_code'=>'0','error_message'=>'02');
                }
            }
            if($service_category->service_type_id == config('constants.cab_service_type_id')){
                //Cab booking pricing logic
                $cart = $this->cart_service->getUserCart();
                if (empty($cart)){
                    return array('delivery_fee' => -1, 'error_code'=>'CART_NOT_FOUND','error_message'=>'Cart not found');
                }
                $fare = $this->cart_service->calculateCabFare($cart->id);
                return array('delivery_fee' => $fare, 'error_code'=>'0','error_message'=>'03');
            }else{
                $delivery_fee = DeliveryFee::where('from_km', '<=', $km_to_travel)->where('to_km','>=',$km_to_travel)->orderBy('created_at', 'desc')->first();
              
                if (empty($delivery_fee)) {
                    return array('delivery_fee' => -1, 'error_code'=>'ZONE_ERROR','error_message'=>'You are outside delivery zone');
                }
                return array('delivery_fee' => $delivery_fee->delivery_fee, 'error_code'=>'0');
            }
        }catch (\Exception $e){
            return array('delivery_fee' => -1, 'error_code'=>'Unknown Error','error_message'=>$e->getMessage()." ".$e->getLine());
        }
    }

    /**
     * Change Delivery Address.
     *
     * @authenticated
     * @bodyParam address_id int required Address Id.
     * @return \Illuminate\Http\Response
     */
    public function changeDeliveryAddress(Request $request){
        $request->validate([
            'address_id' => 'nullable|integer',
            'address' => 'nullable|string',
        ]);
        $user = auth()->user();
        if (empty($user)) {
            return $this->sendError('Unauthorized', 401);
        }
        $cart = Cart::where('user_id', $user->id)->first();
        if(empty($cart)){
            return $this->sendError('Cart not found', 404);
        }
        if (!empty($request->address_id)) {
            $address = Address::select('address', 'latitude', 'longitude')->where('id', $request->address_id)->first();
            if (empty($address)) {
                return $this->sendError('Address not found', 404);
            }
            $address = json_decode($address->toJson());
        }else{
            $address =  json_decode($request->address);
        }

        $cart->delivery_address = $address;
        $cart->address_id = $request->address_id ?? null;

        $cart->save();
        $data = $this->calculateDeliveryCharge();
        if ($data['error_code']!='0'){
            Cart::where('user_id',$user->id)->update(['delivery_fee'=>-1]);
            return $this->sendError($data['error_message'], 404);
        }
        $cart = Cart::with(['coupon','address','items','store','items.product','service_category'])->where('user_id', $user->id)->first();
        return $this->sendResponse($cart, 'Address changed successfully');
    }

    /**
     * Apply Coupon.
     *
     * @authenticated
     * @bodyParam coupon_code string required Coupon code.
     * @bodyParam cart_id integer required Cart id.
     * @return \Illuminate\Http\Response
     */
    public function applyCoupon(Request $request){
        $request->validate([
            'coupon_code' => 'required|string',
        ]);
        $user = auth()->user();
        if (empty($user)) {
            return $this->sendError('Unauthorized', 401);
        }
        $cart = Cart::where('user_id', $user->id)->first();
        if(empty($cart)){
            return $this->sendError('Cart not found');
        }
        $coupon = Coupon::where('code',$request->coupon_code)->where('store_id',$cart->store_id)->where('active',1)->first();
        if(empty($coupon)){
            return $this->sendError('Coupon not found');
        }
        //Check coupon logic
        $startDate = Carbon::parse($coupon->start_date);
        $endDate = Carbon::parse($coupon->end_date);
        $current = Carbon::now();
        if($current->lt($startDate) || $current->gt($endDate)){
            return $this->sendError('Coupon valid from '.$startDate->format('d/m/Y H:i:s').' to '.$endDate->format('d/m/Y H:i:s'), 422);
        }
        if($coupon->min_order_amount > $cart->total_amount){
            return $this->sendError('Minimum order value should be greater than '.$coupon->min_order_amount, 422);
        }
        $usage_count = CouponUsage::where('coupon_id',$coupon->id)->where('user_id',$user->id)->count();
        if($coupon->max_use_per_user > 0 && $usage_count >= $coupon->max_use_per_user){
            return $this->sendError('Coupon usage limit exceeded', 422);
        }
        Cart::where('user_id', $user->id)->update(['coupon_id' => $coupon->id]);
        if($coupon->discount_type == "FREE DELIVERY")
        {
            // if(@$coupon->max_discount_value)
            // {
            // //    $fee = $cart->delivery_fee * $coupon->discount_value / 100;
            //   if( $cart->delivery_fee > $coupon->max_discount_value){
            //     $fee = $cart->delivery_fee - $coupon->max_discount_value;
            //   }
            //  else
            // $fee = 0;
            // }else
            //     $fee = 0;
            
        Cart::where('user_id', $user->id)->update(['delivery_fee' => 0]);
    }
        $this->recalculateCart();
        $cart = Cart::with(['coupon','address','items','store','items.product','service_category'])->where('user_id', $user->id)->first();
        return $this->sendResponse($cart, 'Coupon applied successfully');
    }

    /**
     * Remove Coupon.
     *
     * @authenticated
     * @return \Illuminate\Http\Response
     */
    public function removeCoupon(Request $request){
        $user = auth()->user();
        if (empty($user)) {
            return $this->sendError('Unauthorized', 401);
        }
        Cart::where('user_id', $user->id)->update(['coupon_id' => null]);
        $this->recalculateCart();
        $cart = Cart::with(['coupon','address','items','store','items.product','service_category'])->where('user_id', $user->id)->first();
        return $this->sendResponse($cart, 'Operation successfully');
    }

    /**
     * Set Service ID.
     *
     * @authenticated
     * @bodyParam service_category_id integer required Service id.
     * @return \Illuminate\Http\Response
     */
    public function setService(Request $request){
        $request->validate([
            'service_category_id' => 'required|integer',
        ]);
        $user = auth()->user();
        if (empty($user)) {
            return $this->sendError('Unauthorized', 401);
        }
        $cart = Cart::where('user_id', $user->id)->first();
        if(empty($cart)){
            return $this->sendError('Cart not found');
        }
        $cart->service_category_id = $request->service_category_id;
        $cart->save();
        $this->recalculateCart();
        $cart = Cart::with(['coupon','address','items','store','items.product','service_category'])->where('user_id', $user->id)->first();
        return $this->sendResponse($cart, 'Service changed successfully');
    }

    public function recalculateCart(){
        $user = auth()->user();
        $cart = Cart::with('items')->where('user_id', $user->id)->first();
        if (empty($cart)) {
            return $this->sendError('Cart not found', 404);
        }
      
        $store_tax = 0;
        $product_tax = 0;
        //Set service category id in cart
        if ($cart->store_id!=null){
            $store = Store::find($cart->store_id);
            if(!empty($store)){
                $cart->service_category_id = $store->service_category_id;
                $cart->save();
                $store_tax = $store->tax;
            }
        }

        $grand_total = 0;
        $cart->discount_value = 0;
        $discount = 0;
        $cartItems = CartItem::where('cart_id', $cart->id)->get();
         $tax =0;
        foreach ($cartItems as $cartItem){
            $total = 0;
           
            if($cartItem->product_id != null){
                $product = Product::findOrFail($cartItem->product_id);
                $variantIdArray = array_filter(explode(',', $cartItem->variant_ids));
                if ($product->product_type == 'VARIABLE') {
                    if(count($variantIdArray) > 0){
                        $variation = ProductVariant::find($variantIdArray[0]);
                        if (empty($variation)) {
                            return $this->sendError('Variant with id '.$variantIdArray[0].' not found', 404);
                        }
                        $total += $variation->discount_price * $cartItem->quantity;
                       
                    }else{
                        $total += $product->discount_price * $cartItem->quantity;
                    }
                }else if ($product->product_type == 'CUSTOMIZABLE'){
                    foreach ($variantIdArray as $variantId){
                        $variation = ProductVariant::find($variantId);
                        if (empty($variation)) {
                            return $this->sendError('Variant not found', 404);
                        }
                        $total += $variation->discount_price * $cartItem->quantity;
                    }
                    $total += $product->discount_price * $cartItem->quantity;
                }else {
                    $total += $product->discount_price * $cartItem->quantity;
                }
                 if(@$store){
                    if(@$product->tax != NULL){
                      $item_discount =  $this->applyCouponItem($cart,$total,$cart->discount_value);
                      $discount += $item_discount;
                       $cart->discount_value = $discount;
                      $tax_total = $total - $item_discount;
                    $tax += $product->tax * $tax_total / 100;
                    // $tax = $tax * $cartItem->quantity;
                    
                     }else{
                      if(@$cartItem->variant_ids == null)
                      {
                           $item_discount =  $this->applyCouponItem($cart,$total,$cart->discount_value);
                           $discount += $item_discount;
                          $cart->discount_value = $discount;
                      $tax_total = $total - $item_discount;
                           $tax += $store_tax * $tax_total / 100;
                            // $tax = $tax * $cartItem->quantity;
                      }else
                      {
                            $item_discount =  $this->applyCouponItem($cart,$total,$cart->discount_value);
                            $discount += $item_discount;
                           $cart->discount_value = $discount;
                           $tax_total = $total - $item_discount;
                            $tax += $store_tax * $tax_total / 100;
                            // $tax = $tax * $cartItem->quantity;
                      }
                   
                }
                }
                $cartItem->product_name = $product->name;
                $cartItem->product_price = $product->price;
                $cartItem->cart_price = $total;
                $cartItem->save();
                $grand_total += $total;
            
            }
        }
        //calculate tax
        if(@$store){
        if($cart->pickup_from_store == '1'){
            $cart->delivery_fee = 0;
        }else{
            $this->calculateDeliveryCharge();
        }
        }
        if($cart->coupon_id != null) {
            $coupon = Coupon::find($cart->coupon_id);
            if ($coupon->discount_type == 'FREE DELIVERY') {
        //  if(@$coupon->max_discount_value)
        //     {
        //     //    $fee = $cart->delivery_fee * $coupon->discount_value / 100;
        //       if( $cart->delivery_fee > $coupon->max_discount_value){
        //         $fee =  $coupon->max_discount_value;
        //       }
        //      else
        //     $fee = 0;
        //     }else
        //         $fee = 0;
                
                $cart->discount_value =0;
                $cart->delivery_fee = 0;
         }
             else 
            {
                if($discount > $coupon->max_discount_value)
            {
                $discount =  $coupon->max_discount_value;
            } 
            }
           
        }
     
        $dynamic_tax = $this->getDynamicTax($cart->service_category_id,($grand_total + $cart->delivery_fee));
         
        // if($tax != 0){
        //     $cart->tax = $tax;
        // }
        // else{    
         $tax = $dynamic_tax == -1 ? $tax : $dynamic_tax;
        //  $tax = $dynamic_tax == -1 ? ($grand_total + $cart->delivery_fee) * $store_tax/100 : $dynamic_tax;
         $cart->tax = $tax;
        // }
      
        $cart->total_price = $grand_total;
         $cart->discount_value = $discount;
        // $cart->grand_total = ($grand_total - $new_discount) + $tax + $cart->delivery_fee + $cart->tip;

        // $cart->grand_total = ($grand_total + $tax) + $cart->delivery_fee + $cart->tip;
        $cart->grand_total = ($grand_total - $discount) + $tax + $cart->delivery_fee + $cart->tip;
        $cart->save();
    }
    public function applyCouponItem($cart,$item_amount,$discount)
    {
         if($cart->coupon_id != null) {
            $coupon = Coupon::find($cart->coupon_id);
            if($coupon->discount_type == 'FLAT') {
                $new_discount = ($item_amount * $coupon->discount_value )/ 100;
               if($discount+$new_discount != $coupon->max_discount_value)
               {
                    if ($new_discount > $coupon->max_discount_value) {
                    $new_discount = $coupon->max_discount_value;
                }else if($discount+$new_discount > $coupon->max_discount_value)
                {
                    $new_discount = $coupon->max_discount_value - $discount;
                }
                return $amount_discount = $new_discount;
            
               }else
               return 0;
               
        }else{
            return 0;
        }
    }
    }
}
