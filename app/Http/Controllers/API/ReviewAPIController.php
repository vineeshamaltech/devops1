<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateReviewAPIRequest;
use App\Http\Requests\API\UpdateReviewAPIRequest;
use App\Models\Driver;
use App\Models\Order;
use App\Models\Review;
use App\Models\Store;
use App\Models\User;
use App\Repositories\ReviewRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class ReviewController
 * @package App\Http\Controllers\API
 * @group Reviews
 */

class ReviewAPIController extends AppBaseController
{
    /** @var  ReviewRepository */
    private $reviewRepository;

    public function __construct(ReviewRepository $reviewRepo)
    {
        $this->reviewRepository = $reviewRepo;
    }

    /**
     * Display a listing of the Review.
     * GET|HEAD /reviews
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->reviewRepository->pushCriteria(new RequestCriteria($request));
        $reviews = $this->reviewRepository->all();

        return $this->sendResponse(
            $reviews->toArray(),
            __('lang.messages.retrieved', ['model' => __('models/reviews.plural')])
        );
    }

    /**
     * Store a newly created Review in storage.
     * POST /reviews
     *
     * @param CreateReviewAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateReviewAPIRequest $request)
    {
        $input = $request->all();

        $order = Order::find($input['order_id']);
        $input['driver_id'] = $order->driver_id;
        $input['store_id'] = $order->store_id;
        $review = $this->reviewRepository->updateOrCreate(['order_id' => $input['order_id']], $input);
        //$review = $this->reviewRepository->create($input);

        //Update Driver Rating
        if (!empty($review->driver_id)){
            $avg_driver_rating = Review::where('driver_id', $review->driver_id)->avg('driver_rating');
            Driver::where('id', $input['driver_id'])->update(['rating' => $avg_driver_rating]);
        }

        //Update Store Rating
        if (!empty($review->store_id)){
            $avg_store_rating = Review::where('store_id', $review->store_id)->avg('store_rating');
            Store::where('id', $input['store_id'])->update(['rating' => $avg_store_rating]);
        }

        //Update User Rating
        if (!empty($review->user_id)){
            $avg_user_rating = Review::where('user_id', $review->user_id)->avg('user_rating');
            User::where('id', $input['user_id'])->update(['rating' => $avg_user_rating]);
        }

        //added commit
        return $this->sendResponse(
            $review->toArray(),
            __('lang.messages.saved', ['model' => __('models/reviews.singular')])
        );
    }

    /**
     * Display the specified Review.
     * GET|HEAD /reviews/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Review $review */
        $review = $this->reviewRepository->find($id);

        if (empty($review)) {
            return $this->sendError(
                __('lang.messages.not_found', ['model' => __('models/reviews.singular')])
            );
        }

        return $this->sendResponse(
            $review->toArray(),
            __('lang.messages.retrieved', ['model' => __('models/reviews.singular')])
        );
    }

    /**
     * Update the specified Review in storage.
     * PUT/PATCH /reviews/{id}
     *
     * @param int $id
     * @param UpdateReviewAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateReviewAPIRequest $request)
    {
        $input = $request->all();

        /** @var Review $review */
        $review = $this->reviewRepository->find($id);

        if (empty($review)) {
            return $this->sendError(
                __('lang.messages.not_found', ['model' => __('models/reviews.singular')])
            );
        }

        $review = $this->reviewRepository->update($input, $id);

        //Update Driver Rating
        if (!empty($review->driver_id)){
            $avg_driver_rating = Review::where('driver_id', $review->driver_id)->avg('driver_rating');
            Driver::where('id', $input['driver_id'])->update(['rating' => $avg_driver_rating]);
        }

        //Update Store Rating
        if (!empty($review->store_id)){
            $avg_store_rating = Review::where('store_id', $review->store_id)->avg('store_rating');
            Store::where('id', $input['store_id'])->update(['rating' => $avg_store_rating]);
        }

        //Update User Rating
        if (!empty($review->user_id)){
            $avg_user_rating = Review::where('user_id', $review->user_id)->avg('user_rating');
            User::where('id', $input['user_id'])->update(['rating' => $avg_user_rating]);
        }

        return $this->sendResponse(
            $review->toArray(),
            __('lang.messages.updated', ['model' => __('models/reviews.singular')])
        );
    }

    /**
     * Remove the specified Review from storage.
     * DELETE /reviews/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Review $review */
        $review = $this->reviewRepository->find($id);

        if (empty($review)) {
            return $this->sendError(
                __('lang.messages.not_found', ['model' => __('models/reviews.singular')])
            );
        }

        $review->delete();

        return $this->sendResponse(
            $id,
            __('lang.messages.deleted', ['model' => __('models/reviews.singular')])
        );
    }

}
