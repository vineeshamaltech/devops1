<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateFAQAPIRequest;
use App\Http\Requests\API\UpdateFAQAPIRequest;
use App\Models\FAQ;
use App\Repositories\FAQRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class FAQController
 * @package App\Http\Controllers\API
 */

class FAQAPIController extends AppBaseController
{
    /** @var  FAQRepository */
    private $fAQRepository;

    public function __construct(FAQRepository $fAQRepo)
    {
        $this->fAQRepository = $fAQRepo;
    }

    /**
     * Display a listing of the FAQ.
     * GET|HEAD /fAQS
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->fAQRepository->pushCriteria(new RequestCriteria($request));
        $fAQS = $this->fAQRepository->all();

        return $this->sendResponse(
            $fAQS->toArray(),
            __('lang.messages.retrieved', ['model' => __('models/fAQS.plural')])
        );
    }

    /**
     * Store a newly created FAQ in storage.
     * POST /fAQS
     *
     * @param CreateFAQAPIRequest $request
     *
     * @return Response
     */
//    public function store(CreateFAQAPIRequest $request)
//    {
//        $input = $request->all();
//
//        $fAQ = $this->fAQRepository->create($input);
//
//        return $this->sendResponse(
//            $fAQ->toArray(),
//            __('lang.messages.saved', ['model' => __('models/fAQS.singular')])
//        );
//    }

    /**
     * Display the specified FAQ.
     * GET|HEAD /fAQS/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var FAQ $fAQ */
        $fAQ = $this->fAQRepository->find($id);

        if (empty($fAQ)) {
            return $this->sendError(
                __('lang.messages.not_found', ['model' => __('models/fAQS.singular')])
            );
        }

        return $this->sendResponse(
            $fAQ->toArray(),
            __('lang.messages.retrieved', ['model' => __('models/fAQS.singular')])
        );
    }

    /**
     * Update the specified FAQ in storage.
     * PUT/PATCH /fAQS/{id}
     *
     * @param int $id
     * @param UpdateFAQAPIRequest $request
     *
     * @return Response
     */
//    public function update($id, UpdateFAQAPIRequest $request)
//    {
//        $input = $request->all();
//
//        /** @var FAQ $fAQ */
//        $fAQ = $this->fAQRepository->find($id);
//
//        if (empty($fAQ)) {
//            return $this->sendError(
//                __('lang.messages.not_found', ['model' => __('models/fAQS.singular')])
//            );
//        }
//
//        $fAQ = $this->fAQRepository->update($input, $id);
//
//        return $this->sendResponse(
//            $fAQ->toArray(),
//            __('lang.messages.updated', ['model' => __('models/fAQS.singular')])
//        );
//    }

    /**
     * Remove the specified FAQ from storage.
     * DELETE /fAQS/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
//    public function destroy($id)
//    {
//        /** @var FAQ $fAQ */
//        $fAQ = $this->fAQRepository->find($id);
//
//        if (empty($fAQ)) {
//            return $this->sendError(
//                __('lang.messages.not_found', ['model' => __('models/fAQS.singular')])
//            );
//        }
//
//        $fAQ->delete();
//
//        return $this->sendResponse(
//            $id,
//            __('lang.messages.deleted', ['model' => __('models/fAQS.singular')])
//        );
//    }
}
