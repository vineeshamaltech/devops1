<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateRoleAPIRequest;
use App\Http\Requests\API\UpdateRoleAPIRequest;
use App\Repositories\RoleRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Response;

/**
 * Class RoleController
 * @package App\Http\Controllers\API
 */

class RoleAPIController extends AppBaseController
{
    /** @var  RoleRepository */
    private $roleRepository;

    public function __construct(RoleRepository $roleRepo)
    {
        $this->roleRepository = $roleRepo;
    }

    /**
     * Display a listing of the Role.
     * GET|HEAD /roles
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $roles = $this->roleRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse(
            $roles->toArray(),
            __('messages.retrieved', ['model' => __('models/roles.plural')])
        );
    }

    /**
     * Store a newly created Role in storage.
     * POST /roles
     *
     * @param CreateRoleAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateRoleAPIRequest $request)
    {
        $input = $request->all();

        $role = $this->roleRepository->create($input);

        return $this->sendResponse(
            $role->toArray(),
            __('messages.saved', ['model' => __('models/roles.singular')])
        );
    }

    /**
     * Display the specified Role.
     * GET|HEAD /roles/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Role $role */
        $role = $this->roleRepository->find($id);

        if (empty($role)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('models/roles.singular')])
            );
        }

        return $this->sendResponse(
            $role->toArray(),
            __('messages.retrieved', ['model' => __('models/roles.singular')])
        );
    }

    /**
     * Update the specified Role in storage.
     * PUT/PATCH /roles/{id}
     *
     * @param int $id
     * @param UpdateRoleAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateRoleAPIRequest $request)
    {
        $input = $request->all();

        /** @var Role $role */
        $role = $this->roleRepository->find($id);

        if (empty($role)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('models/roles.singular')])
            );
        }

        $role = $this->roleRepository->update($input, $id);

        return $this->sendResponse(
            $role->toArray(),
            __('messages.updated', ['model' => __('models/roles.singular')])
        );
    }

    /**
     * Remove the specified Role from storage.
     * DELETE /roles/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Role $role */
        $role = $this->roleRepository->find($id);

        if (empty($role)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('models/roles.singular')])
            );
        }
        $role->delete();
        return $this->sendResponse(
            $id,
            __('messages.deleted', ['model' => __('models/roles.singular')])
        );
    }

    public function grant_permission(Request $request){
        $role = Role::findByName($request->role_name);
        $role->givePermissionTo($request->permission_name);
        return $this->sendResponse(null, 'Permission granted');
    }

    public function revoke_permission(Request $request){
        $role = Role::findByName($request->role_name);
        $role->revokePermissionTo($request->permission_name);
        return $this->sendResponse(null, 'Permission revoked');
    }
}
