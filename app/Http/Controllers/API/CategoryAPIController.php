<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateCategoryAPIRequest;
use App\Http\Requests\API\UpdateCategoryAPIRequest;
use App\Models\Category;
use App\Repositories\CategoryRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class CategoryController
 * @package App\Http\Controllers\API
 * @group Category
 */

class CategoryAPIController extends AppBaseController
{
    /** @var  CategoryRepository */
    private $categoryRepository;

    public function __construct(CategoryRepository $categoryRepo)
    {
        $this->categoryRepository = $categoryRepo;
    }

    /**
     * Display a listing of the Category.
     * GET|HEAD /categories
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        
        $this->categoryRepository->pushCriteria(new RequestCriteria($request));
         $categories = $this->categoryRepository->all();
          if(@$request->parent){
                    $categories = $this->categoryRepository->where('parent', $request->parent)->get();
                    // dd($categories);
                }
       
              
        return $this->sendResponse(
            $categories->toArray(),
            __('lang.messages.retrieved', ['model' => __('models/categories.plural')])
        );
    }

    public function get_categories(Request $request)
    {
        
        $categories = Category::has('product')->get();
        return $this->sendResponse(
            $categories->toArray(),
            __('lang.messages.retrieved', ['model' => __('models/categories.plural')])
        );
    }

    /**
     * Store a newly created Category in storage.
     * POST /categories
     *
     * @authenticated
     * @param CreateCategoryAPIRequest $request
     * @return Response
     */
    public function store(CreateCategoryAPIRequest $request)
    {
        $input = $request->all();

        if ($request->hasFile('image')){
            $input['image'] = \Helpers::uploadImage($request->file('image'), 'categories');
        }
        $category = $this->categoryRepository->create($input);

        return $this->sendResponse(
            $category->toArray(),
            __('lang.messages.saved', ['model' => __('models/categories.singular')])
        );
    }

    /**
     * Display the specified Category.
     * GET|HEAD /categories/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Category $category */
        $category = $this->categoryRepository->find($id);
        if (empty($category)) {
            return $this->sendError(
                __('lang.messages.not_found', ['model' => __('models/categories.singular')])
            );
        }
        return $this->sendResponse(
            $category->toArray(),
            __('lang.messages.retrieved', ['model' => __('models/categories.singular')])
        );
    }

    /**
     * Update the specified Category in storage.
     * PUT/PATCH /categories/{id}
     *
     * @authenticated
     * @param int $id
     * @param UpdateCategoryAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateCategoryAPIRequest $request)
    {
        $input = $request->all();
        /** @var Category $category */
        $category = $this->categoryRepository->find($id);

        if (empty($category)) {
            return $this->sendError(
                __('lang.messages.not_found', ['model' => __('models/categories.singular')])
            );
        }

        if ($request->hasFile('image')){
            \Helpers::deleteImage($category->image);
            $input['image'] = \Helpers::uploadImage($request->file('image'), 'categories');
        }

        $category = $this->categoryRepository->update($input, $id);

        return $this->sendResponse(
            $category,
            __('lang.messages.updated', ['model' => __('models/categories.singular')])
        );
    }

    /**
     * Remove the specified Category from storage.
     * DELETE /categories/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Category $category */
        $category = $this->categoryRepository->find($id);

        if (empty($category)) {
            return $this->sendError(
                __('lang.messages.not_found', ['model' => __('models/categories.singular')])
            );
        }

        $category->delete();

        return $this->sendResponse(
            $id,
            __('lang.messages.deleted', ['model' => __('models/categories.singular')])
        );
    }
}
