<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateDeliveryFeeAPIRequest;
use App\Http\Requests\API\UpdateDeliveryFeeAPIRequest;
use App\Models\DeliveryFee;
use App\Repositories\DeliveryFeeRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class DeliveryFeeController
 * @package App\Http\Controllers\API
 */

class DeliveryFeeAPIController extends AppBaseController
{
    /** @var  DeliveryFeeRepository */
    private $deliveryFeeRepository;

    public function __construct(DeliveryFeeRepository $deliveryFeeRepo)
    {
        $this->deliveryFeeRepository = $deliveryFeeRepo;
    }

    /**
     * Get DeliveryFee.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $deliveryFees = $this->deliveryFeeRepository->all();

        return $this->sendResponse(
            $deliveryFees->toArray(),
            __('lang.messages.retrieved', ['model' => __('models/deliveryFees.plural')])
        );
    }

}
