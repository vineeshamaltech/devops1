<?php

namespace App\Http\Controllers\API;

use App\Criteria\ActiveCriteria;
use App\Http\Requests\API\CreateServiceCategoryAPIRequest;
use App\Http\Requests\API\UpdateServiceCategoryAPIRequest;
use App\Models\ServiceCategory;
use App\Repositories\ServiceCategoryRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class ServiceCategoryController
 * @package App\Http\Controllers\API
 * @group ServiceCategory
 */

class ServiceCategoryAPIController extends AppBaseController
{
    /** @var  ServiceCategoryRepository */
    private $serviceCategoryRepository;

    public function __construct(ServiceCategoryRepository $serviceCategoryRepo)
    {
        $this->serviceCategoryRepository = $serviceCategoryRepo;
    }

    /**
     * Display a listing of the ServiceCategory.
     * GET|HEAD /serviceCategories
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->serviceCategoryRepository->pushCriteria(new RequestCriteria($request));
        $this->serviceCategoryRepository->pushCriteria(new ActiveCriteria());
        $serviceCategories = $this->serviceCategoryRepository->get();

        $newServices = [];
        foreach ($serviceCategories as $serviceCategory) {
            if ($serviceCategory->service_type_id == 1 &&
                config('constants.features.purchasing_service',false)){
                $newServices[] = $serviceCategory;
            }
            if ($serviceCategory->service_type_id == 2 &&
                config('constants.features.pick_n_drop_service',false)){
                $newServices[] = $serviceCategory;
            }
            if ($serviceCategory->service_type_id == 3 &&
                config('constants.features.order_anything_service',false)){
                $newServices[] = $serviceCategory;
            }
            if ($serviceCategory->service_type_id == 4 &&
                config('constants.features.passenger_transportation_service',false)){
                $newServices[] = $serviceCategory;
            }
        }

        return $this->sendResponse(
            $newServices,
            __('lang.messages.retrieved', ['model' => __('models/serviceCategories.plural')])
        );
    }

    /**
     * Store a newly created ServiceCategory in storage.
     * POST /serviceCategories
     *
     * @param CreateServiceCategoryAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateServiceCategoryAPIRequest $request)
    {
        $input = $request->all();

        $serviceCategory = $this->serviceCategoryRepository->create($input);

        return $this->sendResponse(
            $serviceCategory->toArray(),
            __('lang.messages.saved', ['model' => __('models/serviceCategories.singular')])
        );
    }

    /**
     * Display the specified ServiceCategory.
     * GET|HEAD /serviceCategories/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var ServiceCategory $serviceCategory */
        $serviceCategory = $this->serviceCategoryRepository->with('payment_methods')->find($id);

        if (empty($serviceCategory)) {
            return $this->sendError(
                __('lang.messages.not_found', ['model' => __('models/serviceCategories.singular')])
            );
        }

        return $this->sendResponse(
            $serviceCategory->toArray(),
            __('lang.messages.retrieved', ['model' => __('models/serviceCategories.singular')])
        );
    }

    /**
     * Update the specified ServiceCategory in storage.
     * PUT/PATCH /serviceCategories/{id}
     *
     * @param int $id
     * @param UpdateServiceCategoryAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateServiceCategoryAPIRequest $request)
    {
        $input = $request->all();

        /** @var ServiceCategory $serviceCategory */
        $serviceCategory = $this->serviceCategoryRepository->find($id);

        if (empty($serviceCategory)) {
            return $this->sendError(
                __('lang.messages.not_found', ['model' => __('models/serviceCategories.singular')])
            );
        }

        $serviceCategory = $this->serviceCategoryRepository->update($input, $id);

        return $this->sendResponse(
            $serviceCategory->toArray(),
            __('lang.messages.updated', ['model' => __('models/serviceCategories.singular')])
        );
    }

    /**
     * Remove the specified ServiceCategory from storage.
     * DELETE /serviceCategories/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var ServiceCategory $serviceCategory */
        $serviceCategory = $this->serviceCategoryRepository->find($id);

        if (empty($serviceCategory)) {
            return $this->sendError(
                __('lang.messages.not_found', ['model' => __('models/serviceCategories.singular')])
            );
        }

        $serviceCategory->delete();

        return $this->sendResponse(
            $id,
            __('lang.messages.deleted', ['model' => __('models/serviceCategories.singular')])
        );
    }
}
