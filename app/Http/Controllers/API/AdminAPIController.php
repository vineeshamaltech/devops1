<?php

namespace App\Http\Controllers\API;

use App\Criteria\Select2SearchCriteria;
use App\Http\Requests\API\CreateAdminAPIRequest;
use App\Http\Requests\API\UpdateAdminAPIRequest;
use App\Models\Admin;
use App\Repositories\AdminRepository;
// custmoer reposiotry
use App\Repositories\UserRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Illuminate\Support\Facades\Auth;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class AdminController
 * @group AdminAPIs
 * @package App\Http\Controllers\API
 */

class AdminAPIController extends AppBaseController
{
    /** @var  AdminRepository */
    private $adminRepository;
    private $userRepository;

    public function __construct(AdminRepository $adminRepo, UserRepository $userRepo)
    {
        $this->adminRepository = $adminRepo;
        $this->userRepository = $userRepo;
    }
  

    /**
     * Display a listing of the Admin.
     * GET|HEAD /admins
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->adminRepository->pushCriteria(new Select2SearchCriteria($request));
        $this->adminRepository->pushCriteria(new RequestCriteria($request));
        $admins = $this->adminRepository->all();

        return $this->sendResponse(
            $admins->toArray(),
            __('lang.messages.retrieved', ['model' => __('models/admins.plural')])
        );
    }

    public function get_admin(Request $request)
    {
        $this->adminRepository->pushCriteria(new Select2SearchCriteria($request));
        $this->adminRepository->pushCriteria(new RequestCriteria($request));
        $admins = $this->adminRepository->where('type','Admin')->get();
        return $this->sendResponse(
            $admins->toArray(),
            __('lang.messages.retrieved', ['model' => __('models/admins.plural')])
        );
    }

    public function get_vendor(Request $request)
    {
        $this->adminRepository->pushCriteria(new Select2SearchCriteria($request));
        $this->adminRepository->pushCriteria(new RequestCriteria($request));
        $admins = $this->adminRepository->where('type','Vendor')->get();
        return $this->sendResponse(
            $admins->toArray(),
            __('lang.messages.retrieved', ['model' => __('models/admins.plural')])
        );
    }
    // get customer
    public function get_customer(Request $request)
    {
      $this->userRepository->pushCriteria(new Select2SearchCriteria($request));
        $this->userRepository->pushCriteria(new RequestCriteria($request)); 
        $customer = $this->userRepository->get();
        return $this->sendResponse(
            $customer->toArray(),
            __('lang.messages.retrieved', ['model' => __('models/admins.plural')])
        );
    }

    /**
     * Store a newly created Admin in storage.
     * POST /admins
     *
     * @param CreateAdminAPIRequest $request
     * @return Response
     */
//    public function store(CreateAdminAPIRequest $request)
//    {
//        $input = $request->all();
//        $admin = $this->adminRepository->create($input);
//        return $this->sendResponse(
//            $admin->toArray(),
//            __('lang.messages.saved', ['model' => __('models/admins.singular')])
//        );
//    }

    /**
     * Display the specified Admin.
     * GET|HEAD /admins/{id}
     *
     * @param int $id
     *
     * @return Response
     */
//    public function show($id)
//    {
//        /** @var Admin $admin */
//        $admin = $this->adminRepository->find($id);
//
//        if (empty($admin)) {
//            return $this->sendError(
//                __('lang.messages.not_found', ['model' => __('models/admins.singular')])
//            );
//        }
//
//        return $this->sendResponse(
//            $admin->toArray(),
//            __('lang.messages.retrieved', ['model' => __('models/admins.singular')])
//        );
//    }

    /**
     * Update
     * PUT/PATCH /admins/{id}
     *
     * @param int $id
     * @param UpdateAdminAPIRequest $request
     *
     * @return Response
     */
//    public function update($id, UpdateAdminAPIRequest $request)
//    {
//        $user = Auth::user();
//        $input = $request->all();
//
//        if ($user->id != $id) {
//            return $this->sendError(
//                __('lang.messages.unauthorized')
//            );
//        }
//
//        $admin = $this->adminRepository->find($id);
//
//        if (empty($admin)) {
//            return $this->sendError(
//                __('lang.messages.not_found', ['model' => __('models/admins.singular')])
//            );
//        }
//
//        $admin = $this->adminRepository->update($input, $id);
//
//        return $this->sendResponse(
//            $admin->toArray(),
//            __('lang.messages.updated', ['model' => __('models/admins.singular')])
//        );
//    }

    /**
     * Remove the specified Admin from storage.
     * DELETE /admins/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
//    public function destroy($id)
//    {
//        /** @var Admin $admin */
//        $admin = $this->adminRepository->find($id);
//
//        if (empty($admin)) {
//            return $this->sendError(
//                __('lang.messages.not_found', ['model' => __('models/admins.singular')])
//            );
//        }
//
//        $admin->delete();
//
//        return $this->sendResponse(
//            $id,
//            __('lang.messages.deleted', ['model' => __('models/admins.singular')])
//        );
//    }
}
