<?php

namespace App\Http\Controllers\API\ServiceBooking;

use App\Helpers\OrderHelper;
use App\Helpers\WalletHelper;
use App\Http\Controllers\AppBaseController;
use App\Models\CouponUsage;
use App\Models\HomeService;
use App\Models\HomeServiceCategory;
use App\Models\DriverHomeServices;
use App\Models\Payment;
use App\Models\ServiceCart;
use App\Models\ServiceCartItem;
use App\Models\ServiceOrder;
use App\Models\Driver;
use App\Models\ServiceOrderProducts;
use App\Models\TimeSlots;
use App\Services\HomeOrderService;
use App\Models\User;
use App\Notifications\NewOrder;
use App\Notifications\PushNotification;
use App\Repositories\ServiceOrderAPIRepository;
use Illuminate\Http\Request;
use Notification;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use DB;

/**
 * Class ServiceCartApiController
 * @package App\Http\Controllers\API
 * @group ServiceOrder
 */

class ServiceOrderAPIController extends AppBaseController
{
    private $serviceOrderAPIRepository;

    public function __construct(ServiceOrderAPIRepository $serviceOrderAPIRepository)
    {
        $this->serviceOrderAPIRepository = $serviceOrderAPIRepository;
    }

    /**
     * Display a listing of Service Order the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->serviceOrderAPIRepository->pushCriteria(new RequestCriteria($request));
        if ($request->has('UPCOMING')){
            $orders = $this->serviceOrderAPIRepository->with(['user', 'driver', 'serviceCategory', 'items'])->scopeQuery(function($query){
                return $query->whereIn('order_status', ['ASSIGNED','ACCEPTED','REACHED','STARTED','FINISHED']);
            })->paginate($request->per_page ?? 10);
        }else if ($request->has('COMPLETED')){
            $orders = $this->serviceOrderAPIRepository->with(['user', 'driver', 'serviceCategory', 'items'])->scopeQuery(function($query){
                return $query->whereIn('order_status', ['COMPLETED']);
            })->paginate($request->per_page ?? 10);
        }else{
            $orders = $this->serviceOrderAPIRepository->with(['user', 'driver', 'serviceCategory', 'items'])->paginate($request->per_page ?? 10);
        }

        return $this->sendResponse(
            $orders->toArray(),
            __('lang.messages.retrieved', ['model' => __('models/orders.plural')])
        );
    }

    /**
     *Service create new order
     *
     * @authenticated
     * @bodyParam payment_method string required .
     * @bodyParam timeslots_id string required .
     * @bodyParam service_date string required .
     * @bodyParam payment_id string optional  require if online
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,HomeOrderService $orderService)
    {
        $request->validate([
            'payment_method' => 'required',
            'timeslots_id' => 'required',
            'service_date' => 'required',
        ]);
        $user = auth()->user();
        if ($user == null) {
            return $this->sendError(__('lang.messages.unauthorized'));
        }
        //find user's cart
        $cart = ServiceCart::with('coupon')->where('user_id', $user->id)->first();
        if (empty($cart)) {
            return $this->sendError("Cart is empty");
        }

        if ($cart->service_address == null) {
            return $this->sendError('Please set address before orders');
        }
        if ($cart == null) {
            return $this->sendError(__('lang.messages.cart_not_found'));
        }
        try {
            $service_category_id = $cart->home_service_category_id;
            $serviceCategory = HomeServiceCategory::find($service_category_id);
        } catch (\Exception$e) {
            return $this->sendError('Could not find service');
        }

        $payment_status = "PENDING";
        $payment_id = $request->input('payment_id');

        if ($request->payment_method == "COD") {
            $payment = Payment::create([
                'payment_request_id' => null,
                'ref_no' => \Helpers::invoice_id(),
                'user_id' => $user->id,
                'user_type' => get_class($user),
                'order_id' => null,
                'remarks' => 'Order placed using COD',
                'payment_method' => 'COD',
                'amount' => $cart->grand_total * 100,
                'gateway_response' => '',
            ]);
            $payment_id = $payment->id;
            $payment_status = "PENDING";
        } else if ($request->payment_method == "WALLET") {
            if ($user->wallet_amount < $cart->grand_total) {
                return $this->sendError(__('lang.messages.insufficient_wallet_balance'));
            } else {
                $ref_no = \Helpers::invoice_id();
                WalletHelper::debitUserWallet($user->id, $cart->grand_total, 'Order placed using wallet. Ref No: ' . $ref_no);
                $payment = Payment::create([
                    'payment_request_id' => null,
                    'ref_no' => $ref_no,
                    'user_id' => $user->id,
                    'user_type' => get_class($user),
                    'order_id' => null,
                    'remarks' => 'Order placed using wallet',
                    'payment_method' => 'WALLET',
                    'amount' => $cart->grand_total * 100,
                    'gateway_response' => '',
                ]);
                $payment_id = $payment->id;
                $payment_status = "SUCCESS";
            }
        } else if ($request->payment_method == "ONLINE") {
            if ($request->payment_id == null) {
                return $this->sendError("Payment id is required");
            }
            $payment = Payment::find($request->payment_id);
            if ($payment == null) {
                return $this->sendError("Invalid payment id");
            }
            if ($payment->gateway_response['status'] == 'captured' ?? false) {
                $payment_status = "SUCCESS";
            }
        } else {
            return $this->sendError("Invalid payment method");
        }

        $slots = TimeSlots::where('id', $request->timeslots_id)->first();
           
             $userZone = \Helpers::findInZone($cart->service_address['latitude'], $cart->service_address['longitude']);
          
            if(@$userZone[0]->id){
                $zone = $userZone[0]->id;
            }  
            else
              $zone = 2;  
        $input = array(
            'user_id' => $user->id,
            'coupon_data' => $cart->coupon == null ? null : json_decode($cart->coupon->toJson()),
            'vendor_id' => 0,
            'service_category_id' => $cart->service_category_id,
            'timeslots_id' => $request->timeslots_id,
            'total_price' => $cart->total_price,
            'discount_value' => $cart->discount_value,
            'tax' => $cart->tax,
            'grand_total' => $cart->grand_total,
            'service_date' => $request->service_date,
            'service_time' => $slots->from,
            'service_address' => $cart->service_address,
            'distance_travelled' => $cart->distance,
            'payment_id' => $payment_id,
            'order_status' => 'RECEIVED',
            'payment_status' => $payment_status,
            'zone_id' => $zone,
            'payment_method' => $request->payment_method,
            'json_data' => $cart->json_data,
            'delivery_otp' => \Helpers::randomOTP(4),
            'start_otp' => \Helpers::randomOTP(4),
        );

        $order = ServiceOrder::create($input);
        if ($cart->coupon_id != null) {
            CouponUsage::create([
                'coupon_id' => $cart->coupon_id,
                'user_id' => $user->id,
                'order_id' => $order->id,
                'coupon_code' => $cart->coupon->code ?? "N/A",
                'discount_value' => $cart->discount_value,
            ]);
        }
        $cartItems = ServiceCartItem::where('cart_id', $cart->id)->get();
        foreach ($cartItems as $cartItem) {
            $service = HomeService::find($cartItem->service_id);
            $service->save();

            ServiceOrderProducts::create([
                'service_order_id' => $order->id,
                'service_name' => $cartItem->service_name,
                'service_price' => $cartItem->service_price,
                'discount_price' => $cartItem->cart_price,
                'quantity' => $cartItem->quantity,
            ]);
        }
       
        // $orderService->calculateEarnings($order->id);
          $earning = $this->calculateEarnings($order->id);
       
        ServiceCartItem::where('cart_id', $cart->id)->delete();
        ServiceCart::where('id',$cart->id)->delete();

        // Notification::send($user, new NewOrder($order));
       $this->send_push($order->user,'Order Placed','Your order  has been Placed successfully #' . $order->id);
        $this->assignHomeServiceDriver($order->id);
      
        $order = ServiceOrder::with(['user', 'driver', 'items', 'service', 'serviceCategory'])->find($order->id);
        return $this->sendResponse(
            $order,
            __('lang.messages.saved', ['model' => __('models/orders.singular')])
        );

    }
    
      public  function assignHomeServiceDriver($order_id, $excluded_drivers = []): bool{
        try {
            $order = ServiceOrder::with('items')->find($order_id);

            $latitude = $order->service_address['latitude'] ?? 0;
            $longitude = $order->service_address['longitude'] ?? 0;
            // dd($latitude, $longitude);
            //Skip is lat long is not available
            if ($latitude == null || $longitude == null) {
                return false;
            }
             
            $drivers_list = DriverHomeServices::select('driver_id')->where('home_service_category_id', $order->service_category_id)->get();

            $drivers = Driver::
                select(DB::raw("SQRT(
                POW(69.1 * (latitude - $latitude), 2) +
                POW(69.1 * ($longitude - longitude) * COS(latitude / 57.3), 2)) AS distance"), 'id', 'on_duty', 'active', 'is_verified', 'cash_in_hand_overflow', 'cash_in_hand','fcm_token')
                ->whereNotIn('id', $excluded_drivers)
                ->whereIn('id', $drivers_list)
                ->where('active', '=', 1)
                ->where('is_verified', '=', 1)
                ->where('on_duty', '=', 1)
                ->where('is_busy', '=', 0)
                ->where('cash_in_hand_overflow', '=', 0)
                ->where('home_service_enabled', 1)
                ->having("distance", "<", setting('find_driver_in_radius', 10))
                // ->whereRaw("find_in_set(".$order->zones.",zones)")
                  ->whereRaw("FIND_IN_SET(".$order->zone_id.",zones)")
                ->orderBy("distance", 'asc')
                ->offset(0)
                ->limit(20)
                ->get();
        
            if (count($drivers) > 0) {
                //driver found
                $driver = $drivers->first();
           
                $order->driver_id = $driver->id;
                $order->order_status = "ASSIGNED";
                $order->save();

                // DB::table('autoassign_logs')->insert([
                //     'order_id' => $order_id,
                //     'assigned' => $driver->id,
                //     'in_range' => json_encode($drivers),
                //     'excluded' => json_encode($excluded_drivers),
                //     'created_at' => date('Y-m-d H:i:s'),
                //     'updated_at' => date('Y-m-d H:i:s'),
                // ]);

                Driver::where('id', $driver->id)->update(['is_busy' => 1]);
                  Notification::send($driver, new PushNotification(['title'=>'#'.$order->id.'order assigned ','description'=>'#'.$order->id.'New order Received.']));
             
                // $driver->notify(new \App\Notifications\NewOrder("Order #" . $order->id . " has been assigned to you.", $order));
                 $this->send_push($driver,'Order Assigned','New order  has been Assigned to You #' . $order->id); 
                return true;
            } else {
                DB::table('autoassign_logs')->insert([
                    'order_id' => $order_id,
                    'assigned' => null,
                    'in_range' => json_encode($drivers),
                    'excluded' => json_encode($excluded_drivers),
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                ]);
            }
            return false;
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }



 public function calculateEarnings($order_id)
    {
        try {
            
      
            $order = ServiceOrder::with('serviceCategory')->find($order_id);
         
            $coupon = $order->coupon_data;
            $driver = Driver::find($order->driver_id);
          
            $tip_commission = setting('tip_commission', 0);

            $admin_discount = 0;
            $vendor_discount = 0;

            if ($coupon != null) {
                if ($coupon['store_id'] == null) {
                    $admin_discount = $order->discount_value;
                } else {
                    $vendor_discount = $order->discount_value;
                }
            }
            
           
            if (!empty($store)) {
                //Calculate Vendor Earnings
                $vendor_commission = $order->total_price - ($order->total_price * ($store_commission / 100));
            }else{
                $vendor_commission = 0;
            }

            //Calculate Driver Earnings
            $driver_tip_commission = $order->tip - ($tip_commission / 100 * $order->tip);

            //Calculate admin Earnings
            
               $vendor_com = setting('home_service_vendor_commission', 0);
               $admin_com = $order->serviceCategory->admin_commission == null ? setting('home_service_admin_commission', 0) :$order->serviceCategory->admin_commission;
             
             
                $vendor_commission =  ($order->total_price * $vendor_com / 100) - $order->tip ;
               $admin_commission = ( $order->total_price * $admin_com / 100 ) - $order->tip ;
           
            //   $admin_commission = $order->tax + ($order->total_price - $vendor_commission) + ($order->delivery_fee - $driver_commission) + ($order->tip - $driver_tip_commission);
             
            //Calculate Driver Earnings
            $order->admin_commission = $admin_commission - $admin_discount + $order->tax;
            $order->vendor_commission = $vendor_commission ;
            $order->driver_commission = $order->total_price - ($admin_commission + $vendor_commission);
            $order->save();
            return "Admin: " . $order->admin_commission . " Vendor: " . $vendor_commission . " Driver: " . $order->driver_commission;
        } catch (\Exception $e) {
            $order = ServiceOrder::find($order_id);
            $order->admin_commission = 0;
            $order->vendor_commission = 0;
            $order->driver_commission = 0;
            $order->save();
            return $e->getMessage();
        }
    }



 public function send_push($user,$title,$desc) {
        
        
         $title = $title;
         $body = $desc;
        //  $imageurl = '';
        // $tokens = array();
        // foreach($alltoken as $token){
        //     array_push($tokens,$token['fcm_token']);
        // }
   
          $fcmkey =  DB::table('settings')->where('key_name','fcm_key')->first();
            
        // $result = array();
        // $chunks = array_chunk($tokens,999);
       
        // foreach($chunks as $chunk){
               $chunk[0] = $user->fcm_token;
        
            $url = 'https://fcm.googleapis.com/fcm/send';    
            $dataArr = array('click_action' => 'FLUTTER_NOTIFICATION_CLICK');
    
            $notification = array('title' =>$title, 'body' => $body,  'sound' => 'default', 'badge' => '1');
    
            $arrayToSend = array('registration_ids' =>$chunk , 'notification' => $notification, 'data' => $dataArr, 'priority'=>'high' );
    
            $tempArr = $arrayToSend;
    
            $fields = json_encode($arrayToSend);
           
            
            $headers = array (
                'Authorization: key=' . $fcmkey->key_value,
                'Content-Type: application/json'
    
            );
    
            //Register notification in database
            $ch = curl_init ();
    
            curl_setopt ( $ch, CURLOPT_URL, $url );
    
            curl_setopt ( $ch, CURLOPT_POST, true );
    
            curl_setopt ( $ch, CURLOPT_HTTPHEADER, $headers );
    
            curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );
    
            curl_setopt ( $ch, CURLOPT_POSTFIELDS, $fields );
            $result[] = curl_exec ( $ch );
   
            //   dd($result);
            curl_close ( $ch );
           return 1;
        // }
        
    }


    public function show($id)
    {
        $order = ServiceOrder::with(['user', 'driver', 'items', 'service', 'service_category'])->find($id);
        if (empty($order)) {
            return $this->sendError(__('models/orders.singular').' not found');
        }
        return $this->sendResponse(
            $order,
            __('lang.messages.found', ['model' => __('models/orders.singular')])
        );
    }
    
    public function check_autoassign(Request $request)
     {
         $this->assignHomeServiceDriver($request->order_id);
     }

    /**
     *Service order change status
     *
     * @authenticated
     * @bodyParam id int required .
     * @bodyParam status string required
     * @return \Illuminate\Http\Response
     */

    public function update_order_status(Request $request)
    {
        $user = auth()->user();
        if (@$user) {
            $request->validate([
                'id' => 'required|numeric',
                'status' => 'required|string',
            ]);
            $st = ServiceOrder::where('id', $request->id)->update(['order_status' => $request->status]);
            $order = ServiceOrder::with(['service', 'user', 'driver', 'serviceCategory'])->where('id', $request->id)->first();
    
            // event(new OrderChanged($order));

            switch ($request->status) {
                case 'ACCEPTED':
                    // OrderHelper::assignHomeServiceDriver($order->id);
                    break;
                case 'ASSIGNED':
                    break;
                case 'RECIEVED':
                    break;
                case 'COMPLETED':
                    break;
                case 'CANCELLED':
                    break;
                case 'REACHED':
                    break;
                case 'STARTED':
                    Driver::where('id', $order->driver_id)->update(['is_busy' => true]);
                    ServiceOrder::where('id', $order->id)->update(['order_status' => 'ASSIGNED']);
                    break;
                case 'FINISHED':
                    ServiceOrder::where('id', $order->id)->update(['payment_status' => 'SUCCESS']);
                    Driver::where('id', $order->driver_id)->update(['is_busy' => false]);
                    break;
            }

            $order = ServiceOrder::with(['service', 'user', 'driver', 'serviceCategory'])->where('id', $request->id)->first();

            if (@$st) {
                  $this->send_push($order->user,'Order Status Changed','Your order status has been changed to ' . $request->status);
                // Notification::send($order->user, new PushNotification(['title' => 'Order Status Changed', 'description' => 'Your order status has been changed to ' . $request->status]));
                return $this->sendResponse(
                    $order,
                    __('lang.messages.updated', ['model' => __('models/orders.singular')])
                );
            } else {
                return $this->sendError("Order not found!!");
            }
        }
    }
}
