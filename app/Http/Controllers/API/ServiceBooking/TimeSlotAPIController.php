<?php

namespace App\Http\Controllers\API\ServiceBooking;

use App\Http\Controllers\AppBaseController;
use App\Models\ServiceOrder;
use App\Models\TimeSlots;
use Illuminate\Http\Request;
use Response;


/**
 * Class TimeSlotAPIController
 * @package App\Http\Controllers\API
 * @group ServiceTimeSlots
 */



class TimeSlotAPIController extends AppBaseController
{

    /**
     *Service Get Slots
     *
     * @authenticated
     * @bodyParam from string required
     * @bodyParam to string required
     * @return \Illuminate\Http\Response
     */

    public function index(Request $request)
    {
        $response['status'] = false;
        $response['msg'] = "Invalid start and end date";
        $response['data'] = [];
        if (!empty($request->from) && !empty($request->to)) {

            $resp = array();
            $start_date = $request->from;
            $end_date = $request->to;
            $current_time = floatval(str_replace(":", ".", $request->current_time));

            // $this->db->select("*");
            // $this->db->from('sma_timeslots');
            // $allslots = $this->db->get()->result();

            $allslots = TimeSlots::get();

            while(strtotime($start_date) <= strtotime($end_date)) {
                $push = array();
                $timestamp = strtotime($start_date);
                $day = date('l', $timestamp);

                $date = date("d-M-Y", strtotime("+0 days", strtotime($start_date)));

                $push['date'] = $date;
                $push['day'] = $day;
                $push['items'] = array();
                
                foreach ($allslots as $arr) {
                    if ($current_time >= floatval($arr->from)) {
                        continue;
                    }
                    $temp = array();
                    $temp['id'] = $arr->id;
                    $temp['day'] = $arr->day;
                    //$temp['date'] = $arr->date;
                    $temp['from'] = (double) $arr->from;
                    $temp['to'] = (double) $arr->to;
                    $temp['max_bookings'] = $arr->max_bookings;
                    if ($temp['day'] == ucfirst($day)) {
                        $bookings = ServiceOrder::where('service_date', $date)->where('service_time', $arr->from)->count();
                        $temp['date'] = $date;
                        $temp['bookings'] = $bookings;
                        array_push($push['items'], $temp);
                    }
                }

                array_push($resp, $push);
                $start_date = date("d-M-Y", strtotime("+1 days", strtotime($start_date)));
            }
            $response['status'] = true;
            $response['msg'] = "Timeslot fetched successfully";
            $response['data'] = $resp;
            return $response;
        } else {
            return $response;
        }

    }

    public function get_array_by_day($array, $key, $date)
    {
        $final = array();
        foreach ($array as $arr) {
            if ($arr->day == $key) {
                $query = $this->db->query('SELECT * FROM sma_sales WHERE delivery_date = "' . $date . '" AND time_slot_id = ' . $arr->id);
                $arr->bookings = 'SELECT * FROM sma_sales WHERE delivery_date = "' . $date . '" AND time_slot_id = ' . $arr->id;
                array_push($final, $arr);
            }
        }
        return $final;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

}
