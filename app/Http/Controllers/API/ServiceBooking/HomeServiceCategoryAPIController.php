<?php

namespace App\Http\Controllers\API\ServiceBooking;

use App\Criteria\HomeService\InZoneCriteria;
use App\Http\Controllers\AppBaseController;
use App\Http\Requests\API\CreateHomeServiceCategoryAPIRequest;
use App\Http\Requests\API\UpdateHomeServiceCategoryAPIRequest;
use App\Models\HomeServiceCategory;
use App\Repositories\HomeServiceCategoryRepository;
use Illuminate\Http\Request;
use Prettus\Repository\Criteria\RequestCriteria;
use DB;
/**
 * Class HomeServiceCategoryController
 * @package App\Http\Controllers\API
 */

class HomeServiceCategoryAPIController extends AppBaseController
{
    /** @var  HomeServiceCategoryRepository */
    private $homeServiceCategoryRepository;

    public function __construct(HomeServiceCategoryRepository $homeServiceCategoryRepo)
    {
        $this->homeServiceCategoryRepository = $homeServiceCategoryRepo;
    }

    /**
     * Display a listing of the HomeServiceCategory.
     * GET|HEAD /homeServiceCategories
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $zone_id = [];
        if ($request->has('latitude') && $request->has('longitude')) {
            $zones = \Helpers::findInZone($request->latitude, $request->longitude);
            if (!empty($zones)) {
                $zone_id = $zones[0]->id;
            }  else{
                return $this->sendError(
                    __('lang.messages.not_found', ['model' => __('models/homeServiceCategories.singular')])
                );
            }
        }
        $this->homeServiceCategoryRepository->pushCriteria(new InZoneCriteria($zone_id));
        $this->homeServiceCategoryRepository->pushCriteria(new RequestCriteria($request));
        $homeServiceCategories = $this->homeServiceCategoryRepository->with(['children','children.children'])->all();
        //  set payment method
      
        foreach ($homeServiceCategories as $key => $value) {
            if(!empty($value->payment_method))
            {
                   $method = $value->payment_method;
              $method = explode(',', $method);
              foreach ($method as $key1 => $value1) {
                    $payment[$key1] =DB::table('payment_methods')->where('id',$value1)->first();
              }
             $homeServiceCategories[$key]->payment_method = $payment;
            }
          
        
        }
       
        return $this->sendResponse(
            $homeServiceCategories->toArray(),
            __('lang.messages.retrieved', ['model' => __('models/homeServiceCategories.plural')])
        );
    }

    /**
     * Store a newly created HomeServiceCategory in storage.
     * POST /homeServiceCategories
     *
     * @param CreateHomeServiceCategoryAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateHomeServiceCategoryAPIRequest $request)
    {
        $input = $request->all();
        
        $homeServiceCategory = $this->homeServiceCategoryRepository->create($input);

        return $this->sendResponse(
            $homeServiceCategory->toArray(),
            __('lang.messages.saved', ['model' => __('models/homeServiceCategories.singular')])
        );
    }

    /**
     * Display the specified HomeServiceCategory.
     * GET|HEAD /homeServiceCategories/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var HomeServiceCategory $homeServiceCategory */
        $homeServiceCategory = $this->homeServiceCategoryRepository->find($id);

        if (empty($homeServiceCategory)) {
            return $this->sendError(
                __('lang.messages.not_found', ['model' => __('models/homeServiceCategories.singular')])
            );
        }

        return $this->sendResponse(
            $homeServiceCategory->toArray(),
            __('lang.messages.retrieved', ['model' => __('models/homeServiceCategories.singular')])
        );
    }

    /**
     * Update the specified HomeServiceCategory in storage.
     * PUT/PATCH /homeServiceCategories/{id}
     *
     * @param int $id
     * @param UpdateHomeServiceCategoryAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateHomeServiceCategoryAPIRequest $request)
    {
        $input = $request->all();

        /** @var HomeServiceCategory $homeServiceCategory */
        $homeServiceCategory = $this->homeServiceCategoryRepository->find($id);

        if (empty($homeServiceCategory)) {
            return $this->sendError(
                __('lang.messages.not_found', ['model' => __('models/homeServiceCategories.singular')])
            );
        }

        $homeServiceCategory = $this->homeServiceCategoryRepository->update($input, $id);

        return $this->sendResponse(
            $homeServiceCategory->toArray(),
            __('lang.messages.updated', ['model' => __('models/homeServiceCategories.singular')])
        );
    }

    /**
     * Remove the specified HomeServiceCategory from storage.
     * DELETE /homeServiceCategories/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var HomeServiceCategory $homeServiceCategory */
        $homeServiceCategory = $this->homeServiceCategoryRepository->find($id);

        if (empty($homeServiceCategory)) {
            return $this->sendError(
                __('lang.messages.not_found', ['model' => __('models/homeServiceCategories.singular')])
            );
        }

        $homeServiceCategory->delete();

        return $this->sendResponse(
            $id,
            __('lang.messages.deleted', ['model' => __('models/homeServiceCategories.singular')])
        );
    }
}
