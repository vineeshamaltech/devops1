<?php

namespace App\Http\Controllers\API\ServiceBooking;

use App\Http\Controllers\AppBaseController;
use App\Models\Address;
use App\Models\HomeService;
use App\Models\HomeServiceCategory;
use App\Models\ServiceCart;
use App\Models\ServiceCartItem;
use Illuminate\Http\Request;

/**
 * Class ServiceCartApiController
 * @package App\Http\Controllers\API
 * @group ServiceCart
 */

class ServiceCartAPIController extends AppBaseController
{
    /**
     *Service Get Cart
     *
     * @authenticated

     * @return \Illuminate\Http\Response
     */

    public function getCart(Request $request)
    {
        $user = auth()->user();
        if (@$user) {
            $cart = ServiceCart::with('items' )->where('user_id', $user->id)->first();

            if (@$cart->items) {$cartItems = $cart->items;
                $cartItems = $cart->items;
                $temp = [];
                foreach ($cartItems as $key => $value) {
                    $value->service_info = HomeService::where('id', $value->service_id)->first();
                    $temp = $value;
                }
                $cart->items = $temp->toJson();
            }

            if (@$cart) {
                return $this->sendResponse($cart, 'service cart fetch successfully');
            } else {
                return $this->sendResponse(null, 'service cart not found!!');
            }

        }
    }
//
    /**
     *Service Add to Cart
     *
     * @authenticated
     * @bodyParam service_id int required The id of the service.
     * @bodyParam quantity int required The quantity of the service.
     * @return \Illuminate\Http\Response
     */

    public function addtocart(Request $request)
    {
        $request->validate([
            'service_id' => 'required|integer',
            'quantity' => 'required|integer',
        ]);
        $user = auth()->user();
        if (empty($user)) {
            return $this->sendError('Unauthorized', 401);
        }

        $cart = ServiceCart::where('user_id', $user->id)->first();
        $service = HomeService::where('id', $request->service_id)->first();

        if (empty($service)) {
            return $this->sendError('Service not found', 404);
        }

        if (empty($cart)) {
            $cart = new ServiceCart();
            $cart->service_category_id = $service->home_service_category_id;
            $cart->user_id = $user->id;
            $cart->save();
        }
        $serviceCat = HomeServiceCategory::where('id', $service->home_service_category_id)->first();
        $cartItem = ServiceCartItem::where('cart_id', $cart->id)->where('service_id', $request->service_id)->first();
        if (empty($cartItem)) {
            if ($request->quantity == 0) {
                return $this->sendError('quantity can not be 0.', 401);
            }
            if ($cart->service_category_id != $service->home_service_category_id) {
                return $this->sendError('You cannot add services from multiple categories in one order', 401);
            }

            $cartItem = new ServiceCartItem();
            $cartItem->cart_id = $cart->id;
            $cartItem->service_id = $request->service_id;
            $cartItem->quantity = $request->quantity;
            $cartItem->service_name = $service->name;
            $cartItem->service_price = $service->original_price;
            $cartItem->cart_price = $service->discount_price;
            $cartItem->tax = $serviceCat->tax ? $serviceCat->tax : 0;
            $cartItem->save();
        } else {
            if ($request->quantity == 0) {
                $cartItem->delete();
                $cart->delete();
            }
            $cartItem->quantity = $request->quantity;
            $cartItem->save();
        }
        $this->recalculateServiceCart();

        $cart = ServiceCart::with(['coupon', 'items'])->where('user_id', $user->id)->first();

        if (@$cart->items) {$cartItems = $cart->items;
            $temp = [];
            foreach ($cartItems as $key => $value) {
                $value->service_info = HomeService::where('id', $value->service_id)->first();
                $temp = $value;
            }
            $cart->items = $temp->toJson();
        }
        return $this->sendResponse($cart, 'Operation successful');
    }

    /**
     *Service set Address to Cart
     *
     * @authenticated
     * @bodyParam addresss string required .

     * @return \Illuminate\Http\Response
     */

    public function setAddress(Request $req)
    {
        $req->validate([
            'address' => 'required',
        ]);
        $user = auth()->user();
        if (empty($user)) {
            return $this->sendError('Unauthorized', 401);
        }
        if (@$user) {
            $cartdata = ServiceCart::where('user_id', $user->id)->first();
            if (empty($cartdata)) {
                return $this->sendError('Cart not found', 401);
            }
            $cartdata->service_address = json_decode($req->address);
            $cartdata->save();
            if (@$cartdata) {

                $cart = ServiceCart::with(['coupon', 'items'])->where('user_id', $user->id)->first();

                if (@$cart->items)
                {
                    $cartItems = $cart->items;
                    $temp = [];
                    foreach ($cartItems as $key => $value) {

                        $value->service_info = HomeService::where('id', $value->service_id)->first();

                        $temp = $value;
                    }

                    $cart->items = $temp->toJson();

                }

                return $this->sendResponse($cart, 'Address updated successfully');

            }
        }
        return $this->sendError( 'Cart not found');

    }

    public function recalculateServiceCart()
    {
        $user = auth()->user();

        $cart = ServiceCart::where('user_id', $user->id)->first();

        if (empty($cart)) {
            return $this->sendError('Cart not found', 404);
        }

        $grand_total = 0.0;
        $total_tax = 0.0;
        $tax = 0.0;
        $cartItems = ServiceCartItem::where('cart_id', $cart->id)->get();

        foreach ($cartItems as $cartItem) {
            $total = 0.0;
            if ($cartItem->service_id != null) {
                $service = HomeService::findOrFail($cartItem->service_id);
                $serviceCat = HomeServiceCategory::where('id', $service->home_service_category_id)->first();
                $total = $service->discount_price * $cartItem->quantity;
                $cartItem->service_name = $service->name;
                $cartItem->service_price = $service->discount_price;
                $cartItem->cart_price = $total;

                $tax = ($serviceCat->tax * $total) / 100;

                $total_tax += $tax;
                $grand_total += $total + $tax;
                $cartItem->tax = $tax;
                $cartItem->save();
            }
        }

        if ($cart->coupon_id != null) {
            $coupon = Coupon::find($cart->coupon_id);
            if ($coupon->discount_type == 'FLAT') {
                $cart->discount_value = $cart->total_price * ($coupon->discount_value / 100);
                if ($cart->discount_value > $coupon->max_discount_value) {
                    $cart->discount_value = $coupon->max_discount_value;
                }
            }
        } else {
            $cart->discount_value = 0;
        }

        $cart->total_price = $grand_total - $total_tax;
        $cart->tax = $total_tax;
        $cart->grand_total = ($grand_total - $cart->discount_value);

        $cart->save();

    }

    /**
     *  Clear Cart.
     *
     * @authenticated
     * @return \Illuminate\Http\Response
     */

    public function clearCart()
    {
        $user = auth()->user();
        if (empty($user)) {
            return $this->sendError('Unauthorized', 401);
        }

        $cart = ServiceCart::where('user_id', $user->id)->first();
        if (@$cart)
        {
            ServiceCartItem::where('cart_id', $cart->id)->delete();
            $cart->delete();
        } else {
            return $this->sendResponse(null, 'Cart empty now');
        }

        return $this->sendResponse(null, 'Cart cleared successfully');
    }

    /**
     *  Refresh Cart.
     *
     * @authenticated
     * @return \Illuminate\Http\Response
     */

    public function refresh_cart()
    {
        $user = auth()->user();
        // $data = $this->calculateDeliveryCharge();
        if ($data['error_code'] != '0') {
            // ServiceCart::where('user_id',$user->id)->update(['delivery_fee'=>-1]);
            return $this->sendError($data['error_message'], 404);
        }
        $this->recalculateCart();
        $cart = ServiceCart::with(['coupon', 'address'])->where('user_id', $user->id)->first();
        return $this->sendResponse($cart, 'Service changed successfully');
    }

}
