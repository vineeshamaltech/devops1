<?php

namespace App\Http\Controllers\API\ServiceBooking;

use App\Http\Controllers\AppBaseController;
use App\Http\Requests\API\CreateHomeServiceAPIRequest;
use App\Http\Requests\API\UpdateHomeServiceAPIRequest;
use App\Models\HomeService;
use App\Repositories\HomeServiceRepository;
use Illuminate\Http\Request;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use function __;

/**
 * Class HomeServiceController
 * @package App\Http\Controllers\API
 */

class HomeServiceAPIController extends AppBaseController
{
    /** @var  HomeServiceRepository */
    private $homeServiceRepository;

    public function __construct(HomeServiceRepository $homeServiceRepo)
    {
        $this->homeServiceRepository = $homeServiceRepo;
    }

    /**
     * Display a listing of the HomeService.
     * GET|HEAD /homeServices
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->homeServiceRepository->pushCriteria(new RequestCriteria($request));
        $homeServices = $this->homeServiceRepository->all();

        return $this->sendResponse(
            $homeServices->toArray(),
            __('lang.messages.retrieved', ['model' => __('models/homeServices.plural')])
        );
    }

    /**
     * Store a newly created HomeService in storage.
     * POST /homeServices
     *
     * @param CreateHomeServiceAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateHomeServiceAPIRequest $request)
    {
        $input = $request->all();

        $homeService = $this->homeServiceRepository->create($input);

        return $this->sendResponse(
            $homeService->toArray(),
            __('lang.messages.saved', ['model' => __('models/homeServices.singular')])
        );
    }

    /**
     * Display the specified HomeService.
     * GET|HEAD /homeServices/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var HomeService $homeService */
        $homeService = $this->homeServiceRepository->find($id);

        if (empty($homeService)) {
            return $this->sendError(
                __('lang.messages.not_found', ['model' => __('models/homeServices.singular')])
            );
        }

        return $this->sendResponse(
            $homeService->toArray(),
            __('lang.messages.retrieved', ['model' => __('models/homeServices.singular')])
        );
    }

    /**
     * Update the specified HomeService in storage.
     * PUT/PATCH /homeServices/{id}
     *
     * @param int $id
     * @param UpdateHomeServiceAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateHomeServiceAPIRequest $request)
    {
        $input = $request->all();

        /** @var HomeService $homeService */
        $homeService = $this->homeServiceRepository->find($id);

        if (empty($homeService)) {
            return $this->sendError(
                __('lang.messages.not_found', ['model' => __('models/homeServices.singular')])
            );
        }

        $homeService = $this->homeServiceRepository->update($input, $id);

        return $this->sendResponse(
            $homeService->toArray(),
            __('lang.messages.updated', ['model' => __('models/homeServices.singular')])
        );
    }

    /**
     * Remove the specified HomeService from storage.
     * DELETE /homeServices/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var HomeService $homeService */
        $homeService = $this->homeServiceRepository->find($id);

        if (empty($homeService)) {
            return $this->sendError(
                __('lang.messages.not_found', ['model' => __('models/homeServices.singular')])
            );
        }

        $homeService->delete();

        return $this->sendResponse(
            $id,
            __('lang.messages.deleted', ['model' => __('models/homeServices.singular')])
        );
    }
}
