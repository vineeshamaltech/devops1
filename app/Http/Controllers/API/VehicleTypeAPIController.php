<?php

namespace App\Http\Controllers\API;
use App\Criteria\ActiveCriteria;
use App\Criteria\LimitOffsetCriteria;
use App\Http\Requests\API\CreateVehicleTypeAPIRequest;
use App\Http\Requests\API\UpdateVehicleTypeAPIRequest;
use App\Models\ServiceCategory;
use App\Models\ServiceCategoryVehicleType;
use App\Models\VehicleType;
use App\Repositories\VehicleTypeRepository;
use App\Services\CartService;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class VehicleTypeController
 * @package App\Http\Controllers\API
 * @group VehicleTypes
 */

class VehicleTypeAPIController extends AppBaseController
{
    /** @var  VehicleTypeRepository */
    private $vehicleTypeRepository;
    private $cartService;

    public function __construct(VehicleTypeRepository $vehicleTypeRepo,CartService $cartService)
    {
        $this->cartService = $cartService;
        $this->vehicleTypeRepository = $vehicleTypeRepo;
    }

    /**
     * Display a listing of the VehicleType.
     * GET|HEAD /vehicleTypes
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        if (!empty($request->service_category_id)){
            $service_category = ServiceCategory::with('vehicles')->find($request->service_category_id);
            $vehicles = $service_category->vehicles;
            $vehicles_arr = [];
            foreach ($vehicles as $vehicle){
                    $url = "https://maps.googleapis.com/maps/api/distancematrix/json?destinations=".$request->drop_lat.",".$request->drop_long."&origins=".$request->pickup_lat.",".$request->pickup_long."&mode=driving&language=en-US&key=".setting('google_map_api_key');
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $response = curl_exec($ch);
        curl_close($ch);

            $response_a = json_decode($response, true);
            $dist = $response_a['rows'][0]['elements'][0]['distance']['value'];
            $time = $response_a['rows'][0]['elements'][0]['duration']['value'];
            $distance = $dist/1000;
                // $distance = $this->cartService->calculateDistance($request->pickup_lat,$request->pickup_long,$request->drop_lat,$request->drop_long);
                $vehicle->distance = $distance;
                if ($vehicle->distance <= 0){
                    continue;
                }
                $vehicle->fare = $this->calculateFare($service_category,$distance);
                $vehicles_arr[] = $vehicle;
            }
            return $this->sendResponse($vehicles_arr, 'Vehicle Types retrieved successfully');
        }else{
            $this->vehicleTypeRepository->pushCriteria(new RequestCriteria($request));
            $this->vehicleTypeRepository->pushCriteria(new LimitOffsetCriteria($request));
            $this->vehicleTypeRepository->pushCriteria(new ActiveCriteria());
            $vehicleTypes = $this->vehicleTypeRepository->all();
            return $this->sendResponse(
                $vehicleTypes->toArray(),
                __('lang.messages.retrieved', ['model' => __('models/vehicleTypes.plural')])
            );
        }
    }

    public function get_vehicles(Request $request){
        $request->validate([
            'service_category_id' => 'required',
            'pickup_lat' => 'required',
            'pickup_long' => 'required',
            'drop_lat' => 'required',
            'drop_long' => 'required',
        ]);
        $service_category = ServiceCategory::with('vehicles')->find($request->service_category_id);
        $vehicle_ids = ServiceCategoryVehicleType::where('service_category_id',$request->service_category_id)->get()->pluck('vehicle_type_id')->toArray();
        $vehicles = VehicleType::whereIn('id',$vehicle_ids)->get();
        if (empty($vehicles)){
            return $this->sendError('Vehicles not found');
        }
        $vehicles_arr = [];
        foreach ($vehicles as $vehicle){
             $url = "https://maps.googleapis.com/maps/api/distancematrix/json?destinations=".$request->drop_lat.",".$request->drop_long."&origins=".$request->pickup_lat.",".$request->pickup_long."&mode=driving&language=en-US&key=".setting('google_map_api_key');
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $response = curl_exec($ch);
        curl_close($ch);

            $response_a = json_decode($response, true);
            $dist = $response_a['rows'][0]['elements'][0]['distance']['value'];
            $time = $response_a['rows'][0]['elements'][0]['duration']['value'];
            $distance = $dist/1000;
            // $distance = $this->cartService->calculateDistance($request->pickup_lat,$request->pickup_long,$request->drop_lat,$request->drop_long);
            $vehicle->distance = $distance;
            $vehicle->time = $time;
            if ($vehicle->distance <= 0){
                continue;
            }
            $vehicle->fare = $this->calculateFare($vehicle,$distance);
            if ($vehicle->fare <= 0){
                continue;
            }
            $vehicles_arr[] = $vehicle;
        }
        return $this->sendResponse($vehicles_arr, 'Vehicle Types retrieved successfully');
    }

    public function calculateFare($vehicle,$km_to_travel){
        $baseDistance = $vehicle->base_km;
        $basePrice = $vehicle->base_price;
        $pricePerKm = $vehicle->price_km;
        if($baseDistance < $km_to_travel)
         $basePrice = $basePrice + ($km_to_travel - $baseDistance) * $pricePerKm;
          else
            $basePrice = $basePrice;
            
            // $basePrice = 0;
            
        return $basePrice;
    }

    /**
     * Store a newly created VehicleType in storage.
     * POST /vehicleTypes
     *
     * @param CreateVehicleTypeAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateVehicleTypeAPIRequest $request)
    {
        $input = $request->all();

        $vehicleType = $this->vehicleTypeRepository->create($input);

        return $this->sendResponse(
            $vehicleType->toArray(),
            __('lang.messages.saved', ['model' => __('models/vehicleTypes.singular')])
        );
    }

    /**
     * Display the specified VehicleType.
     * GET|HEAD /vehicleTypes/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var VehicleType $vehicleType */
        $vehicleType = $this->vehicleTypeRepository->find($id);

        if (empty($vehicleType)) {
            return $this->sendError(
                __('lang.messages.not_found', ['model' => __('models/vehicleTypes.singular')])
            );
        }

        return $this->sendResponse(
            $vehicleType->toArray(),
            __('lang.messages.retrieved', ['model' => __('models/vehicleTypes.singular')])
        );
    }

    /**
     * Update the specified VehicleType in storage.
     * PUT/PATCH /vehicleTypes/{id}
     *
     * @param int $id
     * @param UpdateVehicleTypeAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateVehicleTypeAPIRequest $request)
    {
        $input = $request->all();

        /** @var VehicleType $vehicleType */
        $vehicleType = $this->vehicleTypeRepository->find($id);

        if (empty($vehicleType)) {
            return $this->sendError(
                __('lang.messages.not_found', ['model' => __('models/vehicleTypes.singular')])
            );
        }

        $vehicleType = $this->vehicleTypeRepository->update($input, $id);

        return $this->sendResponse(
            $vehicleType->toArray(),
            __('lang.messages.updated', ['model' => __('models/vehicleTypes.singular')])
        );
    }

    /**
     * Remove the specified VehicleType from storage.
     * DELETE /vehicleTypes/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var VehicleType $vehicleType */
        $vehicleType = $this->vehicleTypeRepository->find($id);

        if (empty($vehicleType)) {
            return $this->sendError(
                __('lang.messages.not_found', ['model' => __('models/vehicleTypes.singular')])
            );
        }

        $vehicleType->delete();

        return $this->sendResponse(
            $id,
            __('lang.messages.deleted', ['model' => __('models/vehicleTypes.singular')])
        );
    }
}
