<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateEarningAPIRequest;
use App\Http\Requests\API\UpdateEarningAPIRequest;
use App\Models\Earning;
use App\Repositories\EarningRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class EarningController
 * @package App\Http\Controllers\API
 */

class EarningAPIController extends AppBaseController
{
    /** @var  EarningRepository */
    private $earningRepository;

    public function __construct(EarningRepository $earningRepo)
    {
        $this->earningRepository = $earningRepo;
    }

    /**
     * Display a listing of the Earning.
     * GET|HEAD /earnings
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $earnings = $this->earningRepository->all();

        return $this->sendResponse(
            $earnings->toArray(),
            __('lang.messages.retrieved', ['model' => __('models/earnings.plural')])
        );
    }

    /**
     * Store a newly created Earning in storage.
     * POST /earnings
     *
     * @param CreateEarningAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateEarningAPIRequest $request)
    {
        $input = $request->all();

        $earning = $this->earningRepository->create($input);

        return $this->sendResponse(
            $earning->toArray(),
            __('lang.messages.saved', ['model' => __('models/earnings.singular')])
        );
    }

    /**
     * Display the specified Earning.
     * GET|HEAD /earnings/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Earning $earning */
        $earning = $this->earningRepository->find($id);
       
        if (empty($earning)) {
            return $this->sendError(
                __('lang.messages.not_found', ['model' => __('models/earnings.singular')])
            );
        }

        return $this->sendResponse(
            $earning->toArray(),
            __('lang.messages.retrieved', ['model' => __('models/earnings.singular')])
        );
    }

    /**
     * Update the specified Earning in storage.
     * PUT/PATCH /earnings/{id}
     *
     * @param int $id
     * @param UpdateEarningAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateEarningAPIRequest $request)
    {
        $input = $request->all();

        /** @var Earning $earning */
        $earning = $this->earningRepository->find($id);

        if (empty($earning)) {
            return $this->sendError(
                __('lang.messages.not_found', ['model' => __('models/earnings.singular')])
            );
        }

        $earning = $this->earningRepository->update($input, $id);

        return $this->sendResponse(
            $earning->toArray(),
            __('lang.messages.updated', ['model' => __('models/earnings.singular')])
        );
    }

    /**
     * Remove the specified Earning from storage.
     * DELETE /earnings/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Earning $earning */
        $earning = $this->earningRepository->find($id);

        if (empty($earning)) {
            return $this->sendError(
                __('lang.messages.not_found', ['model' => __('models/earnings.singular')])
            );
        }

        $earning->delete();

        return $this->sendResponse(
            $id,
            __('lang.messages.deleted', ['model' => __('models/earnings.singular')])
        );
    }
}
