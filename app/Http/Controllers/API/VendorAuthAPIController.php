<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\AppBaseController;
use App\Http\Controllers\Controller;
use App\Models\Admin;
use App\Models\Driver;
use App\Models\User;
use App\Models\Vendor;
use Craftsys\Msg91\Facade\Msg91;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Mail\Message;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Password;

/**
 * Class VendorAuthAPIController
 * @package App\Http\Controllers\API
 * @group Vendor Authentication
 */

class VendorAuthAPIController extends AppBaseController
{

    /**
     * Vendor Login
     *
     * @bodyParam mobile string required Email of the vendor.
     * @bodyParam password string required Password of the vendor.
     */
    public function login(Request $request)
    {
        $request->validate([
            'email' => 'required|string|email',
            'password' => 'required|string',
            'fcm_token' => 'required|string'
        ]);
        $credentials = request(['email', 'password']);
        if (!Auth::guard()->attempt($credentials)) {
            return $this->sendError( 'Incorrect email or password');
        }
        $vendor = auth()->user();
        if($vendor == null){
            return $this->sendError('Vendor not found');
        }
        Admin::where('user_id', $vendor->id)->update(['fcm_token' => $request->fcm_token]);
        $vendor->token = $vendor->createToken('vendor_authentication_token')->plainTextToken;
        $vendor->token_type = 'Bearer';
        return $this->sendResponse($vendor, 'Login Successful');
    }

//    /**
//     * Vendor Login
//     *
//     * This will send a verification code to the vendor's phone number via MSG91. You can only send the is_voice parameter as true if you are resending the OTP.
//     * @bodyParam  mobile string required Phone no of vendor in the format +919999999999
//     * @return Response
//     * */
//    public function phoneLogin(Request $request)
//    {
//        $request->validate([
//            'mobile' => 'required|string|min:13|max:13',
//            'is_voice' => 'nullable|boolean'
//        ]);
//        $country_code = substr($request->mobile, 0, 3);
//        $mobile = substr($request->mobile, 3);
//        $vendor = Admin::where('mobile', $mobile)->where('country_code',$country_code)->first();
//        if($vendor == null){
//            return $this->sendError(' not found: '.$mobile);
//        }
//        if($request->is_voice){
//            $phone = str_replace('+', '', $request->mobile);
//            $res = Msg91::otp()->to($phone)->viaVoice()->resend();
//            if ($res->status_code == "200"){
//                return $this->sendResponse($res, 'OTP Sent Successfully');
//            }else{
//                return $this->sendError('Error sending OTP');
//            }
//        }else{
//            $phone = str_replace('+', '', $request->mobile);
//            $res = Msg91::otp()->to($phone)->send();
//            if ($res->status_code == "200"){
//                return $this->sendResponse($res, 'OTP Sent Successfully');
//            }else{
//                return $this->sendError('Error sending OTP');
//            }
//        }
//    }

    /**
     * Verify Phone No
     *
     * @bodyParam  phone string required Phone no of vendor
     * @bodyParam  otp string required OTP sent on vendor's phone no
     * @return Response
     * */
    public function verifyOtp(Request $request){
        $request->validate([
            'mobile' => 'required|string|min:13|max:13',
            'otp' => 'required|string|min:4|max:4'
        ]);
        $country_code = substr($request->mobile, 0, 3);
        $mobile = substr($request->mobile, 3);
        $phone = str_replace('+', '', $request->mobile);
        try{
            $res = Msg91::otp((int)$request->otp)->to($phone)->verify();
            if($res->status_code == "200"){
                $vendor = Admin::where('mobile', $mobile)->first();
                if($vendor == null){
                    return $this->sendError('Vendor not found');
                }
                $vendor->tokens()->delete();
                $vendor->token = $vendor->createToken('vendor_authentication_token')->plainTextToken;
                $vendor->token_type = 'Bearer';
                return $this->sendResponse($vendor, 'Login Successful');
            }else{
                return $this->sendError('OTP is not valid');
            }
        }catch (\Exception $e){
            return $this->sendError($e->getMessage());
        }

    }

    /**
     * Vendor Registration
     *
     * @bodyParam  name string nullable Name of the vendor.
     * @bodyParam  mobile string required Phone No of the vendor in the format +919999999999. Always include the country code.
     * @bodyParam  email string nullable Email of the vendor.
     * @bodyParam  password string required Password of the vendor.
     * @bodyParam  fcm_token string nullable FCM Token.
     *
     * @return Response
     */
    public function register(Request $request)
    {
        $request->validate([
            'name' => 'required|string',
            'mobile' => 'required|string|min:13|max:13',
            'email' => 'required|string',
            'password' => 'required|string',
            'fcm_token' => 'required|string'
        ]);
        //get country code from phone no
        $country_code = substr($request->mobile, 0, 3);
        $mobile = substr($request->mobile, 3);
        if(Admin::where('mobile', $mobile)->first()){
            return $this->sendError('Mobile number already exists');
        }
        if(Admin::where('email', $request->email)->first()){
            return $this->sendError('Email already exists');
        }
        $vendor = Admin::create([
            'name' => $request->name,
            'email' => $request->email,
            'country_code' => $country_code,
            'mobile' => $mobile,
            'password' => Hash::make($request->password),
            'fcm_token' => $request->fcm_token,
            'role' => 'Vendor',
            'active' => 0,
            'verified' => 0
        ]);
        $vendor = Admin::find($vendor->id);
        $vendor->assignRole('Vendor');
        $vendor->token = $vendor->createToken('user_authentication_token')->plainTextToken;
        $vendor->token_type = 'Bearer';
        return $this->sendResponse($vendor->toArray(), 'Vendor created successfully');
    }

    //update admin profile
    public function updateProfile(Request $request){

    }

    /**
     * Reset Password
     * This will send OTP to the vendor's email
     *
     * @bodyParam  email string required Phone No of the vendor in the format +919999999999. Always include the country code.
     * @return Response
     */
    public function resetPassword(Request $request){
        $otp = \Helpers::randomOTP();
        Admin::where('email', $request->email)->update(['reset_otp' => $otp]);
        $data = [
            'title' => 'Your OTP for reset password is '.$otp,
            'subtitle' => 'Reset your password',
            'body' => 'Use this otp to reset your password. Please contact support if you have not requested this otp.',
        ];
        Mail::send('email.simple', $data, function($message) use ($request) {
            $message->to($request->email)->subject('Password Reset OTP');
        });
        return $this->sendResponse(null, 'OTP sent to your email');
    }

    /**
     * Change Password
     * This will change the vendor's password
     *
     * @bodyParam  password string required Password of the vendor.
     * @bodyParam  password_confirmation string required Confirm Password of the vendor.
     * @bodyParam  otp string required OTP for reset password.
     * @return Response
     */
    public function changePassword(Request $request){
        $request->validate([
            'password' => 'required|confirmed|string|min:6|max:20',
            'otp' => 'required|string|min:6|max:6'
        ]);
        $admin = Admin::where('reset_otp', $request->otp)->first();
        if($admin == null){
            return $this->sendError('OTP is not valid');
        }
        $admin->password = Hash::make($request->password);
        $admin->reset_otp = null;
        $admin->save();
        return $this->sendResponse(null, 'Password changed successfully');
    }

    /**
     * Vendor's Profile
     *
     * Just pass the Bearer token in the header
     * @authenticated
     * @return Response
     *
     * */
    public function profile(){
        $vendor = auth()->user();
        if ($vendor != null) {
            return $this->sendResponse($vendor->toArray(), 'Vendor profile fetched successfully');
        }else{
            return $this->sendError('Unauthenticated');
        }
    }
}
