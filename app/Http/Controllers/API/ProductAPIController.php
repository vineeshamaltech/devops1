<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateProductAPIRequest;
use App\Http\Requests\API\UpdateProductAPIRequest;
use App\Models\Product;
use App\Repositories\ProductRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class ProductController
 * @package App\Http\Controllers\API
 * @group Product
 */

class ProductAPIController extends AppBaseController
{
    /** @var  ProductRepository */
    private $productRepository;

    public function __construct(ProductRepository $productRepo)
    {
        $this->productRepository = $productRepo;
        $this->middleware('auth:sanctum')->only(['store','update', 'destroy']);
    }

    /**
     * Display a listing of the Product.
     *
     * @authenticated
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->productRepository->pushCriteria(new RequestCriteria($request));
        $products = $this->productRepository->paginate(300);

        return $this->sendResponse(
            $products->toArray(),
            __('lang.messages.retrieved', ['model' => __('models/products.plural')])
        );
    }

    /**
     * Store a newly created Product in storage.
     * POST /products
     *
     * @authenticated
     * @param CreateProductAPIRequest $request
     * @return Response
     */
    public function store(CreateProductAPIRequest $request)
    {
        // abort_if(\Gate::denies('products.create'), 403, '403 Forbidden');
        $input = $request->all();
         
        if($request->hasFile('image')){
            $input['image'] = \Helpers::uploadImage($request->file('image'), 'products');
        }
        if(empty($input['sub_category_id'])){
            $input['category_id'] = $request->category_id;
        }else{
            $input['category_id'] = $request->sub_category_id;
        }
        unset($input['sub_category_id']);
        if(@$input['opening_time'] && $input['opening_time'])
        {
             $input['opening_time'] = Carbon::parse($input['opening_time'])->format('H:i:s');
        $input['closing_time'] = Carbon::parse($input['closing_time'])->format('H:i:s'); 
        }
      
        $product = $this->productRepository->create($input);

        return $this->sendResponse(
            $product->toArray(),
            __('lang.messages.saved', ['model' => __('models/products.singular')])
        );
    }

    /**
     * Display the specified Product.
     * GET|HEAD /products/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Product $product */
        $product = $this->productRepository->find($id);

        if (empty($product)) {
            return $this->sendError(
                __('lang.messages.not_found', ['model' => __('models/products.singular')])
            );
        }

        return $this->sendResponse(
            $product->toArray(),
            __('lang.messages.retrieved', ['model' => __('models/products.singular')])
        );
    }

    /**
     * Update the specified Product in storage.
     *
     * @authenticated
     * @param int $id
     * @param UpdateProductAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateProductAPIRequest $request)
    {
        abort_if(\Gate::denies('products.edit'), 403, '403 Forbidden');
        $input = $request->all();

        /** @var Product $product */
        $product = $this->productRepository->find($id);

        if (empty($product)) {
            return $this->sendError(
                __('lang.messages.not_found', ['model' => __('models/products.singular')])
            );
        }

        if($request->hasFile('image')){
            \Helpers::deleteImage($product->image);
            $input['image'] = \Helpers::uploadImage($request->file('image'), 'products');
        }
        if (!empty($input['opening_time'])){
            $input['opening_time'] = Carbon::parse($input['opening_time'])->format('H:i:s');
        }
        if (!empty($input['closing_time'])){
            $input['closing_time'] = Carbon::parse($input['closing_time'])->format('H:i:s');
        }
        $product = $this->productRepository->update($input, $id);
        $product = $this->productRepository->with('variants')->find($id);
        return $this->sendResponse(
            $product->toArray(),
            __('lang.messages.updated', ['model' => __('models/products.singular')])
        );
    }

    /**
     * Remove the specified Product from storage.
     * DELETE /products/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        abort_if(\Gate::denies('products.delete'), 403, '403 Forbidden');
        /** @var Product $product */
        $product = $this->productRepository->find($id);
        if (empty($product)) {
            return $this->sendError(
                __('lang.messages.not_found', ['model' => __('models/products.singular')])
            );
        }
        $product->delete();
        return $this->sendResponse(
            $id,
            __('lang.messages.deleted', ['model' => __('models/products.singular')])
        );
    }
}
