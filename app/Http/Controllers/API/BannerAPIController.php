<?php

namespace App\Http\Controllers\API;

use App\Criteria\ActiveCriteria;
use App\Http\Requests\API\CreateBannerAPIRequest;
use App\Http\Requests\API\UpdateBannerAPIRequest;
use App\Models\Banner;
use App\Repositories\BannerRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class BannerController
 * @package App\Http\Controllers\API
 * @group Banner
 */

class BannerAPIController extends AppBaseController
{
    /** @var  BannerRepository */
    private $bannerRepository;

    public function __construct(BannerRepository $bannerRepo)
    {
        $this->bannerRepository = $bannerRepo;
    }

    /**
     * Display a listing of the Banner.
     * GET|HEAD /banners
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $user = auth()->user();
        $lat = $request->lat;
        $long = $request->long;
         
        $this->bannerRepository->pushCriteria(new RequestCriteria($request));
        $this->bannerRepository->pushCriteria(new ActiveCriteria());
        
        if($lat == null || $lat == "0" || $long == null || $long == "0"){
            $banners = [];
        }else{
            $userZone = \Helpers::findInZone($lat, $long);
            
            if(@$userZone[0]->id){
                if(@$request->top_picks_enabled)
                {
                    $banners = $this->bannerRepository->where('top_picks_enabled',1); 
                }
                if($request->service_category_id){
                    $banners = $this->bannerRepository->findWhere(['service_category_id' => $request->service_category_id]);
                }
                if(@$request->banner_type && $request->service_category_id)
                {
                      $banners = $this->bannerRepository->whereRaw("find_in_set(".$userZone[0]->id.",zones)")
                ->with(['serviceCategory','store','homeServiceCategory'])->where('active',1)->where('type',$request->banner_type)
                ->where(['service_category_id' => $request->service_category_id])
                ->get();
                
                }else {
                      $banners = $this->bannerRepository->whereRaw("find_in_set(".$userZone[0]->id.",zones)")
                ->with(['serviceCategory','store','homeServiceCategory'])->where('active',1)
                ->get();
                }
              
              
            }else{
                $banners = [];
            }
        }
        
        $newBanners = [];
        foreach ($banners as $banner) {
            if ($banner->type == "SERVICE" && config('constants.features.home_service_booking',false)){
                $newBanners[] = $banner;
            }
            if (($banner->type == "STORE" || $banner->type == "CATEGORY") &&
                config('constants.features.purchasing_service',false)){
                $newBanners[] = $banner;
            }
            if ($banner->type == "URL"){
                $newBanners[] = $banner;
            }
        }

        return $this->sendResponse(
            $newBanners,
            __('lang.messages.retrieved', ['model' => __('models/banners.plural')])
        );
    }

    /**
     * Store a newly created Banner in storage.
     * POST /banners
     *
     * @param CreateBannerAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateBannerAPIRequest $request)
    {
        $input = $request->all();
        $banner = $this->bannerRepository->create($input);
        return $this->sendResponse(
            $banner->toArray(),
            __('lang.messages.saved', ['model' => __('models/banners.singular')])
        );
    }

    /**
     * Display the specified Banner.
     * GET|HEAD /banners/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Banner $banner */
        $banner = $this->bannerRepository->find($id);

        if (empty($banner)) {
            return $this->sendError(
                __('lang.messages.not_found', ['model' => __('models/banners.singular')])
            );
        }

        return $this->sendResponse(
            $banner->toArray(),
            __('lang.messages.retrieved', ['model' => __('models/banners.singular')])
        );
    }

    /**
     * Update the specified Banner in storage.
     * PUT/PATCH /banners/{id}
     *
     * @param int $id
     * @param UpdateBannerAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateBannerAPIRequest $request)
    {
        $input = $request->all();
        /** @var Banner $banner */
        $banner = $this->bannerRepository->find($id);

        if (empty($banner)) {
            return $this->sendError(
                __('lang.messages.not_found', ['model' => __('models/banners.singular')])
            );
        }
        $banner = $this->bannerRepository->update($input, $id);

        return $this->sendResponse(
            $banner->toArray(),
            __('lang.messages.updated', ['model' => __('models/banners.singular')])
        );
    }

    /**
     * Remove the specified Banner from storage.
     * DELETE /banners/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Banner $banner */
        $banner = $this->bannerRepository->find($id);

        if (empty($banner)) {
            return $this->sendError(
                __('lang.messages.not_found', ['model' => __('models/banners.singular')])
            );
        }

        $banner->delete();

        return $this->sendResponse(
            $id,
            __('lang.messages.deleted', ['model' => __('models/banners.singular')])
        );
    }
}
