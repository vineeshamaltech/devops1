<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\AppBaseController;
use App\Http\Controllers\Controller;
use App\Models\Setting;
use Illuminate\Http\Request;
use DB;
/**
 * Class SettingsAPIController
 * @package App\Http\Controllers\API
 * @group Settings
 */

class SettingsAPIController extends AppBaseController
{
    /**
     * Get settings
     */
    public function index(Request $request){
        $settings = Setting::select('key_name','key_value')->whereNotIn('key_name',['razorpay_secret','google_map_api_key'])->get()->pluck('key_value', 'key_name');
  
        // $settings['car_pool_booking'] = config('constants.features.car_pool_booking');
        // $settings['home_service_booking'] = config('constants.features.home_service_booking');
        $settings['car_pool_booking'] = $settings['car_pool_show'] == "1" ? true : false;
        $settings['home_service_booking'] = $settings['home_service_show'] == "1" ? true : false;
         $settings['purchasing_service'] = config('constants.features.purchasing_service');
        $settings['passenger_transportation_service'] = config('constants.features.passenger_transportation_service');
        $settings['pick_n_drop_service'] = config('constants.features.pick_n_drop_service');
        $settings['order_anything_service'] = config('constants.features.order_anything_service');
        return $this->sendResponse($settings, 'Settings retrieved successfully.');
    }
}
