<?php

namespace App\Http\Controllers\API;

use App\Criteria\ActiveCriteria;
use App\Http\Requests\API\CreateAddressAPIRequest;
use App\Http\Requests\API\UpdateAddressAPIRequest;
use App\Models\Address;
use App\Models\Cart;
use App\Repositories\AddressRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class AddressController
 * @package App\Http\Controllers\API
 * @group Address
 */

class AddressAPIController extends AppBaseController
{
    /** @var  AddressRepository */
    private $addressRepository;

    public function __construct(AddressRepository $addressRepo)
    {
        $this->addressRepository = $addressRepo;
    }

    /**
     * Get All Addresses of User
     * GET|HEAD /addresses
     *
     * @authenticated
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->addressRepository->pushCriteria(new RequestCriteria($request));
        $user = auth()->user();
        if($user == null){
            return $this->sendError(__('lang.messages.unauthorized'), 401);
        }
        $addresses = $this->addressRepository->findWhere(['user_id'=>$user->id]);

        return $this->sendResponse(
            $addresses->toArray(),
            __('lang.messages.retrieved', ['model' => __('models/addresses.plural')])
        );
    }

    /**
     * Create Address
     *
     * @authenticated
     * @param CreateAddressAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateAddressAPIRequest $request)
    {
        $input = $request->all();

        $user = auth()->user();
        if($user == null){
            return $this->sendError(__('lang.messages.unauthorized'), 401);
        }
        $input['user_id'] = $user->id;
        if (isset($input['is_default']) && $input['is_default'] == 1) {
            Address::where('user_id', $user->id)->update(['is_default'=>0]);
        }
        $address = $this->addressRepository->create($input);
        return $this->sendResponse(
            $address->toArray(),
            __('lang.messages.saved', ['model' => __('models/addresses.singular')])
        );
    }

    /**
     * Get Address
     *
     * @authenticated
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Address $address */
        $address = $this->addressRepository->find($id);

        if (empty($address)) {
            return $this->sendError(
                __('lang.messages.not_found', ['model' => __('models/addresses.singular')])
            );
        }

        return $this->sendResponse(
            $address->toArray(),
            __('lang.messages.retrieved', ['model' => __('models/addresses.singular')])
        );
    }

    /**
     * Update Address
     *
     *
     * @authenticated
     * @param int $id
     * @param UpdateAddressAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateAddressAPIRequest $request)
    {
        $input = $request->all();
        $user = auth()->user();
        if($user == null){
            return $this->sendError(__('lang.messages.unauthorized'), 401);
        }
        /** @var Address $address */
        $address = $this->addressRepository->findWhere(['user_id'=>$user->id]);

        if (empty($address)) {
            return $this->sendError(
                __('lang.messages.not_found', ['model' => __('models/addresses.singular')])
            );
        }
        if (isset($input['is_default']) && $input['is_default'] == 1) {
            Address::where('user_id', $user->id)->update(['is_default'=>0]);
        }
        $address = $this->addressRepository->update($input, $id);

        return $this->sendResponse(
            $address->toArray(),
            __('lang.messages.updated', ['model' => __('models/addresses.singular')])
        );
    }

    /**
     * Set address as default
     *
     * @authenticated
     * @bodyParam address_id int required
     *
     * @return Response
     */
    public function setDefault(Request $request){
        $input = $request->all();
        $user = auth()->user();
        if($user == null){
            return $this->sendError(__('lang.messages.unauthorized'), 401);
        }
        $address = $this->addressRepository->findWhere(['user_id'=>$user->id, 'id'=> $input['address_id']]);
        if (empty($address)) {
            return $this->sendError(
                __('lang.messages.not_found', ['model' => __('models/addresses.singular')])
            );
        }
        Address::where('user_id', $user->id)->update(['is_default'=>0]);
        Address::where('id', $input['address_id'])->update(['is_default'=>1]);
        $address = Address::find($input['address_id']);
        return $this->sendResponse(
            $address,
            __('lang.messages.updated', ['model' => __('models/addresses.singular')])
        );
    }

    /**
     * Delete Address
     *
     *
     * @authenticated
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Address $address */
        $address = $this->addressRepository->find($id);

        if (empty($address)) {
            return $this->sendError(
                __('lang.messages.not_found', ['model' => __('models/addresses.singular')])
            );
        }
        if (Cart::where('address_id', $id)->exists()) {
            return $this->sendError("You can't delete this address because it is used in your cart");
        }
        $address->delete();
        return $this->sendResponse(
            null,
            __('lang.messages.deleted', ['model' => __('models/addresses.singular')])
        );
    }
}
