<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateStoreTimingAPIRequest;
use App\Http\Requests\API\UpdateStoreTimingAPIRequest;
use App\Models\StoreTiming;
use App\Repositories\StoreTimingRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class StoreTimingController
 * @package App\Http\Controllers\API
 * @group StoreTiming
 */

class StoreTimingAPIController extends AppBaseController
{
    /** @var  StoreTimingRepository */
    private $storeTimingRepository;

    public function __construct(StoreTimingRepository $storeTimingRepo)
    {
        $this->storeTimingRepository = $storeTimingRepo;
    }

    /**
     * Display a listing of the StoreTiming.
     * GET|HEAD /storeTimings
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->storeTimingRepository->pushCriteria(new RequestCriteria($request));
        $storeTimings = $this->storeTimingRepository->all();

        return $this->sendResponse(
            $storeTimings->toArray(),
            __('lang.messages.retrieved', ['model' => __('models/storeTimings.plural')])
        );
    }

    /**
     * Store a newly created StoreTiming in storage.
     * POST /storeTimings
     *
     * @param CreateStoreTimingAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateStoreTimingAPIRequest $request)
    {
        $input = $request->all();
        $this->storeTimingRepository->deleteWhere(['store_id' => $input['store_id']]);
        for ($i = 0; $i < count($input['day']); $i++) {
            $data = [
                'store_id' => $input['store_id'],
                'day' => $request->input('day')[$i],
                'open' => Carbon::parse($request->input('open')[$i])->format('H:i:s'),
                'close' => Carbon::parse($request->input('close')[$i])->format('H:i:s')
            ];
            $this->storeTimingRepository->create($data);
        }
        $storeTiming = $this->storeTimingRepository->findWhere(['store_id' => $input['store_id']]);
        return $this->sendResponse(
            $storeTiming->toArray(),
            __('lang.messages.saved', ['model' => __('models/storeTimings.singular')])
        );
    }

    /**
     * Display the specified StoreTiming.
     * GET|HEAD /storeTimings/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var StoreTiming $storeTiming */
        $storeTiming = $this->storeTimingRepository->find($id);

        if (empty($storeTiming)) {
            return $this->sendError(
                __('lang.messages.not_found', ['model' => __('models/storeTimings.singular')])
            );
        }

        return $this->sendResponse(
            $storeTiming->toArray(),
            __('lang.messages.retrieved', ['model' => __('models/storeTimings.singular')])
        );
    }

    /**
     * Update the specified StoreTiming in storage.
     * PUT/PATCH /storeTimings/{id}
     *
     * @param int $id
     * @param UpdateStoreTimingAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateStoreTimingAPIRequest $request)
    {
        $input = $request->all();

        /** @var StoreTiming $storeTiming */
        $storeTiming = $this->storeTimingRepository->find($id);

        if (empty($storeTiming)) {
            return $this->sendError(
                __('lang.messages.not_found', ['model' => __('models/storeTimings.singular')])
            );
        }

        $storeTiming = $this->storeTimingRepository->update($input, $id);

        return $this->sendResponse(
            $storeTiming->toArray(),
            __('lang.messages.updated', ['model' => __('models/storeTimings.singular')])
        );
    }

    /**
     * Remove the specified StoreTiming from storage.
     * DELETE /storeTimings/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var StoreTiming $storeTiming */
        $storeTiming = $this->storeTimingRepository->find($id);

        if (empty($storeTiming)) {
            return $this->sendError(
                __('lang.messages.not_found', ['model' => __('models/storeTimings.singular')])
            );
        }

        $storeTiming->delete();

        return $this->sendResponse(
            $id,
            __('lang.messages.deleted', ['model' => __('models/storeTimings.singular')])
        );
    }
}
