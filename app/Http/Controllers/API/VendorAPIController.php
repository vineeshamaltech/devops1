<?php

namespace App\Http\Controllers\API;

use Auth;
use App\Http\Requests\API\UpdateVendorAPIRequest;
use App\Models\Admin;
use App\Models\Vendor;
use App\Repositories\VendorRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Illuminate\Support\Facades\Hash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class VendorController
 * @package App\Http\Controllers\API
 * @group Vendor
 */

class VendorAPIController extends AppBaseController
{
    /** @var  VendorRepository */
    private $vendorRepository;

    public function __construct(VendorRepository $vendorRepo)
    {
        $this->vendorRepository = $vendorRepo;
    }

    /**
     * Update Profile.
     *
     * @authenticated
     * @param UpdateVendorAPIRequest $request
     *
     * @return Response
     */
    public function update(UpdateVendorAPIRequest $request)
    {
        $input = $request->all();
        $vendor = auth()->user();
        if (empty($vendor)) {
            return $this->sendError(
                __('lang.messages.not_found', ['model' => __('models/vendors.singular')])
            );
        }
        $vendor = $this->vendorRepository->update($input, $vendor->id);
        return $this->sendResponse(
            $vendor->toArray(),
            __('lang.messages.updated', ['model' => __('models/vendors.singular')])
        );
    }

    /**
     * Get Profile.
     *
     * @authenticated
     * @return Response
     */
    public function profile()
    {
        $vendor = auth()->user();

        if (empty($vendor)) {
            return $this->sendError(
                __('lang.messages.not_found', ['model' => __('models/vendors.singular')])
            );
        }
        $vendor = Admin::with('kyc')->find($vendor->id);
        return $this->sendResponse(
            $vendor->toArray(),
            __('lang.messages.retrieved', ['model' => __('models/vendors.singular')])
        );
    }

    /**
     * Vendor Login
     *
     * @bodyParam mobile string required Email of the vendor.
     * @bodyParam password string required Password of the vendor.
     */
    public function login(Request $request)
    {
        $request->validate([
            'email' => 'required|string|email',
            'password' => 'required|string'
        ]);
        $credentials = request(['email', 'password']);
        if (!Auth::guard()->attempt($credentials)) {
            return $this->sendError( 'Incorrect email or password');
        }
        $vendor = auth()->user();
        if($vendor == null){
            return $this->sendError('Vendor not found');
        }
        //$vendor->tokens()->delete();
        $vendor->token = $vendor->createToken('vendor_authentication_token')->plainTextToken;
        $vendor->token_type = 'Bearer';
        return $this->sendResponse($vendor, 'Login Successful');
    }

    /**
     * Vendor Registration
     *
     * @bodyParam  name string nullable Name of the vendor.
     * @bodyParam  mobile string required Phone No of the vendor in the format +919999999999. Always include the country code.
     * @bodyParam  email string nullable Email of the vendor.
     * @bodyParam  password string required Password of the vendor.
     * @bodyParam  fcm_token string nullable FCM Token.
     *
     * @return Response
     */
    public function register(Request $request)
    {
        $request->validate([
            'name' => 'required|string',
            'mobile' => 'required|string|min:13|max:13',
            'email' => 'required|string',
            'password' => 'required|string',
            'fcm_token' => 'required|string',
            'zones' => 'nullable'
        ]);
        //get country code from phone no
        $country_code = substr($request->mobile, 0, 3);
        $mobile = substr($request->mobile, 3);
        if(Admin::where('mobile', $mobile)->first()){
            return $this->sendError('Mobile number already exists');
        }
        if(Admin::where('email', $request->email)->first()){
            return $this->sendError('Email already exists');
        }
        $vendor = Admin::create([
            'name' => $request->name,
            'email' => $request->email,
            'country_code' => $country_code,
            'mobile' => $mobile,
            'password' => Hash::make($request->password),
            'fcm_token' => $request->fcm_token,
            'type' => 'Vendor',
            'active' => 0,
            'verified' => 0,
            'zones' => $request->zones ? $request->zones : null,
        ]);
        
        $vendor = Admin::find($vendor->id);
        $vendor->assignRole('Vendor');
        $vendor->token = $vendor->createToken('user_authentication_token')->plainTextToken;
        $vendor->token_type = 'Bearer';
        return $this->sendResponse($vendor->toArray(), 'Vendor created successfully');
    }

}
