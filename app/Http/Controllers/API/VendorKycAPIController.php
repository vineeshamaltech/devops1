<?php

namespace App\Http\Controllers\API;

use App\Criteria\ActiveCriteria;
use App\Http\Requests\API\CreateVendorKycAPIRequest;
use App\Http\Requests\API\UpdateVendorKycAPIRequest;
use App\Models\VendorKyc;
use App\Repositories\VendorKycRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class VendorKycController
 * @package App\Http\Controllers\API
 * @group VendorKYC
 */

class VendorKycAPIController extends AppBaseController
{
    /** @var  VendorKycRepository */
    private $vendorKycRepository;

    public function __construct(VendorKycRepository $vendorKycRepo)
    {
        $this->vendorKycRepository = $vendorKycRepo;
    }

    /**
     * Add KYC
     * POST /vendorKycs
     *
     * @authenticated
     * @param CreateVendorKycAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateVendorKycAPIRequest $request)
    {
        $input = $request->all();
        if ($request->hasFile('photo')) {
            $input['photo'] = \Helpers::uploadImage($request->file('photo'), 'vendor_kyc');
        }
        $user = auth()->user();
        if (empty($user)){
            return $this->sendError('Unauthorized', 401);
        }
        $input['admin_id'] = $user->id;

        $kyc = VendorKyc::where('doc_type', $input['doc_type'])->where('admin_id', $input['admin_id'])->first();
        if($kyc == null || $kyc->status == 'REJECTED'){
            if($kyc != null){
                \Helpers::deleteImage($kyc->photo);
                $kyc->delete();
            }
            $input['status'] = 'UPLOADED';
            $vendorKyc = $this->vendorKycRepository->create($input);
            return $this->sendResponse(
                $vendorKyc->toArray(),
                __('lang.messages.saved', ['model' => __('models/vendorKycs.singular')])
            );
        }else if($kyc->status == "UPLOADED" || $kyc->status == "APPROVED") {
            return $this->sendError(
                'KYC Already Exists',
                409
            );
        }
    }

    /**
     * Delete KYC.
     * DELETE /vendorKycs/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var VendorKyc $vendorKyc */
        $vendorKyc = $this->vendorKycRepository->find($id);

        if (empty($vendorKyc)) {
            return $this->sendError(
                __('lang.messages.not_found', ['model' => __('models/vendorKycs.singular')])
            );
        }
        $vendorKyc->delete();
        return $this->sendResponse(
            $id,
            __('lang.messages.deleted', ['model' => __('models/vendorKycs.singular')])
        );
    }
}
