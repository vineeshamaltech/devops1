<?php

namespace App\Http\Controllers\API\CarPool;

use App\Http\Controllers\AppBaseController;
use App\Http\Requests\API\CreateRideBookingAPIRequest;
use App\Http\Requests\API\UpdateRideBookingAPIRequest;
use App\Models\Ride;
use App\Models\RideBooking;
use App\Notifications\PushNotification;
use App\Repositories\RideBookingRepository;
use Carbon\Carbon;
use GeometryLibrary\PolyUtil;
use Illuminate\Http\Request;
use Prettus\Repository\Criteria\RequestCriteria;
use Notification;
use Response;

/**
 * Class RideBookingController
 * @package App\Http\Controllers\API
 * @group Ride Booking
 */

class RideBookingAPIController extends AppBaseController
{
    /** @var  RideBookingRepository */
    private $rideBookingRepository;

    public function __construct(RideBookingRepository $rideBookingRepo)
    {
        $this->rideBookingRepository = $rideBookingRepo;
    }

    /**
     * Display a listing of the RideBooking.
     * GET|HEAD /rideBookings
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->rideBookingRepository->pushCriteria(new RequestCriteria($request));
        $rideBookings = $this->rideBookingRepository->with(['ride','user','ride.user'])->paginate(10);

        return $this->sendResponse(
            $rideBookings->toArray(),
            __('lang.messages.retrieved', ['model' => __('models/rideBookings.plural')])
        );
    }

    /**
     * Create booking
     * POST /rideBookings
     *
     * @param CreateRideBookingAPIRequest $request
     *
     * @return Response
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function store(CreateRideBookingAPIRequest $request)
    {
        $input = $request->all();
        $input['user_id'] = auth()->user()->id;
        $ride = Ride::find($input['ride_id']);
        if(empty($ride)){
            return $this->sendError(__('lang.messages.not_found', ['model' => __('models/rides.singular')]));
        }
        if (RideBooking::where('ride_id', $input['ride_id'])->get()->sum('seats') < $ride->max_seats) {
            $input['price'] = $ride->price;
            if ($ride->can_book_instantly){
                $input['status'] = 'CONFIRMED';
                Ride::where('id', $input['ride_id'])->update(['passenger_count' => $ride->passenger_count + $input['seats'],'travel_datetime' => Carbon::parse($ride->travel_datetime)]);
            }else{
                $input['status'] = 'BOOKED';
            }
            if (!empty($input['polyline'])) {
                $input['polyline'] = PolyUtil::decode($input['polyline']);
            }
            $rideBooking = $this->rideBookingRepository->create($input);
            Notification::send($ride->user, new PushNotification(['title' => 'New Booking Received', 'body' => 'Your ride has a new booking. More details in app']));
            return $this->sendResponse(
                $rideBooking->toArray(),
                __('lang.messages.created', ['model' => __('models/rideBookings.singular')])
            );
        } else {
            return $this->sendError("Maximum number of passengers reached. You can't book this ride.",422);
        }
    }

    /**
     * Display the specified RideBooking.
     * GET|HEAD /rideBookings/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var RideBooking $rideBooking */
        $rideBooking = $this->rideBookingRepository->with(['ride','user','ride.user'])->find($id);

        if (empty($rideBooking)) {
            return $this->sendError(
                __('lang.messages.not_found', ['model' => __('models/rideBookings.singular')])
            );
        }

        return $this->sendResponse(
            $rideBooking->toArray(),
            __('lang.messages.retrieved', ['model' => __('models/rideBookings.singular')])
        );
    }

    /**
     * Update the specified RideBooking in storage.
     * PUT/PATCH /rideBookings/{id}
     *
     * @param int $id
     * @param UpdateRideBookingAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateRideBookingAPIRequest $request)
    {
        $input = $request->all();

        /** @var RideBooking $rideBooking */
        $rideBooking = $this->rideBookingRepository->find($id);

        if (empty($rideBooking)) {
            return $this->sendError(
                __('lang.messages.not_found', ['model' => __('models/rideBookings.singular')])
            );
        }

        if (!empty($input['status']) && $input['status'] != $rideBooking->status) {
            if ($input['status'] == 'CONFIRMED') {
                $ride = Ride::find($rideBooking->ride_id);
                $ride->passenger_count = $ride->passenger_count + $rideBooking->seats;
                $ride->save();
                $rideBooking->status = $input['status'];
                $rideBooking->save();
                Notification::send($rideBooking->user, new PushNotification(['title' => 'Booking Confirmed', 'body' => 'Your booking has been confirmed. More details in app']));
            } elseif ($input['status'] == 'CANCELLED') {
                $ride = Ride::find($rideBooking->ride_id);
                $ride->passenger_count = $ride->passenger_count - $rideBooking->seats;
                $ride->save();
                $rideBooking->status = $input['status'];
                $rideBooking->save();
                Notification::send($rideBooking->user, new PushNotification(['title' => 'Booking Cancelled', 'body' => 'Your booking has been cancelled. More details in app']));
            }
            $ride = Ride::find($rideBooking->ride_id);
            $ride->passenger_count = $ride->passenger_count + $rideBooking->seats;
            $ride->save();
        }

        $rideBooking = $this->rideBookingRepository->update($input, $id);

        return $this->sendResponse(
            $rideBooking->toArray(),
            __('lang.messages.updated', ['model' => __('models/rideBookings.singular')])
        );
    }

    /**
     * Remove the specified RideBooking from storage.
     * DELETE /rideBookings/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var RideBooking $rideBooking */
        $rideBooking = $this->rideBookingRepository->find($id);

        if (empty($rideBooking)) {
            return $this->sendError(
                __('lang.messages.not_found', ['model' => __('models/rideBookings.singular')])
            );
        }

        $rideBooking->delete();

        return $this->sendResponse(
            $id,
            __('lang.messages.deleted', ['model' => __('models/rideBookings.singular')])
        );
    }
}
