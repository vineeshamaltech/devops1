<?php

namespace App\Http\Controllers\API\CarPool;

use App\Criteria\CarPool\ActiveRidesCriteria;
use App\Criteria\LimitOffsetCriteria;
use App\Http\Controllers\AppBaseController;
use App\Http\Requests\API\CreateRidesAPIRequest;
use App\Http\Requests\API\UpdateRidesAPIRequest;
use App\Models\Ride;
use App\Repositories\RidesRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class RidesController
 * @package App\Http\Controllers\API
 * @group Rides
 */

class RidesAPIController extends AppBaseController
{
    /** @var  RidesRepository */
    private $ridesRepository;

    public function __construct(RidesRepository $ridesRepo)
    {
        $this->ridesRepository = $ridesRepo;
    }

    /**
     * Display a listing of the Ride.
     * GET|HEAD /rides
     *
     * @authenticated
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ridesRepository->pushCriteria(new RequestCriteria($request));
        $this->ridesRepository->pushCriteria(new LimitOffsetCriteria($request));
        $rides = $this->ridesRepository->with(['stopovers','user','bookings.user'])->paginate($request->per_page ?? 10,$this->ridesRepository->getFieldsSearchable());

        return $this->sendResponse(
            $rides->toArray(),
            __('lang.messages.retrieved', ['model' => __('models/rides.plural')])
        );
    }

    public function search(Request $request){
        $request->validate([
            'from_lat' => 'required',
            'from_long' => 'required',
            'to_lat' => 'required',
            'to_long' => 'required',
            'tolerance' => 'nullable|numeric',
            'date' => 'required|date',
            'passengers' => 'nullable|numeric',
        ]);
        if (empty($request->tolerance)){
            $tolerance = 2000;
        }else{
            $tolerance = $request->tolerance;
        }
        $this->ridesRepository->pushCriteria(new RequestCriteria($request));
        $this->ridesRepository->pushCriteria(new ActiveRidesCriteria($request));
        $rides = $this->ridesRepository->with(['stopovers','user','bookings','bookings.user'])->all();
        $eligibleRides = [];
        foreach ($rides as $ride) {
            $response =  \GeometryLibrary\PolyUtil::isLocationOnPath(['lat' => $request->from_lat, 'lng' => $request->from_long],
                $ride->polyline,tolerance: $tolerance);

            if ($response) {
                $end_response =  \GeometryLibrary\PolyUtil::isLocationOnPath(['lat' => $request->to_lat, 'lng' => $request->to_long],
                    $ride->polyline,tolerance: $tolerance);
                $ride->polyline = null;
                if ($end_response) {
                    $eligibleRides[] = $ride;
                }
            }
        }

        return $this->sendResponse(
            $eligibleRides,
            __('lang.messages.retrieved', ['model' => __('models/rides.plural')])
        );
    }

    /**
     * Store a newly created Ride in storage.
     * POST /rides
     *
     * @param CreateRidesAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateRidesAPIRequest $request)
    {
        $input = $request->all();
        $input['user_id'] = auth()->user()->id;
        if (!empty($input['polyline'])) {
            $input['polyline'] = \GeometryLibrary\PolyUtil::decode($input['polyline']);
        }
        $rides = $this->ridesRepository->create($input);
        $ride = $this->ridesRepository->with(['stopovers','user','bookings'])->find($rides->id,$this->ridesRepository->getFieldsSearchable());
        return $this->sendResponse(
            $ride->toArray(),
            __('lang.messages.saved', ['model' => __('models/rides.singular')])
        );
    }

    /**
     * Calculate Price.
     * POST /rides/calculate
     *
     * @authenticated
     * @bodyParam from_lat float required From Latitude.
     * @bodyParam from_long float required From Longitude.
     * @bodyParam to_lat float required To Latitude.
     * @bodyParam to_long float required To Longitude.
     *
     * @return Response
     */
    public function calculatePrice(Request $request){
        $request->validate([
            'from_lat' => 'required',
            'from_long' => 'required',
            'to_lat' => 'required',
            'to_long' => 'required',
        ]);
        $from_lat = $request->from_lat;
        $from_long = $request->from_long;
        $to_lat = $request->to_lat;
        $to_long = $request->to_long;
        $array = $this->getDistanceBetweenTwoPoints($from_lat, $from_long, $to_lat, $to_long);
        if ($array['distance'] == -1){
            return $this->sendError($array['error_message']);
        }
        $distance = $array['distance'];
        $price = $distance * setting('carpool_per_km_price',-1);
        return $this->sendResponse(['distance'=>$array['distance'],'time'=>$array['time'],'price'=>$price],'Price calculated successfully');
    }

    //get distance from google map api
    public function getDistanceBetweenTwoPoints($from_lat,$from_lng,$to_lat,$to_lng) : array{
        $url = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=".$from_lat.",".$from_lng."&destinations=".$to_lat.",".$to_lng."&mode=driving&language=en-US&key=".setting('google_map_api_key');
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $response = curl_exec($ch);
        curl_close($ch);
        try {
            $response_a = json_decode($response, true);
            $dist = $response_a['rows'][0]['elements'][0]['distance']['value'];
            $time = $response_a['rows'][0]['elements'][0]['duration']['value'];
            return array('distance'=>$dist,'time'=>$time,'error_code'=>'NO_ERROR','error_message'=>'');
        }catch (\Exception $e) {
            return array('distance' => -1, 'time' => -1, 'error_code' => 'PARSE_ERROR', 'error_message' => $e->getMessage());
        }
    }

    /**
     * Display the specified Ride.
     * GET|HEAD /rides/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Ride $rides */
        $rides = $this->ridesRepository->with(['stopovers','user','bookings','bookings.user'])->find($id,$this->ridesRepository->getFieldsSearchable());

        if (empty($rides)) {
            return $this->sendError(
                __('lang.messages.not_found', ['model' => __('models/rides.singular')])
            );
        }

        return $this->sendResponse(
            $rides->toArray(),
            __('lang.messages.retrieved', ['model' => __('models/rides.singular')])
        );
    }

    /**
     * Update the specified Ride in storage.
     * PUT/PATCH /rides/{id}
     *
     * @param int $id
     * @param UpdateRidesAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateRidesAPIRequest $request)
    {
        $input = $request->all();

        /** @var Ride $rides */
        $rides = $this->ridesRepository->find($id);

        if (empty($rides)) {
            return $this->sendError(
                __('lang.messages.not_found', ['model' => __('models/rides.singular')])
            );
        }

        $rides = $this->ridesRepository->update($input, $id);

        return $this->sendResponse(
            $rides->toArray(),
            __('lang.messages.updated', ['model' => __('models/rides.singular')])
        );
    }

    /**
     * Remove the specified Ride from storage.
     * DELETE /rides/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Ride $rides */
        $rides = $this->ridesRepository->find($id);

        if (empty($rides)) {
            return $this->sendError(
                __('lang.messages.not_found', ['model' => __('models/rides.singular')])
            );
        }
        $rides->delete();
        return $this->sendResponse(
            $id,
            __('lang.messages.deleted', ['model' => __('models/rides.singular')])
        );
    }
}
