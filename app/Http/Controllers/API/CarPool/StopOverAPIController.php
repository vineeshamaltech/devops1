<?php

namespace App\Http\Controllers\API\CarPool;

use App\Http\Controllers\AppBaseController;
use App\Http\Requests\API\CreateStopOverAPIRequest;
use App\Http\Requests\API\UpdateStopOverAPIRequest;
use App\Models\StopOver;
use App\Repositories\StopOverRepository;
use Illuminate\Http\Request;
use Response;
use function __;

/**
 * Class StopOverController
 * @package App\Http\Controllers\API
 */

class StopOverAPIController extends AppBaseController
{
    /** @var  StopOverRepository */
    private $stopOverRepository;

    public function __construct(StopOverRepository $stopOverRepo)
    {
        $this->stopOverRepository = $stopOverRepo;
    }

    /**
     * Display a listing of the StopOver.
     * GET|HEAD /stopOvers
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $stopOvers = $this->stopOverRepository->all();

        return $this->sendResponse(
            $stopOvers->toArray(),
            __('lang.messages.retrieved', ['model' => __('models/stopOvers.plural')])
        );
    }

    /**
     * Store a newly created StopOver in storage.
     * POST /stopOvers
     *
     * @param CreateStopOverAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateStopOverAPIRequest $request)
    {
        $input = $request->all();

        $stopOver = $this->stopOverRepository->create($input);

        return $this->sendResponse(
            $stopOver->toArray(),
            __('lang.messages.saved', ['model' => __('models/stopOvers.singular')])
        );
    }

    /**
     * Display the specified StopOver.
     * GET|HEAD /stopOvers/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var StopOver $stopOver */
        $stopOver = $this->stopOverRepository->find($id);

        if (empty($stopOver)) {
            return $this->sendError(
                __('lang.messages.not_found', ['model' => __('models/stopOvers.singular')])
            );
        }

        return $this->sendResponse(
            $stopOver->toArray(),
            __('lang.messages.retrieved', ['model' => __('models/stopOvers.singular')])
        );
    }

    /**
     * Update the specified StopOver in storage.
     * PUT/PATCH /stopOvers/{id}
     *
     * @param int $id
     * @param UpdateStopOverAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateStopOverAPIRequest $request)
    {
        $input = $request->all();

        /** @var StopOver $stopOver */
        $stopOver = $this->stopOverRepository->find($id);

        if (empty($stopOver)) {
            return $this->sendError(
                __('lang.messages.not_found', ['model' => __('models/stopOvers.singular')])
            );
        }

        $stopOver = $this->stopOverRepository->update($input, $id);

        return $this->sendResponse(
            $stopOver->toArray(),
            __('lang.messages.updated', ['model' => __('models/stopOvers.singular')])
        );
    }

    /**
     * Remove the specified StopOver from storage.
     * DELETE /stopOvers/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var StopOver $stopOver */
        $stopOver = $this->stopOverRepository->find($id);

        if (empty($stopOver)) {
            return $this->sendError(
                __('lang.messages.not_found', ['model' => __('models/stopOvers.singular')])
            );
        }

        $stopOver->delete();

        return $this->sendResponse(
            $id,
            __('lang.messages.deleted', ['model' => __('models/stopOvers.singular')])
        );
    }
}
