<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateStoreReviewsAPIRequest;
use App\Http\Requests\API\UpdateStoreReviewsAPIRequest;
use App\Models\StoreReviews;
use App\Repositories\StoreReviewsRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class StoreReviewsController
 * @package App\Http\Controllers\API
 */

class StoreReviewsAPIController extends AppBaseController
{
    /** @var  StoreReviewsRepository */
    private $storeReviewsRepository;

    public function __construct(StoreReviewsRepository $storeReviewsRepo)
    {
        $this->storeReviewsRepository = $storeReviewsRepo;
    }

    /**
     * Display a listing of the StoreReviews.
     * GET|HEAD /storeReviews
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->storeReviewsRepository->pushCriteria(new RequestCriteria($request));
        $storeReviews = $this->storeReviewsRepository->all();
        return $this->sendResponse(
            $storeReviews->toArray(),
            __('lang.messages.retrieved', ['model' => __('models/storeReviews.plural')])
        );
    }

    /**
     * Store a newly created StoreReviews in storage.
     * POST /storeReviews
     *
     * @param CreateStoreReviewsAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateStoreReviewsAPIRequest $request)
    {
        $input = $request->all();

        $storeReviews = $this->storeReviewsRepository->create($input);

        return $this->sendResponse(
            $storeReviews->toArray(),
            __('lang.messages.saved', ['model' => __('models/storeReviews.singular')])
        );
    }

    /**
     * Display the specified StoreReviews.
     * GET|HEAD /storeReviews/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var StoreReviews $storeReviews */
        $storeReviews = $this->storeReviewsRepository->find($id);

        if (empty($storeReviews)) {
            return $this->sendError(
                __('lang.messages.not_found', ['model' => __('models/storeReviews.singular')])
            );
        }

        return $this->sendResponse(
            $storeReviews->toArray(),
            __('lang.messages.retrieved', ['model' => __('models/storeReviews.singular')])
        );
    }

    /**
     * Update the specified StoreReviews in storage.
     * PUT/PATCH /storeReviews/{id}
     *
     * @param int $id
     * @param UpdateStoreReviewsAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateStoreReviewsAPIRequest $request)
    {
        $input = $request->all();

        /** @var StoreReviews $storeReviews */
        $storeReviews = $this->storeReviewsRepository->find($id);

        if (empty($storeReviews)) {
            return $this->sendError(
                __('lang.messages.not_found', ['model' => __('models/storeReviews.singular')])
            );
        }

        $storeReviews = $this->storeReviewsRepository->update($input, $id);

        return $this->sendResponse(
            $storeReviews->toArray(),
            __('lang.messages.updated', ['model' => __('models/storeReviews.singular')])
        );
    }

    /**
     * Remove the specified StoreReviews from storage.
     * DELETE /storeReviews/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var StoreReviews $storeReviews */
        $storeReviews = $this->storeReviewsRepository->find($id);

        if (empty($storeReviews)) {
            return $this->sendError(
                __('lang.messages.not_found', ['model' => __('models/storeReviews.singular')])
            );
        }

        $storeReviews->delete();

        return $this->sendResponse(
            $id,
            __('lang.messages.deleted', ['model' => __('models/storeReviews.singular')])
        );
    }
}
