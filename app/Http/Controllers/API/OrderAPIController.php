<?php

namespace App\Http\Controllers\API;

use App\Criteria\DescendingCriteria;
use App\Events\OrderChanged;
use App\Events\Sockets\FindCabDrivers;
use App\Events\Sockets\OrderTracking;
use App\Helpers\OrderHelper;
use App\Helpers\WalletHelper;
use App\Http\Controllers\AppBaseController;
use App\Http\Requests\API\CreateOrderAPIRequest;
use App\Http\Requests\API\UpdateOrderAPIRequest;
use App\Models\Cart;
use App\Models\CartItem;
use App\Models\Coupon;
use App\Models\CouponUsage;
use App\Models\Driver;
use App\Models\DriverRejectedOrder;
use App\Models\Order;
use App\Models\OrderProducts;
use App\Models\OrderStatusTimestamp;
use App\Models\Payment;
use App\Models\Product;
use App\Models\ServiceCategory;
use App\Models\Store;
use App\Models\User;
use App\Models\Admin;
use App\Models\VehicleType;
use App\Notifications\NewOrder;
use App\Notifications\NewOrderVendor;
use App\Notifications\PushNotification;
use App\Repositories\OrderRepository;
use Illuminate\Http\Request;
use Notification;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use DB;

/**
 * Class OrderController
 * @package App\Http\Controllers\API
 * @group Order
 */

class OrderAPIController extends AppBaseController
{
    /** @var  OrderRepository */
    private $orderRepository;

    public function __construct(OrderRepository $orderRepo)
    {
        $this->orderRepository = $orderRepo;
    }

    /**
     * Display a listing of the Order
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
       
        if (auth()->user()->hasRole('Vendor')) {
            $stores = \Helpers::get_user_stores();
            // $zones = \Helpers::get_user_zones();
            
            if(!@$request->order)
            $this->orderRepository->pushCriteria(new RequestCriteria($request));
            else
            { 
                 //dd($request->order);
                if(@$request->search_status)
                {
                    $or = Order::where('order_status',$request->search_status); 
                }
               else if($request->order == 'ongoing')
                {
                  $or =  Order::whereNotIn('order_status', ['PLACED','DELIVERED','CANCELLED']);
                }
             
               else if($request->order == 'pending')
               {
                    $or = Order::where('order_status','PLACED');
               }
             
               else if($request->order == 'cancelled')
               {
                
                $or = Order::where('order_status','CANCELLED'); 
                  
               }
             
               else if($request->order == 'delivered')
               {
                  $or =  Order::where('order_status','DELIVERED');
               }
             
            }
             
            // $this->orderRepository->pushCriteria(new DescendingCriteria());
            $orders = $or->with(['store', 'user', 'payment', 'driver', 'orderType', 'service_category', 'status_timestamps', 'items', 'review','vehicle'])
            ->whereIn('store_id', $stores)
            // ->whereHas('store', function ($query) use ($zones) {
            //     return $query->whereIn('zone', $zones);
            // })
            ->orderBy('id', 'desc')->where('store_id',$request->store_id)->paginate($request->per_page ?? 10);
        }else{
              
            $this->orderRepository->pushCriteria(new RequestCriteria($request));
            // $this->orderRepository->pushCriteria(new DescendingCriteria());
            $orders = $this->orderRepository->where('user_id',auth()->user()->id)->with(['store', 'user', 'payment', 'driver', 'orderType', 'service_category', 'status_timestamps', 'items', 'review','vehicle'])
            ->orderBy('id', 'desc')->paginate($request->per_page ?? 10);
        }
        
        return $this->sendResponse(
            $orders->toArray(),
            __('lang.messages.retrieved', ['model' => __('models/orders.plural')])
        );
    }

    /**
     * Store a newly created Order in storage.
     * POST /orders
     *
     * @param CreateOrderAPIRequest $request
     *
     * @return Response
     */
    
    public function store(CreateOrderAPIRequest $request)
    {
        $user = auth()->user();
        if ($user == null) {
            return $this->sendError(__('lang.messages.unauthorized'));
        }
        //find user's cart
        $cart = Cart::with('coupon', 'address', 'store')->where('user_id', $user->id)->first();
      //dd($cart);
        if ($cart == null) {
            return $this->sendError(__('lang.messages.cart_not_found'));
        }
        if ($cart->delivery_address == null) {
            return $this->sendError(__('Please Select delivery Address before order '));
        }

        try {
            $service_category_id = $cart->service_category_id;
            $serviceCategory = ServiceCategory::find($service_category_id);
            $serviceType = $serviceCategory->service_type_id;
        } catch (\Exception $e) {
            return $this->sendError('Could not find service type');
        }
        $payment_status = "PENDING";
        $payment_id = $request->input('payment_id');
        if ($request->payment_method == "COD") {
            $payment = Payment::create([
                'payment_request_id' => null,
                'ref_no' => \Helpers::invoice_id(),
                'user_id' => $user->id,
                'user_type' => get_class($user),
                'order_id' => null,
                'remarks' => 'Order placed using COD',
                'payment_method' => 'COD',
                'amount' => $cart->grand_total * 100,
                'gateway_response' => '',
            ]);
            $payment_id = $payment->id;
            $payment_status = "PENDING";
        } else if ($request->payment_method == "WALLET") {
            if ($user->wallet_amount < $cart->grand_total) {
                return $this->sendError(__('lang.messages.insufficient_wallet_balance'));
            } else {
                $ref_no = \Helpers::invoice_id();
                WalletHelper::debitUserWallet($user->id, $cart->grand_total, 'Order placed using wallet. Ref No: ' . $ref_no);
               
                $payment = Payment::create([
                    'payment_request_id' => null,
                    'ref_no' => $ref_no,
                    'user_id' => $user->id,
                    'user_type' => get_class($user),
                    'order_id' => null,
                    'remarks' => 'Order placed using wallet',
                    'payment_method' => 'WALLET',
                    'amount' => $cart->grand_total * 100,
                    'gateway_response' => '',
                ]);
                $payment_id = $payment->id;
                $payment_status = "SUCCESS";
            }
        } else if ($request->payment_method == "ONLINE") {
            if ($request->payment_id == null) {
                return $this->sendError("Payment id is required");
            }
            $payment = Payment::find($request->payment_id);
            if ($payment == null) {
                return $this->sendError("Invalid payment id");
            }
            if ($payment->gateway_response['status'] == 'captured' ?? false) {
                $payment_status = "SUCCESS";
            }
            
            dd($payment);
        } else {
            return $this->sendError("Invalid payment method");
        }
        $input = array(
            'user_id' => $user->id,
            'store_id' => $cart->store_id,
            'coupon_data' => $cart->coupon == null ? null : json_decode($cart->coupon->toJson()),
            'order_type' => $serviceType,
            'service_category_id' => $cart->service_category_id,
            'pickup_from_store' => $cart->pickup_from_store,
            'total_price' => $cart->total_price,
            'discount_value' => $cart->discount_value,
            'delivery_fee' => $cart->delivery_fee,
            'tax' => $cart->tax,
            'tip' => $cart->tip,
            'grand_total' => $cart->grand_total,
            'pickup_address' => $cart->pickup_address,
            'delivery_address' => $cart->delivery_address,
            'distance_travelled' => $cart->distance,
            'travel_time' => $cart->travel_time,
            'preparation_time' => 0,
            'payment_id' => $payment_id,
            'driver_id' => null,
            'order_status' => 'PLACED',
            'vehicle_id' => $cart->vehicle_id == null ? VehicleType::first()->id ?? null : $cart->vehicle_id,
            'payment_status' => $payment_status,
            'payment_method' => $request->payment_method,
            'json_data' => $cart->json_data,
            'pickup_otp' => \Helpers::randomOTP(4),
            'delivery_otp' => \Helpers::randomOTP(4),
        );
        $order = $this->orderRepository->create($input);
        if ($cart->coupon_id != null) {
            CouponUsage::create([
                'coupon_id' => $cart->coupon_id,
                'user_id' => $user->id,
                'order_id' => $order->id,
                'coupon_code' => $cart->coupon->code ?? "N/A",
                'discount_value' => $cart->discount_value,
            ]);
        }
        $cartItems = CartItem::where('cart_id', $cart->id)->get();
        foreach ($cartItems as $cartItem) {
            $product = Product::find($cartItem->product_id);
            $product->sku = $product->sku - $cartItem->quantity;
            $product->save();
//            foreach ($cartItem->variants as $variant){
//                $variant->sku = $variant->sku - 1;
//                $variant->save();
//            }
            OrderProducts::create([
                'order_id' => $order->id,
                'product_name' => $cartItem->product_name,
                'product_description' => $cartItem->product_description,
                'product_price' => $cartItem->product_price,
                'discount_price' => $cartItem->cart_price,
                'quantity' => $cartItem->quantity,
                // 'variant_names' => $cartItem->variants->pluck('price', 'name')->implode(','),
                // removed price and , from above code to get name details of variants
                'variant_names' => $cartItem->variants->pluck( 'name')->implode(''),
            ]);
        }
        OrderStatusTimestamp::create([
            'order_id' => $order->id,
            'status' => 'PLACED',
            'comment' => 'Order placed',
        ]);
        OrderHelper::calculateEarnings($order->id);
        CartItem::where('cart_id', $cart->id)->delete();
        $cart->delete();
        //Send notification to user
        if($order->order_type != 4)
          $this->send_push($user,'Order Placed','Your order has been Placed');
          
        //   $admin = Admin::where('id',1)->first();
        //   $this->send_push($admin,'Order Placed','Your order has been Placed Admin');
        //   $admin2 = Admin::where('id',205)->first();
        //   $this->send_push($admin2,'Order Placed','Your order has been Placed Admin 2');
        
          if($order->order_type == 4 || $order->order_type == 2)
           OrderHelper::assignDriver($order->id);
        // Notification::send($user, new NewOrder($order));
        //Send notification to store
        if ($order->store_id != null) {
            $store = Store::with('vendor')->find($order->store_id);
        
            if ($store != null && $store->vendor != null) {
                // Notification::send($store->vendor, new NewOrderVendor($order));
                  $this->send_push($store->vendor,'# '.$order->id.'New Order Received','Click here see details');
        
            }
        }
          if ($order->order_type == config('constants.cab_service_type_id')){
            //OrderHelper::assignDriver($order->id,[]);
        }
        $order = $this->orderRepository->with(['store', 'user', 'payment', 'driver', 'orderType', 'service_category', 'status_timestamps', 'items'])->find($order->id);
        return $this->sendResponse(
            $order,
            __('lang.messages.saved', ['model' => __('models/orders.singular')])
        );
    }

    public function autoAssign(Request $request)
    {
        $data = OrderHelper::assignDriver($request->order_id);
    }

    /**
     * Update Status
     *
     * @bodyParam  int $id required Order id.
     * @bodyParam  string $status required Order status.
     * @return Response
     */
    public function update_order_status($id, Request $request)
    {
        $request->validate([
            'status' => 'required|string',
            'comment' => 'nullable|string',
            'preparation_time' => 'nullable|numeric',
            'pickup_items' => 'nullable',
            'images' => 'nullable',
        ]);
         $order =  Order::find($id);

        if($order == null){
            return $this->sendError("Order is not available");
        }
        $json_data = $order->json_data;
        $files = $request->file('images');

        $pickup_images = [];
        if ($request->hasFile('images')) {
            foreach ($files as $file) {
                $pickup_images[] = \Helpers::uploadImage($file, 'order_images');
            }
            if ($request->status == "PICKEDUP") {
                $json_data['pickup_images'] = $pickup_images;
            } else {
                $json_data['delivery_images'] = $pickup_images;
            }
        }

        if ($request->has('pickup_items')) {
            $json_data['pickup_items'] = json_decode($request->pickup_items);
        }

        if ($order->order_status == 'DRIVERCANCELLED') {
            $request->status = 'PLACED';
            $order->driver_id = null;
        }

        $order->order_status = $request->status;
        if ($request->status == 'ACCEPTED' && ($order->preparation_time != null || $order->preparation_time != -1)) {
            $order->preparation_time = $request->preparation_time;
        }
        $order->json_data = $json_data;
        $order->save();
        $order = Order::with(['driver', 'user'])->find($order->id);

        OrderStatusTimestamp::create([
            'order_id' => $order->id,
            'status' => $request->status,
            'comment' => $request->comment,
        ]);

        event(new OrderChanged($order));
        event(new OrderTracking($order->id,$order));

        switch ($request->status) {
            case 'ACCEPTED':
                //Assign driver to the order
                if (!$order->pickup_from_store && $order->driver_id == null) {
                    //Auto assign disabled as discussion with client
                    // OrderHelper::assignDriver($order->id);
                       if($order->order_type == 4)
                        $this->send_push($order->user,'Order Placed','Your order has been Placed');
                }elseif($order->driver_id != null){
                     $this->send_push($order->driver,'Order Status Changed','Your order status has been changed to ' . $request->status);
                    //  Notification::send($order->driver, new PushNotification(['title'=>'#'.$order->id.' Order Status Changed','description'=>'#'.$order->id.'Order Status has been chenged to'. $request->status]));
              
                }
                break;
            case 'PREPARED':
                if($order->driver_id != null){
                    // Notification::send($order->driver, new PushNotification(['title'=>'#'.$order->id.' Order Prepared','description'=>'#'.$order->id.' has been prepared. Please pick and deliver it.']));
              $this->send_push($order->driver,'Order Prepared','Your order status has been prepared. Please pick and deliver it ');
                }
                  break;
            case 'ASSIGNED':
                break;
            case 'DRIVERACCEPTED':
                    DB::table('drivers')->where('id',auth()->user()->id)->update(['is_busy'=> 1]);
                   
                Order::where('id', $order->id)->update(['order_status' => 'ASSIGNED','driver_id'=> auth()->user()->id]);
                event(new FindCabDrivers($order->driver_id,$order,"ACCEPTED"));
                break;
            case 'DELIVERED':
                Order::where('id', $order->id)->update(['payment_status' => 'SUCCESS']);
                Driver::where('id', $order->driver_id)->update(['is_busy' => 0]);
                break;
        }
         if(@$order->driver_id && $request->status != "DELIVERED")
        DB::table('drivers')->where('id',$order->driver_id)->update(['is_busy'=> 1]);
        $this->send_push($order->user,'Order Status Changed','Your order status has been changed to ' . $request->status);
         
        if ($order->store_id != null) {
            $store = Store::with('vendor')->find($order->store_id);
        
            if ($store != null && $store->vendor != null) {
                if($request->status == 'DRIVERACCEPTED' || $request->status == 'REACHED')
                {
                      $this->send_push($store->vendor,'Order Status Changed','Your order status has been changed to ' . $request->status);
                }
                // Notification::send($store->vendor, new NewOrderVendor($order));
               
        
            }
        }
        // if(auth()->user()->hasRole('Vendor'))
        // {
           
        // }
        
        // Notification::send($order->user, new PushNotification(['title' => 'Order Status Changed', 'description' => 'Your order status has been changed to ' . $request->status]));
        $order = $this->orderRepository->with(['store', 'user', 'payment', 'driver', 'orderType', 'service_category', 'status_timestamps', 'items'])->find($order->id);
        return $this->sendResponse(
            $order,
            __('lang.messages.updated', ['model' => __('models/orders.singular')])
        );
    }


    /**
     * Update Paymtnt  Status
     *
     * @bodyParam   $id int required Order id.
     * @bodyParam   $payment_id int required payment id.
     * @bodyParam   $status string required Order status.
     * @return Response
     */
    public function update_order_payment(Request $request)
    {
        $request->validate([
            'status' => 'required|string',
            'comment' => 'nullable|string',
        ]);
        $order = Order::find($request->order_id);
        $payment_status = $request->status;
        if($order == null)
        {
         return $this->sendError("Order is not found");
        }
        if ($request->payment_id == null) {
            return $this->sendError("Payment id is required");
        }
        $payment = Payment::find($request->payment_id);
        if ($payment == null) {
            return $this->sendError("Invalid payment id");
        }
        if ($payment->gateway_response['status'] == 'captured' ?? false) {
            $payment_status = "SUCCESS";
        }

         Order::where('id',$request->order_id)->update(['payment_status'=>$request->status]);


          Notification::send($order->user, new PushNotification(['title' => 'Order payment Status Changed', 'description' => 'Your order payment status has been changed to ' . $request->status]));
        $order = $this->orderRepository->with(['store', 'user', 'payment', 'driver', 'orderType', 'service_category', 'status_timestamps', 'items'])->find($order->id);
        return $this->sendResponse(
            $order,
            __('lang.messages.updated', ['model' => __('models/orders.singular')])
        );
    }

    public function change_payment(Request $request) {
        
        $request->validate([
            'order_id' => 'required',
            'payment_method' => 'required|string',
        
        ]);
        $user = auth()->user();
        $order = Order::find($request->order_id);
    
        if ($request->payment_method == "COD") {
            $payment = Payment::create([
                'payment_request_id' => null,
                'ref_no' => \Helpers::invoice_id(),
                'user_id' => $user->id,
                'user_type' => get_class($user),
                'order_id' => null,
                'remarks' => 'Order placed using COD',
                'payment_method' => 'COD',
                'amount' => $order->grand_total * 100,
                'gateway_response' => '',
            ]);

            $payment_id = $payment->id;
            $payment_status = "PENDING";
        } else if ($request->payment_method == "WALLET") {
            if ($user->wallet_amount < $order->grand_total) {
                return $this->sendError(__('lang.messages.insufficient_wallet_balance'));
            } else {
                $ref_no = \Helpers::invoice_id();
                WalletHelper::debitUserWallet($user->id, $order->grand_total, 'Order placed using wallet. Ref No: ' . $ref_no);
                $payment = Payment::create([
                    'payment_request_id' => null,
                    'ref_no' => $ref_no,
                    'user_id' => $user->id,
                    'user_type' => get_class($user),
                    'order_id' => null,
                    'remarks' => 'Order placed using wallet',
                    'payment_method' => 'WALLET',
                    'amount' => $order->grand_total * 100,
                    'gateway_response' => '',
                ]);
                $payment_id = $payment->id;
                $payment_status = "SUCCESS";
            }
        } else if ($request->payment_method == "ONLINE") {
            if ($request->payment_id == null) {
                return $this->sendError("Payment id is required");
            }
            $payment = Payment::where('payment_request_id',$request->payment_id)->first();
          
            if ($payment == null) {
                return $this->sendError("Invalid payment id");
            }
            if ($payment->gateway_response['status'] == 'captured' ?? false) {
                $payment_status = "SUCCESS";
            }
        } else {
            return $this->sendError("Invalid payment method");
        }
        Order::where('id',$order->id)->update(['payment_method'=>$request->payment_method ,'payment_status'=>$payment_status,'payment_id'=>$payment->id ]);
       $orderupdate = Order::find($request->order_id);
        return $this->sendResponse(
            $orderupdate,
            __('lang.messages.updated', ['model' => __('models/orders.singular')])
        );
    }
    
    public function send_push($user,$title,$desc) {
        
        
         $title = $title;
         $body = $desc;
        //  $imageurl = '';
        // $tokens = array();
        // foreach($alltoken as $token){
        //     array_push($tokens,$token['fcm_token']);
        // }
   
          $fcmkey =  DB::table('settings')->where('key_name','fcm_key')->first();
            
        // $result = array();
        // $chunks = array_chunk($tokens,999);
       
        // foreach($chunks as $chunk){
               $chunk = $user->fcm_token;
          
            $url = 'https://fcm.googleapis.com/fcm/send';    
            $dataArr = array('click_action' => 'FLUTTER_NOTIFICATION_CLICK');
    
            $notification = array('title' =>$title, 'body' => $body,  'sound' =>'disabled' , 'default', 'badge' => '1');
    
            $arrayToSend = array('to' =>$chunk , 'notification' => $notification, 'data' => $dataArr, 'priority'=>'high' );
    
            $tempArr = $arrayToSend;
    
            $fields = json_encode($arrayToSend);
           
            
            $headers = array (
                'Authorization: key=' . $fcmkey->key_value,
                'Content-Type: application/json'
    
            );
    
            //Register notification in database
            $ch = curl_init ();
    
            curl_setopt ( $ch, CURLOPT_URL, $url );
    
            curl_setopt ( $ch, CURLOPT_POST, true );
    
            curl_setopt ( $ch, CURLOPT_HTTPHEADER, $headers );
    
            curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );
    
            curl_setopt ( $ch, CURLOPT_POSTFIELDS, $fields );
            $result[] = curl_exec ( $ch );
   
            //   dd($result);
            curl_close ( $ch );
           return 1;
        // }
        
    }


    public function driver_cancel_order(Request $request)
    {
        $request->validate([
            'order_id' => 'required|numeric',
            'comment' => 'nullable|string',
        ]);
        $driver = auth()->user();
        $order = Order::find($request->order_id);
        if($order == null)
       {
        return $this->sendError("Order is not available");
       }
        $order->order_status = 'ACCEPTED';
        $order->driver_id = null;
        $order->save();
        //Free user from the order
        Driver::where('id', $driver->id)->update(['is_busy' => 0]);
        //add log for current driver
        DriverRejectedOrder::create([
            'driver_id' => $driver->id,
            'order_id' => $order->id,
            'reason' => $request->comment,
        ]);
        event(new FindCabDrivers($driver->id,$order,"REJECTED"));
        //find another driver except this one
        $exceptions = DriverRejectedOrder::where('order_id', $order->id)->get()->pluck('driver_id');
        if ($order->order_type != config('constants.cab_service_type_id')){
            OrderHelper::assignDriver($order->id, $exceptions);
        }
        return $this->sendResponse(null, 'Order cancelled');
    }

    /**
     * Display the specified Order.
     * GET|HEAD /orders/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Order $order */
        $order = $this->orderRepository->with(['store', 'user', 'payment', 'driver', 'orderType', 'service_category', 'status_timestamps', 'items', 'review','vehicle'])->find($id);
        if (empty($order)) {
            return $this->sendError(
                __('lang.messages.not_found', ['model' => __('models/orders.singular')])
            );
        }

        return $this->sendResponse(
            $order->toArray(),
            __('lang.messages.retrieved', ['model' => __('models/orders.singular')])
        );
    }

    /**
     * Update the specified Order in storage.
     * PUT/PATCH /orders/{id}
     *
     * @param int $id
     * @param UpdateOrderAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateOrderAPIRequest $request)
    {
        $input = $request->all();

        /** @var Order $order */
        $order = $this->orderRepository->find($id);

        if (empty($order)) {
            return $this->sendError(
                __('lang.messages.not_found', ['model' => __('models/orders.singular')])
            );
        }

        $order = $this->orderRepository->update($input, $id);
        $order = $this->orderRepository->with(['store', 'user', 'payment', 'driver', 'orderType', 'service_category', 'status_timestamps', 'items'])->find($order->id);
        event(new OrderTracking($order->id,$order));
        return $this->sendResponse(
            $order->toArray(),
            __('lang.messages.updated', ['model' => __('models/orders.singular')])
        );
    }


    public function destroy($id)
    {
        /** @var Payment $payment */
        if($id != null)
        {

             $order=  Order::where('id',$id)->delete();
             return $this->sendResponse(null, 'Orders deleted');
     }
     return $this->sendResponse(null, 'Order id not given');
    }





}
