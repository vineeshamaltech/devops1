<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateUserWalletTransactionAPIRequest;
use App\Http\Requests\API\UpdateUserWalletTransactionAPIRequest;
use App\Models\UserWalletTransaction;
use App\Repositories\UserWalletTransactionRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class UserWalletTransactionController
 * @package App\Http\Controllers\API
 * @group User Wallet Transaction
 */

class UserWalletTransactionAPIController extends AppBaseController
{
    /** @var  UserWalletTransactionRepository */
    private $userWalletTransactionRepository;

    public function __construct(UserWalletTransactionRepository $userWalletTransactionRepo)
    {
        $this->userWalletTransactionRepository = $userWalletTransactionRepo;
    }

    /**
     * User's Wallet Transactions
     * GET|HEAD /userWalletTransactions
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->userWalletTransactionRepository->pushCriteria(new RequestCriteria($request));
        $userWalletTransactions = $this->userWalletTransactionRepository->all();

        return $this->sendResponse(
            $userWalletTransactions->toArray(),
            __('lang.messages.retrieved', ['model' => __('models/userWalletTransactions.plural')])
        );
    }

}
