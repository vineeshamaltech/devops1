<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\AppBaseController;
use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Models\Store;
use Illuminate\Http\Request;

/**
 * Class SearchAPIController
 * @package App\Http\Controllers\API
 * @group Search
 */
class SearchAPIController extends AppBaseController
{
    public function index(Request $request){
        $request->validate([
            'q' => 'required|string|min:3',
        ]);
        if ($request->has('q')) {
            $q = $request->q;
            $products = Product::where('name', 'LIKE', "%$q%")->orWhere('description', 'LIKE', "%$q%")->paginate(10);
            $stores = Store::where('name', 'LIKE', "%$q%")->orWhere('description', 'LIKE', "%$q%")->paginate(10);
        }
     
        $returnData['products'] = $products;
        $returnData['stores'] = $stores;
        return $this->sendSuccess($returnData,'Search Success');
    }
}
