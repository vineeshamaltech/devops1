<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\AppBaseController;
use App\Http\Controllers\Controller;
use App\Models\Earning;
use App\Models\Order;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

/**
 * Class ReportsAPIController
 * @package App\Http\Controllers\API
 * @group  Reports
 */

class ReportsAPIController extends AppBaseController
{
    /** Get report
     * @authenticated
     * @bodyParam start_date string required Start date of report
     * @bodyParam end_date string required End date of report
     * @bodyParam type string required Type of report (earnings, orders)
     * @bodyParam interval string required Interval of report (day, week, month, year)
     */
    public function index(Request $request)
    {
        $this->validate($request, [
            'start_date' => 'required',
            'end_date' => 'required',
            'interval' => 'required|in:day,week,month,year',
            'type' => 'required|in:earnings,orders'
        ]);
        $user = auth()->user();
        if ($user != null){
            $startDate = Carbon::parse($request->start_date);
            $endDate = Carbon::parse($request->end_date)->addDays();
            if ($request->type == "earnings"){
                $report = $this->vendorEarningsReport($user->id, $startDate, $endDate, $request->interval);
            }else{
                $report = $this->vendorOrderReport($user->id, $startDate, $endDate, $request->interval);
            }
            return $this->sendResponse($report, 'Report retrieved successfully.');
        }else{
            return $this->sendError("You don't have permission to access this resource");
        }
    }

    public function vendorOrderReport($id,$startDate,$endDate,$interval = "day"){
        $data_title = "Order Report for ".$startDate->format('d-m-Y')." to ".$endDate->format('d-m-Y');
        $stores = \Helpers::get_stores_by_user($id);

        $orders = Order::whereIn('store_id',$stores->pluck('id'))->whereBetween('created_at',[$startDate,$endDate])->get();
        $total_orders = count($orders);
        if ($interval == "day") {
            $c_data = Order::selectRaw('count(*) as total, DATE(created_at) as date')->whereIn('store_id',$stores->pluck('id'))->whereBetween('created_at',[$startDate,$endDate])->groupBy('date')->get()->pluck('total','date')->toArray();
        }else if ($interval == "week") {
            $c_data = Order::selectRaw('count(*) as total, DATE_FORMAT(DATE(created_at),"%u %Y") as date')->whereIn('store_id',$stores->pluck('id'))->whereBetween('created_at',[$startDate,$endDate])->groupBy(DB::raw('DATE_FORMAT(DATE(created_at),"%u %Y")'))->get()->pluck('total','date')->toArray();
        }else if ($interval == "month") {
            $c_data = Order::selectRaw('count(*) as total, DATE_FORMAT(DATE(created_at),"%M %Y") as date')->whereIn('store_id',$stores->pluck('id'))->whereBetween('created_at',[$startDate,$endDate])->groupBy(DB::raw('DATE_FORMAT(DATE(created_at),"%M %Y")'))->get()->pluck('total','date')->toArray();
        }else if ($interval == "year") {
            $c_data = Order::selectRaw('count(*) as total, DATE_FORMAT(DATE(created_at),"%Y") as date')->whereIn('store_id',$stores->pluck('id'))->whereBetween('created_at',[$startDate,$endDate])->groupBy(DB::raw('DATE_FORMAT(DATE(created_at),"%Y")'))->get()->pluck('total','date')->toArray();
        }
        $total_amount = Order::whereIn('store_id',$stores->pluck('id'))->whereBetween('created_at',[$startDate,$endDate])->sum('total_price');
        $earnings = Earning::where('entity_type','App\Models\Store')->whereIn('order_id',$orders->pluck('id'))->whereBetween('created_at',[$startDate,$endDate])->sum('amount');
        //loop through the start and end dates and create an array of dates
        $chart_data = [];
        while ($startDate <= $endDate) {
            if ($interval == "day") {
                $chart_data[$startDate->format('Y-m-d')] = $c_data[$startDate->format('Y-m-d')] ?? 0;
                $startDate->addDay();
            }else if ($interval == "week") {
                $chart_data[$startDate->format('W Y')] = $c_data[$startDate->format('W Y')] ?? 0;
                $startDate->addWeek();
            }else if ($interval == "month") {
                $chart_data[$startDate->format('M Y')] = $c_data[$startDate->format('F Y')] ?? 0;
                $startDate->addMonth();
            }else if ($interval == "year") {
                $chart_data[$startDate->format('Y')] = $c_data[$startDate->format('Y')] ?? 0;
                $startDate->addYear();
            }
        }
        $today_orders = Order::whereIn('store_id',$stores->pluck('id'))->whereDate('created_at',Carbon::now())->count();
        $today_earnings = Earning::where('entity_type','App\Models\Store')->whereIn('entity_id',$stores->pluck('id'))->whereDate('created_at',Carbon::now())->sum('amount');
        $today_earnings = format_number($today_earnings);
        $total_amount = format_number($total_amount);
        $earnings = format_number($earnings);
        return compact('today_orders','today_earnings','total_orders','total_amount','earnings','chart_data','data_title');
    }

    public function vendorEarningsReport($id,$startDate,$endDate,$interval = "day"){
        $data_title = "Earning Report for ".$startDate->format('d-m-Y')." to ".$endDate->format('d-m-Y');
        $stores = \Helpers::get_stores_by_user($id);
        $orders = Order::whereIn('store_id',$stores->pluck('id'))->whereBetween('created_at',[$startDate,$endDate])->get();
        $total_orders = count($orders);
        $total_amount = Order::whereIn('store_id',$stores->pluck('id'))->whereBetween('created_at',[$startDate,$endDate])->sum('total_price');
        $earnings = Earning::where('entity_type','App\Models\Store')->whereIn('entity_id',$stores->pluck('id'))->whereBetween('created_at',[$startDate,$endDate])->sum('amount');

        if ($interval == "day") {
            $earning_loop = Earning::selectRaw('sum(amount) as amount, DATE(created_at) as date')->where('entity_type','App\Models\Store')->whereIn('order_id',$orders->pluck('id'))->whereBetween('created_at',[$startDate,$endDate])->groupBy('date')->get()->pluck('amount','date')->toArray();
        }else if ($interval == "week") {
            $earning_loop = Earning::selectRaw('sum(amount) as amount, DATE_FORMAT(DATE(created_at),"%u %Y") as date')->where('entity_type','App\Models\Store')->whereIn('order_id',$orders->pluck('id'))->whereBetween('created_at',[$startDate,$endDate])->groupBy(DB::raw('DATE_FORMAT(DATE(created_at),"%u %Y")'))->get()->pluck('amount','date')->toArray();
        }else if ($interval == "month") {
            $earning_loop = Earning::selectRaw('sum(amount) as amount, DATE_FORMAT(DATE(created_at),"%M %Y") as date')->where('entity_type','App\Models\Store')->whereIn('order_id',$orders->pluck('id'))->whereBetween('created_at',[$startDate,$endDate])->groupBy(DB::raw('DATE_FORMAT(DATE(created_at),"%M %Y")'))->get()->pluck('amount','date')->toArray();
        }else if ($interval == "year") {
            $c_data = Order::selectRaw('count(*) as total, DATE_FORMAT(DATE(created_at),"%Y") as date')->whereIn('store_id',$stores->pluck('id'))->whereBetween('created_at',[$startDate,$endDate])->groupBy(DB::raw('DATE_FORMAT(DATE(created_at),"%Y")'))->get()->pluck('total','date')->toArray();
            $earning_loop = Earning::selectRaw('sum(amount) as amount, DATE_FORMAT(DATE(created_at),"%Y") as date')->where('entity_type','App\Models\Store')->whereIn('order_id',$orders->pluck('id'))->whereBetween('created_at',[$startDate,$endDate])->groupBy(DB::raw('DATE_FORMAT(DATE(created_at),"%Y")'))->get()->pluck('amount','date')->toArray();
        }

        //loop through the start and end dates and create an array of dates
        $chart_data = [];
        while ($startDate <= $endDate) {
            if ($interval == "day") {
                $chart_data[$startDate->format('Y-m-d')] = $earning_loop[$startDate->format('Y-m-d')] ?? 0;
                $startDate->addDay();
            }else if ($interval == "week") {
                $chart_data[$startDate->format('W Y')] = $earning_loop[$startDate->format('W Y')] ?? 0;
                $startDate->addWeek();
            }else if ($interval == "month") {
                $chart_data[$startDate->format('M Y')] = $earning_loop[$startDate->format('F Y')] ?? 0;
                $startDate->addMonth();
            }else if ($interval == "year") {
                $chart_data[$startDate->format('Y')] = $earning_loop[$startDate->format('Y')] ?? 0;
                $startDate->addYear();
            }
        }
        $today_orders = Order::whereIn('store_id',$stores->pluck('id'))->whereDate('created_at',Carbon::now())->count();
        $today_earnings = Earning::where('entity_type','App\Models\Store')->whereIn('entity_id',$stores->pluck('id'))->whereDate('created_at',Carbon::now())->sum('amount');
        $today_earnings = format_number($today_earnings);
        $total_amount = format_number($total_amount);
        $earnings = format_number($earnings);
        return compact('today_orders','today_earnings','total_orders','total_amount','earnings','chart_data','data_title');
    }
}
