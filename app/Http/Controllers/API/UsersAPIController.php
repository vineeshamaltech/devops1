<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateDriverAPIRequest;
use App\Http\Requests\API\UpdateDriverAPIRequest;
use App\Models\Driver;
use App\Repositories\UserRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Illuminate\Support\Facades\Hash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class DriverController
 * @package App\Http\Controllers\API
 */

class UsersAPIController extends AppBaseController
{
    /** @var  DriverRepository */
    private $userRepository;

    public function __construct(UserRepository $driverRepo)
    {
        $this->userRepository = $driverRepo;
    }

    /**
     * Display a listing of the Driver.
     * GET|HEAD /drivers
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->userRepository->pushCriteria(new RequestCriteria($request));
        $users = $this->userRepository->all();

        return $this->sendResponse(
            $users->toArray(),
            __('lang.messages.retrieved', ['model' => __('models/user.plural')])
        );
    }

    /**
     * Store a newly created Driver in storage.
     * POST /drivers
     *
     * @param CreateUserAPIRequest $request
     *
     * @return Response
     */
  
}
