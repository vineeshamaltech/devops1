<?php

namespace App\Http\Controllers\API;

use App\Criteria\ActiveCriteria;
use App\Http\Requests\API\CreateCouponAPIRequest;
use App\Http\Requests\API\UpdateCouponAPIRequest;
use App\Models\Cart;
use App\Models\Coupon;
use App\Models\CouponUsage;
use App\Repositories\CouponRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class CouponController
 * @package App\Http\Controllers\API
 * @group Coupons
 */

class CouponAPIController extends AppBaseController
{
    /** @var  CouponRepository */
    private $couponRepository;

    public function __construct(CouponRepository $couponRepo)
    {
        $this->couponRepository = $couponRepo;
    }

    public function index(Request $request)
    {
        if(@$request->get_available == true && $request->store_id)
        {
           
            $eligibleCoupons = [];
            $current = Carbon::now();
            $coupons = $this->couponRepository->findWhere(['store_id' => $request->get('store_id')]);
     
            foreach ($coupons as $coupon){
                $startDate = Carbon::parse($coupon->start_date);
                $endDate = Carbon::parse($coupon->end_date);
                if($current->lt($startDate) || $current->gt($endDate)){
                    continue;
                }
                if ($coupon->store_id!=null &&
                    $coupon->store_id !=
                    $request->store_id){
                    continue;
                }
                $usage_count = CouponUsage::where('coupon_id',$coupon->id)->where('user_id',auth()->user()->id)->count();
                if($coupon->max_use_per_user > 0 && $usage_count >= $coupon->max_use_per_user){
                    continue;
                }
                $eligibleCoupons[] = $coupon;
            }

            return $this->sendResponse(
                $eligibleCoupons,
                __('lang.messages.retrieved', ['model' => __('models/coupons.plural')])
            );

        }

        $this->couponRepository->pushCriteria(new RequestCriteria($request));
        $this->couponRepository->pushCriteria(new ActiveCriteria());
        if ($request->has('store_id')){
            $coupons = $this->couponRepository->findWhere(['store_id' => $request->get('store_id')]);
        }else{
            $coupons = $this->couponRepository->all();
        }
        $eligibleCoupons = [];
        if (auth()->user() != null){
            $current = Carbon::now();
            $cart = Cart::where('user_id', auth()->user()->id)->first();
            if (empty($cart)){
                $eligibleCoupons = collect([]);
                return $this->sendResponse(
                    $eligibleCoupons,
                    __('lang.messages.retrieved', ['model' => __('models/coupons.plural')])
                );
            }
            foreach ($coupons as $coupon){
                $startDate = Carbon::parse($coupon->start_date);
                $endDate = Carbon::parse($coupon->end_date);
                if($current->lt($startDate) || $current->gt($endDate)){
                    continue;
                }
                if ($coupon->store_id!=null &&
                    $coupon->store_id !=
                    $cart->store_id){
                    continue;
                }
                if($coupon->min_cart_value > $cart->grand_total){
                    continue;
                }
                $usage_count = CouponUsage::where('coupon_id',$coupon->id)->where('user_id',auth()->user()->id)->count();
                if($coupon->max_use_per_user > 0 && $usage_count >= $coupon->max_use_per_user){
                    continue;
                }
                $eligibleCoupons[] = $coupon;
            }
        }else{
            $eligibleCoupons = collect([]);
        }
        return $this->sendResponse(
            $eligibleCoupons,
            __('lang.messages.retrieved', ['model' => __('models/coupons.plural')])
        );
    }


    /**
     * Store a newly created Coupon in storage.
     * POST /coupons
     *
     * @param CreateCouponAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateCouponAPIRequest $request)
    {
        $input = $request->all();
        if($request->hasFile('image')){
            $input['image'] = \Helpers::uploadImage($request->file('image'),'coupons');
        }
        $coupon = $this->couponRepository->create($input);

        return $this->sendResponse(
            $coupon->toArray(),
            __('lang.messages.saved', ['model' => __('models/coupons.singular')])
        );
    }

    /**
     * Display the specified Coupon.
     * GET|HEAD /coupons/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Coupon $coupon */
        $coupon = $this->couponRepository->find($id);

        if (empty($coupon)) {
            return $this->sendError(
                __('lang.messages.not_found', ['model' => __('models/coupons.singular')])
            );
        }

        return $this->sendResponse(
            $coupon->toArray(),
            __('lang.messages.retrieved', ['model' => __('models/coupons.singular')])
        );
    }

    /**
     * Update the specified Coupon in storage.
     * PUT/PATCH /coupons/{id}
     *
     * @param int $id
     * @param UpdateCouponAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateCouponAPIRequest $request)
    {
        $input = $request->all();

        /** @var Coupon $coupon */
        $coupon = $this->couponRepository->find($id);

        if (empty($coupon)) {
            return $this->sendError(
                __('lang.messages.not_found', ['model' => __('models/coupons.singular')])
            );
        }
        if($request->hasFile('image')){
            $input['image'] = \Helpers::uploadImage($request->file('image'),'coupons');
        }
        $coupon = $this->couponRepository->update($input, $id);

        return $this->sendResponse(
            $coupon->toArray(),
            __('lang.messages.updated', ['model' => __('models/coupons.singular')])
        );
    }

    /**
     * Remove the specified Coupon from storage.
     * DELETE /coupons/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Coupon $coupon */
        $coupon = $this->couponRepository->find($id);

        if (empty($coupon)) {
            return $this->sendError(
                __('lang.messages.not_found', ['model' => __('models/coupons.singular')])
            );
        }

        $coupon->delete();

        return $this->sendResponse(
            $id,
            __('lang.messages.deleted', ['model' => __('models/coupons.singular')])
        );
    }
}
