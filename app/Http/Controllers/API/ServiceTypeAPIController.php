<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateServiceTypeAPIRequest;
use App\Http\Requests\API\UpdateServiceTypeAPIRequest;
use App\Models\ServiceType;
use App\Repositories\ServiceTypeRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class ServiceTypeController
 * @package App\Http\Controllers\API
 * @group ServiceType
 */

class ServiceTypeAPIController extends AppBaseController
{
    /** @var  ServiceTypeRepository */
    private $serviceTypeRepository;

    public function __construct(ServiceTypeRepository $serviceTypeRepo)
    {
        $this->serviceTypeRepository = $serviceTypeRepo;
    }

    /**
     * Display a listing of the ServiceType.
     * GET|HEAD /serviceTypes
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $serviceTypes = $this->serviceTypeRepository->all();

        return $this->sendResponse(
            $serviceTypes->toArray(),
            __('lang.messages.retrieved', ['model' => __('models/serviceTypes.plural')])
        );
    }

    /**
     * Store a newly created ServiceType in storage.
     * POST /serviceTypes
     *
     * @param CreateServiceTypeAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateServiceTypeAPIRequest $request)
    {
        $input = $request->all();

        $serviceType = $this->serviceTypeRepository->create($input);

        return $this->sendResponse(
            $serviceType->toArray(),
            __('lang.messages.saved', ['model' => __('models/serviceTypes.singular')])
        );
    }

    /**
     * Display the specified ServiceType.
     * GET|HEAD /serviceTypes/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var ServiceType $serviceType */
        $serviceType = $this->serviceTypeRepository->find($id);

        if (empty($serviceType)) {
            return $this->sendError(
                __('lang.messages.not_found', ['model' => __('models/serviceTypes.singular')])
            );
        }

        return $this->sendResponse(
            $serviceType->toArray(),
            __('lang.messages.retrieved', ['model' => __('models/serviceTypes.singular')])
        );
    }

    /**
     * Update the specified ServiceType in storage.
     * PUT/PATCH /serviceTypes/{id}
     *
     * @param int $id
     * @param UpdateServiceTypeAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateServiceTypeAPIRequest $request)
    {
        $input = $request->all();

        /** @var ServiceType $serviceType */
        $serviceType = $this->serviceTypeRepository->find($id);

        if (empty($serviceType)) {
            return $this->sendError(
                __('lang.messages.not_found', ['model' => __('models/serviceTypes.singular')])
            );
        }

        $serviceType = $this->serviceTypeRepository->update($input, $id);

        return $this->sendResponse(
            $serviceType->toArray(),
            __('lang.messages.updated', ['model' => __('models/serviceTypes.singular')])
        );
    }

    /**
     * Remove the specified ServiceType from storage.
     * DELETE /serviceTypes/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var ServiceType $serviceType */
        $serviceType = $this->serviceTypeRepository->find($id);

        if (empty($serviceType)) {
            return $this->sendError(
                __('lang.messages.not_found', ['model' => __('models/serviceTypes.singular')])
            );
        }

        $serviceType->delete();

        return $this->sendResponse(
            $id,
            __('lang.messages.deleted', ['model' => __('models/serviceTypes.singular')])
        );
    }
}
