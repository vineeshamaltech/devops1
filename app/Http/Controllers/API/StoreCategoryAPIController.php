<?php

namespace App\Http\Controllers\API;

use App\Criteria\ActiveCriteria;
use App\Http\Requests\API\CreateStoreCategoryAPIRequest;
use App\Http\Requests\API\UpdateStoreCategoryAPIRequest;
use App\Models\StoreCategory;
use App\Repositories\StoreCategoryRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class StoreCategoryController
 * @package App\Http\Controllers\API
 * @group StoreCategory
 */

class StoreCategoryAPIController extends AppBaseController
{
    /** @var  StoreCategoryRepository */
    private $storeCategoryRepository;

    public function __construct(StoreCategoryRepository $storeCategoryRepo)
    {
        $this->storeCategoryRepository = $storeCategoryRepo;
    }

    /**
     * Display a listing of the StoreCategory.
     * GET|HEAD /storeCategories
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->storeCategoryRepository->pushCriteria(new RequestCriteria($request));
        $this->storeCategoryRepository->pushCriteria(new ActiveCriteria());
        $storeCategories = $this->storeCategoryRepository->paginate(10);
        
      
          if (@$request->type == 'vendor') {
          $storeCategories = $this->storeCategoryRepository->get();
          }
        return $this->sendResponse(
            $storeCategories->toArray(),
            __('lang.messages.retrieved', ['model' => __('models/storeCategories.plural')])
        );
    }

    /**
     * Store a newly created StoreCategory in storage.
     * POST /storeCategories
     *
     * @param CreateStoreCategoryAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateStoreCategoryAPIRequest $request)
    {
        $input = $request->all();

        $storeCategory = $this->storeCategoryRepository->create($input);

        return $this->sendResponse(
            $storeCategory->toArray(),
            __('lang.messages.saved', ['model' => __('models/storeCategories.singular')])
        );
    }

    /**
     * Display the specified StoreCategory.
     * GET|HEAD /storeCategories/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var StoreCategory $storeCategory */
        $storeCategory = $this->storeCategoryRepository->find($id);

        if (empty($storeCategory)) {
            return $this->sendError(
                __('lang.messages.not_found', ['model' => __('models/storeCategories.singular')])
            );
        }

        return $this->sendResponse(
            $storeCategory->toArray(),
            __('lang.messages.retrieved', ['model' => __('models/storeCategories.singular')])
        );
    }

    /**
     * Update the specified StoreCategory in storage.
     * PUT/PATCH /storeCategories/{id}
     *
     * @param int $id
     * @param UpdateStoreCategoryAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateStoreCategoryAPIRequest $request)
    {
        $input = $request->all();

        /** @var StoreCategory $storeCategory */
        $storeCategory = $this->storeCategoryRepository->find($id);

        if (empty($storeCategory)) {
            return $this->sendError(
                __('lang.messages.not_found', ['model' => __('models/storeCategories.singular')])
            );
        }

        $storeCategory = $this->storeCategoryRepository->update($input, $id);

        return $this->sendResponse(
            $storeCategory->toArray(),
            __('lang.messages.updated', ['model' => __('models/storeCategories.singular')])
        );
    }

    /**
     * Remove the specified StoreCategory from storage.
     * DELETE /storeCategories/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var StoreCategory $storeCategory */
        $storeCategory = $this->storeCategoryRepository->find($id);

        if (empty($storeCategory)) {
            return $this->sendError(
                __('lang.messages.not_found', ['model' => __('models/storeCategories.singular')])
            );
        }

        $storeCategory->delete();

        return $this->sendResponse(
            $id,
            __('lang.messages.deleted', ['model' => __('models/storeCategories.singular')])
        );
    }
}
