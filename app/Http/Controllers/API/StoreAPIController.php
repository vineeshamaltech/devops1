<?php

namespace App\Http\Controllers\API;

use App\Criteria\Store\ActiveCriteria;
use App\Criteria\Store\EmptyStoreCriteria;
use App\Criteria\Store\InStoreCategoriesCriteria;
use App\Criteria\Store\NearCriteria;
use App\Criteria\Store\OpenCriteria;
use App\Criteria\Store\StoreOfVendorCriteria;
use App\Criteria\Store\VendorActiveCriteria;
use App\Http\Requests\API\CreateStoreAPIRequest;
use App\Http\Requests\API\UpdateStoreAPIRequest;
use App\Models\Admin;
use App\Models\Category;
use App\Models\Earning;
use App\Models\Order;
use App\Models\Store;
use App\Models\StoreTiming;
use App\Models\StoreCategoriesAssoc;
use App\Models\StoreCategory;
use App\Repositories\StoreRepository;
use App\Models\Coupon;
use App\Models\CouponUsage;
use App\Repositories\CouponRepository;
use Carbon\Carbon;

use Faker\Extension\Helper;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Models\Zone;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use App\Http\Controllers\API\ZoneAPIController;

/**
 * Class StoreController
 * @package App\Http\Controllers\API
 * @group Store
 */

class StoreAPIController extends AppBaseController
{
    /** @var  StoreRepository */
    private $storeRepository;

    public function __construct(StoreRepository $storeRepo ,CouponRepository $couponRepo)
    {
        $this->couponRepository = $couponRepo;
        $this->storeRepository = $storeRepo;
    }

    /**
     * Display a listing of the Store.
     * GET|HEAD /stores
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
          $user = auth()->user();
         // dd($request->all());
            $day = date('l');
        Store::where('is_open',true)->update(['is_open'=>false]);
        //die();
        $stores = Store::all();
       // dd($stores);
        foreach($stores as $store){
            $store_timing = StoreTiming::where('store_id',$store->id)->where('day', 'like', $day)->first();
            if($store_timing){
                if($store_timing->open < date("H:i:s") && $store_timing->close > date("H:i:s")){
                    Store::where('id',$store->id)->update(['is_open'=>true]);
                }
            }
        }
        
      
            $type = $request->get('type'); 
        $this->storeRepository->pushCriteria(new RequestCriteria($request));
        $this->storeRepository->pushCriteria(new NearCriteria($request));
        $this->storeRepository->pushCriteria(new VendorActiveCriteria());
        // $this->storeRepository->pushCriteria(new InStoreCategoriesCriteria($request));
        if($request->has('store_categories')){
          
         $this->storeRepository->whereRaw("FIND_IN_SET(".$request->store_categories.", store_categories)");
       
        }
        // $type = $request->get('type');
        // $this->storeRepository->pushCriteria(new RequestCriteria($request));
        // $this->storeRepository->pushCriteria(new NearCriteria($request));
        // $this->storeRepository->pushCriteria(new VendorActiveCriteria());
        // // $this->storeRepository->pushCriteria(new InStoreCategoriesCriteria($request));
        // if($request->has('store_categories')){
          
        //  $this->storeRepository->whereRaw("FIND_IN_SET(".$request->store_categories.", store_categories)");
       
        // }
         
        if($type == 'vendor'){

            // $stores = $this->storeRepository->with(['store_timings','store_categories']);
            // $assignedStores = \Helpers::get_user_stores();
            $zones = \Helpers::get_user_zones();
           
            $zone = [];
            if(@$zones[0])
          $zone =  DB::table('zones')->where('id',$zones[0])->first();
        //   dd(auth()->user()->id);
            $stores = $this->storeRepository->with(['store_timings','store_categories'])->where('admin_id',auth()->user()->id);
            // ->whereIn('zone', $zones)
            // ->whereIn('zone', $zones)

            // ->paginate(10)->toArray();
        
        }else if(@$request->lat) {
              $lat = $request->lat;
              $long = $request->long;
            if($lat == null || $lat == "0" || $long == null || $long == "0"){
                $stores = [];
            }else{

                $userZone = \Helpers::findInZone($lat, $long);
                // dd($userZone);
                 if(!@$userZone) 
                 {  
                     $stores = [];
                      return $this->sendResponse(
                $stores,
                'you are out of serviceable zone'
            );
                 }
                $zones = [];
                foreach($userZone as $zone){
                    array_push($zones,$zone->id);
                }  
                                
               
                if(@$userZone){
                    if(@$request->service_category_id) {
                        // $stores = $this->storeRepository->whereRaw("FIND_IN_SET
                        // (".$zone->id.", zone)")
                        $stores = $this->storeRepository->whereIn('zone',$zones)
                        ->where('service_category_id',$request->service_category_id)
                        ->with(['store_timings','store_categories']);
                        // dd($stores->get()->toArray());
                    } 
                    else {
                      
                        // $stores = $this->storeRepository->whereIn('zone',$zones)
                        $stores = $this->storeRepository->whereRaw("FIND_IN_SET(".$zone->id.", zone)")
                        ->with(['store_timings','store_categories']);
                      
                    }
                  
                }else{
                    $stores = [];
                }
            }
        }
        else if($type =='user'){
            
            $lat = $user->lat;
            $long = $user->long;
            
            if($lat == null || $lat == "0" || $long == null || $long == "0"){
                $stores = [];
            }else{
                $userZone = \Helpers::findInZone($lat, $long);
                $zones = [];
                foreach($userZone as $zone){
                    array_push($zones,$zone->id);
                }   
                if(!empty($userZone)){
                    $stores = Store::whereIn('zone',$zones)
                     ->where('active',1)
                    ->with(['store_timings','store_categories'])
                    ->paginate(10)->toArray();
                }else{
                    $stores = [];
                }
            }
        }else{
            $this->storeRepository->pushCriteria(new ActiveCriteria());
                $this->storeRepository->pushCriteria(new EmptyStoreCriteria());
                $stores = $this->storeRepository->with(['store_timings','store_categories'])->where('active',1)->paginate(10)->toArray();
               // dd($stores);
        }
    //->where('is_open',1)
      $stores = $stores->where('active',1)->paginate(10)->toArray();
            $data = [];
              foreach ($stores['data'] as $key1 => $value) {
             $zone =[];
             $d=  explode(',',$value['zone']);
            
                     foreach ($d as $key => $z) {
                         $zone[$key]=Zone::where('id',$z)->first();

                     }

                    //  $l = $value;
                     if($zone != null)
                     $stores['data'][$key1]['zone'] = $zone;
                     else
                    $stores['data'][$key1]['zone'] = [];
                    //  $data[$key] = $l;

                  
                    //  $l = $value;
                    //  $l['zone'] = $zone;
                   
                    if(@$request->lat)
                    {
                    //   $distance = $this->getDistance($request->lat,$request->long,$value['latitude'],$value['longitude']);
                        $url = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=".$request->lat.",".$request->long."&destinations=".$value['latitude'].",".$value['longitude']."&mode=driving&language=en-US&key=".setting('google_map_api_key');
     
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $response = curl_exec($ch);
        curl_close($ch);
    
            $response_a = json_decode($response, true);
              if(@$response_a['rows'][0]['elements'][0]['distance'])
              {
                  $dist = $response_a['rows'][0]['elements'][0]['distance']['value'];
            $time = $response_a['rows'][0]['elements'][0]['duration']['value'];
            $distKm = $dist/1000;
              
                      $stores['data'][$key1]['distance'] = $distKm;
                  
              }
           
                    }
           
              }
            
             
                        // $stores['data']= $data ;

        //   $stores = $data ;
     
        if($stores){
            return $this->sendResponse(
                $stores,
                __('lang.messages.retrieved', ['model' => __('models/stores.plural')])
            );
        }else{
            return $this->sendResponse(
                $stores,
                __('lang.messages.not_found', ['model' => __('models/stores.plural')])
            );
        }
        
    }


    function getDistance($latitude1, $longitude1, $latitude2, $longitude2) {  
        $earth_radius = 6371;
      
        $dLat = deg2rad($latitude2 - $latitude1);  
        $dLon = deg2rad($longitude2 - $longitude1);  
      
        $a = sin($dLat/2) * sin($dLat/2) + cos(deg2rad($latitude1)) * cos(deg2rad($latitude2)) * sin($dLon/2) * sin($dLon/2);  
        $c = 2 * asin(sqrt($a));  
        $d = $earth_radius * $c;  
      
        return $d;  
      }

    /**
     * Store a newly created Store in storage.
     * POST /stores
     *
     * @param CreateStoreAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateStoreAPIRequest $request)
    {
        $request->validate([
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'store_categories' => 'array',
        ]);
        $input = $request->all();
        $storeCategories = $input['store_categories'];
         if(@$request->zone[0])
         $input['zone'] = implode(',',$request->zone);
        // $input['zone'] = implode()
        $input['store_categories'] = implode(',',$storeCategories);
        if ($request->hasFile('image')) {
            $input['image'] = \Helpers::uploadImage($request->file('image'), 'stores');
        }else{
            $this->sendError('Image is required');
        }
        $store = $this->storeRepository->create($input);
        foreach ($storeCategories as $category) {
            StoreCategoriesAssoc::create([
                'store_id' => $store->id,
                'store_category_id' => $category
            ]);
        }
       
        $store = $this->storeRepository->with(['store_timings','store_categories'])->find($store->id);
        
        $zone =[];
         
        $d=  explode(',',$store['zone']);
                foreach ($d as $key => $z) {
                    $zone[$key]=Zone::where('id',$z)->first();
                }
              
                $store['zone'] = $zone;
             
        return $this->sendResponse(
            $store->toArray(),
            __('lang.messages.saved', ['model' => __('models/stores.singular')])
        );
    }

    /**
     * Get Store.
     * GET|HEAD /stores/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id,Request $request)
    {
        $this->storeRepository->pushCriteria(new NearCriteria($request));
        $tag = $request->tag;
        $price = $request->price;
        $search = $request->search;
        $product_type = $request->product_type;
       $time = Carbon::now()->format('H:i:s');

        $store = $this->storeRepository->withAvg('reviews as rating','rating')->find($id);

        $coupons = $this->couponRepository->findWhere(['store_id' => $id]);
        $Coupons=[] ;
        $current = Carbon::now();
   
        foreach ($coupons as $coupon){
            $startDate = Carbon::parse($coupon->start_date);
            $endDate = Carbon::parse($coupon->end_date);
            if($current->lt($startDate) || $current->gt($endDate)){
                continue;
            }
            if ($coupon->store_id!=null &&
                $coupon->store_id !=
                $id){
                continue;
            }
            $usage_count = CouponUsage::where('coupon_id',$coupon->id)->where('user_id',auth()->user()->id)->count();
            if($coupon->max_use_per_user > 0 && $usage_count >= $coupon->max_use_per_user){
                continue;
            }
            $Coupons[] = $coupon;
        }
// dd($Coupons);
    //   $store['coupons'] = [];
    //        $todaytime = DB::table('store_timings')->where('store_id',$id)->where('day',Carbon::today()->format('l'))->first();
    
    //     dd($todaytime);
        //Find store categories
        $categories = Category::where('store_id',$id)->where('parent',0)->with(['children','products'=>function($query) use ($tag,$price,$search,$product_type,$time){
            // $query->where('active',1);
            if ($search){
                $query->where('name','like','%'.$search.'%');
            }
            if ($tag){
                $query->whereRaw('find_in_set(?,product_tags)',$tag);
            }
            if ($price){
                $query->whereRaw('price <= '.$price);
            }
            if ($product_type){
                $query->where('product_type',$product_type);
            }
            // $query->where('closing_time','>=',$time);
        },'children.products'=>function($query) use ($tag,$price,$search,$product_type){
            
            // $query->where('active',1);
            if ($search){
                $query->where('name','like','%'.$search.'%');
            }
            if ($tag){
                $query->whereRaw('find_in_set(?,product_tags)',$tag);
            }
            if ($price){
                $query->whereRaw('price <= '.$price);
            }
            if ($product_type){
                $query->where('product_type',$product_type);
            }
          
        }])->get();
        $store->categories = $categories;
    $store->coupons = $Coupons;
    
     if(@$request->lat)
                    {
                       
                    //    $distance = $this->getDistance($request->lat,$request->long,$value['latitude'],$value['longitude']);
                      $url = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=".$request->lat.",".$request->long."&destinations=".$store->latitude.",".$store->longitude."&mode=driving&language=en-US&key=".setting('google_map_api_key');
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $response = curl_exec($ch);
        curl_close($ch);
    
            $response_a = json_decode($response, true);
            $dist = $response_a['rows'][0]['elements'][0]['distance']['value'];
            $time = $response_a['rows'][0]['elements'][0]['duration']['value'];
            $distKm = $dist/1000;
       $store->distance = $distKm;
                    }
       
        if (empty($store)) {
            return $this->sendError(
                __('lang.messages.not_found', ['model' => __('models/stores.singular')])
            );
        }

        return $this->sendResponse(
            $store->toArray(),
            __('lang.messages.retrieved', ['model' => __('models/stores.singular')])
        );
    }

    /**
     * Update the specified Store in storage.
     * PUT/PATCH /stores/{id}
     *
     * @param int $id
     * @param UpdateStoreAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateStoreAPIRequest $request)
    {
        $input = $request->all();
        $store = $this->storeRepository->with('store_timings')->find($id);
        
        if (empty($store)) {
            return $this->sendError(
                __('lang.messages.not_found', ['model' => __('models/stores.singular')])
            );
        }
        if (isset($input['store_categories'])){
            $storeCategories = $input['store_categories'];
            $input['store_categories'] = implode(',', $storeCategories);
        }
        if ($request->hasFile('image')) {
            \Helpers::deleteImage($store->image);
            $input['image'] = \Helpers::uploadImage($request->file('image'), 'stores');
        }
        
        $store = $this->storeRepository->update($input, $id);
        if (!empty($storeCategories)){
            StoreCategoriesAssoc::where('store_id', $id)->delete();
            foreach ($storeCategories as $category) {
                StoreCategoriesAssoc::create([
                    'store_id' => $id,
                    'store_category_id' => $category
                ]);
            }
        }
        $store = $this->storeRepository->with(['store_timings','store_categories'])->find($store->id);
        return $this->sendResponse(
            $store->toArray(),
            __('lang.messages.updated', ['model' => __('models/stores.singular')])
        );
    }


    /**
     * Get store statistics
     *
     * @authenticated
     * @bodyParam store_id int required
     *
     */
    public function stats($id){
        
        $user = auth()->user();
   
        if (!empty($user)){
            // $this->storeRepository->pushCriteria(StoreOfVendorCriteria::class);
            $store = $this->storeRepository->find($id);
              
            $data['store_id'] = $store->id;
            $data['store_name'] = $store->name;
            $data['store_image'] = $store->image;
            $data['reviews'] = $store->reviews()->count();
            $data['rating'] = $store->reviews()->avg('rating') ?? 0;
            $data['coupons'] = $store->coupons()->count();
            $data['products'] = $store->products()->count();
            $data['today_orders'] = $store->orders()->whereDate('created_at', Carbon::today())->count();
            $data['total_orders'] = $store->orders()->count();
            $data['total_earnings'] = format_number(Earning::where('entity_type','App\Models\Store')->where('entity_id',$id)->sum('amount'));
            $data['today_earnings'] = format_number(Earning::where('entity_type','App\Models\Store')->where('entity_id',$id)->whereDate('created_at', Carbon::today())->sum('amount'));
            $data['pending_orders'] = Order::where('store_id',$id)->where('order_status','PLACED')->count();
            $data['ongoing_orders'] = Order::where('store_id',$id)->whereNotIn('order_status',['PLACED','CANCELLED','DELIVERED'])->count();
            $data['cancelled_orders'] = Order::where('store_id',$id)->where('order_status','CANCELLED')->count();
            $data['completed_orders'] = Order::where('store_id',$id)->where('order_status','DELIVERED')->count();
            return $this->sendResponse(
                $data,
                __('lang.messages.retrieved', ['model' => __('models/stores.singular')])
            );
        }else{
            return $this->sendError('You are not authorized to access this resource');
        }
    }

    /**
     * Remove the specified Store from storage.
     * DELETE /stores/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Store $store */
        $store = $this->storeRepository->find($id);

        if (empty($store)) {
            return $this->sendError(
                __('lang.messages.not_found', ['model' => __('models/stores.singular')])
            );
        }

        $store->delete();

        return $this->sendResponse(
            $id,
            __('lang.messages.deleted', ['model' => __('models/stores.singular')])
        );
    }

    // public function getStoresByZone(Request $request)
    // {
    //     $user = auth()->user();
        
    //     $type = $request->get('type');
    //     $this->storeRepository->pushCriteria(new RequestCriteria($request));
    //     $this->storeRepository->pushCriteria(new NearCriteria($request));
    //     $this->storeRepository->pushCriteria(new VendorActiveCriteria());
    //     $this->storeRepository->pushCriteria(new InStoreCategoriesCriteria($request));
    //     if ($type!='vendor'){
    //         $this->storeRepository->pushCriteria(new ActiveCriteria());
    //         $this->storeRepository->pushCriteria(new EmptyStoreCriteria());
    //         $stores = $this->storeRepository->with(['store_timings','store_categories'])->paginate(10);
    //     }else{
    //         // $assignedStores = \Helpers::get_user_stores();
    //         // $zones = \Helpers::get_user_zones();
    //         $stores = $this->storeRepository->with(['store_timings','store_categories'])
    //         // ->whereIn('zone', $zones)
    //         // ->whereIn('zone', $zones)
    //         ->paginate(10);
    //     }
        
    //     return $this->sendResponse(
    //         $stores->toArray(),
    //         __('lang.messages.retrieved', ['model' => __('models/stores.plural')])
    //     );
    // }

}
