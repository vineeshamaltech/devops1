<?php

namespace App\Http\Controllers\API;
use App\Events\Sockets\DriverLocation;
use App\Models\Order;
use App\Notifications\WalletNotification;
use App\Http\Controllers\AppBaseController;
use App\Http\Controllers\Controller;
use App\Models\Driver;
use App\Models\Earning;
use App\Models\DriverKyc;
use App\Models\DriverDutySession;
use App\Models\User;
use App\Models\Vendor;
use Notification;
use App\Helpers\GlobalHelpers;
use Carbon\Carbon;
use Craftsys\Msg91\Facade\Msg91;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Str;

/**
 * Class DriverAuthAPIController
 * @package App\Http\Controllers\API
 * @group Driver Authentication
 */
class DriverAuthAPIController extends AppBaseController
{
    /**Login using mobile number and password
     *
     * @bodyParam mobile string required Email of the vendor.
     * @bodyParam password string required Password of the vendor.
     */
    public function login(Request $request)
    {
        $request->validate([
            'mobile' => 'required|string|max:13|min:13',
            'password' => 'required|string'
        ]);
        $country_code = substr($request->mobile, 0, 3);
        $mobile = substr($request->mobile, 3);
        $credentials = request([$mobile, 'password']);
        if (!Auth::guard('driver')->attempt($credentials)) {
            return $this->sendError( 'Incorrect email or password');
        }
        $driver = auth('vendor')->user();
        
        
        if($driver == null){
            return $this->sendError('User not found');
        }
        if($driver->active == 0){
            return $this->sendError('Your account is not active');
        }
        $driver->tokens()->delete();
        $driver->token = $driver->createToken('user_authentication_token')->plainTextToken;
        $driver->token_type = 'Bearer';
        
          $driverkyc = DriverKyc::where('driver_id', $driver->id)->get();
      if($driverkyc != null)
      {
         foreach($driverkyc as $kyc)
         {
             if($kyc->doc_type == "DL")
             $driver['dl_photo']=$kyc->photo;
             else if($kyc->doc_type == "VEHICLEIMG")
              $driver['vehicle_img']=$kyc->photo;
               else if($kyc->doc_type == "AADHARBACK")
              $driver['aadhar_photo_back']=$kyc->photo;
               else if($kyc->doc_type == "AADHAR")
              $driver['aadhar_photo']=$kyc->photo;
         }
        }
        
        
        
        return $this->sendResponse($driver, 'Login Successful');
    }

    /**
     * Login using phone number
     *
     * This will send a verification code to the vendor's phone number via MSG91. You can only send the is_voice parameter as true if you are resending the OTP.
     * @bodyParam  mobile string required Phone no of vendor in the format +919999999999
     * @return Response
     * */
    public function phoneLogin(Request $request)
    {
        $request->validate([
            'mobile' => 'required|string|min:13|max:13',
            'is_voice' => 'nullable|boolean'
        ]);
        $country_code = substr($request->mobile, 0, 3);
        $mobile = substr($request->mobile, 3);
        $driver = Driver::where('mobile', $mobile)->where('country_code',$country_code)->first();
        if (in_array($mobile, \Helpers::getTestPhoneNo())) {
            return $this->sendResponse(array('status_code'=>200), 'Test OTP Sent Successfully');
        }
        if($request->is_voice){
            $phone = str_replace('+', '', $request->mobile);
            $res = Msg91::otp()->to($phone)->viaVoice()->resend();
            if ($res->status_code == "200"){
                return $this->sendResponse($res, 'OTP Sent Successfully');
            }else{
                return $this->sendError('Error sending OTP');
            }
        }else{
            $phone = str_replace('+', '', $request->mobile);
            $res = Msg91::otp()->to($phone)->send();
            if ($res->status_code == "200"){
                return $this->sendResponse($res, 'OTP Sent Successfully');
            }else{
                return $this->sendError('Error sending OTP');
            }
        }

    }
    
      public function driverRegister(Request $request){
        $request->validate([
            'mobile' => 'required|string|min:13|max:13',
            // 'otp' => 'required|string|min:4|max:4',
        ]);
        $country_code = substr($request->mobile, 0, 3);
        $mobile = substr($request->mobile, 3);
        $phone = str_replace('+', '', $request->mobile);
        //Demo login added
        if (in_array($mobile, \Helpers::getTestPhoneNo())) {
            $driver = Driver::with('kyc')->where('mobile', $mobile)->where('country_code',$country_code)->first();
            if($driver == null){
                return $this->sendError('User not found');
            }
            if($driver->active == 0){
                return $this->sendError('Your account is not active');
            }
            $driver->tokens()->delete();
            $driver->token = $driver->createToken('user_authentication_token')->plainTextToken;
            $driver->token_type = 'Bearer';
            
             $driverkyc = DriverKyc::where('driver_id', $driver->id)->get();
      if($driverkyc != null)
      {
         foreach($driverkyc as $kyc)
         {
             if($kyc->doc_type == "DL")
             $driver['dl_photo']=$kyc->photo;
             else if($kyc->doc_type == "VEHICLEIMG")
              $driver['vehicle_img']=$kyc->photo;
               else if($kyc->doc_type == "AADHARBACK")
              $driver['aadhar_photo_back']=$kyc->photo;
               else if($kyc->doc_type == "AADHAR")
              $driver['aadhar_photo']=$kyc->photo;
         }
        }
            
            
            return $this->sendResponse($driver, 'Login Successful');
        }
        //Demo login end here
        try{
            // $res = Msg91::otp((int)$request->otp)->to($phone)->verify();
            // if($res->status_code == "200"){
                $user = Driver::where('mobile', $mobile)->where('country_code',$country_code)->first();
                if(empty($user)){
                    $user = Driver::create([
                        'country_code' => $country_code,
                        'mobile' => $mobile,
                        'password' => Hash::make(Str::random(16)),
                        'service_type_id' => 1
                    ]);
                    $user = Driver::find($user->id);
                }
                $user->tokens()->delete();
                $user->token = $user->createToken('user_authentication_token')->plainTextToken;
                $user->token_type = 'Bearer';
                
                 $driverkyc = DriverKyc::where('driver_id', $user->id)->get();
      if($driverkyc != null)
      {
         foreach($driverkyc as $kyc)
         {
             if($kyc->doc_type == "DL")
             $user['dl_photo']=$kyc->photo;
             else if($kyc->doc_type == "VEHICLEIMG")
              $user['vehicle_img']=$kyc->photo;
               else if($kyc->doc_type == "AADHARBACK")
              $user['aadhar_photo_back']=$kyc->photo;
               else if($kyc->doc_type == "AADHAR")
              $user['aadhar_photo']=$kyc->photo;
         }
        }
                
                
                return $this->sendResponse($user, 'Login Successful');
            // }else{
            //     return $this->sendError('OTP is not valid');
            // }
        }catch (\Exception $e){
            return $this->sendError($e->getMessage());
        }

    }

    /**
     * Verify Phone No
     *
     * @bodyParam  mobile string required Phone no of vendor
     * @bodyParam  otp string required OTP sent on vendor's phone no
     * @return Response
     * */
    public function verifyOtp(Request $request){
        $request->validate([
            'mobile' => 'required|string|min:13|max:13',
            'otp' => 'required|string|min:4|max:4',
        ]);
        $country_code = substr($request->mobile, 0, 3);
        $mobile = substr($request->mobile, 3);
        $phone = str_replace('+', '', $request->mobile);
        //Demo login added
        if (in_array($mobile, \Helpers::getTestPhoneNo())) {
            $driver = Driver::with('kyc')->where('mobile', $mobile)->where('country_code',$country_code)->first();
            if($driver == null){
                return $this->sendError('User not found');
            }
            if($driver->active == 0){
                return $this->sendError('Your account is not active');
            }
            $driver->tokens()->delete();
            $driver->token = $driver->createToken('user_authentication_token')->plainTextToken;
            $driver->token_type = 'Bearer';
            
             $driverkyc = DriverKyc::where('driver_id', $driver->id)->get();
      if($driverkyc != null)
      {
         foreach($driverkyc as $kyc)
         {
             if($kyc->doc_type == "DL")
             $driver['dl_photo']=$kyc->photo;
             else if($kyc->doc_type == "VEHICLEIMG")
              $driver['vehicle_img']=$kyc->photo;
               else if($kyc->doc_type == "AADHARBACK")
              $driver['aadhar_photo_back']=$kyc->photo;
               else if($kyc->doc_type == "AADHAR")
              $driver['aadhar_photo']=$kyc->photo;
         }
        }
            
            
            return $this->sendResponse($driver, 'Login Successful');
        }
        //Demo login end here
        try{
            $res = Msg91::otp((int)$request->otp)->to($phone)->verify();
            if($res->status_code == "200"){
                $user = Driver::where('mobile', $mobile)->where('country_code',$country_code)->first();
                if(empty($user)){
                    $user = Driver::create([
                        'country_code' => $country_code,
                        'mobile' => $mobile,
                        'password' => Hash::make(Str::random(16)),
                        'service_type_id' => 1
                    ]);
                    $user = Driver::find($user->id);
                }
                $user->tokens()->delete();
                $user->token = $user->createToken('user_authentication_token')->plainTextToken;
                $user->token_type = 'Bearer';
                
                 $driverkyc = DriverKyc::where('driver_id', $user->id)->get();
      if($driverkyc != null)
      {
         foreach($driverkyc as $kyc)
         {
             if($kyc->doc_type == "DL")
             $user['dl_photo']=$kyc->photo;
             else if($kyc->doc_type == "VEHICLEIMG")
              $user['vehicle_img']=$kyc->photo;
               else if($kyc->doc_type == "AADHARBACK")
              $user['aadhar_photo_back']=$kyc->photo;
               else if($kyc->doc_type == "AADHAR")
              $user['aadhar_photo']=$kyc->photo;
         }
        }
                
                
                return $this->sendResponse($user, 'Login Successful');
            }else{
                return $this->sendError('OTP is not valid');
            }
        }catch (\Exception $e){
            return $this->sendError($e->getMessage());
        }

    }

    /**
     * Sync driver
     * @authenticated
     *
     * @bodyParam  latitude string required Latitude of the vendor.
     * @bodyParam  longitude string required Longitude of the vendor.
     * @bodyParam  fcm_token string required FCM token of the vendor.
     *
     * @return Response
     */


    public function sync(Request $request){
        $driver = auth()->user();
        if ($request->has('latitude') && $request->has('longitude')) {
            $driver = Driver::find($driver->id);
            $driver->latitude = $request->latitude;
            $driver->longitude = $request->longitude;
            $driver->fcm_token = $request->fcm_token;
            $driver->bearing = $request->bearing;
            $driver->save();
        }
        $duty = DriverDutySession::where('driver_id', $driver->id)->whereNull('end_time')->first();
        $driver->duty = $duty;
        event(new DriverLocation($driver->id,$driver));
        
          $driverkyc = DriverKyc::where('driver_id', $driver->id)->get();
      if($driverkyc != null)
      {
         foreach($driverkyc as $kyc)
         {
             if($kyc->doc_type == "DL")
             $driver['dl_photo']=$kyc->photo;
             else if($kyc->doc_type == "VEHICLEIMG")
              $driver['vehicle_img']=$kyc->photo;
               else if($kyc->doc_type == "AADHARBACK")
              $driver['aadhar_photo_back']=$kyc->photo;
               else if($kyc->doc_type == "AADHAR")
              $driver['aadhar_photo']=$kyc->photo;
         }
        }
            
        
        return $this->sendResponse($driver, 'Driver synced successfully');
    }

    public function update(Request $request){
        $input = $request->all();
        if (!empty($input['mobile'])){
            unset($input['mobile']);
        }
        $upload=[];

        if ($request->hasFile('vehicle_img')){
            $input['vehicle_img'] = \Helpers::uploadImage($request->file('vehicle_img'), 'drivers');
            $upload1 = array('url' => $input['vehicle_img'] ,'name'=>'vehicle_img'   ,'type'=>'VEHICLEIMG',);
            array_push($upload,$upload1);
        }
        if ($request->hasFile('dl_photo')){
            $input['dl_photo'] = \Helpers::uploadImage($request->file('dl_photo'), 'drivers');
            $upload2 = array('url' => $input['dl_photo'] ,'name'=>'dl_photo'   ,'type'=>'DL', );
            array_push($upload,$upload2);
        }
        if ($request->hasFile('aadhar_photo')){
            $input['aadhar_photo'] = \Helpers::uploadImage($request->file('aadhar_photo'), 'drivers');
            $upload3 = array('url' => $input['aadhar_photo'] ,'name'=>'aadhar_photo'   ,'type'=>'AADHAR', );
            array_push($upload,$upload3);
        }
        if ($request->hasFile('aadhar_photo_back')){
            $input['aadhar_photo_back'] = \Helpers::uploadImage($request->file('aadhar_photo_back'), 'drivers');
            $upload4 = array('url' => $input['aadhar_photo_back'] ,'name'=>'aadhar_photo_back'   ,'type'=>'AADHARBACK', );
            array_push($upload,$upload4);
        }
        if ($request->hasFile('profile_img')){
            $input['profile_img'] = \Helpers::uploadImage($request->file('profile_img'), 'drivers');
        }
        if ($request->has('password')){
            $input['password'] = Hash::make($request->password);
        }

        Driver::where('id', Auth::user()->id)->update($input);
        if($upload != [])
        $this->upload_driver_kyc(Auth::user()->id,$upload);
        
        $driver = Driver::find(Auth::user()->id);
        return $this->sendResponse($driver, 'Driver updated successfully');
    }


   public function upload_driver_kyc($driver_id,$input){
       
       foreach ($input as $key => $value) {
            $checkKyc = DriverKyc::where('driver_id',$driver_id)->where('doc_type',$value['type'])->first();
            if($checkKyc == []) {
                  DriverKyc::create([
                    'driver_id' => $driver_id,
                    'doc_type' => $value['type'],
                    'photo' => $value['url'],
                    'status' => 'UPLOADED',
                ]);
                   
                
            }
            else {
                
             DriverKyc::where('driver_id',$driver_id)->where('doc_type',$value['type'])->update(['photo'=>$value['url'],'status'=> 'UPLOADED']);
            
            
            }
       }
   }

    /**
     * Vendor's Profile
     *
     * Just pass the Bearer token in the header
     * @authenticated
     * @return Response
     *
     * */
    public function profile(){
      
        $driver = auth()->user();
        $driver = Driver::with('kyc')->find($driver->id);
         $driverkyc = DriverKyc::where('driver_id', $driver->id)->get();
      
         foreach($driverkyc as $kyc)
         {
             if($kyc->doc_type == "DL")
             $driver['dl_photo']=$kyc->photo;
             else if($kyc->doc_type == "VEHICLEIMG")
              $driver['vehicle_img']=$kyc->photo;
               else if($kyc->doc_type == "AADHARBACK")
              $driver['aadhar_photo_back']=$kyc->photo;
               else if($kyc->doc_type == "AADHAR")
              $driver['aadhar_photo']=$kyc->photo;
         }
      
        if ($driver != null) {
            return $this->sendResponse($driver, 'Vendor profile fetched successfully');
        }else{
            return $this->sendError('Unauthenticated');
        }
    }

    public function toggle_duty(Request $request){
        $request->validate([
            'type' => 'required|in:on,off'
        ]);
        $user = auth()->user();
        $driver = Driver::where('id', $user->id)->first();
        if($driver->active == 0){
            return $this->sendError('You\'ve been deactivated by admin. Please contact support.');
        }
        if($driver->is_verified == 0){
            return $this->sendError('You are not verified yet. Please wait till the verification is completed');
        }
        if ($driver->cash_in_hand_overflow == 1){
            return $this->sendError('BootCash is over due. Please pay first.');
        }
        $last_duty = DriverDutySession::where('driver_id', $user->id)->orderBy('id', 'desc')->first();

        if ($last_duty == null || $last_duty->end_time!=null && $last_duty->start_time!=null){

            //Duty is ended. Start a new duty session
            $duty = DriverDutySession::create([
                'driver_id' => $driver->id,
                'start_time' => Carbon::now()
            ]);
            Driver::where('id', $user->id)->update([
                'on_duty' => true
            ]);
            return $this->sendResponse($duty, 'Duty started successfully');
        }else if ($last_duty->end_time==null){
            if (Order::where('driver_id', $driver->id)->whereNotIn('order_status',['ASSIGNED','DELIVERED','CANCELLED'])->count() > 0){
                return $this->sendError('Please deliver the order before going offline.');
            }

            Driver::where('id', $user->id)->update([
                'on_duty' => false
            ]);
            $last_duty->end_time = Carbon::now();
            $last_duty->save();
            $start = Carbon::parse($last_duty->start_time);
            $end = Carbon::parse($last_duty->end_time);

            $interval_in_seconds = $start->diffInSeconds($end);

            $interval_in_hours = $interval_in_seconds/3600;

            $min_sec_for_incentive = setting('min_hours_for_incentive',10) * 60 * 60;

            if ($interval_in_seconds >= $min_sec_for_incentive){
                $amount = setting('driver_incentive_per_hour',0);

                $amountSecond = round($amount/3600,4);

                $incentive_commission = round($interval_in_seconds * $amountSecond,2);
                $last_duty->active_time = $interval_in_hours;
                $last_duty->incentive = $incentive_commission;

                $driver = Driver::where('id',  $user->id)->first();
                $driver->incentive = $driver->incentive + $incentive_commission;


                $driver->save();
                Earning::create([
                    'payment_id' => NULL,
                    'order_id' =>NULL,
                    'entity_type' => Driver::class,
                    'entity_id' => $user->id,
                    'amount' => $incentive_commission,
                    'type' => 'INCENTIVE',
                    'remarks' => 'Incentive added for ' . intval( $interval_in_hours,0).' hours and '. round($interval_in_seconds/60,0) .' minutes',
                    'is_settled' => false
                ]);
                Notification::send($driver, new WalletNotification($incentive_commission.' credited to your incentive'));
            }else{
                $last_duty->active_time = $interval_in_hours;
                $last_duty->incentive = 0;
            }
            return $this->sendResponse($last_duty, 'Duty ended successfully');
        }

    }
}
