<?php

namespace App\Http\Controllers\Admin;
use App\Models\Order;
use App\Models\Store;
use App\Helpers\Helpers;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(){
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
          GoogleTagManager::set('pageType', 'mainhome');
        return view('home');
    }

    public function profile(){
        $user = auth()->user();
        return view('admin.profile',compact('user'));
    }

    public function settings(){
        $user = auth()->user();
        $dashboard = get_dashboards();
        return view('admin.dashboard',['dashboard'=>$dashboard]);
    }

   public function get_orders()
   {
       $user=auth()->user();
    
       $orderscount=0;

       if($user->type == 'Vendor')
        {
            $stores = \Helpers::get_user_stores($user->id); 
        
            $orders = Order::where('seen',0)->whereIn('store_id',$stores)->get();
        
        $orderscount = count($orders);
        if($orderscount >= 1)
        return $orderscount;

        }

        if($user->type == 'Admin')
        {
            $stores = \Helpers::get_user_stores($user->id); 
            $orders = Order::where('seen',0)->whereIn('store_id',$stores)->get();
        
        $orderscount = count($orders);
        if($orderscount >= 1)
        return $orderscount;

        }
    return  0;
   }



}
