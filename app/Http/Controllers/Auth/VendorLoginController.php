<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class VendorLoginController extends Controller
{
    public function loginform(){
        return view('auth.vendorlogin');
    }

    public function login(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required'
        ]);
        if (auth('vendor')->attempt(['email' => $request->email, 'password' => $request->password])) {
            $user = dd(auth('vendor')->user());
            return redirect()->route('vendor.categories.index');
        }else{
            dd('error');
        }
    }

    public function dashboard(){
        $user = auth('vendor')->user();
        $user->assignRole('Vendor');
        return redirect()->route('vendor.categories.index');
    }
}
