<?php

namespace App\DataTables;

use App\Models\Earnings;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Html\Column;

class EarningsDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable->addColumn('action', 'admin.earnings.datatables_actions');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Earnings $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Earnings $model)
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '120px', 'printable' => false, 'title' => __('lang.action')])
            ->parameters(array_merge(
                config('datatables-buttons.parameters'), [
                    'language' => json_decode(
                        file_get_contents(base_path('resources/lang/' . app()->getLocale() . '/datatable.json')
                        ), true)
                ]
            ));
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'id' => new Column(['title' => __('lang.id'), 'data' => 'id','width' => '80px']),
            'payment_id' => new Column(['title' => __('models/earnings.fields.payment_id'), 'data' => 'payment_id']),
            'credited_to' => new Column(['title' => __('models/earnings.fields.credited_to'), 'data' => 'credited_to']),
            'remarks' => new Column(['title' => __('models/earnings.fields.remarks'), 'data' => 'remarks']),
            'is_settled' => new Column(['title' => __('models/earnings.fields.is_settled'), 'data' => 'is_settled'])
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'earnings_datatable_' . time();
    }
}
