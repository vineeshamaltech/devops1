<?php

namespace App\DataTables;

use App\Helpers\DatatableHelpers;
use App\Models\VehicleType;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Html\Column;

class VehicleTypeDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);
        $dataTable->editColumn('icon', function ($vehicleType) {
            return DatatableHelpers::imageSection($vehicleType->icon);
        });
        $dataTable->editColumn('active', function ($vehicleType) {
            return DatatableHelpers::statusColumn($vehicleType->active);
        });
        $dataTable->addColumn('delete_m', static function ($category) {
            return '<input type="checkbox" class="delete" name="delete[]" value="'.$category->id.'"/>';
        });
        $dataTable->rawColumns(['icon', 'action','active','delete_m']);
        return $dataTable->addColumn('action', 'admin.vehicle_types.datatables_actions');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\VehicleType $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(VehicleType $model)
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '120px', 'printable' => false, 'title' => __('lang.action')])
            ->parameters(array_merge(
                config('datatables-buttons.parameters'), [
                    'language' => json_decode(
                        file_get_contents(base_path('resources/lang/' . app()->getLocale() . '/datatable.json')
                        ), true)
                ]
            ));
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'delete_m' => new Column(['orderable' => false, 'searchable' => false, 'title' => '<input type="checkbox" name="deleteAll" id="deleteAll"/>', 'data' => 'delete_m']),
            'id' => new Column(['title' => __('lang.id'), 'data' => 'id','width' => '80px']),
            'name' => new Column(['title' => __('models/vehicleTypes.fields.name'), 'data' => 'name']),
            'icon' => new Column(['title' => __('models/vehicleTypes.fields.icon'), 'data' => 'icon']),
            'active' => new Column(['title' => __('models/vehicleTypes.fields.active'), 'data' => 'active'])
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'vehicle_types_datatable_' . time();
    }
}
