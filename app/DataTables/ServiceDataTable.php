<?php

namespace App\DataTables;

use App\Models\Category;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Html\Column;

class ServiceDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable->addColumn('action', 'admin.services.datatables_actions');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Category $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Category $model)
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '120px', 'printable' => false, 'title' => __('lang.action')])
            ->parameters(array_merge(
                config('datatables-buttons.parameters'), [
                    'language' => json_decode(
                        file_get_contents(base_path('resources/lang/' . app()->getLocale() . '/datatable.json')
                        ), true)
                ]
            ));
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'id' => new Column(['title' => __('lang.id'), 'data' => 'id','width' => '80px']),
            'icon' => new Column(['title' => __('models/services.fields.icon'), 'data' => 'icon']),
            'bg_image' => new Column(['title' => __('models/services.fields.bg_image'), 'data' => 'bg_image','searchable' => false]),
            'name' => new Column(['title' => __('models/services.fields.name'), 'data' => 'name','searchable' => false]),
            'description' => new Column(['title' => __('models/services.fields.description'), 'data' => 'description','searchable' => false]),
            'type' => new Column(['title' => __('models/services.fields.type'), 'data' => 'type','searchable' => false]),
            'discount' => new Column(['title' => __('models/services.fields.discount'), 'data' => 'discount','searchable' => false]),
            'commission' => new Column(['title' => __('models/services.fields.commission'), 'data' => 'commission','searchable' => false]),
            'vehicles' => new Column(['title' => __('models/services.fields.vehicles'), 'data' => 'vehicles','searchable' => false]),
            'min_price' => new Column(['title' => __('models/services.fields.min_price'), 'data' => 'min_price','searchable' => false]),
            'driver_radius' => new Column(['title' => __('models/services.fields.driver_radius'), 'data' => 'driver_radius','searchable' => false]),
            'active' => new Column(['title' => __('models/services.fields.active'), 'data' => 'active','searchable' => false])
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'services_datatable_' . time();
    }
}
