<?php

namespace App\DataTables;

use App\Helpers\DatatableHelpers;
use App\Models\HomeService;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Html\Column;

class HomeServiceDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);
        $dataTable->editColumn('image', function ($service) {
            return DatatableHelpers::imageSection($service->image);
        });
        $dataTable->editColumn('description', function ($service) {
            return substr($service->description,0,50);
        });
        $dataTable->editColumn('discount_price', function ($service) {
            if($service->discount_price == $service->original_price){
                return 0;
            }else{
                return $service->discount_price;
            }
          
        });
        $dataTable->rawColumns(['image', 'action']);
        return $dataTable->addColumn('action', 'admin.home_services.datatables_actions');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\HomeService $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(HomeService $model)
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '120px', 'printable' => false, 'title' => __('lang.action')])
            ->parameters(array_merge(
                config('datatables-buttons.parameters'), [
                    'language' => json_decode(
                        file_get_contents(base_path('resources/lang/' . app()->getLocale() . '/datatable.json')
                        ), true)
                ]
            ));
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'id' => new Column(['title' => __('lang.id'), 'data' => 'id','width' => '80px']),
            'name' => new Column(['title' => __('models/homeServices.fields.name'), 'data' => 'name']),
            'description' => new Column(['title' => __('models/homeServices.fields.description'), 'data' => 'description']),
            'image' => new Column(['title' => __('models/homeServices.fields.image'), 'data' => 'image']),
            'original_price' => new Column(['title' => __('models/homeServices.fields.original_price'), 'data' => 'original_price']),
            'discount_price' => new Column(['title' => __('models/homeServices.fields.discount_price'), 'data' => 'discount_price'])
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'home_services_datatable_' . time();
    }
}
