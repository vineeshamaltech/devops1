<?php

namespace App\DataTables;

use App\Models\RideBooking;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Html\Column;

class RideBookingDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable->addColumn('action', 'admin.ride_bookings.datatables_actions');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\RideBooking $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(RideBooking $model)
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '120px', 'printable' => false, 'title' => __('lang.action')])
            ->parameters(array_merge(
                config('datatables-buttons.parameters'), [
                    'language' => json_decode(
                        file_get_contents(base_path('resources/lang/' . app()->getLocale() . '/datatable.json')
                        ), true)
                ]
            ));
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'id' => new Column(['title' => __('lang.id'), 'data' => 'id','width' => '80px']),
            'id' => new Column(['title' => __('models/rideBookings.fields.id'), 'data' => 'id']),
            'user_id' => new Column(['title' => __('models/rideBookings.fields.user_id'), 'data' => 'user_id']),
            'ride_id' => new Column(['title' => __('models/rideBookings.fields.ride_id'), 'data' => 'ride_id']),
            'from_name' => new Column(['title' => __('models/rideBookings.fields.from_name'), 'data' => 'from_name']),
            'from_long' => new Column(['title' => __('models/rideBookings.fields.from_long'), 'data' => 'from_long']),
            'to_lat' => new Column(['title' => __('models/rideBookings.fields.to_lat'), 'data' => 'to_lat']),
            'to_long' => new Column(['title' => __('models/rideBookings.fields.to_long'), 'data' => 'to_long']),
            'price' => new Column(['title' => __('models/rideBookings.fields.price'), 'data' => 'price'])
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'ride_bookings_datatable_' . time();
    }
}
