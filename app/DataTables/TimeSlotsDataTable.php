<?php

namespace App\DataTables;

use App\Models\TimeSlots;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;

class TimeSlotsDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);
        // $dataTable->editColumn('store_id',function ($store_timing){
        //     if($store_timing->store == null){
        //         return DatatableHelpers::dangerBadge('Store Not Found');
        //     }else {
        //         return $store_timing->store->name . '<span class="text-muted"> (' . $store_timing->store->id . ')</span>';
        //     }
        // });
        return $dataTable;
        // return $dataTable->addColumn('action', 'admin.time_slots.datatables_actions');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\StoreTiming $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(TimeSlots $model)
    {
        // $stores = \Helpers::get_user_stores();
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            // ->addAction(['width' => '120px', 'printable' => false, 'title' => __('lang.action')])
            ->parameters(array_merge(
                config('datatables-buttons.parameters'), [
                    'language' => json_decode(
                        file_get_contents(base_path('resources/lang/' . app()->getLocale() . '/datatable.json')
                        ), true),
                ]
            ));
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'id' => new Column(['title' => __('lang.id'), 'data' => 'id', 'width' => '80px']),
            'day' => new Column(['title' => __('models/timeSlots.fields.day'), 'data' => 'day']),
            'from' => new Column(['title' => __('models/timeSlots.fields.from'), 'data' => 'from']),
            'to' => new Column(['title' => __('models/timeSlots.fields.to'), 'data' => 'to']),
            'max_bookings' => new Column(['title' => __('models/timeSlots.fields.max'), 'data' => 'max_bookings']),

        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'time_slots_datatable_' . time();
    }
}
