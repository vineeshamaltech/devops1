<?php

namespace App\DataTables;

use App\Models\DeliveryFee;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Html\Column;

class DeliveryFeeDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);
        $dataTable->addColumn('delete_m', static function ($category) {
            return '<input type="checkbox" class="delete" name="delete[]" value="'.$category->id.'"/>';
        });
        $dataTable->rawColumns(['action','delete_m']);
        return $dataTable->addColumn('action', 'admin.delivery_fees.datatables_actions');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\DeliveryFee $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(DeliveryFee $model)
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '120px', 'printable' => false, 'title' => __('lang.action')])
            ->parameters(array_merge(
                config('datatables-buttons.parameters'), [
                    'language' => json_decode(
                        file_get_contents(base_path('resources/lang/' . app()->getLocale() . '/datatable.json')
                        ), true)
                ]
            ));
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'delete_m' => new Column(['orderable' => false, 'searchable' => false, 'title' => '<input type="checkbox" name="deleteAll" id="deleteAll"/>', 'data' => 'delete_m']),
            'id' => new Column(['title' => __('lang.id'), 'data' => 'id','width' => '80px']),
            'from_km' => new Column(['title' => __('models/deliveryFees.fields.from_km'), 'data' => 'from_km']),
            'to_km' => new Column(['title' => __('models/deliveryFees.fields.to_km'), 'data' => 'to_km']),
            'delivery_fee' => new Column(['title' => __('models/deliveryFees.fields.delivery_fee'), 'data' => 'delivery_fee']),
            'driver_commission' => new Column(['title' => __('models/deliveryFees.fields.driver_commission'), 'data' => 'driver_commission'])
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'delivery_fees_datatable_' . time();
    }
}
