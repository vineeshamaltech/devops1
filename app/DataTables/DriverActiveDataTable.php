<?php

namespace App\DataTables;

use App\Helpers\DatatableHelpers;
use App\Models\DriverDutySession;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Html\Column;
use DB;

class DriverActiveDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);
        $dataTable->editColumn('driver_id',function ($model){
            if ($model->driver != null) {
                return $model->driver->name;
            }else{
                return DatatableHelpers::dangerBadge("Deleted");
            }
        });
        $dataTable->editColumn('created_at',function ($driver){
            return DatatableHelpers::dateColumn($driver->created_at,'d M Y');
        });
        $dataTable->editColumn('start_time',function ($driver){
            if ($driver->start_time){
                return DatatableHelpers::timeColumn($driver->start_time);
            }else{
                return '-';
            }
        });
        $dataTable->editColumn('end_time',function ($driver){
            if ($driver->end_time){
                return DatatableHelpers::timeColumn($driver->end_time);
            }else{
                return '-';
            }
        });
        $dataTable->editColumn('time_diff',function ($driver){
            if ($driver->time_diff){
                return seconds_to_hms($driver->time_diff);
            }else{
                return '-';
            }
        });
        $dataTable->rawColumns([
            'created_at',
            'start_time',
            'end_time',
            'time_diff',
            'driver_id'
        ]);
        return $dataTable->addColumn('action', 'admin.drivers.datatables_actions');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Driver $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(DriverDutySession $model)
    {
        return $model->newQuery()->with('driver')->selectRaw('*,TIMESTAMPDIFF(SECOND,start_time,end_time) as time_diff')->where('driver_id',$this->id);


    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->parameters(array_merge(
                config('datatables-buttons.parameters'), [
                    'language' => json_decode(
                        file_get_contents(base_path('resources/lang/' . app()->getLocale() . '/datatable.json')
                        ), true)
                ]
            ));
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'id' => new Column(['title' => __('lang.id'), 'data' => 'id','width' => '80px']),
            'driver_id' => new Column(['title' => __('models/drivers.fields.name'), 'data' => 'driver_id','width' => '80px']),
            'created_at' => new Column(['title' => __('models/drivers.fields.date'), 'data' => 'created_at']),
            'start_time' => new Column(['title' => __('models/drivers.fields.start_time'), 'data' => 'start_time']),
            'end_time' => new Column(['title' => __('models/drivers.fields.end_time'), 'data' => 'end_time']),
            'time_diff' => new Column(['title' => __('models/drivers.fields.activetime'), 'data' => 'time_diff']),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'drivers_datatable_' . time();
    }
}
