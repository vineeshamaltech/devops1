<?php

namespace App\DataTables;

use App\Helpers\DatatableHelpers;
use App\Models\Driver;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Html\Column;

class DriverDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);
        $dataTable->editColumn('active',function ($driver){
            return DatatableHelpers::statusColumn($driver->active);
        });
        $dataTable->editColumn('is_verified',function ($driver){
            return DatatableHelpers::statusColumn($driver->is_verified);
        });
        $dataTable->editColumn('cash_in_hand',function ($driver){
            return DatatableHelpers::currencyColumn($driver->cash_in_hand);
        });
        $dataTable->rawColumns([
            'active',
            'is_verified',
            'action',
            'cash_in_hand'
        ]);
        return $dataTable->addColumn('action', 'admin.drivers.datatables_actions');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Driver $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Driver $model)
    {
        if (auth()->user()->hasRole('Franchisee') || auth()->user()->hasRole('Admin')) {
            $zones = \Helpers::get_user_zones();
            $zone = implode(',',$zones);
            $data = Driver::whereRaw('zones IN('.$zone.')');
            // $data = new Collection();
            // foreach ($zones as $key => $value) {
                // $data1 = Driver::whereRaw("find_in_set('".$value."',zones)")->get();
                // $data = $data->merge($data1);
            // }
        }
            else {
            $data =  $model->newQuery();
        }
        return $data;
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '120px', 'printable' => false, 'title' => __('lang.action')])
            ->parameters(array_merge(
                config('datatables-buttons.parameters'), [
                    'language' => json_decode(
                        file_get_contents(base_path('resources/lang/' . app()->getLocale() . '/datatable.json')
                        ), true)
                ]
            ));
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'id' => new Column(['title' => __('lang.id'), 'data' => 'id','width' => '80px']),
            'name' => new Column(['title' => __('models/drivers.fields.name'), 'data' => 'name']),
            'mobile' => new Column(['title' => __('models/drivers.fields.mobile'), 'data' => 'mobile']),
            'cash_in_hand' => new Column(['title' => __('models/drivers.fields.cash_in_hand'), 'data' => 'cash_in_hand']),
            'vehicle_type' => new Column(['title' => __('models/drivers.fields.vehicle_type'), 'data' => 'vehicle_type']),
            'active' => new Column(['title' => __('models/drivers.fields.active'), 'data' => 'active']),
            'is_verified' => new Column(['title' => __('models/drivers.fields.is_verified'), 'data' => 'is_verified'])
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'drivers_datatable_' . time();
    }
}
