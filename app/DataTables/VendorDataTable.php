<?php

namespace App\DataTables;

use App\Helpers\DatatableHelpers;
use App\Models\Admin;
use App\Models\Vendor;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Html\Column;

class VendorDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);
        $dataTable->editColumn('active',function ($vendor){
            return DatatableHelpers::statusColumn($vendor->active);
        });
        $dataTable->editColumn('verified',function ($vendor){
            return DatatableHelpers::booleanColumn($vendor->active);
        });
        $dataTable->rawColumns(['active','verified','action']);
        return $dataTable->addColumn('action', 'admin.vendors.datatables_actions');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Vendor $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Admin $model)
    {
        return $model->newQuery()->where('type','vendor');
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '120px', 'printable' => false, 'title' => __('lang.action')])
            ->parameters(array_merge(
                config('datatables-buttons.parameters'), [
                    'language' => json_decode(
                        file_get_contents(base_path('resources/lang/' . app()->getLocale() . '/datatable.json')
                        ), true)
                ]
            ));
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'id' => new Column(['title' => __('lang.id'), 'data' => 'id','width' => '80px']),
            'name' => new Column(['title' => __('models/vendors.fields.name'), 'data' => 'name']),
            'mobile' => new Column(['title' => __('models/vendors.fields.mobile'), 'data' => 'mobile']),
            'email' => new Column(['title' => __('models/vendors.fields.email'), 'data' => 'email']),
            'active' => new Column(['title' => __('models/vendors.fields.active'), 'data' => 'active']),
            'verified' => new Column(['title' => __('models/vendors.fields.verified'), 'data' => 'verified'])
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'vendors_datatable_' . time();
    }
}
