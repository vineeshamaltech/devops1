<?php

namespace App\DataTables;

use App\Helpers\DatatableHelpers;
use App\Models\User;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Html\Column;
use Illuminate\Support\Carbon;

class UserDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);
        $dataTable->editColumn('profile_img', function ($user) {
            return DatatableHelpers::imageSection($user->profile_img);
        });
        $dataTable->editColumn('status', function ($user) {
            return DatatableHelpers::statusColumn($user->status);
        });
        $dataTable->editColumn('wallet_amount', function ($user) {
            return DatatableHelpers::currencyColumn($user->wallet_amount);
        });
        $dataTable->editColumn('created_at', function ($user) {
            return DatatableHelpers::dateColumn($user->created_at);
        });
        $dataTable->rawColumns(['created_at','profile_img', 'status', 'wallet_amount','action']);
        return $dataTable->addColumn('action', 'admin.users.datatables_actions');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\User $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(User $model)
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '120px', 'printable' => false, 'title' => __('lang.action')])
            ->parameters(array_merge(
                config('datatables-buttons.parameters'), [
                    'language' => json_decode(
                        file_get_contents(base_path('resources/lang/' . app()->getLocale() . '/datatable.json')
                        ), true)
                ]
            ));
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'id' => new Column(['title' => __('lang.id'), 'data' => 'id']),
            'name' => new Column(['title' => __('models/users.fields.name'), 'data' => 'name']),
            'created_at' => new Column(['title' => __('models/users.fields.created_at'), 'data' => 'created_at']),
            'mobile' => new Column(['title' => __('models/users.fields.mobile'), 'data' => 'mobile']),
            'email' => new Column(['title' => __('models/users.fields.email'), 'data' => 'email']),
            'profile_img' => new Column(['title' => __('models/users.fields.profile_img'), 'data' => 'profile_img']),
            'wallet_amount' => new Column(['title' => __('models/users.fields.wallet_amount'), 'data' => 'wallet_amount','searchable' => false]),
            'status' => new Column(['title' => __('models/users.fields.active'), 'data' => 'status','searchable' => false])
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'users_datatable_' . time();
    }
}
