<?php

namespace App\DataTables;

use App\Helpers\DatatableHelpers;
use App\Models\Product;
use App\Models\ProductVariant;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Html\Column;

class ProductVariantDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);
        $dataTable->editColumn('product_id', function ($productVariant) {
            if ($productVariant->product == null){
                return DatatableHelpers::dangerBadge("Product not found");
            }else{
                return $productVariant->product->name;
            }
        });
        $dataTable->editColumn('image',function ($productVariant) {
            return DatatableHelpers::imageSection($productVariant->image);
        });
        $dataTable->editColumn('sku',function ($productVariant) {
            if($productVariant->sku < 10) {
                return DatatableHelpers::dangerBadge($productVariant->sku);
            }else{
                return $productVariant->sku;
            }
        });
        $dataTable->editColumn('price',function ($productVariant) {
            return DatatableHelpers::currencyColumn($productVariant->price);
        });
        $dataTable->rawColumns(['image','sku','price','action','is_open','product_id']);
        return $dataTable->addColumn('action', 'admin.product_variants.datatables_actions');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\ProductVariant $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(ProductVariant $model)
    {
        $stores = \Helpers::get_user_stores();
        $products = Product::whereIn('store_id',$stores)->pluck('id')->toArray();
        return $model->newQuery()->with('product')->whereIn('product_id',$products);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '120px', 'printable' => false, 'title' => __('lang.action')])
            ->parameters(array_merge(
                config('datatables-buttons.parameters'), [
                    'language' => json_decode(
                        file_get_contents(base_path('resources/lang/' . app()->getLocale() . '/datatable.json')
                        ), true)
                ]
            ));
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'id' => new Column(['title' => __('lang.id'), 'data' => 'id','width' => '80px']),
            'product_id' => new Column(['title' => __('models/productVariants.fields.product_id'), 'data' => 'product_id']),
            'name' => new Column(['title' => __('models/productVariants.fields.name'), 'data' => 'name']),
            'price' => new Column(['title' => __('models/productVariants.fields.price'), 'data' => 'price']),
            'sku' => new Column(['title' => __('models/productVariants.fields.sku'), 'data' => 'sku']),
            'image' => new Column(['title' => __('models/productVariants.fields.image'), 'data' => 'image']),
             'active' => new Column(['title' => __('Active'), 'data' => 'active']),
            'is_open' => new Column(['title' => __('Is_Open'), 'data' => 'is_open']),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'product_variants_datatable_' . time();
    }
}
