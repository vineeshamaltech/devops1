<?php

namespace App\DataTables;

use App\Helpers\DatatableHelpers;
use App\Models\Address;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Html\Column;

class AddressDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);
        $dataTable->editColumn('user_id', function ($address) {
            return $address->user != null ? $address->user->name : "-";
        });
        $dataTable->editColumn('is_default',function ($address) {
            return DatatableHelpers::booleanColumn($address->is_default);
        });
        $dataTable->rawColumns(['is_default','action','user_id']);
        return $dataTable->addColumn('action', 'admin.addresses.datatables_actions');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Address $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Address $model)
    {
        return $model->newQuery()->with("user");
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '120px', 'printable' => false, 'title' => __('lang.action')])
            ->parameters(array_merge(
                config('datatables-buttons.parameters'), [
                    'language' => json_decode(
                        file_get_contents(base_path('resources/lang/' . app()->getLocale() . '/datatable.json')
                        ), true)
                ]
            ));
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'id' => new Column(['title' => __('lang.id'), 'data' => 'id','width' => '80px']),
            'user_id' => new Column(['title' => __('models/addresses.fields.user_id'), 'data' => 'user_id']),
            'name' => new Column(['title' => __('models/addresses.fields.name'), 'data' => 'name']),
            'phone' => new Column(['title' => __('models/addresses.fields.phone'), 'data' => 'phone']),
            'address' => new Column(['title' => __('models/addresses.fields.address'), 'data' => 'address']),
            'is_default' => new Column(['title' => __('models/addresses.fields.is_default'), 'data' => 'is_default','searchable' => false])
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'addresses_datatable_' . time();
    }
}
