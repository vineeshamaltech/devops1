<?php

namespace App\DataTables;

use App\Helpers\DatatableHelpers;
use App\Models\Store;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Html\Column;

class StoreDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);
        // $dataTable->editColumn('admin_id',function ($store){
        //     if($store->vendor == null){
        //         return '<span class="badge badge-danger">Deleted</span>';
        //     }else{
        //         return $store->vendor->name;
        //     }
        // });
        $dataTable->editColumn('service_category_id',function ($store){
            if($store->serviceCategory == null){
                return '<span class="label label-danger">Deleted</span>';
            }else{
                return $store->serviceCategory->name;
            }
        });
        $dataTable->editColumn('image', function ($store) {
            return DatatableHelpers::imageSection($store->image);
        });
        $dataTable->editColumn('active', function ($store) {
            return DatatableHelpers::statusColumn($store->active);
        });
        $dataTable->editColumn('is_open', function ($store) {
            return DatatableHelpers::booleanColumn($store->is_open);
        });
        $dataTable->rawColumns(['image', 'action','is_open', 'active','admin_id']);
        return $dataTable->addColumn('action', 'admin.stores.datatables_actions');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Store $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Store $model)
    {
        if(auth()->user()->hasRole('Vendor')){
            return $model->newQuery()->with(['vendor','serviceCategory'])->where('admin_id',\Auth::user()->id);
            // ->whereIn('zone',$zones);
        }else if(auth()->user()->hasRole('Franchisee')){
            $zones = \Helpers::get_user_zones();
            return $model->newQuery()->with(['vendor','serviceCategory'])->whereIn('zone', $zones);
        }else{
           $result =  $model->with('vendor:id,name','serviceCategory')->select('stores.*')->newQuery();
              return $result;
            // return $model->newQuery()->with(['vendor','serviceCategory']);
            
        } 
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '120px', 'printable' => false, 'title' => __('lang.action')])
            ->parameters(array_merge(
                config('datatables-buttons.parameters'), [
                    'language' => json_decode(
                        file_get_contents(base_path('resources/lang/' . app()->getLocale() . '/datatable.json')
                        ), true)
                ]
            ));
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'id' => new Column(['title' => __('lang.id'), 'data' => 'id','width' => '20px']),
            'name' => new Column(['title' => __('models/stores.fields.name'), 'data' => 'name','searchable' => true]),
            'admin_id' => new Column(['title' => __('models/stores.fields.vendor_id'), 'data' => 'name', 'searchable' => true]),
            'service_cat_id' => new Column(['title' => __('models/stores.fields.service_category_id'), 'data' => 'service_category_id']),
            'image' => new Column(['title' => __('models/stores.fields.image'), 'data' => 'image','searchable' => false]),
            'is_open' => new Column(['title' => __('models/stores.fields.is_open'), 'data' => 'is_open','searchable' => false]),
            'active' => new Column(['title' => __('models/stores.fields.active'), 'data' => 'active','searchable' => false])
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'stores_datatable_' . time();
    }
}
