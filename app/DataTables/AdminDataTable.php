<?php

namespace App\DataTables;

use App\Helpers\DatatableHelpers;
use App\Models\Admin;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Html\Column;

class AdminDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);
        $dataTable->editColumn('active',function ($admin){
            return DatatableHelpers::statusColumn($admin->active);
        });
        $dataTable->editColumn('type',function ($admin){
            if (strcasecmp($admin->type,'Super Admin') == 0) {
                return '<span class="badge badge-success">'.$admin->type.'</span>';
            }else if (strcasecmp($admin->type,'Admin') == 0) {
                return '<span class="badge badge-primary">'.$admin->type.'</span>';
            }else if (strcasecmp($admin->type,'Vendor') == 0) {
                return '<span class="badge badge-warning">'.$admin->type.'</span>';
            }else if (strcasecmp($admin->type,'Franchisee') == 0) {
                return '<span class="badge badge-info">'.$admin->type.'</span>';
            }
        });
        $dataTable->editColumn('created_at',function ($admin){
            if (auth()->user()->type == 'Super Admin'){
                if($admin->canBeImpersonated()){
                    return '<a href="'.route('admin.impersonate',$admin->id).'"><span class="badge badge-primary">Login</span></a>';
                }else{
                    return "-";
                }
            }else{
                return DatatableHelpers::dangerBadge("Not Allowed");
            }
        });
        $dataTable->rawColumns(['active','action','type','created_at']);
        return $dataTable->addColumn('action', 'admin.admins.datatables_actions');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Admin $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Admin $model)
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '120px', 'printable' => false, 'title' => __('lang.action')])
            ->parameters(array_merge(
                config('datatables-buttons.parameters'), [
                    'language' => json_decode(
                        file_get_contents(base_path('resources/lang/' . app()->getLocale() . '/datatable.json')
                        ), true)
                ]
            ));
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'id' => new Column(['title' => __('lang.id'), 'data' => 'id','width' => '80px']),
            'name' => new Column(['title' => __('models/admins.fields.name'), 'data' => 'name']),
            'email' => new Column(['title' => __('models/admins.fields.email'), 'data' => 'email']),
            'type' => new Column(['title' => __('models/admins.fields.type'), 'data' => 'type']),
            'active' => new Column(['title' => __('models/admins.fields.active'), 'data' => 'active']),
            'created_at' => new Column(['title' => __('lang.impersonate'), 'data' => 'created_at'])
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'admins_datatable_' . time();
    }
}
