<?php

namespace App\DataTables;
use App\Helpers\DatatableHelpers;
use App\Models\Order;
use Carbon\Carbon;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Html\Column;
use Illuminate\Support\Facades\DB;
class OrderDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);
        $dataTable->editColumn('user_id', function ($order) {
            if ($order->user == null) {
                return DatatableHelpers::dangerBadge("User Deleted");
            } else {
                return $order->user->name;
            }
        });
        $dataTable->editColumn('pickup_from_store', function ($order) {
            if ($order->user == null) {
                return DatatableHelpers::dangerBadge("User Deleted");
            } else {
                return $order->user->mobile;
            }
        });
        $dataTable->editColumn('store_id', function ($order) {
            if ($order->store == null) {
                return DatatableHelpers::dangerBadge("No Store");
            } else {
                return $order->store->name;
            }
        });
        
        $dataTable->editColumn('order_type', function ($order) {
            if ($order->orderType == null) {
                return DatatableHelpers::dangerBadge("User Deleted");
            } else {
                return $order->service_category->name . ' (<small><cite title="Service Type">' . $order->orderType->name . '</cite></small>)';
            }
        });
        $dataTable->editColumn('grand_total', function ($order) {
            return DatatableHelpers::currencyColumn($order->grand_total);
        });
        $dataTable->editColumn('driver_id', function ($order) {
            if ($order->pickup_from_store == 1) {
                return "Pickup From Store";
            } else {
                if ($order->driver == null) {
                    return '<a type="button" data-orderid="' . $order->id . '" data-toggle="modal" data-target="#driverModal">' . DatatableHelpers::dangerBadge("No Driver") . '</a>';
                } else {
                    $driver_details = $order->driver->name ?? $order->driver->mobile;
                    $driverhtml = '
                    <a href="' . route('admin.drivers.show', $order->driver->id) . '">' . $driver_details . '</a><br>'
                    . '<a type="button" data-orderid="' . $order->id . '" data-toggle="modal" data-target="#pingModal">' . DatatableHelpers::successBadge("Ping Driver") .
                    '</a>';
                    return $driverhtml;
                }
            }
        });
        $dataTable->editColumn('created_at', function ($order) {
            return DatatableHelpers::dateColumn($order->created_at);
        });
        $dataTable->rawColumns(['user_id', 'store_id', 'action', 'driver_id', 'order_type', 'grand_total', 'created_at']);
        return $dataTable->addColumn('action', 'admin.orders.datatables_actions');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Order $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Order $model)
    {
        if (auth()->user()->hasRole('Vendor')) {
            $stores = \Helpers::get_user_stores();
            $data =  $model->newQuery()->with('user','store', 'orderType', 'service_category')->whereIn('store_id', $stores);
        }
        else if (auth()->user()->hasRole('Franchisee') || auth()->user()->hasRole('Admin')) {
            $zones = \Helpers::get_user_zones();
            $zone= $this->zone;
            if(isset($zone) && $zone !="0"){
                $data =  $model->newQuery()->with('user','store', 'orderType', 'service_category')
                ->whereHas('store', function ($query) use ($zone) {
                    return $query->where('zone', $zone);
                });
            }
            else {
                $data =  $model->newQuery()->with('user','store', 'orderType', 'service_category')
                    ->whereHas('store', function ($query) use ($zones) {
                        return $query->whereIn('zone', $zones);
                    });
            }
        }
        else {
                $zone= $this->zone;
                if(isset($zone) && $zone !="0"){
                $data =  $model->newQuery()->with('user','store', 'orderType', 'service_category')
                ->whereHas('store', function ($query) use ($zone) {
                    return $query->where('zone', $zone);
                });
            }
            else {
            $data =  $model->newQuery()->with('user', 'store', 'orderType', 'service_category');
            }
        }
        if($this->daterange){
            $daterange = explode('-', $this->daterange);
            $startDate = Carbon::parse($daterange[0]);
            $endDate = Carbon::parse($daterange[1]);
            if ($endDate == $startDate) {
                $data = $data->whereDate('created_at', Carbon::now());
            } else {
                $data = $data->whereBetween('created_at', [$startDate, $endDate]);
            }
        }
        return $model->newQuery()->with('user','store')->select('orders.*')
        ->addSelect(DB::raw("concat(users.name,' (', users.mobile, ')') as user_name"))
        ->addSelect(DB::raw("concat(stores.name,' (', stores.mobile, ')') as store_name"))
        ->leftJoin('users', 'orders.user_id', '=', 'users.id')
        ->leftJoin('stores', 'orders.store_id', '=', 'stores.id');
        
        return $data;
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '120px', 'printable' => false, 'title' => __('lang.action')])
            ->parameters(array_merge(
                config('datatables-buttons.parameters'),
                [
                    'language' => json_decode(
                        file_get_contents(
                            base_path('resources/lang/' . app()->getLocale() . '/datatable.json')
                        ),
                        true
                    )
                ]
            ));
    }
    

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'id' => new Column(['title' => __('lang.id'), 'data' => 'id', 'width' => '20px','searchable' => true]),
            'user_id' => new Column(['title' => __('models/orders.fields.user_id'), 'data' => 'user.name','searchable' => true]),
            'pickup_from_store' => new Column(['title' => __('models/orders.fields.user_phone'), 'data' => 'pickup_from_store','searchable' => true]),
            'store_id' => new Column(['title' => __('models/orders.fields.store_id'), 'data' => 'store.name','searchable' => true]),
            'order_type' => new Column(['title' => __('models/orders.fields.order_type'), 'data' => 'order_type','searchable' => true]),
            'grand_total' => new Column(['title' => __('models/orders.fields.grand_total'), 'data' => 'grand_total','searchable' => true]),
            'driver_id' => new Column(['title' => __('models/orders.fields.driver_id'), 'data' => 'driver_id','searchable' => true]),
            'order_status' => new Column(['title' => __('models/orders.fields.order_status'), 'data' => 'order_status','searchable' => true]),
            'payment_status' => new Column(['title' => __('models/orders.fields.payment_status'), 'data' => 'payment_status','searchable' => true]),
            'payment_method' => new Column(['title' => __('models/orders.fields.payment_method'), 'data' => 'payment_method', 'searchable' => true]),
            'pickup_otp' => new Column(['title' => __('models/orders.fields.pickup_otp'), 'data' => 'pickup_otp', 'searchable' => true]),
            'created_at' => new Column(['title' => __('models/orders.fields.created_at'), 'data' => 'created_at', 'searchable' => true]),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'orders_datatable_' . time();
    }
}