<?php

namespace App\DataTables;

use App\Helpers\DatatableHelpers;
use App\Models\HomeServiceCategory;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Html\Column;

class HomeServiceCategoryDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);
        $dataTable->editColumn('icon', function ($paymentType) {
            return DatatableHelpers::imageSection($paymentType->icon);
        });
        $dataTable->editColumn('background_image', function ($paymentType) {
            return DatatableHelpers::imageSection($paymentType->background_image);
        });
        $dataTable->editColumn('parent',function ($homeservicecat){
            return $homeservicecat->parent_cat !=null ? $homeservicecat->parent_cat->name: '<strong>Parent Category</strong>';
        });
        $dataTable->rawColumns(['icon','background_image','status', 'action','parent']);
        return $dataTable->addColumn('action', 'admin.home_service_categories.datatables_actions');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\HomeServiceCategory $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(HomeServiceCategory $model)
    {
        $data = $model->with('parent_cat');
        // $data = $model->newQuery()->with('parent_cat')->whereIn('store_id',\Helpers::get_user_stores())->with('store');
    
        return $data;
        // return $model->with('parent')->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '120px', 'printable' => false, 'title' => __('lang.action')])
            ->parameters(array_merge(
                config('datatables-buttons.parameters'), [
                    'language' => json_decode(
                        file_get_contents(base_path('resources/lang/' . app()->getLocale() . '/datatable.json')
                        ), true)
                ]
            ));
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'id' => new Column(['title' => __('lang.id'), 'data' => 'id','width' => '80px']),
            'name' => new Column(['title' => __('models/homeServiceCategories.fields.name'), 'data' => 'name']),
            'icon' => new Column(['title' => __('models/homeServiceCategories.fields.icon'), 'data' => 'icon']),
            'background_image' => new Column(['title' => __('models/homeServiceCategories.fields.background_image'), 'data' => 'background_image']),
            'parent' => new Column(['title' => __('models/homeServiceCategories.fields.parent'), 'data' => 'parent']),
            'tax' => new Column(['title' => __('models/homeServiceCategories.fields.tax'), 'data' => 'tax']),
            'admin_commission' => new Column(['title' =>'Admin Commission', 'data' => 'admin_commission'])
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'home_service_categories_datatable_' . time();
    }
}
