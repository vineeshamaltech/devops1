<?php

namespace App\DataTables;

use App\Helpers\DatatableHelpers;
use App\Models\Earning;
use App\Models\Order;
use App\Models\Payment;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Html\Column;

class EarningDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);
        $dataTable->editColumn('payment_id',function ($model){
            if ($model->payment_id == null){
                return '-';
            }else{
                return '#'.$model->payment_id;
            }
        });
        $dataTable->editColumn('order_id',function ($model){
            if ($model->order_id == null){
                return '-';
            }else{
                return '#'.$model->order_id;
            }
        });
        $dataTable->editColumn('is_settled', function ($model) {
            return DatatableHelpers::booleanColumn($model->is_settled);
        });
        $dataTable->editColumn('amount', function ($model) {
            return DatatableHelpers::currencyColumn($model->amount);
        });
        $dataTable->editColumn('entity_type', function ($model) {
            try {
                $entity = explode("\\",$model->entity_type);
                return $model->entity_type::where('id',$model->entity_id)->first()->name.'('.end($entity).')';
            }catch (\Exception $exception){
                $entity = explode("\\",$model->entity_type);
                return DatatableHelpers::dangerBadge("".end($entity)." (".$model->entity_id.")");
            }
        });
        $dataTable->editColumn('created_at', function ($model) {
            return DatatableHelpers::dateColumn($model->created_at);
        });
        $dataTable->addColumn('delete_m', static function ($category) {
            return '<input type="checkbox" class="delete" name="delete[]" value="'.$category->id.'"/>';
        });
        $dataTable->rawColumns(['created_at','action','order_id','payment_id','entity_type','amount','delete_m']);
        return $dataTable->addColumn('action', 'admin.earnings.datatables_actions');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Earning $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Earning $model)
    {
    $result= $model->newQuery()->with('earnings');
    
    return $result;
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '120px', 'printable' => false, 'title' => __('lang.action')])
            ->parameters(array_merge(
                config('datatables-buttons.parameters'), [
                    'language' => json_decode(
                        file_get_contents(base_path('resources/lang/' . app()->getLocale() . '/datatable.json')
                        ), true)
                ]
            ));
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'delete_m' => new Column(['title' => '<input type="checkbox" name="deleteAll" id="deleteAll"/>', 'data' => 'delete_m']),
            'id' => new Column(['title' => __('lang.id'), 'data' => 'id','width' => '80px']),
           'orders_id' => new Column(['title' => __('models/earnings.fields.order_id'), 'data' => 'order_id','width' => '80px']),
          
            'payment_id' => new Column(['title' => __('models/earnings.fields.payment_id'), 'data' => 'payment_id']),
            'order_id' => new Column(['title' => __('models/earnings.fields.order_id'), 'data' => 'order_id']),
            'entity_name' => new Column(['title' => __('models/earnings.fields.entity_name'), 'data' => 'entity_type']),
            'service_type' => new Column(['title' =>'Service Type', 'data' => 'service','sortable' => false]),
            'amount' => new Column(['title' => __('models/earnings.fields.amount'), 'data' => 'amount']),
            'remarks' => new Column(['title' => __('models/earnings.fields.remarks'), 'data' => 'remarks']),
            'created_at' => new Column(['title' => __('models/earnings.fields.created_at'), 'data' => 'created_at'])
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'earnings_datatable_' . time();
    }
}
