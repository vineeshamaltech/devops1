<?php

namespace App\DataTables;

use App\Helpers\DatatableHelpers;
use App\Models\Coupon;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Html\Column;

class CouponDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);
        $dataTable->editColumn('code', function ($coupon) {
            return '<a href="' . route('admin.coupons.edit', $coupon->id) . '">' . $coupon->code . '</a>';
        });
        $dataTable->editColumn('image',function ($coupon) {
            return DatatableHelpers::imageSection($coupon->image);
        });
        $dataTable->editColumn('active', function ($coupon) {
            return DatatableHelpers::statusColumn($coupon->active);
        });
        $dataTable->editColumn('start_date', function ($coupon) {
            return DatatableHelpers::dateColumn($coupon->start_date);
        });
        $dataTable->editColumn('end_date', function ($coupon) {
            return DatatableHelpers::dateColumn($coupon->end_date);
        });
        // $dataTable->editColumn('store_id',function ($coupon) {
        //     if($coupon->store_id == 0) {
        //         return 'All Stores';
        //     }else{
        //         if($coupon->store == null){
        //             return DatatableHelpers::dangerBadge("Deleted");
        //         }else{
        //             return $coupon->store->name. '<span class="text-muted"> (' . $coupon->store->id . ')</span>';
        //         }
        //     }
        // });
        $dataTable->editColumn('discount_value', function ($coupon) {
            return $coupon->discount_value . '%';
        });
        $dataTable->addColumn('delete_m', static function ($category) {
            return '<input type="checkbox" class="delete" name="delete[]" value="'.$category->id.'"/>';
        });
        $dataTable->rawColumns(['code', 'store_id','image', 'active','action','start_date','end_date','delete_m']);
        return $dataTable->addColumn('action', 'admin.coupons.datatables_actions');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Coupon $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Coupon $model)
    {
        if (auth()->user()->hasRole('Super Admin')) {
          $result = $model->with('store:id,name')->select('coupons.*')->newQuery();
        //   $result =$model->newQuery()->with('store');
        //    dd($result->get());
            return $result;
        } else {
            return $model->newQuery()->with('store')->whereIn('store_id',\Helpers::get_user_stores());
        }
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '120px', 'printable' => false, 'title' => __('lang.action')])
            ->parameters(array_merge(
                config('datatables-buttons.parameters'), [
                    'language' => json_decode(
                        file_get_contents(base_path('resources/lang/' . app()->getLocale() . '/datatable.json')
                        ), true)
                ]
            ));
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'delete_m' => new Column(['orderable' => false, 'searchable' => false, 'title' => '<input type="checkbox" name="deleteAll" id="deleteAll"/>', 'data' => 'delete_m']),
            'id' => new Column(['title' => __('lang.id'), 'data' => 'id','width' => '30px']),
            'store_id' => new Column(['title' => __('models/coupons.fields.store_name'), 'data' => 'store.name']),
            'image' => new Column(['title' => __('models/coupons.fields.image'), 'data' => 'image']),
            'code' => new Column(['title' => __('models/coupons.fields.code'), 'data' => 'code']),
            'start_date' => new Column(['title' => __('models/coupons.fields.start_date'), 'data' => 'start_date']),
            'end_date' => new Column(['title' => __('models/coupons.fields.end_date'), 'data' => 'end_date']),
            'discount_type' => new Column(['title' => __('models/coupons.fields.discount_type'), 'data' => 'discount_type']),
            'discount_value' => new Column(['title' => __('models/coupons.fields.discount_value'), 'data' => 'discount_value']),
            'active' => new Column(['title' => __('models/coupons.fields.active'), 'data' => 'active'])
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'coupons_datatable_' . time();
    }
}
