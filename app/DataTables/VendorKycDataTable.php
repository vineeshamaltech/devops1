<?php

namespace App\DataTables;

use App\Helpers\DatatableHelpers;
use App\Models\Admin;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Html\Column;

class VendorKycDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);
        $dataTable->editColumn('verified',function ($vendorKyc) {
            if ($vendorKyc->verified == 1) {
                return '<span class="badge badge-success">Verified</span>';
            } else {
                return '<span class="badge badge-danger">Not Verified</span>';
            }
        });
        $dataTable->editColumn('created_at',function ($vendorKyc) {
            return $vendorKyc->kyc_count;
        });
        $dataTable->editColumn('action',function ($vendorKyc) {
            return '<a href="'.route('admin.admins.show',$vendorKyc->id).'" class="btn btn-info btn-sm"><i class="fa fa-eye"></i></a>';
        });

        $dataTable->rawColumns(['verified','action','doc_count']);
        return $dataTable->addColumn('action', 'admin.vendor_kycs.datatables_actions');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\VendorKyc $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Admin $model)
    {
        return $model->newQuery()->where('type','Vendor')->withCount('kyc');
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->parameters(array_merge(
                config('datatables-buttons.parameters'), [
                    'language' => json_decode(
                        file_get_contents(base_path('resources/lang/' . app()->getLocale() . '/datatable.json')
                        ), true)
                ]
            ));
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'id' => new Column(['title' => __('lang.id'), 'data' => 'id','width' => '80px']),
            'name' => new Column(['title' => __('models/admins.fields.name'), 'data' => 'name']),
            'created_at' => new Column(['title' => __('models/vendorKycs.fields.doc_count'), 'data' => 'created_at']),
            'verified' => new Column(['title' => __('models/vendorKycs.fields.status'), 'data' => 'verified']),
            'action' => new Column(['title' => __('lang.action'), 'data' => 'action','width' => '80px']),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'vendor_kycs_datatable_' . time();
    }
}
