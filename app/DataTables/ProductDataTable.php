<?php

namespace App\DataTables;

use App\Helpers\DatatableHelpers;
use App\Models\Product;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Html\Column;

class ProductDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);
            $dataTable->addColumn('select_products', static function ($row) {
            return '<input type="checkbox" name="products[]" value="'.$row->id.'"/>';
        })->rawColumns(['select_products']);
        $dataTable->editColumn('image', function ($product) {
            return DatatableHelpers::imageSection($product->image);
        });
        $dataTable->editColumn('price', function ($product) {
            return DatatableHelpers::currencyColumn($product->price);
        });
        $dataTable->editColumn('discount_price', function ($product) {
            return DatatableHelpers::currencyColumn($product->discount_price);
        });
        $dataTable->editColumn('status', function ($product) {
            if ($product->status == 1) {
                return '<span class="badge badge-success">Active</span>';
            } else {
                return '<span class="badge badge-danger">Inactive</span>';
            }
        });
        $dataTable->editColumn('sku', function ($product) {
            if($product->sku < 10) {
                return '<span class="badge badge-danger">'.$product->sku.'</span>';
            } else {
                return '<span class="badge badge-success">'.$product->sku.'</span>';
            }
        });
        // $dataTable->editColumn('store_id',function ($product){
        //     if ($product->store_id !=null){
        //         return $product->store->name. '<span class="text-muted"> (' . $product->store_id . ')</span>';
        //     }else{
        //         return DatatableHelpers::dangerBadge("Deleted");
        //     }
        // });
        $dataTable->addColumn('delete_m', static function ($category) {
            return '<input type="checkbox" class="delete" name="delete[]" value="'.$category->id.'"/>';
        });
        $dataTable->rawColumns(['image','store_id', 'status', 'sku', 'price', 'discount_price','action','delete_m']);
        return $dataTable->addColumn('action', 'admin.products.datatables_actions');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Product $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Product $model)
    {         return  $model->with('store:id,name')->select('products.*')->newQuery()->whereIn('store_id',\Helpers::get_user_stores());           
        // $data = $model->newQuery()->whereIn('store_id',\Helpers::get_user_stores());
      
        // return $data;
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '120px', 'printable' => false, 'title' => __('lang.action')])
            ->parameters(array_merge(
                config('datatables-buttons.parameters'), [
                    'language' => json_decode(
                        file_get_contents(base_path('resources/lang/' . app()->getLocale() . '/datatable.json')
                        ), true)
                ]
            ));
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'delete_m' => new Column(['orderable' => false, 'searchable' => false, 'title' => '<input type="checkbox" name="deleteAll" id="deleteAll"/>', 'data' => 'delete_m']),
            'id' => new Column(['title' => __('lang.id'), 'data' => 'id','width' => '80px']),
            'name' => new Column(['title' => __('models/products.fields.name'), 'data' => 'name']),
            'image' => new Column(['title' => __('models/products.fields.image'), 'data' => 'image','searchable' => false]),
            'unit' => new Column(['title' => __('models/products.fields.unit'), 'data' => 'unit','searchable' => false]),
            'store_id' => new Column(['title' => __('models/products.fields.store_id'), 'data' => 'store.name','searchable' => true, 'visible' => true]),
            'discount_price' => new Column(['title' => __('models/products.fields.discount_price'), 'data' => 'discount_price','searchable' => false]),
            'tax' => new Column(['title' => __('models/products.fields.tax'), 'data' => 'tax','searchable' => false]),
            'sku' => new Column(['title' => __('models/products.fields.sku'), 'data' => 'sku','searchable' => false]),
            'active' => new Column(['title' => __('models/products.fields.active'), 'data' => 'active','searchable' => false])
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'products_datatable_' . time();
    }
}
