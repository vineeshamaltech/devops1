<?php

namespace App\DataTables;

use App\Models\ServiceOrder;
use App\Helpers\DatatableHelpers;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;

class HomeServiceOrderDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);
        $dataTable->editColumn('user_id', function ($order) {
            if ($order->user == null) {
                return DatatableHelpers::dangerBadge("User Deleted");
            } else {
                return $order->user->name;
            }
        });
        $dataTable->editColumn('grand_total', function ($order) {
            return DatatableHelpers::currencyColumn($order->grand_total);
        });
        $dataTable->editColumn('driver_id', function ($order) {
            if ($order->driver == null) {
                return '<a type="button" data-orderid="' . $order->id . '" data-toggle="modal" data-target="#driverModal">' . DatatableHelpers::dangerBadge("No Driver") . '</a>';
            } else {
                // return '<a href="' . route('admin.drivers.show', $order->driver->id) . '">' . $order->driver->name ?? $order->driver->mobile . '</a><a data-orderid="' . $order->id . '" data-toggle="modal" data-target="#statusModal"><i class="fa fa-edit"></i></a>';
          $driver_details = $order->driver->name ?? $order->driver->mobile;
                    $driverhtml = '
                    <a href="' . route('admin.drivers.show', $order->driver->id) . '">' . $driver_details . '</a><br>'
                    . '<a type="button" data-orderid="' . $order->id . '" data-toggle="modal" data-target="#pingModal">' . DatatableHelpers::successBadge("Ping Driver") .
                    '</a>';
                    return $driverhtml;
            }
        });
        $dataTable->editColumn('created_at', function ($order) {
            return DatatableHelpers::dateColumn($order->created_at);
        });
        $dataTable->rawColumns(['user_id', 'action', 'driver_id', 'grand_total', 'created_at']);
        return $dataTable->addColumn('action', 'admin.service_orders.datatables_actions');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Order $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(ServiceOrder $model)
    {
        if (auth()->user()->hasRole('Vendor')) {
            $zones = \Helpers::get_user_zones();
            return $model->newQuery()->with('user')
            ->whereIn('zone_id',$zones);
        } else {
            return $model->newQuery()->with('user');
        }

    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '120px', 'printable' => false, 'title' => __('lang.action')])
            ->parameters(array_merge(
                config('datatables-buttons.parameters'), [
                    'language' => json_decode(
                        file_get_contents(base_path('resources/lang/' . app()->getLocale() . '/datatable.json')
                        ), true),
                ]
            ));
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'id' => new Column(['title' => __('lang.id'), 'data' => 'id', 'width' => '20px']),
            'user_id' => new Column(['title' => __('models/homeServiceOrders.fields.user_id'), 'data' => 'user_id']),
            'driver_id' => new Column(['title' => __('models/homeServiceOrders.fields.driver_id'), 'data' => 'driver_id']),
            'grand_total' => new Column(['title' => __('models/homeServiceOrders.fields.grand_total'), 'data' => 'grand_total']),
            'order_status' => new Column(['title' => __('models/homeServiceOrders.fields.order_status'), 'data' => 'order_status']),
            'payment_status' => new Column(['title' => __('models/homeServiceOrders.fields.payment_status'), 'data' => 'payment_status']),
            'payment_method' => new Column(['title' => __('models/homeServiceOrders.fields.payment_method'), 'data' => 'payment_method', 'visible' => false]),
            'start_otp' => new Column(['title' => __('models/homeServiceOrders.fields.start_otp'), 'data' => 'start_otp', 'visible' => false]),
            'delivery_otp' => new Column(['title' => __('models/homeServiceOrders.fields.delivery_otp'), 'data' => 'delivery_otp', 'visible' => false]),
            'created_at' => new Column(['title' => __('models/homeServiceOrders.fields.created_at'), 'data' => 'created_at', 'visible' => false]),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'service_orders_datatable_' . time();
    }
}
