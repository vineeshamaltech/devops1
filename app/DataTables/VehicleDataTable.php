<?php

namespace App\DataTables;

use App\Models\Vehicle;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Html\Column;

class VehicleDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable->addColumn('action', 'admin.vehicles.datatables_actions');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Vehicle $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Vehicle $model)
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '120px', 'printable' => false, 'title' => __('lang.action')])
            ->parameters(array_merge(
                config('datatables-buttons.parameters'), [
                    'language' => json_decode(
                        file_get_contents(base_path('resources/lang/' . app()->getLocale() . '/datatable.json')
                        ), true)
                ]
            ));
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'id' => new Column(['title' => __('lang.id'), 'data' => 'id','width' => '80px']),
            'brand' => new Column(['title' => __('models/vehicles.fields.brand'), 'data' => 'brand']),
            'type' => new Column(['title' => __('models/vehicles.fields.type'), 'data' => 'type']),
            'variant' => new Column(['title' => __('models/vehicles.fields.variant'), 'data' => 'variant']),
            'color' => new Column(['title' => __('models/vehicles.fields.color'), 'data' => 'color']),
            'reg_no' => new Column(['title' => __('models/vehicles.fields.reg_no'), 'data' => 'reg_no']),
            'photo' => new Column(['title' => __('models/vehicles.fields.photo'), 'data' => 'photo']),
            'active' => new Column(['title' => __('models/vehicles.fields.active'), 'data' => 'active'])
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'vehicles_datatable_' . time();
    }
}
