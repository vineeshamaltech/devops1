<?php

namespace App\DataTables;

use App\Models\Pincode;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Html\Column;

class PincodeDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable->addColumn('action', 'admin.pincodes.datatables_actions');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Pincode $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Pincode $model)
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '120px', 'printable' => false, 'title' => __('lang.action')])
            ->parameters(array_merge(
                config('datatables-buttons.parameters'), [
                    'language' => json_decode(
                        file_get_contents(base_path('resources/lang/' . app()->getLocale() . '/datatable.json')
                        ), true)
                ]
            ));
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'id' => new Column(['title' => __('lang.id'), 'data' => 'id','width' => '80px']),
            'id' => new Column(['title' => __('models/pincodes.fields.id'), 'data' => 'id']),
            'order_id' => new Column(['title' => __('models/pincodes.fields.order_id'), 'data' => 'order_id']),
            'store_rating' => new Column(['title' => __('models/pincodes.fields.store_rating'), 'data' => 'store_rating']),
            'driver_rating' => new Column(['title' => __('models/pincodes.fields.driver_rating'), 'data' => 'driver_rating']),
            'store_review' => new Column(['title' => __('models/pincodes.fields.store_review'), 'data' => 'store_review']),
            'driver_review' => new Column(['title' => __('models/pincodes.fields.driver_review'), 'data' => 'driver_review'])
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'pincodes_datatable_' . time();
    }
}
