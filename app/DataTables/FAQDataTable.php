<?php

namespace App\DataTables;

use App\Helpers\DatatableHelpers;
use App\Models\FAQ;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Html\Column;

class FAQDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);
        $dataTable->editColumn('active',function ($faq){
            return DatatableHelpers::statusColumn($faq->active);
        });
        $dataTable->editColumn('answer',function ($faq){
            return substr($faq->answer,0,100);
        });
        $dataTable->rawColumns(['active','answer','action']);
        return $dataTable->addColumn('action', 'admin.f_a_q_s.datatables_actions');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\FAQ $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(FAQ $model)
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '120px', 'printable' => false, 'title' => __('lang.action')])
            ->parameters(array_merge(
                config('datatables-buttons.parameters'), [
                    'language' => json_decode(
                        file_get_contents(base_path('resources/lang/' . app()->getLocale() . '/datatable.json')
                        ), true)
                ]
            ));
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'id' => new Column(['title' => __('lang.id'), 'data' => 'id','width' => '80px']),
            'question' => new Column(['title' => __('models/fAQS.fields.question'), 'data' => 'question']),
            'answer' => new Column(['title' => __('models/fAQS.fields.answer'), 'data' => 'answer','visible'=>false]),
            'active' => new Column(['title' => __('models/fAQS.fields.active'), 'data' => 'active'])
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'f_a_q_s_datatable_' . time();
    }
}
