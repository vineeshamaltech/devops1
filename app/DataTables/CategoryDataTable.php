<?php

namespace App\DataTables;

use App\Helpers\DatatableHelpers;
use App\Models\Category;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Html\Column;

class CategoryDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
      
        $dataTable = new EloquentDataTable($query);
       
        $dataTable->editColumn('parent',function ($category){
            return $category->parent_cat !=null ? $category->parent_cat->name : '<strong>Parent Category</strong>';
        });
        $dataTable->editColumn('image',function ($category){
            return DatatableHelpers::imageSection($category->image);
        });
        // $dataTable->editColumn('store_id',function ($category){
        //     if ($category->store_id !=null){
        //         return $category->store->name;
        //     }else{
        //         return DatatableHelpers::dangerBadge("Deleted");
        //     }
        // });
        $dataTable->addColumn('delete_m', static function ($category) {
            return '<input type="checkbox" class="delete" name="delete[]" value="'.$category->id.'"/>';
        });
        $dataTable->rawColumns(['image','action','parent','delete_m']);
        return $dataTable->addColumn('action', 'admin.categories.datatables_actions');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Category $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Category $model)
    {          
      $data = $model->with('store:id,name','parent_cat')->select('categories.*')->newQuery()->whereIn('store_id',\Helpers::get_user_stores());
        // $data = $model->newQuery()->with('parent_cat')->whereIn('store_id',\Helpers::get_user_stores())->with('store');
        
        return $data;
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
        
        ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '120px', 'printable' => false, 'title' => __('lang.action')])
            ->parameters(array_merge(
                config('datatables-buttons.parameters'), [
                    'language' => json_decode(
                        file_get_contents(base_path('resources/lang/' . app()->getLocale() . '/datatable.json')
                        ), true)
                ]
            ));
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'delete_m' => new Column(['orderable' => false, 'searchable' => false, 'title' => '<input type="checkbox" name="deleteAll" id="deleteAll"/>', 'data' => 'delete_m']),
            'id' => new Column(['title' => __('lang.id'), 'data' => 'id','width' => '80px','searchable' => false]),
            'name' => new Column(['title' => __('models/categories.fields.name'), 'data' => 'name']),
            'image' => new Column(['title' => __('models/categories.fields.image'), 'data' => 'image']),
            'parent' => new Column(['title' => __('models/categories.fields.parent'), 'data' => 'parent']),
            'store_id' => new Column(['title' => __('models/categories.fields.store'), 'data' => 'store.name','searchable' => true])
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'categories_datatable_' . time();
    }
}
