<?php

namespace App\DataTables;

use App\Helpers\DatatableHelpers;
use App\Models\PaymentType;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Html\Column;

class PaymentTypeDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);
        $dataTable->editColumn('icon', function ($paymentType) {
            return DatatableHelpers::imageSection($paymentType->icon);
        });
        $dataTable->editColumn('service', function ($paymentType) {
            if($paymentType->service == 1){
                return "ON";
            }else{
                return "OFF";
            }
        });
        $dataTable->editColumn('home_service', function ($paymentType) {
            if($paymentType->home_service == 1){
                return "ON";
            }else{
                return "OFF";
            }
        });
        $dataTable->rawColumns(['icon','service','home_service', 'action']);
        return $dataTable->addColumn('action', 'admin.payment_types.datatables_actions');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\PaymentType $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(PaymentType $model)
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '120px', 'printable' => false, 'title' => __('lang.action')])
            ->parameters(array_merge(
                config('datatables-buttons.parameters'), [
                    'language' => json_decode(
                        file_get_contents(base_path('resources/lang/' . app()->getLocale() . '/datatable.json')
                        ), true)
                ]
            ));
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'id' => new Column(['title' => __('lang.id'), 'data' => 'id','width' => '80px']),
            'name' => new Column(['title' => __('models/paymentTypes.fields.name'), 'data' => 'name','searchable' => true]),
            'description' => new Column(['title' => __('models/paymentTypes.fields.description'), 'data' => 'description','searchable' => false]),
            'service' => new Column(['title' => __('models/paymentTypes.fields.service'), 'data' => 'service','searchable' => false]),
            'home_service' => new Column(['title' => __('models/paymentTypes.fields.home_service'), 'data' => 'home_service','searchable' => false]),
            'icon' => new Column(['title' => __('models/paymentTypes.fields.icon'), 'data' => 'icon','searchable' => false]),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'payment_types_datatable_' . time();
    }
}
