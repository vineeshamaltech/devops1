<?php

namespace App\DataTables;

use App\Helpers\DatatableHelpers;
use App\Models\Payment;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Html\Column;

class PaymentDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);
        $dataTable->editColumn('amount', function ($payment) {
            return DatatableHelpers::currencyColumn($payment->amount/100);
        });
        $dataTable->rawColumns(['amount','action']);
        return $dataTable->addColumn('action', 'admin.payments.datatables_actions');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Payment $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Payment $model)
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '120px', 'printable' => false, 'title' => __('lang.action')])
            ->parameters(array_merge(
                config('datatables-buttons.parameters'), [
                    'language' => json_decode(
                        file_get_contents(base_path('resources/lang/' . app()->getLocale() . '/datatable.json')
                        ), true)
                ]
            ));
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'id' => new Column(['title' => __('lang.id'), 'data' => 'id','width' => '80px']),
            'ref_no' => new Column(['title' => __('models/payments.fields.ref_no'), 'data' => 'ref_no']),
            'payment_method' => new Column(['title' => __('models/payments.fields.payment_method'), 'data' => 'payment_method']),
            'amount' => new Column(['title' => __('models/payments.fields.amount'), 'data' => 'amount']),
            'gateway_response' => new Column(['title' => __('models/payments.fields.gateway_response'), 'data' => 'gateway_response','visible'=>false])
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'payments_datatable_' . time();
    }
}
