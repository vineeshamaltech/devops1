<?php

namespace App\DataTables;

use App\Helpers\DatatableHelpers;
use App\Models\UserWalletTransaction;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Html\Column;

class UserWalletTransactionDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);
        $dataTable->editColumn('user_id',function ($model){
           if ($model->user == null){
               return DatatableHelpers::dangerBadge('User Deleted');
           } else{
               return $model->user->name . '<br>' . $model->user->mobile;
           }
        });
        $dataTable->editColumn('amount',function ($model){
            return DatatableHelpers::currencyColumn($model->amount);
        });
        $dataTable->editColumn('type',function ($model){
            if ($model->type == 'CREDIT') {
                return '<span class="badge badge-success">Credit</span>';
            }elseif ($model->type == 'DEBIT'){
                return '<span class="badge badge-danger">Debit</span>';
            }
        });
        $dataTable->addColumn('delete_m', static function ($category) {
            return '<input type="checkbox" class="delete" name="delete[]" value="'.$category->id.'"/>';
        });
        $dataTable->rawColumns(['amount','type','action','user_id','delete_m']);
        return $dataTable->addColumn('action', 'admin.user_wallet_transactions.datatables_actions');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\UserWalletTransaction $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(UserWalletTransaction $model)
    {
       
        return $model->newQuery()->with(['user' => function ($query) {
            $query->select('id', 'name', 'mobile');
        }]);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '120px', 'printable' => false, 'title' => __('lang.action')])
            ->parameters(array_merge(
                config('datatables-buttons.parameters'), [
                    'language' => json_decode(
                        file_get_contents(base_path('resources/lang/' . app()->getLocale() . '/datatable.json')
                        ), true)
                ]
            ));
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'delete_m' => new Column(['orderable' => false, 'searchable' => true,'title' => '<input type="checkbox" name="deleteAll" id="deleteAll"/>', 'data' => 'delete_m']),
            'id' => new Column(['title' => __('lang.id'), 'data' => 'id','width' => '80px']),
             'user' => new Column(['title' => __('models/userWalletTransactions.fields.user'), 'data' => 'user.name','searchable' => true]),
            'amount' => new Column(['title' => __('models/userWalletTransactions.fields.amount'), 'data' => 'amount']),
            'type' => new Column(['title' => __('models/userWalletTransactions.fields.type'), 'data' => 'type']),
            'remarks' => new Column(['title' => __('models/userWalletTransactions.fields.remarks'), 'data' => 'remarks'])
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'user_wallet_transactions_datatable_' . time();
    }
}
