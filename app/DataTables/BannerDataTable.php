<?php

namespace App\DataTables;

use App\Helpers\DatatableHelpers;
use App\Models\Banner;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Html\Column;

class BannerDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);
        $dataTable->editColumn('store_id', function ($model) {
            if ($model->store == null) {
                return DatatableHelpers::dangerBadge("No Store");
            }else{
                return '<span class="badge badge-success">'.$model->store->name.'</span>';
            }
        });
        $dataTable->editColumn('image',function ($model){
            return DatatableHelpers::imageSection($model->image);
        });
        $dataTable->editColumn('service_category_id',function ($model){
            if ($model->serviceCategory == null) {
                return DatatableHelpers::dangerBadge("N/A");
            }else{
                return '<span class="badge badge-primary">'.$model->serviceCategory->name.'</span>';
            }
        });
        $dataTable->editColumn('active', function ($model) {
            return DatatableHelpers::statusColumn($model->active);
        });
        $dataTable->editColumn('url',function ($model){
            return DatatableHelpers::urlColumn($model->url);
        });
        $dataTable->addColumn('delete_m', static function ($model) {
            return '<input type="checkbox" class="delete" name="delete[]" value="'.$model->id.'"/>';
        });

        $dataTable->rawColumns(['image','active','store_id','service_category_id','action','url','delete_m']);
        return $dataTable->addColumn('action', 'admin.banners.datatables_actions');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Banner $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Banner $model)
    {
//        if(\Helpers::is_admin() || \Helpers::is_super_admin()){
//            return $model->newQuery()->with(['store','serviceCategory']);
//        }else{
//            $stores = \Helpers::get_user_stores()->pluck('id')->toArray();
//            return $model->newQuery()->with(['store','serviceCategory'])->whereIn('store_id',$stores);
//        }
        return $model->newQuery()->with(['store','serviceCategory']);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '120px', 'printable' => false, 'title' => __('lang.action')])
            ->parameters(array_merge(
                config('datatables-buttons.parameters'), [
                    'language' => json_decode(
                        file_get_contents(base_path('resources/lang/' . app()->getLocale() . '/datatable.json')
                        ), true)
                ]
            ));
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'delete_m' => new Column(['orderable' => false, 'searchable' => false, 'title' => '<input type="checkbox" name="deleteAll" id="deleteAll"/>', 'data' => 'delete_m']),
            'id' => new Column(['title' => __('lang.id'), 'data' => 'id','width' => '80px']),
            'type' => new Column(['title' => __('models/banners.fields.type'), 'data' => 'type','searchable' => true]),
            'store_id' => new Column(['title' => __('models/banners.fields.store_id'), 'data' => 'store_id','visible'=>false]),
            'image' => new Column(['title' => __('models/banners.fields.image'), 'data' => 'image','searchable' => true]),
            'title' => new Column(['title' => __('models/banners.fields.title'), 'data' => 'title','searchable' => true]),
            'service_category_id' => new Column(['title' => __('models/banners.fields.service_category_id'), 'data' => 'service_category_id','searchable' => true,'visible'=>false]),
            'url' => new Column(['title' => __('models/banners.fields.url'), 'data' => 'url','searchable' => true,'visible'=>false]),
            'active' => new Column(['title' => __('models/banners.fields.active'), 'data' => 'active','searchable' => false])
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'banners_datatable_' . time();
    }
}
