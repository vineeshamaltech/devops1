<?php

namespace App\DataTables;

use App\Helpers\DatatableHelpers;
use App\Models\Store;
use App\Models\StoreTiming;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Html\Column;

class StoreTimingDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);
        $dataTable->editColumn('service_category_id',function ($store){
            $store_timing = '';
            if(count($store->store_timings) == 0){
                return '<span class="label label-danger">Not Assigned</span>';
            }else{
                // $unsortedData = $store->store_timings;
                #default array
                // $daysOfWeek = array('Monday','Tuesday','Wednesday','Thursday','Friday','Saturday','Sunday');
                #create a new array with key association property
                // $daysAux = array();
                // foreach($unsortedData as $k=>$v) {
                //     $key = array_search($v->day, $daysOfWeek);
                //     if($key !== FALSE) {
                //         $daysAux[$key] = $v;
                //     }
                // }
                # array before sort
                // ksort($daysAux);
                // $unsortedData = $daysAux;

                // $store_timing = '';
                // foreach($unsortedData as $timing){
                //     $store_timing = $store_timing . '
                //     <tr>
                //     <td>'.$timing->day.'</td>
                //     <td>'.\Carbon\Carbon::parse($timing->open)->format('g:i a').'</td>
                //     <td>'.\Carbon\Carbon::parse($timing->close)->format('g:i a').'</td>
                //     </tr>
                //     ';
                // }
                foreach($store->store_timings as $timing){
                    $store_timing = $store_timing . '
                    <tr>
                    <td>'.$timing->day.'</td>
                    <td>'.\Carbon\Carbon::parse($timing->open)->format('g:i a').'</td>
                    <td>'.\Carbon\Carbon::parse($timing->close)->format('g:i a').'</td>
                    </tr>
                    ';
                }
                return 
                '<table>
                <thead>
                <th>Day</th>
                <th>Open</th>
                <th>Close</th>
                </thead><tbody>'.$store_timing.'</tbody></table>';
            }
        });
        $dataTable->rawColumns(['action','service_category_id']);
        return $dataTable->addColumn('action', 'admin.store_timings.datatables_actions');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\StoreTiming $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Store $model)
    {
        $stores = \Helpers::get_user_stores();
        return  $model->with('store_timings')->select('stores.*')->whereIn('id',$stores);


        // if (\Helpers::is_super_admin() || \Helpers::is_admin()){
        //    $result =  $model->with('vendor:id,name','serviceCategory')->select('stores.*')->newQuery();
        //       return $result;
        //     // return $model->newQuery()->with(['vendor','serviceCategory']);
        // }else{
        //     return $model->newQuery()->with(['vendor','serviceCategory'])->where('admin_id',\Auth::user()->id);
        // }
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '120px', 'printable' => false, 'title' => __('lang.action')])
            ->parameters(array_merge(
                config('datatables-buttons.parameters'), [
                    'language' => json_decode(
                        file_get_contents(base_path('resources/lang/' . app()->getLocale() . '/datatable.json')
                        ), true)
                ]
            ));
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'id' => new Column(['title' => __('models/stores.fields.store_id'), 'data' => 'id','width' => '30px']),
            'name' => new Column(['title' => __('models/stores.fields.name'), 'data' => 'name','width' => '300px','searchable' => true]),
            'service_cat_id' => new Column(['title' => __('models/stores.fields.store_timing'),'width' => '200px', 'data' => 'service_category_id']),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'store_timings_datatable_' . time();
    }
}
