<?php

namespace App\DataTables;

use App\Models\Ride;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Html\Column;

class RidesDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable->addColumn('action', 'admin.rides.datatables_actions');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Ride $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Ride $model)
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '120px', 'printable' => false, 'title' => __('lang.action')])
            ->parameters(array_merge(
                config('datatables-buttons.parameters'), [
                    'language' => json_decode(
                        file_get_contents(base_path('resources/lang/' . app()->getLocale() . '/datatable.json')
                        ), true)
                ]
            ));
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'id' => new Column(['title' => __('lang.id'), 'data' => 'id','width' => '80px']),
            'id' => new Column(['title' => __('models/rides.fields.id'), 'data' => 'id']),
            'user_id' => new Column(['title' => __('models/rides.fields.user_id'), 'data' => 'user_id']),
            'from_address' => new Column(['title' => __('models/rides.fields.from_address'), 'data' => 'from_address']),
            'to_address' => new Column(['title' => __('models/rides.fields.to_address'), 'data' => 'to_address']),
            'travel_datetime' => new Column(['title' => __('models/rides.fields.travel_datetime'), 'data' => 'travel_datetime']),
            'price' => new Column(['title' => __('models/rides.fields.price'), 'data' => 'price']),
            'status' => new Column(['title' => __('models/rides.fields.status'), 'data' => 'status'])
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'rides_datatable_' . time();
    }
}
