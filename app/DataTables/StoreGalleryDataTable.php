<?php

namespace App\DataTables;

use App\Models\StoreGallery;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Html\Column;

class StoreGalleryDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable->addColumn('action', 'admin.store_galleries.datatables_actions');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\StoreGallery $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(StoreGallery $model)
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '120px', 'printable' => false, 'title' => __('lang.action')])
            ->parameters(array_merge(
                config('datatables-buttons.parameters'), [
                    'language' => json_decode(
                        file_get_contents(base_path('resources/lang/' . app()->getLocale() . '/datatable.json')
                        ), true)
                ]
            ));
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'id' => new Column(['title' => __('lang.id'), 'data' => 'id','width' => '80px']),
            'store_id' => new Column(['title' => __('models/storeGalleries.fields.store_id'), 'data' => 'store_id']),
            'title' => new Column(['title' => __('models/storeGalleries.fields.title'), 'data' => 'title']),
            'description' => new Column(['title' => __('models/storeGalleries.fields.description'), 'data' => 'description']),
            'image' => new Column(['title' => __('models/storeGalleries.fields.image'), 'data' => 'image'])
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'store_galleries_datatable_' . time();
    }
}
