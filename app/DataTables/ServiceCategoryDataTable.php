<?php

namespace App\DataTables;

use App\Helpers\DatatableHelpers;
use App\Models\ServiceCategory;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Html\Column;

class ServiceCategoryDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);
        $dataTable->editColumn('icon', function ($serviceCategory) {
            return DatatableHelpers::imageSection($serviceCategory->icon);
        });
        $dataTable->editColumn('bg_image', function ($serviceCategory) {
            return DatatableHelpers::imageSection($serviceCategory->bg_image);
        });
        $dataTable->editColumn('active', function ($serviceCategory) {
            return DatatableHelpers::statusColumn($serviceCategory->active);
        });
        $dataTable->editColumn('service_type_id', function ($serviceCategory) {
            if($serviceCategory->service_type == null ){
                return '-';
            }else{
                return $serviceCategory->service_type->name;
            }
        });
        $dataTable->editColumn('driver_radius', function ($serviceCategory) {
            return $serviceCategory->driver_radius . ' km';
        });
        $dataTable->editColumn('min_price', function ($serviceCategory) {
            return $serviceCategory->min_price . 'RS';
        });
        $dataTable->editColumn('commission', function ($serviceCategory) {
            return $serviceCategory->commission . '%';
        });
        $dataTable->editColumn('discount', function ($serviceCategory) {
            return $serviceCategory->discount . '%';
        });
        $dataTable->rawColumns(['icon', 'bg_image', 'active','action']);

        return $dataTable->addColumn('action', 'admin.service_categories.datatables_actions');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\ServiceCategory $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(ServiceCategory $model)
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '120px', 'printable' => false, 'title' => __('lang.action')])
            ->parameters(array_merge(
                config('datatables-buttons.parameters'), [
                    'language' => json_decode(
                        file_get_contents(base_path('resources/lang/' . app()->getLocale() . '/datatable.json')
                        ), true)
                ]
            ));
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'id' => new Column(['title' => __('lang.id'), 'data' => 'id','width' => '30px','searchable' => true]),
            'icon' => new Column(['title' => __('models/serviceCategories.fields.icon'), 'data' => 'icon','sortable'=>false,'searchable' => false]),
            'bg_image' => new Column(['title' => __('models/serviceCategories.fields.bg_image'), 'data' => 'bg_image','sortable'=>false,'searchable' => false,'visible'=>false]),
            'name' => new Column(['title' => __('models/serviceCategories.fields.name'), 'data' => 'name','searchable' => true]),
            'description' => new Column(['title' => __('models/serviceCategories.fields.description'), 'data' => 'description','searchable' => false]),
            'service_type_id' => new Column(['title' => __('models/serviceCategories.fields.type'), 'data' => 'service_type_id','sortable'=>true,'searchable' => false]),
            'discount' => new Column(['title' => __('models/serviceCategories.fields.discount'), 'data' => 'discount','searchable' => false,'width' => '50px']),
            'commission' => new Column(['title' => __('models/serviceCategories.fields.commission'), 'data' => 'commission','searchable' => false,'width' => '50px']),
            'min_price' => new Column(['title' => __('models/serviceCategories.fields.min_price'), 'data' => 'min_price','searchable' => false,'visible'=>false]),
            'driver_radius' => new Column(['title' => __('models/serviceCategories.fields.driver_radius'), 'data' => 'driver_radius','searchable' => false,'visible'=>false]),
            'active' => new Column(['title' => __('models/serviceCategories.fields.active'), 'data' => 'active','searchable' => false])
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'service_categories_datatable_' . time();
    }
}
