{!! Form::open(['route' => ['admin.pincodes.destroy', $id], 'method' => 'delete']) !!}
<div class='btn-group'>
    @can('pincodes.view')
    <a href="{{ route('admin.pincodes.show', $id) }}" class='btn btn-default btn-xs'>
        <i class="fa fa-eye"></i>
    </a>
    @endcan
    @can('pincodes.edit')
    <a href="{{ route('admin.pincodes.edit', $id) }}" class='btn btn-default btn-xs'>
        <i class="fa fa-edit"></i>
    </a>
    @endcan
    @can('pincodes.delete')
    {!! Form::button('<i class="fa fa-trash"></i>', [
        'type' => 'submit',
        'class' => 'btn btn-danger btn-xs',
        'onclick' => 'return confirm("'.__('crud.are_you_sure').'")'
    ]) !!}
    @endcan
</div>
{!! Form::close() !!}
