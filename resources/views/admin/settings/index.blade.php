@extends('admin.layouts.app')

@section('title','Settings')

@section('content')
    <div class="content">
        @include('flash::message')
        <div class="clearfix"></div>

        @foreach($groups as $group)
            <div class="accordion" id="accordionExample">
                <div class="accordion_card">
                     <div class="card">
                         <div class="accordian_header">
                             <div class="card-header btn btn-block text-left collapsed" data-toggle="collapse" data-target="#{{$group->group_name ?? 'OTHERS'}}" aria-expanded="false" aria-controls="collapseThree">
                                 {{ucfirst($group->group_name ?? 'OTHERS')}}
{{--                                 <h2 class="mb-0">--}}
{{--                                     <button class="btn btn-block text-left collapsed" type="button" data-toggle="collapse" data-target="#{{$group->group_name}}" aria-expanded="false" aria-controls="collapseThree">--}}
{{--                                         {{ucfirst($group->group_name)}}--}}
{{--                                     </button>--}}
{{--                                 </h2>--}}
                             </div>
                         </div>
                         <div id="{{$group->group_name ?? 'OTHERS'}}" class="collapse accordian_body" aria-labelledby="headingThree" data-parent="#accordionExample">
                             <form action="{{route('admin.settings.update')}}" method="POST" enctype="multipart/form-data">
                                 @csrf
                                 <div class="card-body">
                                        <div class="row">
                                            @foreach($group->settings as $setting)
                                                @if($setting->html_type == 'text')
                                                    <div class="form-group col-md-6">
                                                        <div class="form-group">
                                                            <label for="{{ $setting->key_name }}">{{ Helpers::format_settings_title($setting->key_name) }}</label>
                                                            <input type="text" class="form-control" id="{{ $setting->key_name }}" name="{{ $setting->key_name }}" value="{{ $setting->key_value }}">
                                                        </div>
                                                    </div>
                                                @elseif($setting->html_type == 'textarea')
                                                    <div class="form-group col-md-6">
                                                        <div class="form-group">
                                                            <label for="{{ $setting->key_name }}">{{ Helpers::format_settings_title($setting->key_name) }}</label>
                                                            <textarea class="form-control summernote" name="{{ $setting->key_name }}" id="{{ $setting->key_name }}" rows="5">{{ $setting->key_value }}</textarea>
                                                        </div>
                                                    </div>
                                                @elseif($setting->html_type == 'checkbox')
                                                    <div class="form-group col-sm-3">
                                                        <div class="form-check">

                                                            <input type="hidden" name="{{ $setting->key_name }}" value="0" />
                                                            @if($setting->key_value == 1)
                                                                <input type="checkbox" name="{{ $setting->key_name }}" value="1" checked/>
                                                            @else
                                                                <input type="checkbox" name="{{ $setting->key_name }}" value="1" />
                                                            @endif

                                                            <label for="{{ $setting->key_name }}">{{ Helpers::format_settings_title($setting->key_name) }}</label>
                                                        </div>
                                                    </div>
                                                @elseif($setting->html_type == 'file')
                                                    <div class="form-group col-md-3">
                                                        <div class="form-group">
                                                            <label for="{{ $setting->key_name }}">{{ Helpers::format_settings_title($setting->key_name) }}</label>
                                                            <input type="file" class="form-control" name="{{ $setting->key_name }}" id="{{ $setting->key_name }}">
                                                        </div>
                                                    </div>
                                                @endif
                                            @endforeach
                                        </div>
                                    </div>
                                 <div class="card-footer">
                                        {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
                                        <a href="{{ route('admin.dashboard') }}" class="btn btn-default">
                                            @lang('lang.cancel')
                                        </a>
                                    </div>
                             </form>
                         </div>
                     </div>
                </div>
            </div>
        @endforeach

    </div>

@endsection

@push('third_party_stylesheets')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.20/summernote-bs4.min.css" integrity="sha512-ngQ4IGzHQ3s/Hh8kMyG4FC74wzitukRMIcTOoKT3EyzFZCILOPF0twiXOQn75eDINUfKBYmzYn2AA8DkAk8veQ==" crossorigin="anonymous" referrerpolicy="no-referrer" />
@endpush

@push('third_party_scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.20/summernote.min.js" integrity="sha512-6rE6Bx6fCBpRXG/FWpQmvguMWDLWMQjPycXMr35Zx/HRD9nwySZswkkLksgyQcvrpYMx0FELLJVBvWFtubZhDQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('.summernote').summernote();
        });
    </script>
@endpush
