@extends('admin.layouts.app')

@section('title',trans('lang.edit')." ".trans('models/vendors.singular'))

@section('content')
    <div class="content">

        @include('adminlte-templates::common.errors')

        <div class="card">

            {!! Form::model($vendor, ['route' => ['admin.vendors.update', $vendor->id], 'method' => 'patch']) !!}

            <div class="card-body">
                <div class="row">
                    @include('admin.vendors.fields')
                </div>
            </div>

            <div class="card-footer">
                {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
                <a href="{{ route('admin.vendors.index') }}" class="btn btn-default">
                    @lang('lang.cancel')
                 </a>
            </div>

            {!! Form::close() !!}

        </div>
    </div>
@endsection