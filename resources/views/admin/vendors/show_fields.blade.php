<!-- Name Field -->
<div class="col-sm-12">
    {!! Form::label('name', __('models/vendors.fields.name').':') !!}
    <p>{{ $vendor->name }}</p>
</div>

<!-- Mobile Field -->
<div class="col-sm-12">
    {!! Form::label('mobile', __('models/vendors.fields.mobile').':') !!}
    <p>{{ $vendor->mobile }}</p>
</div>

<!-- Email Field -->
<div class="col-sm-12">
    {!! Form::label('email', __('models/vendors.fields.email').':') !!}
    <p>{{ $vendor->email }}</p>
</div>

<!-- Active Field -->
<div class="col-sm-12">
    {!! Form::label('active', __('models/vendors.fields.active').':') !!}
    <p>{{ $vendor->active }}</p>
</div>

<!-- Verified Field -->
<div class="col-sm-12">
    {!! Form::label('verified', __('models/vendors.fields.verified').':') !!}
    <p>{{ $vendor->verified }}</p>
</div>

<!-- Created At Field -->
<div class="col-sm-12">
    {!! Form::label('created_at', __('models/vendors.fields.created_at').':') !!}
    <p>{{ $vendor->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="col-sm-12">
    {!! Form::label('updated_at', __('models/vendors.fields.updated_at').':') !!}
    <p>{{ $vendor->updated_at }}</p>
</div>

