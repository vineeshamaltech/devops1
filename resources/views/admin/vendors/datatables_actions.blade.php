{!! Form::open(['route' => ['admin.vendors.destroy', $id], 'method' => 'delete']) !!}
<div class='btn-group'>
    @can('vendors.view')
    <a href="{{ route('admin.vendors.show', $id) }}" class='btn btn-default btn-xs'>
        <i class="fa fa-eye"></i>
    </a>
    @endcan
    @can('vendors.edit')
    <a href="{{ route('admin.vendors.edit', $id) }}" class='btn btn-default btn-xs'>
        <i class="fa fa-edit"></i>
    </a>
    @endcan
    @can('vendors.delete')
    {!! Form::button('<i class="fa fa-trash"></i>', [
        'type' => 'submit',
        'class' => 'btn btn-danger btn-xs',
        'onclick' => 'return confirm("'.__('crud.are_you_sure').'")'
    ]) !!}
    @endcan
</div>
{!! Form::close() !!}
