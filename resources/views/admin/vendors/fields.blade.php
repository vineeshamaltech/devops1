<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', __('models/vendors.fields.name').':',['class'=>'required']) !!}
    {!! Form::text('name', null, ['class' => 'form-control','required'=>'true']) !!}
</div>

<!-- Mobile Field -->
<div class="form-group col-sm-6">
    {!! Form::label('mobile', __('models/vendors.fields.mobile').':',['class'=>'required']) !!}
    {!! Form::text('mobile', null, ['class' => 'form-control','required'=>'true']) !!}
</div>

<!-- Email Field -->
<div class="form-group col-sm-6">
    {!! Form::label('email', __('models/vendors.fields.email').':',['class'=>'required']) !!}
    {!! Form::text('email', null, ['class' => 'form-control','required'=>'true']) !!}
</div>

<!-- Password Field -->
<div class="form-group col-sm-6">
    {!! Form::label('password', __('models/vendors.fields.password').':') !!}
    {!! Form::password('password', ['class' => 'form-control']) !!}
</div>

<!-- Active Field -->
<div class="form-group col-sm-6">
    <div class="form-check">
        {!! Form::hidden('active', 0, ['class' => 'form-check-input']) !!}
        {!! Form::checkbox('active', '1', null, ['class' => 'form-check-input']) !!}
        {!! Form::label('active', __('models/vendors.fields.active'), ['class' => 'form-check-label']) !!}
    </div>
</div>


<!-- Verified Field -->
<div class="form-group col-sm-6">
    <div class="form-check">
        {!! Form::hidden('verified', 0, ['class' => 'form-check-input']) !!}
        {!! Form::checkbox('verified', '1', null, ['class' => 'form-check-input']) !!}
        {!! Form::label('verified', __('models/vendors.fields.verified'), ['class' => 'form-check-label']) !!}
    </div>
</div>
