{!! Form::open(['route' => ['admin.vehicleTypes.destroy', $id], 'method' => 'delete']) !!}
<div class='btn-group'>
    @can('vehicleTypes.view')
    <a href="{{ route('admin.vehicleTypes.show', $id) }}" class='btn btn-default btn-xs'>
        <i class="fa fa-eye"></i>
    </a>
    @endcan
    @can('vehicleTypes.edit')
    <a href="{{ route('admin.vehicleTypes.edit', $id) }}" class='btn btn-default btn-xs'>
        <i class="fa fa-edit"></i>
    </a>
    @endcan
    @can('vehicleTypes.delete')
    {!! Form::button('<i class="fa fa-trash"></i>', [
        'type' => 'submit',
        'class' => 'btn btn-danger btn-xs',
        'onclick' => 'return confirm("'.__('crud.are_you_sure').'")'
    ]) !!}
    @endcan
</div>
{!! Form::close() !!}
