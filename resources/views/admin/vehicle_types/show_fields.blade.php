<!-- Name Field -->
<div class="col-sm-12">
    {!! Form::label('name', __('models/vehicleTypes.fields.name').':') !!}
    <p>{{ $vehicleType->name }}</p>
</div>

<!-- Icon Field -->
<div class="col-sm-12">
    {!! Form::label('icon', __('models/vehicleTypes.fields.icon').':') !!}
    <p>{{ $vehicleType->icon }}</p>
</div>

<!-- Active Field -->
<div class="col-sm-12">
    {!! Form::label('active', __('models/vehicleTypes.fields.active').':') !!}
    <p>{{ $vehicleType->active }}</p>
</div>

<!-- Created At Field -->
<div class="col-sm-12">
    {!! Form::label('created_at', __('models/vehicleTypes.fields.created_at').':') !!}
    <p>{{ $vehicleType->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="col-sm-12">
    {!! Form::label('updated_at', __('models/vehicleTypes.fields.updated_at').':') !!}
    <p>{{ $vehicleType->updated_at }}</p>
</div>

