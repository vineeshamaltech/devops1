<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', __('models/vehicleTypes.fields.name').':',['class'=>'required']) !!}
    {!! Form::text('name', null, ['class' => 'form-control','required'=>'true']) !!}
</div>

<!-- Base Price Field -->
<div class="form-group col-sm-6 pick_drop_felds">
    {!! Form::label('max_passengers', __('models/vehicleTypes.fields.max_passengers').':') !!}
    {!! Form::number('max_passengers', null, ['class' => 'form-control']) !!}
</div>

<!-- Icon Field -->
<div class="form-group col-sm-6">
    {!! Form::label('icon', __('models/vehicleTypes.fields.icon').':') !!}
    <div class="input-group">
        <div class="custom-file">
            {!! Form::file('icon', ['class' => 'custom-file-input']) !!}
            {!! Form::label('icon', 'Choose file', ['class' => 'custom-file-label']) !!}
        </div>
    </div>
</div>
<div class="clearfix"></div>

<!-- Icon Field -->
<div class="form-group col-sm-6">
    {!! Form::label('marker_icon', __('models/vehicleTypes.fields.marker_icon').':') !!}
    <div class="input-group">
        <div class="custom-file">
            {!! Form::file('marker_icon', ['class' => 'custom-file-input']) !!}
            {!! Form::label('marker_icon', 'Choose file', ['class' => 'custom-file-label']) !!}
        </div>
    </div>
</div>
<div class="clearfix"></div>


<!-- Base Distance Field -->


<!-- Base Price Field -->
<div class="form-group col-sm-6 pick_drop_felds">
    {!! Form::label('base_price', __('models/serviceCategories.fields.base_price').':') !!}
    {!! Form::number('base_price', null, ['class' => 'form-control']) !!}
</div>

<!-- Base Km Field -->
<div class="form-group col-sm-6 pick_drop_felds">
    {!! Form::label('base_km', __('models/serviceCategories.fields.base_km').':') !!}
    {!! Form::number('base_km', null, ['class' => 'form-control']) !!}
</div>

<!-- Price Km Field -->
<div class="form-group col-sm-6 pick_drop_felds">
    {!! Form::label('price_km', __('models/serviceCategories.fields.price_km').':') !!}
    {!! Form::number('price_km', null, ['class' => 'form-control']) !!}
</div>



<!-- Active Field -->
<div class="form-group col-sm-6">
    <div class="form-check">
        {!! Form::hidden('active', 0, ['class' => 'form-check-input']) !!}
        {!! Form::checkbox('active', '1', null, ['class' => 'form-check-input']) !!}
        {!! Form::label('active', __('models/vehicleTypes.fields.active'), ['class' => 'form-check-label']) !!}
    </div>
</div>
