@extends('admin.layouts.app')

@section('title',trans('lang.edit')." ".trans('models/vehicleTypes.singular'))

@section('content')
    <div class="content">

        @include('adminlte-templates::common.errors')

        <div class="card">

            {!! Form::model($vehicleType, ['route' => ['admin.vehicleTypes.update', $vehicleType->id], 'method' => 'patch', 'files' => true]) !!}

            <div class="card-body">
                <div class="row">
                    @include('admin.vehicle_types.fields')
                </div>
            </div>

            <div class="card-footer">
                {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
                <a href="{{ route('admin.vehicleTypes.index') }}" class="btn btn-default">
                    @lang('lang.cancel')
                 </a>
            </div>

            {!! Form::close() !!}

        </div>
    </div>
@endsection