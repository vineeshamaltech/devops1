@extends('admin.layouts.app')

@section('title',trans('lang.add_new')." ".trans('models/products.singular'))

@section('content')
    <div class="content">

        <!--@include('adminlte-templates::common.errors')-->

        <div class="card">

            {!! Form::open(['route' => 'admin.products.store', 'files' => true]) !!}

            <div class="card-body">
                <div class="row">
                    @include('admin.products.fields')
                </div>
            </div>

            <div class="card-footer">
                {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
                <a href="{{ route('admin.products.index') }}" class="btn btn-default">
                 @lang('lang.cancel')
                </a>
            </div>

            {!! Form::close() !!}

        </div>
    </div>
@endsection
