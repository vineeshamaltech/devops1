<!-- Store Id Field -->
<div class="col-sm-12">
    {!! Form::label('store_id', __('models/products.fields.store_id').':') !!}
    <p>{{ $product->store->name }}</p>
</div>

<!-- Product Type Field -->
<div class="col-sm-12">
    {!! Form::label('product_type', __('models/products.fields.product_type').':') !!}
    <p>{{ $product->product_type }}</p>
</div>

<!-- Name Field -->
<div class="col-sm-12">
    {!! Form::label('name', __('models/products.fields.name').':') !!}
    <p>{{ $product->name }}</p>
</div>

<!-- Description Field -->
<div class="col-sm-12">
    {!! Form::label('description', __('models/products.fields.description').':') !!}
    <p>{{ $product->description }}</p>
</div>

<!-- Image Field -->
<div class="col-sm-12">
    {!! Form::label('image', __('models/products.fields.image').':') !!}
    <p>{{ $product->image }}</p>
</div>

<!-- Unit Field -->
<div class="col-sm-12">
    {!! Form::label('unit', __('models/products.fields.unit').':') !!}
    <p>{{ $product->unit }}</p>
</div>

<!-- Price Field -->
<div class="col-sm-12">
    {!! Form::label('price', __('models/products.fields.price').':') !!}
    <p>{{ $product->price }}</p>
</div>

<!-- Discount Price Field -->
<div class="col-sm-12">
    {!! Form::label('discount_price', __('models/products.fields.discount_price').':') !!}
    <p>{{ $product->discount_price }}</p>
</div>

<!-- Store Category Id Field -->
<div class="col-sm-12">
    {!! Form::label('category_id', __('models/products.fields.category_id').':') !!}
    <p>{{ $product->categories->name }}</p>
</div>

<!-- Sku Field -->
<div class="col-sm-12">
    {!! Form::label('sku', __('models/products.fields.sku').':') !!}
    <p>{{ $product->sku }}</p>
</div>

<!-- Product Tags Field -->
<div class="col-sm-12">
    {!! Form::label('product_tags', __('models/products.fields.product_tags').':') !!}
    <p>{{ $product->product_tags }}</p>
</div>

<!-- Active Field -->
<div class="col-sm-12">
    {!! Form::label('active', __('models/products.fields.active').':') !!}
    <p>{{ $product->active }}</p>
</div>

<!-- Created At Field -->
<div class="col-sm-12">
    {!! Form::label('created_at', __('models/products.fields.created_at').':') !!}
    <p>{{ $product->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="col-sm-12">
    {!! Form::label('updated_at', __('models/products.fields.updated_at').':') !!}
    <p>{{ $product->updated_at }}</p>
</div>

