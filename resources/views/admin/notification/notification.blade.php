@extends('admin.layouts.app')

@section('title',"Send Notification")

@section('content')
    <div class="content">
        @include('flash::message')
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    {!! Form::open(['route' => 'admin.send_push.store', 'files' => true]) !!}
                    <div class="card-header">
                        <h3 class="card-title">
                            <i class="fas fa-file-import"></i>
                           Notification
                        </h3>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="form-group col-sm-3">
                                <label for="report_of">User Type</label>
                                <select name="user_type" id="report_of" class="form-control select2">
                                    <option value="all">All</option>
                                    <option value="vendor">Vendor</option>
                                    <option value="driver">Driver</option>
                                    <option value="user">Customer</option>
                                </select>
                            </div>
                           
                           

                        </div>
                      <div class="form-group col-sm-6">
                                <label for="reportable_id">Select User</label>
                                <select name="user[]" id="reportable_id" multiple class="form-control select2">
                                   
                                </select>
                            </div>
                       
                       
                        <div class="field mt-2" >
                            <label for="notify_title" class="label">Title</label>
                            <div class="control">
                                <input  class="input col-md-6 form-control "  autocomplete="off"  type="text" value="{{ old('title') }}" name="title" required id="title" placeholder="Enter title">
                            </div>
                        </div>
                        

                        <div class="form-group mt-2 ">
                            {!! Form::label('image', __('models/banners.fields.image').':') !!}
                            <div class="input-group ">
                                <div class="custom-file col-sm-6">
                                    {!! Form::file('image', ['class' => 'custom-file-input']) !!}
                                    {!! Form::label('image', 'Choose file', ['class' => 'custom-file-label']) !!}
                                </div>
                            </div>
                        </div>

                        
                        {{-- <div class="field mt-2">
                            <label for="notify_title" class="label">Image</label>
                            <div class="control">
                                <input  class="input col-md-6 form-control" autocomplete="off"  type="file" value="{{ old('image') }}" name="image" required id="image">
                            </div>
                        </div> --}}
        
        
                        <div class="field mt-2">
                            <label for="notify_desc" class="label">Description</label>
                            <div class="control">
                                <input  class="input col-md-6 form-control" autocomplete="off"  type="text" value="{{ old('description') }}" name="description" required id="description" placeholder="@lang('Description')">
                            </div>
                        </div>





                    </div>



                    <div class="card-footer">
                        {!! Form::submit('Send', ['class' => 'btn btn-primary']) !!}

                    </div>
                    {!! Form::close() !!}
                </div>
            </div>

            @if(!empty($data_title))
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">
                            <i class="fas fa-file-import"></i>
                            {{$data_title}}
                        </h3>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="info-box">
                                    <span class="info-box-icon bg-primary"><i class="fa fa-globe-asia"></i></span>

                                    <div class="info-box-content">
                                        <span class="info-box-text">Total Orders</span>
                                        <span class="info-box-number">{{$total_orders}}</span>
                                    </div>
                                    <!-- /.info-box-content -->
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="info-box">
                                    <span class="info-box-icon bg-warning"><i class="fa fa-rupee-sign"></i></span>

                                    <div class="info-box-content">
                                        <span class="info-box-text">Total Order Amount</span>
                                        <span class="info-box-number">{{$total_amount}}</span>
                                    </div>
                                    <!-- /.info-box-content -->
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="info-box">
                                    <span class="info-box-icon bg-success"><i class="far fa-money-bill-alt"></i></span>

                                    <div class="info-box-content">
                                        <span class="info-box-text">Total Earnings</span>
                                        <span class="info-box-number">{{$earnings}}</span>
                                    </div>
                                    <!-- /.info-box-content -->
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="info-box">
                                    <span class="info-box-icon bg-danger"><i class="far fa-minus-square"></i></span>

                                    <div class="info-box-content">
                                        <span class="info-box-text">Total Tax</span>
                                        <span class="info-box-number">{{$total_tax}}</span>
                                    </div>
                                    <!-- /.info-box-content -->
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <canvas id="myChart" width="400" height="100"></canvas>
                            </div>
                        </div>

                    </div>
                </div>

            </div>
            @endif
        </div>

    </div>
@endsection

@push('third_party_stylesheets')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-daterangepicker/3.0.5/daterangepicker.css" integrity="sha512-gp+RQIipEa1X7Sq1vYXnuOW96C4704yI1n0YB9T/KqdvqaEgL6nAuTSrKufUX3VBONq/TPuKiXGLVgBKicZ0KA==" crossorigin="anonymous" referrerpolicy="no-referrer" />
@endpush

@push('third_party_scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/3.6.2/chart.min.js" integrity="sha512-tMabqarPtykgDtdtSqCL3uLVM0gS1ZkUAVhRFu1vSEFgvB73niFQWJuvviDyBGBH22Lcau4rHB5p2K2T0Xvr6Q==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-daterangepicker/3.0.5/daterangepicker.min.js" integrity="sha512-mh+AjlD3nxImTUGisMpHXW03gE6F4WdQyvuFRkjecwuWLwD2yCijw4tKA3NsEFpA1C3neiKhGXPSIGSfCYPMlQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script type="text/javascript">
    // $('#daterange-btn').daterangepicker(
    //     {
    //         ranges   : {
    //             'Today'       : [moment(), moment()],
    //             'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
    //             'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
    //             'Last 30 Days': [moment().subtract(29, 'days'), moment()],
    //             'This Month'  : [moment().startOf('month'), moment().endOf('month')],
    //             'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
    //         },
    //         startDate: moment().subtract(29, 'days'),
    //         endDate  : moment()
    //     },
    //     function (start, end) {
    //         $('#reportrange').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
    //         $('#daterangevalue').val(start.format('MMMM D, YYYY') +'-' + end.format('MMMM D, YYYY'))
    //     }
    // );
    // $(document).ready(function (){
        $('#report_of').on('change',function () {
            $('#reportable_id').html('');
            var data = $(this).val();
            var url = "";
            if(data === 'vendor'){
                url = '/api/admins?filter=id;name&search=type:Vendor&searchFields=name:like';
            }else if(data === 'driver'){
                url = '/api/drivers?filter=id;name;mobile&searchFields=name:like';
            } else if(data === "user"){
                url = '/api/users?filter=id;name;mobile&searchFields=name:like';
            }
            else if(data === ""){
                url = "";
            }
            getData(url);
        }).change();
    // });

    function getData(url) {
        console.log("Loading "+url);
        if(url == ""){
            $('#reportable_id').select2({
                theme: 'classic',
                data:[{
                    id:"all",
                    text:"All"
                }]
            });
        }else{
            var def_data = {
                id: "",
                text: 'All'
            };
            var newOption = new Option(def_data.text, def_data.id, false, false);
                          

            $('#reportable_id').select2({
                theme: 'classic',
                ajax: {
                    url: url,
                    dataType: 'json',
                    data: function (params) {
                        if(params.term !== undefined){
                            return {
                                search: 'name:' + params.term + ';type:Vendor',
                            };
                        }else{
                            return {};
                        }

                        // Query parameters will be ?search=[term]&type=public

                    },


                    processResults: function (data) {
                        // Transforms the top-level key of the response object from 'items' to 'results'
                        if(data.data.length > 0) {
                            
                            return {
                                results: data.data.map(function (item) {
                                    var opt =""
                                    if(item.mobile){
                                        opt = "("+ item.mobile +")"
                                    }
                                    return {
                                        id: item.id,
                                        text: item.name+ opt ,
                                    }
                                })
                            };
                        }
                    }
                }
            });
             $('#reportable_id').append('<option value="all" selected="selected">All</option>');
            $('#reportable_id').trigger('change');
            // $('#reportable_id').append(newOption);
        }
        
    }
</script>
    @if(!empty($chart_data))
    <script>
        const ctx = document.getElementById('myChart');
        var labels = [];
        var data = [];
        @foreach($chart_data as $key => $value)
            labels.push('{{$key}}');
            data.push('{{$value}}');
        @endforeach
        const myChart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: labels,
                datasets: [{
                    label: '# of Orders',
                    data: data,
                    backgroundColor: [
                        'rgba(255, 99, 132, 0.2)',
                        'rgba(54, 162, 235, 0.2)',
                        'rgba(255, 206, 86, 0.2)',
                        'rgba(75, 192, 192, 0.2)',
                        'rgba(153, 102, 255, 0.2)',
                        'rgba(255, 159, 64, 0.2)'
                    ],
                    borderColor: [
                        'rgba(255, 99, 132, 1)',
                        'rgba(54, 162, 235, 1)',
                        'rgba(255, 206, 86, 1)',
                        'rgba(75, 192, 192, 1)',
                        'rgba(153, 102, 255, 1)',
                        'rgba(255, 159, 64, 1)'
                    ],
                    borderWidth: 1
                }]
            },
            options: {
                scales: {
                    y: {
                        beginAtZero: true
                    }
                }
            }
        });

    </script>
    @endif
@endpush
