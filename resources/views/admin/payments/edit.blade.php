@extends('admin.layouts.app')

@section('title',trans('lang.edit')." ".trans('models/payments.singular'))

@section('content')
    <div class="content">

        @include('adminlte-templates::common.errors')

        <div class="card">

            {!! Form::model($payment, ['route' => ['admin.payments.update', $payment->id], 'method' => 'patch']) !!}

            <div class="card-body">
                <div class="row">
                    @include('admin.payments.fields')
                </div>
            </div>

            <div class="card-footer">
                {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
                <a href="{{ route('admin.payments.index') }}" class="btn btn-default">
                    @lang('lang.cancel')
                 </a>
            </div>

            {!! Form::close() !!}

        </div>
    </div>
@endsection