<!-- Ref No Field -->
<div class="form-group col-sm-6">
    {!! Form::label('ref_no', __('models/payments.fields.ref_no').':') !!}
    {!! Form::text('ref_no', null, ['class' => 'form-control']) !!}
</div>

<!-- Payment Method Field -->
<div class="form-group col-sm-6">
    {!! Form::label('payment_method', __('models/payments.fields.payment_method').':') !!}
    {!! Form::text('payment_method', null, ['class' => 'form-control']) !!}
</div>


<!-- Amount Field -->
<div class="form-group col-sm-6">
    {!! Form::label('amount', __('models/payments.fields.amount').':') !!}
    {!! Form::number('amount', null, ['class' => 'form-control']) !!}
</div>

<!-- Gateway Response Field -->
<div class="form-group col-sm-6">
    {!! Form::label('gateway_response', __('models/payments.fields.gateway_response').':') !!}
    {!! Form::text('gateway_response', null, ['class' => 'form-control']) !!}
</div>
