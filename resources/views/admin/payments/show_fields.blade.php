<!-- Ref No Field -->
<div class="col-sm-12">
    {!! Form::label('ref_no', __('models/payments.fields.ref_no').':') !!}
    <p>{{ $payment->ref_no }}</p>
</div>

<!-- Payment Method Field -->
<div class="col-sm-12">
    {!! Form::label('payment_method', __('models/payments.fields.payment_method').':') !!}
    <p>{{ $payment->payment_method }}</p>
</div>

<!-- Amount Field -->
<div class="col-sm-12">
    {!! Form::label('amount', __('models/payments.fields.amount').':') !!}
    <p>{{ $payment->amount/100 }}</p>
</div>

<!-- Gateway Response Field -->
<div class="col-sm-12">
    {!! Form::label('gateway_response', __('models/payments.fields.gateway_response').':') !!}
    <p>{{ $payment->gateway_response }}</p>
</div>

<!-- Created At Field -->
<div class="col-sm-12">
    {!! Form::label('created_at', __('models/payments.fields.created_at').':') !!}
    <p>{{ $payment->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="col-sm-12">
    {!! Form::label('updated_at', __('models/payments.fields.updated_at').':') !!}
    <p>{{ $payment->updated_at }}</p>
</div>

