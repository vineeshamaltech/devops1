{!! Form::open(['route' => ['admin.orders.destroy', $id], 'method' => 'delete']) !!}
<div class='btn-group'>
    @can('orders.view')
    <a href="{{ route('admin.orders.show', $id) }}" class='btn btn-default btn-xs'>
        <i class="fa fa-eye"></i>
    </a>
    @endcan
    @can('orders.edit')
    <a href="{{ route('admin.orders.edit', $id) }}" class='btn btn-default btn-xs'>
        <i class="fa fa-edit"></i>
    </a>
    @endcan
   @can('orders.delete')
 {!! Form::button('<i class="fa fa-trash"></i>', [
      'type' => 'submit',
  'class' => 'btn btn-danger btn-xs',
   'onclick' => 'return confirm("'.__('crud.are_you_sure').'")'
    ]) !!}
  @endcan
</div>
{!! Form::close() !!}
