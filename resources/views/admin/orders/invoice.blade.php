<div class="invoice p-2 mb-3 printarea" id="printarea">
    <!-- title row -->
    <div class="row">
        <div class="col-12">
            <h4>
          
                <img src="{{env('APP_LOGO')}}" width="50" height="50"> {{env('APP_NAME')}}
                <small class="float-right">Date: {{\Carbon\Carbon::parse($order->created_at)->format('d/m/Y H:i:s')}}</small>
            </h4>
        </div>
        <!-- /.col -->
    </div>
    <!-- info row -->
    <div class="row invoice-info">
        <div class="col-sm-4 invoice-col">
            Ordered from:
            <address>
                <strong>{{$order->pickup_address['name'] ?? ""}}</strong><br>
                {{$order->pickup_address['address'] ?? ""}}<br>
                Mobile: {{$order->pickup_address['mobile'] ?? ""}}<br>
            </address>
        </div>
        <!-- /.col -->
        <div class="col-sm-4 invoice-col">
            Delivery To:
            <address>
                <strong>{{$order->delivery_address['name'] ?? ""}}</strong><br>
                {{$order->delivery_address['address']}}<br>
                Mobile: {{$order->delivery_address['phone'] ?? ""}}<br>
            </address>
        </div>
        <!-- /.col -->
        <div class="col-sm-4 invoice-col">
            <b>Order ID:</b> # {{$order->id}}<br>
            <b>Payment Status:</b> {{$order->payment_status}}<br>
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->

    <!-- Table row -->
    <div class="row">
        <div class="col-12 table-responsive">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th>S. No</th>
                    <th>Product</th>
                    <th>Product Variant Name</th>
                    <th>Product Description</th>
                    <th>Quantity</th>
                    <th>Price</th>
                    <th>Subtotal</th>
                </tr>
                </thead>
                <tbody>
                @foreach($order->items as $key => $orderedProduct)
                    <tr>
                        <td>{{$key + 1}}</td>
                        <td>{{$orderedProduct->product_name}}</td>
                       <td>{{$orderedProduct->variant_names}}</td>
                       <td>{{$orderedProduct->product_description}}</td>
                        <td>{{$orderedProduct->quantity}}</td>
                        <td>{{setting('currency_symbol','RS')}} {{$orderedProduct->discount_price/  $orderedProduct->quantity}}</td>
                        <td>{{setting('currency_symbol','RS')}} {{$orderedProduct->discount_price }}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->

    <div class="row">
        <!-- accepted payments column -->
        <div class="col-6">
            <p class="lead">Payment Method: <strong>{{strtoupper($order->payment_method)}}</strong></p>
            <p class="text-muted well well-sm shadow-none" style="margin-top: 10px;">
            </p>
        </div>
        <!-- /.col -->
        <div class="col-6">
            <div class="table-responsive">
                <table class="table">
                    <tbody><tr>
                        <th style="width:50%">Subtotal:</th>
                        <td>{{setting('currency_symbol','RS')}} {{$order->total_price}}</td>
                    </tr>
                    
                      <tr>
                        <th>Driver Tip: </th>
                        <td>{{setting('currency_symbol','RS')}}{{$order->tip}}</td>
                    </tr>
                    <tr>
                        <th>Tax:</th>
                        <td>{{setting('currency_symbol','RS')}} {{$order->tax}}</td>
                    </tr>
                    <tr>
                        <th>Delivery Fee ({{ $order->distance_travelled  }}KM):</th>
                        <td>{{setting('currency_symbol','RS')}} {{$order->delivery_fee}}</td>
                    </tr>
                    <tr>
                        <th>Total:</th>
                        <td>{{setting('currency_symbol','RS')}} {{$order->grand_total}}</td>
                    </tr>
                    </tbody></table>
            </div>
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
<div id="editor"></div>
    <!-- this row will not appear when printing -->
    <div class="row no-print" id="no-print">
        <div class="col-12">
             <button onclick="downloadAsPdf()" rel="noopener" type="button" class="btn btn-primary float-right" style="margin-right: 5px;">
                <i class="fas fa-file"></i> Download Pdf
            </button>
            <button onclick="printDiv()" rel="noopener" type="button" class="btn btn-primary float-right" style="margin-right: 5px;">
                <i class="fas fa-print"></i> Print
            </button>
        </div>
    </div>
     <!-- <button onclick="printDiv()">print</button> -->
</div>


@push('third_party_scripts')
<script>
    // download as pdf printarea div
    function downloadAsPdf() {
        var pdf = new jsPDF('p', 'pt', 'a4');
       
        pdf.addHTML($('#printarea')[0],-64,20, function () {
            pdf.save('Order_invoice.pdf');
        });

    }
  
        function printDiv() {
          
            var divContents = document.getElementById("printarea").innerHTML;
            console.log(divContents)
            var w = window.open();
            w.document.write("<link rel=\"stylesheet\" href=\"{{ asset('assets/css/adminlte.min.css') }}\" type=\"text/css\"/>");
            w.document.write('</head><body onload="window.print();window.close()">');
            w.document.write(divContents);
            w.document.write('</body></html>');
            w.document.close();
        }
    </script>
   
@endpush
