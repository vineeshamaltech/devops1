@extends('admin.layouts.app')
@push('third_party_stylesheets')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-daterangepicker/3.0.5/daterangepicker.css" integrity="sha512-gp+RQIipEa1X7Sq1vYXnuOW96C4704yI1n0YB9T/KqdvqaEgL6nAuTSrKufUX3VBONq/TPuKiXGLVgBKicZ0KA==" crossorigin="anonymous" referrerpolicy="no-referrer" />
@endpush
@section('title','Orders')
@section('content')
    <div class="content">

        @include('flash::message')

        <div class="clearfix"></div>

        <div class="card">
            <div class="card-header">
                <ul class="nav nav-tabs align-items-end card-header-tabs w-100">
                    <li class="nav-item">
                        <a class="nav-link active" href="{!! url()->current() !!}"><i class="fa fa-list mr-2"></i>{{trans('models/orders.plural')}}</a>
                    </li>
{{--                    @can('orders.create')--}}
{{--                    <li class="nav-item">--}}
{{--                        <a class="nav-link" href="{!! route('admin.orders.create') !!}"><i class="fa fa-plus mr-2"></i>{{trans('lang.add')." ".trans('models/orders.singular')}}</a>--}}
{{--                    </li>--}}
{{--                    @endcan--}}

                        @include('admin.layouts.datatables_toolbar', compact('dataTable'))
                </ul>
            </div>

            <div class="card-body">
                <div class="row">
                    <div class="col-lg-4">
                        @can('orders.filter')
                            <form action="{{route('admin.orders.index')}}" method="GET">
                            @csrf
                            <label>Filter By Zone : </label>
                            <select name="zone" id="zone" class="select2">
                                <option value="0">All</option>
                                @foreach($zones as $zone)
                                <option value="{{$zone->id}}">{{$zone->title}}</option>
                                @endforeach
                            </select>
                            <button type="submit" class="btn sm btn-primary">Search</button>
                            </form>
                        @endcan
                    </div>
                    <div class="col-lg-8">
                        <form action="{{route('admin.orders.index')}}" method="GET">
                            @csrf
                            <div class="row">
                                <div class="col-lg-3"><label>Date range button:</label></div>
                                <div class="col-lg-3">
                                    <small id="reportrange"></small>
                                </div>
                                <div class="col-lg-4">
                                    <div class="input-group col-sm-12">
                                        <button type="button" class="btn btn-default col-sm-12" id="daterange-btn">
                                            <i class="far fa-calendar-alt"></i> Date range picker
                                            <i class="fas fa-caret-down"></i>
                                        </button>
                                    </div>
                                    <input id="daterangevalue" name="daterange" type="hidden" value="">
                                </div>
                                <div class="col-lg-1">
                                <button type="submit" class="btn sm btn-primary">Go</button>
                                </div>
                            </div>
                        </form>        
                    </div>
                </div>
                <br>
                @include('admin.orders.table')
                <div class="card-footer clearfix float-right">
                    <div class="float-right">

                    </div>
                </div>
            </div>
        </div>
    </div>

{{--    <div class="modal fade" id="statusModal" tabindex="-1" role="dialog" aria-labelledby="statusModalLabel" aria-hidden="true">--}}
{{--        <div class="modal-dialog" role="document">--}}
{{--            <div class="modal-content">--}}
{{--                <div class="modal-header">--}}
{{--                    <h5 class="modal-title" id="statusModalLabel">Change Status</h5>--}}
{{--                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">--}}
{{--                        <span aria-hidden="true">&times;</span>--}}
{{--                    </button>--}}
{{--                </div>--}}
{{--                <form method="POST" action="{{route('admin.changeStatus')}}">--}}
{{--                    @csrf--}}
{{--                    <input id="order_id" name="orderid" type="hidden" />--}}
{{--                    <div class="modal-body">--}}
{{--                        <select name="status" class="col-sm-12 select2" style="width:100%">--}}
{{--                            <option value="RECEIVED">RECEIVED</option>--}}
{{--                            <option value="ASSIGNED">ASSIGNED</option>--}}
{{--                            <option value="OUT_FOR_DELIVERY">OUT_FOR_DELIVERY</option>--}}
{{--                            <option value="DELIVERED">DELIVERED</option>--}}
{{--                            <option value="USER_CANCELLED">USER_CANCELLED</option>--}}
{{--                        </select>--}}
{{--                    </div>--}}
{{--                    <div class="modal-footer">--}}
{{--                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>--}}
{{--                        <input type="submit" class="btn btn-primary" value="Change Status">--}}
{{--                    </div>--}}
{{--                </form>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}

    <div class="modal fade" id="driverModal" tabindex="-1" role="dialog" aria-labelledby="driverModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="driverModalLabel">Assign Driver</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form method="POST" action="{{route('admin.assignDriver')}}">
                    @csrf
                    <input id="order_id" name="orderid" type="hidden" />
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-sm-12">
                                <select name="driver_id" id="driver_id"  class="form-control select2" style="width:100%">
                                    <option value="">Select Driver</option>
                                     @foreach($drivers as $driver)
                <option value="{{ $driver->id }}" @if($driver->disabled) disabled @endif @if(isset($order) && $order->driver_id == $driver->id) selected @endif>{{ $driver->name }}</option>
                                     @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <input type="submit" class="btn btn-primary" value="Assign">
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="pingModal" tabindex="-1" role="dialog" aria-labelledby="pingModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="pingModalLabel">Ping Driver</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form method="POST" action="{{route('admin.pingDriver')}}">
                    @csrf
                    <input id="d_order_id" name="oorderid" type="hidden" />
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-sm-12">
                                <h1>Are you sure you want to alert the driver again!</h1>  
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <input type="submit" class="btn btn-primary" value="Ping">
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection

@push('third_party_scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-daterangepicker/3.0.5/daterangepicker.min.js" integrity="sha512-mh+AjlD3nxImTUGisMpHXW03gE6F4WdQyvuFRkjecwuWLwD2yCijw4tKA3NsEFpA1C3neiKhGXPSIGSfCYPMlQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script type="text/javascript">
    var start = moment().subtract('start', 'days');
    var end = moment();
    $('#daterange-btn').daterangepicker(
        {
            ranges   : {
                'Today'       : [moment(), moment()],
                'Yesterday'   : [moment().subtract(1, 'days'), moment()],
                'This Week'   : [moment().clone().startOf('isoWeek'), moment().clone().endOf('isoWeek')],
                'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month'  : [moment().startOf('month'), moment().endOf('month')],
                'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            startDate: start,
            endDate  : end
        },cb
    );

    function cb(start,end){
        $('#reportrange').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
        $('#daterangevalue').val(start.format('MMMM D, YYYY') +'-' + end.format('MMMM D, YYYY'))
    }
    cb(start,end)

    $(document).ready(function (){
        // getData('/api/drivers?assignable=1');
        
        $('#driverModal').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget);
            var order_id = button.data('orderid') // Extract info from data-* attributes
            // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
            // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
            var modal = $(this)
            modal.find('#order_id').val(order_id)
        })

        $('#pingModal').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget);
            var order_id = button.data('orderid') // Extract info from data-* attributes
            // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
            // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
            var modal = $(this)
            modal.find('#d_order_id').val(order_id)
        })
    });

    function getData(url) {
        $('#driver_id').select2({
            theme: 'classic',
            ajax: {
                url: url,
                dataType: 'json',
                processResults: function (data) {
                    // Transforms the top-level key of the response object from 'items' to 'results'
                    if(data.data.length > 0) {
                        return {
                            results: data.data.map(function (item) {
                                return {
                                    id: item.id,
                                    text: item.name,
                                    disabled: item.disabled
                                }
                            })
                        };
                    }
                }
            }
        });
    }


</script>
@endpush
