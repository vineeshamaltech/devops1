<div class="col-sm-12">
    <table class="table table-bordered">
        <tr>
            <td>{!! Form::label('user_id', __('models/orders.fields.user_id').':') !!}</td>
            <td>{!! Form::label('store_id', __('models/orders.fields.store_id').':') !!}</td>
            <td>{!! Form::label('vehicle_id', __('models/orders.fields.vehicle_id').':') !!}</td>
            <td>{!! Form::label('total_price', __('models/orders.fields.total_price').':') !!}</td>
        </tr>
        <tr>
            <td><p>{{ $order->user->name ?? "Customer deleted" }}</p></td>
            <td><p>{{ $order->store->name ?? "No store" }}</p></td>
            <td><p>{{ \App\Models\VehicleType::find($order->vehicle_id)->name ?? "No vehicle selected" }}</p></td>
            <td><p>₹{{ $order->total_price }}</p></td>
        </tr>
        <tr>
            <td>{!! Form::label('discount_type', __('models/orders.fields.discount_type').':') !!}</td>
            <td>{!! Form::label('delivery_fee', __('models/orders.fields.delivery_fee').':') !!}</td>
            <td>{!! Form::label('tax', __('models/orders.fields.tax').':') !!}</td>
            <td>{!! Form::label('grand_total', __('models/orders.fields.grand_total').':') !!}</td>
        </tr>
        <tr>
            <td><p>{{ $order->discount_type ?? "No Discount" }}</p></td>
            <td><p>₹{{ $order->delivery_fee }}</p></td>
            <td><p>₹{{ $order->tax }}</p></td>
            <td><p>₹{{ $order->grand_total }}</p></td>
        </tr>
        <tr>
            <td>{!! Form::label('distance_travelled', __('models/orders.fields.distance_travelled').':') !!}</td>
            <td>{!! Form::label('travel_time', __('models/orders.fields.travel_time').':') !!}</td>
            <td>{!! Form::label('payment_id', __('models/orders.fields.payment_id').':') !!}</td>
            <td>{!! Form::label('driver_id', __('models/orders.fields.driver_id').':') !!}</td>
        </tr>
        <tr>
            <td><p>{{ $order->distance_travelled }} Km</p></td>
            <td><p>{{ $order->travel_time }} Minutes</p></td>
            <td><p>{{ $order->payment_id }}</p></td>
            <td><p>{{ \App\Models\Driver::find($order->driver_id)->name ?? "No Driver" }}</p></td>
        </tr>
        <tr>
            <td>{!! Form::label('order_status', __('models/orders.fields.order_status').':') !!}</td>
            <td>{!! Form::label('created_at', __('models/orders.fields.created_at').':') !!}</td>
            <td>{!! Form::label('updated_at', __('models/orders.fields.updated_at').':') !!}</td>
        </tr>
        <tr>
            <td><p>{{ $order->order_status }}</p></td>
            <td><p>{{ $order->created_at }}</p></td>
            <td><p>{{ $order->updated_at }}</p></td>
        </tr>
    </table>
</div>