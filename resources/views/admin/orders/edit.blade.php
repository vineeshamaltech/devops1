@extends('admin.layouts.app')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.4/jspdf.min.js"></script>
@push('third_party_stylesheets')
    <style>
        @media print
        {
            #no-print { display: none; }
        }

    </style>
    
@endpush
@section('title',trans('lang.edit')." ".trans('models/orders.singular'))

@section('content')
    <div class="content">

        @include('adminlte-templates::common.errors')

        <div class="card">

            {!! Form::model($order, ['route' => ['admin.orders.update', $order->id], 'method' => 'patch']) !!}

            <div class="card-body">
                <div class="row">
                    @include('admin.orders.fields')
                </div>
            </div>

            <div class="card-footer">
                {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
                <a href="{{ route('admin.orders.index') }}" class="btn btn-default">
                    @lang('lang.cancel')
                 </a>
            </div>

            {!! Form::close() !!}

        </div>
            @include('admin.orders.invoice')
    </div>

  
@endsection
