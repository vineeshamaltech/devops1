@extends('admin.layouts.app')

@section('title',trans('lang.view')." ".trans('models/orders.singular'))

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>@lang('models/orders.singular')</h1>
                </div>
                <div class="col-sm-6">
                    <a class="btn btn-default float-right"
                       href="{{ route('admin.orders.index') }}">
                         @lang('lang.back')
                    </a>
                </div>
            </div>
        </div>
    </section>

    <div class="content px-3">
        <div class="card">
            <div class="card-body">
                <div class="container">
                <div class="row">
                    @include('admin.orders.show_fields')
                </div>
                </div>
            </div>
        </div>

        <div class="row">
            <!-- JSON Data Field -->
            <div class="col-sm-6">
                <div class="card">
                    <div class="card-header">
                    {!! Form::label('json_data', __('models/orders.fields.json_data').':') !!}
                    </div>
                    <div class="card-body">
                        <!-- Json Data Field -->
                        <div class="col-sm-6">
                          @if($order->json_data!=null)
                                @foreach($order->json_data as $key => $value)
                                    <p><strong>{{ Helpers::format_settings_title($key) }}</strong>:
                                         @if(is_array($value))
                                            @foreach ($value as $val)
                                                <a href="{{$val}}"><img src="{{$val}}" width="100" height="100"></a>
                                                @endforeach
                                                    @else
                                                        {{ $value }}
                                                        @endif
                                                            </p>
                                                                @endforeach
                                                                    @else
                                                                        <p>No json data</p>
                                                                            @endif
                       </div>
                    </div>
                </div>
            </div> 
         

            <div class="col-sm-6">
                <div class="card">
                    <div class="card-header">
                        {!! Form::label('coupon_data', __('models/orders.fields.coupon_data').':') !!}
                    </div>
                    <div class="card-body">
                        <!-- Coupon Data Field -->
                        <div class="col-sm-6">
                            @if($order->coupon_data!=null)
                                @foreach($order->coupon_data as $key => $value)
                                    <p style="border: 1px solid #dee2e6;padding: 5px 5px 5px 5px;"><strong>{{ Helpers::format_settings_title($key) }}</strong>: {{ $value }}</p>
                                @endforeach
                            @else
                                <p>No coupon data</p>
                            @endif
                        </div>
                    </div>
                </div>
            </div>

            <!-- Pickup Address Field -->
            <div class="col-sm-6">
                <div class="card">
                    <div class="card-header">
                        {!! Form::label('pickup_address', __('models/orders.fields.pickup_address').':') !!}
                    </div>
                    <div class="card-body">
                        @foreach($order->pickup_address as $key => $value)
                            <p style="border: 1px solid #dee2e6;padding: 5px 5px 5px 5px;"><strong>{{ Helpers::format_settings_title($key) }}</strong>: {{ $value }}</p>
                        @endforeach
                    </div>
                </div>
            </div>
               <!-- Delivery Address Field -->
            <div class="col-sm-6">
                <div class="card">
                    <div class="card-header">
                        {!! Form::label('delivery_address', __('models/orders.fields.delivery_address').':') !!}
                    </div>
                    <div class="card-body">
                        <div class="col-sm-12">
                            @foreach($order->delivery_address as $key => $value)
                                <p style="border: 1px solid #dee2e6;padding: 5px 5px 5px 5px;"><strong>{{ Helpers::format_settings_title($key) }}</strong>: {{ $value }}</p>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>

            <!-- Earnings section -->
            <div class="col-sm-6">
                <div class="card">
                    <div class="card-header">
                        {!! Form::label('pickup_address', __('models/orders.fields.earnings').':') !!}
                    </div>
                    <div class="card-body">
                            @if(count($order->earnings) > 0)
                                @foreach($order->earnings as $key => $value)
                                    <p style="border: 1px solid #dee2e6;padding: 5px 5px 5px 5px;"><strong>{{ Helpers::format_settings_title($key+1) }}</strong>: {{ $value->remarks }} - <strong>₹{{ $value->amount }}</strong></p>
                                @endforeach
                            @else
                                <p>No earning data</p>
                            @endif
                        
                    </div>
                </div>
            </div>
          
        </div>

    </div>
@endsection
