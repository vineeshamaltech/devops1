@extends('admin.layouts.app')
  <!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-MPB7SLT');</script>
<!-- End Google Tag Manager -->
@section('title', 'Dashboard')
@section('content')
   <!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MPB7SLT"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
    <div class="container">

     <div class="row">
        @can('admins.index')
            <div class="col-lg-3 col-6">
              <!-- small box -->
              <div class="small-box bg-info">
               <div class="inner">

                  <h5>Total Users :&nbsp;&nbsp; <b>{{$dashboard['users']}}</b></h5>
                  <h5>Today Users :&nbsp;&nbsp;<b>{{$dashboard['today_users']}}</b></h5>
                </div>

                <div class="icon">
                  <i class="ion ion-bag"></i>
                </div>
                {{-- <a   href="{{ route('admin.users.index') }}" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a> --}}
              </div>
            </div>

            <div class="col-lg-3 col-6">
              <!-- small box -->
              <div class="small-box bg-warning">
                <div class="inner">

                  <h5>Total Driver     :&nbsp;&nbsp;<b>{{$dashboard['driver']}}</b></h5>
                  <h5>Pending Approval :&nbsp;&nbsp;<b>{{$dashboard['pending_driver']}}</b></h5>
                </div>
                <div class="icon">
                  <i class="ion ion-person-add"></i>
                </div>
                {{-- <a   href="{{ route('admin.drivers.index') }}" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a> --}}
              </div>
            </div>


            <div class="col-lg-3 col-6">
                <!-- small box -->
                <div class="small-box bg-success">
                <div class="inner">

                    <h5>Total Vendors    :&nbsp;&nbsp; <b>{{$dashboard['vendor']}}</b></h5>
                    <h5>Pending Approval :&nbsp;&nbsp;<b>{{$dashboard['pending_vendor']}}</b></h5>

                </div>
                <div class="icon">
                    <i class="ion ion-stats-bars"></i>
                </div>
                {{-- <a   href="{{ route('admin.vendors.index') }}" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a> --}}
                </div>
            </div>


            @endcan
            <!-- ./col -->
            <div class="col-lg-3 col-6">
              <!-- small box -->
              <div class="small-box bg-info">
                <div class="inner">

                  <h5>Total Earnings :&nbsp;&nbsp; <b>₹{{$dashboard['earning']}}</b></h5>
                  <h5>Today Earnings :&nbsp;&nbsp;<b>₹{{$dashboard['today_earning']}}</b></h5>

                </div>
                <div class="icon">
                  <i class="ion ion-pie-graph"></i>
                </div>
                {{-- <a  href="{{ route('admin.earnings.index') }}" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a> --}}
              </div>
            </div>
            <!-- ./col -->
                
            <div class="col-lg-3 col-6">
              <!-- small box -->
              <div class="small-box bg-info">
                <div class="inner">

                  <h5>Total Orders      :&nbsp;&nbsp;<b>{{$dashboard['orders']}}</b></h5>
                  <h5>Today Orders      :&nbsp;&nbsp;<b>{{$dashboard['today_orders']}}</b></h5>
                  <h5>Unassigned Orders :&nbsp;&nbsp;<b>{{$dashboard['unassigned_orders']}}</b></h5>
                </div>
                <div class="icon">
                  <i class="ion ion-pie-graph"></i>
                </div>
                {{-- <a  href="{{ route('admin.orders.index') }}" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a> --}}
              </div>
            </div>
            <!-- ./col -->

            <div class="col-lg-3 col-6">
              <!-- small box -->
              <div class="small-box bg-warning">
                <div class="inner">

                  <h5>Total Admin Commission: &nbsp;&nbsp;<b>₹{{$dashboard['admin_commision']}}</b></h5>
                  <h5>Today Admin Commission: &nbsp;&nbsp;<b>₹{{$dashboard['today_admin_commision']}}</b></h5>
                  <!-- <h5>Unassigned Orders :&nbsp;&nbsp;<b>{{$dashboard['unassigned_orders']}}</b></h5> -->
                </div>
                <div class="icon">
                  <i class="ion ion-pie-graph"></i>
                </div>
                <!-- <a  href="{{ route('admin.orders.index') }}" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a> -->
              </div>
            </div>
            <!-- ./col -->

            <div class="col-lg-3 col-6">
              <!-- small box -->
              <div class="small-box bg-success">
                <div class="inner">

                  <h5>Total Tax: &nbsp;&nbsp;<b>₹{{$dashboard['tax']}}</b></h5>
                  <h5>Today Tax: &nbsp;&nbsp;<b>₹{{$dashboard['today_tax']}}</b></h5>
                  <!-- <h5>Unassigned Orders :&nbsp;&nbsp;<b>{{$dashboard['unassigned_orders']}}</b></h5> -->
                </div>
                <div class="icon">
                  <i class="ion ion-pie-graph"></i>
                </div>
                <!-- <a  href="{{ route('admin.orders.index') }}" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a> -->
              </div>
            </div>
            <!-- ./col -->

            <div class="col-lg-3 col-6">
              <!-- small box -->
              <div class="small-box bg-info">
                <div class="inner">

                  <h5>Total Delivery Fee: &nbsp;&nbsp;<b>₹{{$dashboard['delivery_fee']}}</b></h5>
                  <h5>Today Delivery Fee: &nbsp;&nbsp;<b>₹{{$dashboard['today_delivery_fee']}}</b></h5>
                  <!-- <h5>Unassigned Orders :&nbsp;&nbsp;<b>{{$dashboard['unassigned_orders']}}</b></h5> -->
                </div>
                <div class="icon">
                  <i class="ion ion-pie-graph"></i>
                </div>
                <!-- <a  href="{{ route('admin.orders.index') }}" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a> -->
              </div>
            </div>
            <!-- ./col -->

          </div>

    </div>
@endsection

@push('third_party_scripts')
    <script src="{{ asset('js/app.js') }}"></script>
    <script>
        Echo.channel("order.admin.178").listen("data", (e) => {
            console.log("Data received");
            console.log(e);
            // playSound("{{ asset('audio/notification.wav') }}");
        }).subscribed(() => {
            console.log("Subscribed");
            console.log(Echo.socketId());
        });

        function playSound(url) {
            var a = new Audio(url);
            a.play();
        }
    </script>
@endpush
