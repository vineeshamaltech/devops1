<!-- From Km Field -->
<div class="form-group col-sm-6">
    {!! Form::label('from_km', __('models/deliveryFees.fields.from_km').':') !!}
    @if(@$deliveryFee)
    <input type="double"  class="form-control" name="from_km" value="{{ old('from_km', $deliveryFee->from_km) }}">
    @else
     <input type="double"  class="form-control" name="from_km" >
    @endif
    <!-- {!! Form::number('from_km', null, ['class' => 'form-control']) !!} -->
</div>

<!-- To Km Field -->
<div class="form-group col-sm-6">
    {!! Form::label('to_km', __('models/deliveryFees.fields.to_km').':') !!}
    @if(@$deliveryFee)
    <input type="double"  class="form-control" name="to_km" value="{{ old('to_km', $deliveryFee->to_km) }}">
    @else
     <input type="double"  class="form-control" name="to_km" >
    @endif
    <!-- {!! Form::number('to_km', null, ['class' => 'form-control']) !!} -->
</div>

<!-- Delivery Fee Field -->
<div class="form-group col-sm-6">
    {!! Form::label('delivery_fee', __('models/deliveryFees.fields.delivery_fee').':') !!}
    {!! Form::number('delivery_fee', null, ['class' => 'form-control','required']) !!}
</div>

<!-- Delivery Fee Field -->
<div class="form-group col-sm-6">
    {!! Form::label('driver_commission', __('models/deliveryFees.fields.driver_commission').':') !!}
    {!! Form::number('driver_commission', null, ['class' => 'form-control','required']) !!}
</div>

