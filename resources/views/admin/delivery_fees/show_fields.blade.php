<!-- From Km Field -->
<div class="col-sm-12">
    {!! Form::label('from_km', __('models/deliveryFees.fields.from_km').':') !!}
    <p>{{ $deliveryFee->from_km }}</p>
</div>

<!-- To Km Field -->
<div class="col-sm-12">
    {!! Form::label('to_km', __('models/deliveryFees.fields.to_km').':') !!}
    <p>{{ $deliveryFee->to_km }}</p>
</div>

<!-- Delivery Fee Field -->
<div class="col-sm-12">
    {!! Form::label('delivery_fee', __('models/deliveryFees.fields.delivery_fee').':') !!}
    <p>{{ $deliveryFee->delivery_fee }}</p>
</div>

<!-- Created At Field -->
<div class="col-sm-12">
    {!! Form::label('created_at', __('models/deliveryFees.fields.created_at').':') !!}
    <p>{{ $deliveryFee->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="col-sm-12">
    {!! Form::label('updated_at', __('models/deliveryFees.fields.updated_at').':') !!}
    <p>{{ $deliveryFee->updated_at }}</p>
</div>

