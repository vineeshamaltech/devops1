@extends('admin.layouts.app')

@section('title',trans('lang.add_new')." ".trans('models/deliveryFees.singular'))

@section('content')
    <div class="content">

        @include('adminlte-templates::common.errors')

        <div class="card">

            {!! Form::open(['route' => 'admin.deliveryFees.store']) !!}

            <div class="card-body">
                <div class="row">
                    @include('admin.delivery_fees.fields')
                </div>
            </div>

            <div class="card-footer">
                {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
                <a href="{{ route('admin.deliveryFees.index') }}" class="btn btn-default">
                 @lang('lang.cancel')
                </a>
            </div>

            {!! Form::close() !!}

        </div>
    </div>
@endsection
