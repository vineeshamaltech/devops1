{!! Form::open(['route' => ['admin.deliveryFees.destroy', $id], 'method' => 'delete']) !!}
<div class='btn-group'>
    @can('deliveryFees.view')
    <a href="{{ route('admin.deliveryFees.show', $id) }}" class='btn btn-default btn-xs'>
        <i class="fa fa-eye"></i>
    </a>
    @endcan
    @can('deliveryFees.edit')
    <a href="{{ route('admin.deliveryFees.edit', $id) }}" class='btn btn-default btn-xs'>
        <i class="fa fa-edit"></i>
    </a>
    @endcan
    @can('deliveryFees.delete')
    {!! Form::button('<i class="fa fa-trash"></i>', [
        'type' => 'submit',
        'class' => 'btn btn-danger btn-xs',
        'onclick' => 'return confirm("'.__('crud.are_you_sure').'")'
    ]) !!}
    @endcan
</div>
{!! Form::close() !!}
