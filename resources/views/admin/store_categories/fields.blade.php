<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', __('models/storeCategories.fields.name').':',['class'=>'required']) !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
    @if($errors -> first('name'))
    <div class="alert-danger" >{{$errors -> first('name')}}</div>
    @endif
</div>

<!-- Image Field -->
<div class="form-group col-sm-6">
    {!! Form::label('image', __('models/storeCategories.fields.image').':',['class'=>'required']) !!}
    <div class="input-group">
        <div class="custom-file">
            {!! Form::file('image', ['class' => 'custom-file-input']) !!}
            {!! Form::label('image', 'Choose file', ['class' => 'custom-file-label']) !!}
        </div>
    </div>
</div>
<div class="clearfix"></div>

<!-- Service Category Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', __('models/storeCategories.fields.service_category_id').':',['class'=>'required']) !!}
    <select name="service_category_id" class="form-control select2" required>
        @foreach(\App\Models\ServiceCategory::where('active',1)->get() as $serviceCategory)
            <option value="{{ $serviceCategory->id }}" @if(!empty($storeCategory) && $serviceCategory->id == $storeCategory->service_category_id) selected @endif>{{ $serviceCategory->name }}</option>
        @endforeach
    </select>
</div>


<!-- Status Field -->
<div class="form-group col-sm-6">
    <div class="form-check">
        {!! Form::hidden('active', 0, ['class' => 'form-check-input']) !!}
        {!! Form::checkbox('active', '1', null, ['class' => 'form-check-input']) !!}
        {!! Form::label('active', __('models/storeCategories.fields.active'), ['class' => 'form-check-label']) !!}
    </div>
</div>
