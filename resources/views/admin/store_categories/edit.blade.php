@extends('admin.layouts.app')

@section('title',trans('lang.edit')." ".trans('models/storeCategories.singular'))

@section('content')
    <div class="content">

        <!--@include('adminlte-templates::common.errors')-->
        @include('flash::message')

        <div class="card">

            {!! Form::model($storeCategory, ['route' => ['admin.storeCategories.update', $storeCategory->id], 'method' => 'patch', 'files' => true]) !!}

            <div class="card-body">
                <div class="row">
                    @include('admin.store_categories.fields')
                </div>
            </div>

            <div class="card-footer">
                {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
                <a href="{{ route('admin.storeCategories.index') }}" class="btn btn-default">
                    @lang('lang.cancel')
                 </a>
            </div>

            {!! Form::close() !!}

        </div>
    </div>
@endsection
