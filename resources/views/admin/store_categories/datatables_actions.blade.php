{!! Form::open(['route' => ['admin.storeCategories.destroy', $id], 'method' => 'delete']) !!}
<div class='btn-group'>
    @can('storeCategories.view')
    <a href="{{ route('admin.storeCategories.show', $id) }}" class='btn btn-default btn-xs'>
        <i class="fa fa-eye"></i>
    </a>
    @endcan
    @can('storeCategories.edit')
    <a href="{{ route('admin.storeCategories.edit', $id) }}" class='btn btn-default btn-xs'>
        <i class="fa fa-edit"></i>
    </a>
    @endcan
    @can('storeCategories.delete')
    {!! Form::button('<i class="fa fa-trash"></i>', [
        'type' => 'submit',
        'class' => 'btn btn-danger btn-xs',
        'onclick' => 'return confirm("'.__('crud.are_you_sure').'")'
    ]) !!}
    @endcan
</div>
{!! Form::close() !!}
