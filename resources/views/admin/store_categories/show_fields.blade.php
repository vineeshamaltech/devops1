<!-- Id Field -->
<div class="col-sm-12">
    {!! Form::label('id', __('models/storeCategories.fields.id').':') !!}
    <p>{{ $storeCategory->id }}</p>
</div>

<!-- Name Field -->
<div class="col-sm-12">
    {!! Form::label('name', __('models/storeCategories.fields.name').':') !!}
    <p>{{ $storeCategory->name }}</p>
</div>

<!-- Image Field -->
<div class="col-sm-12">
    {!! Form::label('image', __('models/storeCategories.fields.image').':') !!}
    <p>{{ $storeCategory->image }}</p>
</div>

<!-- Service Category Id Field -->
<div class="col-sm-12">
    {!! Form::label('service_category_id', __('models/storeCategories.fields.service_category_id').':') !!}
    <p>{{ $storeCategory->service_category_id }}</p>
</div>

<!-- Status Field -->
<div class="col-sm-12">
    {!! Form::label('status', __('models/storeCategories.fields.status').':') !!}
    <p>{{ $storeCategory->status }}</p>
</div>

<!-- Created At Field -->
<div class="col-sm-12">
    {!! Form::label('created_at', __('models/storeCategories.fields.created_at').':') !!}
    <p>{{ $storeCategory->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="col-sm-12">
    {!! Form::label('updated_at', __('models/storeCategories.fields.updated_at').':') !!}
    <p>{{ $storeCategory->updated_at }}</p>
</div>

