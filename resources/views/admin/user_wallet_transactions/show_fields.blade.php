<!-- User Id Field -->
<div class="col-sm-12">
    {!! Form::label('user_id', __('models/userWalletTransactions.fields.user_id').':') !!}
    <p>{{ $userWalletTransaction->user_id }}</p>
</div>

<!-- Type Field -->
<div class="col-sm-12">
    {!! Form::label('type', __('models/userWalletTransactions.fields.type').':') !!}
    <p>{{ $userWalletTransaction->type }}</p>
</div>

<!-- Amount Field -->
<div class="col-sm-12">
    {!! Form::label('amount', __('models/userWalletTransactions.fields.amount').':') !!}
    <p>{{ $userWalletTransaction->amount }}</p>
</div>

<!-- Remarks Field -->
<div class="col-sm-12">
    {!! Form::label('remarks', __('models/userWalletTransactions.fields.remarks').':') !!}
    <p>{{ $userWalletTransaction->remarks }}</p>
</div>

<!-- Created At Field -->
<div class="col-sm-12">
    {!! Form::label('created_at', __('models/userWalletTransactions.fields.created_at').':') !!}
    <p>{{ $userWalletTransaction->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="col-sm-12">
    {!! Form::label('updated_at', __('models/userWalletTransactions.fields.updated_at').':') !!}
    <p>{{ $userWalletTransaction->updated_at }}</p>
</div>

