{!! Form::open(['route' => ['admin.userWalletTransactions.destroy', $id], 'method' => 'delete']) !!}
<div class='btn-group'>
    @can('userWalletTransactions.view')
    <a href="{{ route('admin.userWalletTransactions.show', $id) }}" class='btn btn-default btn-xs'>
        <i class="fa fa-eye"></i>
    </a>
    @endcan
    <!-- @can('userWalletTransactions.edit')
    <a href="{{ route('admin.userWalletTransactions.edit', $id) }}" class='btn btn-default btn-xs'>
        <i class="fa fa-edit"></i>
    </a>
    @endcan -->
    <!-- @can('userWalletTransactions.delete')
    {!! Form::button('<i class="fa fa-trash"></i>', [
        'type' => 'submit',
        'class' => 'btn btn-danger btn-xs',
        'onclick' => 'return confirm("'.__('crud.are_you_sure').'")'
    ]) !!}
    @endcan -->
</div>
{!! Form::close() !!}
