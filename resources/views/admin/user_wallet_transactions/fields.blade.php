<!-- User Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('user_id', __('models/userWalletTransactions.fields.user').':') !!}
    <select name="user_id" class="form-control select2">
        @foreach(\App\Models\User::get() as $user)
            <option value="{{ $user->id }}" @if(!empty($address)&&$user->id == $address->user_id) selected @endif>{{ $user->name }} ({{ $user->mobile }})</option>
        @endforeach
    </select>
</div>

<!-- Type Field -->
<div class="form-group col-sm-6">
    {!! Form::label('type', __('models/userWalletTransactions.fields.type').':') !!}
    {!! Form::select('type', ['CREDIT'=>'CREDIT','DEBIT'=>'DEBIT'], null, ['class' => 'form-control select2']) !!}
</div>

<!-- Type Field -->
<div class="form-group col-sm-6">
    {!! Form::label('amount', __('models/userWalletTransactions.fields.amount').':') !!}
    {!! Form::number('amount', null, ['class' => 'form-control']) !!}
</div>


<!-- Remarks Field -->
<div class="form-group col-sm-6">
    {!! Form::label('remarks', __('models/userWalletTransactions.fields.remarks').':') !!}
    {!! Form::text('remarks', null, ['class' => 'form-control']) !!}
</div>
