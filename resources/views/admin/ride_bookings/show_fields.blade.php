<!-- Id Field -->
<div class="col-sm-12">
    {!! Form::label('id', __('models/rideBookings.fields.id').':') !!}
    <p>{{ $rideBooking->id }}</p>
</div>

<!-- User Id Field -->
<div class="col-sm-12">
    {!! Form::label('user_id', __('models/rideBookings.fields.user_id').':') !!}
    <p>{{ $rideBooking->user_id }}</p>
</div>

<!-- Ride Id Field -->
<div class="col-sm-12">
    {!! Form::label('ride_id', __('models/rideBookings.fields.ride_id').':') !!}
    <p>{{ $rideBooking->ride_id }}</p>
</div>

<!-- From Name Field -->
<div class="col-sm-12">
    {!! Form::label('from_name', __('models/rideBookings.fields.from_name').':') !!}
    <p>{{ $rideBooking->from_name }}</p>
</div>

<!-- To Name Field -->
<div class="col-sm-12">
    {!! Form::label('to_name', __('models/rideBookings.fields.to_name').':') !!}
    <p>{{ $rideBooking->to_name }}</p>
</div>

<!-- From Lat Field -->
<div class="col-sm-12">
    {!! Form::label('from_lat', __('models/rideBookings.fields.from_lat').':') !!}
    <p>{{ $rideBooking->from_lat }}</p>
</div>

<!-- From Long Field -->
<div class="col-sm-12">
    {!! Form::label('from_long', __('models/rideBookings.fields.from_long').':') !!}
    <p>{{ $rideBooking->from_long }}</p>
</div>

<!-- To Lat Field -->
<div class="col-sm-12">
    {!! Form::label('to_lat', __('models/rideBookings.fields.to_lat').':') !!}
    <p>{{ $rideBooking->to_lat }}</p>
</div>

<!-- To Long Field -->
<div class="col-sm-12">
    {!! Form::label('to_long', __('models/rideBookings.fields.to_long').':') !!}
    <p>{{ $rideBooking->to_long }}</p>
</div>

<!-- Price Field -->
<div class="col-sm-12">
    {!! Form::label('price', __('models/rideBookings.fields.price').':') !!}
    <p>{{ $rideBooking->price }}</p>
</div>

<!-- Created At Field -->
<div class="col-sm-12">
    {!! Form::label('created_at', __('models/rideBookings.fields.created_at').':') !!}
    <p>{{ $rideBooking->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="col-sm-12">
    {!! Form::label('updated_at', __('models/rideBookings.fields.updated_at').':') !!}
    <p>{{ $rideBooking->updated_at }}</p>
</div>

