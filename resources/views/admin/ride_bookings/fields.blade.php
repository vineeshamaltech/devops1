<!-- Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('id', __('models/rideBookings.fields.id').':') !!}
    {!! Form::text('id', null, ['class' => 'form-control']) !!}
</div>

<!-- User Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('user_id', __('models/rideBookings.fields.user_id').':') !!}
    {!! Form::text('user_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Ride Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('ride_id', __('models/rideBookings.fields.ride_id').':') !!}
    {!! Form::text('ride_id', null, ['class' => 'form-control']) !!}
</div>

<!-- From Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('from_name', __('models/rideBookings.fields.from_name').':') !!}
    {!! Form::text('from_name', null, ['class' => 'form-control']) !!}
</div>

<!-- To Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('to_name', __('models/rideBookings.fields.to_name').':') !!}
    {!! Form::text('to_name', null, ['class' => 'form-control']) !!}
</div>

<!-- From Lat Field -->
<div class="form-group col-sm-6">
    {!! Form::label('from_lat', __('models/rideBookings.fields.from_lat').':') !!}
    {!! Form::text('from_lat', null, ['class' => 'form-control']) !!}
</div>

<!-- From Long Field -->
<div class="form-group col-sm-6">
    {!! Form::label('from_long', __('models/rideBookings.fields.from_long').':') !!}
    {!! Form::text('from_long', null, ['class' => 'form-control']) !!}
</div>

<!-- To Lat Field -->
<div class="form-group col-sm-6">
    {!! Form::label('to_lat', __('models/rideBookings.fields.to_lat').':') !!}
    {!! Form::text('to_lat', null, ['class' => 'form-control']) !!}
</div>

<!-- To Long Field -->
<div class="form-group col-sm-6">
    {!! Form::label('to_long', __('models/rideBookings.fields.to_long').':') !!}
    {!! Form::text('to_long', null, ['class' => 'form-control']) !!}
</div>

<!-- Price Field -->
<div class="form-group col-sm-6">
    {!! Form::label('price', __('models/rideBookings.fields.price').':') !!}
    {!! Form::text('price', null, ['class' => 'form-control']) !!}
</div>