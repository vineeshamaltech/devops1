@extends('admin.layouts.app')

@section('title',trans('lang.edit')." ".trans('models/rideBookings.singular'))

@section('content')
    <div class="content">

        @include('adminlte-templates::common.errors')

        <div class="card">

            {!! Form::model($rideBooking, ['route' => ['admin.rideBookings.update', $rideBooking->id], 'method' => 'patch']) !!}

            <div class="card-body">
                <div class="row">
                    @include('admin.ride_bookings.fields')
                </div>
            </div>

            <div class="card-footer">
                {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
                <a href="{{ route('admin.rideBookings.index') }}" class="btn btn-default">
                    @lang('lang.cancel')
                 </a>
            </div>

            {!! Form::close() !!}

        </div>
    </div>
@endsection