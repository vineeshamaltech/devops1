{!! Form::open(['route' => ['admin.rideBookings.destroy', $id], 'method' => 'delete']) !!}
<div class='btn-group'>
    @can('rideBookings.view')
    <a href="{{ route('admin.rideBookings.show', $id) }}" class='btn btn-default btn-xs'>
        <i class="fa fa-eye"></i>
    </a>
    @endcan
    @can('rideBookings.edit')
    <a href="{{ route('admin.rideBookings.edit', $id) }}" class='btn btn-default btn-xs'>
        <i class="fa fa-edit"></i>
    </a>
    @endcan
    @can('rideBookings.delete')
    {!! Form::button('<i class="fa fa-trash"></i>', [
        'type' => 'submit',
        'class' => 'btn btn-danger btn-xs',
        'onclick' => 'return confirm("'.__('crud.are_you_sure').'")'
    ]) !!}
    @endcan
</div>
{!! Form::close() !!}
