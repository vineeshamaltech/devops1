<!-- Product Id Field -->
<div class="col-sm-12">
    {!! Form::label('product_id', __('models/productVariants.fields.product_id').':') !!}
    <p>{{ $productVariant->product_id }}</p>
</div>

<!-- Name Field -->
<div class="col-sm-12">
    {!! Form::label('name', __('models/productVariants.fields.name').':') !!}
    <p>{{ $productVariant->name }}</p>
</div>

<!-- Price Field -->
<div class="col-sm-12">
    {!! Form::label('price', __('models/productVariants.fields.price').':') !!}
    <p>{{ $productVariant->price }}</p>
</div>

<!-- Sku Field -->
<div class="col-sm-12">
    {!! Form::label('sku', __('models/productVariants.fields.sku').':') !!}
    <p>{{ $productVariant->sku }}</p>
</div>

<!-- Image Field -->
<div class="col-sm-12">
    {!! Form::label('image', __('models/productVariants.fields.image').':') !!}
    <p>{{ $productVariant->image }}</p>
</div>

<!-- Created At Field -->
<div class="col-sm-12">
    {!! Form::label('created_at', __('models/productVariants.fields.created_at').':') !!}
    <p>{{ $productVariant->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="col-sm-12">
    {!! Form::label('updated_at', __('models/productVariants.fields.updated_at').':') !!}
    <p>{{ $productVariant->updated_at }}</p>
</div>

