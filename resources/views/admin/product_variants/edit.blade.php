@extends('admin.layouts.app')

@section('title',trans('lang.edit')." ".trans('models/productVariants.singular'))

@section('content')
    <div class="content">

        @include('adminlte-templates::common.errors')

        <div class="card">

            {!! Form::model($productVariant, ['route' => ['admin.productVariants.update', $productVariant->id], 'method' => 'patch','files'=>true]) !!}

            <div class="card-body">
                <div class="row">
                    @include('admin.product_variants.fields')
                </div>
            </div>

            <div class="card-footer">
                {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
                <a href="{{ route('admin.productVariants.index') }}" class="btn btn-default">
                    @lang('lang.cancel')
                 </a>
            </div>

            {!! Form::close() !!}

        </div>
    </div>
@endsection
