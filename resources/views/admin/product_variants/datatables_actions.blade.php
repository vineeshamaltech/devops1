{!! Form::open(['route' => ['admin.productVariants.destroy', $id], 'method' => 'delete']) !!}
<div class='btn-group'>
    @can('productVariants.view')
    <a href="{{ route('admin.productVariants.show', $id) }}" class='btn btn-default btn-xs'>
        <i class="fa fa-eye"></i>
    </a>
    @endcan
    @can('productVariants.edit')
    <a href="{{ route('admin.productVariants.edit', $id) }}" class='btn btn-default btn-xs'>
        <i class="fa fa-edit"></i>
    </a>
    @endcan
    @can('productVariants.delete')
    {!! Form::button('<i class="fa fa-trash"></i>', [
        'type' => 'submit',
        'class' => 'btn btn-danger btn-xs',
        'onclick' => 'return confirm("'.__('crud.are_you_sure').'")'
    ]) !!}
    @endcan
</div>
{!! Form::close() !!}
