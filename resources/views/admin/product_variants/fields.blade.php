<!-- Store Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('store_id', __('models/products.fields.store_id').':') !!}
    <select name="store_id" class="form-control select2" id="store_id">
        <option value="0">Select Store</option>
        @foreach($stores as $store)
            <option value="{{ $store->id }}" @if(!empty($product)&&$product->store_id == $store->id) selected @endif>{{ $store->name}}</option>
        @endforeach
    </select>
</div>


<!-- Product Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('product_id', __('models/productVariants.fields.product_id').':') !!}
    <select name="product_id" class="form-control select2">
        @foreach($products as $product)
            <option value="{{ $product->id }}" @if(!empty($productVariant)&&$product->id == $productVariant->product_id) selected @endif>{{ $product->name }} ({{ $product->store_name }})</option>
        @endforeach
    </select>
</div>

<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', __('models/productVariants.fields.name').':') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Price Field -->
<div class="form-group col-sm-6">
    {!! Form::label('price', __('models/productVariants.fields.price').':') !!}
    {!! Form::number('price', null, ['class' => 'form-control']) !!}
</div>

<!-- Price Field -->
<div class="form-group col-sm-6">
    {!! Form::label('discount_price', 'Discounted Price:') !!}
    {!! Form::number('discount_price', null, ['class' => 'form-control']) !!}
</div>

<!-- Sku Field -->
<div class="form-group col-sm-6">
    {!! Form::label('sku', __('models/productVariants.fields.sku').':') !!}
    {!! Form::number('sku', null, ['class' => 'form-control']) !!}
</div>

<!-- Image Field -->
<div class="form-group col-sm-6">
    {!! Form::label('image', __('models/productVariants.fields.image').':') !!}
    {!! Form::file('image', ['class' => 'form-control']) !!}
</div>
<!-- Active Field -->
<div class="form-group col-sm-6">
    <div class="form-check">
        {!! Form::hidden('active', 0, ['class' => 'form-check-input']) !!}
        {!! Form::checkbox('active', '1', null, ['class' => 'form-check-input']) !!}
        {!! Form::label('active', __('models/products.fields.active'), ['class' => 'form-check-label']) !!}
    </div>
</div>

 <!--Is Open Field -->
<div class="form-group col-sm-6">
    <div class="form-check">
        {!! Form::hidden('is_open', 0, ['class' => 'form-check-input']) !!}
        {!! Form::checkbox('is_open', '1', null, ['class' => 'form-check-input']) !!}
        {!! Form::label('is_open', __('Is_Open'), ['class' => 'form-check-label']) !!}
    </div>
</div>