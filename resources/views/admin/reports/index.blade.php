@extends('admin.layouts.app')

@section('title',trans('reports.title'))

@section('content')
    <div class="content">
        @include('flash::message')
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    {!! Form::open(['route' => 'admin.reports.generate']) !!}
                    <div class="card-header">
                        <h3 class="card-title">
                            <i class="fas fa-file-import"></i>
                            {{trans('reports.title')}}
                        </h3>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="form-group col-sm-3">
                                <label for="report_of">Report Of</label>
                                <select name="report_of" id="report_of" class="form-control select2">
                                    <option value="">Select</option>
                                    <option value="admin">Admin</option>
                                    <option value="vendor">Vendor</option>
                                    <option value="driver">Driver</option>
                                </select>
                            </div>
                            <div class="form-group col-sm-3">
                                <label for="reportable_id">Choose One</label>
                                <select name="reportable_id" id="reportable_id" class="form-control select2">

                                </select>
                            </div>
                            <div class="form-group col-sm-3">
                                <label for="interval">Choose Interval</label>
                                <select name="interval" id="interval" class="form-control select2">
                                    <option value="day">Daily</option>
                                    <option value="week">Weekly</option>
                                    <option value="month">Monthly</option>
                                    <option value="year">Yearly</option>
                                </select>
                            </div>
                            <div class="form-group col-sm-3">
                                <label>Date range button:</label>
                                <small id="reportrange"></small>
                                <div class="input-group col-sm-12">
                                    <button type="button" class="btn btn-default col-sm-12" id="daterange-btn">
                                        <i class="far fa-calendar-alt"></i> Date range picker
                                        <i class="fas fa-caret-down"></i>
                                    </button>
                                </div>
                                <input id="daterangevalue" name="daterange" type="hidden" value="">
                            </div>

                        </div>
                    </div>
                    <div class="card-footer">
                        {!! Form::submit('Generate', ['class' => 'btn btn-primary']) !!}

                    </div>
                    {!! Form::close() !!}
                </div>
            </div>

            @if(!empty($data_title))
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">
                            <i class="fas fa-file-import"></i>
                            {{$data_title}}
                        </h3>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="info-box">
                                    <span class="info-box-icon bg-primary"><i class="fa fa-globe-asia"></i></span>

                                    <div class="info-box-content">
                                        <span class="info-box-text">Total Orders</span>
                                        <span class="info-box-number">{{$total_orders}}</span>
                                    </div>
                                    <!-- /.info-box-content -->
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="info-box">
                                    <span class="info-box-icon bg-warning"><i class="fa fa-rupee-sign"></i></span>

                                    <div class="info-box-content">
                                        <span class="info-box-text">Total Order Amount</span>
                                        <span class="info-box-number">{{$total_amount}}</span>
                                    </div>
                                    <!-- /.info-box-content -->
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="info-box">
                                    <span class="info-box-icon bg-success"><i class="far fa-money-bill-alt"></i></span>

                                    <div class="info-box-content">
                                        <span class="info-box-text">Total Earnings</span>
                                        <span class="info-box-number">{{$earnings}}</span>
                                    </div>
                                    <!-- /.info-box-content -->
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="info-box">
                                    <span class="info-box-icon bg-danger"><i class="far fa-minus-square"></i></span>

                                    <div class="info-box-content">
                                        <span class="info-box-text">Total Tax</span>
                                        <span class="info-box-number">{{$total_tax ?? 0}}</span>
                                    </div>
                                    <!-- /.info-box-content -->
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="info-box">
                                    <span class="info-box-icon bg-danger"><i class="far fa-minus-square"></i></span>

                                    <div class="info-box-content">
                                        <span class="info-box-text">Total Admin Commission</span>
                                        <span class="info-box-number">{{$admin_commission ?? 0}}</span>
                                    </div>
                                    <!-- /.info-box-content -->
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <canvas id="myChart" width="400" height="100"></canvas>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            @endif

            @if(@$service_data_title)
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">
                            <i class="fas fa-file-import"></i>
                            {{$service_data_title}}
                        
                        </h3>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="info-box">
                                    <span class="info-box-icon bg-primary"><i class="fa fa-globe-asia"></i></span>

                                    <div class="info-box-content">
                                        <span class="info-box-text">Total Orders</span>
                                        <span class="info-box-number">{{$service_total_orders}}</span>
                                    </div>
                                    <!-- /.info-box-content -->
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="info-box">
                                    <span class="info-box-icon bg-warning"><i class="fa fa-rupee-sign"></i></span>

                                    <div class="info-box-content">
                                        <span class="info-box-text">Total Order Amount</span>
                                        <span class="info-box-number">{{$service_total_amount}}</span>
                                    </div>
                                    <!-- /.info-box-content -->
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="info-box">
                                    <span class="info-box-icon bg-success"><i class="far fa-money-bill-alt"></i></span>

                                    <div class="info-box-content">
                                        <span class="info-box-text">Total Earnings</span>
                                        <span class="info-box-number">{{$service_earnings}}</span>
                                    </div>
                                    <!-- /.info-box-content -->
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="info-box">
                                    <span class="info-box-icon bg-danger"><i class="far fa-minus-square"></i></span>

                                    <div class="info-box-content">
                                        <span class="info-box-text">Total Tax</span>
                                        <span class="info-box-number">{{$service_total_tax ?? 0}}</span>
                                    </div>
                                    <!-- /.info-box-content -->
                                </div>
                            </div>
                             <div class="col-sm-12">
                                <canvas id="myServiceChart" width="400" height="100"></canvas>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            @endif


        </div>

    </div>
@endsection

@push('third_party_stylesheets')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-daterangepicker/3.0.5/daterangepicker.css" integrity="sha512-gp+RQIipEa1X7Sq1vYXnuOW96C4704yI1n0YB9T/KqdvqaEgL6nAuTSrKufUX3VBONq/TPuKiXGLVgBKicZ0KA==" crossorigin="anonymous" referrerpolicy="no-referrer" />
@endpush

@push('third_party_scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/3.6.2/chart.min.js" integrity="sha512-tMabqarPtykgDtdtSqCL3uLVM0gS1ZkUAVhRFu1vSEFgvB73niFQWJuvviDyBGBH22Lcau4rHB5p2K2T0Xvr6Q==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-daterangepicker/3.0.5/daterangepicker.min.js" integrity="sha512-mh+AjlD3nxImTUGisMpHXW03gE6F4WdQyvuFRkjecwuWLwD2yCijw4tKA3NsEFpA1C3neiKhGXPSIGSfCYPMlQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script type="text/javascript">
    var start = moment().subtract(29, 'days');
    var end = moment();
    $('#daterange-btn').daterangepicker(
        {
            ranges   : {
                'Today'       : [moment(), moment()],
                'Yesterday'   : [moment().subtract(1, 'days'), moment()],
                'This Week'   : [moment().clone().startOf('isoWeek'), moment().clone().endOf('isoWeek')],
                'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month'  : [moment().startOf('month'), moment().endOf('month')],
                'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            startDate: start,
            endDate  : end
        },cb
    );

    function cb(start,end){
        $('#reportrange').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
        $('#daterangevalue').val(start.format('MMMM D, YYYY') +'-' + end.format('MMMM D, YYYY'))
    }
    cb(start,end)


    $(document).ready(function (){
        $('#report_of').on('change',function () {
            $('#reportable_id').html('');
            var data = $(this).val();
            var url = "";
            if(data === 'admin'){
                url = '/api/get_admin?filter=id;name&searchJoin=and&searchFields=name:like';
            }
            if(data === 'vendor'){
                url = '/api/get_vendor?filter=id;name&searchJoin=and&searchFields=name:like';
            }
            else if(data === 'driver'){
                url = '/api/get_all_driver?filter=id;name;mobile&searchFields=name:like';
                // http://127.0.0.1:8000/api/drivers?filter=id;name;mobile&search=name:arad&searchFields=name:like
            } else if(data === ""){

            }
            getData(url);
        }).change();
    });

    function getData(url) {
        $('#reportable_id').select2({
            theme: 'classic',
            ajax: {
                url: url,
                dataType: 'json',
                data: function (params) {
                    if(params.term !== undefined){
                        return {
                            search: 'name:' + params.term + ';type:Vendor',
                        };
                    }else{
                        return {};
                    }

                    // Query parameters will be ?search=[term]&type=public

                },
                processResults: function (data) {
                    // Transforms the top-level key of the response object from 'items' to 'results'
                    if(data.data.length > 0) {
                        return {
                            results: data.data.map(function (item) {
                                return {
                                    id: item.id,
                                    text: item.name
                                }
                            })
                        };
                    }
                }
            }
        });
    }
</script>
    @if(!empty($chart_data))
    <script>
        const ctx = document.getElementById('myChart');
        var labels = [];
        var data = [];
        @foreach($chart_data as $key => $value)
            labels.push('{{$key}}');
            data.push('{{$value}}');
        @endforeach
      
        const myChart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: labels,
                datasets: [{
                    label: '# of Orders',
                    data: data,
                    backgroundColor: [
                        'rgba(255, 99, 132, 0.2)',
                        'rgba(54, 162, 235, 0.2)',
                        'rgba(255, 206, 86, 0.2)',
                        'rgba(75, 192, 192, 0.2)',
                        'rgba(153, 102, 255, 0.2)',
                        'rgba(255, 159, 64, 0.2)'
                    ],
                    borderColor: [
                        'rgba(255, 99, 132, 1)',
                        'rgba(54, 162, 235, 1)',
                        'rgba(255, 206, 86, 1)',
                        'rgba(75, 192, 192, 1)',
                        'rgba(153, 102, 255, 1)',
                        'rgba(255, 159, 64, 1)'
                    ],
                    borderWidth: 1
                }]
            },
            options: {
                scales: {
                    y: {
                        beginAtZero: true
                    }
                }
            }
        });
    
        const ctxs = document.getElementById('myServiceChart');

        var labels = [];
        var data = [];
          
        @foreach($service_chart_data as $key => $value)
            labels.push('{{$key}}');
            data.push('{{$value}}');
        @endforeach
       console.log(data);
        const myServiceChart = new Chart(ctxs, {
            type: 'bar',
            data: {
                labels: labels,
                datasets: [{
                    label: '# of Orders',
                    data: data,
                    backgroundColor: [
                        'rgba(255, 99, 132, 0.2)',
                        'rgba(54, 162, 235, 0.2)',
                        'rgba(255, 206, 86, 0.2)',
                        'rgba(75, 192, 192, 0.2)',
                        'rgba(153, 102, 255, 0.2)',
                        'rgba(255, 159, 64, 0.2)'
                    ],
                    borderColor: [
                        'rgba(255, 99, 132, 1)',
                        'rgba(54, 162, 235, 1)',
                        'rgba(255, 206, 86, 1)',
                        'rgba(75, 192, 192, 1)',
                        'rgba(153, 102, 255, 1)',
                        'rgba(255, 159, 64, 1)'
                    ],
                    borderWidth: 1
                }]
            },
            options: {
                scales: {
                    y: {
                        beginAtZero: true
                    }
                }
            }
        });






      

    </script>
    @endif


   
@endpush

<script>
 
</script>

