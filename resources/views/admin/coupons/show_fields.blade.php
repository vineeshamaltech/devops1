<!-- Store Id Field -->
<div class="col-sm-12">
    {!! Form::label('store_id', __('models/coupons.fields.store_id').':') !!}
    <p>{{ $coupon->store_id }}</p>
</div>

<!-- Image Field -->
<div class="col-sm-12">
    {!! Form::label('image', __('models/coupons.fields.image').':') !!}
    <p>{{ $coupon->image }}</p>
</div>

<!-- Code Field -->
<div class="col-sm-12">
    {!! Form::label('code', __('models/coupons.fields.code').':') !!}
    <p>{{ $coupon->code }}</p>
</div>

<!-- Description Field -->
<div class="col-sm-12">
    {!! Form::label('description', __('models/coupons.fields.description').':') !!}
    <p>{{ $coupon->description }}</p>
</div>

<!-- Max Use Per User Field -->
<div class="col-sm-12">
    {!! Form::label('max_use_per_user', __('models/coupons.fields.max_use_per_user').':') !!}
    <p>{{ $coupon->max_use_per_user }}</p>
</div>

<!-- Min Cart Value Field -->
<div class="col-sm-12">
    {!! Form::label('min_cart_value', __('models/coupons.fields.min_cart_value').':') !!}
    <p>{{ $coupon->min_cart_value }}</p>
</div>

<!-- Start Date Field -->
<div class="col-sm-12">
    {!! Form::label('start_date', __('models/coupons.fields.start_date').':') !!}
    <p>{{ $coupon->start_date }}</p>
</div>

<!-- End Date Field -->
<div class="col-sm-12">
    {!! Form::label('end_date', __('models/coupons.fields.end_date').':') !!}
    <p>{{ $coupon->end_date }}</p>
</div>

<!-- Discount Type Field -->
<div class="col-sm-12">
    {!! Form::label('discount_type', __('models/coupons.fields.discount_type').':') !!}
    <p>{{ $coupon->discount_type }}</p>
</div>

<!-- Discount Value Field -->
<div class="col-sm-12">
    {!! Form::label('discount_value', __('models/coupons.fields.discount_value').':') !!}
    <p>{{ $coupon->discount_value }}</p>
</div>

<!-- Active Field -->
<div class="col-sm-12">
    {!! Form::label('active', __('models/coupons.fields.active').':') !!}
    <p>{{ $coupon->active }}</p>
</div>

<!-- Created At Field -->
<div class="col-sm-12">
    {!! Form::label('created_at', __('models/coupons.fields.created_at').':') !!}
    <p>{{ $coupon->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="col-sm-12">
    {!! Form::label('updated_at', __('models/coupons.fields.updated_at').':') !!}
    <p>{{ $coupon->updated_at }}</p>
</div>

