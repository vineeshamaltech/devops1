@extends('admin.layouts.app')

@section('title',trans('lang.edit')." ".trans('models/coupons.singular'))

@section('content')
    <div class="content">

        @include('adminlte-templates::common.errors')

        <div class="card">

            {!! Form::model($coupon, ['route' => ['admin.coupons.update', $coupon->id], 'method' => 'patch', 'files'=>true]) !!}

            <div class="card-body">
                <div class="row">
                    @include('admin.coupons.fields')
                </div>
            </div>

            <div class="card-footer">
                {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
                <a href="{{ route('admin.coupons.index') }}" class="btn btn-default">
                    @lang('lang.cancel')
                 </a>
            </div>

            {!! Form::close() !!}

        </div>
    </div>
@endsection
