<!-- Store Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('store_id', __('models/coupons.fields.store_id').':') !!}
    <select name="store_id" class="form-control select2" required>
        @foreach($stores as $store)
            <option @if(!empty($coupon)&&$coupon->store_id ==  $store->id) selected @endif value="{{ $store->id }}">{{ $store->name }}</option>
        @endforeach
    </select>
</div>

<!-- Image Field -->
<div class="form-group col-sm-6">
    {!! Form::label('image', __('models/categories.fields.image').':',['class'=>'required']) !!}
    <div class="input-group">
        <div class="custom-file">
            {!! Form::file('image', ['class' => 'custom-file-input','required'=>'true']) !!}
            {!! Form::label('image', 'Choose file', ['class' => 'custom-file-label']) !!}
        </div>
    </div>
</div>
<div class="clearfix"></div>

<!-- Code Field -->
<div class="form-group col-sm-6">
    {!! Form::label('code', __('models/coupons.fields.code').':',['class'=>'required']) !!}
    {!! Form::text('code', null, ['class' => 'form-control','required'=>'true']) !!}
</div>

<!-- Description Field -->
<div class="form-group col-sm-6">
    {!! Form::label('description', __('models/coupons.fields.description').':') !!}
    {!! Form::text('description', null, ['class' => 'form-control']) !!}
</div>

<!-- Max Use Per User Field -->
<div class="form-group col-sm-6">
    {!! Form::label('max_use_per_user', __('models/coupons.fields.max_use_per_user').':',['class'=>'required']) !!}
    {!! Form::number('max_use_per_user', null, ['class' => 'form-control','required']) !!}
</div>

<!-- Min Cart Value Field -->
<div class="form-group col-sm-6">
    {!! Form::label('min_cart_value', __('models/coupons.fields.min_cart_value').':') !!}
    {!! Form::number('min_cart_value', null, ['class' => 'form-control']) !!}
</div>

<!-- Start Date Field -->
<div class="form-group col-sm-6">
    {!! Form::label('start_date', __('models/coupons.fields.start_date').':') !!}
    <div class="input-group date" id="start_date" data-target-input="nearest">
        <input name="start_date" @if(!empty($coupon)) value="{{$coupon->start_date}}" @endif type="text" class="form-control datetimepicker-input start_date" data-target="#start_date" id="start_date" data-toggle="datetimepicker"/>
        <div class="input-group-append" data-target="#closetime" data-toggle="datetimepicker">
            <div class="input-group-text"><i class="fas fa-clock"></i></div>
        </div>
    </div>
</div>

@push('third_party_stylesheets')
    <link rel="stylesheet" href="{{ asset('assets/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.css') }}"/>
@endpush

@push('third_party_scripts')
    <script src="{{ asset('assets/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.js') }}"></script>
    <script type="text/javascript">
        $(function () {
            $('#start_date').datetimepicker();
        });
    </script>
@endpush

<!-- End Date Field -->
<div class="form-group col-sm-6">
    {!! Form::label('end_date', __('models/coupons.fields.end_date').':') !!}
    <div class="input-group date" id="end_date" data-target-input="nearest">
        <input name="end_date" @if(!empty($coupon)) value="{{$coupon->end_date}}" @endif type="text" class="form-control datetimepicker-input end_date" data-target="#end_date" id="start_date" data-toggle="datetimepicker"/>
        <div class="input-group-append" data-target="#closetime" data-toggle="datetimepicker">
            <div class="input-group-text"><i class="fas fa-clock"></i></div>
        </div>
    </div>
</div>

@push('page_scripts')
    <script type="text/javascript">
        $(function () {
            $('#end_date').datetimepicker();
        });
    </script>
@endpush

<!-- Discount Type Field -->
<div class="form-group col-sm-6">
    {!! Form::label('discount_type', __('models/coupons.fields.discount_type').':') !!}
    {!! Form::select('discount_type', ['FREE DELIVERY'=>'Free Delivery','WALLET CASHBACK'=>'Wallet Cashback','FLAT'=>'FLAT Off'], null, ['class' => 'form-control select2','id'=>'type']) !!}
</div>


<!-- Discount Value Field -->
<div class="form-group col-sm-6">
    {!! Form::label('discount_value', __('models/coupons.fields.discount_value').':',['class'=>'required', 'id'=>'dis_val_label']) !!}
    {!! Form::number('discount_value', null, ['class' => 'form-control','id'=>'dis_val']) !!}
</div>

<!-- Discount Value Field -->
<div class="form-group col-sm-6">
    {!! Form::label('max_discount_value', __('models/coupons.fields.max_discount_value').':',['id'=>'max_val_label']) !!}
    {!! Form::number('max_discount_value', null, ['class' => 'form-control','id'=>'max_val']) !!}
</div>

<!-- Active Field -->
<div class="form-group col-sm-6">
    <div class="form-check">
        {!! Form::hidden('active', 0, ['class' => 'form-check-input']) !!}
        {!! Form::checkbox('active', '1', null, ['class' => 'form-check-input']) !!}
        {!! Form::label('active', __('models/coupons.fields.active'), ['class' => 'form-check-label']) !!}
    </div>
</div>

@push('third_party_scripts')
    <script type="text/javascript">
        $(document).ready(function (){
            // $('#dis_val').hide()
            // $('#max_val').hide()
            //  $('#max_val').show()
            // $('#type').on('change',function () {
               
            //     var discount = $(this).val();
            //     if(discount == 'FREE DELIVERY'){
            //      $('#dis_val').hide()
            //      $('#dis_val_label').hide()
            //     //  $('#max_val_label').hide()
            //     //  $('#max_val').hide()
            //     }
            //     else{
            //      $('#dis_val').show()
            //        $('#dis_val_label').show()
            //      $('#max_val').show()
            //        $('#max_val_label').show()
            //     }
               
            }).change();
        });


    </script>
@endpush

