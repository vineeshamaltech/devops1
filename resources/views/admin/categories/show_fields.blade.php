<!-- Name Field -->
<div class="col-sm-12">
    {!! Form::label('name', __('models/categories.fields.name').':') !!}
    <p>{{ $category->name }}</p>
</div>

<!-- Image Field -->
<div class="col-sm-12">
    {!! Form::label('image', __('models/categories.fields.image').':') !!}
    <p>{{ $category->image }}</p>
</div>

<!-- Parent Field -->
<div class="col-sm-12">
    {!! Form::label('parent', __('models/categories.fields.parent').':') !!}
    <p>{{ $category->parent }}</p>
</div>

<!-- Created At Field -->
<div class="col-sm-12">
    {!! Form::label('created_at', __('models/categories.fields.created_at').':') !!}
    <p>{{ $category->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="col-sm-12">
    {!! Form::label('updated_at', __('models/categories.fields.updated_at').':') !!}
    <p>{{ $category->updated_at }}</p>
</div>

