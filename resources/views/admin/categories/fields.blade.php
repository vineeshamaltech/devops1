<!-- Store Field -->
<div class="form-group col-sm-6">
    {!! Form::label('store_id', __('models/categories.fields.store').':',['class'=>'required']) !!}
    <select name="store_id" class="form-control select2" id="store_id" required>
        @foreach($stores as $store)
            <option @if(!empty($category)&&$category->store_id ==  $store->id) selected @endif value="{{ $store->id }}">{{ $store->name }}</option>
        @endforeach
    </select>
</div>

<!-- Parent Field -->
<div class="form-group col-sm-6">
    {!! Form::label('parent', __('models/categories.fields.parent').':') !!}
    <select id="parent" name="parent" class="form-control select2">
    @if(!empty($parent_category[0]->id))    
        @foreach($categories as $cat)
        <option value="{{$cat->id}}"@if($cat->id == $parent_category[0]->id)selected @endif>{{$cat->name}}</option>
        @endforeach
        @endif
    </select>
</div>


<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', __('models/categories.fields.name').':',['class'=>'required']) !!}
    {!! Form::text('name', null, ['class' => 'form-control','required'=>'true']) !!}
</div>

{{--<!-- Image Field -->--}}
{{--<div class="form-group col-sm-6">--}}
{{--    {!! Form::label('image', __('models/categories.fields.image').':') !!}--}}
{{--    <div class="input-group">--}}
{{--        <div class="custom-file">--}}
{{--            {!! Form::file('image', ['class' => 'custom-file-input']) !!}--}}
{{--            {!! Form::label('image', 'Choose file', ['class' => 'custom-file-label']) !!}--}}
{{--        </div>--}}
{{--    </div>--}}
{{--</div>--}}
{{--<div class="clearfix"></div>--}}

@push('third_party_scripts')
    <script type="text/javascript">
        $(document).ready(function (){
            $('#store_id').on('change',function () {
                var storeId = $(this).val();
                console.log(storeId);
                getCategories(storeId);
            }).change();
        });

        function getCategories($storeId) {
            console.log("getCategories");
            $('#parent').select2({
                theme: 'classic',
                ajax: {
                    url: '/api/categories?search=store_id:'+$storeId+';parent:0&searchJoin=and',
                    dataType: 'json',
                    processResults: function (data) {
                        // Transforms the top-level key of the response object from 'items' to 'results'
                        console.log("processResults");
                        if(data.data.length > 0) {
                            return {
                                results: data.data.map(function (item) {
                                    return {
                                        id: item.id,
                                        text: item.name
                                    }
                                })
                            };
                        }
                    }
                }
            });
        }

    </script>
@endpush

