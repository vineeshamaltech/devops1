{!! Form::open(['route' => ['admin.serviceTypes.destroy', $id], 'method' => 'delete']) !!}
<div class='btn-group'>
    @can('service_types.view')
    <a href="{{ route('admin.serviceTypes.show', $id) }}" class='btn btn-default btn-xs'>
        <i class="fa fa-eye"></i>
    </a>
    @endcan
    @can('service_types.edit')
    <a href="{{ route('admin.serviceTypes.edit', $id) }}" class='btn btn-default btn-xs'>
        <i class="fa fa-edit"></i>
    </a>
    @endcan
{{--    @can('service_types.delete')--}}
{{--    {!! Form::button('<i class="fa fa-trash"></i>', [--}}
{{--        'type' => 'submit',--}}
{{--        'class' => 'btn btn-danger btn-xs',--}}
{{--        'onclick' => 'return confirm("'.__('crud.are_you_sure').'")'--}}
{{--    ]) !!}--}}
{{--    @endcan--}}
</div>
{!! Form::close() !!}
