<!-- Name Field -->
<div class="col-sm-12">
    {!! Form::label('name', __('models/serviceTypes.fields.name').':') !!}
    <p>{{ $serviceType->name }}</p>
</div>

<!-- Created At Field -->
<div class="col-sm-12">
    {!! Form::label('created_at', __('models/serviceTypes.fields.created_at').':') !!}
    <p>{{ $serviceType->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="col-sm-12">
    {!! Form::label('updated_at', __('models/serviceTypes.fields.updated_at').':') !!}
    <p>{{ $serviceType->updated_at }}</p>
</div>

