@extends('admin.layouts.app')
@section('title','Profile')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">
                    {{__('roles.roles')}}
                    </h3>
                </div>

                <div class="card-body">
                    <div class="form-group col-sm-6">
                        <label for="name">Name</label>
                        <input id="name" name="name" value="{{$user->name}}"  type="text" class="form-control"/>
                    </div>
                    <div class="form-group col-sm-6">
                        <label for="email">Email</label>
                        <input id="email" name="email" value="{{$user->email}}"  type="text" class="form-control"/>
                    </div>
                    <div class="form-group col-sm-6">
                        <label for="mobile">Mobile</label>
                        <input id="mobile" name="mobile" value="{{$user->mobile}}"  type="text" class="form-control"/>
                    </div>
                    <div class="form-group col-sm-6">
                        <label for="wallet_balance">Wallet Balance</label>
                        <input id="wallet_balance" name="wallet_balance" value="{{$user->wallet_balance}}"  type="text" class="form-control"/>
                    </div>
                    <div class="form-group col-sm-6">
                        <label for="password">Password</label>
                        <input id="password" name="wallet_balance" autocomplete="off" type="text" class="form-control"/>
                    </div>
                    <div class="form-group col-sm-6">
                        <label for="password_confirmation">Confirm Password</label>
                        <input id="password_confirmation" name="password_confirmation" autocomplete="off" type="text" class="form-control"/>
                    </div>
                    <div class="form-group col-sm-6">
                        <label for="user_type">User Type</label>
                        <select name="role" class="form-control select2">
                            @foreach ($usertype as $type)
                                <option value="{{$type}}">{{$type}}</option>    
                            @endforeach
                        </select>
                        <input id="user_type" name="user_type" value="{{$user->wallet_balance}}"  type="text" class="form-control"/>
                    </div>
                    <div class="form-group col-sm-6">
                        <label for="user_type">Role</label>
                        <select name="role" class="form-control select2">
                            @foreach ($roles as $role)
                                <option value="{{$role->name}}">{{$role->name}}</option>    
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="card-footer">
                    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
                    <a href="{{ route('admin.roles.index') }}" class="btn btn-default">
                        @lang('lang.cancel')
                     </a>
                </div>
            </div>
        </div>
    </div>
@endsection