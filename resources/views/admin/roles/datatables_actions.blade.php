{!! Form::open(['route' => ['admin.roles.destroy', $id], 'method' => 'delete']) !!}
<div class='btn-group'>
    @can('roles.view')
    <a href="{{ route('admin.roles.show', $id) }}" class='btn btn-default btn-xs'>
        <i class="fa fa-eye"></i>
    </a>
    @endcan
    @can('roles.edit')
    <a href="{{ route('admin.roles.edit', [$id,'guard'=>$guard_name]) }}" class='btn btn-default btn-xs'>
        <i class="fa fa-edit"></i>
    </a>
    @endcan
    @can('roles.delete')
    {!! Form::button('<i class="fa fa-trash"></i>', [
        'type' => 'submit',
        'class' => 'btn btn-danger btn-xs',
        'onclick' => 'return confirm("'.__('crud.are_you_sure').'")'
    ]) !!}
    @endcan
</div>
{!! Form::close() !!}
