@extends('admin.layouts.app')
@section('title',trans('models/timeSlots.plural'))
@section('content')
    <div class="content">

        @include('flash::message')

        <div class="clearfix"></div>

        <div class="card">
            <div class="card-header">
                <ul class="nav nav-tabs align-items-end card-header-tabs w-100">
                    <li class="nav-item">
                        <a class="nav-link active" href="{!! url()->current() !!}"><i class="fa fa-list mr-2"></i>{{trans('models/timeSlots.plural')}}</a>
                    </li>
                    @can('homeServiceTimeSlots.create')
                    <li class="nav-item">
                        <a class="nav-link" href="{!! route('admin.timeSlots.create') !!}"><i class="fa fa-plus mr-2"></i>{{trans('lang.add')." Or ".trans('lang.edit')." ".trans('models/timeSlots.singular')}}</a>
                    </li>
                    @endcan
                        
                </ul>
            </div>

            <div class="card-body">
                @if(count($timeslots)>0)
                <div class="row">
                    <table class="table table-striped table-bordered dataTable no-footer">
                        <thead>
                            <td style="font-weight: 800;">Day</td>
                            <td style="font-weight: 800;">From</td>
                            <td style="font-weight: 800;">To</td>
                            <td style="font-weight: 800;">Max Booking</td>
                        </thead>
                        <tbody>
                            @foreach($timeslots as $time)
                            
                            <tr>
                                <td>{{$time->day}}</td>
                                <td>{{\Carbon\Carbon::parse($time->from)->format('g:i a')}}</td>
                                <td>{{\Carbon\Carbon::parse($time->to)->format('g:i a')}}</td>
                                <td>{{$time->max_bookings}}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                @else
                <div class="row">
                    Empty Timeslot
                </div>
                @endif
            </div>
        </div>
    </div>

@endsection