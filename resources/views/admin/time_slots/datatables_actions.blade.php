{!! Form::open(['route' => ['admin.timeSlots.destroy', $id], 'method' => 'delete']) !!}
<div class='btn-group'>
    @can('homeServiceTimeSlots.view')
    <a href="{{ route('admin.timeSlots.show', $id) }}" class='btn btn-default btn-xs'>
        <i class="fa fa-eye"></i>
    </a>
    @endcan
    @can('homeServiceTimeSlots.edit')
    <a href="{{ route('admin.timeSlots.edit', $id) }}" class='btn btn-default btn-xs'>
        <i class="fa fa-edit"></i>
    </a>
    @endcan
    @can('homeServiceTimeSlots.delete')
    {!! Form::button('<i class="fa fa-trash"></i>', [
        'type' => 'submit',
        'class' => 'btn btn-danger btn-xs',
        'onclick' => 'return confirm("'.__('crud.are_you_sure').'")'
    ]) !!}
    @endcan
</div>
{!! Form::close() !!}
