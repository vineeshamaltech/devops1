@extends('admin.layouts.app')

@section('title',trans('lang.edit')." ".trans('models/timeSlots.singular'))

@section('content')
    <div class="content">

        @include('adminlte-templates::common.errors')

        <div class="card">

            {!! Form::model($timeslots, ['route' => ['admin.timeSlots.update_timeslot'], 'method' => 'patch']) !!}

            <div class="card-body">
                <div class="row">
                    @include('admin.time_slots.fields')
                </div>
            </div>

            <div class="card-footer">
                {!! Form::submit('Update', ['class' => 'btn btn-primary']) !!}
                <a href="{{ route('admin.timeSlots.index') }}" class="btn btn-default">
                    @lang('lang.cancel')
                 </a>
            </div>

            {!! Form::close() !!}

        </div>
    </div>
@endsection