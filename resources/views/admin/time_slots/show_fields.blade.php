<!-- Store Id Field -->
<div class="col-sm-12">
    {!! Form::label('max_bookings', __('models/timeSlots.fields.max').':') !!}
    <p>{{ $timeSlots->max_bookings }}</p>
</div>

<!-- Day Field -->
<div class="col-sm-12">
    {!! Form::label('day', __('models/timeSlots.fields.day').':') !!}
    <p>{{ $timeSlots->day }}</p>
</div>

<!-- Open Field -->
<div class="col-sm-12">
    {!! Form::label('open', __('models/timeSlots.fields.from').':') !!}
    <p>{{ $timeSlots->from }}</p>
</div>

<!-- Close Field -->
<div class="col-sm-12">
    {!! Form::label('close', __('models/timeSlots.fields.to').':') !!}
    <p>{{ $timeSlots->to }}</p>
</div>

<!-- Created At Field -->
<div class="col-sm-12">
    {!! Form::label('created_at', __('models/timeSlots.fields.created_at').':') !!}
    <p>{{ $timeSlots->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="col-sm-12">
    {!! Form::label('updated_at', __('models/timeSlots.fields.updated_at').':') !!}
    <p>{{ $timeSlots->updated_at }}</p>
</div>

