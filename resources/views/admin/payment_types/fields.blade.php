<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', __('models/paymentTypes.fields.name').':',['class'=>'required']) !!}
    {!! Form::text('name', null, ['class' => 'form-control','required'=>'true']) !!}
</div>

<!-- Description Field -->
<div class="form-group col-sm-6">
    {!! Form::label('description', __('models/paymentTypes.fields.description').':',['class'=>'required']) !!}
    {!! Form::text('description', null, ['class' => 'form-control','required'=>'true']) !!}
</div>

<!-- Icon Field -->
<div class="form-group col-sm-6">
    {!! Form::label('icon', __('models/paymentTypes.fields.icon').':') !!}
    <div class="input-group">
        <div class="custom-file">
            {!! Form::file('icon', ['class' => 'custom-file-input']) !!}
            {!! Form::label('icon', 'Choose file', ['class' => 'custom-file-label']) !!}
        </div>
    </div>
</div>
<div class="clearfix"></div>


<!-- Payment Type Field -->
<div class="form-group col-sm-6">
    {!! Form::label('payment_key', __('models/paymentTypes.fields.payment_key').':') !!}
    <select name="type" class="form-control select2">
        <option @if(!empty($paymentType) && $paymentType->type == "COD") selected @endif value="COD">COD</option>
        <option @if(!empty($paymentType) && $paymentType->type == "RAZORPAY") selected @endif value="RAZORPAY">Razorpay</option>
        <option @if(!empty($paymentType) && $paymentType->type == "WALLET") selected @endif value="WALLET">Wallet</option>
    </select>
</div>


<!-- Payment Key Field -->
<div class="form-group col-sm-6">
    {!! Form::label('payment_key', __('models/paymentTypes.fields.payment_key').':') !!}
    {!! Form::text('payment_key', null, ['class' => 'form-control']) !!}
</div>

<!-- Payment Secret Field -->
<div class="form-group col-sm-6">
    {!! Form::label('payment_secret', __('models/paymentTypes.fields.payment_secret').':') !!}
    {!! Form::text('payment_secret', null, ['class' => 'form-control']) !!}
</div>

<!-- Payment Type Field -->
<div class="form-group col-sm-6">
    {!! Form::label('service', __('models/paymentTypes.fields.service').':') !!}
    <input type="checkbox" name="service" @if($paymentType->service == 1) checked @endif  data-toggle="toggle" data-size="xs">    
</div>

<div class="form-group col-sm-6">
    {!! Form::label('home_service', __('models/paymentTypes.fields.home_service').':') !!}
    <input type="checkbox" name="home_service" @if($paymentType->home_service == 1) checked @endif  data-toggle="toggle" data-size="xs">    
</div>


