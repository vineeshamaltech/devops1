@extends('admin.layouts.app')
@section('title',trans('models/paymentTypes.plural'))
@section('content')
    <div class="content">

        @include('flash::message')

        <div class="clearfix"></div>

        <div class="card">
            <div class="card-header">
                <ul class="nav nav-tabs align-items-end card-header-tabs w-100">
                    <li class="nav-item">
                        <a class="nav-link active" href="{!! url()->current() !!}"><i class="fa fa-list mr-2"></i>{{trans('models/paymentTypes.plural')}}</a>
                    </li>
                    @can('paymentMethods.create')
                    <li class="nav-item">
                        <!-- <a class="nav-link" href="{!! route('admin.paymentTypes.create') !!}"><i class="fa fa-plus mr-2"></i>{{trans('lang.add')." ".trans('models/paymentTypes.singular')}}</a> -->
                    </li>
                    @endcan
                    @include('admin.layouts.datatables_toolbar', compact('dataTable'))
                </ul>
            </div>

            <div class="card-body">
                @include('admin.payment_types.table')

                <div class="card-footer clearfix float-right">
                    <div class="float-right">

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection


