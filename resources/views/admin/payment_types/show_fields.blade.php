<!-- Name Field -->
<div class="col-sm-12">
    {!! Form::label('name', __('models/paymentTypes.fields.name').':') !!}
    <p>{{ $paymentType->name }}</p>
</div>

<!-- Description Field -->
<div class="col-sm-12">
    {!! Form::label('description', __('models/paymentTypes.fields.description').':') !!}
    <p>{{ $paymentType->description }}</p>
</div>

<!-- Icon Field -->
<div class="col-sm-12">
    {!! Form::label('icon', __('models/paymentTypes.fields.icon').':') !!}
    <p>{{ $paymentType->icon }}</p>
</div>

<!-- Payment Key Field -->
<div class="col-sm-12">
    {!! Form::label('payment_key', __('models/paymentTypes.fields.payment_key').':') !!}
    <p>{{ $paymentType->payment_key }}</p>
</div>

<!-- Payment Secret Field -->
<div class="col-sm-12">
    {!! Form::label('payment_secret', __('models/paymentTypes.fields.payment_secret').':') !!}
    <p>{{ $paymentType->payment_secret }}</p>
</div>

<!-- Created At Field -->
<div class="col-sm-12">
    {!! Form::label('created_at', __('models/paymentTypes.fields.created_at').':') !!}
    <p>{{ $paymentType->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="col-sm-12">
    {!! Form::label('updated_at', __('models/paymentTypes.fields.updated_at').':') !!}
    <p>{{ $paymentType->updated_at }}</p>
</div>

