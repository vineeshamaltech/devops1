{!! Form::open(['route' => ['admin.paymentTypes.destroy', $id], 'method' => 'delete']) !!}
<div class='btn-group'>
    @can('paymentMethods.view')
    <a href="{{ route('admin.paymentTypes.show', $id) }}" class='btn btn-default btn-xs'>
        <i class="fa fa-eye"></i>
    </a>
    @endcan
    @can('paymentMethods.edit')
    <a href="{{ route('admin.paymentTypes.edit', $id) }}" class='btn btn-default btn-xs'>
        <i class="fa fa-edit"></i>
    </a>
    @endcan
{{--    @can('paymentMethods.delete')--}}
{{--    {!! Form::button('<i class="fa fa-trash"></i>', [--}}
{{--        'type' => 'submit',--}}
{{--        'class' => 'btn btn-danger btn-xs',--}}
{{--        'onclick' => 'return confirm("'.__('crud.are_you_sure').'")'--}}
{{--    ]) !!}--}}
{{--    @endcan--}}
</div>
{!! Form::close() !!}
