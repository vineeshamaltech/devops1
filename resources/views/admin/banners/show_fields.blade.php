<!-- Store Id Field -->
<div class="col-sm-12">
    {!! Form::label('store_id', __('models/banners.fields.store_id').':') !!}
    <p>{{ $banner->store_id }}</p>
</div>

<!-- Image Field -->
<div class="col-sm-12">
    {!! Form::label('image', __('models/banners.fields.image').':') !!}
    <p>{{ $banner->image }}</p>
</div>

<!-- Title Field -->
<div class="col-sm-12">
    {!! Form::label('title', __('models/banners.fields.title').':') !!}
    <p>{{ $banner->title }}</p>
</div>

<!-- Service Category Id Field -->
<div class="col-sm-12">
    {!! Form::label('service_category_id', __('models/banners.fields.service_category_id').':') !!}
    <p>{{ $banner->service_category_id }}</p>
</div>

<!-- Active Field -->
<div class="col-sm-12">
    {!! Form::label('active', __('models/banners.fields.active').':') !!}
    <p>{{ $banner->active }}</p>
</div>

<!-- Created At Field -->
<div class="col-sm-12">
    {!! Form::label('created_at', __('models/banners.fields.created_at').':') !!}
    <p>{{ $banner->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="col-sm-12">
    {!! Form::label('updated_at', __('models/banners.fields.updated_at').':') !!}
    <p>{{ $banner->updated_at }}</p>
</div>

