<!-- Store Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('type', __('models/banners.fields.type').':',['class'=>'required']) !!}
    <select name="type" id="type" class="form-control select2" @if(!empty($banner)) value="{{ $banner->type }}" @endif required>
        @if(config('constants.features.purchasing_service',false))
            <option value="STORE" @if(!empty($banner) && $banner->type == 'STORE'   ) selected @endif  >Store</option>
            <option value="CATEGORY"  @if(!empty($banner) && $banner->type == 'CATEGORY'   ) selected @endif >Category</option>
        @endif
        <option value="URL"  @if(!empty($banner) && $banner->type == 'URL'   ) selected @endif >URL</option>
        @if(config('constants.features.home_service_booking',false))
            <option value="SERVICE"  @if(!empty($banner) && $banner->type == 'SERVICE'   ) selected @endif >Home Service</option>
        @endif
    </select>
</div>

<!-- Store Id Field -->

<div class="form-group col-sm-6 home_service_category_id">
    {!! Form::label('home_service_category_id', __('models/banners.fields.home_service_category').':',['class'=>'required']) !!}
    <select name="home_service_category_id" class="form-control select2">
        @foreach(\App\Models\HomeServiceCategory::where('parent',0)->get() as $category)
            <option value="{{ $category->id }}" @if(!empty($banner) && $banner->home_service_category_id == $category->id) selected @endif >{{ $category->name }}</option>
        @endforeach
    </select>
</div>

<!-- Store Id Field -->
<div class="form-group col-sm-6 store_id">
    {!! Form::label('store_id', __('models/banners.fields.store_id').':',['class'=>'required']) !!}
    <select name="store_id" class="form-control select2" required>
        @if(Helpers::is_admin() || Helpers::is_super_admin())
            <option value="0">Select a store</option>
        @endif
        @foreach($stores as $store)
            <option value="{{ $store->id }}" @if(!empty($banner) && $banner->store_id == $store->id) selected @endif >{{ $store->name }}</option>
        @endforeach
    </select>
</div>

<!-- Image Field -->
<div class="form-group col-sm-6">
    {!! Form::label('image', __('models/banners.fields.image').':') !!}
    <div class="input-group">
        <div class="custom-file">
            {!! Form::file('image', ['class' => 'custom-file-input']) !!}
            {!! Form::label('image', 'Choose file', ['class' => 'custom-file-label']) !!}
        </div>
    </div>
</div>
<div class="clearfix"></div>


<!-- Title Field -->
<div class="form-group col-sm-6">
    {!! Form::label('title', __('models/banners.fields.title').':') !!}
    {!! Form::text('title', null, ['class' => 'form-control']) !!}
</div>

<!-- Zone Field -->
<div class="form-group col-sm-6">
    {!! Form::label('zones', __('models/banners.fields.zones').':',['class'=>'required']) !!}
    <select name="zones[]" class="form-control select2" multiple>
        @foreach($zones as $zone)
            <option value="{{ $zone->id }}" @if(!empty($banner) && $zone->selected) selected @endif >{{ $zone->title }}</option>
        @endforeach
    </select>
</div>

<!-- Service Category Id Field -->
<div class="form-group col-sm-6 service_category_id">
    {!! Form::label('service_category_id', __('models/banners.fields.service_category_id').':',['class'=>'required']) !!}
    <select name="service_category_id" class="form-control select2" required>
        <option value="0">Static Banner</option>
        @foreach(\App\Models\ServiceCategory::where('active',1)->get() as $serviceCategory)
            <option value="{{ $serviceCategory->id }}" @if(!empty($banner) && $banner->service_category_id == $serviceCategory->id) selected @endif >{{ $serviceCategory->name }}</option>
        @endforeach
    </select>
</div>


<!-- Title Field -->
<div class="form-group col-sm-6 url">
    {!! Form::label('url', __('models/banners.fields.url').':') !!}
    {!! Form::text('url', null, ['class' => 'form-control']) !!}
</div>

<!-- Active Field -->
<div class="form-group col-sm-6">
    <div class="form-check">
        {!! Form::hidden('active', 0, ['class' => 'form-check-input']) !!}
        {!! Form::checkbox('active', '1', null, ['class' => 'form-check-input']) !!}
        {!! Form::label('active', __('models/banners.fields.active'), ['class' => 'form-check-label']) !!}
    </div>
</div>

<!-- top picks enabled field  -->
<div class="form-group col-sm-6">
    <div class="form-check">
        {!! Form::hidden('top_picks_enabled', 0, ['class' => 'form-check-input']) !!}
        {!! Form::checkbox('top_picks_enabled', '1', null, ['class' => 'form-check-input']) !!}
        {!! Form::label('top_picks_enabled', 'Top Picks Enabled', ['class' => 'form-check-label']) !!}
    </div>


@push('third_party_scripts')
<script type="text/javascript">
    $(document).ready(function(){
        $('#type').on('change',function (){
           var type = $(this).val();
           if(type === 'STORE'){
               $('.store_id').show();
               $('.url').hide();
               $('.service_category_id').hide();
               $('.home_service_category_id').hide();
           }else if(type === 'CATEGORY'){
               $('.store_id').hide();
               $('.url').hide();
               $('.service_category_id').show();
               $('.home_service_category_id').hide();
           }else if(type === 'URL'){
               $('.store_id').hide();
               $('.url').show();
               $('.service_category_id').hide();
               $('.home_service_category_id').hide();
           }else if(type === 'SERVICE'){
               $('.store_id').hide();
               $('.url').hide();
               $('.service_category_id').hide();
               $('.home_service_category_id').show();
           }
        }).change();
    });
</script>
@endpush
