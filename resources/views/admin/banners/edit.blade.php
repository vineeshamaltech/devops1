@extends('admin.layouts.app')

@section('title',trans('lang.edit')." ".trans('models/banners.singular'))

@section('content')
    <div class="content">

        @include('flash::message')

        <div class="card">

            {!! Form::model($banner, ['route' => ['admin.banners.update', $banner->id], 'method' => 'patch', 'files' => true]) !!}

            <div class="card-body">
                <div class="row">
                    @include('admin.banners.fields')
                </div>
            </div>

            <div class="card-footer">
                {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
                <a href="{{ route('admin.banners.index') }}" class="btn btn-default">
                    @lang('lang.cancel')
                 </a>
            </div>

            {!! Form::close() !!}

        </div>
    </div>
@endsection
