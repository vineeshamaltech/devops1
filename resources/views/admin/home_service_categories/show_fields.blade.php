<!-- Id Field -->
<div class="col-sm-12">
    {!! Form::label('id', __('models/homeServiceCategories.fields.id').':') !!}
    <p>{{ $homeServiceCategory->id }}</p>
</div>

<!-- Name Field -->
<div class="col-sm-12">
    {!! Form::label('name', __('models/homeServiceCategories.fields.name').':') !!}
    <p>{{ $homeServiceCategory->name }}</p>
</div>

<!-- Icon Field -->
<div class="col-sm-12">
    {!! Form::label('icon', __('models/homeServiceCategories.fields.icon').':') !!}
    <p>{{ $homeServiceCategory->icon }}</p>
</div>

<!-- Background Image Field -->
<div class="col-sm-12">
    {!! Form::label('background_image', __('models/homeServiceCategories.fields.background_image').':') !!}
    <p>{{ $homeServiceCategory->background_image }}</p>
</div>

<!-- View Field -->
<div class="col-sm-12">
    {!! Form::label('view', __('models/homeServiceCategories.fields.view').':') !!}
    <p>{{ $homeServiceCategory->view }}</p>
</div>

<!-- Parent Field -->
<div class="col-sm-12">
    {!! Form::label('parent', __('models/homeServiceCategories.fields.parent').':') !!}
    <p>{{ $homeServiceCategory->parent }}</p>
</div>

<!-- Tax Field -->
<div class="col-sm-12">
    {!! Form::label('tax', __('models/homeServiceCategories.fields.tax').':') !!}
    <p>{{ $homeServiceCategory->tax }}</p>
</div>

<!-- Created At Field -->
<div class="col-sm-12">
    {!! Form::label('created_at', __('models/homeServiceCategories.fields.created_at').':') !!}
    <p>{{ $homeServiceCategory->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="col-sm-12">
    {!! Form::label('updated_at', __('models/homeServiceCategories.fields.updated_at').':') !!}
    <p>{{ $homeServiceCategory->updated_at }}</p>
</div>

