@extends('admin.layouts.app')

@section('title',trans('lang.edit')." ".trans('models/homeServiceCategories.singular'))

@section('content')
    <div class="content">

        <!--@include('adminlte-templates::common.errors')-->

        <div class="card">

            {!! Form::model($homeServiceCategory, ['route' => ['admin.homeServiceCategories.update', $homeServiceCategory->id], 'method' => 'patch','files'=>true]) !!}

            <div class="card-body">
                <div class="row">
                    @include('admin.home_service_categories.fields')
                </div>
            </div>

            <div class="card-footer">
                {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
                <a href="{{ route('admin.homeServiceCategories.index') }}" class="btn btn-default">
                    @lang('lang.cancel')
                 </a>
            </div>

            {!! Form::close() !!}

        </div>
    </div>
@endsection
