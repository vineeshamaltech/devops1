{!! Form::open(['route' => ['admin.homeServiceCategories.destroy', $id], 'method' => 'delete']) !!}
<div class='btn-group'>
    @can('homeServiceCategories.view')
    <a href="{{ route('admin.homeServiceCategories.show', $id) }}" class='btn btn-default btn-xs'>
        <i class="fa fa-eye"></i>
    </a>
    @endcan
    @can('homeServiceCategories.edit')
    <a href="{{ route('admin.homeServiceCategories.edit', $id) }}" class='btn btn-default btn-xs'>
        <i class="fa fa-edit"></i>
    </a>
    @endcan
    @can('homeServiceCategories.delete')
    {!! Form::button('<i class="fa fa-trash"></i>', [
        'type' => 'submit',
        'class' => 'btn btn-danger btn-xs',
        'onclick' => 'return confirm("'.__('lang.are_you_sure').'")'
    ]) !!}
    @endcan
</div>
{!! Form::close() !!}
