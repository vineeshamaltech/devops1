@extends('admin.layouts.app')

@section('title',trans('lang.edit')." ".trans('models/addresses.singular'))

@section('content')
    <div class="content">

        <!--@include('adminlte-templates::common.errors')-->

        <div class="card">

            {!! Form::model($address, ['route' => ['admin.addresses.update', $address->id], 'method' => 'patch']) !!}

            <div class="card-body">
                <div class="row">
                    @include('admin.addresses.fields')
                </div>
            </div>

            <div class="card-footer">
                {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
                <a href="{{ route('admin.addresses.index') }}" class="btn btn-default">
                    @lang('lang.cancel')
                 </a>
            </div>

            {!! Form::close() !!}

        </div>
    </div>
@endsection