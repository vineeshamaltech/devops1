<!-- User Id Field -->
<div class="col-sm-12">
    {!! Form::label('user_id', __('models/addresses.fields.user_id').':') !!}
    <p>{{ $address->user_id }}</p>
</div>

<!-- Name Field -->
<div class="col-sm-12">
    {!! Form::label('name', __('models/addresses.fields.name').':') !!}
    <p>{{ $address->name }}</p>
</div>

<!-- Phone Field -->
<div class="col-sm-12">
    {!! Form::label('phone', __('models/addresses.fields.phone').':') !!}
    <p>{{ $address->phone }}</p>
</div>

<!-- Address Field -->
<div class="col-sm-12">
    {!! Form::label('address', __('models/addresses.fields.address').':') !!}
    <p>{{ $address->address }}</p>
</div>

<!-- Long Field -->
<div class="col-sm-12">
    {!! Form::label('long', __('models/addresses.fields.long').':') !!}
    <p>{{ $address->long }}</p>
</div>

<!-- Description Field -->
<div class="col-sm-12">
    {!! Form::label('description', __('models/addresses.fields.description').':') !!}
    <p>{{ $address->description }}</p>
</div>

<!-- Is Default Field -->
<div class="col-sm-12">
    {!! Form::label('is_default', __('models/addresses.fields.is_default').':') !!}
    <p>{{ $address->is_default }}</p>
</div>

<!-- Created At Field -->
<div class="col-sm-12">
    {!! Form::label('created_at', __('models/addresses.fields.created_at').':') !!}
    <p>{{ $address->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="col-sm-12">
    {!! Form::label('updated_at', __('models/addresses.fields.updated_at').':') !!}
    <p>{{ $address->updated_at }}</p>
</div>

