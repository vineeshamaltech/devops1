{!! Form::open(['route' => ['admin.addresses.destroy', $id], 'method' => 'delete']) !!}
<div class='btn-group'>
    @can('addresses.view')
    <a href="{{ route('admin.addresses.show', $id) }}" class='btn btn-default btn-xs'>
        <i class="fa fa-eye"></i>
    </a>
    @endcan
    @can('addresses.edit')
    <a href="{{ route('admin.addresses.edit', $id) }}" class='btn btn-default btn-xs'>
        <i class="fa fa-edit"></i>
    </a>
    @endcan
    @can('addresses.delete')
    {!! Form::button('<i class="fa fa-trash"></i>', [
        'type' => 'submit',
        'class' => 'btn btn-danger btn-xs',
        'onclick' => 'return confirm("'.__('crud.are_you_sure').'")'
    ]) !!}
    @endcan
</div>
{!! Form::close() !!}
