{!! Form::open(['route' => ['admin.driverWalletTransactions.destroy', $id], 'method' => 'delete']) !!}
<div class='btn-group'>
    @can('driverWalletTransactions.view')
    <a href="{{ route('admin.driverWalletTransactions.show', $id) }}" class='btn btn-default btn-xs'>
        <i class="fa fa-eye"></i>
    </a>
    @endcan
    <!-- @can('driverWalletTransactions.edit')
    <a href="{{ route('admin.driverWalletTransactions.edit', $id) }}" class='btn btn-default btn-xs'>
        <i class="fa fa-edit"></i>
    </a>
    @endcan -->
    <!-- @can('driverWalletTransactions.delete')
    {!! Form::button('<i class="fa fa-trash"></i>', [
        'type' => 'submit',
        'class' => 'btn btn-danger btn-xs',
        'onclick' => 'return confirm("'.__('crud.are_you_sure').'")'
    ]) !!}
    @endcan -->
</div>
{!! Form::close() !!}
