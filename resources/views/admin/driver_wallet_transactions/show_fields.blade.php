<!-- User Id Field -->
<div class="col-sm-12">
    {!! Form::label('driver_id', __('models/driverWalletTransactions.fields.driver_id').':') !!}
    <p>{{ $driverWalletTransaction->driver_id }}</p>
</div>

<!-- Type Field -->
<div class="col-sm-12">
    {!! Form::label('type', __('models/driverWalletTransactions.fields.type').':') !!}
    <p>{{ $driverWalletTransaction->type }}</p>
</div>

<!-- Amount Field -->
<div class="col-sm-12">
    {!! Form::label('amount', __('models/driverWalletTransactions.fields.amount').':') !!}
    <p>{{ $driverWalletTransaction->amount }}</p>
</div>

<!-- Remarks Field -->
<div class="col-sm-12">
    {!! Form::label('remarks', __('models/driverWalletTransactions.fields.remarks').':') !!}
    <p>{{ $driverWalletTransaction->remarks }}</p>
</div>

<!-- Created At Field -->
<div class="col-sm-12">
    {!! Form::label('created_at', __('models/driverWalletTransactions.fields.created_at').':') !!}
    <p>{{ $driverWalletTransaction->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="col-sm-12">
    {!! Form::label('updated_at', __('models/driverWalletTransactions.fields.updated_at').':') !!}
    <p>{{ $driverWalletTransaction->updated_at }}</p>
</div>

