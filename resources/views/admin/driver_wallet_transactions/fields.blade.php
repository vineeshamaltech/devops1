<!-- User Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('driver_id', __('models/driverWalletTransactions.fields.driver_id').':') !!}
    <select name="driver_id" class="form-control select2" required>
        <option value="">Select </option>
        <option value="-1">All Drivers</option>
        @foreach(\App\Models\Driver::select('id','name','mobile')->get() as $driver)
            <option value="{{ $driver->id }}" @if(isset($driverWalletTransaction) && $driverWalletTransaction->driver_id == $driver->id) selected @endif>{{ $driver->name }}</option>
        @endforeach
    </select>
</div>

<!-- Type Field -->
<div class="form-group col-sm-6">
    {!! Form::label('type', __('models/driverWalletTransactions.fields.type').':') !!}
    {!! Form::select('type', ['CREDIT'=>'Credit','DEBIT'=>'Debit'], null, ['class' => 'form-control select2']) !!}
</div>

<!-- Type Field -->
<div class="form-group col-sm-6">
    {!! Form::label('amount', __('models/driverWalletTransactions.fields.amount').':') !!}
    {!! Form::number('amount', null, ['class' => 'form-control select2']) !!}
</div>


<!-- Remarks Field -->
<div class="form-group col-sm-6">
    {!! Form::label('remarks', __('models/driverWalletTransactions.fields.remarks').':') !!}
    {!! Form::text('remarks', null, ['class' => 'form-control']) !!}
</div>
