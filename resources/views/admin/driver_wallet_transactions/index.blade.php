@extends('admin.layouts.app')
@section('title','Driver Wallet Transactions')
@section('content')
    <div class="content">

        @include('flash::message')

        <div class="clearfix"></div>

        <div class="card">
            <div class="card-header">
                <ul class="nav nav-tabs align-items-end card-header-tabs w-100">
                    <li class="nav-item">
                        <a class="nav-link active" href="{!! url()->current() !!}"><i class="fa fa-list mr-2"></i>{{trans('models/driverWalletTransactions.plural')}}</a>
                    </li>
                    @can('driverWalletTransactions.create')
                    <li class="nav-item">
                        <a class="nav-link" href="{!! route('admin.driverWalletTransactions.create') !!}"><i class="fa fa-plus mr-2"></i>{{trans('lang.add')." ".trans('models/driverWalletTransactions.singular')}}</a>
                    </li>
                    @endcan
                    @include('admin.layouts.datatables_toolbar', compact('dataTable'))
                </ul>
            </div>

            <div class="card-body">
                
                {!! Form::open(['route' => ['admin.destroy_multiple_driverWalletTransactions'], 'method' => 'post']) !!}
                    <!-- <button type="submit" class="btn btn-danger deleteMultiple" onclick="return confirm('Are you sure?')">Delete Multiple</button> -->
                    <hr>
                    @include('admin.driver_wallet_transactions.table')
                {!! Form::close() !!}
                <div class="card-footer clearfix float-right">
                    <div class="float-right">

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection


