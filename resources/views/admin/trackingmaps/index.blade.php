@extends('admin.layouts.app')
@section('title','Drivers Tracking')
@section('content')
    <div class="content">

        @include('flash::message')

        <div class="clearfix"></div>
        
      
   <div class="col-lg-4">
                        @can('trackingmaps.index')
                            <form action="" method="GET">
                            <label>Filter By Zone : </label>
                            <select name="zones" id="zones" class="select2">
                                <option value="">All</option>
                                @foreach($zones as $zone)
                                <option value="{{$zone->id}}" @if(request('zones') == $zone->id) selected @endif>{{$zone->title}}</option>
                                @endforeach
                            </select>
                            <button type="submit" class="btn sm btn-primary">Search</button>
                            </form>
                        @endcan
                    </div>
        <div class="card">
    

  

            

                <div id="map" style="width:100%;height:400px;"></div>

 
               


            </div>
        </div>
    </div>

@endsection






@push('page_scripts')

<script>
var locations = <?php echo json_encode($locations); ?>;
let map;
console.log(locations[0].latitude);
    function initMap() {
         map = new google.maps.Map(document.getElementById('map'), {
                    zoom: 14,
                center: new google.maps.LatLng(15.896622, 80.460434),
                mapTypeId: google.maps.MapTypeId.ROADMAP
            });
            var infowindow = new google.maps.InfoWindow();
            var marker, i;
            for (i = 0; i < locations.length; i++) {
                marker = new google.maps.Marker({
                    position: new google.maps.LatLng(locations[i].latitude, locations[i].longitude),
                    title: locations[i].name,
                    // label:locations[i].name,
                    map: map
                });
                google.maps.event.addListener(marker, 'click', (function (marker, i) {
                    return function () {
                        infowindow.setContent(locations[i].name);
                        infowindow.open(map, marker);
                    }
                })(marker, i));
            }
        }
initMap();
</script>
<script
      src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB58mWCt-krVJrgEtp-fWDKxUXCI80-aL4&callback=initMap&v=weekly" async defer
    ></script>
@endpush