@extends('admin.layouts.app')
@section('title','Home Services')
@section('content')
    <div class="content">

        @include('flash::message')

        <div class="clearfix"></div>

        <div class="card">
            <div class="card-header">
                <ul class="nav nav-tabs align-items-end card-header-tabs w-100">
                    <li class="nav-item">
                        <a class="nav-link active" href="{!! url()->current() !!}"><i class="fa fa-list mr-2"></i>{{trans('models/homeServices.plural')}}</a>
                    </li>
                    @can('homeServices.create')
                    <li class="nav-item">
                        <a class="nav-link" href="{!! route('admin.homeServices.create') !!}"><i class="fa fa-plus mr-2"></i>{{trans('lang.add')." ".trans('models/homeServices.singular')}}</a>
                    </li>
                    @endcan
                    @include('admin.layouts.datatables_toolbar', compact('dataTable'))
                </ul>
            </div>

            <div class="card-body">
                @include('admin.home_services.table')

                <div class="card-footer clearfix float-right">
                    <div class="float-right">

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection


