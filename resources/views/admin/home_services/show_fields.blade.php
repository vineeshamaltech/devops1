<!-- Name Field -->
<div class="col-sm-12">
    {!! Form::label('name', __('models/homeServices.fields.name').':') !!}
    <p>{{ $homeService->name }}</p>
</div>

<!-- Description Field -->
<div class="col-sm-12">
    {!! Form::label('description', __('models/homeServices.fields.description').':') !!}
    <p>{{ $homeService->description }}</p>
</div>

<!-- Image Field -->
<div class="col-sm-12">
    {!! Form::label('image', __('models/homeServices.fields.image').':') !!}
    <p>{{ $homeService->image }}</p>
</div>

<!-- Original Price Field -->
<div class="col-sm-12">
    {!! Form::label('original_price', __('models/homeServices.fields.original_price').':') !!}
    <p>{{ $homeService->original_price }}</p>
</div>

<!-- Discount Price Field -->
<div class="col-sm-12">
    {!! Form::label('discount_price', __('models/homeServices.fields.discount_price').':') !!}
    <p>{{ $homeService->discount_price }}</p>
</div>

<!-- Created At Field -->
<div class="col-sm-12">
    {!! Form::label('created_at', __('models/homeServices.fields.created_at').':') !!}
    <p>{{ $homeService->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="col-sm-12">
    {!! Form::label('updated_at', __('models/homeServices.fields.updated_at').':') !!}
    <p>{{ $homeService->updated_at }}</p>
</div>

