@extends('admin.layouts.app')

@section('title',trans('lang.add_new')." ".trans('models/homeServices.singular'))

@section('content')
    <div class="content">

        <!--@include('adminlte-templates::common.errors')-->

        <div class="card">

            {!! Form::open(['route' => 'admin.homeServices.store', 'files' => true]) !!}

            <div class="card-body">
                <div class="row">
                    @include('admin.home_services.fields')
                </div>
            </div>

            <div class="card-footer">
                {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
                <a href="{{ route('admin.homeServices.index') }}" class="btn btn-default">
                 @lang('lang.cancel')
                </a>
            </div>

            {!! Form::close() !!}

        </div>
    </div>
@endsection
