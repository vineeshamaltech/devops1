{!! Form::open(['route' => ['admin.homeServices.destroy', $id], 'method' => 'delete']) !!}
<div class='btn-group'>
    @can('homeServices.view')
    <a href="{{ route('admin.homeServices.show', $id) }}" class='btn btn-default btn-xs'>
        <i class="fa fa-eye"></i>
    </a>
    @endcan
    @can('homeServices.edit')
    <a href="{{ route('admin.homeServices.edit', $id) }}" class='btn btn-default btn-xs'>
        <i class="fa fa-edit"></i>
    </a>
    @endcan
    @can('homeServices.delete')
    {!! Form::button('<i class="fa fa-trash"></i>', [
        'type' => 'submit',
        'class' => 'btn btn-danger btn-xs',
        'onclick' => 'return confirm("'.__('crud.are_you_sure').'")'
    ]) !!}
    @endcan
    @can('homeServices.timings')
    <a href="{{ route('admin.homeService.timing', $id) }}" class='btn btn-default btn-xs'>
        <i class="fa fa-clock"></i>
    </a>
    @endcan
</div>
{!! Form::close() !!}
