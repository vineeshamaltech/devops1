<!-- Order Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('order_id', __('models/reviews.fields.order_id').':',['class'=>'required']) !!}
    <select name="order_id" class="form-control select2">
        @foreach($orders as $order)
        <option @if(!empty($review) && $review->order_id == $order->id) selected @endif value="{{ $order->id }}">#{{ $order->id }}</option>
        @endforeach
    </select>
</div>
<!-- Order Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('driver_id', __('models/reviews.fields.driver').':',['class'=>'required']) !!}
    <select name="driver_id" class="form-control select2">
        @foreach($drivers as $driver)
        <option @if(!empty($review) && $review->driver_id == $driver->id) selected @endif value="{{ $driver->id }}">{{ $driver->name }}-{{ $driver->mobile }}</option>
        @endforeach
    </select>
</div>
<!-- Store Rating Field -->
<div class="form-group col-sm-6">
    {!! Form::label('store_rating', __('models/reviews.fields.store_rating')."(".__('models/reviews.fields.top_rating').")".':') !!}
    {!! Form::number('store_rating', null, ['class' => 'form-control']) !!}
</div>

<!-- Driver Rating Field -->
<div class="form-group col-sm-6">
    {!! Form::label('driver_rating', __('models/reviews.fields.driver_rating')."(".__('models/reviews.fields.top_rating').")".':') !!}
    {!! Form::number('driver_rating', null, ['class' => 'form-control']) !!}
</div>

<!-- Store Review Field -->
<div class="form-group col-sm-6">
    {!! Form::label('store_review', __('models/reviews.fields.store_review').':') !!}
    {!! Form::textarea('store_review', null, ['class' => 'form-control']) !!}
</div>

<!-- Driver Review Field -->
<div class="form-group col-sm-6">
    {!! Form::label('driver_review', __('models/reviews.fields.driver_review').':') !!}
    {!! Form::textarea('driver_review', null, ['class' => 'form-control']) !!}
</div>