@extends('admin.layouts.app')

@section('title',trans('lang.edit')." ".trans('models/reviews.singular'))

@section('content')
    <div class="content">

        @include('adminlte-templates::common.errors')

        <div class="card">

            {!! Form::model($review, ['route' => ['admin.reviews.update', $review->id], 'method' => 'patch']) !!}

            <div class="card-body">
                <div class="row">
                    @include('admin.reviews.fields')
                </div>
            </div>

            <div class="card-footer">
                {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
                <a href="{{ route('admin.reviews.index') }}" class="btn btn-default">
                    @lang('lang.cancel')
                 </a>
            </div>

            {!! Form::close() !!}

        </div>
    </div>
@endsection