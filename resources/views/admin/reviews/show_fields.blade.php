<!-- Order Id Field -->
<div class="col-sm-3">
    {!! Form::label('order_id', __('models/reviews.fields.order_id').':') !!}
    <p>#{{ $review->order_id }}</p>
</div>

<!-- Store Rating Field -->
<div class="col-sm-3">
    {!! Form::label('store_rating', __('models/reviews.fields.store_rating').':') !!}
    <p>{{ $review->store_rating }}</p>
</div>

<!-- Store Review Field -->
<div class="col-sm-3">
    {!! Form::label('store_review', __('models/reviews.fields.store_review').':') !!}
    <p>{{ $review->store_review }}</p>
</div>

<!-- Driver Rating Field -->
<div class="col-sm-3">
    {!! Form::label('driver', __('models/reviews.fields.driver').':') !!}
    <p>{{ $review->driver->name }}</p>
</div>

<!-- Driver Rating Field -->
<div class="col-sm-3">
    {!! Form::label('driver_rating', __('models/reviews.fields.driver_rating').':') !!}
    <p>{{ $review->driver_rating }}</p>
</div>

<!-- Driver Review Field -->
<div class="col-sm-3">
    {!! Form::label('driver_review', __('models/reviews.fields.driver_review').':') !!}
    <p>{{ $review->driver_review }}</p>
</div>

<!-- Created At Field -->
<div class="col-sm-3">
    {!! Form::label('created_at', __('models/reviews.fields.created_at').':') !!}
    <p>{{ $review->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="col-sm-3">
    {!! Form::label('updated_at', __('models/reviews.fields.updated_at').':') !!}
    <p>{{ $review->updated_at }}</p>
</div>

