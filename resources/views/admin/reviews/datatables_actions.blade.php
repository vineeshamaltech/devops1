{!! Form::open(['route' => ['admin.reviews.destroy', $id], 'method' => 'delete']) !!}
<div class='btn-group'>
    @can('reviews.view')
    <a href="{{ route('admin.reviews.show', $id) }}" class='btn btn-default btn-xs'>
        <i class="fa fa-eye"></i>
    </a>
    @endcan
    @can('reviews.edit')
    <a href="{{ route('admin.reviews.edit', $id) }}" class='btn btn-default btn-xs'>
        <i class="fa fa-edit"></i>
    </a>
    @endcan
    @can('reviews.delete')
    {!! Form::button('<i class="fa fa-trash"></i>', [
        'type' => 'submit',
        'class' => 'btn btn-danger btn-xs',
        'onclick' => 'return confirm("'.__('crud.are_you_sure').'")'
    ]) !!}
    @endcan
</div>
{!! Form::close() !!}
