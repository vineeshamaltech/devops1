@extends('admin.layouts.app')
@section('title',trans('models/serviceCategories.plural'))
@section('content')
    <div class="content">

        @include('flash::message')

        <div class="clearfix"></div>

        <div class="card">
            <div class="card-header">
                <ul class="nav nav-tabs align-items-end card-header-tabs w-100">
                    <li class="nav-item">
                        <a class="nav-link active" href="{!! url()->current() !!}"><i class="fa fa-list mr-2"></i>{{trans('models/serviceCategories.plural')}}</a>
                    </li>
                    @can('serviceCategories.create')
                    <li class="nav-item">
                        <a class="nav-link" href="{!! route('admin.serviceCategories.create') !!}"><i class="fa fa-plus mr-2"></i>{{trans('lang.add')." ".trans('models/serviceCategories.singular')}}</a>
                    </li>
                    @endcan
                    @include('admin.layouts.datatables_toolbar', compact('dataTable'))
                </ul>
            </div>

            <div class="card-body">
                @include('admin.service_categories.table')

                <div class="card-footer clearfix float-right">
                    <div class="float-right">

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection


