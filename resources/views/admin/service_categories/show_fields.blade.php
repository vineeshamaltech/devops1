<!-- Icon Field -->
<div class="col-sm-12">
    {!! Form::label('icon', __('models/serviceCategories.fields.icon').':') !!}
    <p>{{ $serviceCategory->icon }}</p>
</div>

<!-- Bg Image Field -->
<div class="col-sm-12">
    {!! Form::label('bg_image', __('models/serviceCategories.fields.bg_image').':') !!}
    <p>{{ $serviceCategory->bg_image }}</p>
</div>

<!-- Home View Field -->
<div class="col-sm-12">
    {!! Form::label('home_view', __('models/serviceCategories.fields.home_view').':') !!}
    <p>{{ $serviceCategory->home_view }}</p>
</div>

<!-- Name Field -->
<div class="col-sm-12">
    {!! Form::label('name', __('models/serviceCategories.fields.name').':') !!}
    <p>{{ $serviceCategory->name }}</p>
</div>

<!-- Description Field -->
<div class="col-sm-12">
    {!! Form::label('description', __('models/serviceCategories.fields.description').':') !!}
    <p>{{ $serviceCategory->description }}</p>
</div>

<!-- Type Field -->
<div class="col-sm-12">
    {!! Form::label('type', __('models/serviceCategories.fields.type').':') !!}
    <p>{{ $serviceCategory->type }}</p>
</div>

<!-- Discount Field -->
<div class="col-sm-12">
    {!! Form::label('discount', __('models/serviceCategories.fields.discount').':') !!}
    <p>{{ $serviceCategory->discount }}</p>
</div>

<!-- Commission Field -->
<div class="col-sm-12">
    {!! Form::label('commission', __('models/serviceCategories.fields.commission').':') !!}
    <p>{{ $serviceCategory->commission }}</p>
</div>

<!-- Vehicles Field -->
<div class="col-sm-12">
    {!! Form::label('vehicles', __('models/serviceCategories.fields.vehicles').':') !!}
    <p>{{ $serviceCategory->vehicles }}</p>
</div>

<!-- Min Price Field -->
<div class="col-sm-12">
    {!! Form::label('min_price', __('models/serviceCategories.fields.min_price').':') !!}
    <p>{{ $serviceCategory->min_price }}</p>
</div>

<!-- Driver Radius Field -->
<div class="col-sm-12">
    {!! Form::label('driver_radius', __('models/serviceCategories.fields.driver_radius').':') !!}
    <p>{{ $serviceCategory->driver_radius }}</p>
</div>

<!-- Base Distance Field -->
<div class="col-sm-12">
    {!! Form::label('base_distance', __('models/serviceCategories.fields.base_distance').':') !!}
    <p>{{ $serviceCategory->base_distance }}</p>
</div>

<!-- Base Price Field -->
<div class="col-sm-12">
    {!! Form::label('base_price', __('models/serviceCategories.fields.base_price').':') !!}
    <p>{{ $serviceCategory->base_price }}</p>
</div>

<!-- Base Km Field -->
<div class="col-sm-12">
    {!! Form::label('base_km', __('models/serviceCategories.fields.base_km').':') !!}
    <p>{{ $serviceCategory->base_km }}</p>
</div>

<!-- Price Km Field -->
<div class="col-sm-12">
    {!! Form::label('price_km', __('models/serviceCategories.fields.price_km').':') !!}
    <p>{{ $serviceCategory->price_km }}</p>
</div>

<!-- Max Order Distance Field -->
<div class="col-sm-12">
    {!! Form::label('max_order_distance', __('models/serviceCategories.fields.max_order_distance').':') !!}
    <p>{{ $serviceCategory->max_order_distance }}</p>
</div>

<!-- Payment Methods Field -->
<div class="col-sm-12">
    {!! Form::label('payment_methods', __('models/serviceCategories.fields.payment_methods').':') !!}
    <p>{{ $serviceCategory->payment_methods }}</p>
</div>

<!-- Active Field -->
<div class="col-sm-12">
    {!! Form::label('active', __('models/serviceCategories.fields.active').':') !!}
    <p>{{ $serviceCategory->active }}</p>
</div>

