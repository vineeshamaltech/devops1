<!-- Home View Field -->
<div class="form-group col-sm-6">
    {!! Form::label('home_view', __('models/serviceCategories.fields.home_view').':',['class'=>'required']) !!}
    {!! Form::select('home_view', ['BANNER'=>'Banner','CARD'=>'Card View','BACKGROUND'=>'Background'], null, ['class' => 'form-control select2']) !!}
</div>

<!-- Icon Field -->
<div class="form-group col-sm-6 icon_img">
    {!! Form::label('icon', __('models/serviceCategories.fields.icon').':',['class'=>'required']) !!}
    <div class="input-group">
        <div class="custom-file">
            {!! Form::file('icon', ['class' => 'custom-file-input']) !!}
            {!! Form::label('icon', 'Choose file', ['class' => 'custom-file-label']) !!}
        </div>
    </div>
</div>
<div class="clearfix"></div>


<!-- Bg Image Field -->
<div class="form-group col-sm-6 bg_image">
    {!! Form::label('bg_image', __('models/serviceCategories.fields.bg_image').':') !!}
    <div class="input-group">
        <div class="custom-file">
            {!! Form::file('bg_image', ['class' => 'custom-file-input']) !!}
            {!! Form::label('bg_image', 'Choose file', ['class' => 'custom-file-label']) !!}
        </div>
    </div>
</div>
<div class="clearfix"></div>





<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', __('models/serviceCategories.fields.name').':',['class'=>'required']) !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Description Field -->
<div class="form-group col-sm-6">
    {!! Form::label('description', __('models/serviceCategories.fields.description').':') !!}
    {!! Form::text('description', null, ['class' => 'form-control']) !!}
</div>

<!-- Type Field -->
<div class="form-group col-sm-6">
    {!! Form::label('service_type_id', __('models/serviceCategories.fields.type').':',['class'=>'required']) !!}
    <select name="service_type_id" id="service_type" class="form-control select2" required>
        <option value="">Select</option>
        @foreach(\App\Models\ServiceType::where('active',1)->get() as $type)
            <option @if(!empty($serviceCategory) && $serviceCategory->service_type_id == $type->id) selected @endif value="{{$type->id}}">{{$type->name}}</option>
        @endforeach
    </select>
</div>

<!-- Discount Field -->
<div class="form-group col-sm-6">
    {!! Form::label('discount', __('models/serviceCategories.fields.discount').':') !!}
    {!! Form::number('discount', null, ['class' => 'form-control']) !!}
</div>

<!-- Commission Field -->
<div class="form-group col-sm-6">
    {!! Form::label('commission', __('models/serviceCategories.fields.commission').':') !!}
    {!! Form::number('commission', null, ['class' => 'form-control']) !!}
</div>

<!-- Vehicles Field -->
<div class="form-group col-sm-6">
    {!! Form::label('vehicles', __('models/serviceCategories.fields.vehicles').':') !!}
    <select name="vehicles[]" class="form-control select2" multiple required>
        @foreach(\App\Models\VehicleType::where('active',1)->get() as $type)
            <option @if(!empty($serviceCategory) && in_array($type->id,$vehicles)) selected @endif value="{{$type->id}}">{{$type->name}}</option>
        @endforeach
    </select>
</div>


<!-- Min Price Field -->
<div class="form-group col-sm-6">
    {!! Form::label('min_price', __('models/serviceCategories.fields.min_price').':') !!}
    {!! Form::number('min_price', null, ['class' => 'form-control']) !!}
</div>

<!-- Base Distance Field -->
<div class="form-group col-sm-6 pick_drop_felds" id="bs_dist">
    {!! Form::label('base_distance', __('models/serviceCategories.fields.base_distance').':') !!}
    {!! Form::number('base_distance', null, ['class' => 'form-control']) !!}
</div>

<!-- Base Price Field -->
<div class="form-group col-sm-6 pick_drop_felds" id="bs_price">
    {!! Form::label('base_price', __('models/serviceCategories.fields.base_price').':') !!}
    {!! Form::number('base_price', null, ['class' => 'form-control']) !!}
</div>

<!-- Price Km Field -->
<div class="form-group col-sm-6 pick_drop_felds" id="price_km">
    {!! Form::label('price_km', __('models/serviceCategories.fields.price_km').':') !!}
    {!! Form::number('price_km', null, ['class' => 'form-control']) !!}
</div>

<!-- Base Km Field -->
<div class="form-group col-sm-6 pick_drop_felds" id="tax">
    {!! Form::label('tax', __('models/serviceCategories.fields.tax').':') !!}
    {!! Form::number('tax', null, ['class' => 'form-control']) !!}
</div>

<!-- Driver Radius Field -->
<div class="form-group col-sm-6">
    {!! Form::label('driver_radius', __('models/serviceCategories.fields.driver_radius').':',['class'=>'required']) !!}
    {!! Form::number('driver_radius', null, ['class' => 'form-control']) !!}
</div>

<!-- Max Order Distance Field -->
<div class="form-group col-sm-6">
    {!! Form::label('max_order_distance', __('models/serviceCategories.fields.max_order_distance').':',['class'=>'required']) !!}
    {!! Form::number('max_order_distance', null, ['class' => 'form-control']) !!}
</div>

<!-- Payment Methods Field -->
<div class="form-group col-sm-6">
    {!! Form::label('payment_methods', __('models/serviceCategories.fields.payment_methods').':',['class'=>'required']) !!}
    <select name="payment_methods[]" multiple class="form-control select2" required>
        @foreach(\App\Models\PaymentType::where('active',1)->get() as $type)
            <option @if(!empty($serviceCategory) && in_array($type->id,$payment_methods)) selected @endif value="{{$type->id}}">{{$type->name}}</option>
        @endforeach
    </select>
</div>


<!-- Active Field -->
<div class="form-group col-sm-6">
    <div class="form-check">
        {!! Form::hidden('active', 0, ['class' => 'form-check-input']) !!}
        {!! Form::checkbox('active', '1', null, ['class' => 'form-check-input']) !!}
        {!! Form::label('active', __('models/serviceCategories.fields.active'), ['class' => 'form-check-label']) !!}
    </div>
</div>
@push('third_party_scripts')
    {{-- <script type="text/javascript">
        $(document).ready(function () {

            $('select[name="service_type_id"]').on('change', function () {

                var type = $(this).val();
                if (type === "1") {
                    $('.pick_drop_felds').hide();
                } else {
                    $('.pick_drop_felds').show();
                }
            });

            $('select[name="home_view"]').on('change', function () {
                var type = $(this).val();
                if (type === "BANNER") {
                    $('.bg_image').show();
                    $('.icon_img').show();
                } 
                else if (type === "CARD") {
                    $('.bg_image').hide();
                    $('.icon_img').show();
                } 
                else if (type === "BACKGROUND") {
                    $('.bg_image').show();
                    $('.icon_img').hide();
                } 
            });
            $('select[name="service_type_id"]').change();
            $('select[name="home_view"]').change();
        });
    </script> --}}

<script type="text/javascript">
    $(document).ready(function () {

        $('#service_type').on('change', function () {

            var type = $(this).val();

            if (type === "2" || type === "3") {
                $('#bs_price').show();
                $('#price_km').show();
                $('#bs_dist').show();
                $('#tax').show();
            } else if(type === "4") {
                $('#bs_price').hide();
                $('#price_km').hide();
                $('#bs_dist').hide();
                $('#tax').show();
            }else{
                $('#bs_price').hide();
                $('#price_km').hide();
                $('#bs_dist').hide();
                $('#tax').hide();
            }
        });

        $('select[name="home_view"]').on('change', function () {
                var type = $(this).val();
                if (type === "BANNER") {
                    $('.bg_image').show();
                    $('.icon_img').show();
                } 
                else if (type === "CARD") {
                    $('.bg_image').hide();
                    $('.icon_img').show();
                } 
                else if (type === "BACKGROUND") {
                    $('.bg_image').show();
                    $('.icon_img').hide();
                } 
            });
        $('select[name="service_type_id"]').change();
        $('select[name="home_view"]').change();
    });
</script>




@endpush
