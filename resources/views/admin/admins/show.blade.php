@extends('admin.layouts.app')

@section('title',trans('lang.view')." ".trans('models/admins.singular'))

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>@lang('models/admins.singular')</h1>
                </div>
                <div class="col-sm-6">
                    <a class="btn btn-default float-right"
                       href="{{ route('admin.admins.index') }}">
                         @lang('lang.back')
                    </a>
                </div>
            </div>
        </div>
    </section>

    <div class="content px-3">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    @include('admin.admins.show_fields')
                </div>
            </div>
        </div>
    @if($admin->type == "Vendor")
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">KYC Documents</h3>
                @if(count($admin->kyc) > 0)
                <div class="float-right">
                    <a href="{{route('admin.vendorKycs.approveall',['id'=>$admin->id])}}" type="button" class="btn btn-success btn-sm">Accept All</a>
                    <a href="{{route('admin.vendorKycs.rejectall',['id'=>$admin->id])}}" type="button" class="btn btn-danger btn-sm">Reject All</a>
                </div>
                @endif
            </div>
            <div class="card-body">
                <div class="row">
                    @foreach($admin->kyc as $kycDocument)
                        <div class="col-md-6">
                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-header">
                                        <div class="row justify-content-end">
                                            <div class="col-md-6">
                                                <h3 class="card-title">{{ $kycDocument->doc_type }}</h3>
                                                <small class="text-muted m-1"> (updated {{$kycDocument->updated_at->diffForHumans()}})</small>
                                            </div>
                                            <div class="col-md-6 justify-content-end">
                                                @if($kycDocument->status == 'UPLOADED')
                                                    <div class="float-right"><span class="badge badge-warning">{{ $kycDocument->status }}</span></div>
                                                @elseif($kycDocument->status == "APPROVED")
                                                    <div class="float-right"><span class="badge badge-success">{{ $kycDocument->status }}</span></div>
                                                @elseif($kycDocument->status == "REJECTED")
                                                    <div class="float-right"><span class="badge badge-danger">{{ $kycDocument->status }}</span></div>
                                                @endif
                                            </div>
                                        </div>

                                    </div>
                                    @if($kycDocument->photo)
                                    <div class="card-body">
                                        <a target="_blank" href="{{$kycDocument->photo}}"><img src="{{$kycDocument->photo}}" class="img-fluid" alt="{{$kycDocument->doc_type}}" style="width:100%;height:100%;"></a>
                                    </div>
                                    @else
                                    <div class="card-body">
                                        <a target="_blank" href="{{asset('')}}"><img src="{{asset('storage/icons/no_image.jpg')}}" class="img-fluid" alt="{{$kycDocument->doc_type}}" style="width:50%;height:50%;"></a>
                                    </div>
                                    @endif
                                    <div class="card-footer">
                                        <a href="{{route('admin.vendorKycs.approve',['id'=>$kycDocument->id])}}" type="button" class="btn btn-success btn-sm" data-toggle="tooltip" title="Accept">Accept</a>
                                        <a href="{{route('admin.vendorKycs.reject',['id'=>$kycDocument->id])}}" type="button" class="btn btn-danger btn-sm" data-toggle="tooltip" title="Reject">Reject</a>
                                        
                                        @if($kycDocument->photo)
                                            <a href="{{$kycDocument->photo}}" download="{{$kycDocument->doc_type}}" type="button" class="btn btn-primary btn-sm" data-toggle="tooltip" title="Download"><i class="fa fa-download"></i></a>
                                            <a href="{{route('admin.vendorKycs.delete',['id'=>$kycDocument->id])}}" type="button" class="btn btn-danger btn-sm" data-toggle="tooltip" title="Delete"><i class="fa fa-window-close"></i></a>
                                        @else
                                        {!! Form::open(['route' => array('admin.vendorKycs.storeImage', $kycDocument->id), 'files' => true]) !!}
                                            <input type="file" name="photo"/>
                                            <input type="submit">
                                        {!! Form::close() !!}
                                        @endif
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                </div>
            </div>
        </div>
        @endif
    </div>
@endsection
