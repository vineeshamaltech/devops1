<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', __('models/admins.fields.name').':',['class'=>'required']) !!}
    {!! Form::text('name', null, ['class' => 'form-control','required' => 'true']) !!}
</div>

<!-- Email Field -->
<div class="form-group col-sm-6">
    {!! Form::label('mobile', __('models/admins.fields.mobile').':',['class'=>'required']) !!}
    @if(isset($admin))
    <input type="text" name="mobile" class="form-control" required value="{{$admin->country_code.$admin->mobile}}"/>
    @else
    {!! Form::text('mobile', null, ['class' => 'form-control','required' => 'true','maxlength'=>13,'minlength'=>13]) !!}
    @endif
</div>

<!-- Email Field -->
<!-- <div class="form-group col-sm-6">
    {!! Form::label('mobile', __('models/admins.fields.mobile').':',['class'=>'required']) !!}
    {!! Form::text('mobile', null, ['class' => 'form-control','required' => 'true','maxlength'=>13,'minlength'=>13]) !!}
</div> -->

<!-- Email Field -->
<div class="form-group col-sm-6">
    {!! Form::label('email', __('models/admins.fields.email').':',['class'=>'required']) !!}
    {!! Form::text('email', null, ['class' => 'form-control','required' => 'true']) !!}
</div>

<!-- Password Field -->
<div class="form-group col-sm-6">
    {!! Form::label('password', __('models/admins.fields.password').':') !!}
    {!! Form::password('password', ['class' => 'form-control']) !!}
    
     @if($errors -> first('password'))
    <div class="alert-danger" >{{$errors -> first('password')}}</div>
    @endif
</div>

@if(@$admin->type != 'Super Admin')
<!-- Email Field -->
<div class="form-group col-sm-6">
    {!! Form::label('type', __('models/admins.fields.type').':',['class'=>'required']) !!}
    <select name="type[]" class="form-control select2">
        @foreach(\Spatie\Permission\Models\Role::where('name','!=','Super Admin')->get() as $role)
            <option @if(!empty($admin) && $admin->hasRole($role->name)) selected @endif value="{{$role->name}}">{{$role->name}}</option>
        @endforeach
    </select>
</div>
@endif

@if(auth()->user()->type != 'Franchisee')
<!-- Email Field -->
<div class="form-group col-sm-6">
    {!! Form::label('zone', __('models/admins.fields.zone').':',['class'=>'required']) !!}
    <select name="zones[]" class="form-control select2" multiple required>
        @foreach($zones as $zone)
                <option @if(!empty($admin) && $zone->selected) selected @endif value="{{$zone->id}}">{{$zone->title}}</option>
        @endforeach
    </select>
</div>
@endif
<!-- Active Field -->
<div class="form-group col-sm-6">
    <div class="form-check">
        {!! Form::hidden('active', 0, ['class' => 'form-check-input']) !!}
        {!! Form::checkbox('active', '1', null, ['class' => 'form-check-input']) !!}
        {!! Form::label('active', __('models/admins.fields.active'), ['class' => 'form-check-label']) !!}
    </div>
</div>

<!-- Verified Field -->
<div class="form-group col-sm-6">
    <div class="form-check">
        {!! Form::hidden('verified', 0, ['class' => 'form-check-input']) !!}
        {!! Form::checkbox('verified', '1', null, ['class' => 'form-check-input']) !!}
        {!! Form::label('verified', __('models/admins.fields.verified'), ['class' => 'form-check-label']) !!}
    </div>
</div>

