<!-- Name Field -->
<div class="col-sm-12">
    {!! Form::label('name', __('models/admins.fields.name').':') !!}
    <p>{{ $admin->name }}</p>
</div>

<!-- Email Field -->
<div class="col-sm-12">
    {!! Form::label('email', __('models/admins.fields.email').':') !!}
    <p>{{ $admin->email }}</p>
</div>

<!-- Created At Field -->
<div class="col-sm-12">
    {!! Form::label('created_at', __('models/admins.fields.created_at').':') !!}
    <p>{{ $admin->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="col-sm-12">
    {!! Form::label('updated_at', __('models/admins.fields.updated_at').':') !!}
    <p>{{ $admin->updated_at }}</p>
</div>