{!! Form::open(['route' => ['admin.admins.destroy', $id], 'method' => 'delete']) !!}
<div class='btn-group'>
    @can('admins.view')
    <a href="{{ route('admin.admins.show', $id) }}" class='btn btn-default btn-xs'>
        <i class="fa fa-eye"></i>
    </a>
    @endcan
    @can('admins.edit')
    <a href="{{ route('admin.admins.edit', $id) }}" class='btn btn-default btn-xs'>
        <i class="fa fa-edit"></i>
    </a>
    @endcan
{{--    @can('admins.delete')--}}
{{--    {!! Form::button('<i class="fa fa-trash"></i>', [--}}
{{--        'type' => 'submit',--}}
{{--        'class' => 'btn btn-danger btn-xs',--}}
{{--        'onclick' => 'return confirm("'.__('lang.are_you_sure').'")'--}}
{{--    ]) !!}--}}
{{--    @endcan--}}
</div>
{!! Form::close() !!}
