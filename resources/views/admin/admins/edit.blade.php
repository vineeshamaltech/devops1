@extends('admin.layouts.app')

@section('title',trans('lang.edit')." ".trans('models/admins.singular'))

@section('content')
    <div class="content">

        @include('flash::message')

        <div class="card">

            {!! Form::model($admin, ['route' => ['admin.admins.update', $admin->id], 'method' => 'patch']) !!}

            <div class="card-body">
                <div class="row">
                    @include('admin.admins.fields')
                </div>
            </div>

            <div class="card-footer">
                {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
                <a href="{{ route('admin.admins.index') }}" class="btn btn-default">
                    @lang('lang.cancel')
                 </a>
            </div>

            {!! Form::close() !!}

        </div>
    </div>
@endsection
