@extends('admin.layouts.app')

@section('title',trans('models/importExport.singular'))

@section('content')
    <div class="content">
        @include('flash::message')
        <div class="row">
            @if(config('constants.features.purchasing_service',false))
            <div class="col-sm-6">
                <div class="card">
                    {!! Form::open(['route' => 'admin.importExport.import','files'=>true]) !!}
                    <div class="card-header">
                        <h3 class="card-title">
                            <i class="fas fa-file-import"></i>
                            {{trans('models/importExport.fields.import')}}
                        </h3>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="form-group col-sm-12">
                                {!! Form::label('type', __('models/importExport.fields.type').':') !!}
                                <select name="type" id="type" class="form-control select2">
                                    <option value="">Choose an option</option>
                                    <option value="product">Product</option>
                                    <option value="category">Category</option>
                                </select>
                            </div>
                            <div id="productGuide" class="form-group col-sm-12" style="display: none;">
                                {!! Form::label('sample', __('models/importExport.fields.sample_product').':') !!}
                                <a class="btn mb-2 btn-sm btn-info" href="{{asset('storage/imports/product_sample.csv')}}" download>
                                  Sample  <!-- <img src="https://cdn-icons.flaticon.com/png/128/2656/premium/2656481.png?token=exp=1651309358~hmac=07b3fc1cfd40468ffe32f0b5eef332a2" data-src="https://cdn-icons.flaticon.com/png/128/2656/premium/2656481.png?token=exp=1651309358~hmac=07b3fc1cfd40468ffe32f0b5eef332a2" alt="Csv file for Product Table" title="Csv file" width="64" height="64" class="lzy lazyload--done" srcset="https://cdn-icons.flaticon.com/png/128/2656/premium/2656481.png?token=exp=1651309358~hmac=07b3fc1cfd40468ffe32f0b5eef332a2 4x"> -->
                                </a>
                                <div class="alert alert-success">
                                    <h5><i class="icon fas fa-ban"></i> Guidelines to import Product !</h5>
                                    <p>1. Download CSV File</p>
                                    <p>2. Store Id field should be an integer and is mandatory
                                        <small>Please <a style="color:blue;" href="{{route('admin.get_store')}}" >click here</a> to download store list.</small>
                                    </p>
                                    <p>3. Product type fields should be either 'SIMPLE','VARIABLE','CUSTOMIZABLE' and is mandatory</p>
                                    <p>4. Name field should be a string and is mandatory</p>
                                    <p>5. Description field should be a string and is not mandatory</p>
                                    <p>6. Unit fields should be either 'gram','kg','liter','piece' and is mandatory</p>
                                    <p>7. Price field should be a decimal number and is mandatory</p>
                                    <p>8. Discount price field should be a decimal number and is mandatory</p>
                                    <p>9. Parent category(Main category) and Category(sub category) id fields should be an integer and is mandatory
                                    <small>Please <a style="color:blue;" href="{{route('admin.get_categories')}}" >click here</a> to download category list.
                                    </small> In case of blank Categories. Please create new category.</p>
                                    <p>10. Unit fields should be either 'veg','non-veg','others' and is mandatory</p>
                                    <p>11. Active fields should be either '1' which means active or '0' which means inactive and is mandatory</p>
                                    <p>12. Opening Time and Closing Time fields should be in format "hh:mm:ss" where 24 hour format should be used for eg. 3:30 PM = 15:30:00 and are not mandatory</p>
                                </div>
                            </div>
                            <div id="categoryGuide" class="form-group col-sm-12" style="display: none;">
                                {!! Form::label('sample', __('models/importExport.fields.sample_category').':') !!}
                                <a class="btn btn-sm btn-info mb-2" href="{{asset('storage/imports/category_sample.csv')}}" download>
                                   Sample <!-- <img src="https://cdn-icons.flaticon.com/png/128/2656/premium/2656481.png?token=exp=1651309358~hmac=07b3fc1cfd40468ffe32f0b5eef332a2" data-src="https://cdn-icons.flaticon.com/png/128/2656/premium/2656481.png?token=exp=1651309358~hmac=07b3fc1cfd40468ffe32f0b5eef332a2" alt="Csv file for Product Table" title="Csv file" width="64" height="64" class="lzy lazyload--done" srcset="https://cdn-icons.flaticon.com/png/128/2656/premium/2656481.png?token=exp=1651309358~hmac=07b3fc1cfd40468ffe32f0b5eef332a2 4x"> -->
                                </a>
                                <div class="alert alert-success">
                                    <h5><i class="icon fas fa-ban"></i> Guidelines to import Category !</h5>
                                    <p>1. Download CSV File</p>
                                    <p>2. Store Id field should be an integer and is mandatory
                                        <small>Please <a style="color:blue;" href="{{route('admin.get_store')}}" >click here</a> to download store list.</small>
                                    </p>
                                    <p>3. Name field should be a string and is mandatory</p>
                                    <p>4. Parent category id field should be an integer and is not mandatory
                                    <small>Please <a style="color:blue;" href="{{route('admin.get_categories')}}" >click here</a> to download category list.
                                    </small> In case of blank Categories. Please create new category.</p>
                                </div>
                            </div>
                            
                            <div class="form-group col-sm-6">
                                {!! Form::label('file', __('models/importExport.fields.import').':') !!}
                                <div class="input-group">
                                    <div class="custom-file">
                                        {!! Form::file('file', ['class' => 'custom-file-input']) !!}
                                        {!! Form::label('file', 'Choose CSV file', ['class' => 'custom-file-label']) !!}
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>

                        </div>
                    </div>

                    <div class="card-footer">
                        {!! Form::submit('Import', ['class' => 'btn btn-primary']) !!}
                        <a href="{{ route('admin.importExport.index') }}" class="btn btn-default">
                            @lang('lang.cancel')
                        </a>
                    </div>

                    {!! Form::close() !!}

                </div>
            </div>
            @endif

            <div class="col-sm-6">
                <div class="card">
                    {!! Form::open(['route' => 'admin.importExport.export']) !!}
                    <div class="card-header">
                        <h3 class="card-title">
                            <i class="fa fa-file-import"></i>
                            {{trans('models/importExport.fields.export')}}
                        </h3>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="form-group col-sm-12">
                               {{-- {!! Form::label('email', __('models/importExport.fields.email').':') !!}--}}
                                <!-- <input type="email" name="email" class="form-control"> -->
                            </div>
                            <div class="form-group col-sm-12">
                                {!! Form::label('type', __('models/importExport.fields.type').':') !!}
                                <select name="type" id="type_of" class="form-control select2">
                                    <option value="">Choose an option</option>
                                    @if(config('constants.features.purchasing_service',false))
                                    <option value="products">Product</option>
                                    <option value="categories">Category</option>
                                    <option value="stores">Stores</option>
                                    <option value="store_timings">Store Timings</option>
                                    @endif
                                    <option value="banners">Banners</option>
                                    <option value="users">Users</option>
                                    <option value="address">Addressed</option>
                                    <option value="drivers">Drivers</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="card-footer">
                        {!! Form::submit('Export', ['class' => 'btn btn-primary']) !!}
                        <!-- <button type="button" id="export">Export</button> -->
                        <a href="{{ route('admin.importExport.index') }}" class="btn btn-default">
                            @lang('lang.cancel')
                        </a>
                    </div>

                    {!! Form::close() !!}

                </div>
            </div>

        </div>

    </div>
@endsection
@push('third_party_scripts')
<script>
    $(document).on('change', '#type', function() {
        if($(this).val() == 'product'){
            $('#productGuide').show();
            $('#categoryGuide').hide();
        }else if($(this).val() == 'category'){
            $('#categoryGuide').show();
            $('#productGuide').hide();
        }
});

    $('#export').click(function () {
        var table = $('#type_of').val();
        $.ajax({
            url: "/export_data",
            type: "POST",
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            data: {'table':table},
            success: function (response) {
            
                var a = document.createElement("a");
                a.href = response.file;
                a.download = response.name;
            }
        })
    });
</script>
@endpush