@extends('admin.layouts.app')

@section('title',trans('lang.edit')." ".trans('models/users.singular'))

@section('content')
    <div class="content">

        <!--@include('adminlte-templates::common.errors')-->

        <div class="card">

            {!! Form::model($user, ['route' => ['admin.users.update', $user->id], 'method' => 'patch', 'files' => true]) !!}

            <div class="card-body">
                <div class="row">
                    @include('admin.users.fields')
                </div>
            </div>

            <div class="card-footer">
                {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
                <a href="{{ route('admin.users.index') }}" class="btn btn-default">
                    @lang('lang.cancel')
                 </a>
            </div>

            {!! Form::close() !!}

        </div>
    </div>
@endsection