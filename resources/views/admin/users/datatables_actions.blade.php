{!! Form::open(['route' => ['admin.users.destroy', $id], 'method' => 'delete']) !!}
<div class='btn-group'>
    @can('users.view')
    <a href="{{ route('admin.users.show', $id) }}" class='m-1'>
        <i class="fa fa-eye"></i>
    </a>
    @endcan
    @can('users.edit')
    <a href="{{ route('admin.users.edit', $id) }}" class='m-1'>
        <i class="fa fa-edit"></i>
    </a>
    @endcan
    @can('users.delete')
    {!! Form::button('<i class="fa fa-trash"></i>', [
        'type' => 'submit',
        'class' => 'btn btn-danger btn-xs m-1',
        'onclick' => 'return confirm("'.__('crud.are_you_sure').'")'
    ]) !!}
    @endcan
</div>
{!! Form::close() !!}
