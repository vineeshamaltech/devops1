<!-- Name Field -->
<div class="col-sm-12">
    {!! Form::label('name', __('models/users.fields.name').':') !!}
    <p>{{ $user->name }}</p>
</div>

<!-- Mobile Field -->
<div class="col-sm-12">
    {!! Form::label('mobile', __('models/users.fields.mobile').':') !!}
    <p>{{ $user->mobile }}</p>
</div>

<!-- Email Field -->
<div class="col-sm-12">
    {!! Form::label('email', __('models/users.fields.email').':') !!}
    <p>{{ $user->email }}</p>
</div>

<!-- Profile Img Field -->
<div class="col-sm-12">
    {!! Form::label('profile_img', __('models/users.fields.profile_img').':') !!}
    <p>{{ $user->profile_img }}</p>
</div>

<!-- Wallet Amount Field -->
<div class="col-sm-12">
    {!! Form::label('wallet_amount', __('models/users.fields.wallet_amount').':') !!}
    <p>{{ $user->wallet_amount }}</p>
</div>

<!-- Lat Field -->
<div class="col-sm-12">
    {!! Form::label('lat', __('models/users.fields.lat').':') !!}
    <p>{{ $user->lat }}</p>
</div>

<!-- Long Field -->
<div class="col-sm-12">
    {!! Form::label('long', __('models/users.fields.long').':') !!}
    <p>{{ $user->long }}</p>
</div>

<!-- User Type Field -->
<div class="col-sm-12">
    {!! Form::label('user_type', __('models/users.fields.user_type').':') !!}
    <p>{{ $user->user_type }}</p>
</div>

<!-- Active Field -->
<div class="col-sm-12">
    {!! Form::label('active', __('models/users.fields.active').':') !!}
    <p>{{ $user->active }}</p>
</div>

<!-- Created At Field -->
<div class="col-sm-12">
    {!! Form::label('created_at', __('models/users.fields.created_at').':') !!}
    <p>{{ $user->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="col-sm-12">
    {!! Form::label('updated_at', __('models/users.fields.updated_at').':') !!}
    <p>{{ $user->updated_at }}</p>
</div>

