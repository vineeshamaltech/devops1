@extends('admin.layouts.app')
@section('title','Setting')

@prepend('page_css')
<style type="text/css">
    .accordion_card{
        background-color: #FFF;
        border: 2px solid #0084DA;
        margin-bottom: 10px;
        border-radius: 5px;
    }
    .accordian_body{
        border-top: 2px solid #0084DA;
    }
</style>
@endprepend

@section('content')
    <div class="row">
        <div class="col-md-4">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">
                    {{__('roles.roles')}}
                    </h3>
                </div>

                <div class="card-body">
                    <div class="form-group col-sm-12">
                        <label for="name">Name</label>
                        <input id="name" value="{{$role->name}}"  type="text" class="form-control"/>
                    </div>

                </div>
                <div class="card-footer">
                    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
                    <a href="{{ route('admin.roles.index') }}" class="btn btn-default">
                        @lang('lang.cancel')
                     </a>
                </div>
            </div>
        </div>
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">
                    <i class="fas fa-cog"></i>
                    {{__('roles.manage_permissions')}}
                    </h3>
                </div>
                <div class="card-body">
                    <div class="accordion" id="accordionExample">
                        @foreach ($permissions as $key => $permission)
                            <div class="accordion_card">
                                <div class="accordian_header">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link btn-block text-left collapsed" type="button" data-toggle="collapse" data-target="#{{$key}}" aria-expanded="false" aria-controls="collapseThree">
                                            {{ucfirst($key)}}
                                        </button>
                                    </h2>
                                </div>
                                <div id="{{$key}}" class="collapse accordian_body" aria-labelledby="headingThree" data-parent="#accordionExample">
                                    <div class="card-body">
                                        <div class="row">
                                            @foreach ($permission as $item)
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <div class="custom-control custom-switch custom-switch-off-danger custom-switch-on-success">
                                                            <input type="checkbox" @if($item['granted']) checked @endif class="custom-control-input permission" id="{{$item['name']}}">
                                                            <label class="custom-control-label" for="{{$item['name']}}">{{ucfirst(substr($item['name'],strpos($item['name'],'.') + 1,strlen($item['name'])))}}</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>

                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@prepend('page_scripts')
<script type="text/javascript">
    $( document ).ready(function() {
        $('input[type="checkbox"].permission').on('change', function (event) {
            var url = "";
            if(this.checked) {
                url = "{{route('admin.grand_permission')}}";
            }else{
                url = "{{route('admin.revoke_permission')}}";
            }

            $.ajax({
                method: "POST",
                url: url,
                data: {_token: "{{csrf_token()}}", role_name: '{{$role->name}}', permission_name: $(this).attr('id')}
            })
            .done(function (msg) {
                if(msg.message == 'Permission revoked') {
                    toastr.error(msg.message);
                }else {
                    toastr.success('Permission granted')
                }
            });

        });
    });
</script>
@endprepend
