{!! Form::open(['route' => ['admin.vehicles.destroy', $id], 'method' => 'delete']) !!}
<div class='btn-group'>
    @can('vehicles.view')
    <a href="{{ route('admin.vehicles.show', $id) }}" class='btn btn-default btn-xs'>
        <i class="fa fa-eye"></i>
    </a>
    @endcan
    @can('vehicles.edit')
    <a href="{{ route('admin.vehicles.edit', $id) }}" class='btn btn-default btn-xs'>
        <i class="fa fa-edit"></i>
    </a>
    @endcan
    @can('vehicles.delete')
    {!! Form::button('<i class="fa fa-trash"></i>', [
        'type' => 'submit',
        'class' => 'btn btn-danger btn-xs',
        'onclick' => 'return confirm("'.__('crud.are_you_sure').'")'
    ]) !!}
    @endcan
</div>
{!! Form::close() !!}
