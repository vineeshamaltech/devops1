@extends('admin.layouts.app')

@section('title',trans('lang.edit')." ".trans('models/fAQS.singular'))

@section('content')
    <div class="content">

        @include('adminlte-templates::common.errors')

        <div class="card">

            {!! Form::model($fAQ, ['route' => ['admin.faqs.update', $fAQ->id], 'method' => 'patch']) !!}

            <div class="card-body">
                <div class="row">
                    @include('admin.f_a_q_s.fields')
                </div>
            </div>

            <div class="card-footer">
                {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
                <a href="{{ route('admin.faqs.index') }}" class="btn btn-default">
                    @lang('lang.cancel')
                 </a>
            </div>

            {!! Form::close() !!}

        </div>
    </div>
@endsection
