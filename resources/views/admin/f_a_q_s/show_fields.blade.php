<!-- Question Field -->
<div class="col-sm-12">
    {!! Form::label('question', __('models/fAQS.fields.question').':') !!}
    <p>{{ $fAQ->question }}</p>
</div>

<!-- Answer Field -->
<div class="col-sm-12">
    {!! Form::label('answer', __('models/fAQS.fields.answer').':') !!}
    <p>{{ $fAQ->answer }}</p>
</div>

<!-- Active Field -->
<div class="col-sm-12">
    {!! Form::label('active', __('models/fAQS.fields.active').':') !!}
    <p>{{ $fAQ->active }}</p>
</div>

<!-- Created At Field -->
<div class="col-sm-12">
    {!! Form::label('created_at', __('models/fAQS.fields.created_at').':') !!}
    <p>{{ $fAQ->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="col-sm-12">
    {!! Form::label('updated_at', __('models/fAQS.fields.updated_at').':') !!}
    <p>{{ $fAQ->updated_at }}</p>
</div>

