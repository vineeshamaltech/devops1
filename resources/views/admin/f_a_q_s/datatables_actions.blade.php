{!! Form::open(['route' => ['admin.faqs.destroy', $id], 'method' => 'delete']) !!}
<div class='btn-group'>
    @can('faqs.view')
    <a href="{{ route('admin.faqs.show', $id) }}" class='btn btn-default btn-xs'>
        <i class="fa fa-eye"></i>
    </a>
    @endcan
    @can('faqs.edit')
    <a href="{{ route('admin.faqs.edit', $id) }}" class='btn btn-default btn-xs'>
        <i class="fa fa-edit"></i>
    </a>
    @endcan
    @can('faqs.delete')
    {!! Form::button('<i class="fa fa-trash"></i>', [
        'type' => 'submit',
        'class' => 'btn btn-danger btn-xs',
        'onclick' => 'return confirm("'.__('crud.are_you_sure').'")'
    ]) !!}
    @endcan
</div>
{!! Form::close() !!}
