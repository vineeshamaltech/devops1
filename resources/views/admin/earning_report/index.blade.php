@extends('admin.layouts.app')

@section('title',trans('models/importExport.fields.export'))

@section('content')
    <div class="content">

        @include('flash::message')

        <div class="row">
            <div class="col-sm-6">
                <div class="card">
                    {!! Form::open(['route' => 'admin.earnings.report.generate','id'=>'reportForm']) !!}
                    <div class="card-header">
                        <h3 class="card-title">
                            <i class="fas fa-file-import"></i>
                            {{trans('models/importExport.fields.export')}}
                        </h3>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="form-group col-sm-12">
                                {!! Form::label('email', __('earningsreport.email').':') !!}
                                <input type="email" name="email" class="form-control" required>
                            </div>
                            <div class="form-group col-sm-12">
                                {!! Form::label('type', __('earningsreport.type').':') !!}
                                <select name="report_of" class="form-control select2" id="report_of" required>
                                    <option value="">Choose an option</option>
                                    <option value="driver">Driver</option>
                                    <option value="vendor">Vendor</option>
                                </select>
                            </div>
                            <div class="form-group col-sm-12">
                                {!! Form::label('type', __('earningsreport.of').':') !!}
                                <select name="reportable_id" class="form-control select2" id="reportable_id" required>

                                </select>
                            </div>
                            <div class="form-group col-sm-12">
                                <label>Date range button:</label>
                                <small id="reportrange"></small>
                                <div class="input-group col-sm-12">
                                    <button type="button" class="btn btn-default col-sm-12" id="daterange-btn">
                                        <i class="far fa-calendar-alt"></i> Date range picker
                                        <i class="fas fa-caret-down"></i>
                                    </button>
                                </div>
                                <input id="daterangevalue" required name="daterange" type="hidden" value="">
                                <input id="exportType" required name="exportType" type="hidden" value="">
                            </div>
                        </div>
                    </div>

                    <div class="card-footer">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="dropdown">
                                    <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Select Option
                                    </button>
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                        <button class="dropdown-item" type="button" id="mail-btn">Send Mail</button>
                                        <button class="dropdown-item" type="button" id="export-btn">Export Service Report(Csv)</button>
                                        <button class="dropdown-item" type="button" id="export1-btn">Export Report(Csv)</button>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 text-right">
                                <a href="{{ route('admin.earnings.index') }}" class="btn btn-default">
                                    @lang('lang.cancel')
                                </a>
                            </div>
                        </div>
                        <!-- {!! Form::submit('Export', ['class' => 'btn btn-primary']) !!} -->
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>

        </div>

    </div>
@endsection

@push('third_party_stylesheets')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-daterangepicker/3.0.5/daterangepicker.css" integrity="sha512-gp+RQIipEa1X7Sq1vYXnuOW96C4704yI1n0YB9T/KqdvqaEgL6nAuTSrKufUX3VBONq/TPuKiXGLVgBKicZ0KA==" crossorigin="anonymous" referrerpolicy="no-referrer" />
@endpush

@push('third_party_scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-daterangepicker/3.0.5/daterangepicker.min.js" integrity="sha512-mh+AjlD3nxImTUGisMpHXW03gE6F4WdQyvuFRkjecwuWLwD2yCijw4tKA3NsEFpA1C3neiKhGXPSIGSfCYPMlQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script type="text/javascript">
    var start = moment().subtract(29, 'days');
    var end = moment();
    $('#daterange-btn').daterangepicker(
        {
            ranges   : {
                'Today'       : [moment(), moment()],
                'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'This Week'   : [moment().clone().startOf('isoWeek'), moment().clone().endOf('isoWeek')],
                'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month'  : [moment().startOf('month'), moment().endOf('month')],
                'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            startDate: start,
            endDate  : end
        },cb
    );

    function cb(start,end){
        $('#reportrange').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
        $('#daterangevalue').val(start.format('MMMM D, YYYY') +'-' + end.format('MMMM D, YYYY'))
    }

    $(document).ready(function (){
        $('#report_of').on('change',function () {
            $('#reportable_id').html('');
            var data = $(this).val();
            var url = "";
            if(data === 'vendor'){
                url = '/api/admins?filter=id;name&search=type:Vendor&searchJoin=and';
            }else if(data === 'driver'){
                url = '/api/drivers?filter=id;name;mobile';
            } else if(data === ""){

            }
            getData(url);
        }).change();
        // $('#category').on('change',function (){
        //     var parent = $(this).val();
        //     var storeId = $("#store_id").val();
        //     getSubCategories(storeId,parent);
        // }).change();
        // $('#subcategory').on('change',function (){
        //
        // });
    });

    function getData(url) {
        $('#reportable_id').select2({
            theme: 'classic',
            ajax: {
                url: url,
                dataType: 'json',
                processResults: function (data) {
                    // Transforms the top-level key of the response object from 'items' to 'results'
                    if(data.data.length > 0) {
                        return {
                            results: data.data.map(function (item) {
                                return {
                                    id: item.id,
                                    text: item.name
                                }
                            })
                        };
                    }
                }
            }
        });
    }

    cb(start,end);

    $('#mail-btn').click(function () {
        $('#exportType').val('mail');
        $("#reportForm").submit();
    });

    $('#export-btn').click(function () {
        $('#exportType').val('service');
        $("#reportForm").submit();
    });

    $('#export1-btn').click(function () {
        $('#exportType').val('report');
        $("#reportForm").submit();
    });
    
    
</script>
@endpush
