@extends('admin.layouts.app')

@section('title',trans('lang.edit')." ".trans('models/vendorKycs.singular'))

@section('content')
    <div class="content">

        <!--@include('adminlte-templates::common.errors')-->

        <div class="card">

            {!! Form::model($vendorKyc, ['route' => ['admin.vendorKycs.update', $vendorKyc->id], 'method' => 'patch', 'files' => true]) !!}

            <div class="card-body">
                <div class="row">
                    @include('admin.vendor_kycs.fields')
                </div>
            </div>

            <div class="card-footer">
                {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
                <a href="{{ route('admin.vendorKycs.index') }}" class="btn btn-default">
                    @lang('lang.cancel')
                 </a>
            </div>

            {!! Form::close() !!}

        </div>
    </div>
@endsection