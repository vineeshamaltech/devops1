<!-- Vedor Id Field -->
<div class="col-sm-12">
    {!! Form::label('vedor_id', __('models/vendorKycs.fields.vedor_id').':') !!}
    <p>{{ $vendorKyc->vedor_id }}</p>
</div>

<!-- Doc Type Field -->
<div class="col-sm-12">
    {!! Form::label('doc_type', __('models/vendorKycs.fields.doc_type').':') !!}
    <p>{{ $vendorKyc->doc_type }}</p>
</div>

<!-- Photo Field -->
<div class="col-sm-12">
    {!! Form::label('photo', __('models/vendorKycs.fields.photo').':') !!}
    <p>{{ $vendorKyc->photo }}</p>
</div>

<!-- Status Field -->
<div class="col-sm-12">
    {!! Form::label('status', __('models/vendorKycs.fields.status').':') !!}
    <p>{{ $vendorKyc->status }}</p>
</div>

<!-- Created At Field -->
<div class="col-sm-12">
    {!! Form::label('created_at', __('models/vendorKycs.fields.created_at').':') !!}
    <p>{{ $vendorKyc->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="col-sm-12">
    {!! Form::label('updated_at', __('models/vendorKycs.fields.updated_at').':') !!}
    <p>{{ $vendorKyc->updated_at }}</p>
</div>

