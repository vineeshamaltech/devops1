<!-- Vedor Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('admin_id', __('models/vendorKycs.fields.vendor_id').':',['class'=>'required']) !!}
    <select name="admin_id" class="form-control select2" required>
        @foreach(\App\Models\Admin::get() as $item)
            <option value="{{ $item->id }}"
                    @if(isset($vendorKyc) && $vendorKyc->vendor_id == $item->id) selected @endif>{{ $item->name }}</option>
        @endforeach
    </select>
</div>

<!-- Status Field -->
<div class="form-group col-sm-6">
    {!! Form::label('status', __('models/vendorKycs.fields.status').':',['class'=>'required']) !!}
    {!! Form::select('status', ['UPLOADED'=>'Uploaded','APPROVED'=>'Approved','REJECTED'=>'Rejected'], null, ['class' => 'form-control select2','required'=>'true']) !!}
</div>

<!-- Photo Field -->
<div class="form-group col-sm-6">
    {!! Form::label('aadhar', __('models/vendorKycs.fields.aadhar').':') !!}
    <span>Max file upload: 2 MB</span>
    <div class="input-group">
        <div class="custom-file">
            {!! Form::file('aadhar', ['class' => 'custom-file-input']) !!}
            {!! Form::label('aadhar', 'Choose file', ['class' => 'custom-file-label']) !!}
        </div>
    </div>
     @if($errors -> first('aadhar'))
    <div class="alert-danger" >{{$errors -> first('aadhar')}}</div>
    @endif
</div>
<div class="clearfix"></div>

<!-- Photo Field -->
<div class="form-group col-sm-6">
    {!! Form::label('pan', __('models/vendorKycs.fields.pan').':') !!}
    <span>Max file upload: 2 MB</span>
    <div class="input-group">
        <div class="custom-file">
            {!! Form::file('pan', ['class' => 'custom-file-input']) !!}
            {!! Form::label('pan', 'Choose file', ['class' => 'custom-file-label']) !!}
        </div>
    </div>
    @if($errors -> first('pan'))
    <div class="alert-danger" >{{$errors -> first('pan')}}</div>
    @endif
</div>
<div class="clearfix"></div>

<!-- Photo Field -->
<div class="form-group col-sm-6">
    {!! Form::label('fssai', __('models/vendorKycs.fields.fssai').':') !!}
    <span>Max file upload: 2 MB</span>
    <div class="input-group">
        <div class="custom-file">
            {!! Form::file('fssai', ['class' => 'custom-file-input']) !!}
            {!! Form::label('fssai', 'Choose file', ['class' => 'custom-file-label']) !!}
        </div>
    </div>
     @if($errors -> first('fssai'))
    <div class="alert-danger" >{{$errors -> first('fssai')}}</div>
    @endif
</div>
<div class="clearfix"></div>

<!-- Photo Field -->
<div class="form-group col-sm-6">
       {!! Form::label('gst', __('models/vendorKycs.fields.gst').':') !!}
    <span>Max file upload: 2 MB</span>
    <div class="input-group">
        <div class="custom-file">
            {!! Form::file('gst', ['class' => 'custom-file-input']) !!}
            {!! Form::label('gst', 'Choose file', ['class' => 'custom-file-label']) !!}
        </div>
    </div>
     @if($errors -> first('gst'))
    <div class="alert-danger" >{{$errors -> first('gst')}}</div>
    @endif
</div>
<div class="clearfix"></div>

<!-- Photo Field -->
<div class="form-group col-sm-6">
    {!! Form::label('other', __('models/vendorKycs.fields.other').':') !!}
    <span>Max file upload: 2 MB</span>
    <div class="input-group">
        <div class="custom-file">
            {!! Form::file('other', ['class' => 'custom-file-input']) !!}
            {!! Form::label('other', 'Choose file', ['class' => 'custom-file-label']) !!}
        </div>
    </div>
     @if($errors -> first('other'))
    <div class="alert-danger" >{{$errors -> first('other')}}</div>
    @endif
</div>
<div class="clearfix"></div>

<!-- Photo Field -->
<div class="form-group col-sm-6">
    {!! Form::label('photo', __('models/vendorKycs.fields.photo').':') !!}
    <span>Max file upload: 2 MB</span>
    <div class="input-group">
        <div class="custom-file">
            {!! Form::file('photo', ['class' => 'custom-file-input']) !!}
            {!! Form::label('photo', 'Choose file', ['class' => 'custom-file-label']) !!}
        </div>
    </div>
      @if($errors -> first('photo'))
    <div class="alert-danger" >{{$errors -> first('photo')}}</div>
    @endif
</div>
<div class="clearfix"></div>

