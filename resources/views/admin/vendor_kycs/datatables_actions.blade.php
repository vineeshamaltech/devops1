{!! Form::open(['route' => ['admin.vendorKycs.destroy', $id], 'method' => 'delete']) !!}
<div class='btn-group'>
    @can('vendorsKyc.view')
    <a href="{{ route('admin.vendorKycs.show', $id) }}" class='btn btn-default btn-xs'>
        <i class="fa fa-eye"></i>
    </a>
    @endcan
    @can('vendorsKyc.edit')
    <a href="{{ route('admin.vendorKycs.edit', $id) }}" class='btn btn-default btn-xs'>
        <i class="fa fa-edit"></i>
    </a>
    @endcan
    @can('vendorsKyc.delete')
    {!! Form::button('<i class="fa fa-trash"></i>', [
        'type' => 'submit',
        'class' => 'btn btn-danger btn-xs',
        'onclick' => 'return confirm("'.__('crud.are_you_sure').'")'
    ]) !!}
    @endcan
</div>
{!! Form::close() !!}
