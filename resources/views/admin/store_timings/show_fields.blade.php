<!-- Store Id Field -->
<div class="col-sm-12">
    {!! Form::label('store_id', __('models/stores.fields.store_id').':') !!}
    <p>{{ $storeTiming[0]->id }}</p>
</div>

<!-- Store Name Field -->
<div class="col-sm-12">
    {!! Form::label('store_id', __('models/stores.singular').':') !!}
    <p>{{ $storeTiming[0]->name }}</p>
</div>

<div class="col-lg-12">
    <div class="card">
        <div class="card-header">
            <h5>Monday</h5>
        </div>
        <div class="card-body">
            <div class="row">
                <!-- Open Field -->
                <div class="form-group col-lg-4">
                    {!! Form::label('open', __('models/storeTimings.fields.open').':') !!}
                    @if($all_days['Monday']){{$all_days['Monday'][0]}}@endif
                </div>

                <!-- Close Field -->
                <div class="form-group col-lg-4">
                    {!! Form::label('close', __('models/storeTimings.fields.close').':') !!}
                    @if($all_days['Monday']){{$all_days['Monday'][1]}}@endif
                </div>  
            </div>
        </div>
    </div>
</div>
<div class="col-lg-12">
    <div class="card">
        <div class="card-header">
            <h5>Tuesday</h5>
        </div>
        <div class="card-body">
            <div class="row">
                <!-- Open Field -->
                <div class="form-group col-lg-4">
                    {!! Form::label('open', __('models/storeTimings.fields.open').':') !!}
                    @if($all_days['Tuesday']){{$all_days['Tuesday'][0]}}@endif
                </div>

                <!-- Close Field -->
                <div class="form-group col-lg-4">
                    {!! Form::label('close', __('models/storeTimings.fields.close').':') !!}
                    @if($all_days['Tuesday']){{$all_days['Tuesday'][1]}}@endif
                </div>  
            </div>
        </div>
    </div>
</div>
<div class="col-lg-12">
    <div class="card">
        <div class="card-header">
            <h5>Wednesday</h5>
        </div>
        <div class="card-body">
            <div class="row">
                <!-- Open Field -->
                <div class="form-group col-lg-4">
                    {!! Form::label('open', __('models/storeTimings.fields.open').':') !!}
                    @if($all_days['Wednesday']){{$all_days['Wednesday'][0]}}@endif
                </div>

                <!-- Close Field -->
                <div class="form-group col-lg-4">
                    {!! Form::label('close', __('models/storeTimings.fields.close').':') !!}
                    @if($all_days['Wednesday']){{$all_days['Wednesday'][1]}}@endif
                </div>  
            </div>
        </div>
    </div>
</div>
<div class="col-lg-12">
    <div class="card">
        <div class="card-header">
            <h5>Thursday</h5>
        </div>
        <div class="card-body">
            <div class="row">
                <!-- Open Field -->
                <div class="form-group col-lg-4">
                    {!! Form::label('open', __('models/storeTimings.fields.open').':') !!}
                    @if($all_days['Thursday']){{$all_days['Thursday'][0]}}@endif
                </div>

                <!-- Close Field -->
                <div class="form-group col-lg-4">
                    {!! Form::label('close', __('models/storeTimings.fields.close').':') !!}
                    @if($all_days['Thursday']){{$all_days['Thursday'][1]}}@endif
                </div>  
            </div>
        </div>
    </div>
</div>
<div class="col-lg-12">
    <div class="card">
        <div class="card-header">
            <h5>Friday</h5>
        </div>
        <div class="card-body">
            <div class="row">
                <!-- Open Field -->
                <div class="form-group col-lg-4">
                    {!! Form::label('open', __('models/storeTimings.fields.open').':') !!}
                    @if($all_days['Friday']){{$all_days['Friday'][0]}}@endif
                </div>

                <!-- Close Field -->
                <div class="form-group col-lg-4">
                    {!! Form::label('close', __('models/storeTimings.fields.close').':') !!}
                    @if($all_days['Friday']){{$all_days['Friday'][1]}}@endif
                </div>  
            </div>
        </div>
    </div>
</div>
<div class="col-lg-12">
    <div class="card">
        <div class="card-header">
            <h5>Saturday</h5>
        </div>
        <div class="card-body">
            <div class="row">
                <!-- Open Field -->
                <div class="form-group col-lg-4">
                    {!! Form::label('open', __('models/storeTimings.fields.open').':') !!}
                    @if($all_days['Saturday']){{$all_days['Saturday'][0]}}@endif
                </div>

                <!-- Close Field -->
                <div class="form-group col-lg-4">
                    {!! Form::label('close', __('models/storeTimings.fields.close').':') !!}
                    @if($all_days['Saturday']){{$all_days['Saturday'][1]}}@endif
                </div>  
            </div>
        </div>
    </div>
</div>
<div class="col-lg-12">
    <div class="card">
        <div class="card-header">
            <h5>Sunday</h5>
        </div>
        <div class="card-body">
            <div class="row">
                <!-- Open Field -->
                <div class="form-group col-lg-4">
                    {!! Form::label('open', __('models/storeTimings.fields.open').':') !!}
                    @if($all_days['Sunday']){{$all_days['Sunday'][0]}}@endif
                </div>

                <!-- Close Field -->
                <div class="form-group col-lg-4">
                    {!! Form::label('close', __('models/storeTimings.fields.close').':') !!}
                    @if($all_days['Sunday']){{$all_days['Sunday'][1]}}@endif
                </div>  
            </div>
        </div>
    </div>
</div>




