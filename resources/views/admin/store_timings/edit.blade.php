@extends('admin.layouts.app')

@section('title',trans('lang.edit')." ".trans('models/storeTimings.singular'))

@section('content')
    <div class="content">

        @include('adminlte-templates::common.errors')
        <div class="card">
            {!! Form::model($storeTiming, ['route' => ['admin.storeTimings.update', $storeTiming[0]->id], 'method' => 'patch']) !!}

            <div class="card-body">
                <div class="row">
                    @include('admin.store_timings.fields')
                </div>
            </div>

            <div class="card-footer">
                @if(!empty($storeTiming))
                    {!! Form::submit('Update', ['class' => 'btn btn-primary']) !!}
                @else
                {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
                @endif
                <a href="{{ route('admin.storeTimings.index') }}" class="btn btn-default">
                    @lang('lang.cancel')
                 </a>

            </div>

            {!! Form::close() !!}

        </div>
    </div>
@endsection