<div class="col-lg-12">
    <!-- Store Id Field -->
    <div class="form-group col-sm-4">
        {!! Form::label('store_id', __('models/storeTimings.fields.store_id').':') !!}
        <select name="store_id" id="store_id" class="form-control select2">
            @foreach($stores as $store)
                <option value="{{$store->id}}" @if(!empty($storeTiming) && $store->id == $storeTiming[0]->id) selected @endif>{{$store->name}}</option>
            @endforeach
        </select>
    </div>
</div>

@if(isset($all_days["Monday"]) && count($all_days["Monday"])>0)
<div class="col-lg-12">
    <div class="card">
        <div class="card-header">
            <h3>Monday</h3>
        </div>
        <div class="card-body">
            <div class="row">
                <input type="hidden" value='{{count($all_days["Monday"])}}' id="monday_count"/>
                <input type="hidden" value="monday" name="monday"/>
                <table>
                    <tbody>
                        @foreach($all_days["Monday"] as $key => $mon)
                        <tr>
                            <td style="font-weight: 800;">From</td>
                            <td style="font-weight: 800;">To</td>
                            
                            <td></td>
                        </tr>
                        <tr>
                            <td>
                                <div class="input-group date" id="monday_opentime{{$key}}" data-target-input="nearest">
                                    <input name="monday_open[]" id="monday_open{{$key}}" value="{{$mon['open']}}" type="text" class="form-control datetimepicker-input" data-target="#monday_opentime{{$key}}"  data-toggle="datetimepicker"/>
                                    <div class="input-group-append" data-target="#monday_opentime" data-toggle="datetimepicker">
                                        <div class="input-group-text"><i class="fas fa-clock"></i></div>
                                    </div>
                                </div>
                            </td>
                            <td>
                                <div class="input-group date" id="monday_closetime{{$key}}" data-target-input="nearest">
                                    <input name="monday_close[]" id="monday_close{{$key}}" value="{{$mon['close']}}" type="text" class="form-control datetimepicker-input" data-target="#monday_closetime{{$key}}"  data-toggle="datetimepicker"/>
                                    <div class="input-group-append" data-target="#monday_closetime" data-toggle="datetimepicker">
                                        <div class="input-group-text"><i class="fas fa-clock"></i></div>
                                    </div>
                                </div>
                            </td>
                            @if($loop->first == $loop->last)
                            <td>
                                <button type="button" class="btn btn-secondary copy_all">Copy To All</button>
                            </td>
                            <td>
                                <button type="button" class="btn btn-success monday_addmore">Add more</button>
                            </td>
                            @elseif($loop->first)
                            <td>
                                <button type="button" class="btn btn-secondary copy_all">Copy To All</button>
                            </td>
                            @elseif($loop->last)
                            <td>
                                <button type="button" class="btn btn-success monday_addmore">Add more</button>
                            </td>
                            @else
                                <td>
                                    <button type="button" class="btn btn-danger removeMe">Remove</button>
                                </td>
                            @endif
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@else
<div class="col-lg-12">
    <div class="card">
        <div class="card-header">
            <h3>Monday</h3>
        </div>
        <div class="card-body">
            <div class="row">
                <input type="hidden" value="0" id="monday_count"/>
                <input type="hidden" value="monday" name="monday"/>
                <table>
                    <tbody>
                        <tr>
                            <td style="font-weight: 800;">From</td>
                            <td style="font-weight: 800;">To</td>
                            
                            <td></td>
                        </tr>
                        <tr>
                            <td>
                                <div class="input-group date" id="monday_opentime" data-target-input="nearest">
                                    <input name="monday_open[]" id="monday_open0" type="text" class="form-control datetimepicker-input" data-target="#monday_opentime" data-toggle="datetimepicker"/>
                                    <div class="input-group-append" data-target="#monday_opentime" data-toggle="datetimepicker">
                                        <div class="input-group-text"><i class="fas fa-clock"></i></div>
                                    </div>
                                </div>
                            </td>
                            <td>
                                <div class="input-group date" id="monday_closetime" data-target-input="nearest">
                                    <input name="monday_close[]" id="monday_close0" type="text" class="form-control datetimepicker-input" data-target="#monday_closetime" data-toggle="datetimepicker"/>
                                    <div class="input-group-append" data-target="#monday_closetime" data-toggle="datetimepicker">
                                        <div class="input-group-text"><i class="fas fa-clock"></i></div>
                                    </div>
                                </div>
                            </td>
                            <td>
                                <button type="button" class="btn btn-success monday_addmore">Add more</button>
                                <button type="button" class="btn btn-secondary copy_all">Copy To All</button>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endif


@if(isset($all_days["Tuesday"]) && count($all_days["Tuesday"])>0)
<div class="col-lg-12">
    <div class="card">
        <div class="card-header">
            <h3>Tuesday</h3>
        </div>
        <div class="card-body">
            <div class="row">
                <input type="hidden" value="{{count($all_days['Tuesday'])}}" id="tuesday_count"/>
                <input type="hidden" value="tuesday" name="tuesday"/>
                <table>
                    <tbody>
                        @foreach($all_days["Tuesday"] as $key=> $tue)
                        <tr>
                            <td style="font-weight: 800;">From</td>
                            <td style="font-weight: 800;">To</td>
                            
                            <td></td>
                        </tr>
                        <tr>
                            <td>
                                <div class="input-group date" id="tuesday_opentime{{$key}}" data-target-input="nearest">
                                    <input name="tuesday_open[]" id="tuesday_open{{$key}}" value="{{$tue['open']}}" type="text" class="form-control datetimepicker-input" data-target="#tuesday_opentime{{$key}}" data-toggle="datetimepicker"/>
                                    <div class="input-group-append" data-target="#tuesday_opentime" data-toggle="datetimepicker">
                                        <div class="input-group-text"><i class="fas fa-clock"></i></div>
                                    </div>
                                </div>
                            </td>
                            <td>
                                <div class="input-group date" id="tuesday_closetime{{$key}}" data-target-input="nearest">
                                    <input name="tuesday_close[]" id="tuesday_close{{$key}}" value="{{$tue['close']}}" type="text" class="form-control datetimepicker-input" data-target="#tuesday_closetime{{$key}}" data-toggle="datetimepicker"/>
                                    <div class="input-group-append" data-target="#tuesday_closetime" data-toggle="datetimepicker">
                                        <div class="input-group-text"><i class="fas fa-clock"></i></div>
                                    </div>
                                </div>
                            </td>
                            @if($loop->last)
                            <td>
                                <button type="button" class="btn btn-success tuesday_addmore">Add more</button>
                            </td>
                            @else
                                <td>
                                    <button type="button" class="btn btn-danger removeMe">Remove</button>
                                </td>
                            @endif
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                
            </div>
            
        </div>
    </div>
</div>
@else
<div class="col-lg-12">
    <div class="card">
        <div class="card-header">
            <h3>Tuesday</h3>
        </div>
        <div class="card-body">
            <div class="row">
                <input type="hidden" value="0" id="tuesday_count"/>
                <input type="hidden" value="tuesday" name="tuesday"/>
                <table>
                    <tbody>
                        <tr>
                            <td style="font-weight: 800;">From</td>
                            <td style="font-weight: 800;">To</td>
                            
                            <td></td>
                        </tr>
                        <tr>
                            <td>
                                <div class="input-group date" id="tuesday_opentime" data-target-input="nearest">
                                    <input name="tuesday_open[]" id="tuesday_open0" type="text" class="form-control datetimepicker-input" data-target="#tuesday_opentime" data-toggle="datetimepicker"/>
                                    <div class="input-group-append" data-target="#tuesday_opentime" data-toggle="datetimepicker">
                                        <div class="input-group-text"><i class="fas fa-clock"></i></div>
                                    </div>
                                </div>
                            </td>
                            <td>
                                <div class="input-group date" id="tuesday_closetime" data-target-input="nearest">
                                    <input name="tuesday_close[]" id="tuesday_close0" type="text" class="form-control datetimepicker-input" data-target="#tuesday_closetime" data-toggle="datetimepicker"/>
                                    <div class="input-group-append" data-target="#tuesday_closetime" data-toggle="datetimepicker">
                                        <div class="input-group-text"><i class="fas fa-clock"></i></div>
                                    </div>
                                </div>
                            </td>
                            <td>
                                <button type="button" class="btn btn-success tuesday_addmore">Add more</button>
                            </td>
                        </tr>
                    </tbody>
                </table>
                
            </div>
            
        </div>
    </div>
</div>
@endif

@if(isset($all_days["Wednesday"]) && count($all_days["Wednesday"])>0)
<div class="col-lg-12">
    <div class="card">
        <div class="card-header">
            <h3>Wednesday</h3>
        </div>
        <div class="card-body">
            <div class="row">
                <input type="hidden" value="{{count($all_days['Wednesday'])}}" id="wednesday_count"/>
                <input type="hidden" value="wednesday" name="wednesday"/>
                <table>
                    <tbody>
                        @foreach($all_days["Wednesday"] as $key=>$wed)
                        <tr>
                            <td style="font-weight: 800;">From</td>
                            <td style="font-weight: 800;">To</td>
                            
                            <td></td>
                        </tr>
                        <tr>
                            <td>
                                <div class="input-group date" id="wednesday_opentime{{$key}}" data-target-input="nearest">
                                    <input name="wednesday_open[]"  id="wednesday_open{{$key}}" value="{{$wed['open']}}" type="text" class="form-control datetimepicker-input" data-target="#wednesday_opentime{{$key}}" data-toggle="datetimepicker"/>
                                    <div class="input-group-append" data-target="#wednesday_opentime" data-toggle="datetimepicker">
                                        <div class="input-group-text"><i class="fas fa-clock"></i></div>
                                    </div>
                                </div>
                            </td>
                            <td>
                                <div class="input-group date" id="wednesday_closetime{{$key}}" data-target-input="nearest">
                                    <input name="wednesday_close[]" id="wednesday_close{{$key}}" value="{{$wed['close']}}" type="text" class="form-control datetimepicker-input" data-target="#wednesday_closetime{{$key}}" data-toggle="datetimepicker"/>
                                    <div class="input-group-append" data-target="#wednesday_closetime" data-toggle="datetimepicker">
                                        <div class="input-group-text"><i class="fas fa-clock"></i></div>
                                    </div>
                                </div>
                            </td>
                            @if($loop->last)
                            <td>
                                <button type="button" class="btn btn-success wednesday_addmore">Add more</button>
                            </td>
                            @else
                                <td>
                                    <button type="button" class="btn btn-danger removeMe">Remove</button>
                                </td>
                            @endif
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                
            </div>
            
        </div>
    </div>
</div>
@else
<div class="col-lg-12">
    <div class="card">
        <div class="card-header">
            <h3>Wednesday</h3>
        </div>
        <div class="card-body">
            <div class="row">
                <input type="hidden" value="0" id="wednesday_count"/>
                <input type="hidden" value="wednesday" name="wednesday"/>
                <table>
                    <tbody>
                        <tr>
                            <td style="font-weight: 800;">From</td>
                            <td style="font-weight: 800;">To</td>
                            
                            <td></td>
                        </tr>
                        <tr>
                            <td>
                                <div class="input-group date" id="wednesday_opentime" data-target-input="nearest">
                                    <input name="wednesday_open[]" id="wednesday_open0" type="text" class="form-control datetimepicker-input" data-target="#wednesday_opentime" data-toggle="datetimepicker"/>
                                    <div class="input-group-append" data-target="#wednesday_opentime" data-toggle="datetimepicker">
                                        <div class="input-group-text"><i class="fas fa-clock"></i></div>
                                    </div>
                                </div>
                            </td>
                            <td>
                                <div class="input-group date" id="wednesday_closetime" data-target-input="nearest">
                                    <input name="wednesday_close[]" id="wednesday_close0" type="text" class="form-control datetimepicker-input" data-target="#wednesday_closetime" data-toggle="datetimepicker"/>
                                    <div class="input-group-append" data-target="#wednesday_closetime" data-toggle="datetimepicker">
                                        <div class="input-group-text"><i class="fas fa-clock"></i></div>
                                    </div>
                                </div>
                            </td>
                            <td>
                                <button type="button" class="btn btn-success wednesday_addmore">Add more</button>
                            </td>
                        </tr>
                    </tbody>
                </table>
                
            </div>
            
        </div>
    </div>
</div>
@endif
@if(isset($all_days["Thursday"]) && count($all_days["Thursday"])>0)
<div class="col-lg-12">
    <div class="card">
        <div class="card-header">
            <h3>Thursday</h3>
        </div>
        <div class="card-body">
            <div class="row">
                <input type="hidden" value="{{count($all_days['Thursday'])}}" id="thursday_count"/>
                <input type="hidden" value="thursday" name="thursday"/>
                <table>
                    <tbody>
                        @foreach($all_days["Thursday"] as $key=>$thur)
                        <tr>
                            <td style="font-weight: 800;">From</td>
                            <td style="font-weight: 800;">To</td>
                            
                            <td></td>
                        </tr>
                        <tr>
                            <td>
                                <div class="input-group date" id="thursday_opentime{{$key}}" data-target-input="nearest">
                                    <input name="thursday_open[]" id="thursday_open{{$key}}" value="{{$thur['open']}}" type="text" class="form-control datetimepicker-input" data-target="#thursday_opentime{{$key}}" data-toggle="datetimepicker"/>
                                    <div class="input-group-append" data-target="#thursday_opentime" data-toggle="datetimepicker">
                                        <div class="input-group-text"><i class="fas fa-clock"></i></div>
                                    </div>
                                </div>
                            </td>
                            <td>
                                <div class="input-group date" id="thursday_closetime{{$key}}" data-target-input="nearest">
                                    <input name="thursday_close[]" id="thursday_close{{$key}}" value="{{$thur['close']}}" type="text" class="form-control datetimepicker-input" data-target="#thursday_closetime{{$key}}" data-toggle="datetimepicker"/>
                                    <div class="input-group-append" data-target="#thursday_closetime" data-toggle="datetimepicker">
                                        <div class="input-group-text"><i class="fas fa-clock"></i></div>
                                    </div>
                                </div>
                            </td>
                            @if($loop->last)
                            <td>
                                <button type="button" class="btn btn-success thursday_addmore">Add more</button>
                            </td>
                            @else
                                <td>
                                    <button type="button" class="btn btn-danger removeMe">Remove</button>
                                </td>
                            @endif
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                
            </div>
            
        </div>
    </div>
</div>
@else
<div class="col-lg-12">
    <div class="card">
        <div class="card-header">
            <h3>Thursday</h3>
        </div>
        <div class="card-body">
            <div class="row">
                <input type="hidden" value="0" id="thursday_count"/>
                <input type="hidden" value="thursday" name="thursday"/>
                <table>
                    <tbody>
                        <tr>
                            <td style="font-weight: 800;">From</td>
                            <td style="font-weight: 800;">To</td>
                            
                            <td></td>
                        </tr>
                        <tr>
                            <td>
                                <div class="input-group date" id="thursday_opentime" data-target-input="nearest">
                                    <input name="thursday_open[]" id="thursday_open0" type="text" class="form-control datetimepicker-input" data-target="#thursday_opentime" data-toggle="datetimepicker"/>
                                    <div class="input-group-append" data-target="#thursday_opentime" data-toggle="datetimepicker">
                                        <div class="input-group-text"><i class="fas fa-clock"></i></div>
                                    </div>
                                </div>
                            </td>
                            <td>
                                <div class="input-group date" id="thursday_closetime" data-target-input="nearest">
                                    <input name="thursday_close[]" id="thursday_close0" type="text" class="form-control datetimepicker-input" data-target="#thursday_closetime" data-toggle="datetimepicker"/>
                                    <div class="input-group-append" data-target="#thursday_closetime" data-toggle="datetimepicker">
                                        <div class="input-group-text"><i class="fas fa-clock"></i></div>
                                    </div>
                                </div>
                            </td>
                            <td>
                                <button type="button" class="btn btn-success thursday_addmore">Add more</button>
                            </td>
                        </tr>
                    </tbody>
                </table>
                
            </div>
            
        </div>
    </div>
</div>
@endif
@if(isset($all_days["Friday"]) && count($all_days["Friday"])>0)
<div class="col-lg-12">
    <div class="card">
        <div class="card-header">
            <h3>Friday</h3>
        </div>
        <div class="card-body">
            <div class="row">
                <input type="hidden" value="{{count($all_days['Friday'])}}" id="friday_count"/>
                <input type="hidden" value="friday" name="friday"/>
                <table>
                    <tbody>
                        @foreach($all_days["Friday"] as $key=>$fri)
                        <tr>
                            <td style="font-weight: 800;">From</td>
                            <td style="font-weight: 800;">To</td>
                            
                            <td></td>
                        </tr>
                        <tr>
                            <td>
                                <div class="input-group date" id="friday_opentime{{$key}}" data-target-input="nearest">
                                    <input name="friday_open[]" id="friday_open{{$key}}" value="{{$fri['open']}}" type="text" class="form-control datetimepicker-input" data-target="#friday_opentime{{$key}}" data-toggle="datetimepicker"/>
                                    <div class="input-group-append" data-target="#friday_opentime" data-toggle="datetimepicker">
                                        <div class="input-group-text"><i class="fas fa-clock"></i></div>
                                    </div>
                                </div>
                            </td>
                            <td>
                                <div class="input-group date" id="friday_closetime{{$key}}" data-target-input="nearest">
                                    <input name="friday_close[]" id="friday_close{{$key}}" value="{{$fri['close']}}" type="text" class="form-control datetimepicker-input" data-target="#friday_closetime{{$key}}" data-toggle="datetimepicker"/>
                                    <div class="input-group-append" data-target="#friday_closetime" data-toggle="datetimepicker">
                                        <div class="input-group-text"><i class="fas fa-clock"></i></div>
                                    </div>
                                </div>
                            </td>
                            @if($loop->last)
                            <td>
                                <button type="button" class="btn btn-success friday_addmore">Add more</button>
                            </td>
                            @else
                                <td>
                                    <button type="button" class="btn btn-danger removeMe">Remove</button>
                                </td>
                            @endif
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                
            </div>
            
        </div>
    </div>
</div>
@else
<div class="col-lg-12">
    <div class="card">
        <div class="card-header">
            <h3>Friday</h3>
        </div>
        <div class="card-body">
            <div class="row">
                <input type="hidden" value="0" id="friday_count"/>
                <input type="hidden" value="friday" name="friday"/>
                <table>
                    <tbody>
                        <tr>
                            <td style="font-weight: 800;">From</td>
                            <td style="font-weight: 800;">To</td>
                            
                            <td></td>
                        </tr>
                        <tr>
                            <td>
                                <div class="input-group date" id="friday_opentime" data-target-input="nearest">
                                    <input name="friday_open[]" id="friday_open0" type="text" class="form-control datetimepicker-input" data-target="#friday_opentime" data-toggle="datetimepicker"/>
                                    <div class="input-group-append" data-target="#friday_opentime" data-toggle="datetimepicker">
                                        <div class="input-group-text"><i class="fas fa-clock"></i></div>
                                    </div>
                                </div>
                            </td>
                            <td>
                                <div class="input-group date" id="friday_closetime" data-target-input="nearest">
                                    <input name="friday_close[]" id="friday_close0" type="text" class="form-control datetimepicker-input" data-target="#friday_closetime" data-toggle="datetimepicker"/>
                                    <div class="input-group-append" data-target="#friday_closetime" data-toggle="datetimepicker">
                                        <div class="input-group-text"><i class="fas fa-clock"></i></div>
                                    </div>
                                </div>
                            </td>
                            <td>
                                <button type="button" class="btn btn-success friday_addmore">Add more</button>
                            </td>
                        </tr>
                    </tbody>
                </table>
                
            </div>
            
        </div>
    </div>
</div>
@endif
@if(isset($all_days["Saturday"]) && count($all_days["Saturday"])>0)
<div class="col-lg-12">
    <div class="card">
        <div class="card-header">
            <h3>Saturday</h3>
        </div>
        <div class="card-body">
            <div class="row">
                <input type="hidden" value="{{count($all_days['Saturday'])}}" id="saturday_count"/>
                <input type="hidden" value="saturday" name="saturday"/>
                <table>
                    <tbody>
                        @foreach($all_days["Saturday"] as $key=>$sat)
                        <tr>
                            <td style="font-weight: 800;">From</td>
                            <td style="font-weight: 800;">To</td>
                            
                            <td></td>
                        </tr>
                        <tr>
                            <td>
                                <div class="input-group date" id="saturday_opentime{{$key}}" data-target-input="nearest">
                                    <input name="saturday_open[]" id="saturday_open{{$key}}" value="{{$sat['open']}}" type="text" class="form-control datetimepicker-input" data-target="#saturday_opentime{{$key}}" data-toggle="datetimepicker"/>
                                    <div class="input-group-append" data-target="#saturday_opentime" data-toggle="datetimepicker">
                                        <div class="input-group-text"><i class="fas fa-clock"></i></div>
                                    </div>
                                </div>
                            </td>
                            <td>
                                <div class="input-group date" id="saturday_closetime{{$key}}" data-target-input="nearest">
                                    <input name="saturday_close[]" id="saturday_close{{$key}}" value="{{$sat['close']}}" type="text" class="form-control datetimepicker-input" data-target="#saturday_closetime{{$key}}" data-toggle="datetimepicker"/>
                                    <div class="input-group-append" data-target="#saturday_closetime" data-toggle="datetimepicker">
                                        <div class="input-group-text"><i class="fas fa-clock"></i></div>
                                    </div>
                                </div>
                            </td>
                            @if($loop->last)
                            <td>
                                <button type="button" class="btn btn-success saturday_addmore">Add more</button>
                            </td>
                            @else
                                <td>
                                    <button type="button" class="btn btn-danger removeMe">Remove</button>
                                </td>
                            @endif
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                
            </div>
            
        </div>
    </div>
</div>
@else
<div class="col-lg-12">
    <div class="card">
        <div class="card-header">
            <h3>Saturday</h3>
        </div>
        <div class="card-body">
            <div class="row">
                <input type="hidden" value="0" id="saturday_count"/>
                <input type="hidden" value="saturday" name="saturday"/>
                <table>
                    <tbody>
                        <tr>
                            <td style="font-weight: 800;">From</td>
                            <td style="font-weight: 800;">To</td>
                            
                            <td></td>
                        </tr>
                        <tr>
                            <td>
                                <div class="input-group date" id="saturday_opentime" data-target-input="nearest">
                                    <input name="saturday_open[]" id="saturday_open0" type="text" class="form-control datetimepicker-input" data-target="#saturday_opentime" data-toggle="datetimepicker"/>
                                    <div class="input-group-append" data-target="#saturday_opentime" data-toggle="datetimepicker">
                                        <div class="input-group-text"><i class="fas fa-clock"></i></div>
                                    </div>
                                </div>
                            </td>
                            <td>
                                <div class="input-group date" id="saturday_closetime" data-target-input="nearest">
                                    <input name="saturday_close[]" id="saturday_close0" type="text" class="form-control datetimepicker-input" data-target="#saturday_closetime" data-toggle="datetimepicker"/>
                                    <div class="input-group-append" data-target="#saturday_closetime" data-toggle="datetimepicker">
                                        <div class="input-group-text"><i class="fas fa-clock"></i></div>
                                    </div>
                                </div>
                            </td>
                            <td>
                                <button type="button" class="btn btn-success saturday_addmore">Add more</button>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endif
@if(isset($all_days["Sunday"]) && count($all_days["Sunday"])>0)
<div class="col-lg-12">
    <div class="card">
        <div class="card-header">
            <h3>Sunday</h3>
        </div>
        <div class="card-body">
            <div class="row">
                <input type="hidden" value="{{count($all_days['Sunday'])}}" id="sunday_count"/>
                <input type="hidden" value="sunday" name="sunday"/>
                <table>
                    <tbody>
                        @foreach($all_days["Sunday"] as $key=>$sun)
                        <tr>
                            <td style="font-weight: 800;">From</td>
                            <td style="font-weight: 800;">To</td>
                            
                            <td></td>
                        </tr>
                        <tr>
                            <td>
                                <div class="input-group date" id="sunday_opentime{{$key}}" data-target-input="nearest">
                                    <input name="sunday_open[]" id="sunday_open{{$key}}" value="{{$sun['open']}}" type="text" class="form-control datetimepicker-input" data-target="#sunday_opentime{{$key}}" data-toggle="datetimepicker"/>
                                    <div class="input-group-append" data-target="#sunday_opentime" data-toggle="datetimepicker">
                                        <div class="input-group-text"><i class="fas fa-clock"></i></div>
                                    </div>
                                </div>
                            </td>
                            <td>
                                <div class="input-group date" id="sunday_closetime{{$key}}" data-target-input="nearest">
                                    <input name="sunday_close[]" id="sunday_close{{$key}}" value="{{$sun['close']}}" type="text" class="form-control datetimepicker-input" data-target="#sunday_closetime{{$key}}" data-toggle="datetimepicker"/>
                                    <div class="input-group-append" data-target="#sunday_closetime" data-toggle="datetimepicker">
                                        <div class="input-group-text"><i class="fas fa-clock"></i></div>
                                    </div>
                                </div>
                            </td>
                            @if($loop->last)
                            <td>
                                <button type="button" class="btn btn-success sunday_addmore">Add more</button>
                            </td>
                            @else
                                <td>
                                    <button type="button" class="btn btn-danger removeMe">Remove</button>
                                </td>
                            @endif
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                
            </div>
            
        </div>
    </div>
</div>
@else
<div class="col-lg-12">
    <div class="card">
        <div class="card-header">
            <h3>Sunday</h3>
        </div>
        <div class="card-body">
            <div class="row">
                <input type="hidden" value="0" id="sunday_count"/>
                <input type="hidden" value="sunday" name="sunday"/>
                <table>
                    <thead>
                    <tbody>
                        <tr>
                            <td style="font-weight: 800;">From</td>
                            <td style="font-weight: 800;">To</td>
                            
                            <td></td>
                        </tr>
                        <tr>
                            <td>
                                <div class="input-group date" id="sunday_opentime" data-target-input="nearest">
                                    <input name="sunday_open[]" id="sunday_open0" type="text" class="form-control datetimepicker-input" data-target="#sunday_opentime" data-toggle="datetimepicker"/>
                                    <div class="input-group-append" data-target="#sunday_opentime" data-toggle="datetimepicker">
                                        <div class="input-group-text"><i class="fas fa-clock"></i></div>
                                    </div>
                                </div>
                            </td>
                            <td>
                                <div class="input-group date" id="sunday_closetime" data-target-input="nearest">
                                    <input name="sunday_close[]" id="sunday_close0" type="text" class="form-control datetimepicker-input" data-target="#sunday_closetime" data-toggle="datetimepicker"/>
                                    <div class="input-group-append" data-target="#sunday_closetime" data-toggle="datetimepicker">
                                        <div class="input-group-text"><i class="fas fa-clock"></i></div>
                                    </div>
                                </div>
                            </td>
                            <td>
                                <button type="button" class="btn btn-success sunday_addmore">Add more</button>
                            </td>
                        </tr>
                    </tbody>
                </table>
                
            </div>
            
        </div>
    </div>
</div>
@endif

@push('third_party_stylesheets')
    <link rel="stylesheet" href="{{ asset('assets/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.css') }}"/>
@endpush

@push('third_party_scripts')
    <script src="{{ asset('assets/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.js') }}"></script>
    <script type="text/javascript">
        $(function () {
            $('.date').datetimepicker({
                format: 'LT'
            });

            $(document).on('click','.monday_addmore',function(){
                var day = $(this)
                var count = $('#monday_count').val();
                count++;
                var remove = '';
                if(count >1){
                    remove = '<button type="button" class="btn btn-danger removeMe">Remove</button>';
                }
                var html = '<tr><td style="font-weight: 800;">From</td><td style="font-weight: 800;">To</td><td></td></tr>'+
                
                            '<tr>'+
                                '<td>'+
                                    '<div class="input-group date" id="monday_opentime'+count+'" data-target-input="nearest">'+
                                        '<input name="monday_open[]" type="text" class="form-control datetimepicker-input" data-target="#monday_opentime'+count+'" id="monday_opentime'+count+'" data-toggle="datetimepicker"/>'+
                                        '<div class="input-group-append" data-target="#monday_opentime'+count+'" data-toggle="datetimepicker">'+
                                            '<div class="input-group-text"><i class="fas fa-clock"></i></div>'+
                                        '</div>'+
                                    '</div>'+
                                '</td>'+
                                '<td>'+
                                    '<div class="input-group date" id="monday_closetime'+count+'" data-target-input="nearest">'+
                                        '<input name="monday_close[]" type="text" class="form-control datetimepicker-input" data-target="#monday_closetime'+count+'" id="monday_closetime'+count+'" data-toggle="datetimepicker"/>'+
                                        '<div class="input-group-append" data-target="#monday_closetime'+count+'" data-toggle="datetimepicker">'+
                                            '<div class="input-group-text"><i class="fas fa-clock"></i></div>'+
                                        '</div>'+
                                    '</div>'+
                                '</td>'+
                                '<td>'+
                                    '<button type="button" class="btn btn-success monday_addmore">Add more</button>'+
                                '</td>'+
                            '</tr>';
                $(this).closest('tbody').append(html);
                $('.date').datetimepicker({
                    format: 'LT'
                });
                $(this).hide();
                $(this).closest('td').append(remove);
                $('#monday_count').val(count);
            })

            $(document).on('click','.tuesday_addmore',function(){
                var day = $(this)
                var count = $('#tuesday_count').val();
                count++;
                var html = '<tr><td style="font-weight: 800;">From</td><td style="font-weight: 800;">To</td><td></td></tr>'+
                            '<tr>'+
                                '<td>'+
                                    '<div class="input-group date" id="tuesday_opentime'+count+'" data-target-input="nearest">'+
                                        '<input name="tuesday_open[]" type="text" class="form-control datetimepicker-input" data-target="#tuesday_opentime'+count+'" id="tuesday_opentime'+count+'" data-toggle="datetimepicker"/>'+
                                        '<div class="input-group-append" data-target="#tuesday_opentime'+count+'" data-toggle="datetimepicker">'+
                                            '<div class="input-group-text"><i class="fas fa-clock"></i></div>'+
                                        '</div>'+
                                    '</div>'+
                                '</td>'+
                                '<td>'+
                                    '<div class="input-group date" id="tuesday_closetime'+count+'" data-target-input="nearest">'+
                                        '<input name="tuesday_close[]" type="text" class="form-control datetimepicker-input" data-target="#tuesday_closetime'+count+'" id="tuesday_closetime'+count+'" data-toggle="datetimepicker"/>'+
                                        '<div class="input-group-append" data-target="#tuesday_closetime'+count+'" data-toggle="datetimepicker">'+
                                            '<div class="input-group-text"><i class="fas fa-clock"></i></div>'+
                                        '</div>'+
                                    '</div>'+
                                '</td>'+
                                '<td>'+
                                    '<button type="button" class="btn btn-success tuesday_addmore">Add more</button>'+
                                '</td>'+
                            '</tr>';
                $(this).closest('tbody').append(html);
                $('.date').datetimepicker({
                    format: 'LT'
                });
                $(this).hide();
                $(this).closest('td').append('<button type="button" class="btn btn-danger removeMe">Remove</button>');
                $('#tuesday_count').val(count);
            })

            $(document).on('click','.wednesday_addmore',function(){
                var day = $(this)
                var count = $('#wednesday_count').val();
                count++;
                var html = '<tr><td style="font-weight: 800;">From</td><td style="font-weight: 800;">To</td><td></td></tr>'+
                
                            '<tr>'+
                                '<td>'+
                                    '<div class="input-group date" id="wednesday_opentime'+count+'" data-target-input="nearest">'+
                                        '<input name="wednesday_open[]" type="text" class="form-control datetimepicker-input" data-target="#wednesday_opentime'+count+'" id="wednesday_opentime'+count+'" data-toggle="datetimepicker"/>'+
                                        '<div class="input-group-append" data-target="#wednesday_opentime'+count+'" data-toggle="datetimepicker">'+
                                            '<div class="input-group-text"><i class="fas fa-clock"></i></div>'+
                                        '</div>'+
                                    '</div>'+
                                '</td>'+
                                '<td>'+
                                    '<div class="input-group date" id="wednesday_closetime'+count+'" data-target-input="nearest">'+
                                        '<input name="wednesday_close[]" type="text" class="form-control datetimepicker-input" data-target="#wednesday_closetime'+count+'" id="wednesday_closetime'+count+'" data-toggle="datetimepicker"/>'+
                                        '<div class="input-group-append" data-target="#wednesday_closetime'+count+'" data-toggle="datetimepicker">'+
                                            '<div class="input-group-text"><i class="fas fa-clock"></i></div>'+
                                        '</div>'+
                                    '</div>'+
                                '</td>'+
                                '<td>'+
                                    '<button type="button" class="btn btn-success wednesday_addmore">Add more</button>'+
                                '</td>'+
                            '</tr>';
                $(this).closest('tbody').append(html);
                $('.date').datetimepicker({
                    format: 'LT'
                });
                $(this).hide();
                $(this).closest('td').append('<button type="button" class="btn btn-danger removeMe">Remove</button>');
                $('#wednesday_count').val(count);
            })

            $(document).on('click','.thursday_addmore',function(){
                var day = $(this)
                var count = $('#thursday_count').val();
                count++;
                var html = '<tr><td style="font-weight: 800;">From</td><td style="font-weight: 800;">To</td><td></td></tr>'+
                
                            '<tr>'+
                                '<td>'+
                                    '<div class="input-group date" id="thursday_opentime'+count+'" data-target-input="nearest">'+
                                        '<input name="thursday_open[]" type="text" class="form-control datetimepicker-input" data-target="#thursday_opentime'+count+'" id="thursday_opentime'+count+'" data-toggle="datetimepicker"/>'+
                                        '<div class="input-group-append" data-target="#thursday_opentime'+count+'" data-toggle="datetimepicker">'+
                                            '<div class="input-group-text"><i class="fas fa-clock"></i></div>'+
                                        '</div>'+
                                    '</div>'+
                                '</td>'+
                                '<td>'+
                                    '<div class="input-group date" id="thursday_closetime'+count+'" data-target-input="nearest">'+
                                        '<input name="thursday_close[]" type="text" class="form-control datetimepicker-input" data-target="#thursday_closetime'+count+'" id="thursday_closetime'+count+'" data-toggle="datetimepicker"/>'+
                                        '<div class="input-group-append" data-target="#thursday_closetime'+count+'" data-toggle="datetimepicker">'+
                                            '<div class="input-group-text"><i class="fas fa-clock"></i></div>'+
                                        '</div>'+
                                    '</div>'+
                                '</td>'+
                                '<td>'+
                                    '<button type="button" class="btn btn-success thursday_addmore">Add more</button>'+
                                '</td>'+
                            '</tr>';
                $(this).closest('tbody').append(html);
                $('.date').datetimepicker({
                    format: 'LT'
                });
                $(this).hide();
                $(this).closest('td').append('<button type="button" class="btn btn-danger removeMe">Remove</button>');
                $('#thursday_count').val(count);
            })

            $(document).on('click','.friday_addmore',function(){
                var day = $(this)
                var count = $('#friday_count').val();
                count++;
                var html = '<tr><td style="font-weight: 800;">From</td><td style="font-weight: 800;">To</td><td></td></tr>'+
                
                            '<tr>'+
                                '<td>'+
                                    '<div class="input-group date" id="friday_opentime'+count+'" data-target-input="nearest">'+
                                        '<input name="friday_open[]" type="text" class="form-control datetimepicker-input" data-target="#friday_opentime'+count+'" id="friday_opentime'+count+'" data-toggle="datetimepicker"/>'+
                                        '<div class="input-group-append" data-target="#friday_opentime'+count+'" data-toggle="datetimepicker">'+
                                            '<div class="input-group-text"><i class="fas fa-clock"></i></div>'+
                                        '</div>'+
                                    '</div>'+
                                '</td>'+
                                '<td>'+
                                    '<div class="input-group date" id="friday_closetime'+count+'" data-target-input="nearest">'+
                                        '<input name="friday_close[]" type="text" class="form-control datetimepicker-input" data-target="#friday_closetime'+count+'" id="friday_closetime'+count+'" data-toggle="datetimepicker"/>'+
                                        '<div class="input-group-append" data-target="#friday_closetime'+count+'" data-toggle="datetimepicker">'+
                                            '<div class="input-group-text"><i class="fas fa-clock"></i></div>'+
                                        '</div>'+
                                    '</div>'+
                                '</td>'+
                                '<td>'+
                                    '<button type="button" class="btn btn-success friday_addmore">Add more</button>'+
                                '</td>'+
                            '</tr>';
                $(this).closest('tbody').append(html);
                $('.date').datetimepicker({
                    format: 'LT'
                });
                $(this).hide();
                $(this).closest('td').append('<button type="button" class="btn btn-danger removeMe">Remove</button>');
                $('#friday_count').val(count);
            })

            $(document).on('click','.saturday_addmore',function(){
                var day = $(this)
                var count = $('#saturday_count').val();
                count++;
                var html = '<tr><td style="font-weight: 800;">From</td><td style="font-weight: 800;">To</td><td></td></tr>'+
                
                            '<tr>'+
                                '<td>'+
                                    '<div class="input-group date" id="saturday_opentime'+count+'" data-target-input="nearest">'+
                                        '<input name="saturday_open[]" type="text" class="form-control datetimepicker-input" data-target="#saturday_opentime'+count+'" id="saturday_opentime'+count+'" data-toggle="datetimepicker"/>'+
                                        '<div class="input-group-append" data-target="#saturday_opentime'+count+'" data-toggle="datetimepicker">'+
                                            '<div class="input-group-text"><i class="fas fa-clock"></i></div>'+
                                        '</div>'+
                                    '</div>'+
                                '</td>'+
                                '<td>'+
                                    '<div class="input-group date" id="saturday_closetime'+count+'" data-target-input="nearest">'+
                                        '<input name="saturday_close[]" type="text" class="form-control datetimepicker-input" data-target="#saturday_closetime'+count+'" id="saturday_closetime'+count+'" data-toggle="datetimepicker"/>'+
                                        '<div class="input-group-append" data-target="#saturday_closetime'+count+'" data-toggle="datetimepicker">'+
                                            '<div class="input-group-text"><i class="fas fa-clock"></i></div>'+
                                        '</div>'+
                                    '</div>'+
                                '</td>'+
                                '<td>'+
                                    '<button type="button" class="btn btn-success saturday_addmore">Add more</button>'+
                                '</td>'+
                            '</tr>';
                $(this).closest('tbody').append(html);
                $('.date').datetimepicker({
                    format: 'LT'
                });
                $(this).hide();
                $(this).closest('td').append('<button type="button" class="btn btn-danger removeMe">Remove</button>');
                $('#saturday_count').val(count);
            })

            $(document).on('click','.sunday_addmore',function(){
                var day = $(this)
                var count = $('#sunday_count').val();
                count++;
                var html = '<tr><td style="font-weight: 800;">From</td><td style="font-weight: 800;">To</td><td></td></tr>'+
                            '<tr>'+
                                '<td>'+
                                    '<div class="input-group date" id="sunday_opentime'+count+'" data-target-input="nearest">'+
                                        '<input name="sunday_open[]" type="text" class="form-control datetimepicker-input" data-target="#sunday_opentime'+count+'" id="sunday_opentime'+count+'" data-toggle="datetimepicker"/>'+
                                        '<div class="input-group-append" data-target="#sunday_opentime'+count+'" data-toggle="datetimepicker">'+
                                            '<div class="input-group-text"><i class="fas fa-clock"></i></div>'+
                                        '</div>'+
                                    '</div>'+
                                '</td>'+
                                '<td>'+
                                    '<div class="input-group date" id="sunday_closetime'+count+'" data-target-input="nearest">'+
                                        '<input name="sunday_close[]" type="text" class="form-control datetimepicker-input" data-target="#sunday_closetime'+count+'" id="sunday_closetime'+count+'" data-toggle="datetimepicker"/>'+
                                        '<div class="input-group-append" data-target="#sunday_closetime'+count+'" data-toggle="datetimepicker">'+
                                            '<div class="input-group-text"><i class="fas fa-clock"></i></div>'+
                                        '</div>'+
                                    '</div>'+
                                '</td>'+
                                '<td>'+
                                    '<button type="button" class="btn btn-success sunday_addmore">Add more</button>'+
                                '</td>'+
                            '</tr>';
                $(this).closest('tbody').append(html);
                $('.date').datetimepicker({
                    format: 'LT'
                });
                $(this).hide();
                $(this).closest('td').append('<button type="button" class="btn btn-danger removeMe">Remove</button>');
                $('#sunday_count').val(count);
            })

            $(document).on('click','.removeMe',function(){
                $(this).closest('tr').prev('tr').remove();
                $(this).closest('tr').remove();
            });

            $(document).on('click','.copy_all',function(){

                var mon_count = $('#monday_count').val();
                var tue_count = $('#tuesday_count').val();
                var wed_count = $('#wednesday_count').val();
                var thur_count = $('#thursday_count').val();
                var fri_count = $('#friday_count').val();
                var sat_count = $('#saturday_count').val();
                var sun_count = $('#sunday_count').val();
                var open = $('#monday_open0').val();
                var close = $('#monday_close0').val();
                if(tue_count == 0){
                    $('#tuesday_open0').val(open);
                    $('#tuesday_close0').val(close);
                }
                if(wed_count == 0){
                    $('#wednesday_open0').val(open);
                    $('#wednesday_close0').val(close);
                }
                if(thur_count == 0){
                    $('#thursday_open0').val(open);
                    $('#thursday_close0').val(close);
                }
                if(fri_count == 0){
                    $('#friday_open0').val(open);
                    $('#friday_close0').val(close);
                }
                if(sat_count == 0){
                    $('#saturday_open0').val(open);
                    $('#saturday_close0').val(close);
                }
                if(sun_count == 0){
                    $('#sunday_open0').val(open);
                    $('#sunday_close0').val(close);
                }

                for(i=0;i<mon_count;i++){
                    $('#monday_open'+i).val(open);
                    $('#monday_close'+i).val(close);
                }
                for(j=0;j<tue_count;j++){
                    $('#tuesday_open'+j).val(open);
                    $('#tuesday_close'+j).val(close);
                }
                for(k=0;k<wed_count;k++){
                    $('#wednesday_open'+k).val(open);
                    $('#wednesday_close'+k).val(close);
                }
                for(l=0;l<thur_count;l++){
                    $('#thursday_open'+l).val(open);
                    $('#thursday_close'+l).val(close);
                }
                for(m=0;m<fri_count;m++){
                    $('#friday_open'+m).val(open);
                    $('#friday_close'+m).val(close);
                }
                for(n=0;n<sat_count;n++){
                    $('#saturday_open'+n).val(open);
                    $('#saturday_close'+n).val(close);
                }
                for(o=0;o<sun_count;o++){
                    $('#sunday_open'+o).val(open);
                    $('#sunday_close'+o).val(close);
                }
            })
        });
    </script>
@endpush


