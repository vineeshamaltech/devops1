{!! Form::open(['route' => ['admin.storeTimings.destroy', $id], 'method' => 'delete']) !!}
<div class='btn-group'>
    @can('storeTimings.view')
    <a href="{{ route('admin.storeTimings.show', $id) }}" class='btn btn-default btn-xs'>
        <i class="fa fa-eye"></i>
    </a>
    @endcan
    @can('storeTimings.edit')
    <a href="{{ route('admin.storeTimings.edit', $id) }}" class='btn btn-default btn-xs'>
        <i class="fa fa-edit"></i>
    </a>
    @endcan
    @can('storeTimings.delete')
    {!! Form::button('<i class="fa fa-trash"></i>', [
        'type' => 'submit',
        'class' => 'btn btn-danger btn-xs',
        'onclick' => 'return confirm("'.__('crud.are_you_sure').'")'
    ]) !!}
    @endcan
</div>
{!! Form::close() !!}
