{!! Form::open(['route' => ['admin.earnings.destroy', $id], 'method' => 'delete']) !!}
<div class='btn-group'>
    @can('earnings.view')
    @if($order_id != null)
    <a href="{{ route('admin.orders.show', $id) }}" class='btn btn-default btn-xs'>
        <i class="fa fa-eye"></i>
    </a>
    @else
    <a href="{{ route('admin.earnings.show', $id) }}" class='btn btn-default btn-xs'>
        <i class="fa fa-eye"></i>
    </a>
    @endif
    
    @endcan
    @can('earnings.edit')
    <a href="{{ route('admin.earnings.edit', $id) }}" class='btn btn-default btn-xs'>
        <i class="fa fa-edit"></i>
    </a>
    @endcan
    @can('earnings.delete')
    {!! Form::button('<i class="fa fa-trash"></i>', [
        'type' => 'submit',
        'class' => 'btn btn-danger btn-xs',
        'onclick' => 'return confirm("'.__('crud.are_you_sure').'")'
    ]) !!}
    @endcan
</div>
{!! Form::close() !!}
