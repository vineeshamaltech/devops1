@extends('admin.layouts.app')

@section('title',trans('lang.edit')." ".trans('models/earnings.singular'))

@section('content')
    <div class="content">

        @include('adminlte-templates::common.errors')

        <div class="card">

            {!! Form::model($earning, ['route' => ['admin.earnings.update', $earning->id], 'method' => 'patch']) !!}

            <div class="card-body">
                <div class="row">
                    @include('admin.earnings.fields')
                </div>
            </div>

            <div class="card-footer">
                {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
                <a href="{{ route('admin.earnings.index') }}" class="btn btn-default">
                    @lang('lang.cancel')
                 </a>
            </div>

            {!! Form::close() !!}

        </div>
    </div>
@endsection