@extends('admin.layouts.app')
@section('title','Earnings')
@section('content')
    <div class="content">

        @include('flash::message')

        <div class="clearfix"></div>

        <div class="card">
            <div class="card-header">
                <ul class="nav nav-tabs align-items-end card-header-tabs w-100">
                    <li class="nav-item">
                        <a class="nav-link active" href="{!! url()->current() !!}"><i class="fa fa-list mr-2"></i>{{trans('models/earnings.plural')}}</a>
                    </li>
{{--                    @can('earnings.create')--}}
{{--                    <li class="nav-item">--}}
{{--                        <a class="nav-link" href="{!! route('admin.earnings.create') !!}"><i class="fa fa-plus mr-2"></i>{{trans('lang.add')." ".trans('models/earnings.singular')}}</a>--}}
{{--                    </li>--}}
{{--                    @endcan--}}
                    @include('admin.layouts.datatables_toolbar', compact('dataTable'))
                </ul>
            </div>

            <div class="card-body">
                <div class="row">
                    <div class="col-lg-3 col-6">
                    <!-- small box -->
                    <div class="small-box bg-info">
                        <div class="inner">
                            <h5>Total Admin Commission :&nbsp;&nbsp; <b>₹{{$data['total_incentive']}}</b></h5>
                            <h5>Today Admin Commission :&nbsp;&nbsp;<b>₹{{$data['today_incentive']}}</b></h5>
                        </div>

                        <div class="icon">
                            <i class="ion ion-bag"></i>
                        </div>
                        <!-- <a href="{{ route('admin.users.index') }}" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a> -->
                        </div>
                    </div>
                    <div class="col-lg-3 col-6">
                    <!-- small box -->
                    <div class="small-box bg-info">
                        <div class="inner">
                            <h5>Total Store Earning :&nbsp;&nbsp; <b>₹{{$data['total_store_earning']}}</b></h5>
                            <h5>Today Store Earning :&nbsp;&nbsp;<b>₹{{$data['today_store_earning']}}</b></h5>
                        </div>

                        <div class="icon">
                            <i class="ion ion-bag"></i>
                        </div>
                        <!-- <a href="{{ route('admin.users.index') }}" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a> -->
                        </div>
                    </div>
                    <div class="col-lg-3 col-6">
                    <!-- small box -->
                    <div class="small-box bg-info">
                        <div class="inner">
                            <h5>Total Driver Earning :&nbsp;&nbsp; <b>₹{{$data['total_driver_earning']}}</b></h5>
                            <h5>Today Driver Earning :&nbsp;&nbsp;<b>₹{{$data['today_driver_earning']}}</b></h5>
                        </div>

                        <div class="icon">
                            <i class="ion ion-bag"></i>
                        </div>
                        <!-- <a href="{{ route('admin.users.index') }}" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a> -->
                        </div>
                    </div>
                    <div class="col-lg-3 col-6">
                    <!-- small box -->
                    <div class="small-box bg-info">
                        <div class="inner">
                            <h5>Total Incentives :&nbsp;&nbsp; <b>₹{{$data['total_incentive']}}</b></h5>
                            <h5>Today Incentives :&nbsp;&nbsp;<b>₹{{$data['today_incentive']}}</b></h5>
                        </div>

                        <div class="icon">
                            <i class="ion ion-bag"></i>
                        </div>
                        <!-- <a href="{{ route('admin.users.index') }}" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a> -->
                        </div>
                    </div>
                </div>
                {!! Form::open(['route' => ['admin.destroy_multiple_earning'], 'method' => 'post']) !!}
                    <button type="submit" class="btn btn-danger deleteMultiple" onclick="return confirm('Are you sure?')">Delete Multiple</button>
                    <hr>
                    @include('admin.earnings.table')
                {!! Form::close() !!}
                

                <div class="card-footer clearfix float-right">
                    <div class="float-right">

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection


