<!-- Payment Id Field -->
<div class="col-sm-12">
    {!! Form::label('payment_id', __('models/earnings.fields.payment_id').':') !!}
    <p>{{ $earning->payment_id }}</p>
</div>

<!-- Credited To Field -->
<div class="col-sm-12">
    {!! Form::label('credited_to', __('models/earnings.fields.credited_to').':') !!}
    <p>{{ $earning->credited_to }}</p>
</div>

<!-- Amount Field -->
<div class="col-sm-12">
    {!! Form::label('amount', __('models/earnings.fields.amount').':') !!}
    <p>{{ $earning->amount }}</p>
</div>

<!-- Remarks Field -->
<div class="col-sm-12">
    {!! Form::label('remarks', __('models/earnings.fields.remarks').':') !!}
    <p>{{ $earning->remarks }}</p>
</div>

<!-- Is Settled Field -->
<div class="col-sm-12">
    {!! Form::label('is_settled', __('models/earnings.fields.is_settled').':') !!}
    <p>{{ $earning->is_settled }}</p>
</div>

<!-- Created At Field -->
<div class="col-sm-12">
    {!! Form::label('created_at', __('models/earnings.fields.created_at').':') !!}
    <p>{{ $earning->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="col-sm-12">
    {!! Form::label('updated_at', __('models/earnings.fields.updated_at').':') !!}
    <p>{{ $earning->updated_at }}</p>
</div>

