{{--<!-- Payment Id Field -->--}}
{{--<div class="form-group col-sm-6">--}}
{{--    {!! Form::label('payment_id', __('models/earnings.fields.payment_id').':') !!}--}}
{{--    {!! Form::select('payment_id', ], null, ['class' => 'form-control select2']) !!}--}}
{{--</div>--}}


{{--<!-- Credited To Field -->--}}
{{--<div class="form-group col-sm-6">--}}
{{--    {!! Form::label('credited_to', __('models/earnings.fields.credited_to').':') !!}--}}
{{--    {!! Form::select('credited_to', [], null, ['class' => 'form-control select2']) !!}--}}
{{--</div>--}}

<!-- Amount Field -->
<div class="form-group col-sm-6">
    {!! Form::label('entity_id', __('models/earnings.fields.entity_id').':') !!}
    {!! Form::number('entity_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Remarks Field -->
<div class="form-group col-sm-6">
    {!! Form::label('remarks', __('models/earnings.fields.remarks').':') !!}
    {!! Form::text('remarks', null, ['class' => 'form-control']) !!}
</div>

<!-- Is Settled Field -->
<div class="form-group col-sm-6">
    <div class="form-check">
        {!! Form::hidden('is_settled', 0, ['class' => 'form-check-input']) !!}
        {!! Form::checkbox('is_settled', '1', null, ['class' => 'form-check-input']) !!}
        {!! Form::label('is_settled', __('models/earnings.fields.is_settled'), ['class' => 'form-check-label']) !!}
    </div>
</div>
