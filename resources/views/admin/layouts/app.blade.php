<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>@yield('title') | {{ setting('app_name') }}</title>
    <!-- Google Font: Source Sans Pro -->
    <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
    <link rel="stylesheet" href="{{ asset('assets/plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/plugins/toastr/toastr.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/adminlte.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/plugins/select2/css/select2.css') }}">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@fortawesome/fontawesome-free@5.15.4/css/all.min.css">
    <link rel="stylesheet" href="{{ asset('assets/css/global.css') }}">

    @stack('third_party_stylesheets')

    @stack('page_css')
</head>

<body class="hold-transition sidebar-mini layout-fixed @if (setting('dark_mode', 0)) dark-mode @endif">




    <div class="wrapper">
        <!-- Main Header -->
        @impersonating($guard = null)
        <div role="alert" class="main-header alert alert-warning alert-dismissible fade show"
            style="border-radius: 0px;">
            You are logged in as <strong>{{ auth()->user()->name }}</strong>.
            <a href="{{ route('admin.impersonate.leave') }}" class="close" aria-label="Close">
                <span aria-hidden="true">×</span>
            </a>
        </div>
        @endImpersonating
        <nav
            class="main-header navbar navbar-expand @if (setting('dark_mode', 0)) navbar-dark @else  navbar-white navbar-light @endif">
            <!-- Left navbar links -->
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i
                            class="fas fa-bars"></i></a>
                </li>
                <li class="nav-item d-none d-sm-inline-block">
                    <a href="{{ config('app.url') }}" class="nav-link">Home</a>
                </li>
                <!-- <li class="nav-item d-none d-sm-inline-block">
                    <a href="#" class="nav-link">Contact</a>
                </li> -->
            </ul>

            <ul class="navbar-nav ml-auto">
                <!-- Notifications Dropdown Menu -->
                <li class="nav-item dropdown">
                    <!-- <a class="nav-link" data-toggle="dropdown" href="#">
                        <i class="far fa-bell"></i>
                        <span class="badge badge-warning navbar-badge">15</span>
                    </a> -->
                    <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                        <span class="dropdown-header">15 Notifications</span>
                        <div class="dropdown-divider"></div>
                        <a href="#" class="dropdown-item">
                            <i class="fas fa-envelope mr-2"></i> 4 new messages
                            <span class="float-right text-muted text-sm">3 mins</span>
                        </a>
                        <div class="dropdown-divider"></div>
                        <a href="#" class="dropdown-item">
                            <i class="fas fa-users mr-2"></i> 8 friend requests
                            <span class="float-right text-muted text-sm">12 hours</span>
                        </a>
                        <div class="dropdown-divider"></div>
                        <a href="#" class="dropdown-item">
                            <i class="fas fa-file mr-2"></i> 3 new reports
                            <span class="float-right text-muted text-sm">2 days</span>
                        </a>
                        <div class="dropdown-divider"></div>
                        <a href="#" class="dropdown-item dropdown-footer">See All Notifications</a>
                    </div>
                </li>

                <li class="nav-item dropdown user-menu">
                    <a href="#" onclick="showdrop()" class="nav-link dropdown-toggle" data-toggle="dropdown">
                        <img onerror="this.onerror=null;this.src='https://ui-avatars.com/api/?name={{ Auth::user()->name }}';"
                            src="{{ Auth::user()->profile_img }}" class="user-image img-circle" alt="User Image">
                        <span class="d-none d-md-inline">{{ Auth::user()->name }}</span>
                    </a>
                    
                    <div class="dropdown-menu" id="logoutclick">
                        <a class="dropdown-item" id="logoutBtn" href="#"
                            onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Sign
                            out</a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                            @csrf
                        </form>
                    </div>
                </li>
            </ul>
        </nav>

        <!-- Left side column. contains the logo and sidebar -->
        @include('admin.layouts.sidebar')



        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <div class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            <h1 class="m-0">@yield('title')</h1>
                        </div><!-- /.col -->
                        <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item"><a href="{{ config('app.url') }}">Home</a></li>
                                <li class="breadcrumb-item active">@yield('title')</li>
                            </ol>
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div><!-- /.container-fluid -->
            </div>
            <section class="content">
                @yield('content')
            </section>
        </div>


        <button type="button" class="btn btn-primary" id="modalbtn" data-toggle="modal" data-target="#exampleModal"
            hidden>

        </button>


        <div class="modal fade right" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
        aria-hidden="true" data-backdrop="false">
        <div class="modal-dialog modal-side modal-bottom-right modal-notify modal-info" role="document">
          <!--Content-->
          <div class="modal-content">
            <!--Header-->
            <div class="modal-header bg-primary pt-2">
              <p class="heading txt-cl">New Orders !!!
              </p>
      
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true" class="white-text">&times;</span>
              </button>
            </div>
      
            <!--Body-->
            <div class="modal-body">
      
                <div class="modal-body text-center" style="align:center;">
                    ...
                </div>
            </div>
      
            <!--Footer-->
            <div class="modal-footer justify-content-center">
                <button type="button" id="orderview" class="btn btn-primary">View</button>
                <button type="button" id="closemodal" class="btn btn-secondary"
                    data-dismiss="modal">Close</button>
            </div>
          </div>
          <!--/.Content-->
        </div>
      </div>






        {{-- <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
            aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Order Notification</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body text-center admin-notif" style="align:center;">
                        ...
                    </div>
                    <div class="modal-footer">
                        <button type="button" id="orderview" class="btn btn-primary">View</button>
                        <button type="button" id="closemodal" class="btn btn-secondary"
                            data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div> --}}
        <div class="modal-body text-center" id="orderid" style="align:center;" hidden>
            0
        </div>
        <!-- Main Footer -->
        <footer class="main-footer">
            <div class="float-right d-none d-sm-block">
                <b>Version</b> {{ config('app.version') }}
            </div>
            <strong>
                Copyright &copy; @php echo date('Y') @endphp <a href="{{ config('app.url') }}">{{ config('app.name') }}</a>.
            </strong>
            All rights reserved.
        </footer>
    </div>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"
        integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg=="
        crossorigin="anonymous"></script>

    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"
        integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous">
    </script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment.min.js"
        integrity="sha512-qTXRIMyZIFb8iQcfjXWCO8+M5Tbc38Qi5WzdPOYZHIlZpzBHG3L3by84BBBOiRGiEb7KKtAOAs5qYdUiZiQNNQ=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.min.js"
        integrity="sha384-w1Q4orYjBQndcko6MimVbzY0tgp4pWB4lZ7lr30WKz0vr/aWKhXdBNmNb5D92v7s" crossorigin="anonymous">
    </script>

    <script src="https://cdn.jsdelivr.net/npm/bs-custom-file-input/dist/bs-custom-file-input.min.js"></script>

    <script src="{{ asset('assets/js/adminlte.js') }}"></script>
    <script defer src="https://unpkg.com/alpinejs@3.x.x/dist/cdn.min.js"></script>
    {{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"
        integrity="sha512-GDey37RZAxFkpFeJorEUwNoIbkTwsyC736KNSYucu1WJWFK9qTdzYub8ATxktr6Dwke7nbFaioypzbDOQykoRg=="
        crossorigin="anonymous"></script> --}}

    <script src="{{ asset('assets/plugins/select2/js/select2.js') }}"></script>
    <script src="{{ asset('assets/plugins/icheck-bootstrap/icheck.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/toastr/toastr.min.js') }}"></script>
    <script src="{{ asset('assets/js/global.js') }}"></script>
    <script>
    
       $('.alert').fadeIn('slow', function(){
               $('.alert').delay(5000).fadeOut(); 
            });
        function showdrop()
        {
         
             document.getElementById("logoutclick").classList.toggle("show");
        }
        $(function() {
            bsCustomFileInput.init();
        });

        $("input[data-bootstrap-switch]").each(function() {
            $(this).bootstrapSwitch('state', $(this).prop('checked'));
        });


        $(document).ready(function() {

            var pathname = window.location.origin;

            var count = 0

            $('#orderview').on('click', function() {

                window.location = '/admin/orders';

            })




            setInterval(function() {

                @if (auth()->user()->type == 'Vendor')
                    {
                
                    showNotification()
                
                    }
                @endif

            }, 5000);





            function showNotification() {

                var audio = new Audio(pathname + '/sound/notification.wav');

                // audio.play();

                $.ajax({
                        url: "/getorders",

                        // data: {

                        //         success: function(data) {
                        //             console.log(data)
                        //             if(data < 1)
                        //             {
                        //                 console.log(data)
                        //                 $('#closemodal').click();
                        //             }
                        //             $('.modal-body').html('Hey you have ' + data + ' orders!!!')


                        //                 $('#modalbtn').click();

                        //         }

                        // }
                    })
                    .done(function(data, textStatus, jqXHR) {
                        
                            $('.admin-notif').html('Hey you have ' + data + ' new orders!!!')

                            if (data < 1) {

                                $('#modalbtn').on('click', (e) => {
                                    e.stopPropagation();

                                
                                });

                             
                                $('#closemodal').click();
                            }
                            else
                            {
                                $('#modalbtn').click();
                                audio.play()
                            }
                        }

                    );
            }


        });
    </script>

    @stack('third_party_scripts')

    @stack('page_scripts')
</body>

</html>
