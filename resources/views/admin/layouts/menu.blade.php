<li class="nav-item">
    <a href="{{ route('admin.dashboard') }}" class="nav-link {{ Request::is('admin') ? 'active' : '' }}">
        <i class="nav-icon fas fa-tachometer-alt"></i>
        <p>Dashboard</p>
    </a>
</li>
@can('orders.index')
    <li class="nav-item">
        <a href="{{ route('admin.orders.index') }}"
            class="nav-link {{ Request::is('admin/orders*') ? 'active' : '' }}">
            <i class="nav-icon fas fa-clipboard-list"></i>

            <p>@lang('models/orders.plural')</p>
        </a>
    </li>
@endcan
@canany(['vendors.index', 'vendorsKyc.index', 'admins.index'])
    <li class="nav-item has-treeview @if (Request::is('admin/vendors*') || Request::is('admin/vendorKycs*') || Request::is('admin/admins*')) menu-open @endif">
        <a href="#" class="nav-link @if (Request::is('admin/vendors*') || Request::is('admin/vendorKycs*') || Request::is('admin/admins*')) active @endif">
            <i class="nav-icon fas fa-user-shield"></i>
            <p>
                Admin & Vendors
                <i class="right fas fa-angle-right"></i>
            </p>
        </a>
        <ul class="nav nav-treeview">
            @can('admins.index')
                <li class="nav-item">
                    <a href="{{ route('admin.admins.index') }}"
                        class="nav-link {{ Request::is('admin/admins*') ? 'active' : '' }}">
                        <i class="nav-icon fas fa-user-secret"></i>
                        <p>@lang('models/admins.plural')</p>
                    </a>
                </li>
            @endcan
            {{-- @can('vendors.index') --}}
            {{-- <li class="nav-item"> --}}
            {{-- <a href="{{ route('admin.vendors.index') }}" --}}
            {{-- class="nav-link {{ Request::is('admin/vendors*') ? 'active' : '' }}"> --}}
            {{-- <i class="nav-icon fas fa-user-ninja"></i> --}}
            {{-- <p>@lang('models/vendors.plural')</p> --}}
            {{-- </a> --}}
            {{-- </li> --}}
            {{-- @endcan --}}
            @can('vendorsKyc.index')
                <li class="nav-item">
                    <a href="{{ route('admin.vendorKycs.index') }}"
                        class="nav-link {{ Request::is('admin/vendorKycs*') ? 'active' : '' }}">
                        <i class="nav-icon fas fa-file-signature"></i>
                        <p>@lang('models/vendorKycs.plural')</p>
                    </a>
                </li>
            @endcan
        </ul>
    </li>
@endcan
@if (config('constants.features.purchasing_service', false))
    @canany(['stores.index', 'storeTimings.index', 'coupons.index'])
        <li class="nav-item has-treeview @if (Request::is('admin/stores*') || Request::is('admin/storeTimings*') || Request::is('admin/coupons*')) menu-open @endif">
            <a href="#" class="nav-link @if (Request::is('admin/stores*') || Request::is('admin/storeTimings*') || Request::is('admin/coupons*')) active @endif">
                <i class="nav-icon fas fa-store-alt"></i>
                <p>
                    Store Management
                    <i class="right fas fa-angle-right"></i>
                </p>
            </a>
            <ul class="nav nav-treeview">
                @can('stores.index')
                    <li class="nav-item">
                        <a href="{{ route('admin.stores.index') }}"
                            class="nav-link {{ Request::is('admin/stores*') ? 'active' : '' }}">
                            <i class="nav-icon fas fa-store"></i>
                            <p>@lang('models/stores.plural')</p>
                        </a>
                    </li>
                @endcan

                @can('storeTimings.index')
                    <li class="nav-item">
                        <a href="{{ route('admin.storeTimings.index') }}"
                            class="nav-link {{ Request::is('admin/storeTimings*') ? 'active' : '' }}">
                            <i class="nav-icon fas fa-clock"></i>
                            <p>@lang('models/storeTimings.plural')</p>
                        </a>
                    </li>
                @endcan

                @can('coupons.index')
                    <li class="nav-item">
                        <a href="{{ route('admin.coupons.index') }}"
                            class="nav-link {{ Request::is('admin/coupons*') ? 'active' : '' }}">
                            <i class="nav-icon fas fa-percentage"></i>
                            <p>@lang('models/coupons.plural')</p>
                        </a>
                    </li>
                @endcan
            </ul>
        </li>
    @endcan
@endif
@canany(['products.index', 'productVariants.index', 'categories.index'])
    <li class="nav-item has-treeview @if (Request::is('admin/products*') || Request::is('admin/productVariants*') || Request::is('admin/categories*')) menu-open @endif ">
        <a href="#" class="nav-link {{(Request::is('admin/products*') || Request::is('admin/productVariants*') || Request::is('admin/categories*'))  ? 'active' : '' }}">
            <i class="nav-icon fas fa-archive"></i>
            <p>
                Product Management
                <i class="right fas fa-angle-right"></i>
            </p>
        </a>
        <ul class="nav nav-treeview">
            @can('categories.index')
                <li class="nav-item">
                    <a href="{{ route('admin.categories.index') }}"
                        class="nav-link {{ Request::is('admin/categories*') ? 'active' : '' }}">
                        <i class="nav-icon fas fa-list-ol"></i>
                        <p>@lang('models/categories.plural')</p>
                    </a>
                </li>
            @endcan
            @can('products.index')
                <li class="nav-item">
                    <a href="{{ route('admin.products.index') }}"
                        class="nav-link {{ Request::is('admin/products*') ? 'active' : '' }}">
                        <i class="nav-icon fas fa-archive"></i>
                        <p>@lang('models/products.plural')</p>
                    </a>
                </li>
            @endcan
            @can('productVariants.index')
                <li class="nav-item">
                    <a href="{{ route('admin.productVariants.index') }}"
                        class="nav-link {{ Request::is('admin/productVariants*') ? 'active' : '' }}">
                        <i class="nav-icon fas fa-cubes"></i>
                        <p>@lang('models/productVariants.plural')</p>
                    </a>
                </li>
            @endcan

        </ul>
    </li>
@endcan
@canany(['serviceCategories.index', 'storeCategories.index'])
    <li class="nav-item has-treeview @if (Request::is('admin/serviceCategories*') || Request::is('admin/storeCategories*')) menu-open @endif ">
        <a href="#" class="nav-link {{ (Request::is('admin/serviceCategories*') || Request::is('admin/storeCategories*'))  ? 'active' : '' }} ">
            <i class="nav-icon fas fa-diagnoses"></i>
            <p>
                Services
                <i class="right fas fa-angle-right"></i>
            </p>
        </a>
        <ul class="nav nav-treeview">
            @can('serviceCategories.index')
                <li class="nav-item">
                    <a href="{{ route('admin.serviceCategories.index') }}"
                        class="nav-link {{ Request::is('admin/serviceCategories*') ? 'active' : '' }}">
                        <i class="nav-icon fas fa-toolbox"></i>
                        <p>@lang('models/serviceCategories.plural')</p>
                    </a>
                </li>
            @endcan
            @if (config('constants.features.purchasing_service', false))
                @can('storeCategories.index')
                    <li class="nav-item">
                        <a href="{{ route('admin.storeCategories.index') }}"
                            class="nav-link {{ Request::is('admin/storeCategories*') ? 'active' : '' }}">
                            <i class="nav-icon fas fa-project-diagram"></i>
                            <p>@lang('models/storeCategories.plural')</p>
                        </a>
                    </li>
                @endcan
            @endif
        </ul>
    </li>
@endcan

{{-- @if (config('constants.features.home_service_booking', false)) --}}
@if(setting('home_service_show') == 1 )
    @canany(['homeServices.index', 'homeServiceCategories.index', 'homeServiceOrders.index','homeServiceTimeSlots.index'])
        <li class="nav-header">SERVICE BOOKING</li>
        @can('homeServices.index')
            <li class="nav-item">
                <a href="{{ route('admin.homeServices.index') }}"
                    class="nav-link {{ Request::is('admin/homeServices*') ? 'active' : '' }}">
                    <i class="nav-icon fas fa-tree"></i>
                    <p>@lang('models/homeServices.plural')</p>
                </a>
            </li>
        @endcan
        @can('homeServiceCategories.index')
            <li class="nav-item">
                <a href="{{ route('admin.homeServiceCategories.index') }}"
                    class="nav-link {{ Request::is('admin/homeServiceCategories*') ? 'active' : '' }}">
                    <i class="nav-icon fas fa-tree"></i>
                    <p>@lang('models/homeServiceCategories.plural')</p>
                </a>
            </li>
        @endcan
        @can('homeServiceOrders.index')
            <li class="nav-item">
                <a href="{{ route('admin.homeOrders.index') }}"
                    class="nav-link {{ Request::is('admin/homeOrders*') ? 'active' : '' }}">
                    <i class="nav-icon fas fa-shopping-cart"></i>
                    <p>@lang('models/homeServiceOrders.plural')</p>
                </a>
            </li>
        @endcan
        @can('homeServiceTimeSlots.index')
            <li class="nav-item">
                <a href="{{ route('admin.timeSlots.index') }}"
                    class="nav-link {{ Request::is('admin/timeSlots*') ? 'active' : '' }}">
                    <i class="nav-icon fa fa-clock"></i>
                    <p>Home Services TimeSlots</p>
                </a>
            </li>
        @endcan
    @endcanany
@endif
{{-- @if (config('constants.features.car_pool_booking', false)) --}}
@if(setting('car_pool_show') == 1 )
    @canany(['rides.index', 'rideBookings.index'])
        <li class="nav-header">CAR POOLING</li>
        @can('rides.index')
            <li class="nav-item">
                <a href="{{ route('admin.rides.index') }}"
                    class="nav-link {{ Request::is('admin/carpool/rides*') ? 'active' : '' }}">
                    <i class="nav-icon fas fa-car"></i>
                    <p>@lang('models/rides.plural')</p>
                </a>
            </li>
        @endcan
        @can('rideBookings.index')
            <li class="nav-item">
                <a href="{{ route('admin.rideBookings.index') }}"
                    class="nav-link {{ Request::is('admin/carpool/rideBookings*') ? 'active' : '' }}">
                    <i class="nav-icon fas fa-history"></i>
                    <p>@lang('models/rideBookings.plural')</p>
                </a>
            </li>
        @endcan
    @endcanany
@endif

    <li class="nav-header">OTHERS</li>
    @canany(['driverKycs.index', 'driver.index'])
        <li class="nav-item has-treeview @if (Request::is('admin/drivers') || Request::is('admin/driverKycs*')) menu-open @endif">
            <a href="#" class="nav-link {{ (Request::is('admin/drivers') || Request::is('admin/driverKycs*')) ? 'active' : ''}}">
                <i class="nav-icon fas fas fa-motorcycle"></i>
                <p>
                    Driver
                    <i class="right fas fa-angle-right"></i>
                </p>
            </a>
            <ul class="nav nav-treeview">
                @can('drivers.index')
                    <li class="nav-item">
                        <a href="{{ route('admin.drivers.index') }}"
                            class="nav-link {{ Request::is('admin/drivers') ? 'active' : '' }}">
                            <i class="nav-icon fas fa-motorcycle"></i>
                            <p>@lang('models/drivers.plural')</p>
                        </a>
                    </li>
                @endcan
                @can('driverKycs.index')
                    <li class="nav-item">
                        <a href="{{ route('admin.driverKycs.index') }}"
                            class="nav-link {{ Request::is('admin/driverKycs') ? 'active' : '' }}">
                            <i class="nav-icon fas fa-file-signature"></i>
                            <p>@lang('models/driverKycs.plural')</p>
                        </a>
                    </li>
                @endcan
            </ul>
        </li>
    @endcanany
    @canany(['users.index', 'addresses.index'])
        <li class="nav-item has-treeview @if (Request::is('admin/users*') || Request::is('admin/addresses*')) menu-open @endif ">
            <a href="#" class="nav-link {{ (Request::is('admin/users*') || Request::is('admin/addresses*')) ? 'active' : ''}}">
                <i class="nav-icon fas fa-users"></i>
                <p>
                    User Management
                    <i class="right fas fa-angle-right"></i>
                </p>
            </a>
            <ul class="nav nav-treeview">
                @can('users.index')
                    <li class="nav-item">
                        <a href="{{ route('admin.users.index') }}"
                            class="nav-link {{ Request::is('admin/users*') ? 'active' : '' }}">
                            <i class="nav-icon fas fa-user-astronaut"></i>
                            <p>@lang('models/users.plural')</p>
                        </a>
                    </li>
                @endcan
                @can('addresses.index')
                    <li class="nav-item">
                        <a href="{{ route('admin.addresses.index') }}"
                            class="nav-link {{ Request::is('admin/addresses*') ? 'active' : '' }}">
                            <i class="nav-icon fas fa-map-pin"></i>
                            <p>@lang('models/addresses.plural')</p>
                        </a>
                    </li>
                @endcan
            </ul>
        </li>
    @endcan
    @canany(['payments.index','earningReports.index','reports.index','earnings.index', 'userWalletTransactions.index', 'driverWalletTransactions.index'])
        <li class="nav-item has-treeview @if (Request::is('admin/earnings*') || Request::is('admin/userWalletTransactions*') || Request::is('admin/reports*') || Request::is('admin/earningReport*') || Request::is('admin/payments*') || Request::is('admin/driverWalletTransactions*')) menu-open @endif">
            <a href="#" class="nav-link {{(Request::is('admin/earnings*') || Request::is('admin/reports*') || Request::is('admin/earningReport*') || Request::is('admin/payments*') || Request::is('admin/userWalletTransactions*') || Request::is('admin/driverWalletTransactions*')) ? 'active' :'' }}">
                <i class="nav-icon fas fa-store-alt"></i>
                <p>
                    Earning & Transactions
                    <i class="right fas fa-angle-right"></i>
                </p>
            </a>
            <ul class="nav nav-treeview">
            @can('payments.index')
                <li class="nav-item">
                    <a href="{{ route('admin.payments.index') }}"
                        class="nav-link {{ Request::is('admin/payments*') ? 'active' : '' }}">
                        <i class="nav-icon fas fa-money-check-alt"></i>
                        <p>@lang('models/payments.plural')</p>
                    </a>
                </li>
            @endcan
            @can('earningReports.index')
                <li class="nav-item">
                    <a href="{{ route('admin.earnings.report.index') }}"
                        class="nav-link {{ Request::is('admin/earningReport*') ? 'active' : '' }}">
                        <i class="nav-icon fas fa-money-check-alt"></i>
                        <p>@lang('earningsreport.title')</p>
                    </a>
                </li>
            @endcan
            @can('reports.index')
                <li class="nav-item">
                    <a href="{{ route('admin.reports.index') }}"
                    class="nav-link {{ Request::is('admin/reports*') ? 'active' : '' }}">
                        <i class="nav-icon fas fa-money-check-alt"></i>
                        <p>Report</p>
                    </a>
                </li>
            @endcan
            @can('earnings.index')
                <li class="nav-item">
                    <a href="{{ route('admin.earnings.index') }}"
                        class="nav-link {{ Request::is('admin/earnings*') ? 'active' : '' }}">
                        <i class="nav-icon fas fa-money-bill-wave"></i>
                        <p>@lang('models/earnings.plural')</p>
                    </a>
                </li>
            @endcan
            @can('userWalletTransactions.index')
                <li class="nav-item">
                    <a href="{{ route('admin.userWalletTransactions.index') }}"
                        class="nav-link {{ Request::is('admin/userWalletTransactions*') ? 'active' : '' }}">
                        <i class="nav-icon fas fa-wallet"></i>
                        <p>@lang('models/userWalletTransactions.plural')</p>
                    </a>
                </li>
            @endcan
            @can('driverWalletTransactions.index')
                <li class="nav-item">
                    <a href="{{ route('admin.driverWalletTransactions.index') }}"
                        class="nav-link {{ Request::is('admin/driverWalletTransactions') ? 'active' : '' }}">
                        <i class="nav-icon fas fa-wallet"></i>
                        <p>@lang('models/driverWalletTransactions.plural')</p>
                    </a>
                </li>
            @endcan
            </ul>
        </li>
    @endcan
    @can('settings.update')
        <li class="nav-item">
            <a href="{{ route('admin.settings.index') }}"
                class="nav-link {{ Request::is('admin/settings*') ? 'active' : '' }}">
                <i class="nav-icon fas fa-cog"></i>
                <p>Settings</p>
            </a>
        </li>
    @endcan
    @can('paymentMethods.index')
        <li class="nav-item">
            <a href="{{ route('admin.paymentTypes.index') }}"
                class="nav-link {{ Request::is('admin/paymentTypes*') ? 'active' : '' }}">
                <i class="nav-icon fas fa-money-bill-wave"></i>
                <p>@lang('models/paymentTypes.plural')</p>
            </a>
        </li>
    @endcan
    @can('deliveryFees.index')
        <li class="nav-item">
            <a href="{{ route('admin.deliveryFees.index') }}"
                class="nav-link {{ Request::is('admin/deliveryFees*') ? 'active' : '' }}">
                <i class="nav-icon fas fa-rupee-sign"></i>
                <p>@lang('models/deliveryFees.plural')</p>
            </a>
        </li>
    @endcan
    @can('vehicleTypes.index')
        <li class="nav-item">
            <a href="{{ route('admin.vehicleTypes.index') }}"
                class="nav-link {{ Request::is('admin/vehicleTypes*') ? 'active' : '' }}">
                <i class="nav-icon fas fa-truck-pickup"></i>
                <p>@lang('models/vehicleTypes.plural')</p>
            </a>
        </li>
    @endcan
    @can('zones.index')
        <li class="nav-item">
            <a href="{{ route('admin.zones.index') }}"
                class="nav-link {{ Request::is('admin/zones*') ? 'active' : '' }}">
                <i class="nav-icon fas fa-draw-polygon"></i>
                <p>@lang('models/zones.plural')</p>
            </a>
        </li>
    @endcan
    
    

    @canany(['trackingmaps.index', 'trackingmaps.merchants'])
    <li class="nav-item has-treeview @if (Request::is('admin/trackingmaps*') || Request::is('admin/trackingmaps*'))  menu-open @endif">
        <a href="#" class="nav-link @if (Request::is('admin/trackingmaps*') || Request::is('admin/trackingmaps*')) active @endif">
            <i class="nav-icon fas fa-user-shield"></i>
            <p>
                Tracking Maps
                <i class="right fas fa-angle-right"></i>
            </p>
        </a>
        <ul class="nav nav-treeview">
            @can('trackingmaps.index')
                <li class="nav-item">
                    <a href="{{ route('admin.trackingmaps.index') }}"
                        class="nav-link {{ Request::is('admin/trackingmaps*') ? 'active' : '' }}">
                        <i class="nav-icon fas fa-user-secret"></i>
                        <p>Driver Maps</p>
                    </a>
                </li>
            @endcan
      
            @can('trackingmaps.merchants')
                <li class="nav-item">
                    <a href="{{ route('admin.trackingmaps.merchants') }}"
                        class="nav-link {{ Request::is('admin/trackingmaps*') ? 'active' : '' }}">
                        <i class="nav-icon fas fa-file-signature"></i>
                        <p>Merchants maps</p>
                    </a>
                </li>
            @endcan
        </ul>
    </li>
@endcan
    @can('roles.index')
        <li class="nav-item">
            <a href="{{ route('admin.roles.index') }}"
                class="nav-link {{ Request::is('admin/roles*') ? 'active' : '' }}">
                <i class="nav-icon fas fa-user-tag"></i>
                <p>@lang('models/roles.plural')</p>
            </a>
        </li>
    @endcan
    @can('banners.index')
        <li class="nav-item">
            <a href="{{ route('admin.banners.index') }}"
                class="nav-link {{ Request::is('admin/banners*') ? 'active' : '' }}">
                <i class="nav-icon fas fa-flag-checkered"></i>
                <p>@lang('models/banners.plural')</p>
            </a>
        </li>
    @endcan
    @can('notification.index')
        <li class="nav-item">
            {{-- <a href="{{ route('admin.notification.index') }}" --}}
            <a href="{{ route('admin.notification.index') }}"
                class="nav-link {{ Request::is('admin/notification*') ? 'active' : '' }}">
                <i class="nav-icon fas fa-bell"></i>
                <p>@lang('models/notification.plural')</p>
            </a>
        </li>
    @endcan
    @if (config('constants.features.purchasing_service', false))
        @can('reviews.index')
            <li class="nav-item">
                <a href="{{ route('admin.reviews.index') }}"
                    class="nav-link {{ Request::is('admin/reviews*') ? 'active' : '' }}">
                    <i class="nav-icon fas fa-star"></i>
                    <p>@lang('models/reviews.plural')</p>
                </a>
            </li>
        @endcan
    @endif
    @can('sendMails.index')
        <li class="nav-item">
            <a href="{{ route('admin.sendmails.index') }}"
                class="nav-link {{ Request::is('admin/sendmails*') ? 'active' : '' }}">
                <i class="nav-icon fas fa-envelope"></i>
                <p>Send E-mail</p>
            </a>
        </li>
    @endcan
    @can('importExport.index')
        <li class="nav-item">
            <a href="{{ route('admin.importExport.index') }}"
                class="nav-link {{ Request::is('admin/import-export*') ? 'active' : '' }}">
                <i class="nav-icon fas fa-file-export"></i>
                <p>@lang('models/importExport.plural')</p>
            </a>
        </li>
    @endcan
    @can('pages.index')
        <li class="nav-item">
            <a href="{{ route('admin.pages.index') }}"
                class="nav-link {{ Request::is('admin/pages*') ? 'active' : '' }}">
                <i class="nav-icon fas fa-scroll"></i>
                <p>@lang('models/pages.plural')</p>
            </a>
        </li>
    @endcan
    @can('faqs.index')
        <li class="nav-item">
            <a href="{{ route('admin.faqs.index') }}"
               class="nav-link {{ Request::is('admin/fAQS*') ? 'active' : '' }}">
                <i class="nav-icon fa-solid fa-question"></i>
                <p>@lang('models/fAQS.plural')</p>
            </a>
        </li>
    @endcan
