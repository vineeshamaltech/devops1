<div class="ml-auto d-inline-flex">
    <li class="nav-item dropdown">
        <a class="nav-link  dropdown-toggle pt-1" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-save"></i> {{trans('lang.export')}}
        </a>
        <div class="dropdown-menu">
            <a class="dropdown-item" id="exportCsvDatatable" href="#"> <i class="fa fa-file-excel-o mr-2"></i>CSV</a>
            <a class="dropdown-item" id="exportExcelDatatable" href="#"> <i class="fa fa-file-excel-o mr-2"></i>Excel</a>
        </div>
    </li>
    <li class="nav-item">
        <a class="nav-link pt-1" id="refreshDatatable" href="#"><i class="fa fa-refresh"></i> {{trans('lang.refresh')}}</a>
    </li>
    <li class="nav-item">
        <a class="nav-link pt-1" id="printDatatable" href="#"><i class="fa fa-print"></i> {{trans('lang.print')}}</a>
    </li>
    <li class="nav-item">
        <a class="nav-link pt-1" id="resetDatatable" href="#"><i class="fa fa-undo"></i> {{trans('lang.reset')}}</a>
    </li>
    <li id="colVisDatatable" class="nav-item dropdown keepopen">
        <a class="nav-link dropdown-toggle pt-1" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-eye"></i> {{trans('lang.columns')}}
        </a>
        <div class="dropdown-menu">
            @foreach($dataTable->collection as $key => $item)
                <a class="dropdown-item text-bold" href="#" data-column="{{$key}}"> <i class="fa fa-check mr-2"></i>{{$item->title}}</a>
            @endforeach
        </div>
    </li>
</div>

@push('third_party_scripts')
@endpush
