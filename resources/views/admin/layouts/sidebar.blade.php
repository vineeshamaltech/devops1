<aside class="main-sidebar @if(setting('dark_mode',0)) sidebar-dark-blue @else sidebar-light-indigo @endif  elevation-1">
        <!-- Brand Logo -->
        <a href="{{env('APP_URL')}}" class="brand-link">
          <img src="{{env('APP_LOGO') ?? "https://ui-avatars.com/api/?name=".env('APP_NAME')}}" alt="{{config('app.name')}} Logo" class="brand-image img-circle" style="opacity: .8">
          <span class="brand-text font-weight-light">{{config('app.name')}}</span>
        </a>

        <!-- Sidebar -->
        <div class="sidebar">
          <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">
              <img onerror="this.onerror=null;this.src='https://ui-avatars.com/api/?name={{ Auth::user()->name }}';" src="{{ Auth::user()->profile_img }}" class="img-circle" alt="User Image">
            </div>
            <div class="info">
              <a href="#" class="d-block">{{ Auth::user()->name }}</a>
            </div>
          </div>

          <!-- SidebarSearch Form -->
          <div class="form-inline">
            <div class="input-group" data-widget="sidebar-search">
              <input class="form-control form-control-sidebar" type="search" placeholder="Search" aria-label="Search">
              <div class="input-group-append">
                <button class="btn btn-sidebar">
                  <i class="fas fa-search fa-fw"></i>
                </button>
              </div>
            </div>
          </div>

          <!-- Sidebar Menu -->
          <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column nav-flat" data-widget="treeview" role="menu" data-accordion="false">
              <!-- Add icons to the links using the .nav-icon class
                   with font-awesome or any other icon font library -->
                @include('admin.layouts.menu')
            </ul>
          </nav>
          <!-- /.sidebar-menu -->
        </div>
        <!-- /.sidebar -->
</aside>
