@extends('admin.layouts.app')
@section('title','Active Time')
@section('content')
    <div class="content">

        @include('flash::message')

        <div class="clearfix"></div>

        <div class="card">
            @include('admin.driver_activetime.logintime')


            <div class="card-body">
                @include('admin.driver_activetime.table')

                <div class="card-footer clearfix float-right">
                    <div class="float-right">
                        
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection


