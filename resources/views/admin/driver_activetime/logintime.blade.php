<div class="row" >
    <div class="col-md-3 col-3 m-3">
      <!-- small box -->
      <div class="small-box bg-info ">
        <div class="inner">
          <h3>{{$today_time ?? "00:00:00"}}</h3>
          <p>Today Active time</p>
        </div>
            <div class="icon">
              <i class="ion ion-bag"></i>
            </div>
      </div>
    </div>

    <!-- ./col -->
    <div class="col-lg-3 col-6 m-3">
      <!-- small box -->
      <div class="small-box bg-success">
        <div class="inner">
          <h3>{{$driverDuty ?? "00:00:00"}}<sup style="font-size: 20px"></sup></h3>

          <p>All Active Time</p>
        </div>
        <div class="icon">
          <i class="ion ion-stats-bars"></i>
        </div>
       </div>
    </div>
</div>
