@extends('admin.layouts.app')

@section('title','Send Mail')

@section('content')
    <div class="content">
        @include('flash::message')
        <div class="row">
            <div class="col-sm-6">
                <div class="card">
                    {!! Form::open(['route' => 'admin.sendmails.send']) !!}
                    <div class="card-header">
                        <h3 class="card-title">
                            <i class="fas fa-envelope"></i>
                            Send Mail
                        </h3>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="form-group col-sm-12">
                                {!! Form::label('email', 'Choose Recepient Type') !!}<span style="color: red;">*</span>
                                <select name="type" id="type" class="form-control select2">
                                    <option value="">Choose an option</option>
                                    <option value="all">All</option>
                                    <option value="admin">Admin</option>
                                    <option value="vendor">Vendor</option>
                                    <option value="customer">Customer</option>
                                </select>
                            </div>
                            <div class="form-group col-sm-12" id="recepientLoadArea">
                                {!! Form::label('email', 'Select Recepient') !!}<span style="color: red;">*</span>
                                <select name="recepient[]" id="recepient" multiple class="form-control select2">
                                </select>
                            </div>
                            <div class="form-group col-sm-12">
                                {!! Form::label('email', 'Subject') !!}<span style="color: red;">*</span>
                                <input type="text" class="form-control" name="subject"/>
                            </div>

                            <div class="form-group col-sm-12">
                                {!! Form::label('email', 'Body') !!}<span style="color: red;">*</span>
                                <textarea name="body" class="form-control"></textarea>
                            </div>

                            <div class="clearfix"></div>

                        </div>
                    </div>

                    <div class="card-footer">
                        {!! Form::submit('Send', ['class' => 'btn btn-primary']) !!}
                        <a href="{{ route('admin.sendmails.index') }}" class="btn btn-default">
                            @lang('lang.cancel')
                        </a>
                    </div>

                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>
@endsection
@push('third_party_scripts')
    <script>
        $(document).on('change', '#type', function() {
        if($(this).val() == 'admin'){
            $('#recepient').val(null);
            getAdmin();
            $('#recepient').append('<option value="0" selected="selected">All</option>');
            $('#recepient').trigger('change');
        }else if($(this).val() == 'vendor'){
            $('#recepient').val(null);
            getVendor();
            $('#recepient').append('<option value="0" selected="selected">All</option>');
            $('#recepient').trigger('change');
        }else if($(this).val() == 'customer'){
            $('#recepient').val(null);
            getCustomer();
            $('#recepient').append('<option value="0" selected="selected">All</option>');
            $('#recepient').trigger('change');
        }
        else{
            $('#recepient').val(null);
            $('#recepient').append('<option value="0" selected="selected">All</option>');
            $('#recepient').trigger('change');
        }
    });

    function getAdmin(){
        $('#recepient').html('').select2({
            theme: 'classic',
            ajax: {
                url: '/api/get_admin?search=type:Admin',
                dataType: 'json',
                processResults: function (data) {
                    console.log(data);
                    if(data.data.length > 0) {
                        return {
                            results: data.data.map(function (item) {
                                console.log(item);
                                return {
                                    id: item.id,
                                    text: item.name
                                }
                            })
                        };
                    }
                }
            }
        });
    }
    function getCustomer(){
        $('#recepient').html('').select2({
            theme: 'classic',
            ajax: {
                url: '/api/get_customer?search=type:Customer',
                dataType: 'json',
                processResults: function (data) {
                    console.log(data);
                    if(data.data.length > 0) {
                        return {
                            results: data.data.map(function (item) {
                                console.log(item);
                                return {
                                    id: item.id,
                                    text: item.name
                                }
                            })
                        };
                    }
                }
            }
        });
    }
    function getVendor(){
        $('#recepient').html('').select2({
            theme: 'classic',
            ajax: {
                url: '/api/get_vendor?search=type:Vendor',
                dataType: 'json',
                processResults: function (data) {
                    if(data.data.length > 0) {
                        return {
                            results: data.data.map(function (item) {
                                return {
                                    id: item.id,
                                    text: item.name
                                }
                            })
                        };
                    }
                }
            }
        });
    }
    </script>
@endpush