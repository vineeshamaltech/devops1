<!-- User Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('user_id', __('models/rides.fields.user_id').':') !!}
    {!! Form::text('user_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Max Copassengers Field -->
<div class="form-group col-sm-6">
    {!! Form::label('max_copassengers', __('models/rides.fields.max_copassengers').':') !!}
    {!! Form::select('max_copassengers', ['1'=>'1','2'=>'2','3'=>'3','4'=>'4','5'=>'5','6'=>'6','7'=>'7','8'=>'8'], null, ['class' => 'form-control select2']) !!}
</div>


<div class="col-sm-6">
    <div class="row">
        <!-- From Address Field -->
        <div class="form-group col-sm-12">
            {!! Form::label('from_address', __('models/rides.fields.from_address').':') !!}
            {!! Form::text('from_address', null, ['class' => 'form-control']) !!}
        </div>
        <!-- From Lat Field -->
        <div class="form-group col-sm-6">
            {!! Form::label('from_lat', __('models/rides.fields.from_lat').':') !!}
            {!! Form::text('from_lat', null, ['class' => 'form-control']) !!}
        </div>
        <!-- From Long Field -->
        <div class="form-group col-sm-6">
            {!! Form::label('from_long', __('models/rides.fields.from_long').':') !!}
            {!! Form::text('from_long', null, ['class' => 'form-control']) !!}
        </div>
        <!-- To Address Field -->
        <div class="form-group col-sm-12">
            {!! Form::label('to_address', __('models/rides.fields.to_address').':') !!}
            {!! Form::text('to_address', null, ['class' => 'form-control']) !!}
        </div>
        <!-- To Lat Field -->
        <div class="form-group col-sm-6">
            {!! Form::label('to_lat', __('models/rides.fields.to_lat').':') !!}
            {!! Form::text('to_lat', null, ['class' => 'form-control']) !!}
        </div>
        <!-- To Long Field -->
        <div class="form-group col-sm-6">
            {!! Form::label('to_long', __('models/rides.fields.to_long').':') !!}
            {!! Form::text('to_long', null, ['class' => 'form-control']) !!}
        </div>

        <!-- Travel Datetime Field -->
        <div class="form-group col-sm-12">
            {!! Form::label('travel_datetime', __('models/rides.fields.travel_datetime').':') !!}
            {!! Form::text('travel_datetime', null, ['class' => 'form-control','id'=>'travel_datetime']) !!}
        </div>

        <!-- Can Book Instantly Field -->
        <div class="form-group col-sm-12">
            <div class="form-check">
                {!! Form::hidden('can_book_instantly', 0, ['class' => 'form-check-input']) !!}
                {!! Form::checkbox('can_book_instantly', '1', null, ['class' => 'form-check-input']) !!}
                {!! Form::label('can_book_instantly', __('models/rides.fields.can_book_instantly'), ['class' => 'form-check-label']) !!}
            </div>
        </div>

    </div>
</div>
<div class="col-sm-6">
    <div style="width: 100%;height: 500px;border-radius: 10px;" id="map"></div>
    {{--    <iframe style="border: 0;" src="https://www.google.com/maps/embed/v1/place?key={{setting('google_map_api_key')}}&q=senior+assisted+care+near+me&zoom=9" width="600" height="450" frameborder="0" allowfullscreen="allowfullscreen"></iframe>--}}
</div>
@push('third_party_stylesheets')
    <link rel="stylesheet" href="{{ asset('assets/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.css') }}"/>
@endpush

@push('third_party_scripts')
    <script src="{{ asset('assets/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.js') }}"></script>

    <script type="text/javascript">
        $(function () {
            $('#travel_datetime').datetimepicker();
        });
    </script>
@endpush

<!-- Route Field -->
<div class="form-group col-sm-6">
    {!! Form::label('route', __('models/rides.fields.route').':') !!}
    {!! Form::text('route', null, ['class' => 'form-control']) !!}
</div>

<!-- Price Field -->
<div class="form-group col-sm-6">
    {!! Form::label('price', __('models/rides.fields.price').':') !!}
    {!! Form::text('price', null, ['class' => 'form-control']) !!}
</div>

<!-- Status Field -->
<div class="form-group col-sm-6">
    {!! Form::label('status', __('models/rides.fields.status').':') !!}
    {!! Form::select('status', ['DRAFT'=>'Draft','PUBLISHED'=>'Published','STARTED'=>'Started','FINISHED'=>'Finished','CANCELLED'=>'Cancelled'], null, ['class' => 'form-control select2']) !!}
</div>


<!-- Active Field -->
<div class="form-group col-sm-6">
    <div class="form-check">
        {!! Form::hidden('active', 0, ['class' => 'form-check-input']) !!}
        {!! Form::checkbox('active', '1', null, ['class' => 'form-check-input']) !!}
        {!! Form::label('active', __('models/rides.fields.active'), ['class' => 'form-check-label']) !!}
    </div>
</div>

@push('third_party_scripts')
    <script type="text/javascript">
    var markers = [];
    function getLocation() {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(initMap);
        } else {
            alert("Geolocation is not supported by this browser.");
        }
    }

    function initMap(position) {
        var myLatlng = { lat: position.coords.latitude, lng: position.coords.longitude };
        if($("[name='from_lat']").val() !== "" && $("[name='from_long']").val() !== ""){
            myLatlng = { lat: parseFloat($("[name='from_lat']").val()), lng: parseFloat($("[name='from_long']").val()) };
        }

        const map = new google.maps.Map(document.getElementById("map"), {
            zoom: 13,
            center: myLatlng,
        });

        // Configure the click listener.
        map.addListener("click", (mapsMouseEvent) => {
            if (markers.length <= 2) {
                addPlaceMarker(mapsMouseEvent.latLng,map);
                var geocoder= new google.maps.Geocoder();
                geocoder.geocode({
                    'latLng': mapsMouseEvent.latLng
                }, function(results, status) {
                    if (status === google.maps.GeocoderStatus.OK) {
                        if (results[0]) {
                            if(markers.length === 1){
                                $("[name='from_address']").val(results[0].formatted_address);
                            }else if(markers.length === 2){
                                $("[name='to_address']").val(results[0].formatted_address);
                            }
                        }
                    }
                });
                if(markers.length === 1){
                    $("[name='from_lat']").val(mapsMouseEvent.latLng.lat());
                    $("[name='from_long']").val(mapsMouseEvent.latLng.lng());
                }else if(markers.length === 2){
                    $("[name='to_lat']").val(mapsMouseEvent.latLng.lat());
                    $("[name='to_long']").val(mapsMouseEvent.latLng.lng());
                }
                if(markers.length === 2){
                    drawPolylines(map);
                }
            }
        });
    }

    function addPlaceMarker(position, map) {
        var marker = new google.maps.Marker({
            position: position,
            title: "Pickup Location",
            map: map
        });
        markers.push(marker);
        map.panTo(position);
        marker.setMap(map);
    }

    function drawPolylines(map){

        var from_lat = parseFloat($('#from_lat').val());
        var from_long = parseFloat($('#from_long').val());
        var to_lat = parseFloat($('#to_lat').val());
        var to_long = parseFloat($('#to_long').val());

        var lineCoordinates = [
            {lat: from_lat, lng: from_long},
            {lat: to_lat, lng: to_long}
        ];
        var url = "https://maps.googleapis.com/maps/api/directions/json?origin="+from_lat+","+from_long+"&destination="+to_lat+","+to_long+"&key={{setting('google_map_api_key')}}";
        var points = "";
        console.log(url);
        $.ajax({
            url: "https://maps.googleapis.com/maps/api/directions/json?origin=28.37209050759509,76.9197530674591&destination=28.37700930957613,76.95696067091369&key=AIzaSyBcqSSXVZqhYc70o6_vQiN-_jHw8HgxPd0",
            dataType: "jsonp",
            success: function( response ) {
                console.log(JSON.parse(response));
            }
        });
        var linePath = new google.maps.Polyline({
            path: google.maps.geometry.encoding.decodePath(points),
            geodesic: true,
            strokeColor: '#2E86C1'
        });
        linePath.setMap(map);
    }
</script>
<script
    src="https://maps.googleapis.com/maps/api/js?key={{setting('google_map_api_key')}}&callback=getLocation&v=weekly&libraries=geometry"
></script>
@endpush
