@extends('admin.layouts.app')

@section('title',trans('lang.edit')." ".trans('models/rides.singular'))

@section('content')
    <div class="content">

        @include('adminlte-templates::common.errors')

        <div class="card">

            {!! Form::model($rides, ['route' => ['admin.rides.update', $rides->id], 'method' => 'patch']) !!}

            <div class="card-body">
                <div class="row">
                    @include('admin.rides.fields')
                </div>
            </div>

            <div class="card-footer">
                {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
                <a href="{{ route('admin.rides.index') }}" class="btn btn-default">
                    @lang('lang.cancel')
                 </a>
            </div>

            {!! Form::close() !!}

        </div>
    </div>
@endsection