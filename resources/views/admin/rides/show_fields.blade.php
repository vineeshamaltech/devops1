<!-- Id Field -->
<div class="col-sm-12">
    {!! Form::label('id', __('models/rides.fields.id').':') !!}
    <p>{{ $rides->id }}</p>
</div>

<!-- User Id Field -->
<div class="col-sm-12">
    {!! Form::label('user_id', __('models/rides.fields.user_id').':') !!}
    <p>{{ $rides->user_id }}</p>
</div>

<!-- From Address Field -->
<div class="col-sm-12">
    {!! Form::label('from_address', __('models/rides.fields.from_address').':') !!}
    <p>{{ $rides->from_address }}</p>
</div>

<!-- To Address Field -->
<div class="col-sm-12">
    {!! Form::label('to_address', __('models/rides.fields.to_address').':') !!}
    <p>{{ $rides->to_address }}</p>
</div>

<!-- From Lat Field -->
<div class="col-sm-12">
    {!! Form::label('from_lat', __('models/rides.fields.from_lat').':') !!}
    <p>{{ $rides->from_lat }}</p>
</div>

<!-- From Long Field -->
<div class="col-sm-12">
    {!! Form::label('from_long', __('models/rides.fields.from_long').':') !!}
    <p>{{ $rides->from_long }}</p>
</div>

<!-- To Lat Field -->
<div class="col-sm-12">
    {!! Form::label('to_lat', __('models/rides.fields.to_lat').':') !!}
    <p>{{ $rides->to_lat }}</p>
</div>

<!-- To Long Field -->
<div class="col-sm-12">
    {!! Form::label('to_long', __('models/rides.fields.to_long').':') !!}
    <p>{{ $rides->to_long }}</p>
</div>

<!-- Travel Datetime Field -->
<div class="col-sm-12">
    {!! Form::label('travel_datetime', __('models/rides.fields.travel_datetime').':') !!}
    <p>{{ $rides->travel_datetime }}</p>
</div>

<!-- Route Field -->
<div class="col-sm-12">
    {!! Form::label('route', __('models/rides.fields.route').':') !!}
    <p>{{ $rides->route }}</p>
</div>

<!-- Max Copassengers Field -->
<div class="col-sm-12">
    {!! Form::label('max_copassengers', __('models/rides.fields.max_copassengers').':') !!}
    <p>{{ $rides->max_copassengers }}</p>
</div>

<!-- Can Book Instantly Field -->
<div class="col-sm-12">
    {!! Form::label('can_book_instantly', __('models/rides.fields.can_book_instantly').':') !!}
    <p>{{ $rides->can_book_instantly }}</p>
</div>

<!-- Price Field -->
<div class="col-sm-12">
    {!! Form::label('price', __('models/rides.fields.price').':') !!}
    <p>{{ $rides->price }}</p>
</div>

<!-- Status Field -->
<div class="col-sm-12">
    {!! Form::label('status', __('models/rides.fields.status').':') !!}
    <p>{{ $rides->status }}</p>
</div>

<!-- Active Field -->
<div class="col-sm-12">
    {!! Form::label('active', __('models/rides.fields.active').':') !!}
    <p>{{ $rides->active }}</p>
</div>

<!-- Created At Field -->
<div class="col-sm-12">
    {!! Form::label('created_at', __('models/rides.fields.created_at').':') !!}
    <p>{{ $rides->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="col-sm-12">
    {!! Form::label('updated_at', __('models/rides.fields.updated_at').':') !!}
    <p>{{ $rides->updated_at }}</p>
</div>

