{!! Form::open(['route' => ['admin.stores.destroy', $id], 'method' => 'delete']) !!}
<div class='btn-group'>
    @can('stores.view')
    <a href="{{ route('admin.stores.show', $id) }}" class='btn btn-default btn-xs'>
        <i class="fa fa-eye"></i>
    </a>
    @endcan
    @can('stores.edit')
    <a href="{{ route('admin.stores.edit', $id) }}" class='btn btn-default btn-xs'>
        <i class="fa fa-edit"></i>
    </a>
    @endcan
    @can('stores.delete')
    {!! Form::button('<i class="fa fa-trash"></i>', [
        'type' => 'submit',
        'class' => 'btn btn-danger btn-xs',
        'onclick' => 'return confirm("'.__('lang.are_you_sure').'")'
    ]) !!}
    @endcan
</div>
{!! Form::close() !!}
