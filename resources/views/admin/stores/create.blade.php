@extends('admin.layouts.app')

@section('title',trans('lang.add_new')." ".trans('models/stores.singular'))

@section('content')
    <div class="content">

        <!--@include('adminlte-templates::common.errors')-->
        @include('flash::message')

        <div class="card">

            {!! Form::open(['route' => 'admin.stores.store', 'files' => true]) !!}

            <div class="card-body">
                <div class="row">
                    @include('admin.stores.fields')
                </div>
            </div>

            <div class="card-footer">
                {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
                <a href="{{ route('admin.stores.index') }}" class="btn btn-default">
                 @lang('lang.cancel')
                </a>
            </div>

            {!! Form::close() !!}

        </div>
    </div>
@endsection
