<!-- Image Field -->
<div class="col-sm-1">
    {!! Form::label('image', __('models/stores.fields.image').':') !!}
</div>
<!-- Image Field -->
<div class="col-sm-8">
    <img alt="Store Image" src="{{ $store->image }}" style="width:100px;height:100px;" >
</div>
<!-- Vendor Id Field -->
<div class="col-sm-12">
    {!! Form::label('vendor_id', __('models/stores.fields.vendor_id').':') !!}
    <p>{{ $store->vendor->name }}</p>
</div>

<!-- Service Cat Id Field -->
<div class="col-sm-12">
    {!! Form::label('service_cat_id', __('models/stores.fields.service_category_id').':') !!}
    <p>{{ $store->serviceCategory->name }}</p>
</div>

<!-- Store Categories Field -->
<div class="col-sm-12">
    {!! Form::label('store_categories', __('models/stores.fields.store_categories').':') !!}
    <p>{{ $store->serviceCategory->name }}</p>
</div>

<!-- Name Field -->
<div class="col-sm-12">
    {!! Form::label('name', __('models/stores.fields.name').':') !!}
    <p>{{ $store->name }}</p>
</div>

<!-- Description Field -->
<div class="col-sm-12">
    {!! Form::label('description', __('models/stores.fields.description').':') !!}
    <p>{{ $store->description }}</p>
</div>



<!-- Lat Field -->
<div class="col-sm-12">
    {!! Form::label('lat', __('models/stores.fields.lat').':') !!}
    <p>{{ $store->latitude }}</p>
</div>

<!-- Long Field -->
<div class="col-sm-12">
    {!! Form::label('long', __('models/stores.fields.long').':') !!}
    <p>{{ $store->longitude }}</p>
</div>

<!-- Address Field -->
<div class="col-sm-12">
    {!! Form::label('address', __('models/stores.fields.address').':') !!}
    <p>{{ $store->address }}</p>
</div>

<!-- Delivery Range Field -->
<div class="col-sm-12">
    {!! Form::label('delivery_range', __('models/stores.fields.delivery_range').':') !!}
    <p>{{ $store->delivery_range }} Km</p>
</div>

<!-- Base Distance Field -->
<div class="col-sm-12">
    {!! Form::label('base_distance', __('models/stores.fields.base_distance').':') !!}
    <p>{{ $store->base_distance }}</p>
</div>

<!-- Base Price Field -->
<div class="col-sm-12">
    {!! Form::label('base_price', __('models/stores.fields.base_price').':') !!}
    <p>{{ $store->base_price }}</p>
</div>

<!-- Price Per Km Field -->
<div class="col-sm-12">
    {!! Form::label('price_per_km', __('models/stores.fields.price_per_km').':') !!}
    <p>{{ $store->price_per_km }}</p>
</div>

<!-- Mobile Field -->
<div class="col-sm-12">
    {!! Form::label('mobile', __('models/stores.fields.mobile').':') !!}
    <p>{{ $store->mobile }}</p>
</div>

<!-- Email Field -->
<div class="col-sm-12">
    {!! Form::label('email', __('models/stores.fields.email').':') !!}
    <p>{{ $store->email }}</p>
</div>

<!-- Order Count Field -->
<div class="col-sm-12">
    {!! Form::label('order_count', __('models/stores.fields.order_count').':') !!}
    <p>{{ $store->order_count }}</p>
</div>

<!-- Is Open Field -->
<div class="col-sm-12">
    {!! Form::label('is_open', __('models/stores.fields.is_open').':') !!}
    @if($store->is_open == 1)
    <p>Yes</p>
    @else
    <p>No</p>
    @endif
</div>

<!-- Active Field -->
<div class="col-sm-12">
    {!! Form::label('active', __('models/stores.fields.active').':') !!}
    @if($store->active == 1)
    <p>Yes</p>
    @else
    <p>No</p>
    @endif
</div>

<!-- Created At Field -->
<div class="col-sm-12">
    {!! Form::label('created_at', __('models/stores.fields.created_at').':') !!}
    <p>{{ $store->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="col-sm-12">
    {!! Form::label('updated_at', __('models/stores.fields.updated_at').':') !!}
    <p>{{ $store->updated_at }}</p>
</div>

