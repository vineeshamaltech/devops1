<!-- Title Field -->
<div class="form-group col-sm-6">
    {!! Form::label('title', __('models/zones.fields.title').':') !!}
    {!! Form::text('title', null, ['class' => 'form-control']) !!}
</div>

<!-- Polygon Field -->
<div class="form-group col-sm-12">
    <div class="form-group-lg">
        <div id="map"></div>
        <div id="bar">
            <p><a id="clear" href="#">Click here</a> to clear map.</p>
        </div>
        <input type="hidden" name="polygon" class="ranges" @if(!empty($zone)) value="{{json_encode($zone->polygon)}}" @endif>
    </div>
</div>
<div class="clearfix"></div>

<!-- Active Field -->
<div class="form-group col-sm-6">
    <div class="form-check">
        {!! Form::hidden('active', 0, ['class' => 'form-check-input']) !!}
        {!! Form::checkbox('active', '1', null, ['class' => 'form-check-input']) !!}
        {!! Form::label('active', __('models/zones.fields.active'), ['class' => 'form-check-label']) !!}
    </div>
</div>

@push('third_party_scripts')
    <script>
        var map;
        var polygon;
        var input = document.getElementById('pac-input');
        var s_latitude = document.getElementById('latitude');
        var s_longitude = document.getElementById('longitude');
        var s_address = document.getElementById('address');
        var arr = [];
        var selectedShape;

        var old_latlng = new Array();
        var markers = new Array();

        var OldPath = JSON.parse($('input.ranges').val());

        function getLocation() {
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(showPosition);
            } else {
                alert("Geolocation is not supported by this browser.");
            }
        }

        function showPosition(position) {
            var lat = position.coords.latitude;
            var lng = position.coords.longitude;
            map.setCenter(new google.maps.LatLng(lat, lng));
        }

        function initMap() {
            getLocation();
            var userLocation = new google.maps.LatLng(28.3569757,76.952783);

            map = new google.maps.Map(document.getElementById('map'), {
                center: userLocation,
                zoom: 15,
            });

            var drawingManager = new google.maps.drawing.DrawingManager({
                drawingMode: google.maps.drawing.OverlayType.POLYGON,
                drawingControl: true,
                drawingControlOptions: {
                    position: google.maps.ControlPosition.TOP_CENTER,
                    drawingModes: [google.maps.drawing.OverlayType.POLYGON]
                },
                polygonOptions: {
                    editable: true,
                    draggable: false,
                    fillColor: '#ff0000',
                    strokeColor: '#ff0000',
                    strokeWeight: 2
                }
            });
            drawingManager.setMap(map);

            google.maps.event.addListener(drawingManager, 'overlaycomplete', function(e){
                var bounds = [];
                var layer_bounds = [];
                var newShape = e.overlay;
                if (e.type == google.maps.drawing.OverlayType.POLYGON) {
                    var locations = e.overlay.getPath().getArray();
                    arr.push(e);
                    $.each(locations, function(index, value){
                        var markerLat = (value.lat()).toFixed(6);
                        var markerLng = (value.lng()).toFixed(6);
                        layer_bounds.push(new google.maps.LatLng((value.lat()).toFixed(6), (value.lng()).toFixed(6)));
                        json = {
                            'lat': parseFloat(markerLat),
                            'lng': parseFloat(markerLng)
                        };
                        bounds.push(json);
                    });
                    $('input.ranges').val(JSON.stringify(bounds));

                    overlayClickListener(e.overlay);
                    drawingManager.setOptions({drawingMode:null,drawingControl:false});
                    setSelection(newShape);
                }
            });

            $(document).on('click', '#clear', function(ev) {
                drawingManager.setMap(map);
                polygon.setMap(null);
                deleteSelectedShape();
                $('input.ranges').val('');
                ev.preventDefault();
                return false;
            });

            var old_polygon = [];

            $(OldPath).each(function(index, value){
                old_polygon.push(new google.maps.LatLng(value.lat, value.lng));
                old_latlng.push(new google.maps.LatLng(value.lat, value.lng));
            });

            polygon = new google.maps.Polygon({
                path: old_polygon,
                strokeColor: "#ff0000",
                strokeOpacity: 0.8,
                // strokeWeight: 1,
                fillColor: "#ff0000",
                fillOpacity: 0.3,
                editable: true,
                draggable: false,
            });

            polygon.setMap(map);
        }

        function createMarker(markerOptions) {
            var marker = new google.maps.Marker(markerOptions);
            markers.push(marker);
            old_latlng.push(marker.getPosition());
            return marker;
        }

        function overlayClickListener(overlay) {
            google.maps.event.addListener(overlay, "mouseup", function(event){
                var arr_loc = [];
                var locations = overlay.getPath().getArray();
                $.each(locations, function(index, value){
                    var locLat = (value.lat()).toFixed(6);
                    var locLng = (value.lng()).toFixed(6);
                    ltlg['lat'] = parseFloat(locLat);
                    ltlg['lng'] = parseFloat(locLng);
                    arr_loc.push(ltlg);
                });
                $('input.ranges').val(JSON.stringify(arr_loc));
            });
        }

        function setSelection (shape) {
            if (shape.type === 'polygon') {
                clearSelection();
                shape.setEditable(true);
            }
            selectedShape = shape;
        }

        function clearSelection () {
            if (selectedShape) {
                console.log(selectedShape.type);
                if (selectedShape.type === 'polygon') {
                    selectedShape.setEditable(false);
                }
                selectedShape = null;
            }
        }

        function deleteSelectedShape () {
            if (selectedShape) {
                $('input.ranges').val('');
                selectedShape.setMap(null);
            }
        }
    </script>
    <script
        src="https://maps.googleapis.com/maps/api/js?key={{setting('google_map_api_key')}}&libraries=places,drawing&callback=initMap" async defer></script>

@endpush

@push('third_party_stylesheets')
    <style>
        #map {
            height: 100%;
            min-height: 400px;
            border-radius: 10px;
        }
        .controls {
            border: 1px solid transparent;
            border-radius: 2px 0 0 2px;
            box-sizing: border-box;
            -moz-box-sizing: border-box;
            height: 32px;
            outline: none;
            box-shadow: 0 1px 3px rgba(0, 0, 0, 0.3);
            margin-bottom: 10px;
        }
        #pac-input {
            background-color: #fff;
            font-family: Roboto;
            font-size: 15px;
            font-weight: 300;
            padding: 0 11px 0 13px;
            text-overflow: ellipsis;
            width: 100%;
        }
        #pac-input:focus {
            border-color: #4d90fe;
        }
        #bar {
            width: 240px;
            background-color: rgba(255, 255, 255, 0.75);
            margin: 8px;
            padding: 4px;
            border-radius: 4px;
        }
    </style>
@endpush
