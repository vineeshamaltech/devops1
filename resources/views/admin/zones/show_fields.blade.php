



<div class="col-sm-12">
    {!! Form::label('id', __('models/zones.fields.id').':') !!}
    <p>{{ $zone->id }}</p>
</div>


<div class="col-sm-12">
    {!! Form::label('title', __('models/zones.fields.title').':') !!}
    <p>{{ $zone->title }}</p>
</div>


<div class="col-sm-12">
    {!! Form::label('polygon', __('models/zones.fields.polygon').':') !!}
    <p>{{ json_encode($zone->polygon) }}</p>
</div>


<div class="col-sm-12">
    {!! Form::label('created_at', __('models/zones.fields.created_at').':') !!}
    <p>{{ $zone->created_at }}</p>
</div>


<div class="col-sm-12">
    {!! Form::label('updated_at', __('models/zones.fields.updated_at').':') !!}
    <p>{{ $zone->updated_at }}</p>
</div>

