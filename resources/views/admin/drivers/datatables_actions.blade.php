{!! Form::open(['route' => ['admin.drivers.destroy', $id], 'method' => 'delete']) !!}
<div class='btn-group'>
    @can('drivers.view')
    <a href="{{ route('admin.drivers.show', $id) }}" class='btn btn-default btn-xs'>
        <i class="fa fa-eye"></i>
    </a>
    @endcan

    @can('drivers.edit')
    <a href="{{ route('admin.drivers.edit', $id) }}" class='btn btn-default btn-xs'>
        <i class="fa fa-edit"></i>
    </a>

    @endcan
    @can('drivers.edit')
    <a href="{{ route('admin.logintime',$id) }}" class='btn btn-default btn-xs'>
        <i class="fa fa-key"></i>
    </a>

    @endcan
    @can('drivers.delete')
    {!! Form::button('<i class="fa fa-trash"></i>', [
        'type' => 'submit',
        'class' => 'btn btn-danger btn-xs',
        'onclick' => 'return confirm("'.__('crud.are_you_sure').'")'
    ]) !!}
    @endcan



</div>
{!! Form::close() !!}
