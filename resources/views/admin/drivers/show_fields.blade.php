
<div class="container col-md-12">
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Driver Details</h3>
        </div>
        <div class="card-body">



<!-- Name Field -->


<div class="col-sm-12">
    {!! Form::label('name', __('models/drivers.fields.name').':') !!}
    <p>{{ $driver->name }}</p>
</div>

<!-- Mobile Field -->
<div class="col-sm-12">
    {!! Form::label('mobile', __('models/drivers.fields.mobile').':') !!}
    <p>{{ $driver->mobile }}</p>
</div>

<!-- Email Field -->
<div class="col-sm-12">
    {!! Form::label('email', __('models/drivers.fields.email').':') !!}
    <p>{{ $driver->email }}</p>
</div>

<!-- Cash In Hand Field -->
<div class="col-sm-12">
    {!! Form::label('cash_in_hand', __('models/drivers.fields.cash_in_hand').':') !!}
    <p>{{ $driver->cash_in_hand }}</p>
</div>

<!-- Vehicle Type Field -->
<div class="col-sm-12">
    {!! Form::label('vehicle_type', __('models/drivers.fields.vehicle_type').':') !!}
    <p>{{ $driver->vehicle_type }}</p>
</div>

<!-- Vehicle Reg No Field -->
<div class="col-sm-12">
    {!! Form::label('vehicle_reg_no', __('models/drivers.fields.vehicle_reg_no').':') !!}
    <p>{{ $driver->vehicle_reg_no }}</p>
</div>

<!-- Vehicle Color Field -->
<div class="col-sm-12">
    {!! Form::label('vehicle_color', __('models/drivers.fields.vehicle_color').':') !!}
    <p>{{ $driver->vehicle_color }}</p>
</div>


<!-- Service Type Id Field -->
<div class="col-sm-12">
    {!! Form::label('service_type_id', __('models/drivers.fields.service_type_id').':') !!}
    <p>{{ $driver->service_type_id }}</p>
</div>

<!-- Bank Name Field -->
<div class="col-sm-12">
    {!! Form::label('bank_name', __('models/drivers.fields.bank_name').':') !!}
    <p>{{ $driver->bank_name }}</p>
</div>

<!-- Account No Field -->
<div class="col-sm-12">
    {!! Form::label('account_no', __('models/drivers.fields.account_no').':') !!}
    <p>{{ $driver->account_no }}</p>
</div>

<!-- Ifsc Code Field -->
<div class="col-sm-12">
    {!! Form::label('ifsc_code', __('models/drivers.fields.ifsc_code').':') !!}
    <p>{{ $driver->ifsc_code }}</p>
</div>

<!-- Branch Name Field -->
<div class="col-sm-12">
    {!! Form::label('branch_name', __('models/drivers.fields.branch_name').':') !!}
    <p>{{ $driver->branch_name }}</p>
</div>

<!-- Active Field -->
<div class="col-sm-12">
    {!! Form::label('active', __('models/drivers.fields.active').':') !!}
    <p>{{ $driver->active ? "Verified" : "Not Verified" }}</p>
</div>

<!-- Is Verified Field -->
<div class="col-sm-12">
    {!! Form::label('is_verified', __('models/drivers.fields.is_verified').':') !!}
    <p>{{ $driver->is_verified ? "Verified" : "Not Verified" }}</p>
</div>

<!-- Created At Field -->
<div class="col-sm-12">
    {!! Form::label('created_at', __('models/drivers.fields.created_at').':') !!}
    <p>{{ $driver->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="col-sm-12">
    {!! Form::label('updated_at', __('models/drivers.fields.updated_at').':') !!}
    <p>{{ $driver->updated_at }}</p>
</div>
</div>
</div>
</div>




<div class="container col-md-12">
<div class="card">
    <div class="card-header">
        <h3 class="card-title">KYC Documents</h3>
        <div class="float-right">
            <a href="{{route('admin.driverKycs.approveall',['id'=>$driver->id])}}" type="button" class="btn btn-success btn-sm">Accept All</a>
            <a href="{{route('admin.driverKycs.rejectall',['id'=>$driver->id])}}" type="button" class="btn btn-danger btn-sm">Reject All</a>
        </div>
    </div>
    <div class="card-body">
        <div class="row">

            @foreach($driver->kycdocs as $kycDocument)
                <div class="col-md-6">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <div class="row justify-content-end">
                                    <div class="col-md-6">
                                        <h3 class="card-title">{{ $kycDocument->doc_type }}</h3>
                                        <small class="text-muted m-1"> (updated {{$kycDocument->updated_at->diffForHumans()}})</small>
                                    </div>
                                    <div class="col-md-6 justify-content-end">
                                        @if($kycDocument->status == 'UPLOADED')
                                            <div class="float-right"><span class="badge badge-warning">{{ $kycDocument->status }}</span></div>
                                        @elseif($kycDocument->status == "APPROVED")
                                            <div class="float-right"><span class="badge badge-success">{{ $kycDocument->status }}</span></div>
                                        @elseif($kycDocument->status == "REJECTED")
                                            <div class="float-right"><span class="badge badge-danger">{{ $kycDocument->status }}</span></div>
                                        @endif
                                    </div>
                                </div>

                            </div>
                            <div class="card-body">
                                <a target="_blank" href="{{$kycDocument->photo}}"><img src="{{$kycDocument->photo}}" class="img-fluid" alt="{{$kycDocument->doc_type}}"></a>
                            </div>
                            <div class="card-footer">
                                <a href="{{route('admin.driverKycs.approve',['id'=>$kycDocument->id])}}" type="button" class="btn btn-success btn-sm">Accept</a>
                                <a href="{{route('admin.driverKycs.reject',['id'=>$kycDocument->id])}}" type="button" class="btn btn-danger btn-sm">Reject</a>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
        </div>
    </div>
</div>
</div>











{{--

<!-- Vehicle Img Field -->
<div class="col-sm-12">
    {!! Form::label('vehicle_img', __('models/drivers.fields.vehicle_img').':') !!}
    <br>
    <a target="_blank" href="{{$driver->vehicle_img}}"><img src="{{$driver->vehicle_img}}" class="img-fluid" width="300"></a>
</div>

<!-- Dl Photo Field -->
<div class="col-sm-12">
    {!! Form::label('dl_photo', __('models/drivers.fields.dl_photo').':') !!}
    <br>
    <a target="_blank" href="{{$driver->dl_photo}}"><img src="{{$driver->dl_photo}}" class="img-fluid" width="300"></a>
</div>

<!-- Aadhar Photo Field -->
<div class="col-sm-12">
    {!! Form::label('aadhar_photo', __('models/drivers.fields.aadhar_photo').':') !!}
    <br>
    <a target="_blank" href="{{$driver->aadhar_photo}}"><img src="{{$driver->aadhar_photo}}" class="img-fluid" width="300"></a>
</div>

<div class="row">
    <div class="col-md-6">
        {!! Form::model($driver, ['route' => ['admin.drivers.update', $driver->id], 'method' => 'patch']) !!}
            @csrf
            <input type="hidden" value="0" name="is_verified">
            <input class="btn btn-danger" type="submit" value="Reject KYC">
        {!! Form::close() !!}
    </div>

    <div class="col-md-6">
        {!! Form::model($driver, ['route' => ['admin.drivers.update', $driver->id], 'method' => 'patch']) !!}
            <input type="hidden" value="1" name="is_verified">
            <input class="btn btn-success" type="submit" value="Approve KYC">
        {!! Form::close() !!}
    </div>

</div> --}}
