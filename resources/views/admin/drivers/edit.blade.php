@extends('admin.layouts.app')

@section('title',trans('lang.edit')." ".trans('models/drivers.singular'))

@section('content')
    <div class="content">

        <!--@include('adminlte-templates::common.errors')-->

        <div class="card">

            {!! Form::model($driver, ['route' => ['admin.drivers.update', $driver->id], 'method' => 'patch']) !!}

            <div class="card-body">
                <div class="row">
                    @include('admin.drivers.fields')
                </div>
            </div>

            <div class="card-footer">
                {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
                <a href="{{ route('admin.drivers.index') }}" class="btn btn-default">
                    @lang('lang.cancel')
                 </a>
            </div>

            {!! Form::close() !!}

        </div>
    </div>
@endsection