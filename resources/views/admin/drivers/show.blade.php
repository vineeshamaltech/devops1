@extends('admin.layouts.app')

@section('title',trans('lang.view')." ".trans('models/drivers.singular'))

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>@lang('models/drivers.singular')</h1>
                </div>
                <div class="col-sm-6">
                    <a class="btn btn-default float-right"
                       href="{{ route('admin.drivers.index') }}">
                         @lang('crud.back')
                    </a>
                </div>
            </div>
        </div>
    </section>
    @include('flash::message')
    <div class="content px-3">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    @include('admin.drivers.show_fields')
                </div>
            </div>
        </div>
    </div>

@endsection


