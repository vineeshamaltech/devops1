<div class="invoice p-3 mb-3 printarea" id="printarea">
    <!-- title row -->
    <div class="row">
        <div class="col-12">
            <h4>
                <img src="{{env('APP_LOGO')}}" width="50" height="50"> {{env('APP_NAME')}}
                <small class="float-right">Date: {{\Carbon\Carbon::parse($order->created_at)->format('d/m/Y H:i:s')}}</small>
            </h4>
        </div>
        <!-- /.col -->
    </div>
    <!-- info row -->
    <div class="row invoice-info">
        <div class="col-sm-4 invoice-col">
            Selected Address:
            <address>
                <strong>{{$order->service_address['name'] ?? ""}}</strong><br>
                {{$order->service_address['address'] ?? ""}}<br>
                Mobile: {{$order->service_address['mobile'] ?? ""}}<br>
            </address>
        </div>
        <!-- /.col -->
        <div class="col-sm-4 invoice-col">
            Service Time:
            <address>
                <strong>{{$order->service_date->toDateString()}} {{$order->service_time}}</strong><br>
            </address>
        </div>
        <!-- /.col -->
        <div class="col-sm-4 invoice-col">
            <b>Order ID:</b> # {{$order->id}}<br>
            <b>Payment Status:</b> {{$order->payment_status}}<br>
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->

    <!-- Table row -->
    <div class="row">
        <div class="col-12 table-responsive">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th>S. No</th>
                    <th>Product</th>
                    <th>Quantity</th>
                    <th>Price</th>
                    <th>Subtotal</th>
                </tr>
                </thead>
                <tbody>
                @foreach($order->items as $key => $orderedProduct)
                    <tr>
                        <td>{{$key + 1}}</td>
                        <td>{{$orderedProduct->service_name}}</td>
                        <td>{{$orderedProduct->quantity}}</td></td>
                        <td>{{setting('currency_symbol','RS')}} {{$orderedProduct->service_price}}</td>
                        <td>{{setting('currency_symbol','RS')}} {{$orderedProduct->service_price * $orderedProduct->quantity}}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->

    <div class="row">
        <!-- accepted payments column -->
        <div class="col-6">
            <p class="lead">Payment Method: <strong>{{strtoupper($order->payment_method)}}</strong></p>
            <p class="text-muted well well-sm shadow-none" style="margin-top: 10px;">
            </p>
        </div>
        <!-- /.col -->
        <div class="col-6">
            <div class="table-responsive">
                <table class="table">
                    <tbody><tr>
                        <th style="width:50%">Subtotal:</th>
                        <td>{{setting('currency_symbol','RS')}} {{$order->total_price}}</td>
                    </tr>
                    <tr>
                        <th>Tax</th>
                        <td>{{setting('currency_symbol','RS')}} {{$order->tax}}</td>
                    </tr>
                    @if(!empty($order->delivery_fee))
                        <tr>
                            <th>Delivedddry Fee:</th>
                            <td>{{setting('currency_symbol','RS')}} {{$order->delivery_fee}}</td>
                        </tr>
                    @endif
                    <tr>
                        <th>Total:</th>
                        <td>{{setting('currency_symbol','RS')}} {{$order->grand_total}}</td>
                    </tr>
                    </tbody></table>
            </div>
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->

    <!-- this row will not appear when printing -->
    <div class="row no-print">
        <div class="col-12">
            <button onclick="printDiv('printarea')" rel="noopener" type="button" class="btn btn-primary float-right" style="margin-right: 5px;">
                <i class="fas fa-print"></i> Print
            </button>
        </div>
    </div>
</div>

@push('third_party_scripts')
    <script type="text/javascript">
        function printDiv(divName) {
            var printContents = document.getElementById('printarea').innerHTML;
            var originalContents = document.body.innerHTML;
            document.body.innerHTML = printContents;
            window.print();
            document.body.innerHTML = originalContents;
        }
    </script>
@endpush
