
{{--<!-- Store Id Field -->--}}
{{--<div class="form-group col-sm-6">--}}
{{--    {!! Form::label('store_id', __('models/orders.fields.store_id').':') !!}--}}
{{--    <select name="store_id" class="form-control select2" disabled>--}}
{{--        @foreach($stores as $store)--}}
{{--            <option value="{{ $store->id }}" @if(isset($order) && $order->store_id == $store->id) selected @endif>{{ $store->name }}</option>--}}
{{--        @endforeach--}}
{{--    </select>--}}
{{--</div>--}}



<!-- Total Price Field -->
<div class="form-group col-sm-6">
    {!! Form::label('total_price', __('models/orders.fields.total_price').':') !!}
    {!! Form::number('total_price', null, ['class' => 'form-control', 'disabled'=>true]) !!}
</div>

{{--<!-- Discount Type Field -->--}}
{{--<div class="form-group col-sm-6">--}}
{{--    {!! Form::label('discount_type', __('models/orders.fields.discount_type').':') !!}--}}
{{--    {!! Form::number('discount_type', null, ['class' => 'form-control']) !!}--}}
{{--</div>--}}


<!-- Tax Field -->
<div class="form-group col-sm-6">
    {!! Form::label('tax', __('models/orders.fields.tax').':') !!}
    {!! Form::number('tax', null, ['class' => 'form-control','disabled'=>true]) !!}
</div>

<!-- Tax Field -->
{{--<div class="form-group col-sm-6">--}}
{{--    {!! Form::label('tip', __('models/orders.fields.tip').':') !!}--}}
{{--    {!! Form::number('tip', null, ['class' => 'form-control','disabled'=>true]) !!}--}}
{{--</div>--}}

<!-- Grand Total Field -->
<div class="form-group col-sm-6">
    {!! Form::label('grand_total', __('models/orders.fields.grand_total').':') !!}
    {!! Form::number('grand_total', null, ['class' => 'form-control','disabled'=>true]) !!}
</div>
@can('vendorKycs.index')
    @if($order->pickup_from_store == 0)
     <div class="form-group col-sm-6">
        {!! Form::label('driver_id', __('models/orders.fields.driver_id').':') !!}
        <select name="driver_id" class="form-control select2">
            <option value="">Select a driver</option>
            @foreach($drivers as $driver)
                <option value="{{ $driver->id }}" @if($driver->disabled) disabled @endif @if(isset($order) && $order->driver_id == $driver->id) selected @endif>{{ $driver->name }}</option>
            @endforeach
        </select>
    </div>
    @endif
@endcan
<!-- Order Status Field -->
<div class="form-group col-sm-6">
    {!! Form::label('order_status', __('models/orders.fields.order_status').':') !!}
    {!! Form::select('order_status', ['RECEIVED'=>'RECEIVED','ACCEPTED'=>'ACCEPTED','REACHED'=>'REACHED','ASSIGNED'=>'ASSIGNED','STARTED'=>'STARTED','FINISHED'=>'FINISHED','COMPLETED'=>'COMPLETED','CANCELLED'=>'CANCELLED'] , null, ['class' => 'form-control select2']) !!}
</div>
@can('vendorKycs.index')
<!-- Order Status Field -->
<div class="form-group col-sm-6">
    {!! Form::label('payment_status', __('models/orders.fields.payment_status').':') !!}
    {!! Form::select('payment_status', ['PENDING'=>'PENDING','FAILED'=>'FAILED','SUCCESS'=>'SUCCESS','REFUNDED'=>'REFUNDED'], null, ['class' => 'form-control select2']) !!}
</div>

<!-- Order Status Field -->
<div class="form-group col-sm-6">
    {!! Form::label('payment_method', __('models/orders.fields.payment_method').':') !!}
    {!! Form::select('payment_method', ['ONLINE'=>'ONLINE','COD'=>'COD','WALLET'=>'WALLET'], null, ['class' => 'form-control select2']) !!}
</div>

@endcan
{{--<!-- Json Data Field -->--}}
{{--<div class="form-group col-sm-6">--}}
{{--    {!! Form::label('json_data', __('models/orders.fields.json_data').':') !!}--}}
{{--    {!! Form::textarea('json_data', null, ['class' => 'form-control']) !!}--}}
{{--</div>--}}
