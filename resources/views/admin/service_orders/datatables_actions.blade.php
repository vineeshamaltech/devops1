{!! Form::open(['route' => ['admin.homeOrders.destroy', $id], 'method' => 'delete']) !!}
<div class='btn-group'>
    @can('orders.view')
    <a href="{{ route('admin.homeOrders.show', $id) }}" class='btn btn-default btn-xs'>
        <i class="fa fa-eye"></i>
    </a>
    @endcan
    @can('orders.edit')
    <a href="{{ route('admin.homeOrders.edit', $id) }}" class='btn btn-default btn-xs'>
        <i class="fa fa-edit"></i>
    </a>
    @endcan
  @can('orders.delete')
 {!! Form::button('<i class="fa fa-trash"></i>', [
    'type' => 'submit',
     'class' => 'btn btn-danger btn-xs',
      'onclick' => 'return confirm("'.__('crud.are_you_sure').'")'
   ]) !!}
   @endcan
</div>
{!! Form::close() !!}
