@extends('admin.layouts.app')
@section('title','Home Service Orders')
@section('content')
    <div class="content">

        @include('flash::message')

        <div class="clearfix"></div>

        <div class="card">
            <div class="card-header">
                <ul class="nav nav-tabs align-items-end card-header-tabs w-100">
                    <li class="nav-item">
                        <a class="nav-link active" href="{!! url()->current() !!}"><i class="fa fa-list mr-2"></i>{{trans('models/homeServiceOrders.plural')}}</a>
                    </li>
{{--                    @can('orders.create')--}}
{{--                    <li class="nav-item">--}}
{{--                        <a class="nav-link" href="{!! route('admin.orders.create') !!}"><i class="fa fa-plus mr-2"></i>{{trans('lang.add')." ".trans('models/orders.singular')}}</a>--}}
{{--                    </li>--}}
{{--                    @endcan--}}
                    @include('admin.layouts.datatables_toolbar', compact('dataTable'))
                </ul>
            </div>

            <div class="card-body">
                @include('admin.service_orders.table')

                <div class="card-footer clearfix float-right">
                    <div class="float-right">

                    </div>
                </div>
            </div>
        </div>
    </div>

{{--    <div class="modal fade" id="statusModal" tabindex="-1" role="dialog" aria-labelledby="statusModalLabel" aria-hidden="true">--}}
{{--        <div class="modal-dialog" role="document">--}}
{{--            <div class="modal-content">--}}
{{--                <div class="modal-header">--}}
{{--                    <h5 class="modal-title" id="statusModalLabel">Change Status</h5>--}}
{{--                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">--}}
{{--                        <span aria-hidden="true">&times;</span>--}}
{{--                    </button>--}}
{{--                </div>--}}
{{--                <form method="POST" action="{{route('admin.changeStatus')}}">--}}
{{--                    @csrf--}}
{{--                    <input id="order_id" name="orderid" type="hidden" />--}}
{{--                    <div class="modal-body">--}}
{{--                        <select name="status" class="col-sm-12 select2" style="width:100%">--}}
{{--                            <option value="RECEIVED">RECEIVED</option>--}}
{{--                            <option value="ASSIGNED">ASSIGNED</option>--}}
{{--                            <option value="OUT_FOR_DELIVERY">OUT_FOR_DELIVERY</option>--}}
{{--                            <option value="DELIVERED">DELIVERED</option>--}}
{{--                            <option value="USER_CANCELLED">USER_CANCELLED</option>--}}
{{--                        </select>--}}
{{--                    </div>--}}
{{--                    <div class="modal-footer">--}}
{{--                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>--}}
{{--                        <input type="submit" class="btn btn-primary" value="Change Status">--}}
{{--                    </div>--}}
{{--                </form>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}

    <div class="modal fade" id="driverModal" tabindex="-1" role="dialog" aria-labelledby="driverModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="driverModalLabel">Assign Driver</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form method="POST" action="{{route('admin.assignServiceDriver')}}">
                    @csrf
                    <input id="order_id" name="orderid" type="hidden" />
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-sm-12">
                                <select name="driver_id" id="driver_id" class="form-control" style="width:100%">
                                    <option value="">Select Driver</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <input type="submit" class="btn btn-primary" value="Assign">
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection

@push('third_party_scripts')
<script type="text/javascript">
    $(document).ready(function (){

        $('#driverModal').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget);
            var order_id = button.data('orderid') // Extract info from data-* attributes
            // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
            // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
            var modal = $(this)
            modal.find('#order_id').val(order_id)
            getData('/api/service/drivers?order_id='+order_id);
        })
    });

    function getData(url) {
        console.log(url);
        $('#driver_id').select2({
            theme: 'classic',
            ajax: {
                url: url,
                dataType: 'json',
                processResults: function (data) {
                    // Transforms the top-level key of the response object from 'items' to 'results'
                    if(data.data.length > 0) {
                        return {
                            results: data.data.map(function (item) {
                                return {
                                    id: item.id,
                                    text: item.name,
                                    disabled: item.disabled
                                }
                            })
                        };
                    }
                }
            }
        });
    }
</script>
@endpush
