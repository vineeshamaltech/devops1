<!-- User Id Field -->
<div class="col-sm-6">
    {!! Form::label('user_id', __('models/orders.fields.user_id') . ':') !!}
    <p>{{ $order->user->name ?? 'Customer deleted' }}</p>
</div>

<!-- Store Id Field -->
{{-- <div class="col-sm-6">
    {!! Form::label('store_id', __('models/orders.fields.store_id').':') !!}
    <p>{{ $order->store->name ?? "No store" }}</p>
</div> --}}

<!-- Coupon Data Field -->
<div class="col-sm-6">
    {!! Form::label('coupon_data', __('models/orders.fields.coupon_data') . ':') !!}
    @if ($order->coupon_data != null)
        @foreach ($order->coupon_data as $key => $value)
            <p><strong>{{ Helpers::format_settings_title($key) }}</strong>: {{ $value }}</p>
        @endforeach
    @else
        <p>No coupon data</p>
    @endif
</div>

<!-- Total Price Field -->
<div class="col-sm-6">
    {!! Form::label('total_price', __('models/orders.fields.total_price') . ':') !!}
    <p>{{ $order->total_price }}</p>
</div>

<!-- Discount Type Field -->
<div class="col-sm-6">
    {!! Form::label('discount_type', __('models/orders.fields.discount_type') . ':') !!}
    <p>{{ $order->discount_type }}</p>
</div>

<!-- Tax Field -->
<div class="col-sm-6">
    {!! Form::label('tax', __('models/orders.fields.tax') . ':') !!}
    <p>{{ $order->tax }}</p>
</div>

<!-- Grand Total Field -->
<div class="col-sm-6">
    {!! Form::label('grand_total', __('models/orders.fields.grand_total') . ':') !!}
    <p>{{ $order->grand_total }}</p>
</div>

<!-- Pickup Address Field -->
{{-- <div class="col-sm-6">
    <div class="card">
        <div class="card-header">
            {!! Form::label('pickup_address', __('models/orders.fields.pickup_address').':') !!}
        </div>
        <div class="card-body">
            @foreach ($order->pickup_address as $key => $value)
                <p><strong>{{ Helpers::format_settings_title($key) }}</strong>: {{ $value }}</p>
            @endforeach
        </div>
    </div>
</div> --}}





<!-- Distance Travelled Field -->
<div class="col-sm-6">
    {!! Form::label('distance_travelled', __('models/orders.fields.distance_travelled') . ':') !!}
    <p>{{ $order->distance_travelled }}</p>
</div>

<!-- Travel Time Field -->
{{-- <div class="col-sm-6">
    {!! Form::label('travel_time', __('models/orders.fields.travel_time').':') !!}
    <p>{{ $order->travel_time }}</p>
</div> --}}

<!-- Payment Id Field -->
<div class="col-sm-6">
    {!! Form::label('payment_id', __('models/orders.fields.payment_id') . ':') !!}
    <p>{{ $order->payment_id }}</p>
</div>

<!-- Driver Id Field -->
{{-- <div class="col-sm-6">
    {!! Form::label('driver_id', __('models/orders.fields.driver_id').':') !!}
    <p>{{ $order->driver_id }}</p>
</div> --}}

<!-- Order Status Field -->
<div class="col-sm-6">
    {!! Form::label('order_status', __('models/orders.fields.order_status') . ':') !!}
    <p>{{ $order->order_status }}</p>
</div>

<!-- Json Data Field -->
<div class="col-sm-6">
    {!! Form::label('json_data', __('models/orders.fields.json_data') . ':') !!}
    @if ($order->json_data != null)
        @foreach ($order->json_data as $key => $value)
            <p><strong>{{ Helpers::format_settings_title($key) }}</strong>:
                @if (is_array($value))
                    @foreach ($value as $val)
                        <a href="{{ $val }}"><img src="{{ $val }}" width="100" height="100"></a>
                    @endforeach
                @else
                    {{ $value }}
                @endif
            </p>
        @endforeach
    @else
        <p>No json data</p>
    @endif
</div>

<!-- Created At Field -->
<div class="col-sm-6">
    {!! Form::label('created_at', __('models/orders.fields.created_at') . ':') !!}
    <p>{{ $order->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="col-sm-6">
    {!! Form::label('updated_at', __('models/orders.fields.updated_at') . ':') !!}
    <p>{{ $order->updated_at }}</p>
</div>

<!-- Delivery Address Field -->
<div class="col-sm-6">
    <div class="card">
        <div class="card-header">
            {!! Form::label('delivery_address', __('models/homeServiceOrders.fields.service_address') . ':') !!}
        </div>
        <div class="card-body">

            @if ($order->service_address != null)
                @foreach ($order->service_address as $key => $value)
                    <p><strong>{{ Helpers::format_settings_title($key) }}</strong>: {{ $value }}</p>
                @endforeach
            @endif
        </div>
    </div>
</div>
