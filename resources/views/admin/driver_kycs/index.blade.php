@extends('admin.layouts.app')
@section('title',trans('models/driverKycs.plural'))
@section('content')
    <div class="content">

        @include('flash::message')

        <div class="clearfix"></div>

        <div class="card">
            <div class="card-header">
                <ul class="nav nav-tabs align-items-end card-header-tabs w-100">
                    <li class="nav-item">
                        <a class="nav-link active" href="{!! url()->current() !!}"><i class="fa fa-list mr-2"></i>{{trans('models/driverKycs.plural')}}</a>
                    </li>
                    @can('driverKycs.create')
                    <li class="nav-item">
                        <a class="nav-link" href="{!! route('admin.driverKycs.create') !!}"><i class="fa fa-plus mr-2"></i>{{trans('lang.add')." ".trans('models/driverKycs.singular')}}</a>
                    </li>
                    @endcan
                    @include('admin.layouts.datatables_toolbar', compact('dataTable'))
                </ul>
            </div>

            <div class="card-body">

                @include('admin.driver_kycs.table')

                <div class="card-footer clearfix float-right">
                    <div class="float-right">

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection


