{{-- <!-- Vedor Id Field -->

<div class="col-sm-12">
    {!! Form::label('driver_id', __('models/driverKycs.fields.driver_id').':') !!}
    <p>{{ $driverKyc->driver_id }}</p>
</div>

<!-- Doc Type Field -->
<div class="col-sm-12">
    {!! Form::label('doc_type', __('models/driverKycs.fields.doc_type').':') !!}
    <p>{{ $driverKyc->doc_type }}</p>
</div>

<!-- Photo Field -->
<div class="col-sm-12">
    {!! Form::label('photo', __('models/driverKycs.fields.photo').':') !!}
    <p>{{ $driverKyc->photo }}</p>
</div>

<!-- Status Field -->
<div class="col-sm-12">
    {!! Form::label('status', __('models/driverKycs.fields.status').':') !!}
    <p>{{ $driverKyc->status }}</p>
</div>

<!-- Created At Field -->
<div class="col-sm-12">
    {!! Form::label('created_at', __('models/driverKycs.fields.created_at').':') !!}
    <p>{{ $driverKyc->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="col-sm-12">
    {!! Form::label('updated_at', __('models/driverKycs.fields.updated_at').':') !!}
    <p>{{ $driverKyc->updated_at }}</p>
</div>
 --}}


<div class="container col-md-12">
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">KYC Documents</h3>
        </div>
        <div class="card-body">
            <div class="row">
                
                @foreach($driverKyc as $kycDocument)
                    <div class="col-md-6">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    <div class="row justify-content-end">
                                        <div class="col-md-6">
                                            <h3 class="card-title">{{ $kycDocument->doc_type }}</h3>
                                            <small class="text-muted m-1"> (updated {{$kycDocument->updated_at->diffForHumans()}})</small>
                                        </div>
                                        <div class="col-md-6 justify-content-end">
                                            @if($kycDocument->status == 'UPLOADED')
                                                <div class="float-right"><span class="badge badge-warning">{{ $kycDocument->status }}</span></div>
                                            @elseif($kycDocument->status == "APPROVED")
                                                <div class="float-right"><span class="badge badge-success">{{ $kycDocument->status }}</span></div>
                                            @elseif($kycDocument->status == "REJECTED")
                                                <div class="float-right"><span class="badge badge-danger">{{ $kycDocument->status }}</span></div>
                                            @endif
                                        </div>
                                    </div>
    
                                </div>
                                <div class="card-body">
                                    <a target="_blank" href="{{$kycDocument->photo}}"><img src="{{$kycDocument->photo}}" class="img-fluid" alt="{{$kycDocument->doc_type}}"></a>
                                </div>
                                <div class="card-footer">
                                    <a href="{{route('admin.driverKycs.approve',['id'=>$kycDocument->id])}}" type="button" class="btn btn-success btn-sm">Accept</a>
                                    <a href="{{route('admin.driverKycs.reject',['id'=>$kycDocument->id])}}" type="button" class="btn btn-danger btn-sm">Reject</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
            </div>
           
        </div>
    </div>