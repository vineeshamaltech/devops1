{!! Form::open(['route' => ['admin.driverKycs.destroy', $id], 'method' => 'delete']) !!}
<div class='btn-group'>
    @can('driver_kycs.view')
    <a href="{{ route('admin.driverKycs.show', $id) }}" class='btn btn-default btn-xs'>
        <i class="fa fa-eye"></i>
    </a>
    @endcan
    @can('driver_kycs.edit')
    <a href="{{ route('admin.driverKycs.edit', $id) }}" class='btn btn-default btn-xs'>
        <i class="fa fa-edit"></i>
    </a>
    @endcan
    @can('driver_kycs.delete')
    {!! Form::button('<i class="fa fa-trash"></i>', [
        'type' => 'submit',
        'class' => 'btn btn-danger btn-xs',
        'onclick' => 'return confirm("'.__('crud.are_you_sure').'")'
    ]) !!}
    @endcan
</div>
{!! Form::close() !!}
