@extends('admin.layouts.app')

@section('title',trans('lang.add_new')." ".trans('models/driverKycs.singular'))
{{-- @section('title','Add kyc') --}}
@section('content')
    <div class="content">

        @include('adminlte-templates::common.errors')

        <div class="card">

            {!! Form::open(['route' => 'admin.driverKycs.store', 'files' => true]) !!}

            <div class="card-body">
                <div class="row">
                    @include('admin.driver_kycs.fields')
                </div>
            </div>

            <div class="card-footer">
                {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
                <a href="{{ route('admin.driverKycs.index') }}" class="btn btn-default">
                 @lang('lang.cancel')
                </a>
            </div>

            {!! Form::close() !!}

        </div>
    </div>
@endsection
