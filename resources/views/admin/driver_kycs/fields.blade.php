<!-- Vendor Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('driver_id', __('models/driverKycs.fields.driver_id').':',['class'=>'required']) !!}
    <select name="driver_id" class="form-control select2" required>
        @foreach(\App\Models\Driver::get() as $item)
            <option value="{{ $item->id }}"
                    @if(isset($driverKyc) && $driverKyc->driver_id == $item->id) selected @endif>{{ $item->name }} ( {{ $item->mobile }})</option>
        @endforeach
    </select>
</div>

<!-- Status Field -->
<div class="form-group col-sm-6">
    {!! Form::label('status', __('models/driverKycs.fields.status').':',['class'=>'required']) !!}
    {!! Form::select('status', ['UPLOADED'=>'Uploaded','APPROVED'=>'Approved','REJECTED'=>'Rejected'], null, ['class' => 'form-control select2','required'=>'true']) !!}
</div>

<!-- Photo Field -->
<div class="form-group col-sm-6">
    {!! Form::label('aadhar', 'Aadhar:') !!}
    <div class="input-group">
        <div class="custom-file">
            {!! Form::file('aadhar', ['class' => 'custom-file-input']) !!}
            {!! Form::label('aadhar', 'Choose file', ['class' => 'custom-file-label']) !!}
        </div>
    </div>
</div>
<div class="clearfix"></div>

<!-- Photo Field -->
<div class="form-group col-sm-6">
    {!! Form::label('aadhar_back', 'Aadhar Back:') !!}
    <div class="input-group">
        <div class="custom-file">
            {!! Form::file('aadhar_back', ['class' => 'custom-file-input']) !!}
            {!! Form::label('aadhar_back', 'Choose file', ['class' => 'custom-file-label']) !!}
        </div>
    </div>
</div>
<div class="clearfix"></div>

<!-- Photo Field -->
<div class="form-group col-sm-6">
    {!! Form::label('dl', 'Driving License:') !!}
    <div class="input-group">
        <div class="custom-file">
            {!! Form::file('dl', ['class' => 'custom-file-input']) !!}
            {!! Form::label('dl', 'Choose file', ['class' => 'custom-file-label']) !!}
        </div>
    </div>
</div>
<div class="clearfix"></div>

<!-- Photo Field -->
<div class="form-group col-sm-6">
    {!! Form::label('vehicle_img', 'Vehicle Image:') !!}
    <div class="input-group">
        <div class="custom-file">
            {!! Form::file('vehicle_img', ['class' => 'custom-file-input']) !!}
            {!! Form::label('vehicle_img', 'Choose file', ['class' => 'custom-file-label']) !!}
        </div>
    </div>
</div>
<div class="clearfix"></div>
