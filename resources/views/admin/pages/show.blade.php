@extends('layouts.pages')

@section('title',$page->title)

@section('content')
    {!! $page->content !!}
@endsection
