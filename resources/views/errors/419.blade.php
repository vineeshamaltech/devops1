<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>419|Page Expired</title>
    <link rel='stylesheet' href="{{asset('assets/css/error.css')}}"/>
</head>
<body>
<h1>Page expired</h1>
<p class="zoom-area">Please retry again</p>
<section class="error-container">
  <span class="four"><span class="screen-reader-text">4</span></span>
  <span class="zero"><span class="screen-reader-text">0</span></span>
  <span class="four"><span class="screen-reader-text">4</span></span>
</section>
<div class="link-container">
  <a href="{{url('/')}}" class="more-link">Go Back</a>
</div>
</body>
</html>