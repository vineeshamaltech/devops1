<?php
return [
    'roles' => 'Roles',
    'permissions' => 'Permissions',
    'manage_permissions' => 'Manage Permissions'
];