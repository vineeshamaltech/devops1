<?php

return [
    'title' => 'Earnings Report',
    'store' => 'Store',
    'vendor' => 'Vendor',
    'driver' => 'Driver',
    'type' => 'Type',
    'date' => 'Date',
    'email' => 'Email',
    'of' => 'Vendor/Driver'
];
