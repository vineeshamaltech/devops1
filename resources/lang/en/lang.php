<?php

return [
    // CRUD
    'add' => 'Add',
    'create' => 'Create',
    'edit' => 'Edit',
    'view' => 'View',
    'add_new' => 'Add New',
    'action' => 'Action',
    'export' => 'Export',
    'refresh' => 'Refresh',
    'print' => 'Print',
    'columns' => 'Columns',
    'reset' => 'Reset',
    'cancel' => 'Cancel',
    'back' => 'Back',
    'are_you_sure' => 'Are your sure ?',
    //Other commons
    'search' => 'Search',
    'impersonate' => 'Login as',
    //datatables
    'id' => 'ID',
    //messages
    'messages' => [
        'saved' => 'Saved Successfully',
        'deleted' => 'Deleted Successfully',
        'not_found' => 'Not found',
        'no_data_selected' => 'No Data Selected',
        'updated' => 'Updated Successfully',
        'success' => "Success",
        'retrieved' => 'Retrieved Successfully',
        'unauthorized' => 'Unauthorized',
        'insufficient_wallet_balance' => 'Insufficient wallet balance',
        'cart_not_found' => 'Cart not found',
        'registerd' =>'Mobile no alredy registered',
    ]
];
