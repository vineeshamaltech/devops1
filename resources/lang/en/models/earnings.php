<?php

return array (
  'singular' => 'Earnings',
  'plural' => 'Earnings',
  'fields' =>
  array (
    'id' => 'Id',
    'payment_id' => 'Payment',
      'order_id' => 'Order',
    'entity_name' => 'Credited To',
    'entity_id' => 'Admin/Driver/Store ID',
    'amount' => 'Amount',
    'remarks' => 'Remarks',
    'is_settled' => 'Is Settled',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
  ),
);
