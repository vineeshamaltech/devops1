<?php

return array (
  'singular' => 'StoreGallery',
  'plural' => 'StoreGalleries',
  'fields' =>
  array (
    'id' => 'Id',
    'store_id' => 'Store',
    'title' => 'Title',
    'description' => 'Description',
    'image' => 'Image',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
  ),
);
