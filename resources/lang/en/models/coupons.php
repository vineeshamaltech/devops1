<?php

return array (
  'singular' => 'Coupon',
  'plural' => 'Coupons',
  'fields' =>
  array (
    'id' => 'Id',
    'store_id' => 'Store Id',
    'store_name' => 'Store Name',
    'image' => 'Image',
    'code' => 'Code',
    'description' => 'Description',
    'max_use_per_user' => 'Max Use Per User',
    'min_cart_value' => 'Min Cart Value',
    'start_date' => 'Start Date',
    'end_date' => 'End Date',
    'discount_type' => 'Discount Type',
    'discount_value' => 'Discount Value',
    'max_discount_value' => 'Max Discount Value',
    'active' => 'Active',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
  ),
);
