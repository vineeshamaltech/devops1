<?php

return array (
  'singular' => 'Address',
  'plural' => 'Addresses',
  'fields' =>
  array (
    'id' => 'Id',
    'user_id' => 'User',
    'name' => 'Name',
    'phone' => 'Phone',
    'address' => 'Address',
    'lat' => 'Latitude',
    'long' => 'Longitude',
    'house_no' => 'House No',
    'building' => 'Building',
    'type' => 'Type',
    'description' => 'Description',
    'is_default' => 'Is Default',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
  ),
);
