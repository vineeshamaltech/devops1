<?php

return array (
  'singular' => 'Category',
  'plural' => 'Categories',
  'fields' =>
  array (
    'id' => 'Id',
    'name' => 'Name',
    'image' => 'Image',
    'store' => 'Store',
    'parent' => 'Parent',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
  ),
);
