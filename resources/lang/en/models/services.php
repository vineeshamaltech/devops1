<?php

return array (
  'singular' => 'Category',
  'plural' => 'Services',
  'fields' =>
  array (
    'id' => 'Id',
    'icon' => 'Icon',
    'bg_image' => 'Bg Image',
    'home_view' => 'Home View',
    'name' => 'Name',
    'description' => 'Description',
    'type' => 'Type',
    'discount' => 'Discount',
    'commission' => 'Commission',
    'vehicles' => 'Vehicles',
    'min_price' => 'Min Price',
    'driver_radius' => 'Driver Radius',
    'base_distance' => 'Base Distance',
    'base_price' => 'Base Price',
    'base_km' => 'Base Km',
    'price_km' => 'Price Km',
    'max_order_distance' => 'Max Order Distance',
    'payment_methods' => 'Payment Methods',
    'active' => 'Active',
  ),
);
