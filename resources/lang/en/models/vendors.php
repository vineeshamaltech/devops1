<?php

return array (
  'singular' => 'Vendor',
  'plural' => 'Vendors',
  'fields' => 
  array (
    'id' => 'Id',
    'name' => 'Name',
    'mobile' => 'Mobile',
    'email' => 'Email',
    'password' => 'Password',
    'fcm_token' => 'Fcm Token',
    'active' => 'Active',
    'verified' => 'Verified',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
  ),
);
