<?php

return array (
  'singular' => 'StopOver',
  'plural' => 'StopOvers',
  'fields' => 
  array (
    'id' => 'Id',
    'user_id' => 'User Id',
    'ride_id' => 'Ride Id',
    'name' => 'Name',
    'lat' => 'Lat',
    'long' => 'Long',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
  ),
);
