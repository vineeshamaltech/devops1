<?php

return array (
  'singular' => 'Store Category',
  'plural' => 'Store Categories',
  'fields' =>
  array (
    'id' => 'Id',
    'name' => 'Name',
    'image' => 'Image',
    'service_category_id' => 'Service Category',
    'active' => 'Active',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
  ),
);
