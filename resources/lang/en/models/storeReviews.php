<?php

return array (
  'singular' => 'StoreReviews',
  'plural' => 'StoreReviews',
  'fields' =>
  array (
    'id' => 'Id',
    'store_id' => 'Store',
    'title' => 'Title',
    'description' => 'Description',
    'rating' => 'Rating',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
  ),
);
