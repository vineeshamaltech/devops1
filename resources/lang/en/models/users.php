<?php

return array (
  'singular' => 'User',
  'plural' => 'Users',
  'fields' => 
  array (
    'id' => 'Id',
    'name' => 'Name',
    'mobile' => 'Mobile',
    'email' => 'Email',
    'password' => 'Password',
    'confirm_password' => 'Confirm Password',
    'profile_img' => 'Profile Img',
    'wallet_amount' => 'Wallet Amount',
    'fcm_token' => 'Fcm Token',
    'lat' => 'Lat',
    'long' => 'Long',
    'user_type' => 'User Type',
    'active' => 'Active',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
  ),
);
