<?php

return array (
  'singular' => 'Payment Methods',
  'plural' => 'Payment Methods',
  'fields' =>
  array (
    'id' => 'Id',
    'name' => 'Name',
    'description' => 'Description',
    'icon' => 'Icon',
    'payment_key' => 'Payment Key',
    'service' => 'Service Order',
    'home_service' => 'Home Service Order',
    'payment_secret' => 'Payment Secret',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
  ),
);
