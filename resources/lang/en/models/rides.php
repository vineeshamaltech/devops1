<?php

return array (
  'singular' => 'Ride',
  'plural' => 'Ride',
  'fields' =>
  array (
    'id' => 'Id',
    'user_id' => 'User Id',
    'from_address' => 'From Address',
    'to_address' => 'To Address',
    'from_lat' => 'From Lat',
    'from_long' => 'From Long',
    'to_lat' => 'To Lat',
    'to_long' => 'To Long',
    'travel_datetime' => 'Travel Datetime',
    'route' => 'Route',
    'max_copassengers' => 'Max Copassengers',
    'can_book_instantly' => 'Can Book Instantly',
    'price' => 'Price',
    'status' => 'Status',
    'active' => 'Active',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
  ),
);
