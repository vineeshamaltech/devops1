<?php

return array (
  'singular' => 'Zone',
  'plural' => 'Zones',
  'fields' => 
  array (
    'id' => 'Id',
    'title' => 'Title',
    'polygon' => 'Polygon',
    'active' => 'Active',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
  ),
);
