<?php

return array (
  'singular' => 'Delivery Fee',
  'plural' => 'Delivery Fees',
  'fields' =>
  array (
    'id' => 'Id',
    'from_km' => 'From Km',
    'to_km' => 'To Km',
    'delivery_fee' => 'Delivery Fee',
    'driver_commission' => 'Driver Commission in RS',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
  ),
);
