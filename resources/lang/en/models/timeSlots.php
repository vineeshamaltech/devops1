<?php

return array (
  'singular' => 'Home Service Time Slot',
  'plural' => ' Home Service Time Slots',
  'fields' =>
  array (
    'id' => 'Id',
    'store_id' => 'Store',
    'day' => 'Day',
    'from' => 'From',
    'to' => 'To',
    'max' => 'Max Bookings',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
  ),
);
