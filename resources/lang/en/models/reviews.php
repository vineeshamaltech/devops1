<?php

return array (
  'singular' => 'Review',
  'plural' => 'Reviews',
  'fields' => 
  array (
    'id' => 'Id',
    'order_id' => 'Order Id',
    'driver' => 'Driver',
    'store_rating' => 'Store Rating',
    'driver_rating' => 'Driver Rating',
    'store_review' => 'Store Review',
    'driver_review' => 'Driver Review',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
    'top_rating'=>'Out of 5'
  ),
);
