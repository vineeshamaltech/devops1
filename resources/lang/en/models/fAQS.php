<?php

return array (
  'singular' => 'FAQ',
  'plural' => 'FAQS',
  'fields' => 
  array (
    'id' => 'Id',
    'question' => 'Question',
    'answer' => 'Answer',
    'active' => 'Active',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
  ),
);
