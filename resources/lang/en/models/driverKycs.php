<?php

return array (
  'singular' => 'Driver Kyc',
  'plural' => 'Driver Kycs',
  'driver_id' => 'Driver',
  'fields' =>array (
    'id' => 'Id',
    'driver_id' => 'Driver',
    'doc_type' => 'Doc Type',
    'doc_count' => 'Document Count',
    'photo' => 'Photo',
    'status' => 'Status',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
  ),
);
