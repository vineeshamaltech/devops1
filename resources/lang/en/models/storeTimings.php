<?php

return array (
  'singular' => 'StoreTiming',
  'plural' => 'StoreTimings',
  'fields' =>
  array (
    'id' => 'Id',
    'store_id' => 'Store',
    'day' => 'Day',
    'open' => 'Open',
    'close' => 'Close',
    'from' => 'From',
    'to' => 'To',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
  ),
);
