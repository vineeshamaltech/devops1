<?php

return array (
  'singular' => 'User Wallet Transaction',
  'plural' => 'User Wallet Transactions',
  'fields' =>
  array (
    'id' => 'Id',
    'user_id' => 'User Id',
    'user' => 'User',
    'type' => 'Type',
    'amount' => 'Amount',
    'remarks' => 'Remarks',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
  ),
);
