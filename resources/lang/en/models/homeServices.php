<?php

return array (
  'singular' => 'Home Service',
  'plural' => 'Home Services',
  'fields' =>
  array (
    'id' => 'Id',
    'name' => 'Name',
    'description' => 'Description',
    'image' => 'Image',
    'home_service_category' => 'Home Service Category',
    'original_price' => 'Original Price',
    'discount_price' => 'Discounted Price',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
  ),
);
