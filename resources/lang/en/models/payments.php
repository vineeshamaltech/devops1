<?php

return array (
  'singular' => 'Payment',
  'plural' => 'Payments',
  'fields' => 
  array (
    'id' => 'Id',
    'ref_no' => 'Ref No',
    'payment_method' => 'Payment Method',
    'amount' => 'Amount',
    'gateway_response' => 'Gateway Response',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
  ),
);
