<?php

return array (
  'singular' => 'Banner',
  'plural' => 'Banners',
  'fields' =>
  array (
    'id' => 'Id',
    'store_id' => 'Store',
    'image' => 'Image',
    'title' => 'Title',
    'service_category_id' => 'Service Category Id',
    'home_service_category' => 'Home Service Category',
    'url' => 'Url',
    'type' => 'Type',
    'zones' => 'Zone',
    'active' => 'Active',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
  ),
);
