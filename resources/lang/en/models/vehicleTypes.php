<?php

return array (
  'singular' => 'Vehicle Type',
  'plural' => 'Vehicle Types',
  'fields' =>
  array (
    'id' => 'Id',
    'name' => 'Name',
    'icon' => 'Icon',
    'max_passengers' => 'Max. Passengers Allowed (except for the driver)',
    'marker_icon' => 'Marker Icon (for map)',
    'active' => 'Active',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
  ),
);
