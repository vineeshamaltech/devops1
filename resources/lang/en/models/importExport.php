<?php

return array (
    'singular' => 'Import/Export',
    'plural' => 'Imports/Exports',
    'fields' =>
        array (
            'type' => 'Type',
            'message' => 'Upload the CSV file',
            'import' => 'Import',
            'export' => 'Export',
            'email' => 'Email',
            'sample_product' => 'Download CSV file for Product table',
            'sample_category' => 'Download CSV file for Category table',
        ),
);
