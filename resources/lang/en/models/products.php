<?php

return array (
  'singular' => 'Product',
  'plural' => 'Products',
  'fields' =>
  array (
    'id' => 'Id',
    'store_id' => 'Store',
    'product_type' => 'Product Type',
    'name' => 'Name',
    'select_category' => 'Select Category',
    'description' => 'Description',
    'image' => 'Image',
    'unit' => 'Unit(Gram/Piece..)',
    'price' => 'Price',
    'tax' => 'Tax(%)',
    'discount_price' => 'Discount Price',
    'category_id' => 'Category',
    'sub_category_id' => 'Sub Category',
    'store_category_id' => 'Store Category',
    'sku' => 'Sku',
    'product_tags' => 'Product Tags',
    'active' => 'Active',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
  ),
);
