<?php

return array (
  'singular' => 'ServiceType',
  'plural' => 'ServiceTypes',
  'fields' => 
  array (
    'id' => 'Id',
    'name' => 'Name',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
  ),
);
