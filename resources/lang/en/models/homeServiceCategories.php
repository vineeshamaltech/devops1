<?php

return array (
  'singular' => 'Home Service Category',
  'plural' => 'Home Service Categories',
  'fields' =>
  array (
    'id' => 'Id',
    'name' => 'Name',
    'icon' => 'Icon',
    'background_image' => 'Background Image',
    'view' => 'View',
    'parent' => 'Parent',
    'tax' => 'Tax',
    'zones' => 'Zones',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
  ),
);
