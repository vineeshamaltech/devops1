<?php

return array (
  'singular' => 'Pincode',
  'plural' => 'Pincodes',
  'fields' => 
  array (
    'id' => 'Id',
    'order_id' => 'Order Id',
    'store_rating' => 'Store Rating',
    'driver_rating' => 'Driver Rating',
    'store_review' => 'Store Review',
    'driver_review' => 'Driver Review',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
  ),
);
