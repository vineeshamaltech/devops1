<?php

return array (
  'singular' => 'Vehicle',
  'plural' => 'Vehicles',
  'fields' => 
  array (
    'id' => 'Id',
    'brand' => 'Brand',
    'type' => 'Type',
    'variant' => 'Variant',
    'color' => 'Color',
    'reg_no' => 'Reg No',
    'photo' => 'Photo',
    'verified' => 'Verified',
    'active' => 'Active',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
  ),
);
