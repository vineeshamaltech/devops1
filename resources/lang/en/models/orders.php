<?php

return array (
  'singular' => 'Order',
  'plural' => 'Orders',
  'fields' =>
  array (
    'id' => 'Id',
    'user_id' => 'User',
    'store_id' => 'Store',
    'coupon_data' => 'Coupon Data',
    'order_type' => 'Order Type',
    'user_phone' => 'User Phone No.',
    'total_price' => 'Total Price',
    'discount_type' => 'Discount Type',
    'delivery_fee' => 'Delivery Fee',
    'tax' => 'Tax',
    'tip' => 'Tip',
    'grand_total' => 'Grand Total',
    'pickup_address' => 'Pickup Address',
    'delivery_address' => 'Delivery Address',
    'distance_travelled' => 'Distance Travelled',
    'travel_time' => 'Travel Time',
    'payment_id' => 'Payment Id',
    'driver_id' => 'Driver',
    'vehicle_id' => 'Vehicle',
    'order_status' => 'Order Status',
      'pickup_otp' => 'Pickup OTP',
      'delivery_otp' => 'Delivery OTP',
      'payment_status' => 'Payment Status',
      'payment_method' => 'Payment Method',
    'json_data' => 'Json Data',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
    'earnings' => 'Earning Details'
  ),
);
