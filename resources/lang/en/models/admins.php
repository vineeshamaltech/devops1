<?php

return array (
  'singular' => 'Admin/Vendor',
  'plural' => 'Admins & Vendors',
  'fields' =>
  array (
    'id' => 'Id',
    'name' => 'Name',
    'mobile' => 'Mobile (with +91)',
    'email' => 'Email',
    'password' => 'Password',
    'verified' => 'Verified',
    'active' => 'Active',
    'type' => 'Type',
    'zone' => 'Zone',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
  ),
);
