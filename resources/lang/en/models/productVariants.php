<?php

return array (
  'singular' => 'Product Variant',
  'plural' => 'Product Variants',
  'fields' =>
  array (
    'id' => 'Id',
    'product_id' => 'Product',
    'name' => 'Name',
    'price' => 'Price',
    'sku' => 'Sku',
    'image' => 'Image',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
  ),
);
