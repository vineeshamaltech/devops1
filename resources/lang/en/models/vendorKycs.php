<?php

return array (
  'singular' => 'Vendor Kyc',
  'plural' => 'Vendor Kycs',
  'fields' =>
  array (
    'id' => 'Id',
    'vendor_id' => 'Vendor',
    'doc_type' => 'Doc Type',
    'doc_count' => 'Document Count',
    'aadhar' => 'Aadhar',
    'pan' => 'Pan',
    'fssai' => 'FSSAI',
    'gst' => 'GST',
    'other' => 'Other',
    'photo' => 'Photo',
    'status' => 'Status',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
  ),
);
