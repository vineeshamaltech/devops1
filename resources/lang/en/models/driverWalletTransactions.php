<?php

return array (
  'singular' => 'Driver Wallet Transaction',
  'plural' => 'Driver Wallet Transactions',
  'fields' =>
  array (
    'id' => 'Id',
    'driver_id' => 'Driver',
    'type' => 'Type',
    'amount' => 'Amount',
    'remarks' => 'Remarks',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
  ),
);
