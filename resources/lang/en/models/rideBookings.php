<?php

return array (
  'singular' => 'RideBooking',
  'plural' => 'RideBookings',
  'fields' => 
  array (
    'id' => 'Id',
    'user_id' => 'User Id',
    'ride_id' => 'Ride Id',
    'from_name' => 'From Name',
    'to_name' => 'To Name',
    'from_lat' => 'From Lat',
    'from_long' => 'From Long',
    'to_lat' => 'To Lat',
    'to_long' => 'To Long',
    'price' => 'Price',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
  ),
);
