<?php
return [
    'title' => 'Reports',
    'generated_reports' => 'Generated Reports',
    'range' => 'Range',
    'export' => 'Export'
];
