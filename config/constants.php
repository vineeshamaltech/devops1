<?php
return array (
  'currency_symbol' => '₹',
  'dynamic_delivery_ids' =>
  array (
    0 => 2,
    1 => 3,
  ),
  'dynamic_tax_ids' =>
  array (
    0 => 2,
    1 => 3,
    2 => 4,
  ),
  'cab_service_type_id' => 4,
  'car_pool_booking_feature' => true,
  'purchasing_service_feature' => true,
  'features' =>
  array (
    'car_pool_booking' => true,
    'home_service_booking' => true,
    'passenger_transportation_service' => true,
    'purchasing_service' => true,
    'pick_n_drop_service' => true,
    'order_anything_service' => true,
  ),
  'carpool_permissions_list' =>
  array (
    0 => 'rides',
    1 => 'rideBookings',
  ),
  'home_service_permissions_list' =>
  array (
    0 => 'homeServiceCategories',
    1 => 'homeServiceOrders',
    2 => 'homeServices',
  ),
  'purchasing_service_permissions_list' =>
  array (
    0 => 'stores',
    1 => 'storeTimings',
    2 => 'storeCategories',
    3 => 'products',
    4 => 'categories',
    5 => 'coupons',
  ),
);
